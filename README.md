# Bleugrain (Vocac) - An online agriculture marketplace 

## [[Website's screenshots](Bleugrain_An_online_agriculture_marketplace.pdf)]

## Problem, Solution and Benefits:

We all know that from many years farmers are facing many problems while they have to sell their farm produce into the market. Technologies is growing rapidly but there is nothing which can solve their problems in this regard. They still have to go to the market with their farm produce and wait for the agents to start bidding and if the final bid price is not as per their expectations, they still have to sell their farm produce into the market. otherwise it will increase the total cost of bringing farm produce into the market and then taking it back to their native place and then come again another day to follow the same process. 

So what problem we are trying to solve with this application is to give options to the farmer to sell their farm produce online without taking it to the market. We mean we are creating a marketplace for the farmers where they can list out their farm produce for its buyers, make it available for the bidding and take a decision to sell or don't sell without doing round trips to the market.

In legacy market they have only 2-3 options to sell their farm produce into the nearby markets. but with this application they can sell it anywhere in the world. we will make the neccessary arrangements to deliver it to the final customer. they don't have to worry about anything like payment and all. we will handle it. they don't have to make multiple rounds to the aadhatiyas to take their money. we will transfer the money to their account electronically within 2-3 days of sell.

Bleugrain had started its operations in Nov 2014. From than it is continuously improving its services in India. At Bleugrain, we are providing following benefits and services to the our various stakeholders (farmers, traders, companies). 

### Services To Food Processors/Companies:

1. Realtime Bidding System
2. Location Aware Product Listing
3. Complete Protfolio Management
4. Product & Demand Management
5. Effective & Efficient Decisioning
6. High Quality Product Analysis
7. 24x7 Availability & On-Time Delivery
8. Ordering From Anywhere In India
9. Full Security & Reliability
10. Fully Transparent Transport Charges
11. Cost Reduction

### Services for The Farmers

1. Free of Cost Realtime Biddings
2. Opportunities to Sell Anywhere in India.
3. 24x7 Availability & Transport Facilities
4. Options to Sell or Don't Sell without bearing Transport Charges.
5. Option To Sell at Your Own Price

### Services for The Traders:

1. Realtime Bidding on Your Products
2. Location Aware Product Listing
3. Complete Portfolio Management
4. Trader's Trading Services
5. Product & Demand Management
6. Effective & Efficient Decisioning
7. High Quality Product Analysis
8. 24x7 Availability & On-Time Delivery
9. Ordering From Anywhere In India
10. Fully Transparent Transport Charges
11. Transport Facilities
12. Opportunities To Sell Anywhere In India or Outside India