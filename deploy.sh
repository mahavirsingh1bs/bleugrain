#!/bin/bash
# Script to deploy the application

LOAD_BALANCER="52.77.119.121"
PROD_SERVER="54.169.34.71"
WORKSPACE="awsws"
PRIVATE_KEY_FILE="admin-key-pair-asia-pacific-singapore.pem"
BLEUGRAIN_WAR="bleugrain-1.0.0-Final.war"
ROOT_WAR="ROOT.war"


echo ""
echo "Taking latest update and creating build"
svn update && mvn clean install -DskipTests

echo ""
echo "Moving bleugrain war into the ROOT war"
mv ~/Local_Disk_D/JavaEE6/TOGAF9/$WORKSPACE/bleugrain/target/$BLEUGRAIN_WAR ~/Local_Disk_D/JavaEE6/TOGAF9/$WORKSPACE/bleugrain/target/$ROOT_WAR


echo ""
echo "copying $ROOT_WAR onto the $PROD_SERVER"
scp -i ~/Documents/prakrati/$PRIVATE_KEY_FILE ~/Local_Disk_D/JavaEE6/TOGAF9/$WORKSPACE/bleugrain/target/$ROOT_WAR ec2-user@$PROD_SERVER:~/backup/

echo ""
echo "################ ################"
echo "Logging into the $PROD_SERVER"

echo ""
echo "Stopping tomcat8 on $PROD_SERVER"
ssh -i ~/Documents/prakrati/$PRIVATE_KEY_FILE ec2-user@$PROD_SERVER 'sudo service tomcat8 stop'

echo ""
echo "Starting activemq on $PROD_SERVER"
ssh -i ~/Documents/prakrati/$PRIVATE_KEY_FILE ec2-user@$PROD_SERVER '~/Programs/apache-activemq-5.13.3/bin/activemq start'

echo ""
echo "Starting redis-server on $PROD_SERVER"
ssh -i ~/Documents/prakrati/$PRIVATE_KEY_FILE ec2-user@$PROD_SERVER 'redis-server /etc/redis/redis.conf'

echo ""
echo "Testing redis-server whether is it up or not"
redis-cli -h localhost -a secret ping

echo "Redis Server is running"

echo ""
echo "Deleting ROOT application on $PROD_SERVER"
ssh -i ~/Documents/prakrati/$PRIVATE_KEY_FILE ec2-user@$PROD_SERVER 'sudo rm -fR /usr/share/tomcat8/webapps/ROOT*'

echo ""
echo "Copying ROOT.war file into the webapps $PROD_SERVER"
ssh -i ~/Documents/prakrati/$PRIVATE_KEY_FILE ec2-user@$PROD_SERVER 'sudo cp ~/backup/ROOT.war /usr/share/tomcat8/webapps/'

echo ""
echo "Starting tomcat8 on $PROD_SERVER"
ssh -i ~/Documents/prakrati/$PRIVATE_KEY_FILE ec2-user@$PROD_SERVER 'sudo service tomcat8 start'

echo ""
echo "Waiting for tomcat8 to get up on $PROD_SERVER"
ssh -i ~/Documents/prakrati/$PRIVATE_KEY_FILE ec2-user@$PROD_SERVER 'wget -q http://localhost:8080/#/'

echo ""
echo "Disabling search indexing on $PROD_SERVER"
ssh -i ~/Documents/prakrati/$PRIVATE_KEY_FILE ec2-user@$PROD_SERVER 'sudo sed -i "s/search.index.enabled = true/search.index.enabled = false/g" /usr/share/tomcat8/webapps/ROOT/WEB-INF/classes/application.properties'

echo ""
echo "Updating new server address in load balancer"
ssh -i ~/Documents/prakrati/$PRIVATE_KEY_FILE ec2-user@$LOAD_BALANCER 'sudo sed -i "s/54.169.109.179:8080/54.169.34.71:8080/g" /etc/haproxy/haproxy.cfg && sudo service haproxy restart'

