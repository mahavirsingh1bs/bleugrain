package com.carefarm.prakrati.listeners;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.index.ElasticsearchIndexer;

@Component
public class ElasticsearchIndexListener implements ApplicationListener<ContextRefreshedEvent>, Ordered {

	private static final Logger LOGGER = LoggerFactory.getLogger(ElasticsearchIndexListener.class);
	
	private static final int ORDER = 20;
	
	@Autowired
	private boolean searchIndexEnabled;
	
	@Autowired
	private ElasticsearchIndexer indexer;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		if (searchIndexEnabled) {
			LOGGER.info("##################  Elasticsearch indexing has been got Started  ##################");
			indexer.createIndexes();
	        LOGGER.info("##################  Elasticsearch indexing has been Completed Successfully ##################");
		} else {
			LOGGER.info("##################  Elasticsearch indexing on start is not enabled ##################");
		}
	}

	@Override
	public int getOrder() {
		return ORDER;
	}

}
