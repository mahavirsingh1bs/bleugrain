package com.carefarm.prakrati.listeners;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.springframework.stereotype.Component;

import com.carefarm.prakrati.holder.ServletContextHolder;

@Component
@WebListener
public class ServletContextHolderInitializer implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent event) {
		ServletContextHolder.unholdServletContext();
	}

	@Override
	public void contextInitialized(ServletContextEvent event) {
		ServletContextHolder.holdServletContext(event.getServletContext());
	}

}
