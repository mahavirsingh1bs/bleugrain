package com.carefarm.prakrati.listeners;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.session.data.redis.SessionMessageListener;

public class PrakratiSessionMessageListener extends SessionMessageListener {

	public PrakratiSessionMessageListener(ApplicationEventPublisher eventPublisher) {
		super(eventPublisher);
	}

}
