package com.carefarm.prakrati.listeners;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.index.IndexAndMappingCreator;

@Component
public class ElasticsearchMappingListener implements ApplicationListener<ContextRefreshedEvent>, Ordered {

	private static final Logger LOGGER = LoggerFactory.getLogger(ElasticsearchMappingListener.class);

	private static final int ORDER = 10;
	
	@Autowired
	private boolean createIndexAndMappingEnabled;
	
	@Autowired
	private IndexAndMappingCreator indexAndMappingCreator;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent arg0) {
		if (createIndexAndMappingEnabled) {
			LOGGER.info("##################  Creation of Elasticsearch indexes and mappings has been started ##################");
			indexAndMappingCreator.createIndexesAndMappings();
	        LOGGER.info("##################  Creation of Elasticsearch indexes and mappings has been finished ##################");
		} else {
			LOGGER.info("##################  Creation of Elasticsearch indexes and mappings is not enabled ##################");
		}
	}
	
	@Override
	public int getOrder() {
		return ORDER;
	}

}
