package com.carefarm.prakrati.listeners;

import static com.carefarm.prakrati.core.Environment.clock;
import static java.util.Date.from;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.repository.UserRepository;

@Component
public class SuccessfulLoginListener {
	
	@Autowired
	private UserRepository userRepository;
	
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void onSuccessfulLogin(String userNo) {
		User loggedInUser = userRepository.findByUserNo(userNo);
		loggedInUser.setLastLoginOn(from(clock().instant()));
	}
	
}
