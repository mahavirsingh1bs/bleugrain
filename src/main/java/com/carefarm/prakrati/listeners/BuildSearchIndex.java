/*package com.carefarm.prakrati.listeners;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.search.SearchIndexer;

@Component
public class BuildSearchIndex implements ApplicationListener<ContextRefreshedEvent> {

	private static final Logger LOGGER = LoggerFactory.getLogger(BuildSearchIndex.class);
	
	@Autowired
	private boolean searchIndexEnabled;
	
	@Autowired
	private SearchIndexer searchIndexer;
	
	@Override
	public void onApplicationEvent(ContextRefreshedEvent event) {
		if (searchIndexEnabled) {
			LOGGER.info("##################  Indexing Started  ##################");
			searchIndexer.createIndexes();
	        LOGGER.info("##################  Indexing Completed  ##################");
		} else {
			LOGGER.info("##################  Indexing on start is not enabled ##################");
		}
	}

}
*/