package com.carefarm.prakrati.authentication;

import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;

import com.carefarm.prakrati.entity.User;

public class DomainUsernamePasswordAuthenticationToken extends
		UsernamePasswordAuthenticationToken {
	private static final long serialVersionUID = 8750899845510209762L;
	
	private String domain;
	
	public DomainUsernamePasswordAuthenticationToken(String principal, 
			String credentials, String domain) {
		super(principal, credentials);
		this.domain = domain;
	}
	
	public DomainUsernamePasswordAuthenticationToken(User principal, 
			String credentials, String domain, 
			Collection<? extends GrantedAuthority> authorities) {
		super(principal, credentials, authorities);
		this.domain = domain;
	}
	
	public String getDomain() {
		return this.domain;
	}

}
