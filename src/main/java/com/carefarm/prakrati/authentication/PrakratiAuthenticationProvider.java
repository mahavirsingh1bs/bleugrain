package com.carefarm.prakrati.authentication;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.entity.Group;
import com.carefarm.prakrati.entity.Role;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.security.PasswordManager;

@Component
public class PrakratiAuthenticationProvider implements AuthenticationProvider {
	private final UserRepository userRepository;
	
	@Autowired
	public PrakratiAuthenticationProvider(UserRepository userRepository) {
		if (userRepository == null) {
			throw new IllegalArgumentException("userRepository cannot be null");
		}
		this.userRepository = userRepository;
	}
	
	@Override
	public Authentication authenticate(Authentication authentication)
			throws AuthenticationException {
		DomainUsernamePasswordAuthenticationToken token = 
				(DomainUsernamePasswordAuthenticationToken )authentication;
		String username = token.getName().trim();
		
		String domain = null;
		if (StringUtils.isNoneEmpty(token.getDomain())) {
			domain = token.getDomain().trim();
			username = username + "@" + domain;
		}
		
		User user = username == null ? null : userRepository.findByUsernameOrAlternateUsername(username);
		if (Predicates.isNull.test(user)) {
			throw new UsernameNotFoundException("Invalid username for this domain");
		}
		
		String password = user.getPassword().trim();
		String credentials = token.getCredentials().toString();
		if (!StringUtils.equals(password, PasswordManager.encryptPasswd(credentials))) {
			throw new BadCredentialsException("Invalid username/password");
		}
		Collection<? extends GrantedAuthority> authorities = 
				createAuthorities(user);
		return new DomainUsernamePasswordAuthenticationToken(
				user, password, domain, authorities);
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return DomainUsernamePasswordAuthenticationToken.class.equals(authentication);
	}
	
	public Collection<? extends GrantedAuthority> createAuthorities(User user) {
		Group group = user.getGroup();
		Set<SimpleGrantedAuthority> authorities = new HashSet<SimpleGrantedAuthority>();
		for (Role role : group.getRoles()) {
			authorities.add(new SimpleGrantedAuthority(role.getName()));
		}
		return authorities;
	}

}
