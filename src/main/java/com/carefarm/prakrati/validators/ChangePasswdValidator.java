package com.carefarm.prakrati.validators;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.security.PasswordManager;
import com.carefarm.prakrati.web.model.ChangePasswdForm;

@Component
public class ChangePasswdValidator implements Validator {

	@Autowired
	private UserRepository userRepository;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return ChangePasswdForm.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ChangePasswdForm form = (ChangePasswdForm )target;
		
		User user = form.getUserId() == null ? null : userRepository.findByUserNo(form.getUserId());
		if (user == null) {
			errors.reject("errors.settings.userId", "User is not registered with us.");
		}
		
		if (!StringUtils.equals(user.getPassword(), PasswordManager.encryptPasswd(form.getOldPasswd()))) {
			errors.rejectValue("oldPasswd", "errors.signup.oldPassword", "Old Password is not correct.");
		}
		
		if (StringUtils.equals(user.getPassword(), PasswordManager.encryptPasswd(form.getNewPasswd()))) {
			errors.rejectValue("newPasswd", "errors.signup.newPasswd", "New Password should be different from old password");
		}
		
		if (!form.getNewPasswd().equals(form.getConfirmNewPasswd())) {
			errors.rejectValue("confirmNewPasswd", "errors.signup.confirmNewPasswd", "New Passwords are not identical.");
		}
	}

}
