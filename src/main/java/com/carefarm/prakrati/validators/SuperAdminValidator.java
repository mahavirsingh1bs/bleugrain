package com.carefarm.prakrati.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.web.model.SuperAdminForm;

@Component
public class SuperAdminValidator implements Validator {

	@Autowired
	private UserRepository userRepository;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return SuperAdminForm.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		SuperAdminForm form = (SuperAdminForm )target;
		if(userRepository.findByEmailAddress(form.getEmail()) != null) {
            errors.rejectValue("email", "errors.signup.email", "Email address is already in use.");
        }
	}

}
