package com.carefarm.prakrati.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import static com.carefarm.prakrati.common.util.Constants.PLUS;
import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.web.model.FarmerForm;

@Component
public class FarmerValidator implements Validator {
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return FarmerForm.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		FarmerForm farmer = (FarmerForm )target;
		
        if (Predicates.notNull.test(userRepository.findByAlternateUsername(PLUS + farmer.getMobileNo()))) {
        	errors.rejectValue("mobileNo", "errors.signup.mobileNo", "Mobile Number is already in use.");
        }
		
		if(Predicates.notNull.test(farmer.getEmail()) && 
        		Predicates.notNull.test(userRepository.findByEmailAddress(farmer.getEmail()))) {
            errors.rejectValue("email", "errors.signup.email", "Email address is already in use.");
        }		
	}

}
