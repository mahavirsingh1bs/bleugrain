package com.carefarm.prakrati.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.web.model.CompanyAdminForm;

@Component
public class CompanyAdminValidator implements Validator {

	@Autowired
	private UserRepository userRepository;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return CompanyAdminForm.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		CompanyAdminForm form = (CompanyAdminForm )target;
		if(userRepository.findByEmailAddress(form.getEmail()) != null) {
            errors.rejectValue("email", "errors.signup.email", "Email address is already in use.");
        }
	}

}
