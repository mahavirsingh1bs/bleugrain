package com.carefarm.prakrati.validators;

import static com.carefarm.prakrati.common.util.Constants.PLUS;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.web.model.BrokerForm;

@Component
public class BrokerValidator implements Validator {

	@Autowired
	private UserRepository userRepository;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return BrokerForm.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		BrokerForm form = (BrokerForm )target;
		
		if(Predicates.isNull.test(form.getLicenseNo())) {
            errors.rejectValue("licenseNo", "errors.signup.licenseNo", "License No. is required.");
        }
        
		if(Predicates.isNull.test(form.getEmail())) {
            errors.rejectValue("email", "errors.signup.email", "Email address is required.");
        }
        
		if(Predicates.notNull.test(form.getEmail()) && 
        		Predicates.notNull.test(userRepository.findByEmailAddress(form.getEmail()))) {
            errors.rejectValue("email", "errors.signup.email", "Email address is already in use.");
        }
        
        if (Predicates.notNull.test(userRepository.findByAlternateUsername(PLUS + form.getMobileNo()))) {
        	errors.rejectValue("mobileNo", "errors.signup.mobileNo", "Mobile No. is already in use.");
        }
	}

}
