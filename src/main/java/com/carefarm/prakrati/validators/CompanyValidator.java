package com.carefarm.prakrati.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.repository.CompanyRepository;
import com.carefarm.prakrati.web.model.CompanyForm;
import com.carefarm.prakrati.web.model.RegisterForm;

@Component
public class CompanyValidator implements Validator {

	@Autowired
	private CompanyRepository companyRepository;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return CompanyForm.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		RegisterForm form = (RegisterForm )target;
		
		String domain = form.getEmailAddress().substring(form.getEmailAddress().indexOf("@") + 1);
        if(Predicates.notNull.test(companyRepository.findByDomain(domain))) {
            errors.rejectValue("domain", "errors.signup.domain", "A Company with the same domain is already registered.");
        }
        
	}

}
