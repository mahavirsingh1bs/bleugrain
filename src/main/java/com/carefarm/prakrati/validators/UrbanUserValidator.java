package com.carefarm.prakrati.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.web.model.RegisterForm;

@Component
public class UrbanUserValidator implements Validator {

	@Autowired
	private UserRepository userRepository;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return RegisterForm.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		RegisterForm form = (RegisterForm )target;
		
		if (!form.getPassword().equals(form.getConfirmPassword())) {
			errors.rejectValue("password", "errors.signup.password", "Passwords are not identical.");
		}
		
        if (Predicates.notNull.test(userRepository.findByAlternateUsername(form.getMobileNo()))) {
        	errors.rejectValue("mobileNo", "errors.signup.mobileNo", "Mobile No. is already in use.");
        }
		
		if(Predicates.notNull.test(form.getEmailAddress()) && 
        		Predicates.notNull.test(userRepository.findByEmailAddress(form.getEmailAddress()))) {
            errors.rejectValue("emailAddress", "errors.signup.emailAddress", "Email address is already in use.");
        }
	}
	
}
