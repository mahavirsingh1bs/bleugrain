package com.carefarm.prakrati.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.entity.Product;
import com.carefarm.prakrati.repository.ProductRepository;
import com.carefarm.prakrati.web.model.BidForm;

@Component
public class BidValidator implements Validator {

	@Autowired
	private ProductRepository productRepository;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return BidForm.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		BidForm form = (BidForm )target;
		Product product = productRepository.findByUniqueId(form.getUniqueId());
		if (form.getPrice().compareTo(product.getInitialBidAmount()) <= 0) {
			errors.rejectValue("price", "errors.bid.price", "Bid price is less than the highest bid.");
			return;
		}
		
		if (Predicates.notNull.test(product.getHighestBid()) && form.getPrice().compareTo(product.getHighestBid().getPrice()) <= 0) {
			errors.rejectValue("price", "errors.bid.price", "Bid price is less than the highest bid.");
		}
	}

}
