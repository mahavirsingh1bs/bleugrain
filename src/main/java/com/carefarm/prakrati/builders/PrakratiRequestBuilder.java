package com.carefarm.prakrati.builders;

import org.springframework.data.domain.Pageable;

import com.carefarm.prakrati.core.PrakratiRequest;

public class PrakratiRequestBuilder {
	private static final PrakratiRequestBuilder INSTANCE = new PrakratiRequestBuilder();
	private PrakratiRequest prakratiRequest;
	
	public static PrakratiRequestBuilder getInstance() {
		INSTANCE.setPrakratiRequest(new PrakratiRequest());
		return INSTANCE;
	}
	
	public PrakratiRequestBuilder setPageable(Pageable pageable) {
		INSTANCE.prakratiRequest.setPageable(pageable);
		return INSTANCE;
	}
	
	public PrakratiRequestBuilder setUserId(Long userId) {
		INSTANCE.prakratiRequest.setUserId(userId);
		return INSTANCE;
	}
	
	public PrakratiRequestBuilder setCompanyId(Long companyId) {
		INSTANCE.prakratiRequest.setCompanyId(companyId);
		return INSTANCE;
	}
	
	public PrakratiRequestBuilder addParameter(String name, Object value) {
		INSTANCE.prakratiRequest.addParamter(name, value);
		return INSTANCE;
	} 
	
	public PrakratiRequest build() {
		return INSTANCE.prakratiRequest;
	}
	
	private void setPrakratiRequest(PrakratiRequest prakratiRequest) {
		this.prakratiRequest = prakratiRequest;
	}
}
