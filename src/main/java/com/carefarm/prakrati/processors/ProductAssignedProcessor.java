package com.carefarm.prakrati.processors;

import java.util.Locale;

import com.carefarm.prakrati.common.util.Constants;
import com.carefarm.prakrati.util.NotificationCategory;
import com.carefarm.prakrati.web.bean.ProductBean;

public class ProductAssignedProcessor extends PrakratiProcessor {
	
	public void process(ProductBean product) {
		Object[] params = new Object[] { product.getAssignedTo().getFullName() };
		String notification = context.getMessage(Constants.PRODUCT_ASSIGNED, params, Locale.US);
		this.processInternal(NotificationCategory.PRODUCT_ASSIGNED, notification, product.getAssignedTo().getUserNo());
	}
	
}
