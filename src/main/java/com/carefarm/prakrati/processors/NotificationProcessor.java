package com.carefarm.prakrati.processors;

import static com.carefarm.prakrati.common.util.Constants.NOTIFICATIONS_TOPIC;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.service.NotificationService;
import com.carefarm.prakrati.service.dto.NotificationDTO;

@Component
public class NotificationProcessor {

	@Autowired
	private NotificationService notificationService;
	
	public void process(NotificationDTO notificationDTO) {
		notificationService.sendNotification(NOTIFICATIONS_TOPIC, notificationDTO);
	}
}
