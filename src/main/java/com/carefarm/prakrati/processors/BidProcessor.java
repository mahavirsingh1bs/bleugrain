package com.carefarm.prakrati.processors;

import static com.carefarm.prakrati.common.util.Constants.BID;
import static com.carefarm.prakrati.common.util.Constants.BIDS_TOPIC;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.events.SocketEvent;
import com.carefarm.prakrati.web.bean.BidBean;

@Component
public class BidProcessor {

	@Autowired
	private SimpMessagingTemplate template;
	
	public void process(BidBean bid) {
		template.convertAndSend(BIDS_TOPIC, SocketEvent.createEvent(BID, bid));
	}
}
