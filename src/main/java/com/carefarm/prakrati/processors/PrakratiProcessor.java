package com.carefarm.prakrati.processors;

import static com.carefarm.prakrati.common.util.Constants.NOTIFICATIONS_TOPIC;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;

import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.messaging.AccountEmailHandler;
import com.carefarm.prakrati.messaging.MobileMessanger;
import com.carefarm.prakrati.messaging.OrderEmailHandler;
import com.carefarm.prakrati.service.NotificationService;
import com.carefarm.prakrati.service.dto.NotificationDTO;
import com.carefarm.prakrati.util.GlobalConstants;
import com.carefarm.prakrati.util.NotificationCategory;

public abstract class PrakratiProcessor {
	
	protected static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(GlobalConstants.DATE_FORMAT.getValue());
	
	@Value("${vocac.sms.order.enabled}")
	protected boolean orderSmsEnabled;
	
	@Value("${vocac.email.order.enabled}")
	protected boolean orderEmailEnabled;
	
	@Autowired
	protected AccountEmailHandler accountEmailHandler;

	@Autowired
	protected OrderEmailHandler orderEmailHandler;
	
	@Autowired
	protected MobileMessanger mobileMessanger;
	
	@Autowired
	protected ApplicationContext context;

	@Autowired
	protected NotificationService notificationService;

	protected void processInternal(NotificationCategory category, String notification, List<String> users) {
		NotificationDTO dispatcherNotification = NotificationDTO.Builder.getInstance()
				.category(category)
				.notification(notification)
				.users(users)
				.startDate(Date.from(Environment.clock().instant()))
				.expiryDate(Date.from(Environment.clock().instant().plus(5L, ChronoUnit.DAYS)))
				.build();
		notificationService.sendNotification(NOTIFICATIONS_TOPIC, dispatcherNotification);
	}
	
	protected void processInternal(NotificationCategory category, String notification, String user) {
		this.processInternal(category, notification, Arrays.asList(user));
	}
}
