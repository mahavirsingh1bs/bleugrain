package com.carefarm.prakrati.processors;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.orm.jpa.persistenceunit.MutablePersistenceUnitInfo;
import org.springframework.orm.jpa.persistenceunit.PersistenceUnitPostProcessor;
import org.springframework.stereotype.Component;

@Component
public class MergingPersistentUnitPostProcessor implements PersistenceUnitPostProcessor {

	@Value("${search.index.base}")
	private String indexBase;
	
	@Override
	public void postProcessPersistenceUnitInfo(MutablePersistenceUnitInfo pui) {
		pui.getProperties().setProperty("index.base", indexBase);
	}

}
