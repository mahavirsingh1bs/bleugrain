package com.carefarm.prakrati.processors;

import static com.carefarm.prakrati.common.util.Constants.NOTIFICATIONS_TOPIC;

import java.sql.Date;
import java.text.MessageFormat;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.common.util.Constants;
import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.service.dto.NotificationDTO;
import com.carefarm.prakrati.util.NotificationCategory;
import com.carefarm.prakrati.util.UserType;
import com.carefarm.prakrati.web.bean.OrderBean;

@Component
public class OrderPlacementProcessor extends PrakratiProcessor {
	
	@Value("${vocac.sms.order.placed}")
	protected String placementMessage;

	public void process(OrderBean order) {
		Object[] params = new Object[] { order.getCreatedBy().getFullName() };
		NotificationDTO notification = NotificationDTO.Builder.getInstance()
				.category(NotificationCategory.ORDER_PLACED)
				.notification(context.getMessage(Constants.ORDER_PLACED, params, Locale.US))
				.groups(Arrays.asList(UserType.SUPER_ADMIN.getType()))
				.startDate(Date.from(Environment.clock().instant()))
				.expiryDate(Date.from(Environment.clock().instant().plus(5L, ChronoUnit.DAYS)))
				.build();
		notificationService.sendNotification(NOTIFICATIONS_TOPIC, notification);
		
		if (orderSmsEnabled) {
			String message = MessageFormat.format(placementMessage, order.getOrderNo(), order.getDeliveryDate());
			mobileMessanger.sendMessage(order.getCreatedBy().getMobileNo(), message);
        }
        
		if (orderEmailEnabled) {
			orderEmailHandler.sendOrderHasPlaced(order, order.getCreatedBy());
		}
	}
}
