package com.carefarm.prakrati.processors;

import java.util.Locale;

import com.carefarm.prakrati.common.util.Constants;
import com.carefarm.prakrati.util.NotificationCategory;
import com.carefarm.prakrati.web.bean.DemandBean;

public class DemandAssignedProcessor extends PrakratiProcessor {

	public void process(DemandBean demand) {
		Object[] params = new Object[] { demand.getAssignedTo().getFullName() };
		String notification = context.getMessage(Constants.DEMAND_ASSIGNED, params, Locale.US);
		this.processInternal(NotificationCategory.DEMAND_ASSIGNED, notification, demand.getAssignedTo().getUserNo());
	}
}
