package com.carefarm.prakrati.processors;

import java.text.MessageFormat;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.common.util.Constants;
import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.util.NotificationCategory;
import com.carefarm.prakrati.web.bean.OrderBean;

@Component
public class OrderDeliveredProcessor extends PrakratiProcessor {

	@Value("${vocac.sms.order.delivered}")
	protected String deliveredMessage;
	
	public void process(OrderBean order) {
		Object[] params = new Object[] { order.getCreatedBy().getFullName() };
		String notification = context.getMessage(Constants.ORDER_DELIVERED, params, Locale.US);
		this.processInternal(NotificationCategory.ORDER_DELIVERED, notification, order.getCreatedBy().getUserNo());
		
		if (orderSmsEnabled && Predicates.notNull.test(order.getProducts())) {
			String message = MessageFormat.format(deliveredMessage, order.getDeliveryDate());
			mobileMessanger.sendMessage(order.getCreatedBy().getMobileNo(), message);
        }
        
		if (orderEmailEnabled) {
			orderEmailHandler.sendOrderHasDelivered(order, order.getCreatedBy());
		}
	}
}
