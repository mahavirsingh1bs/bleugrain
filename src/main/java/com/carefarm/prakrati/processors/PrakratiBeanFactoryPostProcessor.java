package com.carefarm.prakrati.processors;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

import com.carefarm.prakrati.entity.Product;
import com.carefarm.prakrati.events.ProductAddEvent;
import com.carefarm.prakrati.events.handler.ProductAddEventHandler;
import com.carefarm.prakrati.events.router.DynamicRouter;

public class PrakratiBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory configurableListableBeanFactory) throws BeansException {
		ProductAddEventHandler productAddEventHandler = configurableListableBeanFactory.getBean(ProductAddEventHandler.class);
		DynamicRouter<ProductAddEvent, Product> productAddEventRouter = new DynamicRouter<>();
		productAddEventRouter.registerHandler(ProductAddEvent.class, productAddEventHandler);
		configurableListableBeanFactory.registerSingleton("productAddEventRouter", productAddEventRouter);
	}

}
