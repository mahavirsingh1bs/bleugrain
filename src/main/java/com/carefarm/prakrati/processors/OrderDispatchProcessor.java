package com.carefarm.prakrati.processors;

import java.text.MessageFormat;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.common.util.Constants;
import com.carefarm.prakrati.util.NotificationCategory;
import com.carefarm.prakrati.web.bean.OrderBean;

@Component
public class OrderDispatchProcessor extends PrakratiProcessor {

	@Value("${vocac.sms.order.dispatched}")
	protected String dispatchedMessage;
	
	@Value("${vocac.sms.order.assigned}")
	protected String assignedMessage;
	
	public void process(OrderBean order) {
		Object[] params1 = new Object[] { order.getCreatedBy().getFullName() };
		String buyerNotification = context.getMessage(Constants.ORDER_DISPATCHED, params1, Locale.US);
		this.processInternal(NotificationCategory.ORDER_DISPATCHED, buyerNotification, order.getCreatedBy().getUserNo());

		Object[] params2 = new Object[] { order.getDeliverBy().getFullName() };
		String dispatcherNotification = context.getMessage(Constants.ORDER_ASSIGNED, params2, Locale.US);
		this.processInternal(NotificationCategory.ORDER_ASSIGNED, dispatcherNotification, order.getDeliverBy().getUserNo());
		
		if (orderSmsEnabled) {
			String deliverBy = order.getDeliverBy().getFullName();
			String message = MessageFormat.format(dispatchedMessage, order.getOrderNo(), deliverBy, order.getDeliverBy().getMobileNo());
			mobileMessanger.sendMessage(order.getCreatedBy().getMobileNo(), message);
			
			String assignMessage = MessageFormat.format(assignedMessage, order.getOrderNo(), order.getShippingAddress().getCity(), order.getShippingAddress().getState().getName(), order.getShippingAddress().getPhoneNo());
			mobileMessanger.sendMessage(order.getCreatedBy().getMobileNo(), assignMessage);
        }
        
		if (orderEmailEnabled) {
			orderEmailHandler.sendOrderHasAssigned(order, order.getDeliverBy());
			orderEmailHandler.sendOrderHasDispatched(order, order.getCreatedBy());
		}
	}
}
