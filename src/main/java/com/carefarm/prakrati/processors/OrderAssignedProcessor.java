package com.carefarm.prakrati.processors;

import java.util.Locale;

import com.carefarm.prakrati.common.util.Constants;
import com.carefarm.prakrati.util.NotificationCategory;
import com.carefarm.prakrati.web.bean.OrderBean;

public class OrderAssignedProcessor extends PrakratiProcessor {
	
	public void process(OrderBean order) {
		Object[] params = new Object[] { order.getAssignedTo().getFullName() };
		String notification = context.getMessage(Constants.ORDER_ASSIGNED, params, Locale.US);
		this.processInternal(NotificationCategory.ORDER_ASSIGNED, notification, order.getAssignedTo().getUserNo());
		
		if (orderEmailEnabled) {
			orderEmailHandler.sendOrderHasAssigned(order, order.getAssignedTo());
		}
	}
	
}
