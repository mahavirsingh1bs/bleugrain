package com.carefarm.prakrati.processors;

import java.util.Locale;

import com.carefarm.prakrati.common.util.Constants;
import com.carefarm.prakrati.util.NotificationCategory;
import com.carefarm.prakrati.web.bean.ProductBean;

public class ProductSoldOutProcessor extends PrakratiProcessor {

	public void process(ProductBean product) {
		Object[] params = new Object[] { product.getOwner().getFullName(), product.getName() };
		String notification = context.getMessage(Constants.PRODUCT_SOLDOUT, params, Locale.US);
		this.processInternal(NotificationCategory.PRODUCT_SOLDOUT, notification, product.getOwner().getUserNo());
	}
}
