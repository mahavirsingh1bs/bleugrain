package com.carefarm.prakrati.processors;

import java.util.Locale;

import com.carefarm.prakrati.common.util.Constants;
import com.carefarm.prakrati.util.NotificationCategory;
import com.carefarm.prakrati.web.bean.DemandBean;

public class DemandFulfilledProcessor extends PrakratiProcessor {

	public void process(DemandBean demand) {
		Object[] params = new Object[] { demand.getCreatedBy().getFullName(), demand.getName() };
		String notification = context.getMessage(Constants.DEMAND_FULFILLED, params, Locale.US);
		this.processInternal(NotificationCategory.DEMAND_FULFILLED, notification, demand.getCreatedBy().getUserNo());
	}
	
}
