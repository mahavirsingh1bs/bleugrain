package com.carefarm.prakrati.processors;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.common.util.Constants;
import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.util.NotificationCategory;
import com.carefarm.prakrati.web.bean.OrderBean;

@Component
public class OrderShipmentProcessor extends PrakratiProcessor {
	
	@Value("${vocac.sms.order.shipped}")
	protected String shipmentMessage;
	
	public void process(OrderBean order) {
		Object[] params = new Object[] { order.getCreatedBy().getFullName() };
		String notification = context.getMessage(Constants.ORDER_SHIPPED, params, Locale.US);
		List<String> users = Arrays.asList(order.getCreatedBy().getUserNo());
	
		this.processInternal(NotificationCategory.ORDER_SHIPPED, notification, users);
		
		if (orderSmsEnabled && Predicates.notNull.test(order.getProducts())) {
			String message = MessageFormat.format(shipmentMessage, order.getOrderNo(), order.getDeliveryDate());
			mobileMessanger.sendMessage(order.getCreatedBy().getMobileNo(), message);
        }
        
		if (orderEmailEnabled) {
			orderEmailHandler.sendOrderHasShipped(order, order.getCreatedBy());
		}
	}
}
