package com.carefarm.prakrati.processors;

import static com.carefarm.prakrati.common.util.Constants.PRODUCT;
import static com.carefarm.prakrati.common.util.Constants.PRODUCTS_TOPIC;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.events.SocketEvent;
import com.carefarm.prakrati.web.bean.ProductBean;

@Component
public class ProductProcessor {

	@Autowired
	private SimpMessagingTemplate template;
	
	public void process(ProductBean product) {
		template.convertAndSend(PRODUCTS_TOPIC, SocketEvent.createEvent(PRODUCT, product));
	}
}
