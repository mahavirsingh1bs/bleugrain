package com.carefarm.prakrati.holder;

import javax.servlet.ServletContext;

public class ServletContextHolder {
	
	private static ServletContext servletContext;
	
	public static void holdServletContext(ServletContext servletContext) {
		ServletContextHolder.servletContext = servletContext;
	}
	
	public static ServletContext getServletContext() {
		return ServletContextHolder.servletContext;
	}
	 
	public static void unholdServletContext() {
		ServletContextHolder.servletContext = null;
	}
}
