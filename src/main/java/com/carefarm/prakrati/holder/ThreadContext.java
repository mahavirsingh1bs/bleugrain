package com.carefarm.prakrati.holder;

import com.carefarm.prakrati.entity.User;

public class ThreadContext {
	private static ThreadLocal<User> localUser = new ThreadLocal<>();
	
	public static User getCurrentUser() {
		return localUser.get();
	}
	
	public static void setCurrentUser(User user) {
		localUser.set(user);
	}
}
