package com.carefarm.prakrati.filters;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.authentication.AuthenticationServiceException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.google.gson.Gson;

public class DomainUsernamePasswordJsonAuthenticationFilter extends
		UsernamePasswordAuthenticationFilter {

	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {
		if (!request.getMethod().equals("POST")) {
            throw new AuthenticationServiceException("Authentication method not supported: " + request.getMethod());
        }
		
		if (!request.getContentType().contains("application/json")) {
			throw new AuthenticationServiceException("Authentication type not supported: " + request.getContentType());
		}
		
		LoginRequest loginRequest = this.getLoginRequest(request);

        DomainUsernamePasswordAuthenticationToken authRequest = new DomainUsernamePasswordAuthenticationToken(
        		loginRequest.getUsername(), loginRequest.getPassword(), loginRequest.getDomain());

        setDetails(request, authRequest);
        return this.getAuthenticationManager().authenticate(authRequest);
	}
	
	private LoginRequest getLoginRequest(HttpServletRequest request) {
		BufferedReader reader = null;
		LoginRequest loginRequest = null;
		
		try {
			reader = request.getReader();
			Gson gson = new Gson();
			loginRequest = gson.fromJson(reader, LoginRequest.class);
			
			if (loginRequest.username == null) {
				loginRequest.username = obtainUsername(request);
			}
			
			if (loginRequest.password == null) {
				loginRequest.password = obtainPassword(request);
			}
			
			if (loginRequest.domain == null) {
				loginRequest.domain = request.getParameter("domain");
			}
		} catch (IOException e) {
			Logger.getLogger(DomainUsernamePasswordJsonAuthenticationFilter.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			try {
				reader.close();
			} catch (IOException e) {
				Logger.getLogger(DomainUsernamePasswordJsonAuthenticationFilter.class.getName()).log(Level.SEVERE, null, e);
			}
		}
		
		if (loginRequest == null) {
			loginRequest = new LoginRequest();
		}
		
		return loginRequest;
	}
	
	private static class LoginRequest {
		private String username;
		private String password;
		private String domain;
		
		public String getUsername() {
			return username;
		}
		public String getPassword() {
			return password;
		}
		public String getDomain() {
			return domain;
		}
		
	}
}
