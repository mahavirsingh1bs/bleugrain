package com.carefarm.prakrati.index;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.index.builders.IndexBuilder;

@Component
public class ElasticsearchIndexer {
	@Autowired
	private List<IndexBuilder> builders;
	
	@Transactional
	public void createIndexes() {
		for (IndexBuilder builder: builders) {
			builder.createIndexes();
		}
	}
}
