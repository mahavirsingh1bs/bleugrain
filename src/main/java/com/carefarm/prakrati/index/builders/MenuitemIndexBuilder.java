package com.carefarm.prakrati.index.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.Menuitem;
import com.carefarm.prakrati.repository.MenuitemRepository;
import com.carefarm.prakrati.search.events.MenuitemEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;

@Component
public class MenuitemIndexBuilder extends AbstractIndexBuilder implements IndexBuilder {
	
	@Autowired
	private MenuitemRepository menuitemRepository;
	
	public void createIndexes() {
		Iterable<Menuitem> menuitems = menuitemRepository.findAll();
		eventPublisher.publishEvent(new MenuitemEvent(menuitems, EventType.INSERT));
	}
	
}
