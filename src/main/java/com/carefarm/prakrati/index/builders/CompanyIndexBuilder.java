package com.carefarm.prakrati.index.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.Company;
import com.carefarm.prakrati.repository.CompanyRepository;
import com.carefarm.prakrati.search.events.CompanyEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;

@Component
public class CompanyIndexBuilder extends AbstractIndexBuilder implements IndexBuilder {
	
	@Autowired
	private CompanyRepository companyRepository;
	
	public void createIndexes() {
		Iterable<Company> companies = companyRepository.findAll();
		eventPublisher.publishEvent(new CompanyEvent(companies, EventType.INSERT));
	}
	
}
