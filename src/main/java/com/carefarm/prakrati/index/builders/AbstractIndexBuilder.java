package com.carefarm.prakrati.index.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;

public abstract class AbstractIndexBuilder implements IndexBuilder {
	
	@Autowired
    protected ApplicationEventPublisher eventPublisher;
	
}
