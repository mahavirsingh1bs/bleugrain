package com.carefarm.prakrati.index.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.Demand;
import com.carefarm.prakrati.repository.DemandRepository;
import com.carefarm.prakrati.search.events.DemandEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;

@Component
public class DemandIndexBuilder extends AbstractIndexBuilder implements IndexBuilder {
	
	@Autowired
	private DemandRepository demandRepository;
	
	public void createIndexes() {
		Iterable<Demand> demands = demandRepository.findAll();
		eventPublisher.publishEvent(new DemandEvent(demands, EventType.INSERT));
	}
	
}
