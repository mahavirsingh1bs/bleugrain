package com.carefarm.prakrati.index.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.Category;
import com.carefarm.prakrati.repository.CategoryRepository;
import com.carefarm.prakrati.search.events.CategoryEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;

@Component
public class CategoryIndexBuilder extends AbstractIndexBuilder implements IndexBuilder {
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	public void createIndexes() {
		Iterable<Category> categories = categoryRepository.findAll();
		eventPublisher.publishEvent(new CategoryEvent(categories, EventType.INSERT));
	}
	
}
