package com.carefarm.prakrati.index.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.Bank;
import com.carefarm.prakrati.repository.BankRepository;
import com.carefarm.prakrati.search.events.BankEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;

@Component
public class BankIndexBuilder extends AbstractIndexBuilder implements IndexBuilder {
	
	@Autowired
	private BankRepository bankRepository;
	
	public void createIndexes() {
		Iterable<Bank> banks = bankRepository.findAll();
		eventPublisher.publishEvent(new BankEvent(banks, EventType.INSERT));
	}
	
}
