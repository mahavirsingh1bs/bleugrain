package com.carefarm.prakrati.index.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.PaymentMethod;
import com.carefarm.prakrati.repository.PaymentMethodRepository;
import com.carefarm.prakrati.search.events.PaymentMethodEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;

@Component
public class PaymentMethodIndexBuilder extends AbstractIndexBuilder implements IndexBuilder {
	
	@Autowired
	private PaymentMethodRepository paymentMethodRepository;
	
	public void createIndexes() {
		Iterable<PaymentMethod> paymentMethods = paymentMethodRepository.findAll();
		eventPublisher.publishEvent(new PaymentMethodEvent(paymentMethods, EventType.INSERT));
	}
	
}
