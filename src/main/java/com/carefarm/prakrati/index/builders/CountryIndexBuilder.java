package com.carefarm.prakrati.index.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.common.entity.Country;
import com.carefarm.prakrati.repository.CountryRepository;
import com.carefarm.prakrati.search.events.CountryEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;

@Component
public class CountryIndexBuilder extends AbstractIndexBuilder implements IndexBuilder {
	
	@Autowired
	private CountryRepository countryRepository;
	
	public void createIndexes() {
		Iterable<Country> countries = countryRepository.findAll();
		eventPublisher.publishEvent(new CountryEvent(countries, EventType.INSERT));
	}
	
}
