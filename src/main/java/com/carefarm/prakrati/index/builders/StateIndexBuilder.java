package com.carefarm.prakrati.index.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.common.entity.State;
import com.carefarm.prakrati.repository.StateRepository;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.search.events.StateEvent;

@Component
public class StateIndexBuilder extends AbstractIndexBuilder implements IndexBuilder {
	
	@Autowired
	private StateRepository stateRepository;
	
	public void createIndexes() {
		Iterable<State> states = stateRepository.findAll();
		eventPublisher.publishEvent(new StateEvent(states, EventType.INSERT));
	}
	
}
