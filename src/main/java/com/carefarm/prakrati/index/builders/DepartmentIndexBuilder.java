package com.carefarm.prakrati.index.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.Department;
import com.carefarm.prakrati.repository.DepartmentRepository;
import com.carefarm.prakrati.search.events.DepartmentEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;

@Component
public class DepartmentIndexBuilder extends AbstractIndexBuilder implements IndexBuilder {
	
	@Autowired
	private DepartmentRepository departmentRepository;
	
	public void createIndexes() {
		Iterable<Department> departments = departmentRepository.findAll();
		eventPublisher.publishEvent(new DepartmentEvent(departments, EventType.INSERT));
	}
	
}
