package com.carefarm.prakrati.index.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.PaymentCard;
import com.carefarm.prakrati.repository.PaymentCardRepository;
import com.carefarm.prakrati.search.events.PaymentCardEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;

@Component
public class PaymentCardIndexBuilder extends AbstractIndexBuilder implements IndexBuilder {
	
	@Autowired
	private PaymentCardRepository paymentCardRepository;
	
	public void createIndexes() {
		Iterable<PaymentCard> paymentCards = paymentCardRepository.findAll();
		eventPublisher.publishEvent(new PaymentCardEvent(paymentCards, EventType.INSERT));
	}
	
}
