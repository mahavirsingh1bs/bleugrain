package com.carefarm.prakrati.index.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.Product;
import com.carefarm.prakrati.repository.ProductRepository;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.search.events.ProductEvent;

@Component
public class ProductIndexBuilder extends AbstractIndexBuilder implements IndexBuilder {
	
	@Autowired
	private ProductRepository productRepository;
	
	public void createIndexes() {
		Iterable<Product> products = productRepository.findAll();
		eventPublisher.publishEvent(new ProductEvent(products, EventType.INSERT));
	}
	
}
