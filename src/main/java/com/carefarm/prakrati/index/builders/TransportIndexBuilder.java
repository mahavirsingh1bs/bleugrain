package com.carefarm.prakrati.index.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.delivery.entity.Transport;
import com.carefarm.prakrati.repository.TransportRepository;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.search.events.TransportEvent;

@Component
public class TransportIndexBuilder extends AbstractIndexBuilder implements IndexBuilder {
	
	@Autowired
	private TransportRepository transportRepository;
	
	public void createIndexes() {
		Iterable<Transport> transports = transportRepository.findAll();
		eventPublisher.publishEvent(new TransportEvent(transports, EventType.INSERT));
	}
	
}
