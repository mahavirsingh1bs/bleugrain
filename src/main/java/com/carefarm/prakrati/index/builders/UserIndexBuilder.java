package com.carefarm.prakrati.index.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.search.events.UserEvent;

@Component
public class UserIndexBuilder extends AbstractIndexBuilder implements IndexBuilder {
	
	@Autowired
	private UserRepository userRepository;
	
	public void createIndexes() {
		Iterable<User> users = userRepository.findAll();
		eventPublisher.publishEvent(new UserEvent(users, EventType.INSERT));
	}
	
}
