package com.carefarm.prakrati.index.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.Order;
import com.carefarm.prakrati.repository.OrderRepository;
import com.carefarm.prakrati.search.events.OrderEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;

@Component
public class OrderIndexBuilder extends AbstractIndexBuilder implements IndexBuilder {
	
	@Autowired
	private OrderRepository orderRepository;
	
	public void createIndexes() {
		Iterable<Order> orders = orderRepository.findAll();
		eventPublisher.publishEvent(new OrderEvent(orders, EventType.INSERT));
	}
	
}
