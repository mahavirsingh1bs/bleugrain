package com.carefarm.prakrati.index.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.Designation;
import com.carefarm.prakrati.repository.DesignationRepository;
import com.carefarm.prakrati.search.events.DesignationEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;

@Component
public class DesignationIndexBuilder extends AbstractIndexBuilder implements IndexBuilder {
	
	@Autowired
	private DesignationRepository designationRepository;
	
	public void createIndexes() {
		Iterable<Designation> designations = designationRepository.findAll();
		eventPublisher.publishEvent(new DesignationEvent(designations, EventType.INSERT));
	}
	
}
