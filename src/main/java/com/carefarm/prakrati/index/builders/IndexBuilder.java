package com.carefarm.prakrati.index.builders;

public interface IndexBuilder {
	
	void createIndexes();
	
}
