package com.carefarm.prakrati.index.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.Group;
import com.carefarm.prakrati.repository.GroupRepository;
import com.carefarm.prakrati.search.events.GroupEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;

@Component
public class GroupIndexBuilder extends AbstractIndexBuilder implements IndexBuilder {
	
	@Autowired
	private GroupRepository groupRepository;
	
	public void createIndexes() {
		Iterable<Group> groups = groupRepository.findAll();
		eventPublisher.publishEvent(new GroupEvent(groups, EventType.INSERT));
	}
	
}
