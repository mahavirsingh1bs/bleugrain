package com.carefarm.prakrati.index.builders;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.Role;
import com.carefarm.prakrati.repository.RoleRepository;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.search.events.RoleEvent;

@Component
public class RoleIndexBuilder extends AbstractIndexBuilder implements IndexBuilder {
	
	@Autowired
	private RoleRepository roleRepository;
	
	public void createIndexes() {
		Iterable<Role> roles = roleRepository.findAll();
		eventPublisher.publishEvent(new RoleEvent(roles, EventType.INSERT));
	}
	
}
