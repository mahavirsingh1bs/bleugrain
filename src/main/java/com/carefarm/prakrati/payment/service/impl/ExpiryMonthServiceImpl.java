package com.carefarm.prakrati.payment.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carefarm.prakrati.payment.entity.ExpiryMonth;
import com.carefarm.prakrati.payment.repository.ExpiryMonthRepository;
import com.carefarm.prakrati.payment.service.ExpiryMonthService;
import com.carefarm.prakrati.web.payment.bean.ExpiryMonthBean;

@Service
public class ExpiryMonthServiceImpl implements ExpiryMonthService {

	@Autowired
	private DozerBeanMapper mapper;
	
	@Autowired
	private ExpiryMonthRepository expiryMonthRepository;
	
	@Override
	public List<ExpiryMonthBean> findAll() {
		List<ExpiryMonthBean> expiryMonths = new ArrayList<>();
		for (ExpiryMonth expiryMonth : expiryMonthRepository.findAll()) {
			expiryMonths.add(mapper.map(expiryMonth, ExpiryMonthBean.class));
		}
		return expiryMonths;
	}

}
