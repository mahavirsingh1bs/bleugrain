package com.carefarm.prakrati.payment.service;

import java.util.List;

import com.carefarm.prakrati.web.payment.bean.ExpiryMonthBean;

public interface ExpiryMonthService {
	
	List<ExpiryMonthBean> findAll();
}
