package com.carefarm.prakrati.payment.service.impl;

import static com.carefarm.prakrati.web.util.PrakratiConstants.COMPANY_ID;
import static com.carefarm.prakrati.web.util.PrakratiConstants.USER_ID;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.entity.Company;
import com.carefarm.prakrati.entity.PaymentCard;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.payment.entity.CreditCard;
import com.carefarm.prakrati.payment.entity.ExpiryMonth;
import com.carefarm.prakrati.payment.entity.ExpiryYear;
import com.carefarm.prakrati.payment.repository.BillingDetailsRepository;
import com.carefarm.prakrati.payment.repository.CreditCardRepository;
import com.carefarm.prakrati.repository.CompanyRepository;
import com.carefarm.prakrati.repository.PaymentCardRepository;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.service.CreditCardService;
import com.carefarm.prakrati.service.helper.CreditCardHelper;
import com.carefarm.prakrati.service.impl.PrakratiServiceImpl;
import com.carefarm.prakrati.web.bean.CompanyBean;
import com.carefarm.prakrati.web.bean.CreditCardBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.model.CreditCardForm;
import com.carefarm.prakrati.web.payment.model.BillingDetailsForm;

@Service("creditCardService")
public class CreditCardServiceImpl extends PrakratiServiceImpl implements CreditCardService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CreditCardServiceImpl.class);
	
	@Autowired
	private DozerBeanMapper mapper;
	
	@Autowired
	private BillingDetailsRepository billingDetailsRepository;
	
	@Autowired
	private CompanyRepository companyRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PaymentCardRepository paymentCardRepository;
	
	@Autowired
	private CreditCardRepository creditCardRepository;
	
	@Autowired
	private CreditCardHelper creditCardHelper;

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(BillingDetailsForm form, User currentUser) throws ServiceException {
		CreditCard creditCard = mapper.map(form, CreditCard.class);
		creditCard.setUniqueId(UUID.randomUUID().toString());
		creditCard.setCreatedBy(currentUser);
		creditCard.setDateCreated(Date.from(Environment.clock().instant()));
		if (Predicates.notNull.test(form.getUserNo())) {
			creditCard.setUser(userRepository.findByUserNo(form.getUserNo()));
		} else if (Predicates.notNull.test(form.getCompanyId())) {
			creditCard.setCompany(companyRepository.findByUniqueId(form.getCompanyId()));
		}
		billingDetailsRepository.save(creditCard);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Map<String, Object> findByKeyword(SearchRequest request) {
		Map<String, Object> data = new HashMap<>();
		
		Pageable pageRequest = createPageRequest(request);
		
		Long totalRecord = 0L;
		List<CreditCard> creditCards = null;
		if (Predicates.notNull.test(request.getCompanyId())) {
			totalRecord = creditCardRepository.countActiveCardsByCompanyId(request.getCompanyId());
			creditCards = creditCardRepository.findActiveCardsByCompanyId(request.getCompanyId(), pageRequest);
		} else if (Predicates.notNull.test(request.getUserNo())) {
			totalRecord = creditCardRepository.countActiveCardsByUserNo(request.getUserNo());
			creditCards = creditCardRepository.findActiveCardsByUserNo(request.getUserNo(), pageRequest);
		}
    	
    	List<CreditCardBean> creditCardBeans = new ArrayList<>();
    	
    	creditCards.forEach(creditCard -> {
    		CreditCardBean creditCardBean = mapper.map(creditCard, CreditCardBean.class);
    		Optional.ofNullable(creditCard.getCompany())
    			.ifPresent(c -> creditCardBean.setCompany(mapper.map(c, CompanyBean.class)));
    		Optional.ofNullable(creditCard.getUser())
				.ifPresent(u -> creditCardBean.setUser(mapper.map(u, UserBean.class)));
    		creditCardBeans.add(creditCardBean);
    	});
    	
		data.put("recordsTotal", totalRecord);
		data.put("recordsFiltered", totalRecord);
		data.put("data", creditCardBeans);
		return data;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public CreditCardBean findDetail(String uniqueId) {
		CreditCard creditCard = creditCardRepository.findByUniqueId(uniqueId);
		CreditCardBean creditCardBean = mapper.map(creditCard, CreditCardBean.class);
		return creditCardBean;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(String uniqueId) {
		LOGGER.info("Deleting the credit card with the unique id: " + uniqueId);
		CreditCard creditCard = creditCardRepository.findByUniqueId(uniqueId); 
		creditCardRepository.delete(creditCard);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(CreditCardForm form, User createdBy) throws ServiceException {
		Calendar calendar = Calendar.getInstance();
		CreditCard creditCard = mapper.map(form, CreditCard.class);
		creditCard.setCreatedBy(createdBy);
		creditCard.setDateCreated(calendar.getTime());
		if (form.getUserId() == null) {
			creditCard.setUser(null);
		} else if (form.getCompanyId() == null) {
			creditCard.setCompany(null);
		}
		creditCardRepository.save(creditCard);
		
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(CreditCardBean creditCardBean, User modifiedBy) throws ServiceException {
		Calendar calendar = Calendar.getInstance();
		CreditCard creditCard = creditCardRepository.findByUniqueId(creditCardBean.getUniqueId());
		creditCard.setDefaultBilling(creditCardBean.isDefaultBilling());
		creditCard.setPaymentCard(mapper.map(creditCardBean.getPaymentCard(), PaymentCard.class));
		creditCard.setNumber(creditCardBean.getNumber());
		creditCard.setOwner(creditCardBean.getOwner());
		creditCard.setExpiryMonth(mapper.map(creditCardBean.getExpiryMonth(), ExpiryMonth.class));
		creditCard.setExpiryYear(mapper.map(creditCardBean.getExpiryYear(), ExpiryYear.class));
		creditCard.setModifiedBy(modifiedBy);
		creditCard.setDateModified(calendar.getTime());
	}

	@Override
	public CreditCardBean create(CreditCardForm form,
			Map<String, Object> params, User createdBy) throws ServiceException {
		CreditCard creditCard = mapper.map(form, CreditCard.class);
		
		if (Predicates.notNull.test(params.get(COMPANY_ID))) {
			Long companyId = (Long ) params.get(COMPANY_ID);
			Company company = companyRepository.findOne(companyId);
			
			if (form.isDefaultBilling()) {
				company.setDefaultBillingDetails(creditCard);
			}
			
			creditCard.setCompany(company);
			creditCard.setUser(null);
		} else {
			Long userId = (Long ) params.get(USER_ID);
			User user = userRepository.findOne(userId);
			
			if (form.isDefaultBilling()) {
				user.setDefaultBillingDetails(creditCard);
			}
			
			creditCard.setUser(user);
			creditCard.setCompany(null);
		}
		
		creditCard.setCreatedBy(createdBy);
		creditCard.setDateCreated(Calendar.getInstance().getTime());
		
		PaymentCard paymentCard = paymentCardRepository.findOne(form.getPaymentCardId());
		creditCard.setPaymentCard(paymentCard);
		
		creditCardRepository.save(creditCard);
		creditCard = creditCardRepository.findOne(creditCard.getId());
		
		CreditCardBean creditCardBean = mapper.map(creditCard, CreditCardBean.class);
		return creditCardBean;
	}

	
}
