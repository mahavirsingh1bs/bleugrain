package com.carefarm.prakrati.payment.service;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.web.payment.model.BillingDetailsForm;

public interface BillingDetailsService {

	void create(BillingDetailsForm form, User currentUser) throws ServiceException;
}
