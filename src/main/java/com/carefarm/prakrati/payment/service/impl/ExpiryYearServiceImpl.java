package com.carefarm.prakrati.payment.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carefarm.prakrati.payment.entity.ExpiryYear;
import com.carefarm.prakrati.payment.repository.ExpiryYearRepository;
import com.carefarm.prakrati.payment.service.ExpiryYearService;
import com.carefarm.prakrati.web.payment.bean.ExpiryYearBean;

@Service
public class ExpiryYearServiceImpl implements ExpiryYearService {

	@Autowired
	private DozerBeanMapper mapper;
	
	@Autowired
	private ExpiryYearRepository expiryYearRepository;
	
	@Override
	public List<ExpiryYearBean> findAll() {
		List<ExpiryYearBean> expiryYears = new ArrayList<>();
		for (ExpiryYear expiryYear : expiryYearRepository.findAll()) {
			expiryYears.add(mapper.map(expiryYear, ExpiryYearBean.class));
		}
		return expiryYears;
	}

}
