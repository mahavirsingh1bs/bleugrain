package com.carefarm.prakrati.payment.service;

import java.util.List;

import com.carefarm.prakrati.web.payment.bean.ExpiryYearBean;

public interface ExpiryYearService {
	List<ExpiryYearBean> findAll();
}
