package com.carefarm.prakrati.payment.service.impl;

import static com.carefarm.prakrati.web.util.PrakratiConstants.COMPANY_ID;
import static com.carefarm.prakrati.web.util.PrakratiConstants.USER_ID;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.entity.Bank;
import com.carefarm.prakrati.entity.Company;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.payment.entity.BankAccount;
import com.carefarm.prakrati.payment.repository.BankAccountRepository;
import com.carefarm.prakrati.payment.repository.BillingDetailsRepository;
import com.carefarm.prakrati.repository.BankRepository;
import com.carefarm.prakrati.repository.CompanyRepository;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.service.BankAccountService;
import com.carefarm.prakrati.service.helper.BankAccountHelper;
import com.carefarm.prakrati.service.impl.PrakratiServiceImpl;
import com.carefarm.prakrati.web.bean.BankAccountBean;
import com.carefarm.prakrati.web.bean.BankBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.BankAccountForm;
import com.carefarm.prakrati.web.payment.model.BillingDetailsForm;

@Service("bankAccountService")
public class BankAccountServiceImpl extends PrakratiServiceImpl implements BankAccountService {

	private static final Logger LOGGER = LoggerFactory.getLogger(BankAccountServiceImpl.class);
	
	@Autowired
	private DozerBeanMapper mapper;
	
	@Autowired
	private BillingDetailsRepository billingDetailsRepository;
	
	@Autowired
	private CompanyRepository companyRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BankAccountRepository bankAccountRepository;
	
	@Autowired
	private BankRepository bankRepository;
	
	@Autowired
	private BankAccountHelper bankAccountHelper;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(BillingDetailsForm form, User currentUser) throws ServiceException {
		BankAccount bankAccount = mapper.map(form, BankAccount.class);
		bankAccount.setUniqueId(UUID.randomUUID().toString());
		bankAccount.setCreatedBy(currentUser);
		bankAccount.setDateCreated(Date.from(Environment.clock().instant()));
		if (Predicates.notNull.test(form.getUserNo())) {
			bankAccount.setUser(userRepository.findByUserNo(form.getUserNo()));
		} else if (Predicates.notNull.test(form.getCompanyId())) {
			bankAccount.setCompany(companyRepository.findByUniqueId(form.getCompanyId()));
		}
		billingDetailsRepository.save(bankAccount);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Map<String, Object> findByKeyword(SearchRequest request) {
		Map<String, Object> data = new HashMap<>();
		
		PageRequest pageRequest = createPageRequest(request);
		
		Long totalRecord = 0L;
		List<BankAccount> bankAccounts = null;
		if (Predicates.notNull.test(request.getCompanyId())) {
			totalRecord = bankAccountRepository.countActiveAccountsByCompanyId(request.getCompanyId());
			bankAccounts = bankAccountRepository.findActiveAccountsByCompanyId(request.getCompanyId(), pageRequest);
		} else if (Predicates.notNull.test(request.getUserNo())) {
			totalRecord = bankAccountRepository.countActiveAccountsByUserNo(request.getUserNo());
			bankAccounts = bankAccountRepository.findActiveAccountsByUserNo(request.getUserNo(), pageRequest);
		}
    	
    	List<BankAccountBean> bankAccountsBean = bankAccountHelper.convertEntitiesToBeans(bankAccounts, BankAccountBean.class);
		data.put("recordsTotal", totalRecord);
		data.put("recordsFiltered", totalRecord);
		data.put("data", bankAccountsBean);
		return data;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<BankAccountBean> findAllActiveOnes(Map<String, Object> params) {
		List<BankAccount> bankAccounts = null;
		if (Predicates.notNull.test(params.get(COMPANY_ID))) {
			Long companyId = (Long ) params.get(COMPANY_ID);
			bankAccounts = bankAccountRepository.findActiveAccountsByCompanyId(companyId);
		} else if (Predicates.notNull.test(params.get(USER_ID))) {
			Long userId = (Long ) params.get(USER_ID);
			bankAccounts = bankAccountRepository.findActiveAccountsByUserId(userId);
		}
    	
    	List<BankAccountBean> bankAccountsBean = bankAccountHelper.convertEntitiesToBeans(bankAccounts, BankAccountBean.class);
		return bankAccountsBean;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public BankAccountBean findDetail(String uniqueId) {
		BankAccount bankAccont = bankAccountRepository.findByUniqueId(uniqueId);
		BankAccountBean bankAccountBean = mapper.map(bankAccont, BankAccountBean.class);
		return bankAccountBean;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(BankAccountForm form, User createdBy) throws ServiceException {
		BankAccount bankAccount = mapper.map(form, BankAccount.class);
		bankAccount.setUniqueId(UUID.randomUUID().toString());
		bankAccount.setCreatedBy(createdBy);
		bankAccount.setDateCreated(Date.from(Environment.clock().instant()));
		if (Predicates.notNull.test(form.getUserId())) {
			bankAccount.setUser(userRepository.findByUserNo(form.getUserId()));
		} else if (Predicates.notNull.test(form.getCompanyId())) {
			bankAccount.setCompany(companyRepository.findByUniqueId(form.getCompanyId()));
		}
		bankAccountRepository.save(bankAccount);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(BankAccountBean bankAccountBean, User currentUser) throws ServiceException {
		Calendar calendar = Calendar.getInstance();
		BankAccount bankAccount = bankAccountRepository.findByUniqueId(bankAccountBean.getUniqueId());
		bankAccount.setDefaultBilling(bankAccountBean.isDefaultBilling());
		bankAccount.setOwner(bankAccountBean.getOwner());
		bankAccount.setBank(mapper.map(bankAccountBean.getBank(), Bank.class));
		bankAccount.setSwift(bankAccountBean.getSwift());
		bankAccount.setModifiedBy(currentUser);
		bankAccount.setDateModified(calendar.getTime());
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(String uniqueId) {
		LOGGER.info("Deleting the bank account with the unique id: " + uniqueId);
		BankAccount bankAccount = bankAccountRepository.findByUniqueId(uniqueId);
		bankAccountRepository.delete(bankAccount);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public BankAccountBean create(BankAccountForm form, Map<String, Object> params,
			User createdBy) throws ServiceException {
		BankAccount bankAccount = mapper.map(form, BankAccount.class);
		
		if (Predicates.notNull.test(params.get(COMPANY_ID))) {
			Long companyId = (Long ) params.get(COMPANY_ID);
			Company company = companyRepository.findOne(companyId);
			
			if (form.isDefaultBilling()) {
				company.setDefaultBillingDetails(bankAccount);
			}
			
			bankAccount.setCompany(company);
			bankAccount.setUser(null);
		} else {
			Long userId = (Long ) params.get(USER_ID);
			User user = userRepository.findOne(userId);
			
			if (form.isDefaultBilling()) {
				user.setDefaultBillingDetails(bankAccount);
			}
			
			bankAccount.setUser(user);
			bankAccount.setCompany(null);
		}
		
		bankAccount.setCreatedBy(createdBy);
		bankAccount.setDateCreated(Calendar.getInstance().getTime());
		
		bankAccountRepository.save(bankAccount);
		bankAccount = bankAccountRepository.findOne(bankAccount.getId());
		
		BankAccountBean bankAccountBean = mapper.map(bankAccount, BankAccountBean.class);
		bankAccountBean.setBank(mapper.map(bankRepository.findOne(form.getBankId()), BankBean.class));
		return bankAccountBean;
	}

}
