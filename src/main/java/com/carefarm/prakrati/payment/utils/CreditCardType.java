package com.carefarm.prakrati.payment.utils;

public enum CreditCardType {
	MASTERCARD("MasterCard"),
    VISA("Visa"),
    MAESTRO("Maestro"),
    AMEX("American Express");

    private final String name;

	private CreditCardType(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public String toString() {
	    return name;
	}
	
}
