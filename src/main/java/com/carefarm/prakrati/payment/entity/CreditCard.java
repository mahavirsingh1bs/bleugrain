package com.carefarm.prakrati.payment.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SecondaryTable;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.entity.PaymentCard;

@Entity
@DiscriminatorValue("CC")
@SecondaryTable(
		name = "CF_CREDIT_CARD",
		pkJoinColumns = @PrimaryKeyJoinColumn(name = "CREDIT_CARD_ID")
)
@Getter
@Setter
public class CreditCard extends BillingDetails {

	private static final long serialVersionUID = 315205043276536389L;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(table = "CF_CREDIT_CARD", name = "CC_PAYMENT_CARD_ID")	
	private PaymentCard paymentCard;
	
	@Column(table = "CF_CREDIT_CARD", name = "CC_NUMBER", nullable = false, updatable = false, length = 96)
	private String number;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(table = "CF_CREDIT_CARD", name = "CC_EXP_MONTH_ID")	
	private ExpiryMonth expiryMonth;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(table = "CF_CREDIT_CARD", name = "CC_EXP_YEAR_ID")
	private ExpiryYear expiryYear;
		
}
