package com.carefarm.prakrati.payment.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "CF_EXPIRY_MONTH")
@Getter
@Setter
public class ExpiryMonth {
	
	@Id
	@GenericGenerator(name = "expiry_month_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_EXPIRY_MONTH"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "expiry_month_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;

	@Version
    @Column(name = "OBJ_VERSION")
	@Setter(AccessLevel.NONE)
    private int version = 0;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "MONTH")
	private String month;

	@Column(name = "IS_ARCHIVED")
	private boolean archived;
		
}
