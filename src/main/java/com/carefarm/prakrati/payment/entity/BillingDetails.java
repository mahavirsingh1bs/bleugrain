package com.carefarm.prakrati.payment.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

import com.carefarm.prakrati.entity.AbstractEntity;
import com.carefarm.prakrati.entity.Company;
import com.carefarm.prakrati.entity.User;

@Entity
@Table(name = "CF_BILLING_DETAILS")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
		name = "BILLING_DETAILS_TYPE",
		discriminatorType = DiscriminatorType.STRING
)
@Getter
@Setter
public abstract class BillingDetails extends AbstractEntity implements Serializable {

	private static final long serialVersionUID = 1125927233304390600L;
	
	@Id
	@GenericGenerator(name = "billing_details_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_BILLING_DETAILS"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "billing_details_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;
		
	@Column(name = "UNIQUE_ID", unique = true, nullable = false)
	private String uniqueId;
	
	@Column(name = "OWNER")
	private String owner;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID")
	private User user;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY_ID")
	private Company company;

	@Column(name = "IS_ARCHIVED")
	private boolean archived;
	
	@Column(name = "IS_DEFAULT_BILLING")
	private boolean defaultBilling;
	
	public BillingDetails() { }
	
	public BillingDetails(String owner, User user) { 
		this.owner = owner;
		this.user = user;
	}
	
}
