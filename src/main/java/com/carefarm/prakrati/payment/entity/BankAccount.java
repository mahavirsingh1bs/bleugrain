package com.carefarm.prakrati.payment.entity;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.entity.Bank;
import com.carefarm.prakrati.entity.User;

@Entity
@DiscriminatorValue("BA")
@Getter
@Setter
public class BankAccount extends BillingDetails {

	private static final long serialVersionUID = -4242143708248804337L;
	
	@Column(name = "BA_ACCOUNT", nullable = true, length = 96)
	private String account;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "BA_BANK_ID")
	private Bank bank;
	
	@Column(name = "BA_SWIFT", nullable = true, length = 48)
	private String swift;
	
	public BankAccount() { }
	
	public BankAccount(String owner, User user, String account, Bank bank, String swift) {
		super(owner, user);
		this.account = account;
		this.bank = bank;
		this.swift = swift;
	}

}
