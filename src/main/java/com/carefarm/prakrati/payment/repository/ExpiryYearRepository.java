package com.carefarm.prakrati.payment.repository;

import org.springframework.stereotype.Repository;

import com.carefarm.prakrati.payment.entity.ExpiryYear;
import com.carefarm.prakrati.repository.PrakratiRepository;

@Repository
public interface ExpiryYearRepository extends PrakratiRepository<ExpiryYear, Long> {

}
