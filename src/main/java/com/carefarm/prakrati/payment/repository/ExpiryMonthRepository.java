package com.carefarm.prakrati.payment.repository;

import org.springframework.stereotype.Repository;

import com.carefarm.prakrati.payment.entity.ExpiryMonth;
import com.carefarm.prakrati.repository.PrakratiRepository;

@Repository
public interface ExpiryMonthRepository extends PrakratiRepository<ExpiryMonth, Long> {

}
