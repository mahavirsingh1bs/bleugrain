package com.carefarm.prakrati.payment.repository;

import com.carefarm.prakrati.payment.entity.BillingDetails;
import com.carefarm.prakrati.repository.PrakratiRepository;

public interface BillingDetailsRepository extends PrakratiRepository<BillingDetails, Long> {

}
