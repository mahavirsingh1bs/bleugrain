package com.carefarm.prakrati.payment.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.carefarm.prakrati.payment.entity.BankAccount;
import com.carefarm.prakrati.repository.PrakratiRepository;

public interface BankAccountRepository extends PrakratiRepository<BankAccount, Long> {

	BankAccount findByUniqueId(String uniqueId);
	
	@Query("SELECT ba FROM BankAccount ba WHERE ba.user.id = :userId")
	List<BankAccount> findByUserId(@Param("userId") Long userId, Pageable pageable);

	@Query("SELECT COUNT(ba) FROM BankAccount ba WHERE ba.archived = false AND ba.user.userNo = :userNo")
	Long countActiveAccountsByUserNo(@Param("userNo") String userNo);
	
	@Query("SELECT ba FROM BankAccount ba WHERE ba.archived = false AND ba.user.userNo = :userNo")
	List<BankAccount> findActiveAccountsByUserNo(@Param("userNo") String userNo, Pageable pageable);
	
	@Query("SELECT ba FROM BankAccount ba WHERE ba.archived = false AND ba.user.id = :userId")
	List<BankAccount> findActiveAccountsByUserId(@Param("userId") Long userId);
	
	@Query("SELECT ba FROM BankAccount ba WHERE ba.company.id = :companyId")
	List<BankAccount> findByCompanyId(@Param("companyId") Long companyId, Pageable pageable);
	
	@Query("SELECT COUNT(ba) FROM BankAccount ba WHERE ba.archived = false AND ba.company.uniqueId = :companyId")
	Long countActiveAccountsByCompanyId(@Param("companyId") String companyId);
	
	@Query("SELECT ba FROM BankAccount ba WHERE ba.archived = false AND ba.company.uniqueId = :companyId")
	List<BankAccount> findActiveAccountsByCompanyId(@Param("companyId") String companyId, Pageable pageable);
	
	@Query("SELECT ba FROM BankAccount ba WHERE ba.archived = false AND ba.company.id = :companyId")
	List<BankAccount> findActiveAccountsByCompanyId(@Param("companyId") Long companyId);
}
