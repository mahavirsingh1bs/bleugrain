package com.carefarm.prakrati.payment.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.carefarm.prakrati.payment.entity.CreditCard;
import com.carefarm.prakrati.repository.PrakratiRepository;

public interface CreditCardRepository extends PrakratiRepository<CreditCard, Long> {

	CreditCard findByUniqueId(String uniqueId);
	
	@Query("SELECT COUNT(cc) FROM CreditCard cc WHERE cc.user.id = :userId")
	Long countByUserId(@Param("userId") Long userId);
	
	@Query("SELECT cc FROM CreditCard cc WHERE cc.user.id = :userId")
	List<CreditCard> findByUserId(@Param("userId") Long userId, Pageable pageable);

	@Query("SELECT COUNT(cc) FROM CreditCard cc WHERE cc.company.id = :companyId")
	Long countByCompanyId(@Param("companyId") Long companyId);
	
	@Query("SELECT cc FROM CreditCard cc WHERE cc.company.id = :companyId")
	List<CreditCard> findByCompanyId(@Param("companyId") Long companyId, Pageable pageable);
	
	@Query("SELECT COUNT(cc) FROM CreditCard cc WHERE cc.archived = false AND cc.user.userNo = :userNo")
	Long countActiveCardsByUserNo(@Param("userNo") String userNo);
	
	@Query("SELECT cc FROM CreditCard cc WHERE cc.archived = false AND cc.user.userNo = :userNo")
	List<CreditCard> findActiveCardsByUserNo(@Param("userNo") String userNo, Pageable pageable);

	@Query("SELECT COUNT(cc) FROM CreditCard cc WHERE cc.archived = false AND cc.company.uniqueId = :companyId")
	Long countActiveCardsByCompanyId(@Param("companyId") String companyId);
	
	@Query("SELECT cc FROM CreditCard cc WHERE cc.archived = false AND cc.company.uniqueId = :companyId")
	List<CreditCard> findActiveCardsByCompanyId(@Param("companyId") String companyId, Pageable pageable);

	@Query("SELECT cc FROM CreditCard cc WHERE cc.archived = false AND cc.company.uniqueId = :companyId")
	List<CreditCard> findActiveCardsByCompanyId(@Param("companyId") String companyId);
	
	@Query("SELECT cc FROM CreditCard cc WHERE cc.archived = false AND cc.user.userNo = :userNo")
	List<CreditCard> findActiveCardsByUserNo(@Param("userNo") String userNo);
	
}
