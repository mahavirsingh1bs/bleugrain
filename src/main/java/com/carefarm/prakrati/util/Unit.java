package com.carefarm.prakrati.util;

public enum Unit {
	QUINTAL("quintal", "q"), KILOGRAM("kilogram", "Kg.");
	
	private final String unit;
	private final String symbol;
	
	private Unit(String unit, String symbol) {
		this.unit = unit;
		this.symbol = symbol;
	}
	
	@Override
	public String toString() {
		return unit;
	}
	
	public String getSymbol() {
		return symbol;
	}
}
