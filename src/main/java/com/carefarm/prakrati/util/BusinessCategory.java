package com.carefarm.prakrati.util;

import org.apache.commons.lang3.StringUtils;

public enum BusinessCategory {
	FOOD_PROCESSOR("Food Processor"), EXPORTER("Exporter"), TRANSPORTER("Transporter");
	
	private String name;
	
	private BusinessCategory(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public static BusinessCategory getCategory(String name) {
		for (BusinessCategory category : BusinessCategory.values()) {
			if (StringUtils.equalsIgnoreCase(category.name(), name)) {
				return category;
			}
		}
		return null;
	}
}
