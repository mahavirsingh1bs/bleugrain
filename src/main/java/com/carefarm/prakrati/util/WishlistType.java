package com.carefarm.prakrati.util;

public enum WishlistType {
	BUYING_WISHLIST, SELLING_WISHLIST;
}
