package com.carefarm.prakrati.util;

public enum QuantityType {
	PER_DAY, PER_YEAR
}
