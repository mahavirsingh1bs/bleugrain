package com.carefarm.prakrati.util;

public enum ImageType {
	PRODUCTS(21L, "products"), DEMANDS(1001L, "demands"),  
	USERS(1L, "users"), COMPANIES(1L, "companies");
	
	private long defaultImageId;
	private String type;
	
	private ImageType(long defaultImageId, String type) {
		this.defaultImageId = defaultImageId;
		this.type = type;
	}
	
	public long getDefaultImageId() {
		return defaultImageId;
	}

	public String getType() {
		return type;
	}
	
	@Override
	public String toString() {
		return type;
	}
}
