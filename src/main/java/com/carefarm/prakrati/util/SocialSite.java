package com.carefarm.prakrati.util;

public enum SocialSite {
	TWITTER, FACEBOOK, LINKEDIN, INSTAGRAM;
}
