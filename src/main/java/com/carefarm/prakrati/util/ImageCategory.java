package com.carefarm.prakrati.util;

import org.apache.commons.lang3.StringUtils;

public enum ImageCategory {
	ADMIN, EMPLOYEE, CUSTOMER, FARMER, BROKER, RETAILER, 
	COMPANY_ADMIN, AGENT, COMPANY, PRODUCT, DEMAND, USER;
	
	public static ImageCategory getCategory(UserType userType) {
		for (ImageCategory category: ImageCategory.values()) {
			if (StringUtils.equalsIgnoreCase(category.name(), userType.name())) {
				return category;
			}
		}
		return null;
	}
}
