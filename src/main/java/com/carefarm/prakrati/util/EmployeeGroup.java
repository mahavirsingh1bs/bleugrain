package com.carefarm.prakrati.util;

public enum EmployeeGroup {
	EXECUTIVE, CT_MEMBER, ST_MEMBER;
}
