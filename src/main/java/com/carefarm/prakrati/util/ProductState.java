package com.carefarm.prakrati.util;

public enum ProductState {
	SOLD_OUT("SOLD_OUT", "Sold Out"), ADDED_NEW("ADDED_NEW", "Added New"), ONCE_PRICED("ONCE_PRICED", "Once Priced"),
	DELETED("DELETED", "Deleted"), AVAILABLE("AVAILABLE", "Available"), OUT_OF_STOCK("OUT_OF_STOCK", "Out of Stock"),
	ASSIGNED("ASSIGNED", "Assigned");
	
	private String code;
	private String name;
	
	private ProductState(String code, String name) {
		this.code = code;
		this.name = name;
	}
	
	public String getCode() {
		return code;
	}

	public String getName() {
		return name;
	}
}
