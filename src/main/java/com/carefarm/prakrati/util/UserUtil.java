package com.carefarm.prakrati.util;

import org.apache.commons.lang3.StringUtils;

public class UserUtil {
	public static boolean isACompanyUser(String group) {
		if (StringUtils.equals(group, UserType.COMPANY_ADMIN.getType()) || 
				StringUtils.equals(group, UserType.AGENT.getType())) {
			return true;
		}
		return false;
	}
}
