package com.carefarm.prakrati.util;

public enum DemandStatus {
	OPEN("OPEN", "Open"), CLOSED("CLOSED", "Closed"), TENTATIVE("TENTATIVE", "Tentative"), 
	FULFILLED("FULFILLED", "Fulfilled"), ASSIGNED("ASSIGNED", "Assigned");
	
	private String code;
	private String value;
	
	private DemandStatus(String code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public String getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}

	public static DemandStatus getStatus(String value) {
		for (DemandStatus status : DemandStatus.values()) {
			if (status.value.equals(value)) {
				return status;
			}
		}
		return null;
	}
}
