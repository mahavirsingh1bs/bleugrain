package com.carefarm.prakrati.util;

public enum GlobalConstants {
	DATE_FORMAT("Date Format", "dd/MM/yyyy");
	
	private String name;
	private String value;
	
	private GlobalConstants(String name, String value) {
		this.name = name;
		this.value = value;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
	
}
