package com.carefarm.prakrati.util;

public enum PricingPeriod {
	YEARLY, QUARTERLY, HALF_YEARLY, MONTHLY;
}
