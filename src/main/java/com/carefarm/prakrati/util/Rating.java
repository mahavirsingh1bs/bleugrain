package com.carefarm.prakrati.util;

import java.math.BigDecimal;

public enum Rating {
	ONE(1, new BigDecimal("1.0")), ONE_HALF(15, new BigDecimal("1.5")), TWO(2, new BigDecimal("2.0")), 
	TWO_HALF(25, new BigDecimal("2.5")), THREE(3, new BigDecimal("3.0")), THREE_HALF(35, new BigDecimal("3.5")), 
	FOUR(4, new BigDecimal("4.0")), FOUR_HALF(45, new BigDecimal("4.5")), FIVE(5, new BigDecimal("5.0"));
	
	private Integer id;
	private BigDecimal value;
	
	private Rating(Integer id, BigDecimal value) {
		this.id = id;
		this.value = value;
	}
	
	public Integer getId() {
		return id;
	}

	public BigDecimal getValue() {
		return value;
	}
	
}
