package com.carefarm.prakrati.util;

public enum Currency {
	DOLLAR("Dollar", "$"), RUPEE("Rupee", "Rs."), EURO("Euro", "Eu"), POUND("Pound", "Po");
	
	private final String currencyName;
	private final String symbol;
	
	private Currency(String currencyName, String symbol) {
		this.currencyName = currencyName;
		this.symbol = symbol;
	}
	
	public String toString() {
		return currencyName;
	}
	
	public String getSymbol() {
		return symbol;
	}
	
}
