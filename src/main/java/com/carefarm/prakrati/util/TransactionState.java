package com.carefarm.prakrati.util;

public enum TransactionState {
	INITIATED, STARTED, IN_PROGRESS, PARTIALLY_COMPLETED, COMPLETED;
}
