package com.carefarm.prakrati.util;

public enum Category {
	SELLER("Seller"), CATEGORY("Category"), QUANTITY("Quantity"),
	PRICE("Price"), REVIEW("Review");
	
	private String name;
	
	private Category(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
	public Category valueof(String name) {
		for (Category category: Category.values()) {
			if (category.name.equalsIgnoreCase(name)) {
				return category;
			}
		}
		return null;
	}
	
	public String toString() {
		return name;
	}
	
}
