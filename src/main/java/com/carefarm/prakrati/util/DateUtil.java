package com.carefarm.prakrati.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class DateUtil {
	
	public boolean getDifference(LocalDate first, LocalDate second, ChronoUnit chronoUnit) {
		String pattern = "dd/MM/yyyy";
		DateFormat dateFormat = new SimpleDateFormat(pattern);
		LocalDate currentDate = LocalDate.now();
		
		DateTimeFormatter indianFormatter = DateTimeFormatter.ofPattern(pattern);
		LocalDate createdDate = LocalDate.parse(dateFormat.format(second), indianFormatter);
		
		long numberOfDays = ChronoUnit.DAYS.between(currentDate, createdDate);
		if (numberOfDays <= 5) {
			return true;
		}
		return false;
	}
}
