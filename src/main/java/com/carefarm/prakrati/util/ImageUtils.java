package com.carefarm.prakrati.util;

import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Base64Utils;

import com.carefarm.prakrati.common.util.Constants;
import com.carefarm.prakrati.vo.ImageVO;

public class ImageUtils {
	
	public static ImageVO extractImage(String imageDataUrl) {
		int base64Index = imageDataUrl.indexOf(Constants.BASE64_HEADER);
		int contentTypeIndex = imageDataUrl.indexOf(Constants.CONTENT_TYPE_IMAGE_HEADER);
		String contentType = StringUtils.substring(imageDataUrl, contentTypeIndex, base64Index - 1);
		String extension = StringUtils.substring(contentType, Constants.CONTENT_TYPE_IMAGE_HEADER.length());
		String dataUrlHeaders = Constants.DATA_HEADER + Constants.CONTENT_TYPE_IMAGE_HEADER + extension + 
				Constants.SEMICOLON + Constants.BASE64_HEADER;
		String imageData = StringUtils.substring(imageDataUrl, dataUrlHeaders.length());
		byte[] data = Base64Utils.decodeFromString(StringUtils.replace(imageData, " ", "+"));
		ImageVO imageVO = new ImageVO("croppedImage." + extension, "croppedImageOrig" + extension, contentType, data.length, data);
		return imageVO;
	}
}
