package com.carefarm.prakrati.util;

public enum NotificationCategory {
	EMAIL("E-Mail"), FOLLOWING_YOU("Following You"), PRODUCT_REVIEWED("Product Reviewed"),
	DEMAND_RAISED("Demand Raised"), PRODUCT_ADDED("Product Added"), PRODUCT_UPDATED("Product Updated"),  
	PRODUCT_SOLDOUT("Product SoldOut"), ORDER_PLACED("Order Placed"), ORDER_SHIPPED("Order Shipped"), 
	ORDER_DELIVERED("Order Delivered"), FOLLOWING_PRODUCT("Following Product"), USER_REGISTERED("User Registered"), 
	USER_CREATED("User Created"), ORDER_DISPATCHED("Order Dispatched"), ORDER_ASSIGNED("Order Assigned"),
	PRODUCT_ASSIGNED("Product Assigned"), DEMAND_ASSIGNED("Demand Assigned"), DEMAND_FULFILLED("Demand Fulfilled"),
	ADVANCED_BOOKING("Advanced Booking"), COMPANY_REGISTERED("Company Registered"), COMPANY_CREATED("Company Created");
	
	private String name;
	
	private NotificationCategory(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}
	
}
