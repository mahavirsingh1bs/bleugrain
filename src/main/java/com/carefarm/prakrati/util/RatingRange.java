package com.carefarm.prakrati.util;

import java.math.BigDecimal;

public enum RatingRange {
	ZERO_TO_ONE(1, new BigDecimal("0.0"), new BigDecimal("1.0")), 
	ONE_TO_TWO(2, new BigDecimal("1.0"), new BigDecimal("2.0")), 
	TWO_TO_THREE(3, new BigDecimal("2.0"), new BigDecimal("3.0")), 
	THREE_TO_FOUR(4, new BigDecimal("3.0"), new BigDecimal("4.0")), 
	FOUR_TO_FIVE(5, new BigDecimal("4.0"), new BigDecimal("5.0"));
	
	private Integer id;
	
	private BigDecimal minLimit;
	
	private BigDecimal maxLimit;
	
	private RatingRange(Integer id, BigDecimal minLimit, BigDecimal maxLimit) {
		this.id = id;
		this.minLimit = minLimit;
		this.maxLimit = maxLimit;
	}

	public Integer getId() {
		return id;
	}
	
	public BigDecimal getMinLimit() {
		return minLimit;
	}

	public BigDecimal getMaxLimit() {
		return maxLimit;
	}
	
	@Override
	public String toString() {
		return "" + minLimit + " - " + maxLimit;
	}
	
	public static RatingRange findRange(Rating rating) {
		for (RatingRange ratingRange : RatingRange.values()) {
			if (ratingRange.minLimit.compareTo(rating.getValue()) < 0 && 
					ratingRange.maxLimit.compareTo(rating.getValue()) >= 0) {
				return ratingRange;
			}
		}
		return null;
	}
}
