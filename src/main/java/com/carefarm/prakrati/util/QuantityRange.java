package com.carefarm.prakrati.util;

public enum QuantityRange {
	/*QR_ZERO_TO_ONE_MILLION(1, new BigDecimal("0"), new BigDecimal("1000000"), Unit.QUINTAL),
	QR_ONE_MILLION_TO_TWO_MILLION(2, new BigDecimal("1000000"), new BigDecimal("2000000"), Unit.QUINTAL),
	QR_TWO_MILLION_TO_THREE_MILLION(3, new BigDecimal("2000000"), new BigDecimal("3000000"), Unit.QUINTAL),
	QR_THREE_TO_TO_FOUR_MILLION(4, new BigDecimal("3000000"), new BigDecimal("4000000"), Unit.QUINTAL),
	QR_FOUR_MILLION_TO_FIVE_MILLION(4, new BigDecimal("4000000"), new BigDecimal("5000000"), Unit.QUINTAL),
	QR_FIVE_MILLION_TO_TEN_MILLION(4, new BigDecimal("5000000"), new BigDecimal("10000000"), Unit.QUINTAL),
	QR_TEN_MILLION_TO_TWENTY_MILLION(4, new BigDecimal("10000000"), new BigDecimal("20000000"), Unit.QUINTAL),
	QR_TWENTY_MILLION_THIRTY_FIVE_MILLION(4, new BigDecimal("20000000"), new BigDecimal("30000000"), Unit.QUINTAL);
	
	
	private Integer id;
	private BigDecimal minLimit;
	private BigDecimal maxLimit;
	private com.carefarm.prakrati.common.entity.Unit unit;
	
	private QuantityRange(Integer id, BigDecimal minLimit, BigDecimal maxLimit, com.carefarm.prakrati.common.entity.Unit unit) {
		this.id = id;
		this.minLimit = minLimit;
		this.maxLimit = maxLimit;
		this.unit = unit;
	}

	public Integer getId() {
		return id;
	}
	
	public BigDecimal getMinLimit() {
		return minLimit;
	}

	public BigDecimal getMaxLimit() {
		return maxLimit;
	}

	public Unit getUnit() {
		return unit;
	}
	
	@Override
	public String toString() {
		return "" + minLimit + " - " + maxLimit + "" + unit.getSymbol();
	}
	
	public static QuantityRange findRange(Quantity quantity) {
		for (QuantityRange quantityRange : QuantityRange.values()) {
			if ((quantityRange.minLimit.compareTo(quantity.getQuantity()) <= 0) && 
					(quantityRange.maxLimit.compareTo(quantity.getQuantity()) > 0) && 
					quantityRange.unit == quantity.getUnit()) {
				return quantityRange; 
			}
		}
		return null;
	}*/
}
