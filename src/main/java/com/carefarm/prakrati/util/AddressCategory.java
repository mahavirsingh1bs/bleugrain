package com.carefarm.prakrati.util;

public enum AddressCategory {
	DELIVERY, BILLING;
}
