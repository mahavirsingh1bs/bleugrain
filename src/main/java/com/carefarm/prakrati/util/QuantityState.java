package com.carefarm.prakrati.util;

public enum QuantityState {
	SOLD, UNSOLD;
}
