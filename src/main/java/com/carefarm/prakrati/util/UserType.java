package com.carefarm.prakrati.util;

public enum UserType {
	FARMER(1, "Farmer"), BROKER(2, "Broker"), RETAILER(3, "Retailer"), 
	AGENT(4, "Agent"), DRIVER(5, "Driver"), SUPER_ADMIN(6, "Admin"), 
	COMPANY(7, "Company"), COMPANY_ADMIN(8, "Company Admin"), EMPLOYEE(9, "Employee"), 
	CUSTOMER(10, "Customer"), EXECUTIVE(11, "Executive"), REGION_ADMIN(12, "Region Admin"), ;
	
	private Integer id;
	private String type;
	
	private UserType(Integer id, String type) {
		this.id = id;
		this.type = type;
	}
	
	public Integer getId() {
		return id;
	}
	
	public String getType() {
		return type;
	}
	
	public static UserType getUserType(String name) {
		for (UserType userType : UserType.values()) {
			if (userType.getType().equalsIgnoreCase(name)) {
				return userType;
			}
		}
		return null;
	}
	
	@Override
	public String toString() {
		return type;
	}
}
