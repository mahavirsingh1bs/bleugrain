package com.carefarm.prakrati.util;

public enum CardType {
	VISA("Visa"), MASTER_CARD("Mastercard"), MASTERO("Mastero"), AMERICAN_EXPRESS("American Express"); 
	
	private final String cardName;
	
	private CardType(String cardName) {
		this.cardName = cardName;
	}
	
	public String toString() {
		return cardName;
	}
	
	public boolean isValid() {
		return true;
	}
}
