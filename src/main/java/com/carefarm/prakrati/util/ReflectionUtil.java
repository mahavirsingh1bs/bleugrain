package com.carefarm.prakrati.util;

import java.lang.reflect.Field;

import org.springframework.util.ReflectionUtils;

public class ReflectionUtil {
	
	public static void setField(String fieldName, Object target, Object value) {
		Field field = ReflectionUtils.findField(target.getClass(), fieldName);
		ReflectionUtils.makeAccessible(field);
		ReflectionUtils.setField(field, target, value);
	}
	
}
