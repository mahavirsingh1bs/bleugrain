package com.carefarm.prakrati.util;

public enum PriceRange implements Comparable<PriceRange> {
	/*ZERO_TO_FIVE_THOUSANDS(1, new BigDecimal("0"), new BigDecimal("5000"), Currency.RUPEE, Unit.QUINTAL),
	FIVE_TO_TEN_THOUSANDS(2, new BigDecimal("5000"), new BigDecimal("10000"), Currency.RUPEE, Unit.QUINTAL),
	TEN_TO_TWENTY_THOUSANDS(3, new BigDecimal("10000"), new BigDecimal("20000"), Currency.RUPEE, Unit.QUINTAL),
	TWENTY_TO_FORTY_THOUSANDS(4, new BigDecimal("20000"), new BigDecimal("40000"), Currency.RUPEE, Unit.QUINTAL),
	FOURTY_TO_EIGHTY_THOUSANDS(4, new BigDecimal("40000"), new BigDecimal("80000"), Currency.RUPEE, Unit.QUINTAL),
	EIGHTY_TO_ONE_SIXTY_THOUSANDS(4, new BigDecimal("80000"), new BigDecimal("160000"), Currency.RUPEE, Unit.QUINTAL),
	ONE_SIXTY_TO_THREE_TWENTY_THOUSANDS(4, new BigDecimal("160000"), new BigDecimal("320000"), Currency.RUPEE, Unit.QUINTAL);

	private Integer id;
	private BigDecimal minLimit;
	private BigDecimal maxLimit;
	private Currency currency;
	private Unit unit;
	
	private PriceRange(Integer id, BigDecimal minLimit, BigDecimal maxLimit, Currency currency, Unit unit) {
		this.id = id;
		this.minLimit = minLimit;
		this.maxLimit = maxLimit;
		this.currency = currency;
		this.unit = unit;
	}
	
	public Integer getId() {
		return id;
	}
	
	public BigDecimal getMinLimit() {
		return minLimit;
	}

	public BigDecimal getMaxLimit() {
		return maxLimit;
	}

	public Currency getCurrency() {
		return currency;
	}
	
	public String toString() {
		return "" + minLimit + " - " + maxLimit + " " + currency.getSymbol() + "/" + unit.getSymbol(); 
	}

	public static PriceRange findRange(PricePerUnit pricePerUnit) {
		for (PriceRange priceRange : PriceRange.values()) {
			if ((priceRange.minLimit.compareTo(pricePerUnit.getPricePerUnit()) <= 0) && 
				(priceRange.maxLimit.compareTo(pricePerUnit.getPricePerUnit()) > 0) && 
				priceRange.currency == pricePerUnit.getCurrency() && 
				priceRange.unit == pricePerUnit.getUnit()) {
				return priceRange;
			}
		}
		return null;
	}*/
	
}
