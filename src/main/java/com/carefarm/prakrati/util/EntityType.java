package com.carefarm.prakrati.util;

public enum EntityType {
	COMPANY, USER, PRODUCT;
}
