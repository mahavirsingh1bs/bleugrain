package com.carefarm.prakrati.util;

import java.math.BigDecimal;
import java.util.List;

import com.carefarm.prakrati.common.entity.PricePerUnit;
import com.carefarm.prakrati.entity.Price;

public class CommonUtil {

	public static Double findInitialPrice(List<Price> prices) {
		for (Price price : prices) {
			if (price.isInitial()) {
				return price.getPrice();
			}
		}
		return null;
	}
	
	public static PricePerUnit getMarketPrice(List<Price> prices) {
		for (Price price : prices) {
			if (price.isInitial()) {
				return new PricePerUnit(BigDecimal.valueOf(price.getPrice()), price.getCurrency(), price.getUnit());
			}
		}
		return null;
	}
	
}
