package com.carefarm.prakrati.util;

import java.util.Calendar;
import java.util.Random;

import org.apache.commons.lang.StringUtils;

public class VerifCodeGenerator {

	private static final int START = 0;
	private static final int MAX = 6;
	
	public static String generateVerifCode() {
		Calendar calendar = Calendar.getInstance();
		Random randomGenrtr = new Random(calendar.getTimeInMillis());
		Long userNo = Math.abs(randomGenrtr.nextLong());
		return StringUtils.substring(userNo.toString(), START, MAX);
	}
	
}
