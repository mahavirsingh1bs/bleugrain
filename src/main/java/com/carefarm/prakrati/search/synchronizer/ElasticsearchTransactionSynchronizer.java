package com.carefarm.prakrati.search.synchronizer;

import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;

@Component
public class ElasticsearchTransactionSynchronizer extends
		TransactionSynchronizationAdapter {

	@Override
	public void afterCommit() {
		
	}
}
