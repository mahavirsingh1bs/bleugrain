package com.carefarm.prakrati.search.repository;

import com.carefarm.prakrati.search.document.DepartmentDoc;
import com.carefarm.prakrati.search.repository.custom.DepartmentSearchRepositoryCustom;

public interface DepartmentSearchRepository extends PrakratiSearchRepository<DepartmentDoc, Long>, DepartmentSearchRepositoryCustom {

}
