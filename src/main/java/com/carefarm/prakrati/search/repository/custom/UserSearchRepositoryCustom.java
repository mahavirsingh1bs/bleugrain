package com.carefarm.prakrati.search.repository.custom;

import org.springframework.data.domain.Pageable;

import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.document.UserDoc;
import com.carefarm.prakrati.util.UserType;

public interface UserSearchRepositoryCustom extends PrakratiSearchRepositoryCustom<UserDoc, Long> {
	SearchResult<Long> findByKeywordContains(String keyword, UserType userType, Pageable pageable);
	
	SearchResult<Long> findByKeywordContains(String keyword, String companyId, UserType userType,
			Pageable pageable);
} 