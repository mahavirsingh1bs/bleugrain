package com.carefarm.prakrati.search.repository;

import com.carefarm.prakrati.search.document.MenuitemDoc;
import com.carefarm.prakrati.search.repository.custom.MenuitemSearchRepositoryCustom;

public interface MenuitemSearchRepository extends PrakratiSearchRepository<MenuitemDoc, Long>, MenuitemSearchRepositoryCustom {

}
