package com.carefarm.prakrati.search.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.carefarm.prakrati.search.document.BankDoc;
import com.carefarm.prakrati.search.repository.custom.BankSearchRepositoryCustom;

public interface BankSearchRepository extends PrakratiSearchRepository<BankDoc, Long>, BankSearchRepositoryCustom {
	Page<BankDoc> findByName(String name, Pageable pageable);
}
