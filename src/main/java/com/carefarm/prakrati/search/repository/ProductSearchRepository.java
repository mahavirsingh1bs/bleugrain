package com.carefarm.prakrati.search.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.carefarm.prakrati.search.document.ProductDoc;
import com.carefarm.prakrati.search.repository.custom.ProductSearchRepositoryCustom;

public interface ProductSearchRepository extends PrakratiSearchRepository<ProductDoc, Long>, ProductSearchRepositoryCustom {
	Page<ProductDoc> findByName(String name, Pageable pageable);
}
