package com.carefarm.prakrati.search.repository.impl;

import static com.carefarm.prakrati.web.util.PrakratiConstants.SELECTED;
import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchQuery;
import static org.elasticsearch.index.query.QueryBuilders.multiMatchQuery;
import static org.elasticsearch.index.query.QueryBuilders.nestedQuery;
import static org.elasticsearch.search.aggregations.AggregationBuilders.nested;
import static org.elasticsearch.search.aggregations.AggregationBuilders.range;
import static org.elasticsearch.search.aggregations.AggregationBuilders.terms;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.nested.Nested;
import org.elasticsearch.search.aggregations.bucket.range.Range;
import org.elasticsearch.search.aggregations.bucket.range.RangeBuilder;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.core.ResultsExtractor;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.search.bean.RangeBucket;
import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.bean.TermsBucket;
import com.carefarm.prakrati.search.document.DemandDoc;
import com.carefarm.prakrati.search.dto.SearchCriteria;
import com.carefarm.prakrati.search.repository.custom.DemandSearchRepositoryCustom;
import com.carefarm.prakrati.web.bean.FiltersBean;

public class DemandSearchRepositoryImpl extends PrakratiSearchRepositoryCustomImpl<DemandDoc, Long> implements DemandSearchRepositoryCustom {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DemandSearchRepositoryImpl.class);
	
	@Override
	public SearchResult<Long> findByKeywordContains(SearchCriteria criteria) {
		BoolQueryBuilder boolQuery = boolQuery();
		
		if (Predicates.notNull.test(criteria.getKeywords())) {
			boolQuery.should(multiMatchQuery(criteria.getKeywords(), "name", "tags"));
			boolQuery.minimumNumberShouldMatch(1);
		} else {
			boolQuery.must(
				nestedQuery("categories", 
					boolQuery().must(
						matchQuery("categories.uniqueId", criteria.getUniqueId())
					)
				)
			);
		}
		
		BoolQueryBuilder filterBoolQuery = boolQuery()
			.must(multiMatchQuery(false, "archived", "mocked"))
			.mustNot(matchQuery("status", "CLOSED"));
		
		FiltersBean filters = criteria.getFilters();
		Optional<Map<String, String>> sellersOpt = Optional.ofNullable(filters.getSellers());
		sellersOpt.ifPresent(sellers -> { 
			sellers.forEach((key, value) -> {
				if (StringUtils.equalsIgnoreCase(SELECTED, value)) {
					filterBoolQuery.must(matchQuery("buyerCategory", key));
				}
			});
		});
		
		Optional<Map<String, String>> categoriesOpt = Optional.ofNullable(filters.getCategories());
		categoriesOpt.ifPresent(categories -> { 
			categories.forEach((key, value) -> {
				if (StringUtils.equalsIgnoreCase(SELECTED, value)) {
					filterBoolQuery.must(
						nestedQuery("categories", 
							boolQuery().must(
								matchQuery("categories.name", key)
							)
						)
					);
				}
			});
		});
		
		Optional<Map<String, String>> quantitiesOpt = Optional.ofNullable(filters.getQuantities());
		quantitiesOpt.ifPresent(quantities -> { 
			quantities.forEach((key, value) -> {
				if (StringUtils.equalsIgnoreCase(SELECTED, value)) {
					filterBoolQuery.must(
						nestedQuery("quantity", 
							boolQuery().must(buildRange("quantity.quantity", key))
						)
					);
				}
			});
		});
		
		Optional<Map<String, String>> pricesOpt = Optional.ofNullable(filters.getPrices());
		pricesOpt.ifPresent(prices -> { 
			prices.forEach((key, value) -> {
				if (StringUtils.equalsIgnoreCase(SELECTED, value)) {
					filterBoolQuery.must(buildRange("pricePerUnit", key));
				}
			});
		});
		
		Document annotation = DemandDoc.class.getAnnotation(Document.class);
		NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder();
		nativeSearchQueryBuilder
			.withQuery(boolQuery.filter(filterBoolQuery))
			.withIndices(annotation.indexName())
			.withTypes(annotation.type())
			.withPageable(criteria.getPageable());
		
		nativeSearchQueryBuilder
			.addAggregation(
				nested("categories").path("categories")
					.subAggregation(
							AggregationBuilders
								.terms("name")
								.field("categories.name")
					)
			);
		
		nativeSearchQueryBuilder.addAggregation(terms("buyer").field("buyerCategory"));
		
		RangeBuilder priceRangeBuilder = range("priceRanges").field("pricePerUnit");
		priceRangeBuilder.addUnboundedTo(1000.0);
		priceRangeBuilder.addRange(1000.0, 2000.0);
		priceRangeBuilder.addRange(2000.0, 3000.0);
		priceRangeBuilder.addRange(3000.0, 4000.0);
		priceRangeBuilder.addRange(4000.0, 5000.0);
		priceRangeBuilder.addUnboundedFrom(5000.0);
		nativeSearchQueryBuilder.addAggregation(priceRangeBuilder);
		
		RangeBuilder quantityRangeBuilder = range("quantityRanges").field("quantity.quantity");
		quantityRangeBuilder.addUnboundedTo(200.0);
		quantityRangeBuilder.addRange(200.0, 400.0);
		quantityRangeBuilder.addRange(400.0, 600.0);
		quantityRangeBuilder.addRange(600.0, 800.0);
		quantityRangeBuilder.addRange(800.0, 1000.0);
		quantityRangeBuilder.addUnboundedFrom(1000.0);
		
		nativeSearchQueryBuilder.addAggregation(nested("quantity").path("quantity").subAggregation(quantityRangeBuilder));
	
		SearchQuery searchQuery = nativeSearchQueryBuilder.build();
		Aggregations aggregations = elasticsearchTemplate.query(searchQuery, new ResultsExtractor<Aggregations>() {
			@Override
			public Aggregations extract(SearchResponse response) {
				return response.getAggregations();
			}
		});
		
		long totalHits = elasticsearchTemplate.count(searchQuery, DemandDoc.class);
		List<DemandDoc> hits = elasticsearchTemplate.queryForList(searchQuery, DemandDoc.class);
		
		List<Long> demandIds = new ArrayList<>();
		hits.forEach(hit -> demandIds.add(hit.getId()));
		
		Terms buyerAgg = aggregations.get("buyer");
		Nested categoriesAggs = aggregations.get("categories");
		Terms categories = categoriesAggs.getAggregations().get("name");
		Nested quantity = aggregations.get("quantity");
		Range quantityRanges = quantity.getAggregations().get("quantityRanges");
		Range priceRanges = aggregations.get("priceRanges");
		
		LOGGER.info("Sellers Aggregations: ");
		List<TermsBucket> buyerBuckets = new ArrayList<>();
		for (Terms.Bucket entry: buyerAgg.getBuckets()) {
			buyerBuckets.add(new TermsBucket(entry.getKeyAsString(), entry.getDocCount()));
		}

		LOGGER.info("Categories Aggregations: ");
		List<TermsBucket> categoryBuckets = new ArrayList<>();
		for (Terms.Bucket entry: categories.getBuckets()) {
			categoryBuckets.add(new TermsBucket(entry.getKeyAsString(), entry.getDocCount()));
		}
		
		LOGGER.info("Quantity Aggregations: ");
		List<RangeBucket> quantityBuckets = new ArrayList<>();
		for (Range.Bucket entry: quantityRanges.getBuckets()) {
			addRangeBucket(quantityBuckets, entry);
		}
		
		LOGGER.info("Price Aggregations: ");
		List<RangeBucket> priceBuckets = new ArrayList<>();
		for (Range.Bucket entry: priceRanges.getBuckets()) {
			addRangeBucket(priceBuckets, entry);
		}
		
		SearchResult.Builder<Long> resultBuilder = new SearchResult.Builder<Long>();
		resultBuilder.results(demandIds);
		resultBuilder.buyerBuckets(buyerBuckets);
		resultBuilder.categoryBuckets(categoryBuckets);
		resultBuilder.quantityBuckets(quantityBuckets);
		resultBuilder.priceBuckets(priceBuckets);
		resultBuilder.totalHits(totalHits);
		return resultBuilder.build();
	}

}
