package com.carefarm.prakrati.search.repository;

import com.carefarm.prakrati.search.document.RoleDoc;
import com.carefarm.prakrati.search.repository.custom.RoleSearchRepositoryCustom;

public interface RoleSearchRepository extends PrakratiSearchRepository<RoleDoc, Long>, RoleSearchRepositoryCustom {

}
