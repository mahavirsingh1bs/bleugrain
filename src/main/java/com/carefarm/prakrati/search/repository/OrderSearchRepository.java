package com.carefarm.prakrati.search.repository;

import com.carefarm.prakrati.search.document.OrderDoc;
import com.carefarm.prakrati.search.repository.custom.OrderSearchRepositoryCustom;

public interface OrderSearchRepository extends PrakratiSearchRepository<OrderDoc, Long>, OrderSearchRepositoryCustom {
	
}
