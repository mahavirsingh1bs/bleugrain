package com.carefarm.prakrati.search.repository.custom;

import org.springframework.data.domain.Pageable;

import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.document.BankDoc;
import com.carefarm.prakrati.web.bean.SearchRequest;

public interface BankSearchRepositoryCustom extends PrakratiSearchRepositoryCustom<BankDoc, Long> {
	SearchResult<Long> findByKeywordContains(SearchRequest request, Pageable pageable);
} 