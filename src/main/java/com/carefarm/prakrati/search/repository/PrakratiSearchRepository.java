package com.carefarm.prakrati.search.repository;

import java.io.Serializable;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface PrakratiSearchRepository<T, ID extends Serializable> 
	extends ElasticsearchRepository<T, ID> {
	
}