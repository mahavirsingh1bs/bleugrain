package com.carefarm.prakrati.search.repository;

import com.carefarm.prakrati.search.document.StateDoc;
import com.carefarm.prakrati.search.repository.custom.StateSearchRepositoryCustom;

public interface StateSearchRepository extends PrakratiSearchRepository<StateDoc, Long>, StateSearchRepositoryCustom {

}
