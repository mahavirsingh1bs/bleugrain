package com.carefarm.prakrati.search.repository;

import com.carefarm.prakrati.search.document.CountryDoc;
import com.carefarm.prakrati.search.repository.custom.CountrySearchRepositoryCustom;

public interface CountrySearchRepository extends PrakratiSearchRepository<CountryDoc, Long>, CountrySearchRepositoryCustom {

}
