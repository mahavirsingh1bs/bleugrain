package com.carefarm.prakrati.search.repository;

import com.carefarm.prakrati.search.document.UserDoc;
import com.carefarm.prakrati.search.repository.custom.UserSearchRepositoryCustom;

public interface UserSearchRepository extends PrakratiSearchRepository<UserDoc, Long>, UserSearchRepositoryCustom {

}
