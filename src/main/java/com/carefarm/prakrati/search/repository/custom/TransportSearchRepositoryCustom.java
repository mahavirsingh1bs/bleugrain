package com.carefarm.prakrati.search.repository.custom;

import org.springframework.data.domain.Pageable;

import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.document.TransportDoc;
import com.carefarm.prakrati.web.bean.SearchRequest;


public interface TransportSearchRepositoryCustom extends PrakratiSearchRepositoryCustom<TransportDoc, Long> {
	SearchResult<Long> findByKeywordContains(SearchRequest request, Pageable pageable);
}
