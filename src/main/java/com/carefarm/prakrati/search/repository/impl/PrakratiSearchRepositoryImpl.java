package com.carefarm.prakrati.search.repository.impl;

import java.io.Serializable;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.support.AbstractElasticsearchRepository;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.search.repository.PrakratiSearchRepository;

public class PrakratiSearchRepositoryImpl<T, ID extends Serializable> extends AbstractElasticsearchRepository<T, ID> implements PrakratiSearchRepository<T, ID> {

	@Autowired
	protected ElasticsearchTemplate elasticsearchTemplate;
	
	@Override
	protected String stringIdRepresentation(ID id) {
		if (Predicates.notNull.test(id)) {
			return id.toString();
		}
		return UUID.randomUUID().toString();
	}

}
