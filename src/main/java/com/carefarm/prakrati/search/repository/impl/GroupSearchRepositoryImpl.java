package com.carefarm.prakrati.search.repository.impl;

import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchQuery;
import static org.elasticsearch.index.query.QueryBuilders.multiMatchQuery;
import static org.elasticsearch.index.query.QueryBuilders.nestedQuery;

import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.springframework.data.domain.Pageable;

import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.document.GroupDoc;
import com.carefarm.prakrati.search.repository.custom.GroupSearchRepositoryCustom;
import com.carefarm.prakrati.web.bean.SearchRequest;

public class GroupSearchRepositoryImpl extends PrakratiSearchRepositoryCustomImpl<GroupDoc, Long> implements GroupSearchRepositoryCustom {

	@Override
	public SearchResult<Long> findByKeywordContains(SearchRequest request, Pageable pageable) {
		BoolQueryBuilder boolQuery = boolQuery();
		boolQuery.filter(multiMatchQuery(false, "archived"));
		if (StringUtils.isNotEmpty(request.getKeyword())) {
			boolQuery.should(matchQuery("name", request.getKeyword()));
			boolQuery.should(nestedQuery("createdBy", multiMatchQuery(request.getKeyword(), "createdBy.firstName", "createdBy.lastName")));
			boolQuery.should(nestedQuery("modifiedBy", multiMatchQuery(request.getKeyword(), "modifiedBy.firstName", "modifiedBy.lastName")));
			boolQuery.minimumNumberShouldMatch(1);
		}
		return findByKeywordContainsInternal(boolQuery, pageable, GroupDoc.class);
	}
	
}
