package com.carefarm.prakrati.search.repository;

import com.carefarm.prakrati.search.document.GroupDoc;
import com.carefarm.prakrati.search.repository.custom.GroupSearchRepositoryCustom;

public interface GroupSearchRepository extends PrakratiSearchRepository<GroupDoc, Long>, GroupSearchRepositoryCustom {

}
