package com.carefarm.prakrati.search.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.carefarm.prakrati.search.document.DemandDoc;
import com.carefarm.prakrati.search.repository.custom.DemandSearchRepositoryCustom;

public interface DemandSearchRepository extends PrakratiSearchRepository<DemandDoc, Long>, DemandSearchRepositoryCustom {
	
	Page<DemandDoc> findByName(String name, Pageable pageable);
}
