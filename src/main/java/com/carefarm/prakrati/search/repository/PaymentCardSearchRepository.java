package com.carefarm.prakrati.search.repository;

import com.carefarm.prakrati.search.document.PaymentCardDoc;
import com.carefarm.prakrati.search.repository.custom.DepartmentSearchRepositoryCustom;

public interface PaymentCardSearchRepository extends PrakratiSearchRepository<PaymentCardDoc, Long>, DepartmentSearchRepositoryCustom {

}
