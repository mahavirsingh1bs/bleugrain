package com.carefarm.prakrati.search.repository;

import com.carefarm.prakrati.search.document.PaymentMethodDoc;
import com.carefarm.prakrati.search.repository.custom.DepartmentSearchRepositoryCustom;

public interface PaymentMethodSearchRepository extends PrakratiSearchRepository<PaymentMethodDoc, Long>, DepartmentSearchRepositoryCustom {

}
