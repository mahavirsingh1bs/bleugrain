package com.carefarm.prakrati.search.repository.custom;

import org.springframework.data.domain.Pageable;

import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.document.PaymentCardDoc;
import com.carefarm.prakrati.web.bean.SearchRequest;

public interface PaymentCardSearchRepositoryCustom extends PrakratiSearchRepositoryCustom<PaymentCardDoc, Long> {
	SearchResult<Long> findByKeywordContains(SearchRequest request, Pageable pageable);
} 