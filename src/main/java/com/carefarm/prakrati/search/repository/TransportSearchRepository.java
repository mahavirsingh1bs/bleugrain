package com.carefarm.prakrati.search.repository;

import com.carefarm.prakrati.search.document.TransportDoc;
import com.carefarm.prakrati.search.repository.custom.TransportSearchRepositoryCustom;


public interface TransportSearchRepository extends PrakratiSearchRepository<TransportDoc, Long>, TransportSearchRepositoryCustom {

}
