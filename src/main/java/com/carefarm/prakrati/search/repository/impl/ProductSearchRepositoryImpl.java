package com.carefarm.prakrati.search.repository.impl;

import static com.carefarm.prakrati.web.util.PrakratiConstants.SELECTED;
import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchQuery;
import static org.elasticsearch.index.query.QueryBuilders.multiMatchQuery;
import static org.elasticsearch.index.query.QueryBuilders.nestedQuery;
import static org.elasticsearch.search.aggregations.AggregationBuilders.nested;
import static org.elasticsearch.search.aggregations.AggregationBuilders.range;
import static org.elasticsearch.search.aggregations.AggregationBuilders.terms;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.nested.Nested;
import org.elasticsearch.search.aggregations.bucket.nested.NestedBuilder;
import org.elasticsearch.search.aggregations.bucket.range.Range;
import org.elasticsearch.search.aggregations.bucket.range.RangeBuilder;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.core.ResultsExtractor;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.search.bean.RangeBucket;
import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.bean.TermsBucket;
import com.carefarm.prakrati.search.document.AbstractDoc;
import com.carefarm.prakrati.search.document.ProductDoc;
import com.carefarm.prakrati.search.dto.SearchCriteria;
import com.carefarm.prakrati.search.repository.custom.ProductSearchRepositoryCustom;
import com.carefarm.prakrati.web.bean.FiltersBean;

public class ProductSearchRepositoryImpl extends PrakratiSearchRepositoryCustomImpl<ProductDoc, Long> implements
		ProductSearchRepositoryCustom {
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductSearchRepositoryImpl.class);

	@Override
	public SearchResult<Long> findByKeywordContains(String keywords, String productId, User currentUser, Pageable pageable) {
		BoolQueryBuilder boolQuery = boolQuery()
				.should(multiMatchQuery(keywords, "name", "tags"))
				.minimumNumberShouldMatch(1);
		
		BoolQueryBuilder filterBoolQuery = boolQuery()
			.must(multiMatchQuery(false, "archived", "mocked"))
			.mustNot(matchQuery("uniqueId", productId))
			.mustNot(matchQuery("status", "SOLD_OUT"))
			.mustNot(matchQuery("status", "DELETED"));
		
		if (Predicates.notNull.test(currentUser)) {
			filterBoolQuery.mustNot(
				nestedQuery("owner", 
					matchQuery("owner.userNo", currentUser.getUserNo())
				));
		}
		
		if (Predicates.notNull.test(currentUser) && currentUser.hasRole("ROLE_CUSTOMER")) {
			filterBoolQuery.must(matchQuery("consumerProduct", true));
		} else {
			filterBoolQuery.must(matchQuery("consumerProduct", false));
		}
		
		Document annotation = ProductDoc.class.getAnnotation(Document.class);
		NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder();
		SearchQuery searchQuery = nativeSearchQueryBuilder
			.withQuery(boolQuery.filter(filterBoolQuery))
			.withIndices(annotation.indexName())
			.withTypes(annotation.type())
			.withPageable(pageable)
			.build();
		
		long totalHits = elasticsearchTemplate.count(searchQuery, ProductDoc.class);
		List<? extends AbstractDoc> hits = elasticsearchTemplate.queryForList(searchQuery, ProductDoc.class);
		List<Long> results = new ArrayList<>();
		hits.forEach(hit -> results.add(hit.getId()));
		
		SearchResult.Builder<Long> resultBuilder = new SearchResult.Builder<Long>();
		resultBuilder.results(results);
		resultBuilder.totalHits(totalHits);
		return resultBuilder.build();
	}
	
	@Override
	public SearchResult<Long> findByKeywordContains(SearchCriteria criteria) {
		BoolQueryBuilder boolQuery = boolQuery();
			
		if (Predicates.notNull.test(criteria.getKeywords())) {
			boolQuery.should(multiMatchQuery(criteria.getKeywords(), "name", "tags"));
			boolQuery.minimumNumberShouldMatch(1);
		} else {
			boolQuery.must(
				nestedQuery("categories", 
					boolQuery().must(
						matchQuery("categories.uniqueId", criteria.getUniqueId())
					)
				)
			);
		}
		
		if (Predicates.notNull.test(criteria.getCurrentUser())) {
			boolQuery.mustNot(matchQuery("owner.userNo", criteria.getCurrentUser().getUserNo()));
		}
		
		BoolQueryBuilder filterBoolQuery = boolQuery()
			.must(multiMatchQuery(false, "archived", "mocked"))
			.must(matchQuery("consumerProduct", criteria.isCnsmrPrdt()))
			.mustNot(matchQuery("status", "SOLD_OUT"))
			.mustNot(matchQuery("status", "DELETED"));
		
		FiltersBean filters = criteria.getFilters();
		Optional<Map<String, String>> sellersOpt = Optional.ofNullable(filters.getSellers());
		sellersOpt.ifPresent(sellers -> { 
			sellers.forEach((key, value) -> {
				if (StringUtils.equalsIgnoreCase(SELECTED, value)) {
					filterBoolQuery.must(
						nestedQuery("owner", 
							boolQuery().must(
								matchQuery("owner.userType", key)
							)
						)
					);
				}
			});
		});
		
		Optional<Map<String, String>> categoriesOpt = Optional.ofNullable(filters.getCategories());
		categoriesOpt.ifPresent(categories -> { 
			categories.forEach((key, value) -> {
				if (StringUtils.equalsIgnoreCase(SELECTED, value)) {
					filterBoolQuery.must(
						nestedQuery("categories", 
							boolQuery().must(
								matchQuery("categories.name", key)
							)
						)
					);
				}
			});
		});
		
		Optional<Map<String, String>> quantitiesOpt = Optional.ofNullable(filters.getQuantities());
		quantitiesOpt.ifPresent(quantities -> { 
			quantities.forEach((key, value) -> {
				if (StringUtils.equalsIgnoreCase(SELECTED, value)) {
					filterBoolQuery.must(
						nestedQuery("quantity", 
							boolQuery().must(buildRange("quantity.quantity", key))
						)
					);
				}
			});
		});
		
		Optional<Map<String, String>> pricesOpt = Optional.ofNullable(filters.getPrices());
		pricesOpt.ifPresent(prices -> { 
			prices.forEach((key, value) -> {
				if (StringUtils.equalsIgnoreCase(SELECTED, value)) {
					filterBoolQuery.must(buildRange("latestPrice", key));
				}
			});
		});
		
		Optional<Map<String, String>> ratingsOpt = Optional.ofNullable(filters.getReviews());
		ratingsOpt.ifPresent(ratings -> { 
			ratings.forEach((key, value) -> {
				if (StringUtils.equalsIgnoreCase(SELECTED, value)) {
					filterBoolQuery.must(buildRange("rating", key));
				}
			});
		});
		
		NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder();
		nativeSearchQueryBuilder
			.withQuery(boolQuery.filter(filterBoolQuery))
			.withPageable(criteria.getPageable());
		
		nativeSearchQueryBuilder
			.addAggregation(
				nested("categories").path("categories")
					.subAggregation(terms("names").field("categories.name"))
					.subAggregation(
						nested("childCategories").path("categories.childCategories")
							.subAggregation(terms("names").field("categories.childCategories.name"))
							.subAggregation(
								nested("grandChildCategories").path("categories.childCategories.childCategories")
									.subAggregation(terms("names").field("categories.childCategories.childCategories.name")))
							
					)
			);
		
		RangeBuilder ratingRangeBuilder = range("ratingRanges").field("rating");
		ratingRangeBuilder.addUnboundedTo(1.0);
		ratingRangeBuilder.addRange(1.0, 2.0);
		ratingRangeBuilder.addRange(2.0, 3.0);
		ratingRangeBuilder.addRange(3.0, 4.0);
		ratingRangeBuilder.addRange(4.0, 5.0);
		nativeSearchQueryBuilder.addAggregation(ratingRangeBuilder);
		
		if (criteria.isCnsmrPrdt()) {
			RangeBuilder priceRangeBuilder = range("priceRanges").field("latestPrice");
			priceRangeBuilder.addUnboundedTo(100.0);
			priceRangeBuilder.addRange(100.0, 200.0);
			priceRangeBuilder.addRange(200.0, 300.0);
			priceRangeBuilder.addRange(300.0, 400.0);
			priceRangeBuilder.addRange(400.0, 500.0);
			priceRangeBuilder.addUnboundedFrom(500.0);
			nativeSearchQueryBuilder.addAggregation(priceRangeBuilder);
		} else if (!criteria.isCnsmrPrdt()) {
			NestedBuilder userTypeBuilder = nested("ownerType").path("owner");
			userTypeBuilder.subAggregation(terms("type").field("owner.userType"));
			nativeSearchQueryBuilder.addAggregation(userTypeBuilder);
			
			RangeBuilder priceRangeBuilder = range("priceRanges").field("latestPrice");
			priceRangeBuilder.addUnboundedTo(1000.0);
			priceRangeBuilder.addRange(1000.0, 2000.0);
			priceRangeBuilder.addRange(2000.0, 3000.0);
			priceRangeBuilder.addRange(3000.0, 4000.0);
			priceRangeBuilder.addRange(4000.0, 5000.0);
			priceRangeBuilder.addUnboundedFrom(5000.0);
			nativeSearchQueryBuilder.addAggregation(priceRangeBuilder);
			
			RangeBuilder quantityRangeBuilder = range("quantityRanges").field("quantity.quantity");
			quantityRangeBuilder.addUnboundedTo(200.0);
			quantityRangeBuilder.addRange(200.0, 400.0);
			quantityRangeBuilder.addRange(400.0, 600.0);
			quantityRangeBuilder.addRange(600.0, 800.0);
			quantityRangeBuilder.addRange(800.0, 1000.0);
			quantityRangeBuilder.addUnboundedFrom(1000.0);
			
			nativeSearchQueryBuilder.addAggregation(nested("quantity").path("quantity").subAggregation(quantityRangeBuilder));
		}
		
		Document annotation = ProductDoc.class.getAnnotation(Document.class);
		SearchQuery searchQuery = nativeSearchQueryBuilder
				.withIndices(annotation.indexName())
				.withTypes(annotation.type())
				.build();
		SearchResponse response = elasticsearchTemplate.query(searchQuery, new ResultsExtractor<SearchResponse>() {
			@Override
			public SearchResponse extract(SearchResponse response) {
				return response;
			}
		});
		
		SearchHits hits = response.getHits();
		Long totalHits = hits.getTotalHits();
		
		List<Long> productIds = new ArrayList<>();
		for (SearchHit hit: hits) {
			productIds.add(NumberUtils.toLong(hit.getId()));
		}
		
		Aggregations aggregations = response.getAggregations();
		Nested ownerTypesAggs = aggregations.get("ownerType");
		Terms ownerType = ownerTypesAggs.getAggregations().get("type");
		Nested categoriesAggs = aggregations.get("categories");
		Terms categories = categoriesAggs.getAggregations().get("names");
		Nested quantity = aggregations.get("quantity");
		Range quantityRanges = quantity.getAggregations().get("quantityRanges");
		Range priceRanges = aggregations.get("priceRanges");
		Range ratingRanges = aggregations.get("ratingRanges");
		
		LOGGER.info("Sellers Aggregations: ");
		List<TermsBucket> sellerBuckets = new ArrayList<>();
		for (Terms.Bucket entry: ownerType.getBuckets()) {
			sellerBuckets.add(new TermsBucket(entry.getKeyAsString(), entry.getDocCount()));
		}

		LOGGER.info("Categories Aggregations: ");
		List<TermsBucket> categoryBuckets = new ArrayList<>();
		for (Terms.Bucket entry: categories.getBuckets()) {
			categoryBuckets.add(new TermsBucket(entry.getKeyAsString(), entry.getDocCount()));
		}
		
		LOGGER.info("Quantity Aggregations: ");
		List<RangeBucket> quantityBuckets = new ArrayList<>();
		for (Range.Bucket entry: quantityRanges.getBuckets()) {
			addRangeBucket(quantityBuckets, entry);
		}
		
		LOGGER.info("Price Aggregations: ");
		List<RangeBucket> priceBuckets = new ArrayList<>();
		for (Range.Bucket entry: priceRanges.getBuckets()) {
			addRangeBucket(priceBuckets, entry);
		}
		
		LOGGER.info("Rating Aggregations: ");
		List<RangeBucket> ratingBuckets = new ArrayList<>();
		for (Range.Bucket entry: ratingRanges.getBuckets()) {
			addRangeBucket(ratingBuckets, entry);
		}
		
		SearchResult.Builder<Long> resultBuilder = new SearchResult.Builder<Long>();
		resultBuilder.results(productIds);
		resultBuilder.sellerBuckets(sellerBuckets);
		resultBuilder.categoryBuckets(categoryBuckets);
		resultBuilder.quantityBuckets(quantityBuckets);
		resultBuilder.priceBuckets(priceBuckets);
		resultBuilder.ratingBuckets(ratingBuckets);
		resultBuilder.totalHits(totalHits);
		return resultBuilder.build();
	}
	
}
