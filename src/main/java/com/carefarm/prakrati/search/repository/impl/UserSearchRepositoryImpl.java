package com.carefarm.prakrati.search.repository.impl;

import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchQuery;
import static org.elasticsearch.index.query.QueryBuilders.multiMatchQuery;
import static org.elasticsearch.index.query.QueryBuilders.nestedQuery;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.search.SearchHits;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ResultsExtractor;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;

import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.document.UserDoc;
import com.carefarm.prakrati.search.repository.custom.UserSearchRepositoryCustom;
import com.carefarm.prakrati.util.UserType;

public class UserSearchRepositoryImpl extends PrakratiSearchRepositoryCustomImpl<UserDoc, Long> implements UserSearchRepositoryCustom {

	@Override
	public SearchResult<Long> findByKeywordContains(String keyword,
			UserType userType, Pageable pageable) {
		BoolQueryBuilder boolQuery = boolQuery();
		boolQuery.filter(boolQuery()
				.must(multiMatchQuery(false, "archived", "mocked"))
				.must(matchQuery("userType", userType)));
		
		if (StringUtils.isNotEmpty(keyword)) {
			boolQuery.should(multiMatchQuery(keyword, "firstName", "lastName"));
			boolQuery.should(nestedQuery("address", multiMatchQuery(keyword, "address.city", "address.state", "address.country")));
			boolQuery.minimumNumberShouldMatch(1);
		}
		
		return executeAndExtractResult(pageable, boolQuery);
	}

	@Override
	public SearchResult<Long> findByKeywordContains(String keyword, String companyId,
			UserType userType, Pageable pageable) {
		BoolQueryBuilder boolQuery = boolQuery();
		boolQuery.must(multiMatchQuery(false, "archived", "mocked"));
		boolQuery.must(matchQuery("userType", userType));
		boolQuery.must(nestedQuery("company", matchQuery("company.uniqueId", companyId)));
		
		if (StringUtils.isNotEmpty(keyword)) {
			boolQuery.should(multiMatchQuery(keyword, "firstName", "lastName"));
			boolQuery.should(nestedQuery("company", multiMatchQuery(keyword, "company.name", "company.registrNo")));
			boolQuery.should(nestedQuery("address", multiMatchQuery(keyword, "address.city", "address.state", "address.country")));
			boolQuery.minimumNumberShouldMatch(1);
		}
		
		return executeAndExtractResult(pageable, boolQuery);
	}

	private SearchResult<Long> executeAndExtractResult(Pageable pageable,
			BoolQueryBuilder boolQuery) {
		NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder();
		SearchQuery searchQuery = nativeSearchQueryBuilder.withQuery(boolQuery).withPageable(pageable).build();
		
		SearchResponse response = elasticsearchTemplate.query(searchQuery, new ResultsExtractor<SearchResponse>() {
			@Override
			public SearchResponse extract(SearchResponse response) {
				return response;
			}
		});
		
		SearchHits hits = response.getHits();
		Long totalHits = hits.getTotalHits();
		
		List<Long> results = new ArrayList<>();
		hits.forEach(hit -> results.add(NumberUtils.toLong(hit.getId())));
		
		SearchResult.Builder<Long> resultBuilder = new SearchResult.Builder<Long>();
		resultBuilder.results(results);
		resultBuilder.totalHits(totalHits);
		return resultBuilder.build();
	}

}
