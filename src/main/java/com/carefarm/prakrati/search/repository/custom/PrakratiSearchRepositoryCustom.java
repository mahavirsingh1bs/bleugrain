package com.carefarm.prakrati.search.repository.custom;

import java.io.Serializable;

import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface PrakratiSearchRepositoryCustom<T, ID extends Serializable> {
	
}
