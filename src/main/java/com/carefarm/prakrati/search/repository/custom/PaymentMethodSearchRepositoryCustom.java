package com.carefarm.prakrati.search.repository.custom;

import org.springframework.data.domain.Pageable;

import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.document.PaymentMethodDoc;
import com.carefarm.prakrati.web.bean.SearchRequest;

public interface PaymentMethodSearchRepositoryCustom extends PrakratiSearchRepositoryCustom<PaymentMethodDoc, Long> {
	SearchResult<Long> findByKeywordContains(SearchRequest request, Pageable pageable);
} 