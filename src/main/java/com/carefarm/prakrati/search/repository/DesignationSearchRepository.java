package com.carefarm.prakrati.search.repository;

import com.carefarm.prakrati.search.document.DesignationDoc;
import com.carefarm.prakrati.search.repository.custom.DesignationSearchRepositoryCustom;

public interface DesignationSearchRepository extends PrakratiSearchRepository<DesignationDoc, Long>, DesignationSearchRepositoryCustom {

}
