package com.carefarm.prakrati.search.repository;

import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import com.carefarm.prakrati.search.document.CompanyDoc;
import com.carefarm.prakrati.search.repository.custom.CompanySearchRepositoryCustom;

public interface CompanySearchRepository extends ElasticsearchRepository<CompanyDoc, Long>, CompanySearchRepositoryCustom {

}
