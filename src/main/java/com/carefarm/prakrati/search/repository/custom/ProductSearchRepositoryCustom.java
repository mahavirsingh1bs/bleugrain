package com.carefarm.prakrati.search.repository.custom;

import org.springframework.data.domain.Pageable;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.document.ProductDoc;
import com.carefarm.prakrati.search.dto.SearchCriteria;

public interface ProductSearchRepositoryCustom extends PrakratiSearchRepositoryCustom<ProductDoc, Long> {
	SearchResult<Long> findByKeywordContains(String keywords, String productId, User currentUser, Pageable pageable);
	SearchResult<Long> findByKeywordContains(SearchCriteria criteria);
} 