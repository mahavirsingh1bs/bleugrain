package com.carefarm.prakrati.search.repository.custom;

import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.document.DemandDoc;
import com.carefarm.prakrati.search.dto.SearchCriteria;

public interface DemandSearchRepositoryCustom extends PrakratiSearchRepositoryCustom<DemandDoc, Long> {
	SearchResult<Long> findByKeywordContains(SearchCriteria criteria);
}