package com.carefarm.prakrati.search.repository.impl;

import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchQuery;
import static org.elasticsearch.index.query.QueryBuilders.multiMatchQuery;
import static org.elasticsearch.index.query.QueryBuilders.nestedQuery;

import java.util.ArrayList;
import java.util.List;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.document.OrderDoc;
import com.carefarm.prakrati.search.dto.SearchCriteria;
import com.carefarm.prakrati.search.repository.custom.OrderSearchRepositoryCustom;

public class OrderSearchRepositoryImpl extends PrakratiSearchRepositoryCustomImpl<OrderDoc, Long> implements
		OrderSearchRepositoryCustom {

	@Override
	public SearchResult<Long> findByKeywordContains(SearchCriteria criteria) {
		BoolQueryBuilder boolQuery = boolQuery();
			
		if (Predicates.notNull.test(criteria.getKeywords())) {
			boolQuery.should(multiMatchQuery(criteria.getKeywords(), "name", "tags"));
			boolQuery.should(
				nestedQuery("categories", boolQuery().must(matchQuery("categories.name", criteria.getKeywords()))));
			boolQuery.minimumNumberShouldMatch(1);
		} else {
			boolQuery.must(
				nestedQuery("categories", 
					boolQuery().must(
						matchQuery("categories.uniqueId", criteria.getUniqueId())
					)
				)
			);
		}
		
		if (Predicates.notNull.test(criteria.getCurrentUser())) {
			boolQuery.mustNot(matchQuery("owner.userNo", criteria.getCurrentUser().getUserNo()));
		}
		
		BoolQueryBuilder filterBoolQuery = boolQuery()
			.must(multiMatchQuery(false, "archived", "mocked"))
			.must(matchQuery("consumerProduct", criteria.isCnsmrPrdt()))
			.mustNot(matchQuery("status", "SOLD_OUT"))
			.mustNot(matchQuery("status", "DELETED"));
		
		NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder();
		SearchQuery searchQuery = nativeSearchQueryBuilder
				.withQuery(boolQuery.filter(filterBoolQuery))
				.withPageable(criteria.getPageable())
				.build();
		
		long totalHits = elasticsearchTemplate.count(searchQuery, OrderDoc.class);
		List<OrderDoc> hits = elasticsearchTemplate.queryForList(searchQuery, OrderDoc.class);
		
		List<Long> orderIds = new ArrayList<>();
		for (OrderDoc hit: hits) {
			orderIds.add(hit.getId());
		}
				
		SearchResult.Builder<Long> resultBuilder = new SearchResult.Builder<Long>();
		resultBuilder.results(orderIds);
		resultBuilder.totalHits(totalHits);
		return resultBuilder.build();
	}
	
}
