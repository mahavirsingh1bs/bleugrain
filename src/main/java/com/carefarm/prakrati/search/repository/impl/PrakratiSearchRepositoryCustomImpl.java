package com.carefarm.prakrati.search.repository.impl;

import static com.carefarm.prakrati.web.util.PrakratiConstants.ASTERIK;
import static com.carefarm.prakrati.web.util.PrakratiConstants.FIRST;
import static com.carefarm.prakrati.web.util.PrakratiConstants.HYPHEN;
import static com.carefarm.prakrati.web.util.PrakratiConstants.SECOND;
import static org.elasticsearch.index.query.QueryBuilders.rangeQuery;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.RangeQueryBuilder;
import org.elasticsearch.search.aggregations.bucket.range.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.data.repository.NoRepositoryBean;

import com.carefarm.prakrati.search.bean.RangeBucket;
import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.bean.RangeBucket.Type;
import com.carefarm.prakrati.search.document.AbstractDoc;
import com.carefarm.prakrati.search.repository.custom.PrakratiSearchRepositoryCustom;

@NoRepositoryBean
public abstract class PrakratiSearchRepositoryCustomImpl<T, ID extends Serializable> implements
		PrakratiSearchRepositoryCustom<T, ID> {
	
	@Autowired
	protected ElasticsearchTemplate elasticsearchTemplate;
	
	protected SearchResult<Long> findByKeywordContainsInternal(BoolQueryBuilder boolQuery, Pageable pageable, Class<? extends AbstractDoc> clazz) {
		NativeSearchQueryBuilder nativeSearchQueryBuilder = new NativeSearchQueryBuilder();
		SearchQuery searchQuery = nativeSearchQueryBuilder.withQuery(boolQuery).withPageable(pageable).build();
		long totalHits = elasticsearchTemplate.count(searchQuery, clazz);
		List<? extends AbstractDoc> hits = elasticsearchTemplate.queryForList(searchQuery, clazz);
		List<Long> results = new ArrayList<>();
		hits.forEach(hit -> results.add(hit.getId()));
		
		SearchResult.Builder<Long> resultBuilder = new SearchResult.Builder<Long>();
		resultBuilder.results(results);
		resultBuilder.totalHits(totalHits);
		return resultBuilder.build();
	}
	
	protected RangeQueryBuilder buildRange(String field, String key) {
		String[] tokens = key.split(HYPHEN);
		String from = tokens[FIRST]; String to = tokens[SECOND];
		if (StringUtils.equalsIgnoreCase(from, ASTERIK)) {
			return rangeQuery(field).lt(NumberUtils.toDouble(to));
		} else if (StringUtils.equalsIgnoreCase(to, ASTERIK)) {
			return rangeQuery(field).gte(NumberUtils.toDouble(from));
		} else {
			return rangeQuery(field).gte(NumberUtils.toDouble(from)).lt(NumberUtils.toDouble(to));
		}
	}

	protected void addRangeBucket(List<RangeBucket> priceBuckets,
			Range.Bucket entry) {
		String key = entry.getKeyAsString();
		Number from = (Number) entry.getFrom();
		Number to = (Number) entry.getTo();
		long docCount = entry.getDocCount();
		addRangeBucketInternal(priceBuckets, entry, key, from, to, docCount);
	}

	protected void addRangeBucketInternal(List<RangeBucket> priceBuckets,
			Range.Bucket entry, String key, Number from, Number to,
			long docCount) {
		if (!NumberUtils.isNumber(entry.getFromAsString())) {
			priceBuckets.add(new RangeBucket(key, to, docCount, Type.TO));
		} else if (!NumberUtils.isNumber(entry.getToAsString())) {
			priceBuckets.add(new RangeBucket(key, from, docCount, Type.FROM));
		} else {
			priceBuckets.add(new RangeBucket(key, from, to, docCount));
		}
	}

	
}
