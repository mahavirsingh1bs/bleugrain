package com.carefarm.prakrati.search.repository.custom;

import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.document.OrderDoc;
import com.carefarm.prakrati.search.dto.SearchCriteria;

public interface OrderSearchRepositoryCustom extends PrakratiSearchRepositoryCustom<OrderDoc, Long> {
	SearchResult<Long> findByKeywordContains(SearchCriteria criteria);
} 