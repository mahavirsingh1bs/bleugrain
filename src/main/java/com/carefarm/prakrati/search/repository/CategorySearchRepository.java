package com.carefarm.prakrati.search.repository;

import com.carefarm.prakrati.search.document.CategoryDoc;
import com.carefarm.prakrati.search.repository.custom.CategorySearchRepositoryCustom;

public interface CategorySearchRepository extends PrakratiSearchRepository<CategoryDoc, Long>, CategorySearchRepositoryCustom {

}
