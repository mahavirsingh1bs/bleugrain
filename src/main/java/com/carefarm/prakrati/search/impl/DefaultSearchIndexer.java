/*package com.carefarm.prakrati.search.impl;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.search.jpa.FullTextEntityManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.search.SearchIndexer;

@Component
public class DefaultSearchIndexer implements SearchIndexer {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultSearchIndexer.class);
	
	@PersistenceContext
	private EntityManager entityManager;

	public void createIndexes() {
		try {
			FullTextEntityManager fullTextEntityManager = 
	        		org.hibernate.search.jpa.Search.getFullTextEntityManager(entityManager);
	        fullTextEntityManager.createIndexer().startAndWait();
        } catch (InterruptedException ex) {
        	LOGGER.error("Got An Exception while doing indexing " + ex);
        }
	}
}
*/