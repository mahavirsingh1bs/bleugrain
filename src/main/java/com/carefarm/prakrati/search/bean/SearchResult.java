package com.carefarm.prakrati.search.bean;

import java.util.List;

import lombok.Getter;

@Getter
public class SearchResult<T> {
	private long totalHits;
	private List<T> results;
	private List<TermsBucket> sellerBuckets;
	private List<TermsBucket> buyerBuckets;
	private List<TermsBucket> categoryBuckets;
	private List<RangeBucket> quantityBuckets;
	private List<RangeBucket> priceBuckets;
	private List<RangeBucket> ratingBuckets;

	public static class Builder<T> {
		private long totalHits;
		private List<T> results;
		private List<TermsBucket> sellerBuckets;
		private List<TermsBucket> buyerBuckets;
		private List<TermsBucket> categoryBuckets;
		private List<RangeBucket> quantityBuckets;
		private List<RangeBucket> priceBuckets;
		private List<RangeBucket> ratingBuckets;
		
		public Builder<T> totalHits(long totalHits) {
			this.totalHits = totalHits;
			return this;
		}

		public Builder<T> results(List<T> results) {
			this.results = results;
			return this;
		}

		public Builder<T> buyerBuckets(List<TermsBucket> buyerBuckets) {
			this.buyerBuckets = buyerBuckets;
			return this;
		}
		
		public Builder<T> sellerBuckets(List<TermsBucket> sellerBuckets) {
			this.sellerBuckets = sellerBuckets;
			return this;
		}

		public Builder<T> categoryBuckets(List<TermsBucket> categoryBuckets) {
			this.categoryBuckets = categoryBuckets;
			return this;
		}

		public Builder<T> quantityBuckets(List<RangeBucket> quantityBuckets) {
			this.quantityBuckets = quantityBuckets;
			return this;
		}

		public Builder<T> priceBuckets(List<RangeBucket> priceBuckets) {
			this.priceBuckets = priceBuckets;
			return this;
		}

		public Builder<T> ratingBuckets(List<RangeBucket> ratingBuckets) {
			this.ratingBuckets = ratingBuckets;
			return this;
		}

		public SearchResult<T> build() {
			return new SearchResult<T>(this);
		}
	}

	private SearchResult(Builder<T> builder) {
		this.totalHits = builder.totalHits;
		this.results = builder.results;
		this.sellerBuckets = builder.sellerBuckets;
		this.buyerBuckets = builder.buyerBuckets;
		this.categoryBuckets = builder.categoryBuckets;
		this.quantityBuckets = builder.quantityBuckets;
		this.priceBuckets = builder.priceBuckets;
		this.ratingBuckets = builder.ratingBuckets;
	}
}
