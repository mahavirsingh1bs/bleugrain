package com.carefarm.prakrati.search.bean;

import lombok.Getter;

@Getter
public class RangeBucket {
	private String name;
	private Number from;
	private Number to;
	private long count;
	
	public RangeBucket() { }

	public RangeBucket(String name, Number value, long count, Type type) {
		this.name = name;
		if (type == Type.FROM) {
			this.from = value;
		} else {
			this.to = value;
		}
		this.count = count;
	}
	
	public RangeBucket(String name, Number from, Number to, long count) {
		this.name = name;
		this.from = from;
		this.to = to;
		this.count = count;
	}
	
	@Override
	public String toString() {
		return "RangeBucket [name=" + name + ", from=" + from + ", to=" + to
				+ ", count=" + count + "]";
	}
	
	public static enum Type {
		FROM, TO;
	}
}
