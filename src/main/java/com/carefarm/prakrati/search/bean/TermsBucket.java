package com.carefarm.prakrati.search.bean;

import lombok.Getter;

@Getter
public class TermsBucket {
	private String name;
	private long count;
	
	public TermsBucket() {}
	
	public TermsBucket(String name, long count) {
		this.name = name;
		this.count = count;
	}

	@Override
	public String toString() {
		return "TermsBucket [name=" + name + ", count=" + count + "]";
	}
	
}
