package com.carefarm.prakrati.search.dto;

public enum SearchStatus {
	FOUND, NOT_FOUND
}
