package com.carefarm.prakrati.search.dto;

import lombok.Getter;
import lombok.Setter;

import org.springframework.data.domain.Pageable;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.web.bean.FiltersBean;

@Getter
@Setter
public class SearchCriteria {
	private String uniqueId;
	private String keywords;
	private boolean cnsmrPrdt;
	private FiltersBean filters;
	private User currentUser;
	private Pageable pageable;
		
}
