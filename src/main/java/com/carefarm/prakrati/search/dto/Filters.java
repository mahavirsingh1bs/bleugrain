package com.carefarm.prakrati.search.dto;

import java.util.List;

import lombok.Getter;

import com.carefarm.prakrati.search.bean.RangeBucket;
import com.carefarm.prakrati.search.bean.TermsBucket;

@Getter
public class Filters {
	private List<TermsBucket> sellers;
	private List<TermsBucket> buyers;
	private List<TermsBucket> categories;
	private List<RangeBucket> quantities;
	private List<RangeBucket> prices;
	private List<RangeBucket> reviews;

	public static class Builder {
		private List<TermsBucket> sellers;
		private List<TermsBucket> buyers;
		private List<TermsBucket> categories;
		private List<RangeBucket> quantities;
		private List<RangeBucket> prices;
		private List<RangeBucket> reviews;

		public static Builder getInstance() {
			return new Builder();
		}
		
		public Builder sellers(List<TermsBucket> sellers) {
			this.sellers = sellers;
			return this;
		}

		public Builder buyers(List<TermsBucket> buyers) {
			this.buyers = buyers;
			return this;
		}

		public Builder categories(List<TermsBucket> categories) {
			this.categories = categories;
			return this;
		}

		public Builder quantities(List<RangeBucket> quantities) {
			this.quantities = quantities;
			return this;
		}

		public Builder prices(List<RangeBucket> prices) {
			this.prices = prices;
			return this;
		}

		public Builder reviews(List<RangeBucket> reviews) {
			this.reviews = reviews;
			return this;
		}

		public Filters build() {
			return new Filters(this);
		}
	}

	private Filters(Builder builder) {
		this.sellers = builder.sellers;
		this.buyers = builder.buyers;
		this.categories = builder.categories;
		this.quantities = builder.quantities;
		this.prices = builder.prices;
		this.reviews = builder.reviews;
	}
}
