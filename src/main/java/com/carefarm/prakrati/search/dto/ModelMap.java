package com.carefarm.prakrati.search.dto;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ModelMap<T> {
	private SearchStatus status;
	private String message;
	private String lastKeyword;
	private int lastPage;
	private int lastSize;
	private Filters filters;
	private long resultSize;
	private List<T> results;
		
	public boolean isEmptyResult() {
		return this.results.isEmpty();
	}
	
	public T getFirstResult() {
		if (!this.results.isEmpty()) {
			return this.results.get(0);
		}
		return null;
	}
}
