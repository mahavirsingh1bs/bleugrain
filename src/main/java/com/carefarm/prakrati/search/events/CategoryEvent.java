package com.carefarm.prakrati.search.events;

import com.carefarm.prakrati.entity.Category;

public class CategoryEvent extends PrakratiEvent<Iterable<Category>> {
	
	private static final long serialVersionUID = 5502743954361337391L;

	public CategoryEvent(Iterable<Category> source, EventType eventType) {
		super(source, eventType);
	}

}
