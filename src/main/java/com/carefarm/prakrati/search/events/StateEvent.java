package com.carefarm.prakrati.search.events;

import com.carefarm.prakrati.common.entity.State;

public class StateEvent extends PrakratiEvent<Iterable<State>> {
	
	private static final long serialVersionUID = 5502743954361337391L;

	public StateEvent(Iterable<State> source, EventType eventType) {
		super(source, eventType);
	}

}
