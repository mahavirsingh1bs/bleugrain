package com.carefarm.prakrati.search.events;

import com.carefarm.prakrati.entity.PaymentCard;

public class PaymentCardEvent extends PrakratiEvent<Iterable<PaymentCard>> {
	
	private static final long serialVersionUID = 5502743954361337391L;

	public PaymentCardEvent(Iterable<PaymentCard> source, EventType eventType) {
		super(source, eventType);
	}

}
