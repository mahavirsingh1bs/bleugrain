package com.carefarm.prakrati.search.events;

import com.carefarm.prakrati.entity.User;

public class UserEvent extends PrakratiEvent<Iterable<User>> {
	
	private static final long serialVersionUID = 5502743954361337391L;

	public UserEvent(Iterable<User> source, EventType eventType) {
		super(source, eventType);
	}

}
