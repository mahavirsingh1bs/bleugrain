package com.carefarm.prakrati.search.events;

import com.carefarm.prakrati.entity.Menuitem;

public class MenuitemEvent extends PrakratiEvent<Iterable<Menuitem>> {
	
	private static final long serialVersionUID = 5502743954361337391L;

	public MenuitemEvent(Iterable<Menuitem> source, EventType eventType) {
		super(source, eventType);
	}

}
