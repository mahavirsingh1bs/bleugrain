package com.carefarm.prakrati.search.events;

import com.carefarm.prakrati.entity.Bank;

public class BankEvent extends PrakratiEvent<Iterable<Bank>> {
	private static final long serialVersionUID = -1826512266267805933L;

	public BankEvent(Iterable<Bank> source, EventType eventType) {
		super(source, eventType);
	}

}
