package com.carefarm.prakrati.search.events;

import com.carefarm.prakrati.entity.Designation;

public class DesignationEvent extends PrakratiEvent<Iterable<Designation>> {
	
	private static final long serialVersionUID = 5502743954361337391L;

	public DesignationEvent(Iterable<Designation> source, EventType eventType) {
		super(source, eventType);
	}

}
