package com.carefarm.prakrati.search.events;

import com.carefarm.prakrati.entity.Company;

public class CompanyEvent extends PrakratiEvent<Iterable<Company>> {
	
	private static final long serialVersionUID = 5502743954361337391L;

	public CompanyEvent(Iterable<Company> source, EventType eventType) {
		super(source, eventType);
	}

}
