package com.carefarm.prakrati.search.events;

import com.carefarm.prakrati.common.entity.Country;

public class CountryEvent extends PrakratiEvent<Iterable<Country>> {
	
	private static final long serialVersionUID = 5502743954361337391L;

	public CountryEvent(Iterable<Country> source, EventType eventType) {
		super(source, eventType);
	}

}
