package com.carefarm.prakrati.search.events;

import org.springframework.context.ApplicationEvent;

public abstract class PrakratiEvent<T> extends ApplicationEvent {
	private static final long serialVersionUID = -7822257528505265895L;
	
	private EventType eventType;
	
	public PrakratiEvent(T source, EventType eventType) {
		super(source);
		this.eventType = eventType;
	}

	@Override
	@SuppressWarnings("unchecked")
	public T getSource() {
		return (T )this.source;
	}
	
	public EventType getEventType() {
		return eventType;
	}

	public void setEventType(EventType eventType) {
		this.eventType = eventType;
	}

	public static enum EventType {
		INSERT, UPDATE, DELETE;
	}
}
