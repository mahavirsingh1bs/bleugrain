package com.carefarm.prakrati.search.events;

import com.carefarm.prakrati.entity.Demand;

public class DemandEvent extends PrakratiEvent<Iterable<Demand>> {
	
	private static final long serialVersionUID = 5502743954361337391L;

	public DemandEvent(Iterable<Demand> source, EventType eventType) {
		super(source, eventType);
	}

}
