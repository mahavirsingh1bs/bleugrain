package com.carefarm.prakrati.search.events;

import com.carefarm.prakrati.delivery.entity.Transport;

public class TransportEvent extends PrakratiEvent<Iterable<Transport>> {
	
	private static final long serialVersionUID = 5502743954361337391L;

	public TransportEvent(Iterable<Transport> source, EventType eventType) {
		super(source, eventType);
	}

}
