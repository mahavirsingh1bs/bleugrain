package com.carefarm.prakrati.search.events;

import com.carefarm.prakrati.entity.Order;

public class OrderEvent extends PrakratiEvent<Iterable<Order>> {
	private static final long serialVersionUID = -9204016054403642709L;

	public OrderEvent(Iterable<Order> source, EventType eventType) {
		super(source, eventType);
	}

}
