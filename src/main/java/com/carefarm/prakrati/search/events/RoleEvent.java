package com.carefarm.prakrati.search.events;

import com.carefarm.prakrati.entity.Role;

public class RoleEvent extends PrakratiEvent<Iterable<Role>> {
	
	private static final long serialVersionUID = 5502743954361337391L;

	public RoleEvent(Iterable<Role> source, EventType eventType) {
		super(source, eventType);
	}

}
