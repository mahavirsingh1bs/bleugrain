package com.carefarm.prakrati.search.events;

import com.carefarm.prakrati.entity.Department;

public class DepartmentEvent extends PrakratiEvent<Iterable<Department>> {
	
	private static final long serialVersionUID = 5502743954361337391L;

	public DepartmentEvent(Iterable<Department> source, EventType eventType) {
		super(source, eventType);
	}

}
