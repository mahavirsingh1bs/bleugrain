package com.carefarm.prakrati.search.document;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.elasticsearch.annotations.Document;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.search.document.Shared.Address;
import com.carefarm.prakrati.search.document.Shared.Bid;
import com.carefarm.prakrati.search.document.Shared.Category;
import com.carefarm.prakrati.search.document.Shared.Currency;
import com.carefarm.prakrati.search.document.Shared.Quantity;
import com.carefarm.prakrati.search.document.Shared.Unit;
import com.carefarm.prakrati.search.document.Shared.User;

@Document(indexName = "bleugrain", type = "products", shards = 5, replicas = 3)
public class ProductDoc extends AbstractDoc implements Categorizable, Tagable {
	private String name;
	private String shortDesc;
	private String desc;
	private Quantity quantity;
	private String status;
	private boolean mocked;
	private User owner;
	private Address address;
	private List<Category> categories;
	private List<String> tags;
	private BigDecimal minimumPrice;
	private BigDecimal maximumPrice;
	private BigDecimal latestPrice;
	private BigDecimal lastPrice;
	private Bid highestBid;
	private boolean bidEnabled;
	private Date enableTillDate;
	private BigDecimal initialBidAmount;
	private Currency biddingCurrency;
	private Unit biddingUnit;
	private BigDecimal rating;
	private boolean consumerProduct;
	private User assignedTo;
	private User assignedBy;
	private Date dateAssigned;
	private String comments;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShortDesc() {
		return shortDesc;
	}

	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public Quantity getQuantity() {
		return quantity;
	}

	public void setQuantity(Quantity quantity) {
		this.quantity = quantity;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public User getOwner() {
		return owner;
	}

	public void setOwner(User owner) {
		this.owner = owner;
	}

	public boolean isMocked() {
		return mocked;
	}

	public void setMocked(boolean mocked) {
		this.mocked = mocked;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}
	
	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	@Override
	public void addCategory(Category category) {
		if (Predicates.isNull.test(this.categories)) {
			this.categories = new ArrayList<>();
		}
		this.categories.add(category);
	}

	public void addCategory(Category category, String uniqueId) {
		this.categories.forEach(cat -> cat.addChildCategory(category, uniqueId));
	}
	
	public Optional<Category> contains(String uniqueId) {
		if (Predicates.isNull.test(this.categories)) {
			return null;
		}
		
		for (Category category: this.categories) {
			if (StringUtils.equalsIgnoreCase(category.getUniqueId(), uniqueId)) {
				return Optional.of(category);
			}
		}
		return Optional.empty();
	}

	public boolean isExists(String uniqueId) {
		if (Predicates.isNull.test(this.categories)) {
			return false;
		}
		
		for (Shared.Category category: this.categories) {
			if (category.isExists(uniqueId)) {
				return true;
			}
		}
		
		return false;
	}
	
	public List<String> getTags() {
		return tags;
	}

	public void setTags(List<String> tags) {
		this.tags = tags;
	}

	@Override
	public void addTag(String tag) {
		if (Predicates.isNull.test(this.tags)) {
			this.tags = new ArrayList<>();
		}
		this.tags.add(tag);
	}
	
	public BigDecimal getMinimumPrice() {
		return minimumPrice;
	}

	public void setMinimumPrice(BigDecimal minimumPrice) {
		this.minimumPrice = minimumPrice;
	}

	public BigDecimal getMaximumPrice() {
		return maximumPrice;
	}

	public void setMaximumPrice(BigDecimal maximumPrice) {
		this.maximumPrice = maximumPrice;
	}

	public BigDecimal getLatestPrice() {
		return latestPrice;
	}

	public void setLatestPrice(BigDecimal latestPrice) {
		this.latestPrice = latestPrice;
	}

	public BigDecimal getLastPrice() {
		return lastPrice;
	}

	public void setLastPrice(BigDecimal lastPrice) {
		this.lastPrice = lastPrice;
	}

	public Bid getHighestBid() {
		return highestBid;
	}

	public void setHighestBid(Bid highestBid) {
		this.highestBid = highestBid;
	}

	public boolean isBidEnabled() {
		return bidEnabled;
	}

	public void setBidEnabled(boolean bidEnabled) {
		this.bidEnabled = bidEnabled;
	}

	public Date getEnableTillDate() {
		return enableTillDate;
	}

	public void setEnableTillDate(Date enableTillDate) {
		this.enableTillDate = enableTillDate;
	}

	public BigDecimal getInitialBidAmount() {
		return initialBidAmount;
	}

	public void setInitialBidAmount(BigDecimal initialBidAmount) {
		this.initialBidAmount = initialBidAmount;
	}

	public Currency getBiddingCurrency() {
		return biddingCurrency;
	}

	public void setBiddingCurrency(Currency biddingCurrency) {
		this.biddingCurrency = biddingCurrency;
	}

	public Unit getBiddingUnit() {
		return biddingUnit;
	}

	public void setBiddingUnit(Unit biddingUnit) {
		this.biddingUnit = biddingUnit;
	}

	public BigDecimal getRating() {
		return rating;
	}

	public void setRating(BigDecimal rating) {
		this.rating = rating;
	}

	public boolean isConsumerProduct() {
		return consumerProduct;
	}

	public void setConsumerProduct(boolean consumerProduct) {
		this.consumerProduct = consumerProduct;
	}

	public User getAssignedTo() {
		return assignedTo;
	}

	public void setAssignedTo(User assignedTo) {
		this.assignedTo = assignedTo;
	}

	public User getAssignedBy() {
		return assignedBy;
	}

	public void setAssignedBy(User assignedBy) {
		this.assignedBy = assignedBy;
	}

	public Date getDateAssigned() {
		return dateAssigned;
	}

	public void setDateAssigned(Date dateAssigned) {
		this.dateAssigned = dateAssigned;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	@Override
	public String toString() {
		return "ProductDoc [categories=" + categories + "]";
	}
	
}

