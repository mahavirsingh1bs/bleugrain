package com.carefarm.prakrati.search.document;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import org.springframework.data.elasticsearch.annotations.Document;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.search.document.Shared.Address;
import com.carefarm.prakrati.search.document.Shared.Category;
import com.carefarm.prakrati.search.document.Shared.Currency;
import com.carefarm.prakrati.search.document.Shared.Quantity;
import com.carefarm.prakrati.search.document.Shared.Unit;
import com.carefarm.prakrati.search.document.Shared.User;

@Document(indexName = "bleugrain", type = "demands", shards = 5, replicas = 3)
@Getter
@Setter
public class DemandDoc extends AbstractDoc implements Categorizable, Tagable {
	private String name;
	private String shortDesc;
	private String desc;
	private String buyerCategory;
	private List<Category> categories;
	private List<String> tags;
	private Quantity quantity;
	private BigDecimal minPricePerUnit;
	private BigDecimal maxPricePerUnit;
	private BigDecimal pricePerUnit;
	private Currency currency;
	private Unit unit;
	private Date deliveryDate;
	private Address deliveryAddress;
	private String status;
	private boolean mocked;
	private User user;
	private User assignedTo;
	private User assignedBy;
	private Date dateAssigned;
	private String comments;
	
	@Override
	public void addCategory(Category category) {
		if (Predicates.isNull.test(this.categories)) {
			this.categories = new ArrayList<>();
		}
		this.categories.add(category);
	}
	
	public void addCategory(Category category, String uniqueId) {
		this.categories.forEach(cat -> cat.addChildCategory(category, uniqueId));
	}

	@Override
	public boolean isExists(String uniqueId) {
		if (Predicates.isNull.test(this.categories)) {
			return false;
		}
		
		for (Shared.Category category: this.categories) {
			if (category.isExists(uniqueId)) {
				return true;
			}
		}
		
		return false;
	}
	
	@Override
	public void addTag(String tag) {
		if (Predicates.isNull.test(this.tags)) {
			this.tags = new ArrayList<>();
		}
		this.tags.add(tag);
	}
		
}
