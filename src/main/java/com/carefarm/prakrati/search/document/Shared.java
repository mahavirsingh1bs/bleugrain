package com.carefarm.prakrati.search.document;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import org.apache.commons.lang3.StringUtils;

import com.carefarm.prakrati.constants.Predicates;

public class Shared {

	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
	@EqualsAndHashCode
	public static class Bid {
		private String firstName;
		private String lastName;
		private String company;
		private String unit;
		
	}
	
	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
	@EqualsAndHashCode
	public static class Address {
		private String city;
		private String country;
		private String state;
		private String zipcode;
		
	}

	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
	@EqualsAndHashCode
	public static class Permit {
		private Long id;
		private String name;
	
	}
		
	@Getter
	@Setter
	@NoArgsConstructor
	public static class Category {
		private Long id;
		private String uniqueId;
		private String name;
		private List<Category> childCategories;
		
		public Category(Long id, String uniqueId, String name) {
			this.id = id;
			this.uniqueId = uniqueId;
			this.name = name;
		}

		public void addChildCategory(Category childCategory) {
			if (Predicates.isNull.test(this.childCategories)) {
				this.childCategories = new ArrayList<>();
			}
			this.childCategories.add(childCategory);
		}
		
		public void addChildCategory(Category childCategory, String uniqueId) {
			if (StringUtils.equalsIgnoreCase(this.uniqueId, uniqueId)) {
				addChildCategory(childCategory);
			}
			
			if (Predicates.notNull.test(this.childCategories)) {
				this.childCategories
					.forEach(childCat -> 
						childCat.addChildCategory(childCategory, uniqueId));
			}
		}
		
		public boolean isExists(String uniqueId) {
			if (StringUtils.equalsIgnoreCase(this.uniqueId, uniqueId)) {
				return true;
			}
			
			if (Predicates.isNull.test(this.childCategories)) {
				return false;
			}
			
			for (Shared.Category childCategory: this.childCategories) {
				if (childCategory.isExists(uniqueId)) {
					return true;
				}
			}
			
			return false;
		}

		@Override
		public String toString() {
			return "Category [uniqueId=" + uniqueId + ", name=" + name
					+ ", childCategories=" + childCategories + "]";
		}
		
	}
	
	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
	@EqualsAndHashCode
	public static class Quantity {
		private BigDecimal quantity;
		private Unit unit;
		
	}
	
	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
	@EqualsAndHashCode
	public static class Capacity {
		private BigDecimal capacity;
		private Unit unit;
		
	}
	
	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
	@EqualsAndHashCode
	public static class PricePerUnit {
		private BigDecimal price;
		private Currency currency;
		private Unit unit;
		
	}
	
	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
	@EqualsAndHashCode
	public static class Unit {
		private String name;
		private String symbol;
			
	}
	
	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
	@EqualsAndHashCode
	public static class Currency {
		private String name;
		private String symbol;
			
	}

	@Getter
	@Setter
	@NoArgsConstructor
	@EqualsAndHashCode
	public static class User {
		private Long id;
		private String userType;
		private String userNo;
		private String firstName;
		private String lastName;
			
		public User(String firstName, String lastName) {
			this.firstName = firstName;
			this.lastName = lastName;
		}

	}
	
	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
	@EqualsAndHashCode
	public static class Menuitem {
		private Long id;
		private String uniqueId;
		private String name;	
		
	}
	
	@Getter
	@Setter
	@NoArgsConstructor
	@AllArgsConstructor
	@EqualsAndHashCode
	public static class Company {
		private Long id;
		private String uniqueId;
		private String name;
			
	}
}
