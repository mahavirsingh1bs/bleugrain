package com.carefarm.prakrati.search.document;

import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "bleugrain", type = "countries", shards = 5, replicas = 3)
public class CountryDoc extends AbstractDoc {
	private String code;
	private String name;
	
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
		
}
