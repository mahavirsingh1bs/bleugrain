package com.carefarm.prakrati.search.document;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

import org.springframework.data.annotation.Id;

import com.carefarm.prakrati.search.document.Shared.User;

@Getter
@Setter
public class AbstractDoc {
	@Id
	protected Long id;
	protected int version;
	protected String uniqueId;
	protected boolean archived;
	protected User createdBy;
	protected Date dateCreated;
	protected User modifiedBy;
	protected Date dateModified;
	
}
