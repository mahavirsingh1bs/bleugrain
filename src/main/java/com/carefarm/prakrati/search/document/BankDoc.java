package com.carefarm.prakrati.search.document;

import lombok.Getter;
import lombok.Setter;

import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "bleugrain", type = "banks", shards = 5, replicas = 3)
@Getter
@Setter
public class BankDoc extends AbstractDoc {
	private String name;
	
}
