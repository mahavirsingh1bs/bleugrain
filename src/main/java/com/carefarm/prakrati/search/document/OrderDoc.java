package com.carefarm.prakrati.search.document;

import java.util.Date;

import org.springframework.data.elasticsearch.annotations.Document;

import com.carefarm.prakrati.search.document.Shared.Address;
import com.carefarm.prakrati.search.document.Shared.Company;
import com.carefarm.prakrati.search.document.Shared.User;

@Document(indexName = "bleugrain", type = "orders", shards = 5, replicas = 3)
public class OrderDoc extends AbstractDoc {
	private Date orderDate;
	private User deliverBy;
	private Date deliveryDate;
	private Address billingAddress;
	private Address shippingAddress;
	private String status;
	private boolean mocked;
	private User user;
	private Company company;
	private User assignedBy;
	private User assignedTo;
	private Date dateAssigned;
	private String comments;
	public Date getOrderDate() {
		return orderDate;
	}
	public void setOrderDate(Date orderDate) {
		this.orderDate = orderDate;
	}
	public User getDeliverBy() {
		return deliverBy;
	}
	public void setDeliverBy(User deliverBy) {
		this.deliverBy = deliverBy;
	}
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public Address getBillingAddress() {
		return billingAddress;
	}
	public void setBillingAddress(Address billingAddress) {
		this.billingAddress = billingAddress;
	}
	public Address getShippingAddress() {
		return shippingAddress;
	}
	public void setShippingAddress(Address shippingAddress) {
		this.shippingAddress = shippingAddress;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public boolean isMocked() {
		return mocked;
	}
	public void setMocked(boolean mocked) {
		this.mocked = mocked;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public User getAssignedBy() {
		return assignedBy;
	}
	public void setAssignedBy(User assignedBy) {
		this.assignedBy = assignedBy;
	}
	public User getAssignedTo() {
		return assignedTo;
	}
	public void setAssignedTo(User assignedTo) {
		this.assignedTo = assignedTo;
	}
	public Date getDateAssigned() {
		return dateAssigned;
	}
	public void setDateAssigned(Date dateAssigned) {
		this.dateAssigned = dateAssigned;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
		
}

