package com.carefarm.prakrati.search.document;

import com.carefarm.prakrati.search.document.Shared.Category;

public interface Categorizable {
	void addCategory(Category category);
	
	boolean isExists(String uniqueId);
	
	void addCategory(Category category, String uniqueId);
}
