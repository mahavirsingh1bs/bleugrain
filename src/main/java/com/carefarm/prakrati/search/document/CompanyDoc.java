package com.carefarm.prakrati.search.document;

import org.springframework.data.elasticsearch.annotations.Document;

import com.carefarm.prakrati.search.document.Shared.Address;

@Document(indexName = "bleugrain", type = "companies", shards = 5, replicas = 3)
public class CompanyDoc extends AbstractDoc {
	private String name;
	private String shortDesc;
	private String registrNo;
	private String domain;
	private boolean mocked;
	private boolean verified;
	private Address address;
	private String phoneNo;
	private String emailId;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getShortDesc() {
		return shortDesc;
	}
	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}
	public String getRegistrNo() {
		return registrNo;
	}
	public void setRegistrNo(String registrNo) {
		this.registrNo = registrNo;
	}
	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public boolean isMocked() {
		return mocked;
	}
	public void setMocked(boolean mocked) {
		this.mocked = mocked;
	}
	public boolean isVerified() {
		return verified;
	}
	public void setVerified(boolean verified) {
		this.verified = verified;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}
	
}
