package com.carefarm.prakrati.search.document;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import org.springframework.data.elasticsearch.annotations.Document;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.search.document.Shared.Capacity;
import com.carefarm.prakrati.search.document.Shared.Company;
import com.carefarm.prakrati.search.document.Shared.Currency;
import com.carefarm.prakrati.search.document.Shared.Unit;
import com.carefarm.prakrati.search.document.Shared.User;

@Document(indexName = "bleugrain", type = "transports", shards = 5, replicas = 3)
@Getter
@Setter
public class TransportDoc extends AbstractDoc {
	private String name;
	private String vehicleNo;
	private Capacity capacity;
	private BigDecimal pricePerUnit;
	private Currency currency;
	private Unit unit;
	private List<String> permits;
	private User driver;
	private Company company;
	
	public void addPermit(String permit) {
		if (Predicates.isNull.test(this.permits)) {
			this.permits = new ArrayList<>();
		}
		this.permits.add(permit);
	}
}
