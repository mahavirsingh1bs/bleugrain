package com.carefarm.prakrati.search.document;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.elasticsearch.annotations.Document;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.search.document.Shared.Category;

@Document(indexName = "bleugrain", type = "categories", shards = 5, replicas = 3)
public class CategoryDoc extends AbstractDoc implements Categorizable {
	private String name;
	private String desc;
	private List<Category> parentCategories;
	private boolean active;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDesc() {
		return desc;
	}
	public void setDesc(String desc) {
		this.desc = desc;
	}
	public List<Category> getParentCategories() {
		return parentCategories;
	}
	public void setParentCategories(List<Category> parentCategories) {
		this.parentCategories = parentCategories;
	}
	public void addParentCategory(Category parentCategory) {
		if (Predicates.isNull.test(this.parentCategories)) {
			this.parentCategories = new ArrayList<>();
		}
		this.parentCategories.add(parentCategory);
	}
	@Override
	public void addCategory(Category category) {
		addParentCategory(category);
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	@Override
	public boolean isExists(String uniqueId) {
		// TODO Auto-generated method stub
		return false;
	}
	@Override
	public void addCategory(Category category, String uniqueId) {
		// TODO Auto-generated method stub
		
	}
	
}
