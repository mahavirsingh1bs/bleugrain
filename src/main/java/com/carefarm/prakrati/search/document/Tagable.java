package com.carefarm.prakrati.search.document;

public interface Tagable {
	void addTag(String tag);
}
