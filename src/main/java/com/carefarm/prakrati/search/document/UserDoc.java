package com.carefarm.prakrati.search.document;

import org.springframework.data.elasticsearch.annotations.Document;

import com.carefarm.prakrati.search.document.Shared.Address;
import com.carefarm.prakrati.search.document.Shared.Company;

@Document(indexName = "bleugrain", type = "users", shards = 5, replicas = 3)
public class UserDoc extends AbstractDoc {
	private String userType;
	private String userNo;
	private String firstName;
	private String lastName;
	private String shortDesc;
	private String username;
	private String email;
	private Address address;
	private String phoneNo;
	private String mobileNo;
	private boolean transportAvailable;
	private boolean mobileVerified;
	private boolean emailVerified;
	private boolean mocked;
	private boolean verified;
	private String group;
	private Company company;
	private String employeeId;
	private String designation;
	private String department;
	
	public String getUserType() {
		return userType;
	}
	public void setUserType(String userType) {
		this.userType = userType;
	}
	public String getUserNo() {
		return userNo;
	}
	public void setUserNo(String userNo) {
		this.userNo = userNo;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getShortDesc() {
		return shortDesc;
	}
	public void setShortDesc(String shortDesc) {
		this.shortDesc = shortDesc;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Address getAddress() {
		return address;
	}
	public void setAddress(Address address) {
		this.address = address;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getMobileNo() {
		return mobileNo;
	}
	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}
	public boolean isTransportAvailable() {
		return transportAvailable;
	}
	public void setTransportAvailable(boolean transportAvailable) {
		this.transportAvailable = transportAvailable;
	}
	public boolean isMobileVerified() {
		return mobileVerified;
	}
	public void setMobileVerified(boolean mobileVerified) {
		this.mobileVerified = mobileVerified;
	}
	public boolean isEmailVerified() {
		return emailVerified;
	}
	public void setEmailVerified(boolean emailVerified) {
		this.emailVerified = emailVerified;
	}
	public boolean isMocked() {
		return mocked;
	}
	public void setMocked(boolean mocked) {
		this.mocked = mocked;
	}
	public boolean isVerified() {
		return verified;
	}
	public void setVerified(boolean verified) {
		this.verified = verified;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}
	public Company getCompany() {
		return company;
	}
	public void setCompany(Company company) {
		this.company = company;
	}
	public String getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getDepartment() {
		return department;
	}
	public void setDepartment(String department) {
		this.department = department;
	}
	
}
