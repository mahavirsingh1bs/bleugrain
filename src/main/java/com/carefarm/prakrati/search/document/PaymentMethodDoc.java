package com.carefarm.prakrati.search.document;

import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "bleugrain", type = "paymentMethods", shards = 5, replicas = 3)
public class PaymentMethodDoc extends AbstractDoc {
	private String name;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
}