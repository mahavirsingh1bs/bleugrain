package com.carefarm.prakrati.search.document;

import org.springframework.data.elasticsearch.annotations.Document;

@Document(indexName = "bleugrain", type = "states", shards = 5, replicas = 3)
public class StateDoc extends AbstractDoc {
	private String name;
	private String country;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
}
