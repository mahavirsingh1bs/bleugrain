package com.carefarm.prakrati.search.document;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.elasticsearch.annotations.Document;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.search.document.Shared.Category;
import com.carefarm.prakrati.search.document.Shared.Menuitem;

@Document(indexName = "bleugrain", type = "menuitems", shards = 5, replicas = 3)
public class MenuitemDoc extends AbstractDoc {
	private String name;
	private String keywords;
	private Category category;
	private List<Menuitem> parentMenuitems;
	private boolean active;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public List<Menuitem> getParentMenuitems() {
		return parentMenuitems;
	}
	public void setParentMenuitems(List<Menuitem> parentMenuitems) {
		this.parentMenuitems = parentMenuitems;
	}
	public void addParentMenuitem(Menuitem parentMenuitem) {
		if (Predicates.isNull.test(this.parentMenuitems)) {
			this.parentMenuitems = new ArrayList<>();
		}
		this.parentMenuitems.add(parentMenuitem);
	}
	public boolean isActive() {
		return active;
	}
	public void setActive(boolean active) {
		this.active = active;
	}
	
}
