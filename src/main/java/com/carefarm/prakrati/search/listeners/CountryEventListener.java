package com.carefarm.prakrati.search.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.common.entity.Country;
import com.carefarm.prakrati.search.document.CountryDoc;
import com.carefarm.prakrati.search.events.CountryEvent;
import com.carefarm.prakrati.search.repository.CountrySearchRepository;

@Component
public class CountryEventListener extends PrakratiEventListener<Country, CountryDoc, CountryEvent> {

	@Autowired
	private CountrySearchRepository countrySearchRepository;

	@Override
	protected void merge(Iterable<Country> entities) {
		countrySearchRepository.save(documents(entities));
	}
	
	@Override
	protected void delete(Iterable<Country> entities) {
		countrySearchRepository.delete(documents(entities));
	}

}
