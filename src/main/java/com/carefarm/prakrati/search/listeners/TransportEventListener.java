package com.carefarm.prakrati.search.listeners;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.delivery.entity.Permit;
import com.carefarm.prakrati.delivery.entity.Transport;
import com.carefarm.prakrati.entity.Company;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.search.document.Shared;
import com.carefarm.prakrati.search.document.TransportDoc;
import com.carefarm.prakrati.search.events.TransportEvent;
import com.carefarm.prakrati.search.repository.TransportSearchRepository;

@Component
public class TransportEventListener extends PrakratiEventListener<Transport, TransportDoc, TransportEvent> {

	@Autowired
	private TransportSearchRepository transportSearchRepository;

	@Override
	protected void merge(Iterable<Transport> entities) {
		transportSearchRepository.save(documents(entities));
	}
	
	@Override
	protected void delete(Iterable<Transport> entities) {
		transportSearchRepository.delete(documents(entities));
	}

	@Override
	protected TransportDoc createDocument(Transport entity) {
		TransportDoc transport = mapper.map(entity, TransportDoc.class);
		
		Optional<List<Permit>> permitsOpt = Optional.ofNullable(entity.getPermits());
		permitsOpt.ifPresent(permits -> permits.forEach(permit -> transport.addPermit(permit.getName())));

		Optional<User> driverOpt = Optional.ofNullable(entity.getDriver());
		driverOpt.ifPresent(driver -> transport.setDriver(mapper.map(driver, Shared.User.class)));

		Optional<Company> companyOpt = Optional.ofNullable(entity.getCompany());
		companyOpt.ifPresent(company -> transport.setCompany(mapper.map(company, Shared.Company.class)));
		
		return transport;
	}

}
