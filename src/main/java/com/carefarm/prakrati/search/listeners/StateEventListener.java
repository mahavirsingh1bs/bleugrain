package com.carefarm.prakrati.search.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.common.entity.State;
import com.carefarm.prakrati.search.document.StateDoc;
import com.carefarm.prakrati.search.events.StateEvent;
import com.carefarm.prakrati.search.repository.StateSearchRepository;

@Component
public class StateEventListener extends PrakratiEventListener<State, StateDoc, StateEvent> {

	@Autowired
	private StateSearchRepository stateSearchRepository;

	@Override
	protected void merge(Iterable<State> entities) {
		stateSearchRepository.save(documents(entities));
	}
	
	@Override
	protected void delete(Iterable<State> entities) {
		stateSearchRepository.delete(documents(entities));
	}

}
