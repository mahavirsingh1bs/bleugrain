package com.carefarm.prakrati.search.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.PaymentCard;
import com.carefarm.prakrati.search.document.PaymentCardDoc;
import com.carefarm.prakrati.search.events.PaymentCardEvent;
import com.carefarm.prakrati.search.repository.PaymentCardSearchRepository;

@Component
public class PaymentCardEventListener extends PrakratiEventListener<PaymentCard, PaymentCardDoc, PaymentCardEvent> {

	@Autowired
	private PaymentCardSearchRepository paymentCardSearchRepository;

	@Override
	protected void merge(Iterable<PaymentCard> entities) {
		paymentCardSearchRepository.save(documents(entities));
	}
	
	@Override
	protected void delete(Iterable<PaymentCard> entities) {
		paymentCardSearchRepository.delete(documents(entities));
	}

}
