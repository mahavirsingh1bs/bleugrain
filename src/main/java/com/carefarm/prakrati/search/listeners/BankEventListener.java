package com.carefarm.prakrati.search.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.Bank;
import com.carefarm.prakrati.search.document.BankDoc;
import com.carefarm.prakrati.search.events.BankEvent;
import com.carefarm.prakrati.search.repository.BankSearchRepository;

@Component
public class BankEventListener extends PrakratiEventListener<Bank, BankDoc, BankEvent> {

	@Autowired
	private BankSearchRepository bankSearchRepository;
	
	@Override
	protected void merge(Iterable<Bank> entities) {
		bankSearchRepository.save(documents(entities));
	}
	
	@Override
	protected void delete(Iterable<Bank> entities) {
		bankSearchRepository.delete(documents(entities));
	}

}
