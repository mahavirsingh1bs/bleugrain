package com.carefarm.prakrati.search.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.PaymentMethod;
import com.carefarm.prakrati.search.document.PaymentMethodDoc;
import com.carefarm.prakrati.search.events.PaymentMethodEvent;
import com.carefarm.prakrati.search.repository.PaymentMethodSearchRepository;

@Component
public class PaymentMethodEventListener extends PrakratiEventListener<PaymentMethod, PaymentMethodDoc, PaymentMethodEvent> {

	@Autowired
	private PaymentMethodSearchRepository paymentMethodSearchRepository;

	@Override
	protected void merge(Iterable<PaymentMethod> entities) {
		paymentMethodSearchRepository.save(documents(entities));
	}
	
	@Override
	protected void delete(Iterable<PaymentMethod> entities) {
		paymentMethodSearchRepository.delete(documents(entities));
	}

}
