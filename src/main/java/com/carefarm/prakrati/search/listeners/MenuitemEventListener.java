package com.carefarm.prakrati.search.listeners;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.entity.Menuitem;
import com.carefarm.prakrati.search.document.MenuitemDoc;
import com.carefarm.prakrati.search.document.Shared;
import com.carefarm.prakrati.search.events.MenuitemEvent;
import com.carefarm.prakrati.search.repository.MenuitemSearchRepository;

@Component
public class MenuitemEventListener extends PrakratiEventListener<Menuitem, MenuitemDoc, MenuitemEvent> {

	@Autowired
	private MenuitemSearchRepository menuitemSearchRepository;
	
	@Override
	protected void merge(Iterable<Menuitem> entities) {
		menuitemSearchRepository.save(documents(entities));
	}
	
	@Override
	protected void delete(Iterable<Menuitem> entities) {
		menuitemSearchRepository.delete(documents(entities));
	}

	@Override
	protected MenuitemDoc createDocument(Menuitem entity) {
		MenuitemDoc menuitem = mapper.map(entity, MenuitemDoc.class);
		Optional<Menuitem> parentMenuitemOpt = Optional.ofNullable(entity.getParentMenuitem());
		parentMenuitemOpt.ifPresent(parentMenuitem -> {
			menuitem.addParentMenuitem(mapper.map(parentMenuitem, Shared.Menuitem.class));
			addParentMenuitems(parentMenuitem, menuitem);
		});
		return menuitem;
	}

	private void addParentMenuitems(Menuitem menuitem, MenuitemDoc menuitemDoc) {
		if (Predicates.notNull.test(menuitem.getParentMenuitem())) {
			Shared.Menuitem parentMenuitem = mapper.map(menuitem.getParentMenuitem(), Shared.Menuitem.class);
			menuitemDoc.addParentMenuitem(parentMenuitem);
			addParentMenuitems(menuitem.getParentMenuitem(), menuitemDoc);
		}
	}
	
}
