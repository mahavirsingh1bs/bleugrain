package com.carefarm.prakrati.search.listeners;

import static com.carefarm.prakrati.constants.Predicates.isNull;

import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.entity.AbstractEntity;
import com.carefarm.prakrati.entity.Category;
import com.carefarm.prakrati.search.document.AbstractDoc;
import com.carefarm.prakrati.search.document.Categorizable;
import com.carefarm.prakrati.search.document.Shared;
import com.carefarm.prakrati.search.events.PrakratiEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;

public abstract class PrakratiEventListener<E extends AbstractEntity, D extends AbstractDoc, V extends PrakratiEvent<Iterable<E>>> implements ApplicationListener<V> {

	private Class<D> documentClass;
	
	@Autowired
	protected DozerBeanMapper mapper;
	
	@SuppressWarnings("unchecked")
	public PrakratiEventListener() {
		this.documentClass = (Class<D>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];
	}
	
	@Override
	public void onApplicationEvent(V event) {
		if (isNull.test(event)) return;
		
		final Iterable<E> entities = (Iterable<E>)event.getSource();
		
		if (isNull.test(entities)) return;
		if (!entities.iterator().hasNext()) return;
		
		if (event.getEventType() == EventType.INSERT || event.getEventType() == EventType.UPDATE) {
			this.merge(entities);
		} else if (event.getEventType() == EventType.DELETE) {
			this.delete(entities);
		}
	}
	
	protected <T extends Categorizable> void addParentCategories(Category category, T categorizable) {
		if (Predicates.notNull.test(category.getParentCategory())) {
			Shared.Category parentCategory = mapper.map(category.getParentCategory(), Shared.Category.class);
			categorizable.addCategory(parentCategory);
			addParentCategories(category.getParentCategory(), categorizable);
		}
	}
	
	protected void addCategory(Category category, Categorizable document) {
		if (category.isRootNode()) {
			if (!document.isExists(category.getUniqueId())) {
				Shared.Category sharedCategory = mapper.map(category, Shared.Category.class);
				document.addCategory(sharedCategory);
			}
		} else {
			if (!document.isExists(category.getUniqueId())) {
				Category parentCategory = category.getParentCategory();
				addCategory(parentCategory, document);
				Shared.Category sharedCategory = mapper.map(category, Shared.Category.class);
				document.addCategory(sharedCategory, parentCategory.getUniqueId());
			}
		}
	}
	
	protected List<D> documents(Iterable<E> entities) {
		List<D> documents = new ArrayList<>();
		entities.forEach(entity -> {
			documents.add(createDocument(entity));
		});
		return documents;
	}
	
	protected D createDocument(E entity) {
		return mapper.map(entity, documentClass);
	} 
	
	protected abstract void merge(Iterable<E> entities);
	
	protected abstract void delete(Iterable<E> entities);

}
