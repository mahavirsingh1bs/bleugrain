package com.carefarm.prakrati.search.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.Department;
import com.carefarm.prakrati.search.document.DepartmentDoc;
import com.carefarm.prakrati.search.events.DepartmentEvent;
import com.carefarm.prakrati.search.repository.DepartmentSearchRepository;

@Component
public class DepartmentEventListener extends PrakratiEventListener<Department, DepartmentDoc, DepartmentEvent> {

	@Autowired
	private DepartmentSearchRepository departmentSearchRepository;

	@Override
	protected void merge(Iterable<Department> entities) {
		departmentSearchRepository.save(documents(entities));
	}
	
	@Override
	protected void delete(Iterable<Department> entities) {
		departmentSearchRepository.delete(documents(entities));
	}

}
