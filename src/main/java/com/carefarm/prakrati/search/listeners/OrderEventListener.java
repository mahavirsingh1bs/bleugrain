package com.carefarm.prakrati.search.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.Order;
import com.carefarm.prakrati.search.document.OrderDoc;
import com.carefarm.prakrati.search.events.OrderEvent;
import com.carefarm.prakrati.search.repository.OrderSearchRepository;

@Component
public class OrderEventListener extends PrakratiEventListener<Order, OrderDoc, OrderEvent> {

	@Autowired
	private OrderSearchRepository orderSearchRepository;

	@Override
	protected void merge(Iterable<Order> entities) {
		orderSearchRepository.save(documents(entities));
	}

	@Override
	protected void delete(Iterable<Order> entities) {
		orderSearchRepository.delete(documents(entities));
	}

}
