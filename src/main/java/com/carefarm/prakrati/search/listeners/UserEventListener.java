package com.carefarm.prakrati.search.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.search.document.UserDoc;
import com.carefarm.prakrati.search.events.UserEvent;
import com.carefarm.prakrati.search.repository.UserSearchRepository;

@Component
public class UserEventListener extends PrakratiEventListener<User, UserDoc, UserEvent> {

	@Autowired
	private UserSearchRepository userSearchRepository;
	
	@Override
	protected void merge(Iterable<User> entities) {
		userSearchRepository.save(documents(entities));
	}
	
	@Override
	protected void delete(Iterable<User> entities) {
		userSearchRepository.delete(documents(entities));
	}

}
