package com.carefarm.prakrati.search.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.Designation;
import com.carefarm.prakrati.search.document.DesignationDoc;
import com.carefarm.prakrati.search.events.DesignationEvent;
import com.carefarm.prakrati.search.repository.DesignationSearchRepository;

@Component
public class DesignationEventListener extends PrakratiEventListener<Designation, DesignationDoc, DesignationEvent> {

	@Autowired
	private DesignationSearchRepository designationSearchRepository;

	@Override
	protected void merge(Iterable<Designation> entities) {
		designationSearchRepository.save(documents(entities));
	}
	
	@Override
	protected void delete(Iterable<Designation> entities) {
		designationSearchRepository.delete(documents(entities));
	}

}
