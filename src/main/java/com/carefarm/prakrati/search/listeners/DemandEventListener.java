package com.carefarm.prakrati.search.listeners;

import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.Category;
import com.carefarm.prakrati.entity.Demand;
import com.carefarm.prakrati.entity.Tag;
import com.carefarm.prakrati.search.document.DemandDoc;
import com.carefarm.prakrati.search.events.DemandEvent;
import com.carefarm.prakrati.search.repository.DemandSearchRepository;

@Component
public class DemandEventListener extends PrakratiEventListener<Demand, DemandDoc, DemandEvent> {

	@Autowired
	private DemandSearchRepository demandSearchRepository;

	@Override
	protected void merge(Iterable<Demand> entities) {
		demandSearchRepository.save(documents(entities));
	}
	
	@Override
	protected void delete(Iterable<Demand> entities) {
		demandSearchRepository.delete(documents(entities));
	}

	@Override
	protected DemandDoc createDocument(Demand entity) {
		DemandDoc demand = mapper.map(entity, DemandDoc.class);
		Optional<Category> categoryOpt = Optional.ofNullable(entity.getCategory());
		categoryOpt.ifPresent(category -> {
			this.addCategory(category, demand);
		});
		
		Optional<Set<Tag>> tagsOpt = Optional.ofNullable(entity.getTags());
		tagsOpt.ifPresent(tags -> tags.forEach(tag -> demand.addTag(tag.getName())));
		return demand;
	}
	
}
