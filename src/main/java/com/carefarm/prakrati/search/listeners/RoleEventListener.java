package com.carefarm.prakrati.search.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.Role;
import com.carefarm.prakrati.search.document.RoleDoc;
import com.carefarm.prakrati.search.events.RoleEvent;
import com.carefarm.prakrati.search.repository.RoleSearchRepository;

@Component
public class RoleEventListener extends PrakratiEventListener<Role, RoleDoc, RoleEvent> {

	@Autowired
	private RoleSearchRepository roleSearchRepository;

	@Override
	protected void merge(Iterable<Role> entities) {
		roleSearchRepository.save(documents(entities));
	}
	
	@Override
	protected void delete(Iterable<Role> entities) {
		roleSearchRepository.delete(documents(entities));
	}

}
