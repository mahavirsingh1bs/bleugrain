package com.carefarm.prakrati.search.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.Company;
import com.carefarm.prakrati.search.document.CompanyDoc;
import com.carefarm.prakrati.search.events.CompanyEvent;
import com.carefarm.prakrati.search.repository.CompanySearchRepository;

@Component
public class CompanyEventListener extends PrakratiEventListener<Company, CompanyDoc, CompanyEvent> {

	@Autowired
	private CompanySearchRepository companySearchRepository;

	@Override
	protected void merge(Iterable<Company> entities) {
		companySearchRepository.save(documents(entities));
	}
	
	@Override
	protected void delete(Iterable<Company> entities) {
		companySearchRepository.delete(documents(entities));
	}

}
