package com.carefarm.prakrati.search.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.Category;
import com.carefarm.prakrati.search.document.CategoryDoc;
import com.carefarm.prakrati.search.events.CategoryEvent;
import com.carefarm.prakrati.search.repository.CategorySearchRepository;

@Component
public class CategoryEventListener extends PrakratiEventListener<Category, CategoryDoc, CategoryEvent> {

	@Autowired
	private CategorySearchRepository categorySearchRepository;

	@Override
	protected void merge(Iterable<Category> entities) {
		categorySearchRepository.save(documents(entities));
	}

	@Override
	protected void delete(Iterable<Category> entities) {
		categorySearchRepository.delete(documents(entities));
	}
	
	@Override
	protected CategoryDoc createDocument(Category entity) {
		CategoryDoc category = mapper.map(entity, CategoryDoc.class);
		this.addParentCategories(entity, category);
		return category;
	}
	
}
