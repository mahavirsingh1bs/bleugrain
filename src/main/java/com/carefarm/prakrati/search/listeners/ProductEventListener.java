package com.carefarm.prakrati.search.listeners;

import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.Category;
import com.carefarm.prakrati.entity.Product;
import com.carefarm.prakrati.entity.Tag;
import com.carefarm.prakrati.search.document.ProductDoc;
import com.carefarm.prakrati.search.events.ProductEvent;
import com.carefarm.prakrati.search.repository.ProductSearchRepository;

@Component
public class ProductEventListener extends PrakratiEventListener<Product, ProductDoc, ProductEvent> {

	@Autowired
	private ProductSearchRepository productSearchRepository;

	@Override
	protected void merge(Iterable<Product> entities) {
		productSearchRepository.save(documents(entities));
	}
	
	@Override
	protected void delete(Iterable<Product> entities) {
		productSearchRepository.delete(documents(entities));
	}

	@Override
	protected ProductDoc createDocument(Product entity) {
		ProductDoc product = mapper.map(entity, ProductDoc.class);
		Optional<Set<Category>> categoriesOpt = Optional.ofNullable(entity.getCategories());
		categoriesOpt.ifPresent(categories -> {
			categories.forEach(category -> {
				this.addCategory(category, product);
			});
		});
		
		Optional<Set<Tag>> tagsOpt = Optional.ofNullable(entity.getTags());
		tagsOpt.ifPresent(tags -> tags.forEach(tag -> product.addTag(tag.getName())));
		return product;
	}	
	
}
