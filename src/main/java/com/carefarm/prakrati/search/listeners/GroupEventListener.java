package com.carefarm.prakrati.search.listeners;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.Group;
import com.carefarm.prakrati.search.document.GroupDoc;
import com.carefarm.prakrati.search.events.GroupEvent;
import com.carefarm.prakrati.search.repository.GroupSearchRepository;

@Component
public class GroupEventListener extends PrakratiEventListener<Group, GroupDoc, GroupEvent> {

	@Autowired
	private GroupSearchRepository groupSearchRepository;

	@Override
	protected void merge(Iterable<Group> entities) {
		groupSearchRepository.save(documents(entities));
	}
	
	@Override
	protected void delete(Iterable<Group> entities) {
		groupSearchRepository.delete(documents(entities));
	}

}
