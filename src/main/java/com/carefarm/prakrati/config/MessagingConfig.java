package com.carefarm.prakrati.config;

import java.util.Arrays;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.activemq.pool.PooledConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.RoutesBuilder;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jms.JmsConfiguration;
import org.apache.camel.spring.boot.CamelContextConfiguration;
// import org.apache.camel.spring.boot.CamelContextConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.carefarm.prakrati.constants.Endpoints;
import com.carefarm.prakrati.processors.BidProcessor;
import com.carefarm.prakrati.processors.DemandAssignedProcessor;
import com.carefarm.prakrati.processors.DemandFulfilledProcessor;
import com.carefarm.prakrati.processors.NotificationProcessor;
import com.carefarm.prakrati.processors.OrderAssignedProcessor;
import com.carefarm.prakrati.processors.OrderDeliveredProcessor;
import com.carefarm.prakrati.processors.OrderDispatchProcessor;
import com.carefarm.prakrati.processors.OrderPlacementProcessor;
import com.carefarm.prakrati.processors.OrderShipmentProcessor;
import com.carefarm.prakrati.processors.ProductAssignedProcessor;
import com.carefarm.prakrati.processors.ProductProcessor;
import com.carefarm.prakrati.processors.ProductSoldOutProcessor;

@Configuration
public class MessagingConfig /*extends SingleRouteCamelConfiguration */{
	
	@Bean
	public RoutesBuilder prakratiRouter() {
		return new RouteBuilder() {

			@Override
			public void configure() throws Exception {
				from(Endpoints.PRAKRATI_NOTIFICATIONS).bean(NotificationProcessor.class);
				from(Endpoints.PRAKRATI_BIDS).bean(BidProcessor.class);
				from(Endpoints.BIDDABLE_PRODUCTS).multicast().bean(ProductProcessor.class);
				from(Endpoints.ORDER_PLACED).bean(OrderPlacementProcessor.class);
				from(Endpoints.ORDER_SHIPPED).multicast().bean(OrderShipmentProcessor.class);
				from(Endpoints.ORDER_ASSIGNED).multicast().bean(OrderAssignedProcessor.class);
				from(Endpoints.ORDER_DISPATCHED).bean(OrderDispatchProcessor.class);
				from(Endpoints.ORDER_DELIVERED).multicast().bean(OrderDeliveredProcessor.class);
				from(Endpoints.PRODUCT_ASSIGNED).multicast().bean(ProductAssignedProcessor.class);
				from(Endpoints.PRODUCT_SOLDOUT).multicast().bean(ProductSoldOutProcessor.class);
				from(Endpoints.DEMAND_ASSIGNED).multicast().bean(DemandAssignedProcessor.class);
				from(Endpoints.DEMAND_FULFILLED).multicast().bean(DemandFulfilledProcessor.class);
			}

		};
	}
	/*
	@Bean
    public SpringCamelContext camelContext(ApplicationContext applicationContext) throws Exception {
        SpringCamelContext camelContext = new SpringCamelContext(applicationContext);
        camelContext.addComponent("activemq", activeMQComponent("vm://localhost?broker.persistent=false"));
        camelContext.addRoutes(prakratiRouter());
        return camelContext;
    }
	*/
	
	/*
    @Bean
    public ConnectionFactory jmsConnectionFactory() {
        // use a pool for ActiveMQ connections
        PooledConnectionFactory pool = new PooledConnectionFactory();
        pool.setConnectionFactory(new ActiveMQConnectionFactory("tcp://localhost:61616"));
        return pool;
    }
	*/
	@Bean
	public CamelContextConfiguration contextConfiguration() {
		return new CamelContextConfiguration() {

			@Override
			public void afterApplicationStart(CamelContext camelContext) {
				
			}

			@Override
			public void beforeApplicationStart(CamelContext camelContext) {
				camelContext.addComponent("activemq", activeMQComponent("tcp://localhost:61616?broker.persistent=true"));
			}
			
		};
	}
	
	private ActiveMQComponent activeMQComponent(String brokerUrl) {
		ActiveMQConnectionFactory jmsConnectionFactory = new ActiveMQConnectionFactory(brokerUrl);
		jmsConnectionFactory.setTrustedPackages(Arrays.asList("com.carefarm.prakrati.web.bean".split(",")));
		PooledConnectionFactory pooledConnectionFactory = new PooledConnectionFactory(jmsConnectionFactory);
		pooledConnectionFactory.setMaxConnections(10);
		
		JmsConfiguration jmsConfiguration = new JmsConfiguration();
		jmsConfiguration.setConnectionFactory(pooledConnectionFactory);
		jmsConfiguration.setConcurrentConsumers(10);
		
		ActiveMQComponent activeMQComponent = new ActiveMQComponent();
		activeMQComponent.setConfiguration(jmsConfiguration);
		
		return activeMQComponent;
	}

	/*
	@Override
	public RouteBuilder route() {
		return new RouteBuilder() {

			@Override
			public void configure() throws Exception {
				from("direct:start").toF("log:%s?level=DEBUG", getClass().getName());
			}

		};
	}
	*/
	
}
