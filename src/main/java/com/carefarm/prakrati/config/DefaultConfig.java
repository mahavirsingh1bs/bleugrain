package com.carefarm.prakrati.config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import org.dozer.CustomConverter;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.web.config.SpringDataWebConfiguration;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import com.carefarm.prakrati.aspects.SystemArchitecture;
import com.carefarm.prakrati.aspects.TimeEstimator;
import com.carefarm.prakrati.converters.PaymentDecryptor;
import com.carefarm.prakrati.converters.PaymentEncryptor;
import com.carefarm.prakrati.security.Encryptor;
import com.carefarm.prakrati.security.impl.DefaultEncryptorImpl;

import freemarker.template.TemplateExceptionHandler;

@Configuration
public class DefaultConfig {
	
	@Value("${mail.default.host}")
	private String mailHost;
	
	@Value("${mail.default.port}")
	private int mailPort;
	
	@Value("${mail.default.username}")
	private String mailUsername;
	
	@Value("${mail.default.password}")
	private String mailPassword;

	@Value("${elasticsearch.index.search.indexing.enabled}")
	private boolean searchIndexEnabled;
	
	@Value("${elasticsearch.index.mapping.creation.enabled}")
	private boolean createIndexAndMappingEnabled;
	
	@Value("${search.index.base}")
	private String searchIndexBase;
	
	@Value("${database.driverClassName}")
	private String driverClassName;
	
	@Value("${database.jdbcUrl}")
	private String databaseJdbcUrl;
	
	@Value("${database.username}")
	private String databaseUsername;
	
	@Value("${database.password}")
	private String databasePassword;
	
	@Value("${com.carefarm.prakrati.query.next.product.no}")
	private String nextProductNoQuery;
	
	private String key = "JamesBarkAtMeAt$"; // 128 bit key
	private String initVector = "Hack18YearsOld27"; // 16 bytes IV
	
	@Autowired
	private PaymentDecryptor paymentDecryptor;
	
	@Autowired
	private PaymentEncryptor paymentEncryptor;
	
	@Autowired
	private Encryptor encryptor;
	
	@Autowired
	private ResourceLoader resourceLoader;
	
	@Bean
	public boolean searchIndexEnabled() {
		return searchIndexEnabled;
	}
	
	@Bean
	public boolean createIndexAndMappingEnabled() {
		return createIndexAndMappingEnabled;
	}
	
	@Bean
	public String searchIndexBase() {
		return searchIndexBase;
	}
	
	@Bean
	public String driverClassName() {
		return driverClassName;
	}

	@Bean
	public String databaseJdbcUrl() {
		return databaseJdbcUrl;
	}

	@Bean
	public String databaseUsername() {
		return databaseUsername;
	}

	@Bean
	public String databasePassword() {
		return databasePassword;
	}

	@Bean
	public String nextProductNoQuery() {
		return nextProductNoQuery;
	}
	
	@Bean
	public SystemArchitecture systemArchitecture() {
		return new SystemArchitecture();
	}
	
	@Bean
	public TimeEstimator timeEstimator() {
		return new TimeEstimator();
	}

	@Bean
	public LocaleResolver localeResolver() {
		SessionLocaleResolver localeResolver = new SessionLocaleResolver();
		localeResolver.setDefaultLocale(Locale.ENGLISH);
		return localeResolver;
	}
	
	@Bean
	public MessageSource messageSource() {
		SerializableResourceBundleMessageSource messageSource = new SerializableResourceBundleMessageSource();
		messageSource.setBasename("classpath:/Messages");
		return messageSource;
	}
	
	/*
	@Bean
	@Autowired
	public DomainClassConverter<?> domainClassConverter(@Qualifier("mvcConversionService") ConversionService conversionService) {
		@SuppressWarnings({ "rawtypes", "unchecked" })
		DomainClassConverter domainClassConverter = new DomainClassConverter(conversionService);
		return domainClassConverter;
	}
 	*/
	
	@Bean
	public SpringDataWebConfiguration springDataWebConfiguration() {
		return new SpringDataWebConfiguration();
	}
	
	@Bean
	public JavaMailSenderImpl mailSender() {
		Properties javaMailProperties = new Properties();
		javaMailProperties.setProperty("mail.transport.protocol", "smtp");
		javaMailProperties.setProperty("mail.smtp.auth", "true");
		javaMailProperties.setProperty("mail.smtp.starttls.enable", "true");
		javaMailProperties.setProperty("mail.debug", "true");
		javaMailProperties.setProperty("mail.smtp.ssl.enable", "true");
		
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
		mailSender.setHost(encryptor.decrypt(mailHost));
		mailSender.setPort(mailPort);
		mailSender.setUsername(encryptor.decrypt(mailUsername));
		mailSender.setPassword(encryptor.decrypt(mailPassword));
		mailSender.setJavaMailProperties(javaMailProperties);
		return mailSender;
	}

	@Bean
	public DozerBeanMapper dozerBeanMapper() {
		Map<String, CustomConverter> customConvertersWithId = new HashMap<>();
		customConvertersWithId.put("paymentEncryptor", paymentEncryptor);
		customConvertersWithId.put("paymentDecryptor", paymentDecryptor);
		DozerBeanMapper dozerBeanMapper = new DozerBeanMapper();
		List<String> mappingFiles = new ArrayList<>();
		mappingFiles.add("mapping/dozer-bean-mappings.xml");
		mappingFiles.add("mapping/dozer-search-mappings.xml");
		dozerBeanMapper.setMappingFiles(mappingFiles);
		dozerBeanMapper.setCustomConvertersWithId(customConvertersWithId);
		return dozerBeanMapper;
	}
	
	@Bean
	public Encryptor encryptor() {
		return new DefaultEncryptorImpl(key, initVector);
	}

	@Bean
	public RestTemplate restTemplate() {
		return new RestTemplate();
	}
	
	@Bean
	public freemarker.template.Configuration configuration() throws IOException {
		freemarker.template.Configuration cfg = new freemarker.template.Configuration(freemarker.template.Configuration.VERSION_2_3_25);
		Resource templateDir = resourceLoader.getResource("classpath:static/assets/emails/templates");
		cfg.setDirectoryForTemplateLoading(templateDir.getFile());
		cfg.setDefaultEncoding("UTF-8");
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
		cfg.setLogTemplateExceptions(false);
		return cfg;
	}

}
