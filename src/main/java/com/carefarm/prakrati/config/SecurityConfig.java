package com.carefarm.prakrati.config;

import javax.servlet.Filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.csrf.CsrfFilter;
import org.springframework.security.web.csrf.CsrfTokenRepository;
import org.springframework.security.web.csrf.HttpSessionCsrfTokenRepository;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import com.carefarm.prakrati.filters.CsrfHeaderFilter;
import com.carefarm.prakrati.filters.DomainUsernamePasswordJsonAuthenticationFilter;
import com.carefarm.prakrati.handlers.PrakratiAuthenticationSuccessHandler;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private AuthenticationProvider authenticationProvider;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		auth.authenticationProvider(authenticationProvider);
	}
	
	@Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }
	
	@Override
    public void configure(WebSecurity web) throws Exception {
		web.ignoring()
        	.antMatchers("/assets/**")
        	.antMatchers("/wro/**");
    }
	
	protected void configure(HttpSecurity http) throws Exception {
		http
			.exceptionHandling()
				.authenticationEntryPoint(authenticationEntryPoint())
				.and()
			.authorizeRequests()
				.antMatchers("/login", "/logout", "/welcome", "/errors/*", "/bleugrain/*", "/notification/*", "/app/*", "/menuitems/**").permitAll()
				.antMatchers("/", "/signup/*", "/register/*", "/default", "/Messages", "/search/**", "/account/**", "/businessCategories/**").permitAll()
				.antMatchers("/users").hasAnyAuthority("ROLE_ADMIN", "ROLE_FARMER", "ROLE_BROKER", "ROLE_RETAILER", "ROLE_AGENT", "ROLE_COMPANY_ADMIN", "ROLE_CUSTOMER")
				.antMatchers("/user").hasAnyAuthority("ROLE_ADMIN", "ROLE_FARMER", "ROLE_BROKER", "ROLE_RETAILER", "ROLE_AGENT", "ROLE_COMPANY_ADMIN", "ROLE_CUSTOMER")
				.antMatchers("/states/**", "/countries/**", "/categories/**", "/units/**", "/units/**").permitAll()
				.antMatchers("/currentUser", "/advancedBooking", "/others/**").permitAll()
				.antMatchers("/events/**").hasAnyAuthority("ROLE_ADMIN", "ROLE_EXECUTIVE", "ROLE_STMember", "ROLE_CTMember")
				.antMatchers("/accounts/**").hasAnyAuthority("ROLE_ADMIN", "ROLE_FARMER", "ROLE_BROKER", "ROLE_RETAILER", "ROLE_AGENT", "ROLE_COMPANY_ADMIN", "ROLE_CUSTOMER")
				.antMatchers("/products/**", "/buyers/**").hasAnyAuthority("ROLE_ADMIN", "ROLE_FARMER", "ROLE_BROKER", "ROLE_RETAILER", "ROLE_COMPANY_ADMIN", "ROLE_AGENT", "ROLE_REGION_ADMIN", "ROLE_REGION_MANAGER", "ROLE_DISPATCHER")
				.antMatchers("/pmethods/**", "/sellers/**").hasAnyAuthority("ROLE_ADMIN", "ROLE_BROKER", "ROLE_RETAILER", "ROLE_AGENT", "ROLE_COMPANY_ADMIN", "ROLE_CUSTOMER")
				.antMatchers("/companies/**").hasAnyAuthority("ROLE_ADMIN", "ROLE_FARMER", "ROLE_BROKER", "ROLE_RETAILER", "ROLE_AGENT", "ROLE_COMPANY_ADMIN", "ROLE_CUSTOMER", "ROLE_EXECUTIVE", "ROLE_CTMEMBER", "ROLE_STMEMBER")
				.antMatchers("/brokers/**", "/farmers/**", "/employees/**", "/owners/**").hasAnyAuthority("ROLE_ADMIN", "ROLE_FARMER", "ROLE_BROKER", "ROLE_RETAILER", "ROLE_AGENT", "ROLE_COMPANY_ADMIN")
				.antMatchers("/drivers/**", "/transports/**").hasAnyAuthority("ROLE_ADMIN", "ROLE_COMPANY_ADMIN", "ROLE_DRIVER")
				.antMatchers("/users/**").hasAnyAuthority("ROLE_ADMIN", "ROLE_FARMER", "ROLE_BROKER", "ROLE_RETAILER", "ROLE_AGENT", "ROLE_COMPANY_ADMIN", "ROLE_CUSTOMER")
				.antMatchers("/addresses/**").hasAnyAuthority("ROLE_ADMIN", "ROLE_FARMER", "ROLE_BROKER", "ROLE_RETAILER", "ROLE_AGENT", "ROLE_COMPANY_ADMIN", "ROLE_CUSTOMER")
				.antMatchers("/orders/**").hasAnyAuthority("ROLE_ADMIN", "ROLE_FARMER", "ROLE_BROKER", "ROLE_RETAILER", "ROLE_AGENT", "ROLE_COMPANY_ADMIN", "ROLE_CUSTOMER", "ROLE_REGION_ADMIN", "ROLE_REGION_MANAGER", "ROLE_DISPATCHER")
				.antMatchers("/demands/**").hasAnyAuthority("ROLE_ADMIN", "ROLE_FARMER", "ROLE_BROKER", "ROLE_RETAILER", "ROLE_AGENT", "ROLE_COMPANY_ADMIN", "ROLE_CUSTOMER", "ROLE_REGION_ADMIN", "ROLE_REGION_MANAGER", "ROLE_DISPATCHER")
				.antMatchers("/admin/**").hasAnyAuthority("ROLE_ADMIN")
				.antMatchers("/home/**", "/profile/**", "/myaccount/**").hasAnyAuthority("ROLE_USER")
				.antMatchers("/pricing/**").hasAnyAuthority("ROLE_EMPLOYEE")
				.antMatchers("/agents/**").hasAnyAuthority("ROLE_ADMIN", "ROLE_FARMER", "ROLE_BROKER", "ROLE_RETAILER", "ROLE_AGENT", "ROLE_COMPANY_ADMIN")
				.antMatchers("/retailers/**").hasAnyAuthority("ROLE_ADMIN", "ROLE_FARMER", "ROLE_BROKER", "ROLE_RETAILER", "ROLE_AGENT", "ROLE_COMPANY_ADMIN")
				.antMatchers("/employees/**").hasAnyAuthority("ROLE_ADMIN", "ROLE_FARMER", "ROLE_BROKER", "ROLE_RETAILER", "ROLE_AGENT", "ROLE_COMPANY_ADMIN", "ROLE_REGION_ADMIN", "ROLE_REGION_MANAGER", "ROLE_DISPATCHER")
				.antMatchers("/productStatuses/**", "/productCategories/**", "/units/**", "/addresses/**").hasAnyAuthority("ROLE_ADMIN", "ROLE_FARMER", "ROLE_BROKER", "ROLE_RETAILER", "ROLE_AGENT", "ROLE_COMPANY_ADMIN", "ROLE_CUSTOMER")
				.antMatchers("/**").hasAnyAuthority("ROLE_USER")
				.and()
			.formLogin()
				.and()
			.csrf()
				.csrfTokenRepository(csrfTokenRepository())
				.and()
			.addFilterAfter(csrfHeaderFilter(), CsrfFilter.class)
			.addFilterBefore(domainJsonFormLoginFilter(), UsernamePasswordAuthenticationFilter.class)
			.logout()
				.logoutUrl("/logout")
				.logoutSuccessUrl("/login?logout")
				.permitAll()
				.and()
			/*.headers()
				.frameOptions()
					.sameOrigin()*/;
	}
	
	@Bean
	public Filter domainJsonFormLoginFilter() {
		DomainUsernamePasswordJsonAuthenticationFilter authenticationFilter = new DomainUsernamePasswordJsonAuthenticationFilter();
		// authenticationFilter.setFilterProcessesUrl("/login");
		authenticationFilter.setAuthenticationManager(authenticationManager);
		authenticationFilter.setUsernameParameter("username");
		authenticationFilter.setPasswordParameter("password");
		authenticationFilter.setAuthenticationSuccessHandler(prakratiAuthenticationSuccessHandler());
		authenticationFilter.setAuthenticationFailureHandler(prakratiAuthenticationFailureHandler());
		authenticationFilter.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/login","POST"));
		return authenticationFilter;
	}

	@Bean
	public AuthenticationSuccessHandler prakratiAuthenticationSuccessHandler() {
		return new PrakratiAuthenticationSuccessHandler();
	}
	
	@Bean
	public AuthenticationFailureHandler prakratiAuthenticationFailureHandler() {
		SimpleUrlAuthenticationFailureHandler authenticationFailureHandler = new SimpleUrlAuthenticationFailureHandler();
		authenticationFailureHandler.setDefaultFailureUrl("/login?error");
		return authenticationFailureHandler;
	}
	
	@Bean
	public Filter csrfHeaderFilter() {
		return new CsrfHeaderFilter();
	}
	
	public CsrfTokenRepository csrfTokenRepository() {
		HttpSessionCsrfTokenRepository csrfTokenRepository = new HttpSessionCsrfTokenRepository();
		csrfTokenRepository.setHeaderName("X-XSRF-TOKEN");
		return csrfTokenRepository;
	}
	
	@Bean
	public LoginUrlAuthenticationEntryPoint authenticationEntryPoint() {
		LoginUrlAuthenticationEntryPoint authenticationEntryPoint = new LoginUrlAuthenticationEntryPoint("/login");
		return authenticationEntryPoint;
	}
	
	/*
	@Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
                .antMatchers("/", "/home").permitAll()
                .anyRequest().authenticated()
                .and()
            .formLogin()
                .loginPage("/login")
                .permitAll()
                .and()
            .logout()
                .permitAll();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
            .inMemoryAuthentication()
                .withUser("user").password("password").roles("USER");
    }
    */
}
