package com.carefarm.prakrati.config;

import static com.carefarm.prakrati.common.util.Constants.BIDS_TOPIC;
import static com.carefarm.prakrati.common.util.Constants.NOTIFICATIONS_TOPIC;
import static com.carefarm.prakrati.common.util.Constants.PRODUCTS_TOPIC;
import static org.springframework.messaging.simp.SimpMessageType.CONNECT;
import static org.springframework.messaging.simp.SimpMessageType.DISCONNECT;
import static org.springframework.messaging.simp.SimpMessageType.MESSAGE;
import static org.springframework.messaging.simp.SimpMessageType.SUBSCRIBE;
import static org.springframework.messaging.simp.SimpMessageType.UNSUBSCRIBE;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.messaging.MessageSecurityMetadataSourceRegistry;
import org.springframework.security.config.annotation.web.socket.AbstractSecurityWebSocketMessageBrokerConfigurer;

@Configuration
public class WebSocketSecurityConfig extends AbstractSecurityWebSocketMessageBrokerConfigurer {
	
	protected void configureInbound(MessageSecurityMetadataSourceRegistry messages) {
        messages
        	.nullDestMatcher().authenticated()
        	.simpSubscribeDestMatchers(BIDS_TOPIC).hasAnyAuthority("ROLE_ADMIN", "ROLE_EXECUTIVE", "ROLE_COMPANY_ADMIN", "ROLE_AGENT", "ROLE_BROKER")
        	.simpSubscribeDestMatchers(PRODUCTS_TOPIC).hasAnyAuthority("ROLE_ADMIN", "ROLE_EXECUTIVE", "ROLE_COMPANY_ADMIN", "ROLE_AGENT", "ROLE_BROKER")
        	.simpSubscribeDestMatchers(NOTIFICATIONS_TOPIC).hasAnyAuthority("ROLE_USER")
        	.simpDestMatchers(BIDS_TOPIC, NOTIFICATIONS_TOPIC, PRODUCTS_TOPIC).hasAnyAuthority("ROLE_ADMIN", "ROLE_EXECUTIVE", "ROLE_COMPANY_ADMIN", "ROLE_AGENT", "ROLE_BROKER")
        	.simpDestMatchers(NOTIFICATIONS_TOPIC).hasAnyAuthority("ROLE_USER")
        	.simpTypeMatchers(CONNECT, MESSAGE, SUBSCRIBE).authenticated()
        	.simpTypeMatchers(UNSUBSCRIBE, DISCONNECT).permitAll();
    }
	
	@Override
    protected boolean sameOriginDisabled() {
        return true;
    }
}
