package com.carefarm.prakrati.config;

import static com.carefarm.prakrati.constants.Endpoints.NOTIFICATION_BID;
import static com.carefarm.prakrati.constants.Endpoints.NOTIFICATION_PRODUCT;
import static com.carefarm.prakrati.constants.Endpoints.NOTIFICATION_SEND;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.session.ExpiringSession;
import org.springframework.session.web.socket.config.annotation.AbstractSessionWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;

@Configuration
@EnableWebSocketMessageBroker
public class WebSocketConfig extends AbstractSessionWebSocketMessageBrokerConfigurer<ExpiringSession> /*AbstractWebSocketMessageBrokerConfigurer*/ {

	private static final String QUEUE = "/queue";
	private static final String TOPIC = "/topic";
	
	@Value("${websocket.relayhost}")
	private String relayHost;
	
	@Value("${websocket.relayport}")
	private int relayPort;
	
	@Override
	public void configureMessageBroker(MessageBrokerRegistry registry) {
		registry.enableStompBrokerRelay(TOPIC, QUEUE)
			.setRelayHost(relayHost)
			.setRelayPort(relayPort)
			.setSystemHeartbeatSendInterval(20000)
			.setSystemHeartbeatReceiveInterval(20000);
		registry.setApplicationDestinationPrefixes("/bleugrain");
	}
	
	@Override
	public void configureStompEndpoints(StompEndpointRegistry registry) {
		registry.addEndpoint(NOTIFICATION_SEND).withSockJS();
		registry.addEndpoint(NOTIFICATION_BID).withSockJS();
		registry.addEndpoint(NOTIFICATION_PRODUCT).withSockJS();
	}

}
