package com.carefarm.prakrati.config;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.annotation.Resource;

import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

import com.thoughtworks.xstream.InitializationException;

@Configuration
@PropertySource(value = "classpath:elasticsearch.properties")
@EnableElasticsearchRepositories(basePackages = { "com.carefarm.prakrati" })
public class ElasticsearchConfig {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ElasticsearchConfig.class);
	
	@Resource
    private Environment environment;

	@Bean
	public Client client() throws NumberFormatException, UnknownHostException {
		TransportClient client = TransportClient.builder().build()
        	.addTransportAddress(new InetSocketTransportAddress(
        			InetAddress.getByName(environment.getProperty("elasticsearch.host")), 
        			Integer.parseInt(environment.getProperty("elasticsearch.port"))));
		return client;
	}

	@Bean
	public ElasticsearchOperations elasticsearchTemplate() {
		try {
			return new ElasticsearchTemplate(client());
		} catch (NumberFormatException | UnknownHostException e) {
			LOGGER.info("Got an error while creating a bean of elasticsearch template");
			throw new InitializationException("Got an error while creating a bean of elasticsearch template", e);
		}
	}
}
