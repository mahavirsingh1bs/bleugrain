package com.carefarm.prakrati.config;

import java.beans.PropertyVetoException;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import com.carefarm.prakrati.processors.MergingPersistentUnitPostProcessor;
import com.carefarm.prakrati.security.Encryptor;
import com.mchange.v2.c3p0.ComboPooledDataSource;

@Configuration
@EnableSpringDataWebSupport
@EnableTransactionManagement
@EnableJpaRepositories(basePackages = { 
		"com.carefarm.prakrati.repository", 
		"com.carefarm.prakrati.payment.repository", 
		"com.carefarm.prakrati.delivery.repository"
	}
)
public class JpaConfig {
	
	@Autowired
	private String searchIndexBase;
	
	@Autowired
	private String driverClassName;
	
	@Autowired
	private String databaseJdbcUrl;
	
	@Autowired
	private String databaseUsername;
	
	@Autowired
	private String databasePassword;
	
	@Autowired
	private Encryptor encryptor;
	
	@Autowired
	private DataSource dataSource;
	
	@Bean
	@Primary
	@Profile("default")
	public DataSource dataSource() {
		ComboPooledDataSource dataSource = new ComboPooledDataSource();
		try {
			dataSource.setDriverClass(driverClassName);
			dataSource.setJdbcUrl(encryptor.decrypt(databaseJdbcUrl));
			dataSource.setUser(encryptor.decrypt(databaseUsername));
			dataSource.setPassword(encryptor.decrypt(databasePassword));
			dataSource.setMinPoolSize(5);
			dataSource.setMaxPoolSize(20);
			dataSource.setMaxIdleTime(300);
			dataSource.setMaxStatements(50);
			dataSource.setIdleConnectionTestPeriod(3000);
		} catch (PropertyVetoException ex) {
			ex.printStackTrace();
		}
		return dataSource;
	}
	
	@Bean
	@Profile("default")
	public EntityManagerFactory entityManagerFactory() {
		Properties jpaProperties = new Properties();
		
		/*
		jpaProperties.put("javax.persistence.jdbc.url", "com.mysql.jdbc.Driver");
		jpaProperties.put("javax.persistence.jdbc.url", encryptor.decrypt(databaseJdbcUrl));
		jpaProperties.put("javax.persistence.jdbc.user", encryptor.decrypt(databaseUsername));
		jpaProperties.put("javax.persistence.jdbc.password", encryptor.decrypt(databasePassword));
		*/
		
		// Second Level Cache
		jpaProperties.put("hibernate.cache.use_second_level_cache", true);
		jpaProperties.put("hibernate.cache.use_query_cache", true);
		jpaProperties.put("hibernate.cache.region.factory_class", "org.hibernate.cache.ehcache.EhCacheRegionFactory");
		
		/*
		jpaProperties.put("hibernate.connection.provider_class", "org.hibernate.c3p0.internal.C3P0ConnectionProvider");
		jpaProperties.put("hibernate.c3p0.min_size", 5);
		jpaProperties.put("hibernate.c3p0.max_size", 20);
		jpaProperties.put("hibernate.c3p0.timeout", 300);
		jpaProperties.put("hibernate.c3p0.max_statements", 50);
		jpaProperties.put("hibernate.c3p0.idle_test_period", 3000);
		*/
		
		jpaProperties.put("hibernate.dialect", "org.hibernate.dialect.PostgreSQLDialect");
		jpaProperties.put("hibernate.show_sql", true);
		jpaProperties.put("hibernate.hbm2ddl.auto", "none");
		
		jpaProperties.put("hibernate.search.default.directory_provider", "filesystem");
		jpaProperties.put("hibernate.search.default.indexBase", searchIndexBase);
	
        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setDataSource(dataSource);
        factory.setPackagesToScan("com.carefarm.prakrati.entity", "com.carefarm.prakrati.common.entity", "com.carefarm.prakrati.delivery.entity", "com.carefarm.prakrati.payment.entity");
        factory.setJpaVendorAdapter(jpaVendorAdapter());
        factory.setPersistenceProvider(new HibernatePersistenceProvider());
        factory.setJpaProperties(jpaProperties);
        factory.setPersistenceUnitName("bleugrain-PU");
        factory.afterPropertiesSet();
        return factory.getObject();
	}
	
	@Bean
	public JpaVendorAdapter jpaVendorAdapter() {
		HibernateJpaVendorAdapter jpaVendorAdapter = new HibernateJpaVendorAdapter();
		jpaVendorAdapter.setShowSql(Boolean.TRUE);
		jpaVendorAdapter.setGenerateDdl(Boolean.TRUE);
		return jpaVendorAdapter;
	}
	
	@Bean
	public PlatformTransactionManager transactionManager() {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setDataSource(dataSource);
		transactionManager.setEntityManagerFactory(entityManagerFactory());
		return transactionManager;
	}
	
	@Bean
	public JdbcTemplate jdbcTemplate() {
		return new JdbcTemplate(dataSource);
	}
	
	@Bean
	public NamedParameterJdbcTemplate namedParameterJdbcTemplate() {
		return new NamedParameterJdbcTemplate(dataSource);
	}
	
	@Bean
	public PersistenceExceptionTranslationPostProcessor persistenceExceptionTranslationPostProcessor() {
		return new PersistenceExceptionTranslationPostProcessor();
	}
	
	@Bean
	public MergingPersistentUnitPostProcessor mergingPersistentUnitPostProcessor() {
		return new MergingPersistentUnitPostProcessor();
	}
	
	/*
	@Bean 
	public PersistenceAnnotationBeanPostProcessor persistenceAnnotationBeanPostProcessor() {
		return new PersistenceAnnotationBeanPostProcessor();
	}
	*/
	
}
