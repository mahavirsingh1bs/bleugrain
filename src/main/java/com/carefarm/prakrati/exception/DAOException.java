package com.carefarm.prakrati.exception;

public class DAOException extends RuntimeException {

	private static final long serialVersionUID = -7638432912908467284L;

	public DAOException() {
    }

    public DAOException(String msg) {
        super(msg);
    }

    public DAOException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public DAOException(Throwable cause) {
        super(cause);
    }
}
