package com.carefarm.prakrati.exception;

public class MessagingException extends RuntimeException {
	private static final long serialVersionUID = -2672603008509067826L;

	public MessagingException() {
		super();
	}
	
	public MessagingException(String msg) {
		super(msg);
	}
	
	public MessagingException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
	public MessagingException(Throwable cause) {
		super(cause);
	}
}
