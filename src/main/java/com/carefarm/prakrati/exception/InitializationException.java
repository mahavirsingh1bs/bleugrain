package com.carefarm.prakrati.exception;

public class InitializationException extends RuntimeException {

	private static final long serialVersionUID = 1241626348223124724L;

	public InitializationException() {
		super();
	}
	
	public InitializationException(String msg) {
		super(msg);
	}
	
	public InitializationException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
	public InitializationException(Throwable cause) {
		super(cause);
	}
	
}
