package com.carefarm.prakrati.exception;

public class ServiceException extends RuntimeException {
	
	private static final long serialVersionUID = -2672603008509067826L;

	public ServiceException() {
		super();
	}
	
	public ServiceException(String msg) {
		super(msg);
	}
	
	public ServiceException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
	public ServiceException(Throwable cause) {
		super(cause);
	}
}

