package com.carefarm.prakrati.exception;

public class OldPasswordIncorrectException extends ServiceException {

	private static final long serialVersionUID = 2155852436682041537L;
	
	public OldPasswordIncorrectException() {
		super();
	}
	
	public OldPasswordIncorrectException(String msg) {
		super(msg);
	}
	
	public OldPasswordIncorrectException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
	public OldPasswordIncorrectException(Throwable cause) {
		super(cause);
	}

}
