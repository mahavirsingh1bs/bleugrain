package com.carefarm.prakrati.exception;

public class OTPVerificationException extends ServiceException {
	
	private static final long serialVersionUID = 1481829756861212318L;

	public OTPVerificationException() {
		super();
	}
	
	public OTPVerificationException(String msg) {
		super(msg);
	}
	
	public OTPVerificationException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
	public OTPVerificationException(Throwable cause) {
		super(cause);
	}
	
}
