package com.carefarm.prakrati.exception;

public class NewPasswdDoesntMatchException extends ServiceException {

	private static final long serialVersionUID = 7081565101559891686L;

	public NewPasswdDoesntMatchException() {
		super();
	}
	
	public NewPasswdDoesntMatchException(String msg) {
		super(msg);
	}
	
	public NewPasswdDoesntMatchException(String msg, Throwable cause) {
		super(msg, cause);
	}
	
	public NewPasswdDoesntMatchException(Throwable cause) {
		super(cause);
	}
}
