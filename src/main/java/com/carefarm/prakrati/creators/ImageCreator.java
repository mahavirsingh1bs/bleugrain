package com.carefarm.prakrati.creators;

import com.carefarm.prakrati.entity.Image;

public class ImageCreator {
	
	public static Image createInstance(String filename, String filepath, String...otherPaths) {
		Image image = new Image(filename, filepath);
		
		if (otherPaths.length > 0 && otherPaths[0] != null) {
			image.setSmallFilepath(otherPaths[0]);
		} else {
			image.setSmallFilepath(filepath);
		}
		
		if (otherPaths.length > 0 && otherPaths[1] != null) {
			image.setMediumFilepath(otherPaths[1]);
		} else {
			image.setMediumFilepath(filepath);
		}
		
		if (otherPaths.length > 0 && otherPaths[2] != null) {
			image.setLargeFilepath(otherPaths[2]);
		} else {
			image.setLargeFilepath(filepath);
		}
		
		return image;
	}
	
}
