package com.carefarm.prakrati.comparator;

import java.util.Comparator;

import org.apache.commons.lang3.math.NumberUtils;

import com.carefarm.prakrati.entity.Menuitem;

public class DisplayIndexComparator implements Comparator<Menuitem> {

	@Override
	public int compare(Menuitem m1, Menuitem m2) {
		return NumberUtils.compare(m1.getDisplayIndex(), m2.getDisplayIndex());
	}

}
