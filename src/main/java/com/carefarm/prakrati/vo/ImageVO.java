package com.carefarm.prakrati.vo;

import org.springframework.web.multipart.MultipartFile;

public class ImageVO {
	private String filename;
	private String origFilename;
	private String contentType;
	private long size;
	private byte[] data;
	private MultipartFile file;
	
	public ImageVO() { }

	public ImageVO(String filename, String origFilename, String contentType, long size, byte[] data) {
		super();
		this.filename = filename;
		this.origFilename = origFilename;
		this.contentType = contentType;
		this.size = size;
		this.data = data;
	}

	public String getFilename() {
		return filename;
	}

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getOrigFilename() {
		return origFilename;
	}

	public void setOrigFilename(String origFilename) {
		this.origFilename = origFilename;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public byte[] getData() {
		return data;
	}

	public void setData(byte[] data) {
		this.data = data;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}
	
}
