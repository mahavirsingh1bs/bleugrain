package com.carefarm.prakrati.vo;

import java.util.HashSet;
import java.util.Set;

import org.hibernate.validator.constraints.NotEmpty;

public class CompanyVO {
	@NotEmpty(message="Company Name is required")
	private String name;
	
	@NotEmpty(message="Registr No. is required")
	private String registrNo;
	
	private String phoneNo;
	
	private String email;
	
	private Set<CompanyAdminVO> admins;
	
	@NotEmpty(message="Address Line 1 is required")
	private String addressLine1;
	
	private String addressLine2;
	
	@NotEmpty(message="City is required")
	private String city;
	
	private long stateId;
	private long countryId;
	
	@NotEmpty(message="Zipcode is required")
	private String zipcode;
	
	public CompanyVO() { }
	
	public CompanyVO(String name, String registrNo, String phoneNo, String email) {
		this.name = name;
		this.registrNo = registrNo;
		this.phoneNo = phoneNo;
		this.email = email;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getRegistrNo() {
		return registrNo;
	}
	public void setRegistrNo(String registrNo) {
		this.registrNo = registrNo;
	}
	public String getPhoneNo() {
		return phoneNo;
	}
	public void setPhoneNo(String phoneNo) {
		this.phoneNo = phoneNo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}

	public Set<CompanyAdminVO> getAdmins() {
		return admins;
	}

	public void setAdmins(Set<CompanyAdminVO> admins) {
		this.admins = admins;
	}
	
	public void addAdmin(CompanyAdminVO admin) {
		if (this.admins == null) {
			this.admins = new HashSet<CompanyAdminVO>();
		}
		this.admins.add(admin);
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public long getStateId() {
		return stateId;
	}

	public void setStateId(long stateId) {
		this.stateId = stateId;
	}

	public long getCountryId() {
		return countryId;
	}

	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	
}
