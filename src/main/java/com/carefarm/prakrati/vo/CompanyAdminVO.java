package com.carefarm.prakrati.vo;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

public class CompanyAdminVO {
	@NotEmpty(message="First Name is required")
	private String firstName;
	private String middleInitial;
	private String lastName;
	
	@NotEmpty(message="Phone No. is required")
	private String mobileNo;
	
	@Email(message="Please provide a valid email address")
    @NotEmpty(message="Email Address is required")
	private String email;
	
	@NotEmpty(message="Address Line 1 is required")
	private String addressLine1;
	
	private String addressLine2;
	
	@NotEmpty(message="City is required")
	private String city;
	
	private long stateId;
	private long countryId;
	
	@NotEmpty(message="Zipcode is required")
	private String zipcode;

	public CompanyAdminVO() { }
	
	public CompanyAdminVO(String firstName, String lastName, String mobileNo,
			String email) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.mobileNo = mobileNo;
		this.email = email;
	}
	
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getMiddleInitial() {
		return middleInitial;
	}

	public void setMiddleInitial(String middleInitial) {
		this.middleInitial = middleInitial;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getAddressLine1() {
		return addressLine1;
	}

	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}

	public String getAddressLine2() {
		return addressLine2;
	}

	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public long getStateId() {
		return stateId;
	}

	public void setStateId(long stateId) {
		this.stateId = stateId;
	}

	public long getCountryId() {
		return countryId;
	}

	public void setCountryId(long countryId) {
		this.countryId = countryId;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	
}
