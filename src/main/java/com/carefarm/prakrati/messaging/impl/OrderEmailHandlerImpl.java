package com.carefarm.prakrati.messaging.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.carefarm.prakrati.messaging.OrderEmailHandler;
import com.carefarm.prakrati.messaging.params.OrderEmailParams;
import com.carefarm.prakrati.web.bean.OrderBean;
import com.carefarm.prakrati.web.bean.UserBean;

@Component
public class OrderEmailHandlerImpl extends AbstractEmailHandler implements OrderEmailHandler {	

	public OrderEmailHandlerImpl() throws IOException { }

	@Override
	public void sendOrderHasPlaced(OrderBean order, UserBean user) {
		String from = properties.getProperty("mail.order.placed.success.from");
		String subject = properties.getProperty("mail.order.placed.success.subject");
		String template = properties.getProperty("mail.order.placed.success.template");
		
		Map<String, Object> model = new HashMap<>();
		model.put("user", user);
		model.put("order", order);
		model.put("billingAddress", order.getBillingAddress());
		model.put("shippingAddress", order.getShippingAddress());
		model.put("contextPath", contextPath);
		
		OrderEmailParams params = new OrderEmailParams(user.getEmailId(), from, subject);
		params.setTemplateFile(template);
		params.setModel(model);
		params.setOrder(order);
		sendDynamicEmail(params);
	}

	@Override
	public void sendOrderHasShipped(OrderBean order, UserBean user) {
		String from = properties.getProperty("mail.order.shipped.success.from");
		String subject = properties.getProperty("mail.order.shipped.success.subject");
		String template = properties.getProperty("mail.order.shipped.success.template");
		
		Map<String, Object> model = new HashMap<>();
		model.put("user", user);
		model.put("order", order);
		model.put("contextPath", contextPath);
		
		OrderEmailParams params = new OrderEmailParams(user.getEmailId(), from, subject);
		params.setTemplateFile(template);
		params.setModel(model);
		sendDefaultEmail(params);
	}
	
	@Override
	public void sendOrderHasDispatched(OrderBean order, UserBean user) {
		String from = properties.getProperty("mail.order.dispatched.success.from");
		String subject = properties.getProperty("mail.order.dispatched.success.subject");
		String template = properties.getProperty("mail.order.dispatched.success.template");
		
		Map<String, Object> model = new HashMap<>();
		model.put("user", user);
		model.put("order", order);
		model.put("contextPath", contextPath);
		
		OrderEmailParams params = new OrderEmailParams(user.getEmailId(), from, subject);
		params.setTemplateFile(template);
		params.setModel(model);
		sendDefaultEmail(params);
	}
	
	@Override
	public void sendOrderHasAssigned(OrderBean order, UserBean user) {
		String from = properties.getProperty("mail.order.assigned.success.from");
		String subject = properties.getProperty("mail.order.assigned.success.subject");
		String template = properties.getProperty("mail.order.assigned.success.template");
		
		Map<String, Object> model = new HashMap<>();
		model.put("user", user);
		model.put("order", order);
		model.put("billingAddress", order.getBillingAddress());
		model.put("shippingAddress", order.getShippingAddress());
		model.put("contextPath", contextPath);
		
		OrderEmailParams params = new OrderEmailParams(user.getEmailId(), from, subject);
		params.setTemplateFile(template);
		params.setModel(model);
		params.setOrder(order);
		sendDynamicEmail(params);
	}
	
	@Override
	public void sendOrderHasDelivered(OrderBean order, UserBean user) {
		String from = properties.getProperty("mail.order.delivered.success.from");
		String subject = properties.getProperty("mail.order.delivered.success.subject");
		String template = properties.getProperty("mail.order.delivered.success.template");
		
		Map<String, Object> model = new HashMap<>();
		model.put("user", user);
		model.put("order", order);
		model.put("contextPath", contextPath);
		
		OrderEmailParams params = new OrderEmailParams(user.getEmailId(), from, subject);
		params.setTemplateFile(template);
		params.setModel(model);
		sendDefaultEmail(params);
	}

}
