package com.carefarm.prakrati.messaging.impl;

import java.io.IOException;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.util.Properties;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.messaging.EmailHandler;
import com.carefarm.prakrati.messaging.params.DefaultEmailParams;
import com.carefarm.prakrati.messaging.params.OrderEmailParams;
import com.carefarm.prakrati.messaging.params.SimpleEmailParams;
import com.carefarm.prakrati.web.bean.ItemBean;
import com.carefarm.prakrati.web.bean.OrderBean;
import com.carefarm.prakrati.web.bean.ProductBean;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

public abstract class AbstractEmailHandler implements EmailHandler {	
	
	@Value("${application.context.path}")
	protected String contextPath;

	@Autowired
	private Configuration configuration;
	
	@Autowired
	private JavaMailSenderImpl mailSender;
	
	protected Properties properties;
	
	public AbstractEmailHandler() throws IOException {
		Resource resource = new ClassPathResource("/application.properties");
		properties = PropertiesLoaderUtils.loadProperties(resource);
	}	

	@Override
	public void sendSimpleEmail(SimpleEmailParams params) {
		SimpleMailMessage message = new SimpleMailMessage();
		message.setFrom(params.getFrom());
		message.setTo(params.getEmailAddress());
		message.setSubject(params.getSubject());
		message.setText(params.getContent());
		mailSender.send(message);
	}
	
	@Override
	public void sendDefaultEmail(DefaultEmailParams params) {
		StringWriter writer = new StringWriter();
		try {
			Template template = configuration.getTemplate(params.getTemplateFile());
			template.process(params.getModel(), writer);
		} catch (TemplateException | IOException e) {
			e.printStackTrace();
		}
		
		mailSender.send(new MimeMessagePreparator() {
			   public void prepare(MimeMessage mimeMessage) throws MessagingException {
			     MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
			     message.setTo(params.emailAddress);
			     message.setFrom(params.from);
			     message.setSubject(params.subject);
			     message.setText(writer.toString(), true);
			     message.addInline("facebook", new ClassPathResource("static/assets/emails/img/icons/icon_facebook.png"));
			     message.addInline("twitter", new ClassPathResource("static/assets/emails/img/icons/icon_twitter.png"));
			     message.addInline("youtube", new ClassPathResource("static/assets/emails/img/icons/icon_youtube.png"));
			   }
			 });
	}


	protected void sendDynamicEmail(OrderEmailParams params) {
		StringWriter writer = new StringWriter();
		try {
			Template template = configuration.getTemplate(params.getTemplateFile());
			template.process(params.getModel(), writer);
		} catch (TemplateException | IOException e) {
			e.printStackTrace();
		}
		
		mailSender.send(new MimeMessagePreparator() {
			   public void prepare(MimeMessage mimeMessage) throws MessagingException, MalformedURLException {
			     MimeMessageHelper message = new MimeMessageHelper(mimeMessage, true, "UTF-8");
			     message.setTo(params.emailAddress);
			     message.setFrom(params.from);
			     message.setSubject(params.subject);
			     message.setText(writer.toString(), true);
			     addOrderImagesInline(params.getOrder(), message);
			   }
			});
	}

	private void addOrderImagesInline(OrderBean order,
			MimeMessageHelper message) throws MessagingException, MalformedURLException {
		if (Predicates.notNull.test(order.getProducts())) {
		     for (ProductBean product : order.getProducts()) {
		    	 message.addInline(product.getUniqueId(), new UrlResource(product.getDefaultImage().getSmallImgPath())); 
		     }
	     }
	     
	     if (Predicates.notNull.test(order.getItems())) {
		     for (ItemBean item : order.getItems()) {
		    	 message.addInline(item.getUniqueId(), new UrlResource(item.getImage().getSmallImgPath())); 
		     }
	     }
	}
	

}
