package com.carefarm.prakrati.messaging.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.generators.RandomPasswdGenerator;
import com.carefarm.prakrati.messaging.AccountEmailHandler;
import com.carefarm.prakrati.messaging.params.DefaultEmailParams;
import com.carefarm.prakrati.messaging.params.SimpleEmailParams;

@Component
@Primary
public class DefaultEmailHandler extends AbstractEmailHandler implements AccountEmailHandler {

	public DefaultEmailHandler() throws IOException { }

	@Override
	public void sendUserConcern(String from, String concern) {
		String to = properties.getProperty("mail.concern.subject");
		String subject = properties.getProperty("mail.concern.subject");
		
		SimpleEmailParams params = new SimpleEmailParams(to, from, subject);
		params.setContent(concern);
		sendSimpleEmail(params);
	}

	@Override
	public void sendFeedbackThanks(String to) {
		String subject = properties.getProperty("mail.feedback.subject");
		String from = properties.getProperty("mail.feedback.from");
		String content = properties.getProperty("mail.feedback.content");
		
		SimpleEmailParams params = new SimpleEmailParams(to, from, subject);
		params.setContent(content);
		sendSimpleEmail(params);
	}
	
	public void sendResetPassword(String emailAddress, String passwdResetUrl, User user) {
		String from = properties.getProperty("mail.password.reset.from");
		String subject = properties.getProperty("mail.password.reset.subject");
		String template = properties.getProperty("mail.password.reset.template");
		
		Map<String, Object> model = new HashMap<>();
		model.put("user", user);
		model.put("passwdResetUrl", passwdResetUrl);
		model.put("contextPath", contextPath);
		
		DefaultEmailParams params = new DefaultEmailParams(emailAddress, from, subject);
		params.setTemplateFile(template);
		params.setModel(model);
		sendDefaultEmail(params);
	}

	@Override
	public void notifyOnSuccessfulReset(String emailAddress, User user) {
		String from = properties.getProperty("mail.password.reset.success.from");
		String subject = properties.getProperty("mail.password.reset.success.subject");
		String template = properties.getProperty("mail.password.reset.success.template");
		
		Map<String, Object> model = new HashMap<>();
		model.put("user", user);
		model.put("contextPath", contextPath);
		
		DefaultEmailParams params = new DefaultEmailParams(emailAddress, from, subject);
		params.setTemplateFile(template);
		params.setModel(model);
		sendDefaultEmail(params);
	}

	@Override
	public void notifyOnSuccessfulChange(String emailAddress) {
		String from = properties.getProperty("mail.password.change.success.from");
		String subject = properties.getProperty("mail.password.change.success.subject");
		String content = properties.getProperty("mail.password.change.success.content");
		
		SimpleEmailParams params = new SimpleEmailParams(emailAddress, from, subject);
		params.setContent(content);
		sendSimpleEmail(params);
	}

	@Override
	public void verifyEmailAddress(String emailAddress, String verifyEmailPath, User currentUser) {
		String from = properties.getProperty("mail.verify.email.success.from");
		String subject = properties.getProperty("mail.verify.email.success.subject");
		String template = properties.getProperty("mail.verify.email.success.template");
		
		Map<String, Object> model = new HashMap<>();
		model.put("user", currentUser);
		model.put("verifyEmailPath", verifyEmailPath);
		model.put("contextPath", contextPath);
		
		DefaultEmailParams params = new DefaultEmailParams(emailAddress, from, subject);
		params.setTemplateFile(template);
		params.setModel(model);
		sendDefaultEmail(params);
	}
	
	@Override
	public void sendVerificationLink(String emailAddress, String verifyEmailPath, User user) {
		String from = properties.getProperty("mail.verify.email.link.from");
		String subject = properties.getProperty("mail.verify.email.link.subject");
		String template = properties.getProperty("mail.verify.email.link.template");
		
		Map<String, Object> model = new HashMap<>();
		model.put("user", user);
		model.put("verifyEmailPath", verifyEmailPath);
		model.put("contextPath", contextPath);
		
		DefaultEmailParams params = new DefaultEmailParams(emailAddress, from, subject);
		params.setTemplateFile(template);
		params.setModel(model);
		sendDefaultEmail(params);
	}

	@Override
	public void accountCreatedEmail(String emailAddress, String verifyEmailPath, String resetPasswdPath, User currentUser) {
		String from = properties.getProperty("account.created.email.success.from");
		String subject = properties.getProperty("account.created.email.success.subject");
		String template = properties.getProperty("account.created.email.success.template");
		
		Map<String, Object> model = new HashMap<>();
		model.put("user", currentUser);
		model.put("contextPath", contextPath);
		model.put("verifyEmailPath", verifyEmailPath);
		model.put("resetPasswdPath", resetPasswdPath);
		model.put("oneTimePassword", RandomPasswdGenerator.nextRandomPasswd());
		
		DefaultEmailParams params = new DefaultEmailParams(emailAddress, from, subject);
		params.setTemplateFile(template);
		params.setModel(model);
		sendDefaultEmail(params);
	}
	
}
