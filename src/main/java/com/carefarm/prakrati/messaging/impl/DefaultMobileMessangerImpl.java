package com.carefarm.prakrati.messaging.impl;

import java.io.UnsupportedEncodingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.carefarm.prakrati.exception.MessagingException;
import com.carefarm.prakrati.messaging.MobileMessanger;
import com.carefarm.prakrati.security.Encryptor;

@Component
public class DefaultMobileMessangerImpl implements MobileMessanger {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultMobileMessangerImpl.class);

	private static final String ENCODING = "UTF-8";
	
	@Value("${messaging.base.url}")
	private String baseUrl;
	
	@Value("${messaging.workingkey}")
	private String workingKey;
	
	@Value("${messaging.sender}")
	private String sender;
	
	@Autowired
	private Encryptor encryptor;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Override
	public String sendMessage(String mobileNo, String message) {
		UriComponents uri = UriComponentsBuilder
				.fromHttpUrl(encryptor.decrypt(baseUrl))
				.queryParam("apikey", encryptor.decrypt(workingKey))
				.queryParam("sender", encryptor.decrypt(sender))
				.queryParam("to", mobileNo)
				.queryParam("message", message)
				.build();
		
			String response = null;
			try {
				LOGGER.info("Sending a message to: " + uri.toUri().toString());
				response = restTemplate.getForObject(uri.encode(ENCODING).toUri(), String.class);
			} catch (RestClientException | UnsupportedEncodingException e) {
				LOGGER.error("We got an exception when we are trying to send a message to: " + mobileNo);
				throw new MessagingException("We got an exception when we are trying to send a message to: " + mobileNo, e.getCause());
			}
			return response;
	}
}
