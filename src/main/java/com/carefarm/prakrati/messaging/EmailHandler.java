package com.carefarm.prakrati.messaging;

import com.carefarm.prakrati.messaging.params.DefaultEmailParams;
import com.carefarm.prakrati.messaging.params.SimpleEmailParams;


public interface EmailHandler {
	
	void sendSimpleEmail(SimpleEmailParams params);
	
	void sendDefaultEmail(DefaultEmailParams params);
}