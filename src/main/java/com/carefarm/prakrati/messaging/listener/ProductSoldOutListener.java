package com.carefarm.prakrati.messaging.listener;

import org.apache.camel.InOnly;

import com.carefarm.prakrati.web.bean.ProductBean;

public interface ProductSoldOutListener {

	@InOnly
	void notify(ProductBean product);
}