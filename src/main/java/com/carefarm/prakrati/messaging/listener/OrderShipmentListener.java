package com.carefarm.prakrati.messaging.listener;

import org.apache.camel.InOnly;

import com.carefarm.prakrati.web.bean.OrderBean;

public interface OrderShipmentListener {

	@InOnly
	void notify(OrderBean order);
	
}
