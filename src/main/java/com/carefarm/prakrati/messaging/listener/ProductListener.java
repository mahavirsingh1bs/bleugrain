package com.carefarm.prakrati.messaging.listener;

import org.apache.camel.InOnly;

import com.carefarm.prakrati.web.bean.ProductBean;

public interface ProductListener {

	@InOnly
	void sendBiddableProduct(ProductBean product);
}
