package com.carefarm.prakrati.messaging.listener;

import org.apache.camel.InOnly;

import com.carefarm.prakrati.web.bean.DemandBean;

public interface DemandFulfilledListener {

	@InOnly
	void notify(DemandBean product);
}