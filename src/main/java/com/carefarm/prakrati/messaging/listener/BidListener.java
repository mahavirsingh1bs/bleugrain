package com.carefarm.prakrati.messaging.listener;

import org.apache.camel.InOnly;

import com.carefarm.prakrati.web.bean.BidBean;

public interface BidListener {

	@InOnly
	void sendNotification(BidBean bidBean);
}
