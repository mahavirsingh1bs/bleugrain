package com.carefarm.prakrati.messaging.listener;

import org.apache.camel.InOnly;

import com.carefarm.prakrati.web.bean.DemandBean;

public interface DemandAssignedListener {

	@InOnly
	void notify(DemandBean product);
}
