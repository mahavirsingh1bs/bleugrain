package com.carefarm.prakrati.messaging.listener;

import org.apache.camel.InOnly;

import com.carefarm.prakrati.service.dto.NotificationDTO;

public interface NotificationListener {

	@InOnly
	void sendNotification(NotificationDTO notificationDTO);
}
