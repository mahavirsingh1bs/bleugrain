package com.carefarm.prakrati.messaging;

import com.carefarm.prakrati.web.bean.OrderBean;
import com.carefarm.prakrati.web.bean.UserBean;

public interface OrderEmailHandler extends EmailHandler {
	
	void sendOrderHasPlaced(OrderBean order, UserBean createdBy);
	
	void sendOrderHasShipped(OrderBean order, UserBean user);
	
	void sendOrderHasDispatched(OrderBean order, UserBean user);
	
	void sendOrderHasAssigned(OrderBean order, UserBean user);
	
	void sendOrderHasDelivered(OrderBean order, UserBean user);

}
