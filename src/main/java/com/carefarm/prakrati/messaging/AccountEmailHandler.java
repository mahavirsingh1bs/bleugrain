package com.carefarm.prakrati.messaging;

import com.carefarm.prakrati.entity.User;

public interface AccountEmailHandler extends EmailHandler {

	void sendUserConcern(String from, String concern);
	
	void sendFeedbackThanks(String to);
	
	void sendResetPassword(String emailAddress, String passwdResetUrl, User user);

	void verifyEmailAddress(String emailAddress, String contextPath, User currentUser);
	
	void sendVerificationLink(String emailAddress, String contextPath, User user);
	
	void accountCreatedEmail(String emailAddress, String verifyEmailPath, String resetPasswdPath, User currentUser);
	
	void notifyOnSuccessfulReset(String emailAddress, User user);
	
	void notifyOnSuccessfulChange(String email);
	
}
