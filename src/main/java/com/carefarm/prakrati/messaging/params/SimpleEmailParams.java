package com.carefarm.prakrati.messaging.params;

public class SimpleEmailParams extends AbstractEmailParams {
	public String content;

	public SimpleEmailParams(String emailAddress, String from, String subject) {
		super(emailAddress, from, subject);
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
}