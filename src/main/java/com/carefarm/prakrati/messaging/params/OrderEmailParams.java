package com.carefarm.prakrati.messaging.params;

import com.carefarm.prakrati.web.bean.OrderBean;

public class OrderEmailParams extends DefaultEmailParams {
	private OrderBean order;
	
	public OrderEmailParams(String emailAddress, String from, String subject) {
		super(emailAddress, from, subject);
	}

	public OrderBean getOrder() {
		return order;
	}

	public void setOrder(OrderBean order) {
		this.order = order;
	}
	
}