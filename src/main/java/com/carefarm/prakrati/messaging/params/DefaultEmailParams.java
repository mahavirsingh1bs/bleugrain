package com.carefarm.prakrati.messaging.params;

import java.util.Map;

public class DefaultEmailParams extends AbstractEmailParams {
	private String templateFile;
	private Map<String, Object> model;

	public DefaultEmailParams(String emailAddress, String from, String subject) {
		super(emailAddress, from, subject);
	}

	public String getTemplateFile() {
		return templateFile;
	}

	public void setTemplateFile(String templateFile) {
		this.templateFile = templateFile;
	}

	public Map<String, Object> getModel() {
		return model;
	}

	public void setModel(Map<String, Object> model) {
		this.model = model;
	}
	
}