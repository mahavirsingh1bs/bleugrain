package com.carefarm.prakrati.messaging.params;

public class AbstractEmailParams {

	public String emailAddress;
	public String from;
	public String subject;

	public AbstractEmailParams(String emailAddress, String from, String subject) {
		this.emailAddress = emailAddress;
		this.from = from;
		this.subject = subject;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public String getFrom() {
		return from;
	}

	public String getSubject() {
		return subject;
	}

}