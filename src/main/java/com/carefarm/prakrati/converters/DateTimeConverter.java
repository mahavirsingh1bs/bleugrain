package com.carefarm.prakrati.converters;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.dozer.CustomConverter;

public class DateTimeConverter implements CustomConverter {

	private static final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
	
	@Override
	public Object convert(Object destination, Object source, Class<?> destClass, Class<?> sourceClass) {
		if (source == null) {
			return null;
		}
		
		if (source instanceof Date) {
			return DATE_TIME_FORMAT.format((Date)source);
		}
		return source;
	}

}
