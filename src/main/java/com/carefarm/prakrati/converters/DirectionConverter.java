package com.carefarm.prakrati.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.data.domain.Sort.Direction;

public class DirectionConverter implements Converter<String, Direction> {

	@Override
	public Direction convert(String source) {
		return Direction.valueOf(source.toUpperCase());
	}
	
}
