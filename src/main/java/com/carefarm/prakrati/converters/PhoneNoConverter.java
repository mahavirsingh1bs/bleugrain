package com.carefarm.prakrati.converters;

import static com.carefarm.prakrati.common.util.Constants.PLUS;

import org.dozer.CustomConverter;

public class PhoneNoConverter implements CustomConverter {

	@Override
	public Object convert(Object destination, Object source, Class<?> destClass, Class<?> sourceClass) {
		if (source == null) {
			return null;
		}
		
		if (source instanceof String && !((String) source).startsWith(PLUS)) {
			return PLUS + ((String) source);
		}
		return source;
	}

}
