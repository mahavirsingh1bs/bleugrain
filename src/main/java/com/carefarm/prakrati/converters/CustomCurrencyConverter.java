package com.carefarm.prakrati.converters;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.dozer.CustomConverter;
import org.dozer.MappingException;

public class CustomCurrencyConverter implements CustomConverter {
	private NumberFormat customFormatter = null;
	private Locale locale = Locale.US;
	
	@Override
	public Object convert(Object destination, Object source, Class<?> destClass, Class<?> sourceClass) {
		if (source == null) {
			return null;
		}
		
		if (source instanceof String && !NumberUtils.isParsable(source.toString())) {
			throw new MappingException("CustomCurrencyConverter has been used"
					+ " incorrectly. Arguments passed in were: " + destination + " and " + source);
		}
		
		if (source instanceof Integer || source instanceof Long) {
			customFormatter = NumberFormat.getInstance(locale);
			return StringUtils.trim(customFormatter.format(source));
		} else if (source instanceof Float || source instanceof Double) {
			customFormatter = DecimalFormat.getInstance(locale);
			return StringUtils.trim(customFormatter.format(source));
		} else if (source instanceof BigDecimal) {
			customFormatter = NumberFormat.getCurrencyInstance();
			return StringUtils.trim(customFormatter.format(source));
		} else if (source instanceof String) {
			customFormatter = DecimalFormat.getInstance(locale);
			return StringUtils.trim(customFormatter.format(source));
		} else {
			throw new MappingException("CustomCurrencyConverter has been used"
					+ " incorrectly. Arguments passed in were: " + destination + " and " + source);
		}
	}

}
