package com.carefarm.prakrati.converters;

import org.springframework.core.convert.converter.Converter;

import com.carefarm.prakrati.web.model.CompanyForm;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CompanyConverter implements Converter<String, CompanyForm> {

	private ObjectMapper objectMapper;
	
	public CompanyConverter() {
		objectMapper = new ObjectMapper();
	    objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}
	
	@Override
	public CompanyForm convert(String company) {
		return objectMapper.convertValue(company, CompanyForm.class);
	}

}
