package com.carefarm.prakrati.converters;

import org.apache.commons.lang3.StringUtils;
import org.dozer.CustomConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.security.Encryptor;

@Component
public class PaymentDecryptor implements CustomConverter {

	private static final String ACCOUNT_NO_PREFIX = "**********";
	
	@Autowired
	private Encryptor encryptor;
	
	@Override
	public Object convert(Object destination, Object source, Class<?> destClass, Class<?> sourceClass) {
		if (source == null) {
			return null;
		}
		
		String decrypted = encryptor.decrypt(source.toString());
		String encryptedAccount = ACCOUNT_NO_PREFIX.concat(decrypted.substring(decrypted.length() - 4));
		return StringUtils.trim(encryptedAccount);
	}

}
