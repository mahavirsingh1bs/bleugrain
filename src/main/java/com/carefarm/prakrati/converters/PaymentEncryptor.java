package com.carefarm.prakrati.converters;

import org.apache.commons.lang3.StringUtils;
import org.dozer.CustomConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.security.Encryptor;

@Component
public class PaymentEncryptor implements CustomConverter {

	@Autowired
	private Encryptor encryptor;
	
	@Override
	public Object convert(Object destination, Object source, Class<?> destClass, Class<?> sourceClass) {
		if (source == null) {
			return null;
		}
		
		return StringUtils.trim(encryptor.encrypt(source.toString()));
	}

}
