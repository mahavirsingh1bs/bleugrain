package com.carefarm.prakrati.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.carefarm.prakrati.entity.Bid;

@Repository
public interface BidRepository extends PrakratiRepository<Bid, Long> {

	@Query("SELECT b FROM Bid b LEFT OUTER JOIN b.product p WHERE p.uniqueId = :uniqueId")
	List<Bid> findByUniqueId(@Param("uniqueId") String uniqueId);
	
	@Query("SELECT b FROM Bid b LEFT OUTER JOIN b.product p WHERE p.uniqueId = :uniqueId ORDER BY b.price DESC")
	List<Bid> findByUniqueId(@Param("uniqueId") String uniqueId, Pageable pageable);
	
	@Query("SELECT b FROM Bid b LEFT OUTER JOIN b.user u LEFT OUTER JOIN b.product p WHERE u.userNo = :userNo GROUP BY p.uniqueId ORDER BY b.price desc")
	List<Bid> findByUserNo(@Param("userNo") String userNo, Pageable pageable);
}
