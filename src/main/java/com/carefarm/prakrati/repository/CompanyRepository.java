package com.carefarm.prakrati.repository;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.carefarm.prakrati.entity.Company;

public interface CompanyRepository extends PrakratiRepository<Company, Long> {
	
	@Query("SELECT count(c) from Company c WHERE c.archived = false")
	long countAll();
	
	@Query("SELECT c from Company c WHERE c.uniqueId in (:companies) AND c.archived = false")
	Set<Company> findByUniqueIds(@Param("companies") List<String> companies);
	
	@Query("SELECT count(c) from Company c WHERE c.dateCreated < :createdOn AND c.archived = false")
	long countByDateCreated(@Param("createdOn") Date createdOn);
	
	@Query("SELECT c from Company c WHERE c.uniqueId = :uniqueId")
	Company findByUniqueId(@Param("uniqueId") String uniqueId);
	
	@Query("SELECT c from Company c WHERE c.domain = :domain")
	Company findByDomain(@Param("domain") String domain);
	
	@Query("SELECT COUNT(c) from Company c WHERE c.archived = false")
	Long countAllActiveCompanies();
	
	@Query("SELECT c from Company c WHERE c.archived = false")
	List<Company> findAllActiveCompanies(Pageable pageable);
	
	@Query("SELECT c from Company c JOIN FETCH c.agents JOIN FETCH c.images WHERE c.id = :companyId")
	Company findCompanyById(@Param("companyId") Long companyId);
	
	@Query("UPDATE Company c SET c.archived = :archive WHERE c.uniqueId = :uniqueId")
	Company archive(@Param("archive") boolean archive, @Param("uniqueId") String uniqueId);
	
	@Query("SELECT c FROM Company c where c.name like :keywords AND c.archived = false")
	List<Company> findByMatchingName(@Param("keywords") String keywords);
}
