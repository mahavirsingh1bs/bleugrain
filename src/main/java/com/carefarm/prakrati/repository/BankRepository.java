package com.carefarm.prakrati.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import com.carefarm.prakrati.entity.Bank;

public interface BankRepository extends PrakratiRepository<Bank, Long> {

	@Query("SELECT count(b) from Bank b WHERE b.archived = false")
	Long countAllActiveOnes();
	
	@Query("SELECT b from Bank b WHERE b.archived = false")
	List<Bank> findAllActiveOnes(Pageable pageable);
}
