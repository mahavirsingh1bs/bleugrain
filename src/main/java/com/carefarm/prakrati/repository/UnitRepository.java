package com.carefarm.prakrati.repository;

import org.springframework.stereotype.Repository;

import com.carefarm.prakrati.common.entity.Unit;

@Repository
public interface UnitRepository extends PrakratiRepository<Unit, Long> {

}
