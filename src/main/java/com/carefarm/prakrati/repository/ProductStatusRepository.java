package com.carefarm.prakrati.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.carefarm.prakrati.common.entity.ProductStatus;

public interface ProductStatusRepository extends PrakratiRepository<ProductStatus, Long> {

	@Query("SELECT ps FROM ProductStatus ps WHERE ps.status = :status")
	ProductStatus findByStatus(@Param("status") String status);
	
	ProductStatus findByCode(String code);
}
