package com.carefarm.prakrati.repository;

import com.carefarm.prakrati.entity.Feedback;

public interface FeedbackRepository extends PrakratiRepository<Feedback, Long> {

}
