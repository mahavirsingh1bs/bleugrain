package com.carefarm.prakrati.repository;

import com.carefarm.prakrati.delivery.entity.Transport;

public interface TransportRepository extends PrakratiRepository<Transport, Long> {
	Transport findByUniqueId(String uniqueId);
}
