package com.carefarm.prakrati.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import com.carefarm.prakrati.entity.PaymentCard;

public interface PaymentCardRepository extends PrakratiRepository<PaymentCard, Long> {

	@Query("SELECT count(pc) from PaymentCard pc WHERE pc.archived = false")
	Long countAllActiveOnes();
	
	@Query("SELECT pc from PaymentCard pc WHERE pc.archived = false")
	List<PaymentCard> findAllActiveOnes(Pageable pageable);
}
