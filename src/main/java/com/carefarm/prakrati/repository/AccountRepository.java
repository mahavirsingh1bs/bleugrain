package com.carefarm.prakrati.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.carefarm.prakrati.entity.Account;

public interface AccountRepository extends PrakratiRepository<Account, Long> {

	@Query("SELECT a FROM Account a WHERE a.owner.id = :userId AND a.isActive = :status")
	public List<Account> findAccountsByUserIdAndStatus(@Param("userId") Long userId, 
			@Param("status") Boolean status);
}
