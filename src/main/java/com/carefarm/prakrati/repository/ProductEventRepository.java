package com.carefarm.prakrati.repository;

import org.springframework.stereotype.Repository;

import com.carefarm.prakrati.entity.ProductEvent;

@Repository
public interface ProductEventRepository extends PrakratiRepository<ProductEvent, Long> {

}