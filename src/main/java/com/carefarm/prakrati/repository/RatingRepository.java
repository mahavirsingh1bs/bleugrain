package com.carefarm.prakrati.repository;

import com.carefarm.prakrati.common.entity.Rating;

public interface RatingRepository extends PrakratiRepository<Rating, Long> {
	
	Rating findByValue(Double value);
}
