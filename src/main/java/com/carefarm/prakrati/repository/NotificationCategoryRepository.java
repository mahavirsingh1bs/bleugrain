package com.carefarm.prakrati.repository;

import org.springframework.stereotype.Repository;

import com.carefarm.prakrati.entity.NotificationCategory;

@Repository
public interface NotificationCategoryRepository extends PrakratiRepository<NotificationCategory, Long> {

	NotificationCategory findByCode(String code);
	
	NotificationCategory findByName(String name);
}
