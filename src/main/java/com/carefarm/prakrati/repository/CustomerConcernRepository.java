package com.carefarm.prakrati.repository;

import com.carefarm.prakrati.entity.CustomerConcern;

public interface CustomerConcernRepository extends PrakratiRepository<CustomerConcern, Long> {

}
