package com.carefarm.prakrati.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.carefarm.prakrati.entity.Tag;

public interface TagRepository extends PrakratiRepository<Tag, Long> {
	
	@Query("SELECT t FROM Tag t where t.name like :keywords and t.archived = false")
	List<Tag> findByMatchingName(@Param("keywords") String keywords);
	
}
