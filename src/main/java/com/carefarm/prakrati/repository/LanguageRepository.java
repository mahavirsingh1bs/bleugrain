package com.carefarm.prakrati.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.carefarm.prakrati.entity.Language;

public interface LanguageRepository extends PrakratiRepository<Language, Long> {
	
	Language findByCode(String code);
	
	@Query("SELECT l FROM Language l where l.name like :keywords AND l.archived = false")
	List<Language> findByMatchingName(@Param("keywords") String keywords);
	
}
