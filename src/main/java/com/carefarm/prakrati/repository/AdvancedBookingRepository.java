package com.carefarm.prakrati.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.carefarm.prakrati.entity.AdvancedBooking;

public interface AdvancedBookingRepository extends PrakratiRepository<AdvancedBooking, Long> {
	
	@Query("SELECT ab FROM AdvancedBooking ab ORDER BY ab.dateCreated DESC")
	List<AdvancedBooking> findAllOrderByDateCreatedDesc();
	
}
