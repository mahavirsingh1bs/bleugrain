package com.carefarm.prakrati.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.carefarm.prakrati.entity.Branch;

public interface BranchRepository extends PrakratiRepository<Branch, Long> {
	
	@Query("SELECT COUNT(b) FROM Branch b WHERE b.company.id = :companyId")
	Long countByCompanyId(@Param("companyId") Long companyId);
	
	@Query("SELECT b FROM Branch b WHERE b.company.id = :companyId")
	Page<Branch> findByCompanyId(@Param("companyId") Long companyId, Pageable pageable);
}
