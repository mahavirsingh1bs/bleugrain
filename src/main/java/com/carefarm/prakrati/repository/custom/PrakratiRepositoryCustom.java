package com.carefarm.prakrati.repository.custom;

import java.io.Serializable;

import org.springframework.data.repository.NoRepositoryBean;

@NoRepositoryBean
public interface PrakratiRepositoryCustom<T, ID extends Serializable> {

}
