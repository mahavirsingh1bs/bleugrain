package com.carefarm.prakrati.repository.custom;

import java.util.List;

import com.carefarm.prakrati.entity.Product;
import com.carefarm.prakrati.repository.results.CategoryWiseReviews;

public interface ProductRepositoryCustom extends PrakratiRepositoryCustom<Product, Long> {
	List<CategoryWiseReviews> findCategoryWiseReviews();
}
