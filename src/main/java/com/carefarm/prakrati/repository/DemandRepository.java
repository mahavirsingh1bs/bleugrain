package com.carefarm.prakrati.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.carefarm.prakrati.entity.Demand;
import com.carefarm.prakrati.util.DemandStatus;

@Repository
public interface DemandRepository extends PrakratiRepository<Demand, Long> {

	Demand findByUniqueId(String uniqueId);
	
	@Query("SELECT count(d) FROM Demand d WHERE d.status.code = :code AND d.archived = false")
	long countByCode(@Param("code") String code);
	
	@Query("SELECT count(d) FROM Demand d WHERE d.status.code IN (:codes) AND d.archived = false")
	long countByStatusCodes(@Param("codes") String... codes);
	
	@Query("SELECT count(d) FROM Demand d JOIN d.assignedTo a WHERE a.userNo = :userNo AND d.status.code IN (:codes) AND d.archived = false")
	long countDemandsAssignedToUserWhereStatusCodes(@Param("userNo") String userNo, @Param("codes") String... codes);
	
	@Query("SELECT count(d) FROM Demand d JOIN d.assignedTo a WHERE a.userNo = :userNo AND d.dateAssigned <= :assignedOn AND d.status.code IN (:codes) AND d.archived = false")
	long countDemandsAssignedToUserWhereAssignDateAndStatusCodes(@Param("assignedOn") Date assignedOn, @Param("userNo") String userNo, @Param("codes") String... codes);
	
	@Query("SELECT d FROM Demand d JOIN d.user u WHERE u.id = :userId AND d.archived = false")
	List<Demand> findByUserId(@Param("userId") Long userId, Pageable pageable);
	
	@Query("SELECT d FROM Demand d JOIN d.user u WHERE u.id = :userId AND d.status = :status AND d.archived = false")
	List<Demand> findByUserIdAndStatus(@Param("userId") Long userId, 
			@Param("status") DemandStatus status);
	
	@Query("SELECT d FROM Demand d JOIN d.user u WHERE u.userNo = :userNo AND d.status.code = :code AND d.archived = false")
	List<Demand> findByUserNoAndStatus(@Param("userNo") String userNo, @Param("code") String code);
	
	@Query("SELECT d FROM Demand d JOIN d.user u WHERE u.userNo = :userNo AND d.status.code in (:codes) AND d.archived = false")
	List<Demand> findByUserNoAndStatusCodes(@Param("userNo") String userNo, @Param("codes") String... codes);
	
	@Query("SELECT d FROM Demand d JOIN d.createdBy u WHERE u.userNo = :userNo AND d.status.code = :code AND d.archived = false")
	List<Demand> findByCreatedByAndStatus(@Param("userNo") String userNo, @Param("code") String code);
	
	@Query("SELECT count(d) FROM Demand d JOIN d.company c JOIN d.status s WHERE c.uniqueId = :companyId AND s.code in (:codes) AND d.mocked = false AND d.archived = false")
	Long countByCompanyIdAndStatusCodes(@Param("companyId") String companyId, @Param("codes") String... codes);
	
	@Query("SELECT d FROM Demand d JOIN d.company c JOIN d.status s WHERE c.uniqueId = :companyId AND s.code in (:codes) AND d.mocked = false AND d.archived = false")
	List<Demand> findByCompanyIdAndStatusCodes(@Param("companyId") String companyId, @Param("codes") String[] codes, Pageable pageable);

	@Query("SELECT count(*) FROM Demand d JOIN d.status s JOIN d.user u JOIN d.assignedTo a WHERE (u.userNo = :userNo AND a.userNo = :userNo) AND s.status = :status AND d.archived = false")
	Long countByUserNoAndStatus(@Param("userNo") String userId, @Param("status") String status);
	
	@Query("SELECT d FROM Demand d JOIN d.status s JOIN d.user u JOIN d.assignedTo a WHERE (u.userNo = :userNo AND a.userNo = :userNo) AND s.status = :status AND d.archived = false")
	List<Demand> findByUserNoAndStatus(@Param("userNo") String userNo, @Param("status") String status, Pageable pageable);
	
	@Query("SELECT count(*) FROM Demand d JOIN d.status s LEFT JOIN d.user u LEFT JOIN d.assignedTo a WHERE (u.userNo = :userNo OR a.userNo = :userNo) AND s.code IN (:codes) AND d.mocked = false AND d.archived = false")
	Long countByUserNoAndStatusCodes(@Param("userNo") String userId, @Param("codes") String... codes);
	
	@Query("SELECT d FROM Demand d JOIN d.status s LEFT JOIN d.user u LEFT JOIN d.assignedTo a WHERE (u.userNo = :userNo OR a.userNo = :userNo) AND s.code IN (:codes) AND d.mocked = false AND d.archived = false")
	List<Demand> findByUserNoAndStatusCodes(@Param("userNo") String userNo, @Param("codes") String[] codes, Pageable pageable);
	
	@Query("SELECT count(*) FROM Demand d JOIN d.status s WHERE s.status = :status AND d.mocked = false AND d.archived = false")
	Long countAllOpenOnes(@Param("status") String status);
	
	@Query("SELECT d FROM Demand d JOIN d.status s WHERE s.status = :status AND d.mocked = false AND d.archived = false")
	List<Demand> findAllOpenOnes(@Param("status") String status, Pageable pageable);
	
	@Query("SELECT count(d) FROM Demand d JOIN d.category c WHERE (c.uniqueId = :categoryId OR c.parentCategory.uniqueId = :categoryId) AND d.archived = false")
	long countByCategoryId(@Param("categoryId") String categoryId);

	@Query("SELECT count(d) FROM Demand d JOIN d.category c LEFT OUTER JOIN c.parentCategory pc WHERE (c.uniqueId = :categoryId OR pc.uniqueId = :categoryId) AND d.status.code in (:codes) AND d.mocked = false AND d.archived = false")
	long countByCategoryIdAndStatusCodes(@Param("categoryId") String categoryId, @Param("codes") String... codes);
	
	@Query("SELECT count(d) FROM Demand d JOIN d.category c LEFT OUTER JOIN c.parentCategory pc WHERE d.name like :keywords AND (c.uniqueId = :categoryId OR pc.uniqueId = :categoryId) AND d.status.code in (:codes) AND d.mocked = false AND d.archived = false")
	long countByKeywordsAndCategoryIdAndStatusCodes(@Param("keywords") String keywords, @Param("categoryId") String categoryId, @Param("codes") String... codes);
	
	@Query("SELECT d FROM Demand d JOIN d.category c LEFT OUTER JOIN c.parentCategory pc WHERE (c.uniqueId = :categoryId OR pc.uniqueId = :categoryId) AND d.status.code in (:codes) AND d.mocked = false AND d.archived = false")
	List<Demand> findByCategoryIdAndStatusCodes(@Param("categoryId") String categoryId, @Param("codes") String[] codes, Pageable pageable);
	
	@Query("SELECT d FROM Demand d JOIN d.category c LEFT OUTER JOIN c.parentCategory pc WHERE d.name like :keywords AND (c.uniqueId = :categoryId OR pc.uniqueId = :categoryId) AND d.status.code in (:codes) AND d.mocked = false AND d.archived = false")
	List<Demand> findByKeywordsAndCategoryIdAndStatusCodes(@Param("keywords") String keywords, @Param("categoryId") String categoryId, @Param("codes") String[] codes, Pageable pageable);
	
}
