package com.carefarm.prakrati.repository;

import java.util.List;
import java.util.Set;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.carefarm.prakrati.entity.Group;

public interface GroupRepository extends PrakratiRepository<Group, Long> {

	Group findByUniqueId(String uniqueId);
	
	Group findGroupByName(String name);
	
	@Query("SELECT count(g) from Group g WHERE g.archived = false")
	Long countAllActiveOnes();
	
	@Query("SELECT g from Group g WHERE g.name in (:groups) AND g.archived = false")
	Set<Group> findByNames(@Param("groups") List<String> groups);
	
	@Query("SELECT g from Group g WHERE g.archived = false")
	List<Group> findAllActiveOnes(Pageable pageable);
	
	@Query("SELECT g FROM Group g where g.name like :keywords AND g.archived = false")
	List<Group> findByMatchingName(@Param("keywords") String keywords);
	
}
