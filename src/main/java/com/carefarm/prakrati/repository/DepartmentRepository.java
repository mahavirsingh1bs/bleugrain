package com.carefarm.prakrati.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import com.carefarm.prakrati.entity.Department;

public interface DepartmentRepository extends PrakratiRepository<Department, Long> {

	@Query("SELECT count(d) from Department d WHERE d.archived = false")
	Long countAllActiveOnes();
	
	@Query("SELECT d from Department d WHERE d.archived = false")
	List<Department> findAllActiveOnes(Pageable pageable);
}
