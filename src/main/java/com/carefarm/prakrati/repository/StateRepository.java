package com.carefarm.prakrati.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.carefarm.prakrati.common.entity.State;

public interface StateRepository extends PrakratiRepository<State, Long> {

	@Query("SELECT s FROM State s WHERE s.country.id = :countryId")
	public List<State> findStatesByCountryId(@Param("countryId") Long countryId);
	
	@Query("SELECT s FROM State s WHERE s.name LIKE %:keyword%")
	public List<State> findByNameContains(@Param("keyword") String keyword);
	
	@Query("SELECT count(s) from State s WHERE s.archived = false")
	Long countAllActiveOnes();
	
	@Query("SELECT s from State s WHERE s.archived = false")
	List<State> findAllActiveOnes(Pageable pageable);
}
