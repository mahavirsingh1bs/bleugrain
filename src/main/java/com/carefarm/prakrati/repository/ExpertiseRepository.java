package com.carefarm.prakrati.repository;

import com.carefarm.prakrati.entity.Expertise;

public interface ExpertiseRepository extends PrakratiRepository<Expertise, Long> {
	
	Expertise findByUniqueId(String uniqueId);
}
