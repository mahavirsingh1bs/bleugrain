package com.carefarm.prakrati.repository;

import org.springframework.stereotype.Repository;

import com.carefarm.prakrati.common.entity.Currency;


@Repository
public interface CurrencyRepository extends PrakratiRepository<Currency, Long> {

}
