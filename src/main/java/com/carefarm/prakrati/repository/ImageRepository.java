package com.carefarm.prakrati.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.carefarm.prakrati.entity.Image;
import com.carefarm.prakrati.util.ImageCategory;

public interface ImageRepository extends PrakratiRepository<Image, Long> {

	@Query("SELECT i FROM Image i WHERE i.category = :category AND i.isDefault = true")
	Image findDefaultImageForCategory(@Param("category") ImageCategory category);
	
	@Query("SELECT i FROM Image i WHERE i.product IS NOT NULL")
	List<Image> findProductImages();
}
