package com.carefarm.prakrati.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import com.carefarm.prakrati.common.entity.Country;

public interface CountryRepository extends PrakratiRepository<Country, Long> {

	@Query("SELECT count(c) from Country c WHERE c.archived = false")
	Long countAllActiveOnes();
	
	@Query("SELECT c from Country c WHERE c.archived = false")
	List<Country> findAllActiveOnes(Pageable pageable);
	
	Country findByCode(String code);
}
