package com.carefarm.prakrati.repository.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import com.carefarm.prakrati.entity.Product;
import com.carefarm.prakrati.repository.custom.ProductRepositoryCustom;
import com.carefarm.prakrati.repository.results.CategoryWiseReviews;

public class ProductRepositoryImpl extends PrakratiRepositoryCustomImpl<Product, Long> implements ProductRepositoryCustom {

	@SuppressWarnings("unchecked")
	@Override
	public List<CategoryWiseReviews> findCategoryWiseReviews() {
		List<CategoryWiseReviews> categoryWiseReviews = new ArrayList<>();
		Query query = this.entityManager.createQuery("SELECT p.category.id as categoryId, p.category.name as categoryName, COUNT(rw.rating) as totalReviews, SUM(rw.rating.value) as totalRating FROM Product p JOIN p.reviews rw GROUP BY p.category");
		List<Object[]> result = query.getResultList();
		for (Object[] tuple: result) {
			Long categoryId = (Long )tuple[0];
			String categoryName = (String )tuple[1];
			long totalReviews = (long )tuple[2];
			BigDecimal totalRating = (BigDecimal )tuple[3];
			categoryWiseReviews.add(new CategoryWiseReviews(categoryId, categoryName, totalReviews, totalRating));
		}
		return categoryWiseReviews;
	}

}
