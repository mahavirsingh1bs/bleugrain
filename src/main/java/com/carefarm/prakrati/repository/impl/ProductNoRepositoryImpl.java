package com.carefarm.prakrati.repository.impl;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.repository.ProductNoRepository;

@Component
public class ProductNoRepositoryImpl implements ProductNoRepository {

	@Autowired
	private String nextProductNoQuery;
	
	@Autowired
	private NamedParameterJdbcTemplate namedParameterJdbcTemplate;
	
	@Override
	public String findNextProductNo() {
		return namedParameterJdbcTemplate.queryForObject(nextProductNoQuery, new HashMap<String, String>(), String.class);
	}

}
