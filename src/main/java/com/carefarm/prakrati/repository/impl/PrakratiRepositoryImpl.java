package com.carefarm.prakrati.repository.impl;

import java.io.Serializable;

import javax.persistence.EntityManager;

import org.springframework.data.jpa.repository.support.SimpleJpaRepository;

import com.carefarm.prakrati.repository.PrakratiRepository;

public class PrakratiRepositoryImpl<T, ID extends Serializable> extends SimpleJpaRepository<T, ID>
		implements PrakratiRepository<T, ID> {

	private final EntityManager entityManager;
	
	public PrakratiRepositoryImpl(Class<T> domainClass, EntityManager entityManager) {
		super(domainClass, entityManager);	
		this.entityManager = entityManager;
	}
	
	public EntityManager getEntityManager() {
		return entityManager;
	}

}
