package com.carefarm.prakrati.repository.impl;

import java.io.Serializable;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.carefarm.prakrati.repository.custom.PrakratiRepositoryCustom;

public abstract class PrakratiRepositoryCustomImpl<T, ID extends Serializable> implements PrakratiRepositoryCustom<T, ID> {

	@PersistenceContext
	protected EntityManager entityManager;

	
}
