package com.carefarm.prakrati.repository;

public interface ProductNoRepository {
	String findNextProductNo();
}
