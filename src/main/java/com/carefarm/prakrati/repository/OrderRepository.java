package com.carefarm.prakrati.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.carefarm.prakrati.entity.Order;

public interface OrderRepository extends PrakratiRepository<Order, Long> {

	Order findByOrderNo(String orderNo);
	
	List<Order> findAll();
	
	@Query("SELECT o FROM Order o JOIN o.user u WHERE u.id = :userId")
	List<Order> findOrdersByUserId(@Param("userId") Long userId);

	@Query("SELECT count(o) FROM Order o JOIN o.company c WHERE c.uniqueId = :companyId AND o.status.code NOT IN (:codes) AND o.mocked = false AND o.archived = false")
	Long countByCompanyIdAndStatusCodesNotIn(@Param("companyId") String companyId, @Param("codes") String... codes);

	@Query("SELECT o FROM Order o JOIN o.company c WHERE c.uniqueId = :companyId AND o.status.code NOT IN (:codes) AND o.mocked = false AND o.archived = false")
	List<Order> findByCompanyIdAndStatusCodesNotIn(@Param("companyId") String companyId, @Param("codes") String[] codes, Pageable pageable);
	
	@Query("SELECT count(o) FROM Order o JOIN o.assignedTo a WHERE a.userNo = :userNo AND o.status.code NOT IN (:codes) AND o.archived = false")
	long countOrdersAssignedToUserWhereStatusCodesNotIn(@Param("userNo") String userNo, @Param("codes") String... codes);
	
	@Query("SELECT count(o) FROM Order o JOIN o.assignedTo a WHERE a.userNo = :userNo AND o.dateAssigned <= :assignedOn AND o.status.code NOT IN (:statusCodes) AND o.archived = false")
	long countOrdersAssignedToUserWhereAssignDateIsAndStatusCodesNotIn(@Param("assignedOn") Date assignedOn, @Param("userNo") String userNo, @Param("statusCodes") String... statusCodes);

	@Query("SELECT count(o) FROM Order o JOIN o.user u LEFT JOIN o.assignedTo a WHERE (u.userNo = :userNo OR a.userNo = :userNo) AND o.status.code NOT IN (:codes) AND o.mocked = false AND o.archived = false")
	long countByUserNoAndStatusCodesNotIn(@Param("userNo") String userNo, @Param("codes") String... codes);
	
	@Query("SELECT o FROM Order o JOIN o.user u LEFT JOIN o.assignedTo a WHERE (u.userNo = :userNo OR a.userNo = :userNo) AND o.status.code NOT IN (:codes) AND o.mocked = false AND o.archived = false")
	List<Order> findByUserNoAndStatusCodesNotIn(@Param("userNo") String userNo, @Param("codes") String[] codes, Pageable pageable);
		
	@Query("SELECT o FROM Order o JOIN o.user u WHERE u.userNo = :userNo AND o.dateCreated <= :createdOn AND o.archived = false")
	List<Order> findByUserIdAndDate(@Param("userNo") String userNo, @Param("createdOn") Date createdOn);
	
	@Query("SELECT o FROM Order o JOIN o.user u WHERE u.userNo = :uniqueId AND o.archived = false")
	List<Order> findByUniqueId(@Param("uniqueId") String uniqueId);
	
	@Query("SELECT count(o) FROM Order o WHERE o.status.code NOT IN (:codes) AND o.mocked = false AND o.archived = false")
	Long countByStatusCodesNotIn(@Param("codes") String...codes);
	
	@Query("SELECT o FROM Order o WHERE o.status.code NOT IN (:codes) AND o.mocked = false AND o.archived = false")
	List<Order> findByStatusCodesNotIn(@Param("codes") String[] codes, Pageable pageable);
	
	@Query("SELECT count(o) FROM Order o JOIN o.products p JOIN p.categories c WHERE (c.uniqueId = :categoryId OR c.parentCategory.uniqueId = :categoryId) AND o.dateCreated <= :createdOn AND o.archived = false")
	long countByCategoryIdAndDate(@Param("categoryId") String categoryId, @Param("createdOn") Date createdOn);
}
