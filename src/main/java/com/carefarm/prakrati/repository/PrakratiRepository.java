package com.carefarm.prakrati.repository;

import java.io.Serializable;

import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.PagingAndSortingRepository;

@NoRepositoryBean
public interface PrakratiRepository<T, ID extends Serializable> 
	extends PagingAndSortingRepository<T, ID> {
	
}
