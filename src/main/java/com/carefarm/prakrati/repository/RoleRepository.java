package com.carefarm.prakrati.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import com.carefarm.prakrati.entity.Role;

public interface RoleRepository extends PrakratiRepository<Role, Long> {

	@Query("SELECT count(r) from Role r WHERE r.archived = false")
	Long countAllActiveOnes();
	
	@Query("SELECT r from Role r WHERE r.archived = false")
	List<Role> findAllActiveOnes(Pageable pageable);
	
	@Query("SELECT r from Role r WHERE r.archived = false")
	List<Role> findAllActiveOnes(String keyword, Pageable pageable);
}
