package com.carefarm.prakrati.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.carefarm.prakrati.entity.Activity;

public interface ActivityRepository extends PrakratiRepository<Activity, Long> {
	
	@Query("SELECT count(u) FROM Activity a LEFT OUTER JOIN a.user u WHERE a.product.uniqueId = :uniqueId GROUP BY u")
	Long countByProductId(@Param("uniqueId") String uniqueId);
	
}
