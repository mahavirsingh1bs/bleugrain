package com.carefarm.prakrati.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.carefarm.prakrati.entity.Menuitem;


public interface MenuitemRepository extends PrakratiRepository<Menuitem, Long> {

	@Query("SELECT m FROM Menuitem m WHERE m.parentMenuitem IS NULL and m.active = :active and m.archived = false ORDER BY m.displayIndex ASC")
	List<Menuitem> findMainMenuitems(@Param("active") boolean active);
	
	@Query("SELECT m FROM Menuitem m WHERE m.name LIKE :keyword AND m.parentMenuitem IS NULL AND m.archived = false ORDER BY m.displayIndex ASC")
	List<Menuitem> findMainMenuitems(@Param("keyword") String keyword, Pageable pageable);
	
	@Query("SELECT count(m) FROM Menuitem m WHERE m.name LIKE :keyword AND m.parentMenuitem IS NULL and m.archived = false")
	int countMainMenuitems(@Param("keyword") String keyword);
	
	Menuitem findByUniqueId(String uniqueId);
	
}
