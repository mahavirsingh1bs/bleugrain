package com.carefarm.prakrati.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.carefarm.prakrati.entity.Product;
import com.carefarm.prakrati.repository.custom.ProductRepositoryCustom;

public interface ProductRepository extends PrakratiRepository<Product, Long>,
		ProductRepositoryCustom {

	@Query("SELECT p FROM Product p WHERE p.owner.id = :userId")
	List<Product> findByUserId(@Param("userId") long userId, Pageable pageable);

	@Query("SELECT count(p) FROM Product p JOIN p.assignedTo a WHERE a.userNo = :userNo AND p.status.code in (:codes)")
	long countProductsAssignedToUserWhereStatusCodes(@Param("userNo") String userNo, @Param("codes") String... codes);
	
	@Query("SELECT count(p) FROM Product p JOIN p.assignedTo a WHERE a.userNo = :userNo AND p.dateAssigned <= :assignedOn AND p.status.code IN (:codes)")
	long countProductsAssignedToUserWhereAssignDateAndStatusCodes(@Param("assignedOn") Date assignedOn, @Param("userNo") String userNo, @Param("codes") String... code);
	
	@Query("SELECT count(p) FROM Product p WHERE p.status.code in (:statusCodes) AND archived = false")
	Long countByStatusCodes(@Param("statusCodes") String... statusCodes);
	
	@Query("SELECT count(p) FROM Product p WHERE p.status.code in (:statusCodes) AND p.dateCreated <= :createdOn AND archived = false")
	Long countByDateCreatedAndStatusCodes(@Param("createdOn") Date createdOn, @Param("statusCodes") String... statusCodes);
	
	@Query("SELECT p FROM Product p WHERE p.owner.userNo = :userNo AND p.status.code = :code")
	List<Product> findByUserNoAndStatus(@Param("userNo") String userNo, @Param("code") String code);

	@Query("SELECT count(p) FROM Product p JOIN p.owner o LEFT JOIN p.assignedTo a WHERE (o.userNo = :userNo OR a.userNo = :userNo) AND p.status.code IN (:codes) AND p.mocked = false AND p.archived = false")
	long countByUserNoAndStatusCodes(@Param("userNo") String userNo, @Param("codes") String... codes);
	
	@Query("SELECT p FROM Product p JOIN p.owner o JOIN p.assignedTo a WHERE (o.userNo = :userNo OR a.userNo = :userNo) AND p.status.code IN (:codes) AND p.archived = false")
	List<Product> findByUserNoAndStatusCodes(@Param("userNo") String userNo, @Param("codes") String[] codes);
	
	@Query("SELECT p FROM Product p JOIN p.owner o LEFT JOIN p.assignedTo a WHERE (o.userNo = :userNo OR a.userNo = :userNo) AND p.status.code in (:codes) AND p.mocked = false AND p.archived = false")
	List<Product> findByUserNoAndStatusCodes(@Param("userNo") String userNo, @Param("codes") String[] codes, Pageable pageable);
	
	@Query("SELECT count(p) FROM Product p LEFT OUTER JOIN p.categories c LEFT OUTER JOIN c.parentCategory pc WHERE (c.uniqueId = :uniqueId OR pc.uniqueId = :uniqueId) AND p.consumerProduct = true AND p.mocked = false AND p.archived = false")
	Long countConsumerProducts(@Param("uniqueId") String uniqueId);
	
	@Query("SELECT p FROM Product p WHERE p.consumerProduct = true")
	List<Product> findAllConsumerProducts();
	
	@Query("SELECT p FROM Product p LEFT OUTER JOIN p.categories c LEFT OUTER JOIN c.parentCategory pc WHERE (c.uniqueId = :uniqueId OR pc.uniqueId = :uniqueId) AND p.consumerProduct = true AND p.mocked = false AND p.archived = false")
	List<Product> findConsumerProducts(@Param("uniqueId") String uniqueId, Pageable pageable);
	
	@Query("SELECT count(p) FROM Product p LEFT JOIN p.assignedTo a WHERE (p.owner.userNo = :userNo OR a.userNo = :userNo) AND p.status.code = :code AND p.mocked = false AND p.archived = false")
	long countByUserNoAndStatus(@Param("userNo") String userNo, @Param("code") String code);
	
	@Query("SELECT p FROM Product p LEFT JOIN p.assignedTo a WHERE (p.owner.userNo = :userNo OR a.userNo = :userNo) AND p.status.code = :code AND p.mocked = false AND p.archived = false")
	List<Product> findByUserNoAndStatus(@Param("userNo") String userNo, @Param("code") String code, Pageable pageable);
	
	@Query("SELECT count(p) FROM Product p JOIN p.categories c LEFT OUTER JOIN c.parentCategory pc WHERE (c.uniqueId = :categoryId OR pc.uniqueId = :categoryId)  AND p.consumerProduct = :consumerProducts AND p.status.code IN (:codes) AND p.mocked = false AND p.archived = false")
	long countByCategoryIdAndTypeAndStatusCodes(@Param("categoryId") String categoryId, @Param("consumerProducts") boolean consumerProducts, @Param("codes") String... codes);
	
	@Query("SELECT count(p) FROM Product p JOIN p.categories c LEFT OUTER JOIN c.parentCategory pc WHERE p.name like :keywords AND (c.uniqueId = :categoryId OR pc.uniqueId = :categoryId)  AND p.consumerProduct = :consumerProducts AND p.status.code IN (:codes) AND p.mocked = false AND p.archived = false")
	long countByKeywordsAndCategoryIdAndTypeAndStatusCodes(@Param("keywords") String keywords, @Param("categoryId") String categoryId, @Param("consumerProducts") boolean consumerProducts, @Param("codes") String... codes);
	
	@Query("SELECT p FROM Product p JOIN p.categories c LEFT OUTER JOIN c.parentCategory pc WHERE (c.uniqueId = :categoryId OR pc.uniqueId = :categoryId) AND p.consumerProduct = :consumerProducts AND p.status.code IN (:codes) AND p.mocked = false AND p.archived = false ORDER BY p.id ASC")
	List<Product> findByCategoryIdAndTypeAndStatusCodes(@Param("categoryId") String categoryId, @Param("consumerProducts") boolean consumerProducts, @Param("codes") String[] codes, Pageable pageable);
	
	@Query("SELECT p FROM Product p JOIN p.categories c LEFT OUTER JOIN c.parentCategory pc WHERE p.name like :keywords AND (c.uniqueId = :categoryId OR pc.uniqueId = :categoryId)  AND p.consumerProduct = :consumerProducts AND p.status.code IN (:codes) AND p.mocked = false AND p.archived = false ORDER BY p.id ASC")
	List<Product> findByKeywordsAndCategoryIdAndTypeAndStatusCodes(@Param("keywords") String keywords, @Param("categoryId") String categoryId, @Param("consumerProducts") boolean consumerProducts, @Param("codes") String[] codes, Pageable pageable);
	
	@Query("SELECT count(p) FROM Product p WHERE p.status.status = :status AND p.mocked = false AND p.archived = false")
	Long countByStatus(@Param("status") String status);
	
	@Query("SELECT p FROM Product p WHERE p.status.status = :status AND p.mocked = false AND p.archived = false")
	List<Product> findByStatus(@Param("status") String status, Pageable pageable);
	
	@Query("SELECT p FROM Product p WHERE p.uniqueId = :uniqueId")
	Product findByUniqueId(@Param("uniqueId") String uniqueId);
	
	@Query("SELECT p FROM Product p WHERE p.owner.userNo = :userNo AND p.bidEnabled = true AND p.archived = false")
	List<Product> findBiddableByUserNo(@Param("userNo") String userNo);
	
	@Query("SELECT p FROM Product p LEFT OUTER JOIN p.categories c LEFT OUTER JOIN c.parentCategory pc WHERE (c.uniqueId = :uniqueId OR pc.uniqueId = :uniqueId) AND p.bidEnabled = true AND p.archived = false")
	List<Product> findBiddableByCategoryId(@Param("uniqueId") String uniqueId, Pageable pageable);
	
	@Query("SELECT p FROM Product p LEFT OUTER JOIN p.address.state s LEFT OUTER JOIN p.categories c LEFT OUTER JOIN c.parentCategory pc WHERE s.id = :stateId AND (c.uniqueId = :uniqueId OR pc.uniqueId = :uniqueId ) AND p.bidEnabled = true AND p.archived = false")
	List<Product> findBiddableByStateIdAndCategoryId(@Param("stateId") Long stateId, @Param("uniqueId") String uniqueId, Pageable pageable);
	
	@Query("SELECT p FROM Product p JOIN p.bids b WHERE b.createdBy.userNo = :userNo AND b.dateCreated <= :createdOn AND p.archived = false")
	List<Product> findProductsForWhichBidPlaceByUserAndDate(@Param("userNo") String userNo, @Param("createdOn") Date createdOn);
	
	@Query("SELECT count(p) FROM Product p LEFT OUTER JOIN p.categories c LEFT OUTER JOIN c.parentCategory pc WHERE (c.uniqueId = :uniqueId OR pc.uniqueId = :uniqueId) AND p.consumerProduct = false AND p.archived = false")
	long countByMainCategoryId(@Param("uniqueId") String uniqueId);

	@Query("SELECT count(p) FROM Product p LEFT OUTER JOIN p.categories c LEFT OUTER JOIN c.parentCategory pc WHERE (c.uniqueId = :uniqueId OR pc.uniqueId = :uniqueId) AND p.status.code in (:codes) AND p.consumerProduct = false AND p.archived = false")
	long countByMainCategoryIdAndStatusCodes(@Param("uniqueId") String uniqueId, @Param("codes") String... codes);
	
	@Query("SELECT count(p) FROM Product p LEFT OUTER JOIN p.categories c WHERE (c.uniqueId = :uniqueId OR c.parentCategory.uniqueId = :uniqueId) AND p.bidEnabled = true AND p.archived = false")
	long countBiddableByCategoryId(@Param("uniqueId") String uniqueId);

	@Query("SELECT p FROM Product p LEFT OUTER JOIN p.bids b LEFT OUTER JOIN b.user u WHERE u.userNo = :userNo")
	List<Product> findProductsBiddedByUser(@Param("userNo") String userNo, Pageable pageable);
	
	@Query("SELECT p FROM Product p LEFT OUTER JOIN p.bids b LEFT OUTER JOIN p.categories c LEFT OUTER JOIN c.parentCategory pc LEFT OUTER JOIN b.user u WHERE u.userNo = :userNo AND (c.uniqueId = :uniqueId OR pc.uniqueId = :uniqueId)")
	List<Product> findProductsBiddedByUserAndCategory(@Param("userNo") String userNo, @Param("uniqueId") String categoryId, Pageable pageable);
	
	@Query("SELECT p FROM Product p LEFT OUTER JOIN p.bids b LEFT OUTER JOIN p.categories c LEFT OUTER JOIN c.parentCategory pc LEFT OUTER JOIN b.company c WHERE c.uniqueId = :companyId AND (c.uniqueId = :uniqueId OR pc.uniqueId = :uniqueId)")
	List<Product> findProductsBiddedByCompanyAndCategory(@Param("companyId") String companyId, @Param("uniqueId") String categoryId, Pageable pageable);
	
}
