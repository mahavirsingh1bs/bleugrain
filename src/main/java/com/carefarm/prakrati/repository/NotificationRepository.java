package com.carefarm.prakrati.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.carefarm.prakrati.entity.Notification;
import com.carefarm.prakrati.entity.User;

@Repository
public interface NotificationRepository extends PrakratiRepository<Notification, Long> {
	
	List<Notification> findAll();
	
	List<Notification> findByUser(User user);
	
	@Query("SELECT n FROM Notification n LEFT JOIN n.category c WHERE c.code = :code")
	List<Notification> findByCategoryCode(@Param("code") String code);
	
	@Query("SELECT n FROM Notification n WHERE n.createdBy.userNo = :userNo AND n.archived = false")
	List<Notification> findByCreatedByUserNo(@Param("userNo") String userNo);
	
	@Query("SELECT DISTINCT n FROM Notification n LEFT OUTER JOIN n.groups g LEFT OUTER JOIN n.users u WHERE g.uniqueId = :groupId OR u.userNo = :userNo AND n.startDate <= :currentDate AND n.expiryDate >= :currentDate AND n.archived = false")
	List<Notification> findByGroupIdAndUserNo(@Param("groupId") String groupId, @Param("userNo") String userNo, @Param("currentDate") Date currentDate);
	
	@Query("SELECT DISTINCT n FROM Notification n LEFT OUTER JOIN n.groups g LEFT OUTER JOIN n.users u LEFT OUTER JOIN n.companies c WHERE g.uniqueId = :groupId OR u.userNo = :userNo OR c.uniqueId = :companyId AND n.startDate <= :currentDate AND n.expiryDate >= :currentDate AND n.archived = false")
	List<Notification> findByGroupIdAndUserNoAndCompanyId(@Param("groupId") String groupId, @Param("userNo") String userNo, @Param("companyId") String companyId, @Param("currentDate") Date currentDate);
}
