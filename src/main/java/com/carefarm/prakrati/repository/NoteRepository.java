package com.carefarm.prakrati.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.carefarm.prakrati.entity.Note;

@Repository
public interface NoteRepository extends PrakratiRepository<Note, Long> {

	List<Note> findAll();
	
	Note findByUniqueId(String uniqueId);
	
	@Query("SELECT n FROM Note n WHERE n.archived = false")
	List<Note> findAllActiveOnes();
	
	@Query("SELECT n FROM Note n WHERE n.user.userNo = :userNo AND n.archived = false")
	List<Note> findByUserNo(@Param("userNo") String userNO);
}
