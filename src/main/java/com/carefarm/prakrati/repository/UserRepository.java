package com.carefarm.prakrati.repository;

import java.util.Date;
import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.util.UserType;

public interface UserRepository extends PrakratiRepository<User, Long>, JpaSpecificationExecutor<User> {

	long countByUserType(UserType userType);
	
	@Query("SELECT u from User u WHERE u.emailVerifyCode = :emailVerifCode AND u.archived = false")
	User findByEmailVerifCode(@Param("emailVerifCode") String emailVerifCode);
	
	@Query("SELECT u from User u WHERE u.userNo in (:users) AND u.archived = false")
	Set<User> findByUserNos(@Param("users") List<String> users);
	
	@Query("SELECT u FROM User u WHERE u.email = :emailAddress")
	User findByEmailAddress(@Param("emailAddress") String emailAddress);
	
	@Query("SELECT u FROM User u WHERE u.email = :emailAddress")
	List<User> findUsersByEmailAddress(@Param("emailAddress") String emailAddress);
	
	@Query("SELECT u FROM User u WHERE u.uniqueCode = :uniqueCode")
	User findByUniqueCode(@Param("uniqueCode") String uniqueCode);
	
	@Query("SELECT u FROM User u WHERE u.alternateUsername = :alternateUsername")
	User findByAlternateUsername(@Param("alternateUsername") String alternateUsername);
	
	@Query("SELECT u FROM User u WHERE u.username = :username OR u.alternateUsername = :username")
	User findByUsernameOrAlternateUsername(@Param("username") String username);

	@Query("SELECT u FROM User u where u.username = :username")
	User findByUsername(@Param("username") String username);
	
	@Query("SELECT u FROM User u where u.userNo = :userNo")
	User findByUserNo(@Param("userNo") String userNo);
	
	@Query("SELECT u FROM User u where u.resetPasswdCode = :resetPasswdCode")
	User findByResetCode(@Param("resetPasswdCode") String resetPasswdCode);
	
	@Query("SELECT u FROM User u where u.email like :keywords OR u.phoneNo like :keywords "
			+ " OR u.mobileNo like :keywords OR u.userNo like :keywords AND u.mocked = false AND u.archived = false")
	List<User> findByMatchingEmailAddressOrPhoneNoOrMobileNoOrUniqueId(@Param("keywords") String keywords);
	
	@Query("SELECT u FROM User u where (u.email like :keywords OR u.phoneNo like :keywords "
			+ " OR u.mobileNo like :keywords OR u.userNo like :keywords) AND u.userType in (:userTypes) AND u.mocked = false AND u.archived = false")
	List<User> findByMatchingEmailAddressOrPhoneNoOrMobileNoOrUniqueIdAndUserType(@Param("keywords") String keywords, @Param("userTypes") UserType... userTypes);
	
	@Query("SELECT u FROM User u where (u.firstName like :keywords OR u.lastName like :keywords OR u.email like :keywords OR u.phoneNo like :keywords "
			+ " OR u.mobileNo like :keywords OR u.userNo like :keywords) AND u.company.uniqueId = :companyId AND u.userType in (:userTypes) AND u.mocked = false AND u.archived = false")
	List<User> findByMatchingEmailAddressOrPhoneNoOrMobileNoOrUniqueIdAndCompanyIdAndUserType(@Param("keywords") String keywords, @Param("companyId") String companyId, @Param("userTypes") UserType... userTypes);
	
	@Query("SELECT COUNT(u) FROM User u WHERE u.company.id = :companyId AND u.group.name = :groupName AND archived = false AND u.mocked = false AND u.archived = false")
	Long countByCompanyIdAndGroupName(@Param("companyId") Long companyId, @Param("groupName") String groupName);
	
	@Query("SELECT u FROM User u WHERE u.company.id = :companyId AND u.group.name = :groupName AND u.mocked = false AND u.archived = false")
	List<User> findByCompanyIdAndGroupName(@Param("companyId") Long companyId, @Param("groupName") String groupName);
	
	@Query("SELECT COUNT(u) FROM User u WHERE u.group.name = :groupName AND archived = false")
	Long countByGroupName(@Param("groupName") String groupName);
	
	@Query("SELECT COUNT(u) FROM User u WHERE u.group.name = :groupName AND u.dateCreated < :createdOn AND archived = false")
	Long countByDateAndGroupName(@Param("createdOn") Date createdOn, @Param("groupName") String groupName);
	
	@Query("SELECT COUNT(u) FROM User u WHERE u.group.name in (:groupNames) AND archived = false")
	Long countByGroupNames(@Param("groupNames") String... groupNames);
	
	@Query("SELECT COUNT(u) FROM User u WHERE u.group.name in (:groupNames) AND u.dateCreated < :createdOn AND archived = false")
	Long countByGroupsAndDate(@Param("createdOn") Date createdOn, @Param("groupNames") String... groupNames);
	
	@Query("SELECT u FROM User u WHERE u.group.name = :groupName AND archived = false")
	List<User> findByGroupName(@Param("groupName") String groupName);
	
	@Query("SELECT u FROM User u WHERE u.company.uniqueId = :companyId AND archived = false")
	List<User> findByCompanyId(@Param("companyId") String companyId);
	
	@Query("SELECT u FROM User u WHERE u.company.uniqueId = :companyId AND u.userType = :userType AND u.mocked = false AND u.archived = false")
	List<User> findByCompanyIdAndUserType(@Param("companyId") String companyId, @Param("userType") UserType userType);
	
}
