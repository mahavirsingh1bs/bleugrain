package com.carefarm.prakrati.repository;

import org.springframework.stereotype.Repository;

import com.carefarm.prakrati.entity.OrderHistory;

@Repository
public interface OrderHistoryRepository extends PrakratiRepository<OrderHistory, Long> {

}
