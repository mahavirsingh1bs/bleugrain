package com.carefarm.prakrati.repository.results;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class CategoryWiseReviews {
	private Long id;
	private String name;
	private long totalReviews;
	private BigDecimal totalRating;

	public CategoryWiseReviews() { }

	public CategoryWiseReviews(Long id, String name, long totalReviews, BigDecimal totalRating) {
		this.id = id;
		this.name = name;
		this.totalReviews = totalReviews;
		this.totalRating = totalRating;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getTotalReviews() {
		return totalReviews;
	}

	public void setTotalReviews(long totalReviews) {
		this.totalReviews = totalReviews;
	}

	public BigDecimal getTotalRating() {
		return totalRating;
	}

	public void setTotalRating(BigDecimal totalRating) {
		this.totalRating = totalRating;
	}

	public int getRatingPercent() {
		return totalRating.divide(BigDecimal.valueOf(totalReviews * 5), 2, RoundingMode.HALF_UP).multiply(BigDecimal.valueOf(100)).intValue();
	}
}
