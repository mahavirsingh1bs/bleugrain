package com.carefarm.prakrati.repository;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.carefarm.prakrati.entity.Contact;

@Repository
public interface ContactRepository extends PrakratiRepository<Contact, Contact.Id> {
	
	public List<Contact> findByGroupId(long groupId);
	
}
