package com.carefarm.prakrati.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.carefarm.prakrati.entity.Product;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.entity.Wishlist;

@Repository
public interface WishlistRepository extends PrakratiRepository<Wishlist, Long> {

	@Query("SELECT count(p) FROM Wishlist w JOIN w.products p WHERE w.user.userNo = :userNo AND w.archived = false")
	long countProductsByUserNo(@Param("userNo") String userNo);
	
	@Query("SELECT p FROM Wishlist w JOIN w.products p WHERE w.user.userNo = :userNo AND w.archived = false")
	List<Product> findProductsByUserNo(@Param("userNo") String userNo, Pageable pageable);
	
	@Query("SELECT count(d) FROM Wishlist w JOIN w.demands d WHERE w.user.userNo = :userNo AND w.archived = false")
	long countDemandsByUserNo(@Param("userNo") String userNo);
	
	@Query("SELECT count(w) FROM Wishlist w JOIN w.products p WHERE p.uniqueId = :uniqueId AND w.archived = false")
	long countByProductId(@Param("uniqueId") String uniqueId);

	@Query("SELECT count(w) FROM Wishlist w JOIN w.demands d WHERE d.uniqueId = :uniqueId AND w.archived = false")
	long countByDemandId(@Param("uniqueId") String uniqueId);
	
	@Query("SELECT p, count(p) FROM Wishlist w JOIN w.products p WHERE p.owner.userNo = :userNo AND w.archived = false")
	Object[] countByProductsUserNo(@Param("userNo") String userNo);
	
	Wishlist findByUser(User user);
	
	@Query("SELECT count(w) FROM Wishlist w JOIN w.products p JOIN p.categories c WHERE (c.uniqueId = :categoryId OR c.parentCategory.uniqueId = :categoryId) AND w.archived = false")
	long countByMainCategoryId(@Param("categoryId") String categoryId);
}
