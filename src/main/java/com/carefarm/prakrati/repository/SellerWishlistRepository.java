package com.carefarm.prakrati.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.carefarm.prakrati.entity.SellerWishlist;

public interface SellerWishlistRepository extends PrakratiRepository<SellerWishlist, Long> {
	
	@Query("SELECT sw FROM SellerWishlist sw WHERE sw.seller.id = :sellerId")
	List<SellerWishlist> findWishlistBySellerId(@Param("sellerId") Long sellerId);

	@Query("SELECT count(sw) FROM SellerWishlist sw JOIN sw.seller s WHERE s.company.id = :companyId")
	Long countByCompanyId(@Param("companyId") Long companyId);

	@Query("SELECT sw FROM SellerWishlist sw JOIN sw.seller s WHERE s.company.id = :companyId")
	List<SellerWishlist> findByCompanyId(@Param("companyId") Long companyId, Pageable pageable);
	
	@Query("SELECT count(sw) FROM SellerWishlist sw JOIN sw.seller s WHERE s.id = :userId")
	Long countByUserId(@Param("userId") Long userId);
	
	@Query("SELECT sw FROM SellerWishlist sw JOIN sw.seller s WHERE s.id = :userId")
	List<SellerWishlist> findByUserId(@Param("userId") Long userId, Pageable pageable);

}
