package com.carefarm.prakrati.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;

import com.carefarm.prakrati.delivery.entity.Permit;

public interface PermitRepository extends PrakratiRepository<Permit, Long> {
	@Query("SELECT p FROM Permit p WHERE p.archived = false")
	List<Permit> findAllByArchived();
}
