package com.carefarm.prakrati.repository;

import com.carefarm.prakrati.entity.SocialAccount;

public interface SocialAccountRepository extends PrakratiRepository<SocialAccount, Long> {
	
	public SocialAccount findByUniqueId(String uniqueId);
	
}
