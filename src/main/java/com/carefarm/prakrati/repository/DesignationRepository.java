package com.carefarm.prakrati.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;

import com.carefarm.prakrati.entity.Designation;

public interface DesignationRepository extends PrakratiRepository<Designation, Long> {

	@Query("SELECT count(d) from Designation d WHERE d.archived = false")
	Long countAllActiveOnes();
	
	@Query("SELECT d from Designation d WHERE d.archived = false")
	List<Designation> findAllActiveOnes(Pageable pageable);
}
