package com.carefarm.prakrati.repository;

import org.springframework.stereotype.Repository;

import com.carefarm.prakrati.common.entity.OrderStatus;

@Repository
public interface OrderStatusRepository extends PrakratiRepository<OrderStatus, Long> {
	
	OrderStatus findByCode(String code);
	
	OrderStatus findByStatus(String status);
}
