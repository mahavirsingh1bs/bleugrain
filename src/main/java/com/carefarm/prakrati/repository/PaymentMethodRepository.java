package com.carefarm.prakrati.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.carefarm.prakrati.entity.PaymentMethod;

public interface PaymentMethodRepository extends PrakratiRepository<PaymentMethod, Long> {
	
	@Query("SELECT pm FROM PaymentMethod pm WHERE pm.createdBy.id = :userId")
	public List<PaymentMethod> findPaymentMethodByUserId(@Param("userId") long userId);
	
	@Query("SELECT count(pm) from PaymentMethod pm WHERE pm.archived = false")
	Long countAllActiveOnes();
	
	@Query("SELECT pm from PaymentMethod pm WHERE pm.archived = false")
	List<PaymentMethod> findAllActiveOnes(Pageable pageable);
}
