package com.carefarm.prakrati.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.carefarm.prakrati.entity.GrainCart;
import com.carefarm.prakrati.entity.User;

@Repository
public interface GrainCartRepository extends PrakratiRepository<GrainCart, Long> {
	GrainCart findByUser(User user);
	
	@Query("SELECT count(gc) FROM GrainCart gc LEFT OUTER JOIN gc.products p WHERE p.uniqueId = :uniqueId")
	long countByProductId(@Param("uniqueId") String uniqueId);
}
