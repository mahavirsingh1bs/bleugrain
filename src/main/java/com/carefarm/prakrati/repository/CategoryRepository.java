package com.carefarm.prakrati.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.carefarm.prakrati.entity.Category;

public interface CategoryRepository extends PrakratiRepository<Category, Long> {

	Category findByUniqueId(String uniqueId);
	
	@Query("SELECT c FROM Category c WHERE c.parentCategory.id = :categoryId")
	List<Category> findChildCategories(@Param("categoryId") Long categoryId);

	@Query("SELECT c FROM Category c WHERE c.parentCategory IS NULL")
	List<Category> findMainCategories();
	
	@Query("SELECT c FROM Category c WHERE c.name like :keyword AND c.parentCategory IS NULL")
	List<Category> findMainCategories(@Param("keyword") String keyword, Pageable pageable);
	
	@Query("SELECT count(c) FROM Category c WHERE c.name like :keyword AND c.parentCategory IS NULL")
	int countMainCategories(@Param("keyword") String keyword);
	
	@Query("SELECT count(c) from Category c WHERE c.archived = false")
	Long countAllActiveOnes();
	
	@Query("SELECT c from Category c WHERE c.archived = false")
	List<Category> findAllActiveOnes(Pageable pageable);

	@Query("SELECT DISTINCT(c) FROM Category c where LOWER(c.name) like LOWER(:keywords) and c.archived = false")
	List<Category> findByMatchingName(@Param("keywords") String string);
}
