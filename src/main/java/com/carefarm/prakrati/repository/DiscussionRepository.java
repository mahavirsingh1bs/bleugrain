package com.carefarm.prakrati.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.carefarm.prakrati.entity.Discussion;

@Repository
public interface DiscussionRepository extends PrakratiRepository<Discussion, Long> {
	
	List<Discussion> findAll();
	
	@Query("SELECT d FROM Discussion d WHERE d.archived = false")
	List<Discussion> findAllActiveOnes();
	
	@Query("SELECT DISTINCT d FROM Discussion d LEFT OUTER JOIN d.user u WHERE u.userNo = :userNo AND d.archived = false")
	List<Discussion> findByUserNo(@Param("userNo") String userNo);
	
	@Query("SELECT DISTINCT d FROM Discussion d LEFT OUTER JOIN d.groups g WHERE g.uniqueId = :groupId AND d.archived = false")
	List<Discussion> findByGroupId(@Param("groupId") String groupId);
	
}
