package com.carefarm.prakrati.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.carefarm.prakrati.entity.UpcomingEvent;

@Repository
public interface UpcomingEventRepository extends PrakratiRepository<UpcomingEvent, Long> {
	
	List<UpcomingEvent> findAll();
	
	UpcomingEvent findByUniqueId(String uniqueId);
	
	@Query("SELECT DISTINCT ue FROM UpcomingEvent ue WHERE ue.archived = false")
	List<UpcomingEvent> findAllActiveOnes();
	
	@Query("SELECT DISTINCT ue FROM UpcomingEvent ue LEFT OUTER JOIN ue.groups g LEFT OUTER JOIN ue.users u WHERE g.uniqueId = :groupId OR u.userNo = :userNo AND ue.archived = false")
	List<UpcomingEvent> findByGroupIdOrUserId(@Param("groupId") String groupId, @Param("userNo") String userNo);
	
	@Query("SELECT DISTINCT ue FROM UpcomingEvent ue LEFT OUTER JOIN ue.groups g LEFT OUTER JOIN ue.users u LEFT OUTER JOIN ue.companies c WHERE g.uniqueId = :groupId OR u.userNo = :userNo OR c.uniqueId = :companyId AND ue.archived = false")
	List<UpcomingEvent> findByGroupIdOrUserIdOrCompanyId(@Param("groupId") String groupId, @Param("userNo") String userNo, @Param("companyId") String companyId);
}
