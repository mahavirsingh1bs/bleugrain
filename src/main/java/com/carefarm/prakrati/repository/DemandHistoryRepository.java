package com.carefarm.prakrati.repository;

import org.springframework.stereotype.Repository;

import com.carefarm.prakrati.entity.DemandHistory;

@Repository
public interface DemandHistoryRepository extends PrakratiRepository<DemandHistory, Long> {

}
