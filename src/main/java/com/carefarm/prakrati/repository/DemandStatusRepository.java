package com.carefarm.prakrati.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.carefarm.prakrati.common.entity.DemandStatus;

@Repository
public interface DemandStatusRepository extends PrakratiRepository<DemandStatus, Long> {

	DemandStatus findByCode(String code);
	
	@Query("SELECT ds FROM DemandStatus ds WHERE ds.status = :status")
	DemandStatus findByStatus(@Param("status") String status);
}
