package com.carefarm.prakrati.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.carefarm.prakrati.entity.BuyerWishlist;

public interface BuyerWishlistRepository extends PrakratiRepository<BuyerWishlist, Long> {

	@Query("SELECT bw FROM BuyerWishlist bw WHERE bw.buyer.id = :buyerId")
	public List<BuyerWishlist> findWishlistByBuyerId(@Param("buyerId") Long buyerId);

	@Query("SELECT count(bw) FROM BuyerWishlist bw JOIN bw.buyer b WHERE b.company.id = :companyId")
	Long countByCompanyId(@Param("companyId") Long companyId);

	@Query("SELECT bw FROM BuyerWishlist bw JOIN bw.buyer b WHERE b.company.id = :companyId")
	List<BuyerWishlist> findByCompanyId(@Param("companyId") Long companyId, Pageable pageable);

	@Query("SELECT count(bw) FROM BuyerWishlist bw JOIN bw.buyer b WHERE b.id = :userId")
	Long countByUserId(@Param("userId") Long userId);
	
	@Query("SELECT bw FROM BuyerWishlist bw JOIN bw.buyer b WHERE b.id = :userId")
	List<BuyerWishlist> findByUserId(@Param("userId") Long userId, Pageable pageable);
}
