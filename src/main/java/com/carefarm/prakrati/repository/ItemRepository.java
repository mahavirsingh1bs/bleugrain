package com.carefarm.prakrati.repository;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.carefarm.prakrati.entity.Item;

@Repository
public interface ItemRepository extends PrakratiRepository<Item, Long> {

	@Modifying
	@Query("DELETE FROM Item i WHERE i.uniqueId = :uniqueId")
	void removeByUniqueId(@Param("uniqueId") String uniqueId);
}