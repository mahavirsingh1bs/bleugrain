package com.carefarm.prakrati.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.carefarm.prakrati.entity.AddressEntity;
import com.carefarm.prakrati.util.AddressCategory;

public interface AddressRepository extends PrakratiRepository<AddressEntity, Long> {
	
	@Query("SELECT ae FROM AddressEntity ae WHERE ae.company.id = :companyId AND ae.addressCategory = :category AND ae.archived = false")
	List<AddressEntity> findByCompanyIdAndCategory(@Param("companyId") Long companyId, @Param("category") AddressCategory category);
	
	@Query("SELECT ae FROM AddressEntity ae WHERE ae.company.id = :companyId AND ae.addressCategory = :category AND ae.defaultAddress = :defaultAddress AND ae.archived = false")
	List<AddressEntity> findByCompanyIdAndCategoryAndType(@Param("companyId") Long companyId, @Param("category") AddressCategory category, @Param("defaultAddress") boolean defaultAddress); 
	
	@Query("SELECT ae FROM AddressEntity ae WHERE ae.user.id = :userId AND ae.addressCategory = :category AND ae.archived = false")
	List<AddressEntity> findByUserIdAndCategory(@Param("userId") Long userId, @Param("category") AddressCategory category);
	
	@Query("SELECT ae FROM AddressEntity ae WHERE ae.user.id = :userId AND ae.addressCategory = :category AND ae.defaultAddress = :defaultAddress AND ae.archived = false")
	List<AddressEntity> findByUserIdAndCategoryAndType(@Param("userId") Long userId, @Param("category") AddressCategory category, @Param("defaultAddress") boolean defaultAddress);
}
