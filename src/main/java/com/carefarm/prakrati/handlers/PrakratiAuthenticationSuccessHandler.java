package com.carefarm.prakrati.handlers;

import static com.carefarm.prakrati.common.util.Constants.COMPANY_ID;
import static com.carefarm.prakrati.common.util.Constants.CURRENT_USER;
import static com.carefarm.prakrati.common.util.Constants.ID;
import static com.carefarm.prakrati.common.util.Constants.USER;
import static com.carefarm.prakrati.common.util.Constants.USER_ID;
import static com.carefarm.prakrati.common.util.Constants.USER_NO;
import static com.carefarm.prakrati.common.util.Constants.USER_TYPE;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.support.json.Jackson2JsonObjectMapper;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.listeners.SuccessfulLoginListener;
import com.carefarm.prakrati.service.helper.UserHelper;
import com.carefarm.prakrati.web.bean.UserBean;

public class PrakratiAuthenticationSuccessHandler implements
		AuthenticationSuccessHandler {
	
	@Autowired
	private DozerBeanMapper mapper;
	
	@Autowired
	private UserHelper userHelper;
	
	@Autowired
	private SuccessfulLoginListener successfulLoginListener;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void onAuthenticationSuccess(HttpServletRequest request,
			HttpServletResponse response, Authentication authentication)
			throws IOException, ServletException {
		HttpSession httpSession = request.getSession(false);
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
    	
    	httpSession.setAttribute(USER_ID, user.getId());
    	httpSession.setAttribute(USER_NO, user.getUserNo());
    	httpSession.setAttribute(USER_TYPE, user.getClass().getSimpleName());
    	if (Predicates.notNull.test(user.getCompany())) {
    		httpSession.setAttribute(COMPANY_ID, user.getCompany().getId());
    	}
    	
    	Jackson2JsonObjectMapper jsonMapper = new Jackson2JsonObjectMapper();
    	try {
    		Map<String, Object> responseData = new HashMap<>();
    		responseData.put(ID, httpSession.getId());
    		UserBean userBean = userHelper.mapCurrentUser(user);
    		httpSession.setAttribute(CURRENT_USER, userBean);
			
    		successfulLoginListener.onSuccessfulLogin(user.getUserNo());
    		responseData.put(USER, userBean);
    		response.getWriter().write(jsonMapper.toJson(responseData));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
