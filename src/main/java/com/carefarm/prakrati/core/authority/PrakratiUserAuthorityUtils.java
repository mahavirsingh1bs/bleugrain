package com.carefarm.prakrati.core.authority;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import com.carefarm.prakrati.entity.User;

public class PrakratiUserAuthorityUtils {

    public static Collection<? extends GrantedAuthority> createAuthorities(User user) {
    	List<String> authorityList = new ArrayList<>();
    	user.getGroup().getRoles()
    		.forEach(role -> authorityList.add(role.getName()));
    	return AuthorityUtils.createAuthorityList(authorityList.toArray(new String[authorityList.size()]));
    }
    
}
