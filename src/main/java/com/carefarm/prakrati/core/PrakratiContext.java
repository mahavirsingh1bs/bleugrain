package com.carefarm.prakrati.core;

import com.carefarm.prakrati.entity.User;

public class PrakratiContext {
	private String contextPath;
	private User currentUser;
	
	public String getContextPath() {
		return contextPath;
	}
	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}
	public User getCurrentUser() {
		return currentUser;
	}
	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}
	
}
