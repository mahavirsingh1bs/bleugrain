package com.carefarm.prakrati.core.userdetails;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.core.authority.PrakratiUserAuthorityUtils;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.repository.UserRepository;

@Component
public class PrakratiUserDetailsService implements UserDetailsService {
	private final UserRepository userRepository;
	
	@Autowired
	public PrakratiUserDetailsService(UserRepository userRepository) {
		if (userRepository == null) {
			throw new IllegalArgumentException("userDAO cannot be null");
		}
		this.userRepository = userRepository;
	}
	
	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		User user = userRepository.findByEmailAddress(username);
		if (user == null) {
			throw new UsernameNotFoundException("Invalid Username/password");
		}
		return new PrakratiUserDetails(user);
	}
	
	private class PrakratiUserDetails extends User implements UserDetails {
		private static final long serialVersionUID = -7299028486174971169L;

		public PrakratiUserDetails(User user) {
			setId(user.getId());
			setEmail(user.getEmail());
			setFirstName(user.getFirstName());
			setLastName(user.getLastName());
			setUsername(user.getUsername());
			setPassword(user.getPassword());
		}
		
		@Override
		public Collection<? extends GrantedAuthority> getAuthorities() {
			return PrakratiUserAuthorityUtils.createAuthorities(this);
		}

		@Override
		public boolean isAccountNonExpired() {
			return true;
		}

		@Override
		public boolean isAccountNonLocked() {
			return true;
		}

		@Override
		public boolean isCredentialsNonExpired() {
			return true;
		}

		@Override
		public boolean isEnabled() {
			return true;
		}

	}

}
