package com.carefarm.prakrati.core;

import java.time.Clock;
import java.time.ZoneId;

public class Environment {
	
	private static final ZoneId ASIA_ZONE_ID = ZoneId.of("Asia/Kolkata");
	
	private static final Clock ASIA_CLOCK = Clock.system(ASIA_ZONE_ID); 
	
	public static Clock clock() {
		return ASIA_CLOCK;
	}
	
	public static ZoneId zoneId() {
		return ASIA_ZONE_ID;
	}
}
