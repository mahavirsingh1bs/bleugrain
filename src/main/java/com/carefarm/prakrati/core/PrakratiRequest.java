package com.carefarm.prakrati.core;

import java.util.HashMap;
import java.util.Map;

import org.springframework.data.domain.Pageable;

import com.carefarm.prakrati.constants.Predicates;

public class PrakratiRequest {

	private Long userId;
	private Long companyId;
	private Pageable pageable;
	private Map<String, Object> parameters;
	
	public Long getUserId() {
		return userId;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public Pageable getPageable() {
		return pageable;
	}

	public Map<String, Object> getParameters() {
		return parameters;
	}

	public Object getParameter(String name) {
		if (Predicates.notNull.test(parameters)) {
			return this.parameters.get(name);
		}
		return null;
	}
	
	public PrakratiRequest setPageable(Pageable pageable) {
		this.pageable = pageable;
		return this;
	}
	
	public PrakratiRequest setUserId(Long userId) {
		this.userId = userId;
		return this;
	}
	
	public PrakratiRequest setCompanyId(Long companyId) {
		this.companyId = companyId;
		return this;
	}

	public PrakratiRequest addParamter(String name, Object value) {
		if (this.parameters == null) {
			parameters = new HashMap<String, Object>();
		}
		parameters.put(name, value);
		return this;
	}
	
	@Override
	public String toString() {
		return "PrakratiRequest [userId=" + userId + ", companyId=" + companyId
				+ ", pageable=" + pageable + ", parameters=" + parameters + "]";
	}
	
}
