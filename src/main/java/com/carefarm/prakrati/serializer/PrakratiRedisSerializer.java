package com.carefarm.prakrati.serializer;

import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.SerializationException;

import com.carefarm.prakrati.entity.User;

public class PrakratiRedisSerializer implements RedisSerializer<User> {

	@Override
	public User deserialize(byte[] bytes) throws SerializationException {
		return null;
	}

	@Override
	public byte[] serialize(User user) throws SerializationException {
		return null;
	}

}
