package com.carefarm.prakrati.constants;

import java.util.Objects;
import java.util.function.Predicate;

public interface Predicates {
	Predicate<Object> notNull = Objects::nonNull;
	Predicate<Object> isNull = Objects::isNull;
}
