package com.carefarm.prakrati.constants;

public class Endpoints {

	public static final String NOTIFICATION_SEND = "/notification/send";
	public static final String NOTIFICATION_BID = "/notification/bid";
	public static final String NOTIFICATION_PRODUCT = "/notification/product";
	
	public static final String PRAKRATI_NOTIFICATIONS = "direct:prakrati.notifications";
	
	public static final String PRAKRATI_BIDS = "direct:prakrati.bids";
	
	public static final String BIDDABLE_PRODUCTS = "direct:products.biddable";
	
	public static final String PRODUCT_ASSIGNED = "seda:product.assigned?waitForTaskToComplete=Never";
	public static final String PRODUCT_SOLDOUT = "seda:product.soldOut?waitForTaskToComplete=Never";
	
	public static final String DEMAND_ASSIGNED = "seda:demand.assigned?waitForTaskToComplete=Never";
	public static final String DEMAND_FULFILLED = "seda:demand.fulfilled?waitForTaskToComplete=Never";
	
	public static final String ORDER_PLACED = "seda:order.placed?waitForTaskToComplete=Never";
	public static final String ORDER_SHIPPED = "seda:order.shipped?waitForTaskToComplete=Never";
	public static final String ORDER_ASSIGNED = "seda:order.assigned?waitForTaskToComplete=Never";
	public static final String ORDER_DISPATCHED = "seda:order.dispatched?waitForTaskToComplete=Never";
	public static final String ORDER_DELIVERED = "seda:order.delivered?waitForTaskToComplete=Never";
	
}
