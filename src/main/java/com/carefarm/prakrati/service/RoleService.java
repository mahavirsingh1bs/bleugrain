package com.carefarm.prakrati.service;

import java.util.List;
import java.util.Map;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.web.bean.RoleBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.RoleForm;

public interface RoleService {
	List<RoleBean> findAll();
	
	Map<String, Object> findByKeyword(SearchRequest searchRequest);

	void create(RoleForm form, User user) throws ServiceException;
	
	void delete(String uniqueId);

	RoleBean findDetails(long roleId, Object object);

	void update(RoleBean roleBean, User modifiedBy) throws ServiceException;
}
