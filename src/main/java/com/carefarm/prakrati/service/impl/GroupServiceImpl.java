package com.carefarm.prakrati.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.entity.Group;
import com.carefarm.prakrati.entity.Role;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.repository.GroupRepository;
import com.carefarm.prakrati.repository.RoleRepository;
import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.search.repository.GroupSearchRepository;
import com.carefarm.prakrati.service.GroupService;
import com.carefarm.prakrati.service.helper.GroupHelper;
import com.carefarm.prakrati.service.helper.RoleHelper;
import com.carefarm.prakrati.web.bean.GroupBean;
import com.carefarm.prakrati.web.bean.RoleBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.model.GroupForm;

@Service
public class GroupServiceImpl extends PrakratiServiceImpl implements GroupService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(GroupServiceImpl.class);
	
	@Autowired
	private GroupRepository groupRepository;

	@Autowired
	private GroupSearchRepository groupSearchRepository;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private RoleHelper roleHelper;

	@Autowired
	private GroupHelper groupHelper;
	
	@Override
	public List<GroupBean> findAll() {
		List<GroupBean> groups = new ArrayList<>();
		for (Group group : groupRepository.findAll()) {
			GroupBean groupBean = mapper.map(group, GroupBean.class);
			groups.add(groupBean);
		}
		return groups;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Map<String, Object> findByKeyword(SearchRequest searchRequest) {
		Map<String, Object> data = new HashMap<>();
		
		PageRequest pageRequest = createPageRequest(searchRequest);
		
		SearchResult<Long> result = groupSearchRepository.findByKeywordContains(searchRequest, pageRequest);
		long totalRecord = result.getTotalHits();
		Iterable<Group> groups = groupRepository.findAll(result.getResults());
		
		List<GroupBean> groupBeans = new ArrayList<>();
		for (Group group : groups) {
			GroupBean groupBean = mapper.map(group, GroupBean.class);
			this.mapBasicFields(group, groupBean);
			groupBeans.add(groupBean);
		}
		
		data.put("recordsTotal", totalRecord);
		data.put("recordsFiltered", totalRecord);
		data.put("data", groupBeans);
		return data;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(GroupForm form, User createdBy) throws ServiceException {
		Group group = mapper.map(form, Group.class);
		group.setUniqueId(UUID.randomUUID().toString());
		
		form.getSelectedRoleIds()
			.forEach(selectedRoleId -> group.addRole(roleRepository.findOne(selectedRoleId)));
		
		group.setCreatedBy(createdBy);
		group.setDateCreated(Calendar.getInstance().getTime());
		groupRepository.save(group);
		groupHelper.publishIndexEvent(group, EventType.INSERT);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(String uniqueId) {
		Group group = groupRepository.findOne(NumberUtils.toLong(uniqueId));
		group.setArchived(Boolean.TRUE);
		groupHelper.publishIndexEvent(group, EventType.DELETE);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public GroupBean findDetails(long groupId, Object object) {
		Group group = groupRepository.findOne(groupId);
		GroupBean groupBean = mapper.map(group, GroupBean.class);
		this.mapBasicFields(group, groupBean);
		for (Role role : group.getRoles()) {
			groupBean.addSelectedRoleId(role.getId());
			groupBean.addSelectedRole(mapper.map(role, RoleBean.class));
		}
		return groupBean;
	}

	private void mapBasicFields(Group group, GroupBean groupBean) {
		if (group.getCreatedBy() != null) {
			groupBean.setCreatedBy(mapper.map(group.getCreatedBy(), UserBean.class));
		}
		
		if (group.getModifiedBy() != null) {
			groupBean.setModifiedBy(mapper.map(group.getModifiedBy(), UserBean.class));
		}
		
		if (group.getRoles() != null) {
			groupBean.setRoles(roleHelper.convertEntitiesToBeans(group.getRoles(), RoleBean.class));
		}
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(GroupBean groupBean, User modifiedBy) throws ServiceException {
		LOGGER.info("Updating role details");
		Group group = groupRepository.findOne(groupBean.getId());
		groupHelper.convertBeanToEntity(group, groupBean);
		Set<Role> roles = new HashSet<>();
		for (Long selectedRoleId : groupBean.getSelectedRoleIds()) {
			roles.add(roleRepository.findOne(selectedRoleId));
		}
		group.setRoles(roles);
		group.setModifiedBy(modifiedBy);
		group.setDateModified(Calendar.getInstance().getTime());
		groupHelper.publishIndexEvent(group, EventType.UPDATE);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<GroupBean> findByMatchingKeywords(String keywords) {
		List<Group> matchingGroups = groupRepository.findByMatchingName("%" + keywords + "%");
		List<GroupBean> matchingGroupBeans = new ArrayList<>();
		matchingGroups.forEach(group -> matchingGroupBeans.add(mapper.map(group, GroupBean.class)));
		return matchingGroupBeans;
	}
	
}
