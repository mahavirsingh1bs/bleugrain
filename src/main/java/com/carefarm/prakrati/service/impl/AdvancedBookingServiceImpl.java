/**
 * 
 */
package com.carefarm.prakrati.service.impl;

import static com.carefarm.prakrati.util.NotificationCategory.ADVANCED_BOOKING;
import static org.springframework.transaction.annotation.Propagation.REQUIRES_NEW;

import java.util.Date;
import java.util.Locale;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.common.util.Constants;
import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.entity.AdvancedBooking;
import com.carefarm.prakrati.repository.AdvancedBookingRepository;
import com.carefarm.prakrati.service.AdvancedBookingService;
import com.carefarm.prakrati.web.model.AdvancedBookingForm;

/**
 * @author msingh
 *
 */
@Service
public class AdvancedBookingServiceImpl extends PrakratiServiceImpl implements AdvancedBookingService {

	@Autowired
	private AdvancedBookingRepository advancedBookingRepository;
	
	@Override
	@Transactional(propagation = REQUIRES_NEW)
	public void advanceBooking(AdvancedBookingForm form) {
		AdvancedBooking advancedBooking = mapper.map(form, AdvancedBooking.class);
		advancedBooking.setUniqueId(UUID.randomUUID().toString());
		advancedBooking.setDateCreated(Date.from(Environment.clock().instant()));
		advancedBookingRepository.save(advancedBooking);
		
		Object[] params = new Object[] { form.getFirstName(), form.getLastName() };
        String notification = context.getMessage(Constants.ADVANCED_BOOKING, params, Locale.US);
        notificationHelper.sendNotification(notification, ADVANCED_BOOKING);
	}

}
