package com.carefarm.prakrati.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.entity.NotificationCategory;
import com.carefarm.prakrati.repository.NotificationCategoryRepository;
import com.carefarm.prakrati.service.NotificationCategoryService;
import com.carefarm.prakrati.web.bean.NotificationCategoryBean;

@Service
public class NotificationCategoryServiceImpl extends PrakratiServiceImpl implements NotificationCategoryService {

	@Autowired
	private NotificationCategoryRepository categoryRepository;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NotificationCategoryBean> findAll() {
		List<NotificationCategoryBean> categories = new ArrayList<>();
		for (NotificationCategory category : categoryRepository.findAll()) {
			NotificationCategoryBean categoryBean = mapper.map(category, NotificationCategoryBean.class);
			categories.add(categoryBean);
		}
		return categories;
	}

}
