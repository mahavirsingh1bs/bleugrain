package com.carefarm.prakrati.service.impl;

import static com.carefarm.prakrati.web.util.PrakratiConstants.COMPANY_ID;
import static com.carefarm.prakrati.web.util.PrakratiConstants.USER_ID;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.codec.binary.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.common.util.Constants;
import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.entity.Address;
import com.carefarm.prakrati.entity.AddressEntity;
import com.carefarm.prakrati.entity.Company;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.repository.AddressRepository;
import com.carefarm.prakrati.repository.CompanyRepository;
import com.carefarm.prakrati.repository.CountryRepository;
import com.carefarm.prakrati.repository.StateRepository;
import com.carefarm.prakrati.service.AddressService;
import com.carefarm.prakrati.service.helper.AddressEntityHelper;
import com.carefarm.prakrati.util.AddressCategory;
import com.carefarm.prakrati.web.bean.AddressBean;
import com.carefarm.prakrati.web.bean.AddressEntityBean;
import com.carefarm.prakrati.web.bean.CountryBean;
import com.carefarm.prakrati.web.bean.StateBean;
import com.carefarm.prakrati.web.model.AddressEntityForm;
import com.carefarm.prakrati.web.model.AddressForm;
import com.carefarm.prakrati.web.util.PrakratiConstants;

@Service
public class AddressServiceImpl extends PrakratiServiceImpl implements AddressService {

	@Autowired
	private CompanyRepository companyRepository;
	
	@Autowired
	private StateRepository stateRepository;
	
	@Autowired
	private CountryRepository countryRepository;
	
	@Autowired
	private AddressRepository addressRepository;

	@Autowired
	private AddressEntityHelper addressEntityHelper;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AddressEntityBean> findDeliveryAddresses(
			Map<String, Object> params) {
		List<AddressEntity> deliveryAddresses = null;
		if (Predicates.notNull.test(params.get(COMPANY_ID))) {
			Long companyId = (Long ) params.get(COMPANY_ID);
			deliveryAddresses = addressRepository.findByCompanyIdAndCategory(companyId, AddressCategory.DELIVERY);
		} else {
			Long userId = (Long ) params.get(USER_ID);
			deliveryAddresses = addressRepository.findByUserIdAndCategory(userId, AddressCategory.DELIVERY);
		}
		
		List<AddressEntityBean> deliveryAddressBeans = new ArrayList<>();
		deliveryAddresses.forEach(address -> deliveryAddressBeans.add(mapper.map(address, AddressEntityBean.class)));
		return deliveryAddressBeans;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<AddressEntityBean> findBillingAddresses(Map<String, Object> params) {
		List<AddressEntity> billingAddresses = null;
		if (Predicates.notNull.test(params.get(COMPANY_ID))) {
			Long companyId = (Long ) params.get(COMPANY_ID);
			billingAddresses = addressRepository.findByCompanyIdAndCategory(companyId, AddressCategory.BILLING);
		} else {
			Long userId = (Long ) params.get(USER_ID);
			billingAddresses = addressRepository.findByUserIdAndCategory(userId, AddressCategory.BILLING);
		}
		
		List<AddressEntityBean> billingAddressBeans = new ArrayList<>();
		billingAddresses.forEach(address -> billingAddressBeans.add(mapper.map(address, AddressEntityBean.class)));
		return billingAddressBeans;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public AddressEntityBean findDefaultDeliveryAddress(
			Map<String, Object> params) {
		List<AddressEntity> deliveryAddresses = null; 
		if (Predicates.notNull.test(params.get(COMPANY_ID))) {
			Long companyId = (Long ) params.get(COMPANY_ID);
			deliveryAddresses = addressRepository.findByCompanyIdAndCategoryAndType(companyId, AddressCategory.DELIVERY, Boolean.TRUE);
		} else {
			Long userId = (Long ) params.get(USER_ID);
			deliveryAddresses = addressRepository.findByUserIdAndCategoryAndType(userId, AddressCategory.DELIVERY, Boolean.TRUE);
		}
		
		if (deliveryAddresses.size() == 1) {
			return mapper.map(deliveryAddresses.get(0), AddressEntityBean.class);
		}
		return null;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public AddressEntityBean findDefaultBillingAddress(Map<String, Object> params) {
		List<AddressEntity> billingAddresses = null; 
		if (Predicates.notNull.test(params.get(COMPANY_ID))) {
			Long companyId = (Long ) params.get(COMPANY_ID);
			billingAddresses = addressRepository.findByCompanyIdAndCategoryAndType(companyId, AddressCategory.BILLING, Boolean.TRUE);
		} else {
			Long userId = (Long ) params.get(USER_ID);
			billingAddresses = addressRepository.findByUserIdAndCategoryAndType(userId, AddressCategory.BILLING, Boolean.TRUE);
		}
		
		if (billingAddresses.size() == 1) {
			return mapper.map(billingAddresses.get(0), AddressEntityBean.class);
		}
		return null;
	}	
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void addAddress(AddressForm addressForm, String userNo, User currentUser) {
		User user = userRepository.findByUserNo(userNo);
		
		Address address = mapper.map(addressForm, Address.class);
		address.setState(stateRepository.findOne(addressForm.getStateId()));
		address.setCountry(countryRepository.findOne(addressForm.getCountryId()));
		user.setAddress(address);
		user.setModifiedBy(currentUser);
		user.setDateModified(Date.from(Environment.clock().instant()));
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void editAddress(AddressBean addressBean, String userNo, User currentUser) {
		User user = userRepository.findByUserNo(userNo);
		
		Address address = mapper.map(addressBean, Address.class);
		user.setAddress(address);
		user.setModifiedBy(currentUser);
		user.setDateModified(Date.from(Environment.clock().instant()));
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void addBillingAddress(AddressEntityForm billingAddressForm, String userNo, User currentUser) {
		User user = userRepository.findByUserNo(userNo);
		
		AddressEntity billingAddress = mapper.map(billingAddressForm, AddressEntity.class);
		billingAddress.setAddressCategory(AddressCategory.BILLING);
		this.setBillingAddressFor(user, billingAddress, billingAddressForm.isDefaultAddress());
		billingAddress.setCreatedBy(currentUser);
		billingAddress.setDateCreated(Date.from(Environment.clock().instant()));
		user.addBillingAddress(billingAddress);
	}

	private void setBillingAddressFor(User user, AddressEntity billingAddress, boolean isDefault) {
		if (user.isACompanyUser()) {
			Company company = user.getCompany();
			billingAddress.setCompany(user.getCompany());
			if (isDefault) {
				company.getBillingAddresses()
					.forEach(address -> address.setDefaultAddress(Boolean.FALSE));
				company.setDefaultBillingAddress(billingAddress);
			} 
		} else {
			billingAddress.setUser(user);
			if (isDefault) {
				user.getBillingAddresses()
					.forEach(address -> address.setDefaultAddress(Boolean.FALSE));
				user.setDefaultBillingAddress(billingAddress);
			}
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void addDeliveryAddress(AddressEntityForm deliveryAddressForm, String userNo, User currentUser) {
		User user = userRepository.findByUserNo(userNo);
		
		AddressEntity deliveryAddress = mapper.map(deliveryAddressForm, AddressEntity.class);
		deliveryAddress.setAddressCategory(AddressCategory.DELIVERY);
		this.setDeliveryAddressFor(user, deliveryAddress, deliveryAddressForm.isDefaultAddress());
		deliveryAddress.setCreatedBy(currentUser);
		deliveryAddress.setDateCreated(Date.from(Environment.clock().instant()));
		user.addDeliveryAddress(deliveryAddress);
	}

	private void setDeliveryAddressFor(User user, AddressEntity deliveryAddress, boolean isDefault) {
		if (user.isACompanyUser()) {
			Company company = user.getCompany();
			deliveryAddress.setCompany(user.getCompany());
			if (isDefault) {
				company.getDeliveryAddresses()
					.forEach(address -> address.setDefaultAddress(Boolean.FALSE));
				company.setDefaultDeliveryAddress(deliveryAddress);
			} 
		} else {
			deliveryAddress.setUser(user);
			if (isDefault) {
				user.getDeliveryAddresses()
					.forEach(address -> address.setDefaultAddress(Boolean.FALSE));
				user.setDefaultDeliveryAddress(deliveryAddress);
			}
		}
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void editAddress(AddressEntityBean addressBean, String userNo, User currentUser) {
		AddressEntity addressEntity = mapper.map(addressBean, AddressEntity.class);
		addressEntity.setModifiedBy(currentUser);
		addressEntity.setDateModified(Date.from(Environment.clock().instant()));
		addressRepository.save(addressEntity);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void setDefaultAddress(Long id, String category, String userNo, User currentUser) {
		User user = userRepository.findByUserNo(userNo);
		AddressEntity address = addressRepository.findOne(id);
		if (StringUtils.equals(category, Constants.ADDR_CAT_BILLING)) {
			this.setBillingAddressFor(user, address, Boolean.TRUE);
		} else if (StringUtils.equals(category, Constants.ADDR_CAT_DELIVERY)) {
			this.setDeliveryAddressFor(user, address, Boolean.TRUE);
		}
		address.setDefaultAddress(Boolean.TRUE);
		address.setModifiedBy(currentUser);
		address.setDateModified(Date.from(Environment.clock().instant()));
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(String uniqueId, User modifiedBy) {
		AddressEntity addressEntity = addressRepository.findOne(NumberUtils.toLong(uniqueId));
		addressEntity.setArchived(Boolean.TRUE);
		addressEntity.setModifiedBy(modifiedBy);
		addressEntity.setDateModified(Calendar.getInstance().getTime());
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public AddressEntityBean findAddressDetails(String uniqueId) {
		AddressEntity addressEntity = addressRepository.findOne(NumberUtils.toLong(uniqueId));
		AddressEntityBean addressEntityBean = mapper.map(addressEntity, AddressEntityBean.class);
		addressEntityBean.setState(mapper.map(addressEntity.getState(), StateBean.class));
		addressEntityBean.setCountry(mapper.map(addressEntity.getCountry(), CountryBean.class));
		return addressEntityBean;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public boolean updateAddress(AddressEntityBean addressBean, Map<String, Object> params, User modifiedBy) {
		AddressEntity address = addressRepository.findOne(addressBean.getId());
		addressEntityHelper.convertBeanToEntity(addressBean, address);
		
		if (addressBean.isDefaultAddress()) {
			AddressCategory category = (AddressCategory )params.get(PrakratiConstants.CATEGORY);
			if (Predicates.notNull.test(params.get(COMPANY_ID))) {
				Long companyId = (Long ) params.get(COMPANY_ID);
				Company company = companyRepository.findOne(companyId);
				List<AddressEntity> deliveryAddresses = addressRepository.findByCompanyIdAndCategory(companyId, category);
				deliveryAddresses.forEach(deliveryAddress -> deliveryAddress.setDefaultAddress(Boolean.FALSE));
				company.setDefaultDeliveryAddress(address);
				address.setDefaultAddress(Boolean.TRUE);
			} else if (Predicates.notNull.test(params.get(USER_ID))) {
				Long userId = (Long ) params.get(USER_ID);
				User user = userRepository.findOne(userId);
				List<AddressEntity> deliveryAddresses = addressRepository.findByUserIdAndCategory(userId, category);
				deliveryAddresses.forEach(deliveryAddress -> deliveryAddress.setDefaultAddress(Boolean.FALSE));
				user.setDefaultDeliveryAddress(address);
				address.setDefaultAddress(Boolean.TRUE);
			}
		}
		
		address.setModifiedBy(modifiedBy);
		address.setDateModified(Calendar.getInstance().getTime());
		return Boolean.TRUE;
	}

}
