package com.carefarm.prakrati.service.impl;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.entity.Address;
import com.carefarm.prakrati.entity.Branch;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.repository.BranchRepository;
import com.carefarm.prakrati.service.BranchService;
import com.carefarm.prakrati.service.helper.BranchHelper;
import com.carefarm.prakrati.web.bean.BranchBean;
import com.carefarm.prakrati.web.model.BranchForm;

@Service
public class BranchServiceImpl extends PrakratiServiceImpl implements BranchService {
	
	@Autowired
	private BranchRepository branchRepository;
	
	@Autowired
	private BranchHelper branchHelper;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Map<String, Object> findByKeyword(String keyword,
			Pageable pageable, Map<String, Object> params) {
		Map<String, Object> data = new HashMap<>();
		
		Integer draw = (Integer ) params.get("draw");
		Long totalRecord = 0L;
		Page<Branch> branches = null;
		if (params.get("companyId") != null) {
			Long companyId = (Long ) params.get("companyId");
			totalRecord = branchRepository.countByCompanyId(companyId);
			branches = branchRepository.findByCompanyId(companyId, pageable);
		} else {
			totalRecord = branchRepository.count();
			branches = branchRepository.findAll(pageable);
		}
    	
    	List<BranchBean> branchBeans = branchHelper.convertEntitiesToBeans(branches, BranchBean.class);
		data.put("recordsTotal", totalRecord);
		data.put("recordsFiltered", totalRecord);
		data.put("draw", draw);
		data.put("data", branchBeans);
		return data;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(String uniqueId) {
		Branch branch = branchRepository.findOne(NumberUtils.toLong(uniqueId));
		branch.setArchived(Boolean.TRUE);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(BranchForm form, User createdBy) throws ServiceException {
		Calendar calendar = Calendar.getInstance();
		Branch branch = mapper.map(form, Branch.class);
		branch.setCreatedBy(createdBy);
		branch.setDateCreated(calendar.getTime());
		branchRepository.save(branch);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public BranchBean findDetail(String uniqueId) {
		Branch branch = branchRepository.findOne(NumberUtils.toLong(uniqueId));
		return mapper.map(branch, BranchBean.class);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(BranchBean branchBean) throws ServiceException {
		Branch branch = branchRepository.findOne(branchBean.getId());
		branch.setName(branchBean.getName());
		branch.setAddress(mapper.map(branchBean.getAddress(), Address.class));
	}

}
