package com.carefarm.prakrati.service.impl;

import java.sql.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.entity.Expertise;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.repository.ExpertiseRepository;
import com.carefarm.prakrati.service.ExpertiseService;
import com.carefarm.prakrati.service.helper.ExpertiseHelper;
import com.carefarm.prakrati.web.bean.ExpertiseBean;
import com.carefarm.prakrati.web.model.ExpertiseForm;

@Service
public class ExpertiseServiceImpl extends PrakratiServiceImpl implements ExpertiseService {

	@Autowired
	private ExpertiseHelper expertiseHelper;
	
	@Autowired
	private ExpertiseRepository expertiseRepository;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void addExpertise(ExpertiseForm expertiseForm, String userNo, User currentUser) {
		User user = userRepository.findByUserNo(userNo);
		Expertise expertise = mapper.map(expertiseForm, Expertise.class);
		expertise.setUniqueId(UUID.randomUUID().toString());
		setExpertiseOf(user, expertise);
		expertise.setCreatedBy(currentUser);
		expertise.setDateCreated(Date.from(Environment.clock().instant()));
		expertiseRepository.save(expertise);
	}

	private void setExpertiseOf(User user, Expertise expertise) {
		if (user.isACompanyUser()) {
			expertise.setCompany(user.getCompany());
			user.getCompany().addExpertise(expertise);
		} else {
			expertise.setUser(user);
			user.addExpertise(expertise);
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public ExpertiseBean find(String uniqueId) {
		Expertise expertise = expertiseRepository.findByUniqueId(uniqueId);
		return mapper.map(expertise, ExpertiseBean.class);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(ExpertiseBean bean, User currentUser) {
		Expertise expertise = expertiseRepository.findByUniqueId(bean.getUniqueId());
		expertiseHelper.convertBeanToEntity(expertise, bean);
		expertise.setModifiedBy(currentUser);
		expertise.setDateModified(Date.from(Environment.clock().instant()));
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void remove(String uniqueId, User currentUser) {
		Expertise expertise = expertiseRepository.findByUniqueId(uniqueId);
		expertiseRepository.delete(expertise);
	}

}
