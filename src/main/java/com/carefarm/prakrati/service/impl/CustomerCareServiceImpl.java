package com.carefarm.prakrati.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.ObjectUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.entity.CustomerConcern;
import com.carefarm.prakrati.entity.Feedback;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.messaging.AccountEmailHandler;
import com.carefarm.prakrati.repository.CustomerConcernRepository;
import com.carefarm.prakrati.repository.FeedbackRepository;
import com.carefarm.prakrati.repository.RatingRepository;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.service.CustomerCareService;
import com.carefarm.prakrati.web.model.ContactUsForm;
import com.carefarm.prakrati.web.model.FeedbackForm;

@Service
public class CustomerCareServiceImpl extends PrakratiServiceImpl implements CustomerCareService {
	
	@Autowired
	private AccountEmailHandler emailHandler;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private RatingRepository ratingRepository;
	
	@Autowired
	private FeedbackRepository feedbackRepository;
	
	@Autowired
	private CustomerConcernRepository customerConcernRepository;
	
	@Override
	public void processConcern(ContactUsForm form, Map<String, Object> params) throws ServiceException {
		CustomerConcern customerConcern = mapper.map(form, CustomerConcern.class);
		
		if (ObjectUtils.notEqual(params.get("userId"), null)) {
			Long userId = (Long )params.get("userId");
			User user = userRepository.findOne(userId);
			customerConcern.setUser(user);
		}
		
		customerConcern.setCreatedOn(Date.from(Environment.clock().instant()));
		customerConcernRepository.save(customerConcern);
		emailHandler.sendUserConcern(form.getEmailId(), form.getMessage());
	}

	@Override
	public void processFeedback(FeedbackForm form, Map<String, Object> params) throws ServiceException {
		Feedback feedback = new Feedback();
		feedback.setServicesRating(ratingRepository.findOne(form.getServicesRating()));
		feedback.setExecutivesRating(ratingRepository.findOne(form.getExecutivesRating()));
		feedback.setSiteRating(ratingRepository.findOne(form.getSiteRating()));
		feedback.setFeedback(form.getFeedback());
		
		User user = null;
		if (ObjectUtils.notEqual(params.get("userId"), null)) {
			Long userId = (Long )params.get("userId");
			user = userRepository.findOne(userId);
			feedback.setUser(user);
		}
		
		feedback.setCreatedOn(Calendar.getInstance().getTime());
		feedbackRepository.save(feedback);
		emailHandler.sendFeedbackThanks(user.getEmail());
	}

}
