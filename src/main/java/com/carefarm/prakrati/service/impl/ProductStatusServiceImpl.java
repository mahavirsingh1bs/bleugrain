package com.carefarm.prakrati.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.repository.ProductStatusRepository;
import com.carefarm.prakrati.service.ProductStatusService;
import com.carefarm.prakrati.web.bean.ProductStatusBean;

@Service
public class ProductStatusServiceImpl extends PrakratiServiceImpl implements ProductStatusService {
	
	@Autowired
	private ProductStatusRepository productStatusRepository;
	
	@Override
	@Transactional
	public List<ProductStatusBean> findAll() {
		List<ProductStatusBean> productStatuses = new ArrayList<>();
		productStatusRepository.findAll()
			.forEach((ps) -> productStatuses.add(mapper.map(ps, ProductStatusBean.class)));
		return productStatuses;
	}

}
