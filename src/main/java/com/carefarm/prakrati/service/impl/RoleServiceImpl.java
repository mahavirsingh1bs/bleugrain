package com.carefarm.prakrati.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.entity.Role;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.repository.RoleRepository;
import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.repository.RoleSearchRepository;
import com.carefarm.prakrati.service.RoleService;
import com.carefarm.prakrati.service.helper.RoleHelper;
import com.carefarm.prakrati.web.bean.RoleBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.model.RoleForm;

@Service
public class RoleServiceImpl extends PrakratiServiceImpl implements RoleService {

	private static final Logger LOGGER = LoggerFactory.getLogger(RoleServiceImpl.class);
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Autowired
	private RoleSearchRepository roleSearchRepository;
	
	@Autowired
	private RoleHelper roleHelper;
	
	@Override
	public List<RoleBean> findAll() {
		List<RoleBean> roles = new ArrayList<>();
		for (Role role : roleRepository.findAll()) {
			RoleBean roleBean = mapper.map(role, RoleBean.class);
			roles.add(roleBean);
		}
		return roles;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Map<String, Object> findByKeyword(SearchRequest searchRequest) {
		Map<String, Object> data = new HashMap<>();
		List<RoleBean> roleBeans = new ArrayList<>();
		
		PageRequest pageRequest = createPageRequest(searchRequest);
		
		SearchResult<Long> result = roleSearchRepository.findByKeywordContains(searchRequest, pageRequest);
		long totalRecord = result.getTotalHits();
		Iterable<Role> roles = roleRepository.findAll(result.getResults());
		
		for (Role role : roles) {
			RoleBean roleBean = mapper.map(role, RoleBean.class);
			this.mapBasicFields(role, roleBean);
			roleBeans.add(roleBean);
		}
		
		data.put("recordsTotal", totalRecord);
		data.put("recordsFiltered", totalRecord);
		data.put("data", roleBeans);
		return data;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(RoleForm form, User createdBy) throws ServiceException {
		Role role = mapper.map(form, Role.class);
		role.setCreatedBy(createdBy);
		role.setDateCreated(Calendar.getInstance().getTime());
		roleRepository.save(role);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(String uniqueId) {
		Role role = roleRepository.findOne(NumberUtils.toLong(uniqueId));
		role.setArchived(Boolean.TRUE);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(RoleBean roleBean, User modifiedBy) throws ServiceException {
		LOGGER.info("Updating role details");
		Role role = roleRepository.findOne(roleBean.getId());
		roleHelper.convertBeanToEntity(role, roleBean);
		role.setModifiedBy(modifiedBy);
		role.setDateModified(Calendar.getInstance().getTime());
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public RoleBean findDetails(long roleId, Object object) {
		Role role = roleRepository.findOne(roleId);
		RoleBean roleBean = mapper.map(role, RoleBean.class);
		this.mapBasicFields(role, roleBean);
		return roleBean;
	}

	private void mapBasicFields(Role role, RoleBean roleBean) {
		if (role.getCreatedBy() != null) {
			roleBean.setCreatedBy(mapper.map(role.getCreatedBy(), UserBean.class));
		}
		
		if (role.getModifiedBy() != null) {
			roleBean.setModifiedBy(mapper.map(role.getModifiedBy(), UserBean.class));
		}
	}

}
