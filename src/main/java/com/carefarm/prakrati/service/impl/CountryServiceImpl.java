package com.carefarm.prakrati.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.common.entity.Country;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.repository.CountryRepository;
import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.search.repository.CountrySearchRepository;
import com.carefarm.prakrati.service.CountryService;
import com.carefarm.prakrati.service.helper.CountryHelper;
import com.carefarm.prakrati.web.bean.CountryBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.model.CountryForm;

@Service
public class CountryServiceImpl extends PrakratiServiceImpl implements CountryService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CountryServiceImpl.class);
	
	@Autowired
	private CountryRepository countryRepository;
	
	@Autowired
	private CountrySearchRepository countrySearchRepository;
	
	@Autowired
	private CountryHelper countryHelper;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<CountryBean> findAll() {
		List<CountryBean> countries = new ArrayList<>();
		for (Country country : countryRepository.findAll()) {
			CountryBean countryBean = mapper.map(country, CountryBean.class);
			countries.add(countryBean);
		}
		return countries;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Map<String, Object> findByKeyword(SearchRequest searchRequest) {
		Map<String, Object> data = new HashMap<>();
		List<CountryBean> countryBeans = new ArrayList<>();
		
		PageRequest pageRequest = createPageRequest(searchRequest);
		
		SearchResult<Long> result = countrySearchRepository.findByKeywordContains(searchRequest, pageRequest);
		long totalRecord = result.getTotalHits();
		Iterable<Country> countries = countryRepository.findAll(result.getResults());
		
		for (Country country : countries) {
			CountryBean countryBean = mapper.map(country, CountryBean.class);
			if (country.getCreatedBy() != null) {
				UserBean createdBy = mapper.map(country.getCreatedBy(), UserBean.class);
				countryBean.setCreatedBy(createdBy);
			}
			
			if (country.getModifiedBy() != null) {
				UserBean modifiedBy = mapper.map(country.getModifiedBy(), UserBean.class);
				countryBean.setModifiedBy(modifiedBy);
			}
			
			countryBeans.add(countryBean);
		}
		
		data.put("recordsTotal", totalRecord);
		data.put("recordsFiltered", totalRecord);
		data.put("data", countryBeans);
		return data;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(CountryForm form, User createdBy) throws ServiceException {
		Country country = mapper.map(form, Country.class);
		country.setCreatedBy(createdBy);
		country.setDateCreated(Calendar.getInstance().getTime());
		countryRepository.save(country);
		countryHelper.publishIndexEvent(country, EventType.INSERT);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public CountryBean findDetails(long countryId, Object object) {
		Country country = countryRepository.findOne(countryId);
		CountryBean countryBean = mapper.map(country, CountryBean.class);
		
		if (country.getCreatedBy() != null) {
			countryBean.setCreatedBy(mapper.map(country.getCreatedBy(), UserBean.class));
		}
		
		if (country.getModifiedBy() != null) {
			countryBean.setModifiedBy(mapper.map(country.getModifiedBy(), UserBean.class));
		}
		return countryBean;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(CountryBean countryBean, User modifiedBy) throws ServiceException {
		LOGGER.info("Updating country details");
		Country country = countryRepository.findOne(countryBean.getId());
		countryHelper.convertBeanToEntity(country, countryBean);
		country.setModifiedBy(modifiedBy);
		country.setDateModified(Calendar.getInstance().getTime());
		countryHelper.publishIndexEvent(country, EventType.UPDATE);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(String uniqueId) {
		Country country = countryRepository.findOne(NumberUtils.toLong(uniqueId));
		country.setArchived(Boolean.TRUE);
		countryHelper.publishIndexEvent(country, EventType.DELETE);
	}
}
