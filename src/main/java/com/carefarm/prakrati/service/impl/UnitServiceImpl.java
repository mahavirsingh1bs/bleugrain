package com.carefarm.prakrati.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.repository.UnitRepository;
import com.carefarm.prakrati.service.UnitService;
import com.carefarm.prakrati.service.helper.UnitHelper;
import com.carefarm.prakrati.web.bean.UnitBean;

@Service
public class UnitServiceImpl extends PrakratiServiceImpl implements UnitService {

	@Autowired
	private UnitHelper unitHelper;
	
	@Autowired
	private UnitRepository unitRepository;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<UnitBean> findAll() {
		return unitHelper.convertEntitiesToBeans(unitRepository.findAll(), UnitBean.class);
	}

}
