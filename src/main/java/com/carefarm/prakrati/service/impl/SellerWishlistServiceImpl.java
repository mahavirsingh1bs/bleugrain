package com.carefarm.prakrati.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.carefarm.prakrati.entity.SellerWishlist;
import com.carefarm.prakrati.repository.SellerWishlistRepository;
import com.carefarm.prakrati.service.SellerWishlistService;
import com.carefarm.prakrati.web.bean.SellerWishlistBean;

@Service
public class SellerWishlistServiceImpl extends PrakratiServiceImpl implements SellerWishlistService {
	
	@Autowired
	private SellerWishlistRepository wishlistRepository;
	
	@Override
	public Map<String, Object> findByKeyword(String keyword,
			PageRequest pageRequest, Map<String, Object> params) {
		Integer draw = (Integer ) params.get("draw");
		Map<String, Object> data = new HashMap<>();
		Long totalRecord = 0L;
		List<SellerWishlist> wishlist = null;
		if (params.get("companyId") != null) {
			Long companyId = (Long )params.get("companyId");
			totalRecord = wishlistRepository.countByCompanyId(companyId);
			wishlist = wishlistRepository.findByCompanyId(companyId, pageRequest);
		} else {
			Long userId = (Long )params.get("userId");
			totalRecord = wishlistRepository.countByUserId(userId);
			wishlist = wishlistRepository.findByUserId(userId, pageRequest);
		}
		
		List<SellerWishlistBean> wishlistBeans = new ArrayList<>();
		for (SellerWishlist sellerWishlist : wishlist) {
			SellerWishlistBean wishlistBean = mapper.map(sellerWishlist, SellerWishlistBean.class);
			wishlistBeans.add(wishlistBean);
		}
		data.put("recordsTotal", totalRecord);
		data.put("recordsFiltered", totalRecord);
		data.put("draw", draw);
		data.put("data", wishlistBeans);
		return data;
	}

}
