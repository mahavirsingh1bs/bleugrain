package com.carefarm.prakrati.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.OTPVerificationException;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.functional.AdminDashboardProvider;
import com.carefarm.prakrati.functional.AgentDashboardProvider;
import com.carefarm.prakrati.functional.BrokerDashboardProvider;
import com.carefarm.prakrati.functional.CompanyAdminDashboardProvider;
import com.carefarm.prakrati.functional.CustomerDashboardProvider;
import com.carefarm.prakrati.functional.EmployeeDashboardProvider;
import com.carefarm.prakrati.functional.FarmerDashboardProvider;
import com.carefarm.prakrati.functional.RetailerDashboardProvider;
import com.carefarm.prakrati.messaging.AccountEmailHandler;
import com.carefarm.prakrati.repository.ContactRepository;
import com.carefarm.prakrati.repository.DiscussionRepository;
import com.carefarm.prakrati.repository.NoteRepository;
import com.carefarm.prakrati.repository.NotificationRepository;
import com.carefarm.prakrati.repository.UpcomingEventRepository;
import com.carefarm.prakrati.service.ProfileService;
import com.carefarm.prakrati.service.helper.NotificationHelper;
import com.carefarm.prakrati.util.VerifCodeGenerator;
import com.carefarm.prakrati.web.bean.ContactBean;
import com.carefarm.prakrati.web.bean.CountryBean;
import com.carefarm.prakrati.web.bean.DiscussionBean;
import com.carefarm.prakrati.web.bean.ImageBean;
import com.carefarm.prakrati.web.bean.NoteBean;
import com.carefarm.prakrati.web.bean.NotificationBean;
import com.carefarm.prakrati.web.bean.UpcomingEventBean;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.model.VerifyMobileNoForm;

@Service
public class ProfileServiceImpl extends PrakratiServiceImpl implements ProfileService {
	
	@Value("${application.context.path}")
	protected String contextPath;
	
	@Autowired
	private AdminDashboardProvider adminDashboardProvider;
	
	@Autowired
	private CompanyAdminDashboardProvider companyAdminDashboardProvider;
	
	@Autowired
	private AgentDashboardProvider agentDashboardProvider;
	
	@Autowired
	private BrokerDashboardProvider brokerDashboardProvider;
	
	@Autowired
	private RetailerDashboardProvider retailerDashboardProvider;
	
	@Autowired
	private FarmerDashboardProvider farmerDashboardProvider;
	
	@Autowired
	private CustomerDashboardProvider customerDashboardProvider;
	
	@Autowired
	private EmployeeDashboardProvider employeeDashboardProvider;
		
	@Autowired
	private NotificationRepository notificationRepository;
	
	@Autowired
	private NoteRepository noteRepository;

	@Autowired
	private ContactRepository contactRepository;
	
	@Autowired
	private UpcomingEventRepository upcomingEventRepository;
	
	@Autowired
	private DiscussionRepository discussionRepository;
	
	@Autowired
	private NotificationHelper notificationHelper;
	
	@Autowired
	private AccountEmailHandler emailHandler;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Map<String, Object> getDashboardDetails(User currentUser) {
		User user = userRepository.findByUserNo(currentUser.getUserNo());
		
		Map<String, Object> dashboardDetails = new HashMap<>(); 
		
		List<NotificationBean> notifications = notificationHelper.findNotificationBy(user.getGroup().getUniqueId(), user.getUserNo());
		dashboardDetails.put("notifications", notifications);
		
		List<ContactBean> contacts = new ArrayList<>();
		contactRepository.findByGroupId(user.getGroup().getId())
			.stream().forEach(contact -> {
				ContactBean contactBean = mapper.map(contact, ContactBean.class);
				UserBean contactPerson = mapper.map(contact.getUser(), UserBean.class);
				contactPerson.setDefaultImage(mapper.map(contact.getUser().getDefaultImage(), ImageBean.class));
				contactBean.setUser(contactPerson);
				contactBean.setCountry(mapper.map(contact.getCountry(), CountryBean.class));
				contacts.add(contactBean);
			});
		dashboardDetails.put("contacts", contacts);
		
		List<DiscussionBean> discussions = new ArrayList<>();
		discussionRepository.findByGroupId(user.getGroup().getUniqueId())
			.forEach((d) -> {
				DiscussionBean discussionBean = mapper.map(d, DiscussionBean.class);
				discussionBean.setUser(mapper.map(d.getUser(), UserBean.class));
				discussions.add(discussionBean);
			});
		dashboardDetails.put("discussions", discussions);
		
		List<UpcomingEventBean> upcomingEvents = new ArrayList<>();
		if (currentUser.hasRole("ROLE_COMPANY_ADMIN")) {
			upcomingEventRepository.findByGroupIdOrUserIdOrCompanyId(user.getGroup().getUniqueId(), user.getUserNo(), user.getCompany().getUniqueId())
				.forEach((upcomingEvent) -> {
					upcomingEvents.add(mapper.map(upcomingEvent, UpcomingEventBean.class));
				});
		} else {
			upcomingEventRepository.findByGroupIdOrUserId(user.getGroup().getUniqueId(), user.getUserNo())
				.forEach((upcomingEvent) -> {
					upcomingEvents.add(mapper.map(upcomingEvent, UpcomingEventBean.class));
				});
		}
		dashboardDetails.put("upcomingEvents", upcomingEvents);
		
		List<NoteBean> notes = new ArrayList<>();
		noteRepository.findByUserNo(currentUser.getUserNo())
			.forEach((note) -> {
				notes.add(mapper.map(note, NoteBean.class));
			});
		dashboardDetails.put("notes", notes);
		
		if (currentUser.hasRole("ROLE_ADMIN")) {
			adminDashboardProvider.provideDashboard(dashboardDetails, currentUser.getUserNo());
		} else if (currentUser.hasRole("ROLE_COMPANY_ADMIN")) {
			companyAdminDashboardProvider.provideDashboard(dashboardDetails, currentUser.getCompany().getUniqueId());
		} else if (currentUser.hasRole("ROLE_AGENT")) {
			agentDashboardProvider.provideDashboard(dashboardDetails, currentUser.getUserNo());
		} else if (currentUser.hasRole("ROLE_BROKER")) {
			brokerDashboardProvider.provideDashboard(dashboardDetails, currentUser.getUserNo());
		} else if (currentUser.hasRole("ROLE_RETAILER")) {
			retailerDashboardProvider.provideDashboard(dashboardDetails, currentUser.getUserNo());
		} else if (currentUser.hasRole("ROLE_FARMER")) {
			farmerDashboardProvider.provideDashboard(dashboardDetails, currentUser.getUserNo());
		} else if (currentUser.hasRole("ROLE_CUSTOMER")) {
			customerDashboardProvider.provideDashboard(dashboardDetails, currentUser.getUserNo());
		} else {
			employeeDashboardProvider.provideDashboard(dashboardDetails, currentUser.getUserNo());
		}
		
		return dashboardDetails;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void sendMobileVerificationCode(String userNo, User currentUser) {
		User user = userRepository.findByUserNo(userNo);
		String verificCode = VerifCodeGenerator.generateVerifCode();
		user.setMobileVerifyCode(verificCode);
		this.sendVerificationCode(user.getMobileNo(), verificCode);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void sendEmailVerificationLink(String userNo, User currentUser) {
		User user = userRepository.findByUserNo(userNo);
		String emailVerifyCode = UUID.randomUUID().toString();
		String verifyEmailPath = contextPath + "#/VerifyEmailAddress?" + "verificationCode=" + emailVerifyCode;
		user.setEmailVerifyCode(emailVerifyCode);
		emailHandler.sendVerificationLink(user.getEmail(), verifyEmailPath, user);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void verifyOneTimePassword(VerifyMobileNoForm verifyMobileNoForm, User currentUser) throws ServiceException {
		User user = userRepository.findByUserNo(verifyMobileNoForm.getUserNo());
		if (StringUtils.equalsIgnoreCase(user.getMobileVerifyCode(), verifyMobileNoForm.getOneTimePassword())) {
			user.setMobileVerifyCode(StringUtils.EMPTY);
			user.setMobileVerified(Boolean.TRUE);
		} else {
			throw new OTPVerificationException("The OTP(One Time Password) which got entered is not correct.");
		}
	}

}
