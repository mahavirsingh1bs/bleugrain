package com.carefarm.prakrati.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.entity.Designation;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.repository.DesignationRepository;
import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.search.repository.DesignationSearchRepository;
import com.carefarm.prakrati.service.DesignationService;
import com.carefarm.prakrati.service.helper.DesignationHelper;
import com.carefarm.prakrati.web.bean.DesignationBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.model.DesignationForm;

@Service
public class DesignationServiceImpl extends PrakratiServiceImpl implements DesignationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(DesignationServiceImpl.class);
	
	@Autowired
	private DesignationRepository designationRepository; 
	
	@Autowired
	private DesignationSearchRepository designationSearchRepository; 
	
	@Autowired
	private DesignationHelper designationHelper;
	
	@Override
	public List<DesignationBean> findAll() {
		List<DesignationBean> designations = new ArrayList<>();
		for (Designation designation : designationRepository.findAll()) {
			DesignationBean designationBean = mapper.map(designation, DesignationBean.class);
			designations.add(designationBean);
		}
		return designations;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Map<String, Object> findByKeyword(SearchRequest searchRequest) {
		Map<String, Object> data = new HashMap<>();
		List<DesignationBean> designationBeans = new ArrayList<>();
		
		PageRequest pageRequest = createPageRequest(searchRequest);
		
		SearchResult<Long> result = designationSearchRepository.findByKeywordContains(searchRequest, pageRequest);
		long totalRecord = result.getTotalHits();
		Iterable<Designation> designations = designationRepository.findAll(result.getResults());
		
		for (Designation designation : designations) {
			DesignationBean designationBean = mapper.map(designation, DesignationBean.class);
			if (designation.getCreatedBy() != null) {
				UserBean createdBy = mapper.map(designation.getCreatedBy(), UserBean.class);
				designationBean.setCreatedBy(createdBy);
			}
			
			if (designation.getModifiedBy() != null) {
				UserBean modifiedBy = mapper.map(designation.getModifiedBy(), UserBean.class);
				designationBean.setModifiedBy(modifiedBy);
			}
			
			designationBeans.add(designationBean);
		}
		
		data.put("recordsTotal", totalRecord);
		data.put("recordsFiltered", totalRecord);
		data.put("data", designationBeans);
		return data;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(DesignationForm form, User createdBy) throws ServiceException {
		Designation designation = mapper.map(form, Designation.class);
		designation.setCreatedBy(createdBy);
		designation.setDateCreated(Calendar.getInstance().getTime());
		designationRepository.save(designation);
		designationHelper.publishIndexEvent(designation, EventType.INSERT);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public DesignationBean findDetails(long designationId, Object object) {
		Designation designation = designationRepository.findOne(designationId);
		DesignationBean designationBean = mapper.map(designation, DesignationBean.class);
		
		if (designation.getCreatedBy() != null) {
			designationBean.setCreatedBy(mapper.map(designation.getCreatedBy(), UserBean.class));
		}
		
		if (designation.getModifiedBy() != null) {
			designationBean.setModifiedBy(mapper.map(designation.getModifiedBy(), UserBean.class));
		}
		return designationBean;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(DesignationBean designationBean, User modifiedBy) throws ServiceException {
		LOGGER.info("Updating designation details");
		Designation designation = designationRepository.findOne(designationBean.getId());
		designationHelper.convertBeanToEntity(designation, designationBean);
		designation.setModifiedBy(modifiedBy);
		designation.setDateModified(Calendar.getInstance().getTime());
		designationHelper.publishIndexEvent(designation, EventType.UPDATE);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(String uniqueId) {
		Designation designation = designationRepository.findOne(NumberUtils.toLong(uniqueId));
		designation.setArchived(Boolean.TRUE);
		designationHelper.publishIndexEvent(designation, EventType.DELETE);
	}
}
