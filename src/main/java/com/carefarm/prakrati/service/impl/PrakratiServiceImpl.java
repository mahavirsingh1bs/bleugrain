package com.carefarm.prakrati.service.impl;

import static com.carefarm.prakrati.common.util.Constants.PLUS;

import java.text.MessageFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.multipart.MultipartFile;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.delivery.util.OrderStatus;
import com.carefarm.prakrati.entity.AbstractEntity;
import com.carefarm.prakrati.entity.Role;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.messaging.MobileMessanger;
import com.carefarm.prakrati.repository.CountryRepository;
import com.carefarm.prakrati.repository.GroupRepository;
import com.carefarm.prakrati.repository.ImageRepository;
import com.carefarm.prakrati.repository.LanguageRepository;
import com.carefarm.prakrati.repository.NotificationRepository;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.search.repository.UserSearchRepository;
import com.carefarm.prakrati.service.helper.AmazonS3Helper;
import com.carefarm.prakrati.service.helper.NotificationHelper;
import com.carefarm.prakrati.util.DemandStatus;
import com.carefarm.prakrati.util.GlobalConstants;
import com.carefarm.prakrati.util.ImageCategory;
import com.carefarm.prakrati.util.ImageType;
import com.carefarm.prakrati.util.ProductState;
import com.carefarm.prakrati.web.bean.SearchRequest;

public abstract class PrakratiServiceImpl {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PrakratiServiceImpl.class);
	
	protected final String[] notInOrderStatuses = { OrderStatus.DELIVERED.getCode() };
	protected final String[] productStatuses = { ProductState.ADDED_NEW.getCode(), ProductState.ONCE_PRICED.getCode() };
	protected final String[] demandStatuses = { DemandStatus.OPEN.getCode(), DemandStatus.TENTATIVE.getCode() };
	
	protected static final String DEFAULT_PASSWD = "prak1234";
	
	protected static final String PERC = "%";

	protected static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(GlobalConstants.DATE_FORMAT.getValue());
	
	protected static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
	
	protected static final NumberFormat NUMBER_FORMATTER = NumberFormat.getCurrencyInstance();
	
	@Value("${verify.user.email.from}")
	protected String from;

	@Value("${verify.user.email.subject}")
	protected String subject;
	
	@Value("${verify.user.email.content}")
	protected String content;
	
	@Value("${verify.mobile.enabled}")
	protected boolean isVerifyMobileEnabled;
	
	@Value("${verify.email.enabled}")
	protected boolean isVerifyEmailEnabled;
	
	@Value("${vocac.sms.adhoc.mobile.verification}")
	protected String adhocVerificMessage;
	
	@Value("${vocac.sms.mobile.verification}")
	protected String mobileVerificMessage;
	
	@Autowired
	protected ApplicationContext context;
	
	@Autowired
	protected DozerBeanMapper mapper;
	
	@Autowired
	protected UserRepository userRepository;

	@Autowired
	protected UserSearchRepository userSearchRepository;
	
	@Autowired
	protected GroupRepository groupRepository;
	
	@Autowired
	protected CountryRepository countryRepository;
	
	@Autowired
	protected LanguageRepository languageRepository;
	
	@Autowired
	protected ImageRepository imageRepository;
	
	@Autowired
	protected NotificationRepository notificationRepository;
	
	@Autowired
	protected MobileMessanger mobileMessanger;
/*
	@Autowired
    protected ApplicationEventPublisher eventPublisher;
*/	
	@Autowired
	protected NotificationHelper notificationHelper;
	
	@Autowired
	protected AmazonS3Helper amazonS3Helper;
	
	protected PageRequest createPageRequest(SearchRequest searchRequest) {
		PageRequest pageRequest;
		if (searchRequest.getSortFields() != null) {
			String[] sortFields = searchRequest.getSortFields().split(",");
			Direction direction = searchRequest.getDirection();
			
			Sort sort = new Sort(direction, sortFields[0]);
			for (int index = 1; index < sortFields.length; index++) { 
				sort = sort.and(new Sort(direction, sortFields[index]));
			}
			pageRequest = new PageRequest(searchRequest.getPage(), searchRequest.getPageSize(), sort);
		} else {
			pageRequest = new PageRequest(searchRequest.getPage(), searchRequest.getPageSize());
		}
		return pageRequest;
	}
	
	protected boolean hasRole(User currentUser, String expRole) {
		for (Role role: currentUser.getGroup().getRoles()) {
			if (role.getName().equalsIgnoreCase(expRole)) {
				return true;
			}
		}
		return false;
	}

	protected Date getParseDate(String date) {
		try {
			return DATE_FORMAT.parse(date);
		} catch (ParseException e) {
			LOGGER.error("Got an error while trying to format date: " + date);
			throw new ServiceException("Got an error while trying to format date: " + date);
		}
	}
	
	protected String getFormatDate(Date date) {
		return DATE_FORMAT.format(date);
	}
	
	protected void sendVerificationCode(String mobileNo, String verificationCode) {
		if (isVerifyMobileEnabled && Predicates.notNull.test(mobileNo)) {
        	String message = MessageFormat.format(mobileVerificMessage, verificationCode);
        	if (mobileNo.startsWith(PLUS)) {
        		mobileMessanger.sendMessage(mobileNo.substring(mobileNo.indexOf(PLUS) + 1), message);
        	} else {
        		mobileMessanger.sendMessage(mobileNo, message);
        	}
        }
	}
	
	protected void handleImageInternal(MultipartFile image, User user, ImageCategory category) {
		if (Predicates.notNull.test(image)) {
        	user.setDefaultImage(amazonS3Helper.handleImage(image, ImageType.USERS, user));
		} else {
			user.setDefaultImage(imageRepository.findDefaultImageForCategory(category));
		}
	}
	
}
