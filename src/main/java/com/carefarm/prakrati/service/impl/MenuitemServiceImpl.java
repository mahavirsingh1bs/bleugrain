package com.carefarm.prakrati.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.common.util.Constants;
import com.carefarm.prakrati.comparator.DisplayIndexComparator;
import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.entity.Menuitem;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.repository.CategoryRepository;
import com.carefarm.prakrati.repository.MenuitemRepository;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.search.repository.MenuitemSearchRepository;
import com.carefarm.prakrati.service.MenuitemService;
import com.carefarm.prakrati.service.helper.MenuitemHelper;
import com.carefarm.prakrati.web.bean.CategoryBean;
import com.carefarm.prakrati.web.bean.MenuitemBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.MenuitemForm;

@Service
public class MenuitemServiceImpl extends PrakratiServiceImpl implements
		MenuitemService {

	private static final Logger LOGGER = LoggerFactory.getLogger(MenuitemServiceImpl.class);
	
	@Autowired
	private MenuitemRepository menuitemRepository;
	
	@Autowired
	private MenuitemSearchRepository menuitemSearchRepository;
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private MenuitemHelper menuitemHelper;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public Map<String, Object> findByKeyword(SearchRequest searchRequest) {
		Map<String, Object> data = new HashMap<>();
		List<MenuitemBean> menuitemBeans = new ArrayList<>();
		
		PageRequest pageRequest = createPageRequest(searchRequest);
		
		long totalRecord = 0;
		Iterable<Menuitem> menuitems = null;
		if (searchRequest.isMainMenuitems()) {
			String keyword = PERC + StringUtils.defaultIfEmpty(searchRequest.getKeyword(), Constants.BLANK) + PERC;
			totalRecord = menuitemRepository.countMainMenuitems(keyword);
			menuitems = menuitemRepository.findMainMenuitems(keyword, pageRequest);
		} else {
			SearchResult<Long> result = menuitemSearchRepository.findByKeywordContains(searchRequest, pageRequest);
			totalRecord = result.getTotalHits();
			menuitems = menuitemRepository.findAll(result.getResults());
		}
		
		for (Menuitem menuitem : menuitems) {
			MenuitemBean menuitemBean = mapper.map(menuitem, MenuitemBean.class);
			menuitemHelper.mapBasicFields(menuitem, menuitemBean);
			menuitemBeans.add(menuitemBean);
		}
		
		data.put("recordsTotal", totalRecord);
		data.put("recordsFiltered", totalRecord);
		data.put("data", menuitemBeans);
		return data;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public List<MenuitemBean> findMainMenuitems() {
		List<MenuitemBean> menuitems = new ArrayList<>();
		
		List<Menuitem> mainMenuitems = menuitemRepository.findMainMenuitems(Boolean.TRUE);
		Collections.sort(mainMenuitems, new DisplayIndexComparator());
		for (Menuitem menuitem : mainMenuitems) {
			MenuitemBean menuitemBean = mapper.map(menuitem, MenuitemBean.class);
			
			Collections.sort(menuitem.getChildMenuitems(), new DisplayIndexComparator());
			for (Menuitem childMenuitem : menuitem.getChildMenuitems()) {
				if (childMenuitem.isActive()) {
					createNode(menuitemBean, childMenuitem);
				}
			}
			menuitems.add(menuitemBean);
		}
		return menuitems;
	}

	public void createNode(MenuitemBean parentMenuitemBean, Menuitem menuitem) {
		MenuitemBean menuitemBean = mapper.map(menuitem, MenuitemBean.class);
		parentMenuitemBean.addChildMenuitem(menuitemBean);
		
		Collections.sort(menuitem.getChildMenuitems(), new DisplayIndexComparator());
		for (Menuitem childMenuitem : menuitem.getChildMenuitems()) {
			if (childMenuitem.isActive()) {
				createNode(menuitemBean, childMenuitem);
			}
		}
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(MenuitemForm form, User currentUser) throws ServiceException {
		Menuitem menuitem = mapper.map(form, Menuitem.class);
		menuitem.setUniqueId(UUID.randomUUID().toString());
		menuitem.setCategory(categoryRepository.findByUniqueId(form.getCategoryId()));
		menuitem.setParentMenuitem(menuitemRepository.findByUniqueId(form.getParentMenuitemId()));
		menuitem.setCreatedBy(currentUser);
		menuitem.setDateCreated(Date.from(Environment.clock().instant()));
		
		menuitemRepository.save(menuitem);
		menuitemHelper.publishIndexEvent(menuitem, EventType.INSERT);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public MenuitemBean findDetails(String uniqueId) {
		Menuitem menuitem = menuitemRepository.findByUniqueId(uniqueId);
		MenuitemBean menuitemBean = mapper.map(menuitem, MenuitemBean.class);
		menuitemBean.setParentMenuitemId(menuitem.getParentMenuitem().getUniqueId());
		menuitemBean.setParentMenuitem(mapper.map(menuitem.getParentMenuitem(), MenuitemBean.class));
		menuitemBean.setCategory(mapper.map(menuitem.getCategory(), CategoryBean.class));
		return menuitemBean;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(MenuitemBean menuitemBean, User modifiedBy)
			throws ServiceException {
		LOGGER.info("Updating menuitem details");
		Menuitem menuitem = menuitemRepository.findByUniqueId(menuitemBean.getUniqueId());
		menuitem.setName(menuitemBean.getName());
		menuitem.setParentMenuitem(menuitemRepository.findByUniqueId(menuitemBean.getParentMenuitemId()));
		menuitem.setCategory(categoryRepository.findByUniqueId(menuitemBean.getCategoryId()));
		menuitem.setKeywords(menuitemBean.getKeywords());
		menuitem.setDisplayIndex(menuitemBean.getDisplayIndex());
		menuitem.setActive(menuitemBean.isActive());
		menuitem.setModifiedBy(modifiedBy);
		menuitem.setDateModified(Calendar.getInstance().getTime());
		menuitemHelper.publishIndexEvent(menuitem, EventType.UPDATE);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(String uniqueId) {
		Menuitem menuitem = menuitemRepository.findByUniqueId(uniqueId);
		menuitem.setArchived(Boolean.TRUE);
		menuitemHelper.publishIndexEvent(menuitem, EventType.DELETE);
	}

}
