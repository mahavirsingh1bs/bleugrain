package com.carefarm.prakrati.service.impl;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.entity.Note;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.generators.UniqueIdBuilder;
import com.carefarm.prakrati.repository.NoteRepository;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.service.NoteService;
import com.carefarm.prakrati.web.model.NoteForm;

@Service
public class NoteServiceImpl extends PrakratiServiceImpl implements NoteService {

	@Autowired
	private NoteRepository noteRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void addNote(NoteForm noteForm, String userNo, User currentUser) {
		User user = userRepository.findByUserNo(userNo);
		
		Note note = mapper.map(noteForm, Note.class);
		note.setUniqueId(UniqueIdBuilder.generateUniqueId());
		note.setUser(user);
		note.setCreatedBy(currentUser);
		note.setDateCreated(Date.from(Environment.clock().instant()));
		
		noteRepository.save(note);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(String uniqueId, User currentUser) {
		Note note = noteRepository.findByUniqueId(uniqueId);
		note.setArchived(Boolean.TRUE);
		note.setModifiedBy(currentUser);
		note.setDateModified(Date.from(Environment.clock().instant()));
	}
	
}
