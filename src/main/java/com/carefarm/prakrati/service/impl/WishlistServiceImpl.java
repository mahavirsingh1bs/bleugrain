package com.carefarm.prakrati.service.impl;

import java.time.Clock;
import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.entity.Demand;
import com.carefarm.prakrati.entity.Product;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.entity.Wishlist;
import com.carefarm.prakrati.repository.DemandRepository;
import com.carefarm.prakrati.repository.ProductRepository;
import com.carefarm.prakrati.repository.WishlistRepository;
import com.carefarm.prakrati.service.WishlistService;
import com.carefarm.prakrati.web.bean.CompanyBean;
import com.carefarm.prakrati.web.bean.DemandBean;
import com.carefarm.prakrati.web.bean.ProductBean;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.bean.WishlistBean;

@Service
public class WishlistServiceImpl extends PrakratiServiceImpl implements WishlistService {
	
	@Autowired
	private WishlistRepository wishlistRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private DemandRepository demandRepository;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public WishlistBean findWishlist(User user) {
		Wishlist wishlist = wishlistRepository.findByUser(user);
		
		WishlistBean wishlistBean = null;
		if (Predicates.notNull.test(wishlist)) {
			wishlistBean = mapper.map(wishlist, WishlistBean.class);
			wishlistBean.setTotalItems(wishlist.totalItems());
			this.extractCommonProperties(wishlistBean, wishlist);
			return wishlistBean;
		} else {
			wishlistBean = new WishlistBean();
		}
		
		return wishlistBean;
	}

	private void extractCommonProperties(WishlistBean wishlistBean, Wishlist wishlist) {
		wishlistBean.setUser(mapper.map(wishlist.getUser(), UserBean.class));
		
		Optional.ofNullable(wishlist.getCompany())
			.ifPresent(company -> 
				wishlistBean.setCompany(
						mapper.map(company, CompanyBean.class)));
		
		wishlist.getProducts()
			.forEach(product -> 
				wishlistBean.addProduct(
					mapper.map(product, ProductBean.class)));
		
		wishlist.getDemands()
			.forEach(demand -> 
				wishlistBean.addDemand(
					mapper.map(demand, DemandBean.class)));
		
		Optional.ofNullable(wishlist.getCreatedBy())
			.ifPresent(createdBy -> 
				wishlistBean.setCreatedBy(
					mapper.map(createdBy, UserBean.class)));
		
		Optional.ofNullable(wishlist.getModifiedBy())
			.ifPresent(modifiedBy -> 
				wishlistBean.setModifiedBy(
					mapper.map(modifiedBy, UserBean.class)));
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void addProductToWishlist(String uniqueId, User currentUser) {
		Clock clock = Environment.clock();
		
		Wishlist wishlist = wishlistRepository.findByUser(currentUser);
		Product product = productRepository.findByUniqueId(uniqueId);
		if (Predicates.notNull.test(wishlist)) {
			wishlist.addProduct(product);
			wishlist.setModifiedBy(currentUser);
			wishlist.setDateModified(Date.from(clock.instant()));
		} else {
			wishlist = new Wishlist();
			setWishlistOf(wishlist, currentUser);
			wishlist.setMocked(Boolean.FALSE);
			wishlist.addProduct(product);
			wishlist.setCreatedBy(currentUser);
			wishlist.setDateCreated(Date.from(clock.instant()));
			wishlistRepository.save(wishlist);
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void removeProductFromWishlist(String uniqueId, User currentUser) {
		Wishlist wishlist = wishlistRepository.findByUser(currentUser);
		wishlist.removeProduct(uniqueId);
		wishlist.setModifiedBy(currentUser);
		wishlist.setDateModified(Date.from(Environment.clock().instant()));
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void addDemandToWishlist(String uniqueId, User currentUser) {
		Clock clock = Environment.clock();
		
		Wishlist wishlist = wishlistRepository.findByUser(currentUser);
		Demand demand = demandRepository.findByUniqueId(uniqueId);
		if (Predicates.notNull.test(wishlist)) {
			wishlist.addDemand(demand);
			wishlist.setModifiedBy(currentUser);
			wishlist.setDateModified(Date.from(clock.instant()));
		} else {
			wishlist = new Wishlist();
			setWishlistOf(wishlist, currentUser);
			wishlist.setMocked(Boolean.FALSE);
			wishlist.addDemand(demand);
			wishlist.setCreatedBy(currentUser);
			wishlist.setDateCreated(Date.from(clock.instant()));
			wishlistRepository.save(wishlist);
		}
	}

	private void setWishlistOf(Wishlist wishlist, User currentUser) {
		if (currentUser.isACompanyUser()) {
			wishlist.setCompany(currentUser.getCompany());
		}
		wishlist.setUser(currentUser);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void removeDemandFromWishlist(String uniqueId, User currentUser) {
		Wishlist wishlist = wishlistRepository.findByUser(currentUser);
		wishlist.removeDemand(uniqueId);
		wishlist.setModifiedBy(currentUser);
		wishlist.setDateModified(Date.from(Environment.clock().instant()));
	}

}
