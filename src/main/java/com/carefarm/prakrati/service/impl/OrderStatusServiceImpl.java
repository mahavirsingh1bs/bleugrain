package com.carefarm.prakrati.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.repository.OrderStatusRepository;
import com.carefarm.prakrati.service.OrderStatusService;
import com.carefarm.prakrati.web.bean.OrderStatusBean;

@Service
public class OrderStatusServiceImpl extends PrakratiServiceImpl implements OrderStatusService {
	
	@Autowired
	private OrderStatusRepository orderStatusRepository;
	
	@Override
	@Transactional
	public List<OrderStatusBean> findAll() {
		List<OrderStatusBean> orderStatuses = new ArrayList<>();
		orderStatusRepository.findAll()
			.forEach((os) -> orderStatuses.add(mapper.map(os, OrderStatusBean.class)));
		return orderStatuses;
	}

}
