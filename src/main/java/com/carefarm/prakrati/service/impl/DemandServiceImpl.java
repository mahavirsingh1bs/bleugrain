package com.carefarm.prakrati.service.impl;

import static com.carefarm.prakrati.web.util.PrakratiConstants.DEMAND_STATUS_OPEN;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.carefarm.prakrati.common.entity.Currency;
import com.carefarm.prakrati.common.entity.Unit;
import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.entity.Address;
import com.carefarm.prakrati.entity.Demand;
import com.carefarm.prakrati.entity.Quantity;
import com.carefarm.prakrati.entity.Tag;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.generators.UniqueIdBuilder;
import com.carefarm.prakrati.repository.CategoryRepository;
import com.carefarm.prakrati.repository.CompanyRepository;
import com.carefarm.prakrati.repository.CurrencyRepository;
import com.carefarm.prakrati.repository.DemandRepository;
import com.carefarm.prakrati.repository.DemandStatusRepository;
import com.carefarm.prakrati.repository.ImageRepository;
import com.carefarm.prakrati.repository.TagRepository;
import com.carefarm.prakrati.repository.UnitRepository;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.service.DemandService;
import com.carefarm.prakrati.service.helper.AmazonS3Helper;
import com.carefarm.prakrati.service.helper.DemandHelper;
import com.carefarm.prakrati.util.DemandStatus;
import com.carefarm.prakrati.util.ImageCategory;
import com.carefarm.prakrati.util.ImageType;
import com.carefarm.prakrati.web.bean.CategoryBean;
import com.carefarm.prakrati.web.bean.DemandBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.bean.TagBean;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.model.DemandAssignForm;
import com.carefarm.prakrati.web.model.DemandForm;
import com.carefarm.prakrati.web.model.PriceUpdateForm;

@Service
public class DemandServiceImpl extends PrakratiServiceImpl implements DemandService {

	private static final Logger LOGGER = LoggerFactory.getLogger(DemandServiceImpl.class);

	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private CompanyRepository companyRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private DemandRepository demandRepository;
	
	@Autowired
	private TagRepository tagRepository;
	
	@Autowired
	private DemandStatusRepository demandStatusRepository;

	@Autowired
	private CurrencyRepository currencyRepository;
	
	@Autowired
	private UnitRepository unitRepository;
	
	@Autowired
	private ImageRepository imageRepository;
	
	@Autowired
	private DemandHelper demandHelper;
	
	@Autowired
	private AmazonS3Helper amazonS3Helper;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Map<String, Object> findByKeyword(SearchRequest searchRequest) {
		Map<String, Object> data = new HashMap<>();
		
		Pageable pageable = createPageRequest(searchRequest);
		
		Long totalRecord = 0L;
		List<Demand> demands = null;
		if (Predicates.notNull.test(searchRequest.getCompanyId())) {
			totalRecord = demandRepository.countByCompanyIdAndStatusCodes(searchRequest.getCompanyId(), demandStatuses);
			demands = demandRepository.findByCompanyIdAndStatusCodes(searchRequest.getCompanyId(), demandStatuses, pageable);
		} else if (Predicates.notNull.test(searchRequest.getUserNo())) {
			totalRecord = demandRepository.countByUserNoAndStatusCodes(searchRequest.getUserNo(), demandStatuses);
			demands = demandRepository.findByUserNoAndStatusCodes(searchRequest.getUserNo(), demandStatuses, pageable);
		} else {
			totalRecord = demandRepository.countAllOpenOnes(DEMAND_STATUS_OPEN);
			demands = demandRepository.findAllOpenOnes(DEMAND_STATUS_OPEN, pageable);
		}
		List<DemandBean> demandBeans = new ArrayList<>();
		for (Demand demand : demands) {
			DemandBean demandBean = mapper.map(demand, DemandBean.class);
			demand.getTags().forEach(t -> demandBean.addTag(mapper.map(t, TagBean.class)));
			demandBeans.add(demandBean);
		}
		data.put("recordsTotal", totalRecord);
		data.put("recordsFiltered", totalRecord);
		data.put("data", demandBeans);
		return data;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(String uniqueId, User currentUser) {
		Demand demand = demandRepository.findByUniqueId(uniqueId);
		demand.setArchived(Boolean.TRUE);
		demand.setModifiedBy(currentUser);
		demand.setDateModified(Date.from(Environment.clock().instant()));
		demandHelper.publishIndexEvent(demand, EventType.DELETE);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(DemandForm form, List<MultipartFile> files, User currentUser) throws ServiceException {
		Calendar calendar = Calendar.getInstance();
		Demand demand = mapper.map(form, Demand.class);
		demand.setUniqueId(UniqueIdBuilder.generateUniqueId());
		demand.setCategory(categoryRepository.findByUniqueId(form.getCategoryId()));
		demand.setDeliveryDate(super.getParseDate(form.getDeliveryDate()));
		demand.setMocked(Boolean.FALSE);
		demand.setCreatedBy(currentUser);
		demand.setDateCreated(calendar.getTime());
		demand.setRequestDate(calendar.getTime());
		demand.setStatus(demandStatusRepository.findByCode(DemandStatus.OPEN.getCode()));
		demandHelper.setBuyerCategory(demand, currentUser);
		
		form.getTags().forEach(tagBean -> {
        	Tag tag = mapper.map(tagBean, Tag.class);
        	if (Predicates.isNull.test(tag.getId())) {
        		tag.setCreatedBy(currentUser);
        		tag.setDateCreated(Date.from(Environment.clock().instant()));
        		tagRepository.save(tag);
        	}
        	demand.addTag(tag);
        });
        
		if (Predicates.isNull.test(form.getUserId())) {
			demand.setUser(null);
			demand.setCompany(companyRepository.findByUniqueId(form.getCompanyId()));
		} else if (Predicates.isNull.test(form.getCompanyId())) {
			demand.setCompany(null);
			demand.setUser(userRepository.findByUserNo(form.getUserId()));
		}
		
		demandRepository.save(demand);
		
		if (files.size() > 0) {
			files.forEach(image -> { 
				demand.setDefaultImage(amazonS3Helper.handleImage(image, ImageType.DEMANDS, demand.getUniqueId()));
			});
		} else {
			demand.setDefaultImage(imageRepository.findDefaultImageForCategory(ImageCategory.DEMAND));
		}
		
		demandHelper.publishIndexEvent(demand, EventType.INSERT);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public DemandBean findDetail(String uniqueId) {
		Demand demand = demandRepository.findByUniqueId(uniqueId);
		DemandBean demandBean = mapper.map(demand, DemandBean.class);
		
		Optional.ofNullable(demand.getAssignedBy())
			.ifPresent(assignedBy -> 
				demandBean.setAssignedBy(mapper.map(assignedBy, UserBean.class)));
		
		Optional.ofNullable(demand.getAssignedTo())
			.ifPresent(assignedTo -> 
				demandBean.setAssignedTo(mapper.map(assignedTo, UserBean.class)));
	
		demandBean.setCategory(mapper.map(demand.getCategory(), CategoryBean.class));
		demand.getTags().forEach(t -> demandBean.addTag(mapper.map(t, TagBean.class)));
		return demandBean;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(DemandBean demandBean, User currentUser)
			throws ServiceException {
		
		Quantity quantity = new Quantity();
		quantity.setQuantity(NumberUtils.createBigDecimal(demandBean.getQuantity().replace(",", "")).doubleValue());
		quantity.setUnit(mapper.map(demandBean.getQuantityUnit(), Unit.class));
		
		Calendar calendar = Calendar.getInstance();
		Demand demand = demandRepository.findByUniqueId(demandBean.getUniqueId());
		demand.setQuantity(quantity);
		demand.setPricePerUnit(demandBean.getPrice().doubleValue());
		demand.setCurrency(mapper.map(demandBean.getPriceCurrency(), Currency.class));
		demand.setUnit(mapper.map(demandBean.getPriceUnit(), Unit.class));
		demand.setCategory(categoryRepository.findByUniqueId(demandBean.getCategoryId()));
		
		try {
			demand.setDeliveryDate(DATE_FORMAT.parse(demandBean.getDeliveryDate()));
		} catch (ParseException e) {
			LOGGER.error("Got error while parsing delivery date");
		}
		
		Set<Tag> tags = new HashSet<>();
		demandBean.getTags().forEach(tagBean -> {
        	Tag tag = mapper.map(tagBean, Tag.class);
        	if (Predicates.isNull.test(tag.getId())) {
        		tag.setCreatedBy(currentUser);
        		tag.setDateCreated(Date.from(Environment.clock().instant()));
        		tagRepository.save(tag);
        	}
        	tags.add(tag);
        });
        demand.setTags(tags);
		
		Optional.ofNullable(demandBean.getDeliveryAddress())
			.ifPresent(address -> demand.setDeliveryAddress(mapper.map(address, Address.class)));
		Optional.ofNullable(demandBean.getCompany())
			.ifPresent(company -> demand.setCompany(companyRepository.findByUniqueId(company.getUniqueId())));
		Optional.ofNullable(demandBean.getUser())
			.ifPresent(user -> demand.setUser(userRepository.findByUserNo(user.getUserNo())));
	
		demand.setModifiedBy(currentUser);
		demand.setDateModified(calendar.getTime());
		demandHelper.publishIndexEvent(demand, EventType.UPDATE);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void assign(DemandAssignForm form, User currentUser) {
		Demand demand = demandRepository.findByUniqueId(form.getUniqueId());
		demand.setAssignedBy(currentUser);
		demand.setAssignedTo(userRepository.findByUserNo(form.getAssignTo().getUserNo()));
		demand.setDateAssigned(Date.from(Environment.clock().instant()));
		demand.setComments(form.getComments());
		demand.setModifiedBy(currentUser);
		demand.setDateModified(Date.from(Environment.clock().instant()));
		
		demandHelper.createAndSendMessages(demand, DemandStatus.ASSIGNED.getCode());
		demandHelper.publishIndexEvent(demand, EventType.UPDATE);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void markAsFulfilled(String uniqueId, User currentUser) {
		Demand demand = demandRepository.findByUniqueId(uniqueId);
		demand.setStatus(demandStatusRepository.findByCode(DemandStatus.FULFILLED.getCode()));
		demand.setModifiedBy(currentUser);
		demand.setDateModified(Date.from(Environment.clock().instant()));
		
		demandHelper.createAndSendMessages(demand, DemandStatus.FULFILLED.getCode());
		demandHelper.publishIndexEvent(demand, EventType.UPDATE);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updatePrice(PriceUpdateForm form, User currentUser) {
		Demand demand = demandRepository.findByUniqueId(form.getUniqueId());
		demand.setPricePerUnit(form.getPrice());
		demand.setCurrency(currencyRepository.findOne(form.getPriceCurrencyId()));
		demand.setUnit(unitRepository.findOne(form.getPriceUnitId()));
		demand.setModifiedBy(currentUser);
		demand.setDateModified(Date.from(Environment.clock().instant()));
		demandHelper.publishIndexEvent(demand, EventType.UPDATE);
	}
}
