package com.carefarm.prakrati.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.common.entity.State;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.repository.CountryRepository;
import com.carefarm.prakrati.repository.StateRepository;
import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.search.repository.StateSearchRepository;
import com.carefarm.prakrati.service.StateService;
import com.carefarm.prakrati.service.helper.StateHelper;
import com.carefarm.prakrati.web.bean.CountryBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.bean.StateBean;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.model.StateForm;

@Service
public class StateServiceImpl extends PrakratiServiceImpl implements StateService {

	private static final Logger LOGGER = LoggerFactory.getLogger(StateServiceImpl.class);

	@Autowired
	private CountryRepository countryRepository;
	
	@Autowired
	private StateRepository stateRepository;
	
	@Autowired
	private StateSearchRepository stateSearchRepository;
	
	@Autowired
	private StateHelper stateHelper;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<StateBean> findAll() {
		List<StateBean> states = new ArrayList<>();
		for (State state : stateRepository.findAll()) {
			StateBean stateBean = mapper.map(state, StateBean.class);
			states.add(stateBean);
		}
		return states;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Map<String, Object> findByKeyword(SearchRequest searchRequest) {
		Map<String, Object> data = new HashMap<>();
		List<StateBean> stateBeans = new ArrayList<>();
		
		PageRequest pageRequest = createPageRequest(searchRequest);
		
		SearchResult<Long> result = stateSearchRepository.findByKeywordContains(searchRequest, pageRequest);
		long totalRecord = result.getTotalHits();
		Iterable<State> states = stateRepository.findAll(result.getResults());
		
		for (State state : states) {
			StateBean stateBean = mapper.map(state, StateBean.class);
			this.mapBasicFields(state, stateBean);
			stateBeans.add(stateBean);
		}
		
		data.put("recordsTotal", totalRecord);
		data.put("recordsFiltered", totalRecord);
		data.put("data", stateBeans);
		return data;
	}

	private void mapBasicFields(State state, StateBean stateBean) {
		stateBean.setCountry(mapper.map(state.getCountry(), CountryBean.class));
		if (state.getCreatedBy() != null) {
			stateBean.setCreatedBy(mapper.map(state.getCreatedBy(), UserBean.class));
		}
		
		if (state.getModifiedBy() != null) {
			stateBean.setModifiedBy(mapper.map(state.getModifiedBy(), UserBean.class));
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(StateForm form, User createdBy) throws ServiceException {
		State state = mapper.map(form, State.class);
		state.setCreatedBy(createdBy);
		state.setDateCreated(Calendar.getInstance().getTime());
		stateRepository.save(state);
		stateHelper.publishIndexEvent(state, EventType.INSERT);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public StateBean findDetails(long stateId, Object object) {
		State state = stateRepository.findOne(stateId);
		StateBean stateBean = mapper.map(state, StateBean.class);
		this.mapBasicFields(state, stateBean);
		return stateBean;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(StateBean stateBean, User modifiedBy) throws ServiceException {
		LOGGER.info("Updating state details");
		State state = stateRepository.findOne(stateBean.getId());
		stateHelper.convertBeanToEntity(state, stateBean);
		state.setCountry(countryRepository.findOne(stateBean.getCountryId()));
		state.setModifiedBy(modifiedBy);
		state.setDateModified(Calendar.getInstance().getTime());
		stateHelper.publishIndexEvent(state, EventType.UPDATE);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(String uniqueId) {
		State state = stateRepository.findOne(NumberUtils.toLong(uniqueId));
		state.setArchived(Boolean.TRUE);
		stateHelper.publishIndexEvent(state, EventType.DELETE);
	}
}
