package com.carefarm.prakrati.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.entity.Department;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.repository.DepartmentRepository;
import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.search.repository.DepartmentSearchRepository;
import com.carefarm.prakrati.service.DepartmentService;
import com.carefarm.prakrati.service.helper.DepartmentHelper;
import com.carefarm.prakrati.web.bean.DepartmentBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.model.DepartmentForm;

@Service
public class DepartmentServiceImpl extends PrakratiServiceImpl implements DepartmentService {

	private static final Logger LOGGER = LoggerFactory.getLogger(DepartmentServiceImpl.class);
	
	@Autowired
	private DepartmentRepository departmentRepository;

	@Autowired
	private DepartmentSearchRepository departmentSearchRepository;
	
	@Autowired
	private DepartmentHelper departmentHelper;
	
	@Override
	public List<DepartmentBean> findAll() {
		List<DepartmentBean> departments = new ArrayList<>();
		for (Department department : departmentRepository.findAll()) {
			DepartmentBean departmentBean = mapper.map(department, DepartmentBean.class);
			departments.add(departmentBean);
		}
		return departments;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Map<String, Object> findByKeyword(SearchRequest searchRequest) {
		Map<String, Object> data = new HashMap<>();
		List<DepartmentBean> departmentBeans = new ArrayList<>();
		
		PageRequest pageRequest = null;
		
		if (searchRequest.getSortFields() != null) {
			String[] sortFields = searchRequest.getSortFields().split(",");
			Direction direction = searchRequest.getDirection();
			
			Sort sort = new Sort(direction, sortFields[0]);
			for (int index = 1; index < sortFields.length; index++) { 
				sort = sort.and(new Sort(direction, sortFields[index]));
			}
			pageRequest = new PageRequest(searchRequest.getPage(), searchRequest.getPageSize(), sort);
		} else {
			pageRequest = new PageRequest(searchRequest.getPage(), searchRequest.getPageSize());
		}
		
		SearchResult<Long> result = departmentSearchRepository.findByKeywordContains(searchRequest, pageRequest);
		long totalRecord = result.getTotalHits();
		Iterable<Department> departments = departmentRepository.findAll(result.getResults());
		
		for (Department department: departments) {
			DepartmentBean departmentBean = mapper.map(department, DepartmentBean.class);
			if (department.getCreatedBy() != null) {
				UserBean createdBy = mapper.map(department.getCreatedBy(), UserBean.class);
				departmentBean.setCreatedBy(createdBy);
			}
			
			if (department.getModifiedBy() != null) {
				UserBean modifiedBy = mapper.map(department.getModifiedBy(), UserBean.class);
				departmentBean.setModifiedBy(modifiedBy);
			}
			
			departmentBeans.add(departmentBean);
		}
		
		data.put("recordsTotal", totalRecord);
		data.put("recordsFiltered", totalRecord);
		data.put("data", departmentBeans);
		return data;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(DepartmentForm form, User createdBy) throws ServiceException {
		Department department = mapper.map(form, Department.class);
		department.setCreatedBy(createdBy);
		department.setDateCreated(Calendar.getInstance().getTime());
		departmentRepository.save(department);
		departmentHelper.publishIndexEvent(department, EventType.INSERT);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public DepartmentBean findDetails(long departmentId, Object object) {
		Department department = departmentRepository.findOne(departmentId);
		DepartmentBean departmentBean = mapper.map(department, DepartmentBean.class);
		
		if (department.getCreatedBy() != null) {
			departmentBean.setCreatedBy(mapper.map(department.getCreatedBy(), UserBean.class));
		}
		
		if (department.getModifiedBy() != null) {
			departmentBean.setModifiedBy(mapper.map(department.getModifiedBy(), UserBean.class));
		}
		return departmentBean;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(DepartmentBean departmentBean, User modifiedBy) throws ServiceException {
		LOGGER.info("Updating department details");
		Department department = departmentRepository.findOne(departmentBean.getId());
		departmentHelper.convertBeanToEntity(department, departmentBean);
		department.setModifiedBy(modifiedBy);
		department.setDateModified(Calendar.getInstance().getTime());
		departmentHelper.publishIndexEvent(department, EventType.UPDATE);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(String uniqueId) {
		Department department = departmentRepository.findOne(NumberUtils.toLong(uniqueId));
		department.setArchived(Boolean.TRUE);
		departmentHelper.publishIndexEvent(department, EventType.DELETE);
	}

}
