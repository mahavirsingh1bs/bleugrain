package com.carefarm.prakrati.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.entity.Bid;
import com.carefarm.prakrati.entity.Product;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.repository.BidRepository;
import com.carefarm.prakrati.repository.ProductRepository;
import com.carefarm.prakrati.service.BidService;
import com.carefarm.prakrati.service.helper.BidHelper;
import com.carefarm.prakrati.web.bean.BidBean;
import com.carefarm.prakrati.web.bean.BidsRequest;
import com.carefarm.prakrati.web.bean.CompanyBean;
import com.carefarm.prakrati.web.bean.CurrencyBean;
import com.carefarm.prakrati.web.bean.ProductBean;
import com.carefarm.prakrati.web.bean.UnitBean;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.model.BidForm;

@Service
public class BidServiceImpl extends PrakratiServiceImpl implements BidService {

	@Autowired
	private BidRepository bidRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private BidHelper bidHelper;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public List<BidBean> findByUniqueId(BidsRequest bidsRequest) {
		List<BidBean> bids = new ArrayList<>();
		bidRepository.findByUniqueId(bidsRequest.getUniqueId(), 
				new PageRequest(bidsRequest.getPage(), bidsRequest.getPageSize()))
			.forEach(b -> { 
				BidBean bid = mapper.map(b, BidBean.class);
				bid.setCurrency(mapper.map(b.getCurrency(), CurrencyBean.class));
				bid.setUnit(mapper.map(b.getUnit(), UnitBean.class));
				Optional.ofNullable(b.getUser())
					.ifPresent(u -> bid.setUser(mapper.map(u, UserBean.class)));
				Optional.ofNullable(b.getCompany())
					.ifPresent(c -> bid.setCompany(mapper.map(c, CompanyBean.class)));
				bids.add(bid); 
			});
		
		return bids;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public List<BidBean> findByUser(BidsRequest bidsRequest) {
		Pageable pageable = new PageRequest(bidsRequest.getPage(), bidsRequest.getPageSize());
		List<BidBean> bidBeans = new ArrayList<>();
		List<Product> products = productRepository.findProductsBiddedByUserAndCategory(bidsRequest.getUserNo(), bidsRequest.getUniqueId(), pageable);
		products.forEach(product -> {
			Optional<Bid> optionalBid = product.highestBidOfUser(bidsRequest.getUserNo());
			optionalBid.ifPresent(bid -> {
				BidBean bidBean = mapper.map(bid, BidBean.class);
				ProductBean productBean = mapper.map(product, ProductBean.class);
				productBean.setQuantityUnit(mapper.map(product.getQuantity().getUnit(), UnitBean.class));
				bidBean.setProduct(productBean);
				bidBean.setCurrency(mapper.map(bid.getCurrency(), CurrencyBean.class));
				bidBean.setUnit(mapper.map(bid.getUnit(), UnitBean.class));
				bidBeans.add(bidBean);
			});
		});
		return bidBeans;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public List<BidBean> findByCompany(BidsRequest bidsRequest) {
		Pageable pageable = new PageRequest(bidsRequest.getPage(), bidsRequest.getPageSize());
		List<BidBean> bidBeans = new ArrayList<>();
		List<Product> products = productRepository.findProductsBiddedByCompanyAndCategory(bidsRequest.getCompanyId(), bidsRequest.getUniqueId(), pageable);
		products.forEach(product -> {
			Optional<Bid> optionalBid = product.highestBidOfCompany(bidsRequest.getCompanyId());
			optionalBid.ifPresent(bid -> {
				BidBean bidBean = mapper.map(bid, BidBean.class);
				ProductBean productBean = mapper.map(product, ProductBean.class);
				productBean.setQuantityUnit(mapper.map(product.getQuantity().getUnit(), UnitBean.class));
				bidBean.setProduct(productBean);
				bidBean.setCurrency(mapper.map(bid.getCurrency(), CurrencyBean.class));
				bidBean.setUnit(mapper.map(bid.getUnit(), UnitBean.class));
				bidBean.setCreatedBy(mapper.map(bid.getCreatedBy(), UserBean.class));
				bidBeans.add(bidBean);
			});
		});
		return bidBeans;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void place(BidForm form, User currentUser) {
		Bid latestBid = new Bid();
		
		Product product = productRepository.findByUniqueId(form.getUniqueId());
		product.setHighestBid(latestBid);
		
		latestBid.setPrice(form.getPrice());
		latestBid.setCurrency(product.getBiddingCurrency());
		latestBid.setUnit(product.getBiddingUnit());
		latestBid.setProduct(product);
		setBidOf(latestBid, currentUser);
		latestBid.setCreatedBy(currentUser);
		latestBid.setDateCreated(Date.from(Environment.clock().instant()));
		bidRepository.save(latestBid);
		
		bidHelper.sendNotification(latestBid);
	}

	public void setBidOf(Bid bid, User currentUser) {
		if (currentUser.isACompanyUser()) {
			bid.setCompany(currentUser.getCompany());
		}
		bid.setUser(currentUser);
	}
}
