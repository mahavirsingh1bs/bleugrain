package com.carefarm.prakrati.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.entity.PaymentMethod;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.repository.PaymentMethodRepository;
import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.repository.PaymentMethodSearchRepository;
import com.carefarm.prakrati.service.PaymentMethodService;
import com.carefarm.prakrati.service.helper.PaymentMethodHelper;
import com.carefarm.prakrati.web.bean.PaymentMethodBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.model.PaymentMethodForm;

@Service
public class PaymentMethodServiceImpl extends PrakratiServiceImpl implements PaymentMethodService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentMethodServiceImpl.class);
	
	@Autowired
	private PaymentMethodRepository paymentMethodRepository;
	
	@Autowired
	private PaymentMethodSearchRepository paymentMethodSearchRepository;

	@Autowired
	private PaymentMethodHelper paymentMethodHelper;
	
	@Override
	public List<PaymentMethodBean> findAll() {
		List<PaymentMethodBean> paymentMethods = new ArrayList<>();
		for (PaymentMethod paymentMethod : paymentMethodRepository.findAll()) {
			paymentMethods.add(mapper.map(paymentMethod, PaymentMethodBean.class));
		}
		return paymentMethods;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Map<String, Object> findByKeyword(SearchRequest searchRequest) {
		Map<String, Object> data = new HashMap<>();
		List<PaymentMethodBean> paymentMethodBeans = new ArrayList<>();
		
		PageRequest pageRequest = createPageRequest(searchRequest);
		
		SearchResult<Long> result = paymentMethodSearchRepository.findByKeywordContains(searchRequest, pageRequest);
		long totalRecord = result.getTotalHits();
		Iterable<PaymentMethod> paymentMethods = paymentMethodRepository.findAll(result.getResults());
		
		for (PaymentMethod paymentMethod : paymentMethods) {
			PaymentMethodBean paymentMethodBean = mapper.map(paymentMethod, PaymentMethodBean.class);
			if (paymentMethod.getCreatedBy() != null) {
				UserBean createdBy = mapper.map(paymentMethod.getCreatedBy(), UserBean.class);
				paymentMethodBean.setCreatedBy(createdBy);
			}
			
			if (paymentMethod.getModifiedBy() != null) {
				UserBean modifiedBy = mapper.map(paymentMethod.getModifiedBy(), UserBean.class);
				paymentMethodBean.setModifiedBy(modifiedBy);
			}
			
			paymentMethodBeans.add(paymentMethodBean);
		}
		
		data.put("recordsTotal", totalRecord);
		data.put("recordsFiltered", totalRecord);
		data.put("data", paymentMethodBeans);
		return data;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(PaymentMethodForm form, User createdBy) throws ServiceException {
		PaymentMethod paymentMethod = mapper.map(form, PaymentMethod.class);
		paymentMethod.setCreatedBy(createdBy);
		paymentMethod.setDateCreated(Calendar.getInstance().getTime());
		paymentMethodRepository.save(paymentMethod);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public PaymentMethodBean findDetails(long paymentMethodId, Object object) {
		PaymentMethod paymentMethod = paymentMethodRepository.findOne(paymentMethodId);
		PaymentMethodBean paymentMethodBean = mapper.map(paymentMethod, PaymentMethodBean.class);
		
		if (paymentMethod.getCreatedBy() != null) {
			paymentMethodBean.setCreatedBy(mapper.map(paymentMethod.getCreatedBy(), UserBean.class));
		}
		
		if (paymentMethod.getModifiedBy() != null) {
			paymentMethodBean.setModifiedBy(mapper.map(paymentMethod.getModifiedBy(), UserBean.class));
		}
		return paymentMethodBean;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(PaymentMethodBean paymentMethodBean, User modifiedBy) throws ServiceException {
		LOGGER.info("Updating paymentMethod details");
		PaymentMethod paymentMethod = paymentMethodRepository.findOne(paymentMethodBean.getId());
		paymentMethodHelper.convertBeanToEntity(paymentMethod, paymentMethodBean);
		paymentMethod.setModifiedBy(modifiedBy);
		paymentMethod.setDateModified(Calendar.getInstance().getTime());
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(String uniqueId) {
		PaymentMethod paymentMethod = paymentMethodRepository.findOne(NumberUtils.toLong(uniqueId));
		paymentMethod.setArchived(Boolean.TRUE);
	}

}
