package com.carefarm.prakrati.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carefarm.prakrati.entity.Language;
import com.carefarm.prakrati.repository.LanguageRepository;
import com.carefarm.prakrati.service.LanguageService;
import com.carefarm.prakrati.web.bean.LanguageBean;

@Service
public class LanguageServiceImpl extends PrakratiServiceImpl implements LanguageService {

	@Autowired
	private LanguageRepository languageRepository;
	
	@Override
	public List<LanguageBean> findByMatchingKeywords(String keywords) {
		List<Language> matchingLanguages = languageRepository.findByMatchingName("%" + keywords + "%");
		List<LanguageBean> matchingLanguageBeans = new ArrayList<>();
		matchingLanguages.forEach(user -> matchingLanguageBeans.add(mapper.map(user, LanguageBean.class)));
		return matchingLanguageBeans;
	}

}
