package com.carefarm.prakrati.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.entity.Bank;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.repository.BankRepository;
import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.search.repository.BankSearchRepository;
import com.carefarm.prakrati.service.BankService;
import com.carefarm.prakrati.service.helper.BankHelper;
import com.carefarm.prakrati.web.bean.BankBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.model.BankForm;

@Service
public class BankServiceImpl extends PrakratiServiceImpl implements BankService {

	private static final Logger LOGGER = LoggerFactory.getLogger(BankServiceImpl.class);
	
	@Autowired
	private BankRepository bankRepository;
	
	@Autowired
	private BankSearchRepository bankSearchRepository;
	
	@Autowired
	private BankHelper bankHelper;
	
	@Override
	public List<BankBean> findAll() {
		return bankHelper.convertEntitiesToBeans(bankRepository.findAll(), BankBean.class);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Map<String, Object> findByKeyword(SearchRequest searchRequest) {
		Map<String, Object> data = new HashMap<>();
		PageRequest pageRequest = createPageRequest(searchRequest);

		SearchResult<Long> result = bankSearchRepository.findByKeywordContains(searchRequest, pageRequest);
		long totalRecord = result.getTotalHits();
		Iterable<Bank> banks = bankRepository.findAll(result.getResults());
		
		List<BankBean> bankBeans = new ArrayList<>();
		for (Bank bank : banks) {
			BankBean bankBean = mapper.map(bank, BankBean.class);
			if (Predicates.notNull.test(bank.getCreatedBy())) {
				UserBean createdBy = mapper.map(bank.getCreatedBy(), UserBean.class);
				bankBean.setCreatedBy(createdBy);
			}
			
			if (Predicates.notNull.test(bank.getModifiedBy())) {
				UserBean modifiedBy = mapper.map(bank.getModifiedBy(), UserBean.class);
				bankBean.setModifiedBy(modifiedBy);
			}
			
			bankBeans.add(bankBean);
		}
		
		data.put("recordsTotal", totalRecord);
		data.put("recordsFiltered", totalRecord);
		data.put("data", bankBeans);
		return data;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(BankForm form, User createdBy) throws ServiceException {
		Bank bank = mapper.map(form, Bank.class);
		bank.setCreatedBy(createdBy);
		bank.setDateCreated(Calendar.getInstance().getTime());
		bankRepository.save(bank);
		bankHelper.publishIndexEvent(bank, EventType.INSERT);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public BankBean findDetails(long bankId, Object object) {
		Bank bank = bankRepository.findOne(bankId);
		BankBean bankBean = mapper.map(bank, BankBean.class);
		
		if (bank.getCreatedBy() != null) {
			bankBean.setCreatedBy(mapper.map(bank.getCreatedBy(), UserBean.class));
		}
		
		if (bank.getModifiedBy() != null) {
			bankBean.setModifiedBy(mapper.map(bank.getModifiedBy(), UserBean.class));
		}
		return bankBean;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(BankBean bankBean, User modifiedBy) throws ServiceException {
		LOGGER.info("Updating bank details");
		Bank bank = bankRepository.findOne(bankBean.getId());
		bankHelper.convertBeanToEntity(bank, bankBean);
		bank.setModifiedBy(modifiedBy);
		bank.setDateModified(Calendar.getInstance().getTime());
		bankHelper.publishIndexEvent(bank, EventType.UPDATE);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(String uniqueId) {
		Bank bank = bankRepository.findOne(NumberUtils.toLong(uniqueId));
		bank.setArchived(Boolean.TRUE);
		bankHelper.publishIndexEvent(bank, EventType.DELETE);
	}

}
