package com.carefarm.prakrati.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.entity.PaymentCard;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.repository.PaymentCardRepository;
import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.repository.PaymentCardSearchRepository;
import com.carefarm.prakrati.service.PaymentCardService;
import com.carefarm.prakrati.service.helper.PaymentCardHelper;
import com.carefarm.prakrati.web.bean.PaymentCardBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.model.PaymentCardForm;

@Service
public class PaymentCardServiceImpl extends PrakratiServiceImpl implements PaymentCardService {

	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentCardServiceImpl.class);
	
	@Autowired
	private PaymentCardRepository paymentCardRepository;

	@Autowired
	private PaymentCardSearchRepository paymentCardSearchRepository;
	
	@Autowired
	private PaymentCardHelper paymentCardHelper;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<PaymentCardBean> findAll() {
		List<PaymentCardBean> validCards = new ArrayList<>();
		for (PaymentCard paymentCard : paymentCardRepository.findAll()) {
			PaymentCardBean paymentCardBean = mapper.map(paymentCard, PaymentCardBean.class);
			validCards.add(paymentCardBean);
		}
		return validCards;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Map<String, Object> findByKeyword(SearchRequest searchRequest) {
		Map<String, Object> data = new HashMap<>();
		List<PaymentCardBean> paymentCardBeans = new ArrayList<>();
		
		PageRequest pageRequest = createPageRequest(searchRequest);
		
		SearchResult<Long> result = paymentCardSearchRepository.findByKeywordContains(searchRequest, pageRequest);
		long totalRecord = result.getTotalHits();
		Iterable<PaymentCard> paymentCards = paymentCardRepository.findAll(result.getResults());
		
		for (PaymentCard paymentCard : paymentCards) {
			PaymentCardBean paymentCardBean = mapper.map(paymentCard, PaymentCardBean.class);
			if (paymentCard.getCreatedBy() != null) {
				UserBean createdBy = mapper.map(paymentCard.getCreatedBy(), UserBean.class);
				paymentCardBean.setCreatedBy(createdBy);
			}
			
			if (paymentCard.getModifiedBy() != null) {
				UserBean modifiedBy = mapper.map(paymentCard.getModifiedBy(), UserBean.class);
				paymentCardBean.setModifiedBy(modifiedBy);
			}
			
			paymentCardBeans.add(paymentCardBean);
		}
		
		data.put("recordsTotal", totalRecord);
		data.put("recordsFiltered", totalRecord);
		data.put("data", paymentCardBeans);
		return data;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(PaymentCardForm form, User createdBy) throws ServiceException {
		PaymentCard paymentCard = mapper.map(form, PaymentCard.class);
		paymentCard.setCreatedBy(createdBy);
		paymentCard.setDateCreated(Calendar.getInstance().getTime());
		paymentCardRepository.save(paymentCard);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public PaymentCardBean findDetails(long paymentCardId, Object object) {
		PaymentCard paymentCard = paymentCardRepository.findOne(paymentCardId);
		PaymentCardBean paymentCardBean = mapper.map(paymentCard, PaymentCardBean.class);
		
		if (paymentCard.getCreatedBy() != null) {
			paymentCardBean.setCreatedBy(mapper.map(paymentCard.getCreatedBy(), UserBean.class));
		}
		
		if (paymentCard.getModifiedBy() != null) {
			paymentCardBean.setModifiedBy(mapper.map(paymentCard.getModifiedBy(), UserBean.class));
		}
		return paymentCardBean;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(PaymentCardBean paymentCardBean, User modifiedBy) throws ServiceException {
		LOGGER.info("Updating paymentCard details");
		PaymentCard paymentCard = paymentCardRepository.findOne(paymentCardBean.getId());
		paymentCardHelper.convertBeanToEntity(paymentCard, paymentCardBean);
		paymentCard.setModifiedBy(modifiedBy);
		paymentCard.setDateModified(Calendar.getInstance().getTime());
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(String uniqueId) {
		PaymentCard paymentCard = paymentCardRepository.findOne(NumberUtils.toLong(uniqueId));
		paymentCard.setArchived(Boolean.TRUE);
	}
}
