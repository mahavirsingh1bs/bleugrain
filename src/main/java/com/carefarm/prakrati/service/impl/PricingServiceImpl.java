package com.carefarm.prakrati.service.impl;

import static com.carefarm.prakrati.util.DemandStatus.ASSIGNED;
import static com.carefarm.prakrati.util.DemandStatus.OPEN;
import static com.carefarm.prakrati.util.DemandStatus.TENTATIVE;
import static com.carefarm.prakrati.util.ProductState.ADDED_NEW;
import static com.carefarm.prakrati.util.ProductState.AVAILABLE;
import static com.carefarm.prakrati.util.ProductState.ONCE_PRICED;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.entity.Demand;
import com.carefarm.prakrati.entity.Product;
import com.carefarm.prakrati.repository.DemandRepository;
import com.carefarm.prakrati.repository.ProductRepository;
import com.carefarm.prakrati.service.PricingService;
import com.carefarm.prakrati.web.bean.DemandBean;
import com.carefarm.prakrati.web.bean.ProductBean;
import com.carefarm.prakrati.web.bean.SearchRequest;

@Service
public class PricingServiceImpl extends PrakratiServiceImpl implements PricingService {

	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private DemandRepository demandRepository;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public Map<String, Object> findProducts(SearchRequest request) {
		Pageable pageable = createPageRequest(request);
		
		String[] statusCodes = { ADDED_NEW.getCode(), ONCE_PRICED.getCode(), AVAILABLE.getCode() };
		
		List<ProductBean> productBeans = new ArrayList<>();
		
		Long totalRecord = null;
		List<Product> products = null;
		if (Predicates.notNull.test(request.getKeyword())) {
			totalRecord = productRepository.countByKeywordsAndCategoryIdAndTypeAndStatusCodes("%" + request.getKeyword() + "%", request.getCategoryId(), request.isConsumerProducts(), statusCodes);
			products = productRepository.findByKeywordsAndCategoryIdAndTypeAndStatusCodes("%" + request.getKeyword() + "%", request.getCategoryId(), request.isConsumerProducts(), statusCodes, pageable);
		} else {
			totalRecord = productRepository.countByCategoryIdAndTypeAndStatusCodes(request.getCategoryId(), request.isConsumerProducts(), statusCodes);
			products = productRepository.findByCategoryIdAndTypeAndStatusCodes(request.getCategoryId(), request.isConsumerProducts(), statusCodes, pageable);
		}
				
		for (Product product: products) {
			productBeans.add(mapper.map(product, ProductBean.class));
		}
		
		Map<String, Object> data = new HashMap<>();
		data.put("recordsTotal", totalRecord);
		data.put("recordsFiltered", totalRecord);
		data.put("data", productBeans);
		return data;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public Map<String, Object> findDemands(SearchRequest request) {
		Pageable pageable = createPageRequest(request);
		
		String[] statusCodes = { OPEN.getCode(), TENTATIVE.getCode(), ASSIGNED.getCode() };
		
		List<DemandBean> demandBeans = new ArrayList<>();
		
		Long totalRecord = null;
		List<Demand> demands = null;
		if (Predicates.notNull.test(request.getKeyword())) {
			totalRecord = demandRepository.countByKeywordsAndCategoryIdAndStatusCodes("%" + request.getKeyword() + "%", request.getCategoryId(), statusCodes);
			demands = demandRepository.findByKeywordsAndCategoryIdAndStatusCodes("%" + request.getKeyword() + "%", request.getCategoryId(), statusCodes, pageable);
		} else {
			totalRecord = demandRepository.countByCategoryIdAndStatusCodes(request.getCategoryId(), statusCodes);
			demands = demandRepository.findByCategoryIdAndStatusCodes(request.getCategoryId(), statusCodes, pageable);
		}
				
		for (Demand demand: demands) {
			demandBeans.add(mapper.map(demand, DemandBean.class));
		}
		
		Map<String, Object> data = new HashMap<>();
		data.put("recordsTotal", totalRecord);
		data.put("recordsFiltered", totalRecord);
		data.put("data", demandBeans);
		return data;
	}
	
}
