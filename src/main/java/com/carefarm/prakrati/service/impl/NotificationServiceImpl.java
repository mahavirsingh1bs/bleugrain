package com.carefarm.prakrati.service.impl;

import static com.carefarm.prakrati.common.util.Constants.NOTIFICATION;

import java.sql.Date;
import java.time.Clock;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.entity.Notification;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.events.SocketEvent;
import com.carefarm.prakrati.repository.CompanyRepository;
import com.carefarm.prakrati.repository.GroupRepository;
import com.carefarm.prakrati.repository.NotificationCategoryRepository;
import com.carefarm.prakrati.repository.NotificationRepository;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.service.NotificationService;
import com.carefarm.prakrati.service.dto.NotificationDTO;
import com.carefarm.prakrati.service.helper.NotificationHelper;
import com.carefarm.prakrati.web.bean.CompanyBean;
import com.carefarm.prakrati.web.bean.GroupBean;
import com.carefarm.prakrati.web.bean.NotificationBean;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.model.NotificationForm;

@Service
public class NotificationServiceImpl extends PrakratiServiceImpl implements NotificationService {

	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationServiceImpl.class);
	
	@Autowired
	private SimpMessagingTemplate template;
	
	@Autowired
	private GroupRepository groupRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private CompanyRepository companyRepository;
	
	@Autowired
	private NotificationRepository notificationRepository;
	
	@Autowired
	private NotificationCategoryRepository categoryRepository;
	
	@Autowired
	private NotificationHelper notificationHelper;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void sendNotification(NotificationForm form, String userNo, User currentUser) {
		Clock clock = Environment.clock();
		
		Notification notification = mapper.map(form, Notification.class);
		notification.setCategory(categoryRepository.findOne(form.getCategoryId()));
		
		form.getGroups().forEach(group -> notification.addGroup(groupRepository.findOne(group.getId())));
		form.getUsers().forEach(user -> notification.addUser(userRepository.findByUserNo(user.getUserNo())));
		form.getCompanies().forEach(company -> notification.addCompany(companyRepository.findByUniqueId(company.getUniqueId())));
		
		notification.setCreatedBy(currentUser);
		notification.setDateCreated(Date.from(clock.instant()));
		
		notificationRepository.save(notification);
	}

	@Override
	@Transactional(propagation = Propagation.MANDATORY)
	public void sendNotification(String topic, NotificationDTO notificationDTO, User currentUser) {
		
		Clock clock = Environment.clock();
		
		Notification notification = new Notification();
		notification.setCategory(categoryRepository.findByCode(notificationDTO.getCategory().name()));
		notification.setNotification(notificationDTO.getNotification());
		notification.setGroups(groupRepository.findByNames(notificationDTO.getGroups()));
		notification.setUsers(userRepository.findByUserNos(notificationDTO.getUsers()));
		notification.setCompanies(companyRepository.findByUniqueIds(notificationDTO.getCompanies()));
		notification.setStartDate(notificationDTO.getStartDate());
		notification.setExpiryDate(notificationDTO.getExpiryDate());
		notification.setCreatedBy(currentUser);
		notification.setDateCreated(Date.from(clock.instant()));
		notificationRepository.save(notification);
		
		NotificationBean notificationBean = mapper.map(notification, NotificationBean.class);
		
		Set<GroupBean> groups = new HashSet<>();
		notification.getGroups().forEach(g -> groups.add(mapper.map(g, GroupBean.class)));
		
		notificationBean.setGroups(groups);
		template.convertAndSend(topic, notificationBean);
		LOGGER.info("Notification has been sent");
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void sendNotification(String topic, NotificationDTO notificationDTO) {
		
		Clock clock = Environment.clock();
		
		Notification notification = new Notification();
		notification.setCategory(categoryRepository.findByCode(notificationDTO.getCategory().name()));
		notification.setNotification(notificationDTO.getNotification());
		notification.setGroups(groupRepository.findByNames(notificationDTO.getGroups()));
		notification.setUsers(userRepository.findByUserNos(notificationDTO.getUsers()));
		notification.setCompanies(companyRepository.findByUniqueIds(notificationDTO.getCompanies()));
		notification.setStartDate(notificationDTO.getStartDate());
		notification.setExpiryDate(notificationDTO.getExpiryDate());
		notification.setCreatedBy(notificationDTO.getCurrentUser());
		notification.setDateCreated(Date.from(clock.instant()));
		notificationRepository.save(notification);
		
		NotificationBean notificationBean = mapper.map(notification, NotificationBean.class);
		
		notification.getUsers().forEach(u -> notificationBean.addUser(mapper.map(u, UserBean.class)));
		notification.getGroups().forEach(g -> notificationBean.addGroup(mapper.map(g, GroupBean.class)));
		notification.getCompanies().forEach(c -> notificationBean.addCompany(mapper.map(c, CompanyBean.class)));
		
		template.convertAndSend(topic, SocketEvent.createEvent(NOTIFICATION, notificationBean));
		LOGGER.info("Notification has been sent");
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<NotificationBean> findAll(User currentUser) {
		return notificationHelper.findNotificationBy(currentUser.getGroup().getUniqueId(), currentUser.getUserNo());
	}

}
