package com.carefarm.prakrati.service.impl;

import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.core.authority.PrakratiUserAuthorityUtils;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.service.UserContext;

@Component
public class SpringSecurityUserContext implements UserContext {

	@Override
	public User getCurrentUser() {
		SecurityContext context = SecurityContextHolder.getContext();
		Authentication authentication = context.getAuthentication();
        if (authentication == null) {
            return null;
        }
        return (User) authentication.getPrincipal();
	}

	@Override
	public void setCurrentUser(User user) {
		if (user == null) {
            throw new IllegalArgumentException("user cannot be null");
        }
		Collection<? extends GrantedAuthority> authorities = PrakratiUserAuthorityUtils.createAuthorities(user);
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(user,
                user.getPassword(), authorities);
        SecurityContextHolder.getContext().setAuthentication(authentication);
	}
	
}
