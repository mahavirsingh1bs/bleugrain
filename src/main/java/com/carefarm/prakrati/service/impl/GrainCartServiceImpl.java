package com.carefarm.prakrati.service.impl;

import java.text.NumberFormat;
import java.time.Clock;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.common.entity.Unit;
import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.entity.GrainCart;
import com.carefarm.prakrati.entity.Item;
import com.carefarm.prakrati.entity.Product;
import com.carefarm.prakrati.entity.Quantity;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.generators.UniqueIdBuilder;
import com.carefarm.prakrati.repository.GrainCartRepository;
import com.carefarm.prakrati.repository.ItemRepository;
import com.carefarm.prakrati.repository.ProductRepository;
import com.carefarm.prakrati.repository.UnitRepository;
import com.carefarm.prakrati.service.GrainCartService;
import com.carefarm.prakrati.web.bean.CompanyBean;
import com.carefarm.prakrati.web.bean.GrainCartBean;
import com.carefarm.prakrati.web.bean.ItemBean;
import com.carefarm.prakrati.web.bean.ProductBean;
import com.carefarm.prakrati.web.bean.UnitBean;
import com.carefarm.prakrati.web.bean.UserBean;

@Service
public class GrainCartServiceImpl extends PrakratiServiceImpl implements GrainCartService {

	private NumberFormat numberFormat = NumberFormat.getCurrencyInstance();
	
	@Autowired
	private GrainCartRepository grainCartRepository;

	@Autowired
	private ItemRepository itemRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private UnitRepository unitRepository;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void addToCart(String uniqueId, User user) {
		Clock clock = Environment.clock();
		
		GrainCart grainCart = grainCartRepository.findByUser(user);
		Product product = productRepository.findByUniqueId(uniqueId);
		if (Predicates.notNull.test(grainCart)) {
			grainCart.addProduct(product);
			grainCart.setModifiedBy(user);
			grainCart.setDateModified(Date.from(clock.instant()));
		} else {
			grainCart = new GrainCart();
			grainCart.addProduct(product);
			grainCart.setUser(user);
			grainCart.setDateCreated(Date.from(clock.instant()));
			grainCartRepository.save(grainCart);
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void addToConsumerCart(ProductBean productBean, User user) {
		Clock clock = Environment.clock();
		
		GrainCart grainCart = grainCartRepository.findByUser(user);
		Product product = productRepository.findByUniqueId(productBean.getUniqueId());
		
		UnitBean unitBean = productBean.getQuantityUnit();
		Unit quantityUnit = unitRepository.findOne(unitBean.getId());
		
		Item item = new Item();
		item.setUniqueId(UniqueIdBuilder.generateUniqueId());
		item.setName(product.getName());
		item.setPrice(product.getLatestPrice());
		item.setQuantity(new Quantity(productBean.getQuantity().doubleValue(), quantityUnit));
		item.setImage(product.getDefaultImage());
		
		if (Predicates.notNull.test(grainCart)) {
			grainCart.addItem(item);
			grainCart.setModifiedBy(user);
			grainCart.setDateModified(Date.from(clock.instant()));
		} else {
			GrainCart newGrainCart = new GrainCart();
			newGrainCart.addItem(item);
			newGrainCart.setConsumerCart(Boolean.TRUE);
			newGrainCart.setUser(user);
			newGrainCart.setDateCreated(Date.from(clock.instant()));
			grainCartRepository.save(newGrainCart);
		}
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public GrainCartBean findCartDetails(User user) {
		GrainCart grainCart = grainCartRepository.findByUser(user);
		
		GrainCartBean grainCartBean = null;
		if (Predicates.notNull.test(grainCart)) {
			grainCartBean = mapper.map(grainCart, GrainCartBean.class);
			grainCartBean.setTotalItems(grainCart.totalItems());
			Double totalCost = grainCart.getSubtotal() + grainCart.getShippingCost();
			grainCartBean.setTotalCost(numberFormat.format(totalCost));
			this.extractCommonProperties(grainCartBean, grainCart);
			return grainCartBean;
		} else {
			grainCartBean = new GrainCartBean();
		}
		
		return grainCartBean;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void removeFromCart(String uniqueId, User user) {
		GrainCart grainCart = grainCartRepository.findByUser(user);
		grainCart.removeProduct(uniqueId);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void removeFromConsumerCart(String uniqueId, User user) {
		GrainCart grainCart = grainCartRepository.findByUser(user);
		grainCart.removeItem(uniqueId);
		itemRepository.removeByUniqueId(uniqueId);
	}
	
	private void extractCommonProperties(GrainCartBean grainCartBean, GrainCart grainCart) {
		if (Predicates.notNull.test(grainCart.getUser())) {
			grainCartBean.setUser(mapper.map(grainCart.getUser(), UserBean.class));
		}
		
		if (Predicates.notNull.test(grainCart.getCompany())) {
			grainCartBean.setCompany(mapper.map(grainCart.getCompany(), CompanyBean.class));
		}
		
		List<ProductBean> products = new ArrayList<>();
		grainCart.getProducts()
			.forEach(product -> products.add(mapper.map(product, ProductBean.class)));
		grainCartBean.setProducts(products);
		
		List<ItemBean> items = new ArrayList<>();
		grainCart.getItems()
			.forEach(item -> {
				ItemBean itemBean = mapper.map(item, ItemBean.class);
				Double totalPrice = item.getPrice().getPrice() * item.getQuantity().getQuantity();
				itemBean.setTotalPrice(numberFormat.format(totalPrice));
				items.add(itemBean);
			});
		grainCartBean.setItems(items);
		
		if (Predicates.notNull.test(grainCart.getCreatedBy())) {
			grainCartBean.setCreatedBy(mapper.map(grainCart.getCreatedBy(), UserBean.class));
		}
		
		if (Predicates.notNull.test(grainCart.getModifiedBy())) {
			grainCartBean.setModifiedBy(mapper.map(grainCart.getModifiedBy(), UserBean.class));
		}
	}
	
}
