package com.carefarm.prakrati.service.impl;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.Clock;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.delivery.util.OrderStatus;
import com.carefarm.prakrati.entity.AddressEntity;
import com.carefarm.prakrati.entity.GrainCart;
import com.carefarm.prakrati.entity.Order;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.generators.OrderNoGenerator;
import com.carefarm.prakrati.repository.GrainCartRepository;
import com.carefarm.prakrati.repository.OrderRepository;
import com.carefarm.prakrati.repository.OrderStatusRepository;
import com.carefarm.prakrati.repository.ProductStatusRepository;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.service.OrderService;
import com.carefarm.prakrati.service.helper.ItemHelper;
import com.carefarm.prakrati.service.helper.OrderHelper;
import com.carefarm.prakrati.service.helper.ProductHelper;
import com.carefarm.prakrati.web.bean.AddressEntityBean;
import com.carefarm.prakrati.web.bean.CompanyBean;
import com.carefarm.prakrati.web.bean.ItemBean;
import com.carefarm.prakrati.web.bean.OrderBean;
import com.carefarm.prakrati.web.bean.OrderDetailsBean;
import com.carefarm.prakrati.web.bean.ProductBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.model.OrderAssignForm;
import com.carefarm.prakrati.web.model.OrderDispatchForm;
import com.carefarm.prakrati.web.model.OrderUpdateForm;

@Service
public class OrderServiceImpl extends PrakratiServiceImpl implements OrderService {

	@Autowired
	private ItemHelper itemHelper;
	
	@Autowired
	private ProductHelper productHelper;

	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private GrainCartRepository grainCartRepository;
	
	@Autowired
	private ProductStatusRepository productStatusRepository;
	
	@Autowired
	private OrderStatusRepository orderStatusRepository;
	
	@Autowired
	private OrderHelper orderHelper;
	
	@Override
	public List<Order> findOrdersByUser(Long userId) throws ServiceException {
		return orderRepository.findOrdersByUserId(userId);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Map<String, Object> findByKeyword(SearchRequest searchRequest) {
		Map<String, Object> data = new HashMap<>();
		
		Pageable pageable = createPageRequest(searchRequest);
		
		Long totalRecord = 0L;
		List<Order> orders = null;
		if (Predicates.notNull.test(searchRequest.getCompanyId())) {
			totalRecord = orderRepository.countByCompanyIdAndStatusCodesNotIn(searchRequest.getCompanyId(), notInOrderStatuses);
			orders = orderRepository.findByCompanyIdAndStatusCodesNotIn(searchRequest.getCompanyId(), notInOrderStatuses, pageable);
		} else if (Predicates.notNull.test(searchRequest.getUserNo())) {
			totalRecord = orderRepository.countByUserNoAndStatusCodesNotIn(searchRequest.getUserNo(), notInOrderStatuses);
			orders = orderRepository.findByUserNoAndStatusCodesNotIn(searchRequest.getUserNo(), notInOrderStatuses, pageable);
		} else {
			totalRecord = orderRepository.countByStatusCodesNotIn(notInOrderStatuses);
			orders = orderRepository.findByStatusCodesNotIn(notInOrderStatuses, pageable);
		}
		List<OrderBean> orderBeans = new ArrayList<>();
		for (Order order : orders) {
			OrderBean orderBean = mapper.map(order, OrderBean.class);
			Optional.ofNullable(order.getUser())
				.ifPresent(user -> orderBean.setUser(mapper.map(user, UserBean.class)));
			Optional.ofNullable(order.getCompany())
				.ifPresent(company -> orderBean.setCompany(mapper.map(company, CompanyBean.class)));
			orderBean.setBillingAddress(mapper.map(order.getBillingAddress(), AddressEntityBean.class));
			orderBean.setBillingAddress(mapper.map(order.getShippingAddress(), AddressEntityBean.class));
			orderBean.setProducts(productHelper.convertEntitiesToBeans(order.getProducts(), ProductBean.class));
			orderBean.setDeliveryDate(DATE_FORMAT.format(order.getDeliveryDate()));
			orderBeans.add(orderBean);
		}
		data.put("recordsTotal", totalRecord);
		data.put("recordsFiltered", totalRecord);
		data.put("data", orderBeans);
		return data;
	}


	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void processOrder(OrderDetailsBean orderDetails, User currentUser) {
		Clock clock = Environment.clock();
		Order order = new Order();
		GrainCart grainCart = grainCartRepository.findOne(orderDetails.getGrainCartId());
		
		order.setOrderNo(OrderNoGenerator.generateOrderNo().toString());
		order.setMocked(Boolean.FALSE);
		
		if (!grainCart.isConsumerCart()) {
			grainCart.getProducts()
				.forEach((p) -> p.setStatus(productStatusRepository.findByCode("SOLD_OUT")));
			order.setProducts(grainCart.getProducts());
		} else {
			order.setItems(grainCart.getItems());
		}
		
		order.setSubtotal(BigDecimal.valueOf(grainCart.getSubtotal()));
		order.setShippingCost(BigDecimal.valueOf(grainCart.getShippingCost()));
		order.setTotalPrice(BigDecimal.valueOf(grainCart.getSubtotal() + grainCart.getShippingCost()));
		order.setAmountPaid(orderDetails.getAdvancedPayment());
		order.setStatus(orderStatusRepository.findByStatus(OrderStatus.PLACED.getValue()));
		order.setBillingAddress(mapper.map(orderDetails.getBillingAddress(), AddressEntity.class));
		order.setShippingAddress(mapper.map(orderDetails.getShippingAddress(), AddressEntity.class));
		order.setOrderDate(Date.from(clock.instant()));
		order.setDeliveryDate(Date.from(clock.instant().plus(7L, ChronoUnit.DAYS)));
		setOrderOf(order, currentUser);
		order.setCreatedBy(currentUser);
		order.setDateCreated(Date.from(clock.instant()));
		orderRepository.save(order);
		grainCartRepository.delete(grainCart);
		
		orderHelper.createAndSendMessages(order, OrderStatus.PLACED.getCode());
		orderHelper.publishIndexEvent(order, EventType.INSERT);
	}
	
	public void setOrderOf(Order order, User currentUser) {
		if (currentUser.isACompanyUser()) {
			order.setCompany(currentUser.getCompany());
		}
		order.setUser(currentUser);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public OrderBean findDetail(String orderNo) {
		Order order = orderRepository.findByOrderNo(orderNo);
		OrderBean orderBean = mapper.map(order, OrderBean.class);
		orderBean.setBillingAddress(mapper.map(order.getBillingAddress(), AddressEntityBean.class));
		orderBean.setShippingAddress(mapper.map(order.getShippingAddress(), AddressEntityBean.class));
		orderBean.setProducts(productHelper.convertEntitiesToBeans(order.getProducts(), ProductBean.class));
		orderBean.setItems(itemHelper.convertEntitiesToBeans(order.getItems(), ItemBean.class));
		orderBean.setDeliveryDate(getFormatDate(order.getDeliveryDate()));
		
		Optional.ofNullable(order.getAssignedBy())
			.ifPresent(assignedBy -> 
				orderBean.setAssignedBy(mapper.map(assignedBy, UserBean.class)));
		
		Optional.ofNullable(order.getAssignedTo())
			.ifPresent(assignedTo -> 
				orderBean.setAssignedTo(mapper.map(assignedTo, UserBean.class)));
	
		return orderBean;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(String uniqueId) {
		Order order= orderRepository.findOne(NumberUtils.toLong(uniqueId));
		order.setArchived(Boolean.TRUE);
		orderHelper.publishIndexEvent(order, EventType.DELETE);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updateOrder(OrderUpdateForm form, User currentUser) {
		Order order = orderRepository.findByOrderNo(form.getOrderNo());
		order.setStatus(orderStatusRepository.findByStatus(form.getStatus().getStatus()));
		if (form.getStatus().getCode().equals(OrderStatus.DISPATCHED.getCode())) {
			order.setDeliverBy(userRepository.findByUserNo(form.getDeliverBy().getUserNo()));
			order.setDeliveryDate(getParseDate(form.getDeliveryDate()));
		}
		order.setModifiedBy(currentUser);
		order.setDateModified(Date.from(Environment.clock().instant()));
		
		orderHelper.createAndSendMessages(order, form.getStatus().getCode());
		orderHelper.publishIndexEvent(order, EventType.UPDATE);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void assignOrder(OrderAssignForm form, User currentUser) {
		Order order = orderRepository.findByOrderNo(form.getOrderNo());
		order.setAssignedBy(currentUser);
		order.setAssignedTo(userRepository.findByUserNo(form.getAssignTo().getUserNo()));
		order.setDateAssigned(Date.from(Environment.clock().instant()));
		order.setComments(form.getComments());
		order.setModifiedBy(currentUser);
		order.setDateModified(Date.from(Environment.clock().instant()));
		
		orderHelper.createAndSendMessages(order, OrderStatus.ASSIGNED.getCode());
		orderHelper.publishIndexEvent(order, EventType.UPDATE);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void shipOrder(String orderNo, User currentUser) {
		Order order = orderRepository.findByOrderNo(orderNo);
		order.setStatus(orderStatusRepository.findByCode(OrderStatus.SHIPPED.getCode()));
		order.setModifiedBy(currentUser);
		order.setDateModified(Date.from(Environment.clock().instant()));
		
		orderHelper.createAndSendMessages(order, OrderStatus.SHIPPED.getCode());
		orderHelper.publishIndexEvent(order, EventType.UPDATE);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void dispatchOrder(OrderDispatchForm form, User currentUser) {
		Order order = orderRepository.findByOrderNo(form.getOrderNo());
		order.setStatus(orderStatusRepository.findByCode(OrderStatus.DISPATCHED.getCode()));
		order.setDeliverBy(userRepository.findByUserNo(form.getDeliverBy().getUserNo()));
		order.setDeliveryDate(getParseDate(form.getDeliveryDate()));
		order.setModifiedBy(currentUser);
		order.setDateModified(Date.from(Environment.clock().instant()));
		
		orderHelper.createAndSendMessages(order, OrderStatus.DISPATCHED.getCode());
		orderHelper.publishIndexEvent(order, EventType.UPDATE);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void deliverOrder(String orderNo, User currentUser) {
		Order order = orderRepository.findByOrderNo(orderNo);
		order.setStatus(orderStatusRepository.findByCode(OrderStatus.DELIVERED.getCode()));
		order.setModifiedBy(currentUser);
		order.setDateModified(Date.from(Environment.clock().instant()));
		
		orderHelper.createAndSendMessages(order, OrderStatus.DELIVERED.getCode());
		orderHelper.publishIndexEvent(order, EventType.UPDATE);
	}

}
