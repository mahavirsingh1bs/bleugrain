package com.carefarm.prakrati.service.impl;

import java.text.ParseException;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.entity.UpcomingEvent;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.generators.UniqueIdBuilder;
import com.carefarm.prakrati.repository.CompanyRepository;
import com.carefarm.prakrati.repository.UpcomingEventRepository;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.service.UpcomingEventService;
import com.carefarm.prakrati.web.model.UpcomingEventForm;

@Service
public class UpcomingEventServiceImpl extends PrakratiServiceImpl implements UpcomingEventService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UpcomingEventServiceImpl.class);
	
	@Autowired
	private CompanyRepository companyRepository;
	
	@Autowired
	private UpcomingEventRepository upcomingEventRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void addUpcomingEvent(UpcomingEventForm form, String userNo, User currentUser) {
		
		User user = userRepository.findByUserNo(userNo);
		
		UpcomingEvent upcomingEvent = mapper.map(form, UpcomingEvent.class);
		upcomingEvent.setUniqueId(UniqueIdBuilder.generateUniqueId());
		upcomingEvent.addUser(user);
		
		form.getGroups().forEach(g -> upcomingEvent.addGroup(groupRepository.findOne(g.getId())));
		form.getUsers().forEach(u -> upcomingEvent.addUser(userRepository.findByUserNo(u.getUserNo())));
		form.getCompanies().forEach(c -> upcomingEvent.addCompany(companyRepository.findByUniqueId(c.getUniqueId())));
		
		try {
			upcomingEvent.setEventDate(DATE_FORMAT.parse(form.getEventDate()));
		} catch (ParseException e) {
			LOGGER.error("Got an error while trying to format event date: " + form.getEventDate());
		}
		
		upcomingEvent.setUser(userRepository.findByUserNo(form.getOrganizerId()));
		upcomingEvent.setCreatedBy(currentUser);
		upcomingEvent.setDateCreated(Date.from(Environment.clock().instant()));
		
		upcomingEventRepository.save(upcomingEvent);
		
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(String uniqueId, User currentUser) {
		UpcomingEvent upcomingEvent = upcomingEventRepository.findByUniqueId(uniqueId);
		upcomingEvent.setArchived(Boolean.TRUE);
		upcomingEvent.setModifiedBy(currentUser);
		upcomingEvent.setDateModified(Date.from(Environment.clock().instant()));
	}

}
