package com.carefarm.prakrati.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.entity.Category;
import com.carefarm.prakrati.repository.CategoryRepository;
import com.carefarm.prakrati.repository.CompanyRepository;
import com.carefarm.prakrati.repository.ProductRepository;
import com.carefarm.prakrati.service.HomeService;
import com.carefarm.prakrati.util.UserType;

@Service
public class HomeServiceImpl extends PrakratiServiceImpl implements HomeService {

	@Autowired
	private CompanyRepository companyRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Map<String, Object> getApplicationDetails() {
		Map<String, Object> applicationDetails = new HashMap<>();
		applicationDetails.put("farmers", userRepository.countByUserType(UserType.FARMER));
		applicationDetails.put("retailers", userRepository.countByUserType(UserType.RETAILER));
		applicationDetails.put("brokers", userRepository.countByUserType(UserType.BROKER));
		applicationDetails.put("companies", companyRepository.countAll());
		
		for (Category category : categoryRepository.findMainCategories()) {
			applicationDetails.put(category.getId() + "", productRepository.countByMainCategoryId(category.getUniqueId()));
		}
		return applicationDetails;
	}
	
}
