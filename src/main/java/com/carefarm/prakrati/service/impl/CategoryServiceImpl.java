package com.carefarm.prakrati.service.impl;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.common.util.Constants;
import com.carefarm.prakrati.entity.Category;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.repository.CategoryRepository;
import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.search.repository.CategorySearchRepository;
import com.carefarm.prakrati.service.CategoryService;
import com.carefarm.prakrati.service.helper.CategoryHelper;
import com.carefarm.prakrati.web.bean.CategoryBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.CategoryForm;

@Service
public class CategoryServiceImpl extends PrakratiServiceImpl implements CategoryService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CategoryServiceImpl.class);
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private CategorySearchRepository categorySearchRepository;
	
	@Autowired
	private CategoryHelper categoryHelper;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<CategoryBean> findMainCategories() {
		List<CategoryBean> categories = new ArrayList<>();
		for (Category category : categoryRepository.findMainCategories()) {
			CategoryBean mainCategory = mapper.map(category, CategoryBean.class);
			for (Category childCategory : category.getChildCategories()) {
				createCategoryNode(mainCategory, childCategory);
			}
			categories.add(mainCategory);
		}
		return categories;
	}

	public void createCategoryNode(CategoryBean parentCategoryBean, Category category) {
		CategoryBean categoryBean = mapper.map(category, CategoryBean.class);
		parentCategoryBean.addChildCategory(categoryBean);
		
		for (Category childCategory : category.getChildCategories()) {
			createCategoryNode(categoryBean, childCategory);
		}
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<CategoryBean> findAll() {
		List<CategoryBean> categories = new ArrayList<>();
		for (Category category : categoryRepository.findAll()) {
			CategoryBean categoryBean = mapper.map(category, CategoryBean.class);
			categories.add(categoryBean);
		}
		return categories;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Map<String, Object> findByKeyword(SearchRequest searchRequest) {
		Map<String, Object> data = new HashMap<>();
		List<CategoryBean> categoryBeans = new ArrayList<>();
		
		PageRequest pageRequest = createPageRequest(searchRequest);
		
		long totalRecord = 0;
		Iterable<Category> categories = null;
		if (searchRequest.isMainCategories()) {
			String keyword = PERC + StringUtils.defaultIfEmpty(searchRequest.getKeyword(), Constants.BLANK) + PERC;
			totalRecord = categoryRepository.countMainCategories(keyword);
			categories = categoryRepository.findMainCategories(keyword, pageRequest);
		} else {
			SearchResult<Long> result = categorySearchRepository.findByKeywordContains(searchRequest, pageRequest);
			totalRecord = result.getTotalHits();
			categories = categoryRepository.findAll(result.getResults());
		}
		
		for (Category category : categories) {
			CategoryBean categoryBean = mapper.map(category, CategoryBean.class);
			categoryHelper.mapBasicFields(category, categoryBean);
			categoryBeans.add(categoryBean);
		}
		
		data.put("recordsTotal", totalRecord);
		data.put("recordsFiltered", totalRecord);
		data.put("data", categoryBeans);
		return data;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(CategoryForm form, User createdBy) throws ServiceException {
		Category category = mapper.map(form, Category.class);
		category.setUniqueId(UUID.randomUUID().toString());
		category.setCreatedBy(createdBy);
		category.setDateCreated(Calendar.getInstance().getTime());
		Optional<String> parentCategoryIdOpt = Optional.ofNullable(form.getParentCategoryId());
		parentCategoryIdOpt.ifPresent(pci -> {
			Category parentCategory = categoryRepository.findByUniqueId(pci);
			category.setParentCategory(parentCategory);
		});
		
		categoryRepository.save(category);
		categoryHelper.publishIndexEvent(category, EventType.INSERT);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public CategoryBean findDetails(long categoryId, Object object) {
		Category category = categoryRepository.findOne(categoryId);
		CategoryBean categoryBean = mapper.map(category, CategoryBean.class);
		categoryHelper.mapBasicFields(category, categoryBean);
		return categoryBean;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(CategoryBean categoryBean, User modifiedBy) throws ServiceException {
		LOGGER.info("Updating category details");
		Category category = categoryRepository.findOne(categoryBean.getId());
		categoryHelper.convertBeanToEntity(category, categoryBean);
		category.setModifiedBy(modifiedBy);
		category.setDateModified(Calendar.getInstance().getTime());
		categoryHelper.publishIndexEvent(category, EventType.UPDATE);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(String uniqueId) {
		Category category = categoryRepository.findOne(NumberUtils.toLong(uniqueId));
		category.setArchived(Boolean.TRUE);
		categoryHelper.publishIndexEvent(category, EventType.DELETE);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<CategoryBean> findByMatchingKeywords(String keywords) {
		List<Category> matchingCategories = categoryRepository.findByMatchingName("%" + keywords + "%");
		List<CategoryBean> matchingCategoryBeans = new ArrayList<>();
		matchingCategories.forEach(category -> matchingCategoryBeans.add(mapper.map(category, CategoryBean.class)));
		return matchingCategoryBeans;
	}

}
