package com.carefarm.prakrati.service.impl;

import java.time.Clock;
import java.util.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.entity.Discussion;
import com.carefarm.prakrati.entity.Tag;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.repository.DiscussionRepository;
import com.carefarm.prakrati.repository.TagRepository;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.service.DiscussionService;
import com.carefarm.prakrati.web.model.DiscussionForm;

@Service
public class DiscussionServiceImpl extends PrakratiServiceImpl implements DiscussionService {

	@Autowired
	private DiscussionRepository discussionRepository;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private TagRepository tagRepository;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void addDiscussion(DiscussionForm form, String userNo, User currentUser) {
		
		Clock clock = Environment.clock();
		User user = userRepository.findByUserNo(userNo);
		
		Discussion discussion = mapper.map(form, Discussion.class);
		discussion.setUniqueId(UUID.randomUUID().toString());
		
		form.getTags().forEach(tagBean -> {
        	Tag tag = mapper.map(tagBean, Tag.class);
        	if (Predicates.isNull.test(tag.getId())) {
        		tag.setCreatedBy(currentUser);
        		tag.setDateCreated(Date.from(clock.instant()));
        		tagRepository.save(tag);
        	}
        	discussion.addTag(tag);
        });
        
		discussion.setUser(user);
		discussion.setCreatedBy(currentUser);
		discussion.setDateCreated(Date.from(Environment.clock().instant()));
		
		discussionRepository.save(discussion);
	}
}
