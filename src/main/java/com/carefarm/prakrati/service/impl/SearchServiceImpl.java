package com.carefarm.prakrati.service.impl;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.entity.Demand;
import com.carefarm.prakrati.entity.Product;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.repository.CompanyRepository;
import com.carefarm.prakrati.repository.DemandRepository;
import com.carefarm.prakrati.repository.ProductRepository;
import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.dto.Filters;
import com.carefarm.prakrati.search.dto.ModelMap;
import com.carefarm.prakrati.search.dto.SearchCriteria;
import com.carefarm.prakrati.search.dto.SearchStatus;
import com.carefarm.prakrati.search.repository.CompanySearchRepository;
import com.carefarm.prakrati.search.repository.DemandSearchRepository;
import com.carefarm.prakrati.search.repository.ProductSearchRepository;
import com.carefarm.prakrati.service.SearchService;
import com.carefarm.prakrati.service.helper.ProductEventHelper;
import com.carefarm.prakrati.util.UserType;
import com.carefarm.prakrati.web.bean.AttributeBean;
import com.carefarm.prakrati.web.bean.CategoryBean;
import com.carefarm.prakrati.web.bean.CompanyBean;
import com.carefarm.prakrati.web.bean.DemandBean;
import com.carefarm.prakrati.web.bean.ImageBean;
import com.carefarm.prakrati.web.bean.OwnerBean;
import com.carefarm.prakrati.web.bean.ProductBean;
import com.carefarm.prakrati.web.bean.ProductEventBean;
import com.carefarm.prakrati.web.bean.ProductStatusBean;
import com.carefarm.prakrati.web.bean.ReviewBean;
import com.carefarm.prakrati.web.bean.TagBean;
import com.carefarm.prakrati.web.bean.UserBean;

@Service
public class SearchServiceImpl extends PrakratiServiceImpl implements SearchService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SearchServiceImpl.class);
	
	@Autowired
	private CompanyRepository companyRepository;
	
	@Autowired
	private CompanySearchRepository companySearchRepository;
	
	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private ProductSearchRepository productSearchRepository;
	
	@Autowired
	private DemandRepository demandRepository;
		
	@Autowired
	private DemandSearchRepository demandSearchRepository;
	
	@Autowired
	private ProductEventHelper productEventHelper;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ModelMap<ProductBean> search(SearchCriteria criteria) {
		List<ProductBean> matchedProducts = new ArrayList<>();
		
		SearchResult<Long> result = productSearchRepository.findByKeywordContains(criteria);
		for (Product product : productRepository.findAll(result.getResults())) {
			List<ImageBean> images = new ArrayList<>();
			product.getImages().forEach(image -> images.add(mapper.map(image, ImageBean.class)));
			
			ProductBean matchedProduct = mapper.map(product, ProductBean.class);
			matchedProduct.setStatus(mapper.map(product.getStatus(), ProductStatusBean.class));
			matchedProduct.setImages(images);
			matchedProduct.setTotalRating(BigDecimal.valueOf(product.totalRating()));
			matchedProduct.setTotalReviews(product.totalReviews());
			matchedProduct.setRecent(product.isRecent());
			matchedProduct.setOwner(mapper.map(product.getOwner(), OwnerBean.class));
			
			UserType userType = UserType.getUserType(product.getOwner().getGroup().getName());
			matchedProduct.setOwnerType(userType.getType());
			matchedProducts.add(matchedProduct);
		}
		
		ModelMap<ProductBean> modelMap = new ModelMap<>();
		modelMap.setLastKeyword(criteria.getKeywords());
		modelMap.setLastPage(criteria.getPageable().getPageNumber());
		modelMap.setLastSize(criteria.getPageable().getPageSize());
		modelMap.setResultSize(result.getTotalHits());
		modelMap.setResults(matchedProducts);
		
		if (!matchedProducts.isEmpty()) {
			Filters filters = Filters.Builder.getInstance()
				.sellers(result.getSellerBuckets())
				.categories(result.getCategoryBuckets())
				.quantities(result.getQuantityBuckets())
				.prices(result.getPriceBuckets())
				.reviews(result.getRatingBuckets())
				.build();
			modelMap.setFilters(filters);
			modelMap.setStatus(SearchStatus.FOUND);
		} else {
			modelMap.setStatus(SearchStatus.NOT_FOUND);
			String message = String.format("Could not find any matches '%s'", criteria.getKeywords());
			modelMap.setMessage(message);
		}
		return modelMap;
	}

	@Override
	public ModelMap<DemandBean> searchDemands(SearchCriteria criteria) {
		List<DemandBean> demandsData = new ArrayList<>();
		
		SearchResult<Long> result = demandSearchRepository.findByKeywordContains(criteria);
		for (Demand demand : demandRepository.findAll(result.getResults())) {
			DemandBean demandBean = mapper.map(demand, DemandBean.class);
			demandBean.setRecent(demand.isRecent());
			Optional.ofNullable(demand.getDefaultImage())
				.ifPresent(image -> demandBean.setDefaultImage(mapper.map(image, ImageBean.class)));
			
			Optional.ofNullable(demand.getUser())
				.ifPresent(user -> {
					demandBean.setUser(mapper.map(user, UserBean.class));
					UserType userType = UserType.getUserType(user.getGroup().getName());
					demandBean.setCustType(userType.getType());
				});
			
			Optional.ofNullable(demand.getCompany())
				.ifPresent(company -> { 
					demandBean.setCompany(mapper.map(company, CompanyBean.class));
					demandBean.setCustType(UserType.COMPANY.getType());
				});
			demandBean.setCreatedBy(mapper.map(demand.getCreatedBy(), UserBean.class));
			demandsData.add(demandBean);
		}
		
		ModelMap<DemandBean> modelMap = new ModelMap<>();
		modelMap.setLastKeyword(criteria.getKeywords());
		modelMap.setLastPage(criteria.getPageable().getPageNumber());
		modelMap.setLastSize(criteria.getPageable().getPageSize());
		modelMap.setResultSize(result.getTotalHits());
		modelMap.setResults(demandsData);
		
		if (!demandsData.isEmpty()) {
			Filters filters = Filters.Builder.getInstance()
				.buyers(result.getBuyerBuckets())
				.categories(result.getCategoryBuckets())
				.quantities(result.getQuantityBuckets())
				.prices(result.getPriceBuckets())
				.build();
			modelMap.setFilters(filters);
			modelMap.setStatus(SearchStatus.FOUND);
		} else {
			modelMap.setStatus(SearchStatus.NOT_FOUND);
			String message = String.format("Could not find any matches '%s'", criteria.getKeywords());
			modelMap.setMessage(message);
		}
		return modelMap;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Map<String, Object> findProductDetail(String uniqueId, User currentUser) {
		Map<String, Object> productDetail = new HashMap<>();
		
		LOGGER.info("START: Getting product detail " + uniqueId);
		Product product = productRepository.findByUniqueId(uniqueId);
		ProductBean productBean = mapper.map(product, ProductBean.class);
		
		Optional.ofNullable(product.getStatus())
			.ifPresent(status -> 
				productBean.setStatus(mapper.map(status, ProductStatusBean.class)));
		
		product.getCategories().forEach(c -> productBean.addCategory(mapper.map(c, CategoryBean.class)));
		productBean.setTotalRating(BigDecimal.valueOf(product.totalRating()));
		productBean.setTotalReviews(product.totalReviews());
		
		User owner = product.getOwner();
		OwnerBean ownerBean = mapper.map(owner, OwnerBean.class);
		if (Predicates.notNull.test(owner.getDefaultImage())) {
			ownerBean.setPhoto(owner.getDefaultImage().getMediumFilepath());
		}
		
		productBean.setOwner(ownerBean);;

		List<ImageBean> images = new ArrayList<>();
		product.getImages().forEach(image -> images.add(mapper.map(image, ImageBean.class)));
		productBean.setImages(images);
		
		List<TagBean> tags = new ArrayList<>();
		product.getTags().forEach(tag -> tags.add(mapper.map(tag, TagBean.class)));
		productBean.setTags(tags);
		
		List<AttributeBean> attributes = new ArrayList<>();
		product.getAttributes().forEach(attribute -> attributes.add(mapper.map(attribute, AttributeBean.class)));
		productBean.setAttributes(attributes);
		
		List<ReviewBean> reviews = new ArrayList<>();
		product.getReviews().forEach(review -> {
			ReviewBean reviewBean = mapper.map(review, ReviewBean.class);
			reviewBean.setReviewBy(mapper.map(review.getReviewBy(), UserBean.class));
			reviews.add(reviewBean); 
		});
		productBean.setReviews(reviews);
		
		productBean.setEvents(productEventHelper.convertEntitiesToBeans(product.getEvents(), ProductEventBean.class));
		productDetail.put("product", productBean);
		
		StringBuilder keywords = new StringBuilder();
		product.getCategories().forEach(category -> keywords.append(category.getName() + ", "));
		product.getTags().forEach(tag -> keywords.append(tag.getName() + ", "));
		
		SearchResult<Long> result = productSearchRepository.findByKeywordContains(keywords.substring(0, keywords.length() - 2), product.getUniqueId(), currentUser, new PageRequest(0, 10));
		List<ProductBean> relatedProductBeans = new ArrayList<ProductBean>();
		for (Product relatedProduct : productRepository.findAll(result.getResults())) {
			ProductBean relatedProductBean = mapper.map(relatedProduct, ProductBean.class);
			relatedProductBean.setTotalRating(BigDecimal.valueOf(product.totalRating()));
			relatedProductBean.setTotalReviews(product.totalReviews());
			relatedProductBean.setRecent(product.isRecent());
			relatedProductBean.setOwner(mapper.map(product.getOwner(), OwnerBean.class));
			relatedProductBean.setDefaultImage(mapper.map(relatedProduct.getDefaultImage(), ImageBean.class));
			UserType userType = UserType.getUserType(product.getOwner().getGroup().getName());
			productBean.setOwnerType(userType.getType());
			relatedProductBeans.add(relatedProductBean);
		}
		
		productDetail.put("relatedProducts", relatedProductBeans);
		return productDetail;
	}
}
