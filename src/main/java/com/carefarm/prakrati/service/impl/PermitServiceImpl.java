package com.carefarm.prakrati.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.carefarm.prakrati.delivery.entity.Permit;
import com.carefarm.prakrati.repository.PermitRepository;
import com.carefarm.prakrati.service.PermitService;
import com.carefarm.prakrati.web.bean.PermitBean;

@Service
public class PermitServiceImpl extends PrakratiServiceImpl implements
		PermitService {

	@Autowired
	private PermitRepository permitRepository;
	
	@Override
	public List<PermitBean> findAll() {
		List<PermitBean> permits = new ArrayList<>();
		for (Permit permit: permitRepository.findAll()) {
			permits.add(mapper.map(permit, PermitBean.class));
		}
		return permits;
	}

}
