package com.carefarm.prakrati.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.entity.Tag;
import com.carefarm.prakrati.repository.TagRepository;
import com.carefarm.prakrati.service.TagService;
import com.carefarm.prakrati.web.bean.TagBean;

@Service
public class TagServiceImpl extends PrakratiServiceImpl implements TagService {

	@Autowired
	private TagRepository tagRepository;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<TagBean> findByMatchingKeywords(String keywords) {
		List<Tag> matchingTags = tagRepository.findByMatchingName("%" + keywords + "%");
		List<TagBean> matchingTagBeans = new ArrayList<>();
		matchingTags.forEach(tag -> matchingTagBeans.add(mapper.map(tag, TagBean.class)));
		return matchingTagBeans;
	}

}
