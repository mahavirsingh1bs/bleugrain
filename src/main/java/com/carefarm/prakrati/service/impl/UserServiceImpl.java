package com.carefarm.prakrati.service.impl;

import java.time.Clock;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.common.util.Constants;
import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.messaging.AccountEmailHandler;
import com.carefarm.prakrati.payment.entity.BankAccount;
import com.carefarm.prakrati.payment.entity.CreditCard;
import com.carefarm.prakrati.payment.repository.BankAccountRepository;
import com.carefarm.prakrati.payment.repository.CreditCardRepository;
import com.carefarm.prakrati.repository.OrderRepository;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.security.PasswordManager;
import com.carefarm.prakrati.service.UserService;
import com.carefarm.prakrati.service.helper.AmazonS3Helper;
import com.carefarm.prakrati.service.helper.BankAccountHelper;
import com.carefarm.prakrati.service.helper.CreditCardHelper;
import com.carefarm.prakrati.service.helper.UserHelper;
import com.carefarm.prakrati.util.UserType;
import com.carefarm.prakrati.vo.ImageVO;
import com.carefarm.prakrati.web.bean.BankAccountBean;
import com.carefarm.prakrati.web.bean.CountryBean;
import com.carefarm.prakrati.web.bean.CreditCardBean;
import com.carefarm.prakrati.web.bean.ImageBean;
import com.carefarm.prakrati.web.bean.LanguageBean;
import com.carefarm.prakrati.web.bean.OrderBean;
import com.carefarm.prakrati.web.bean.ReviewBean;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.model.BankAccountForm;
import com.carefarm.prakrati.web.model.ChangePasswdForm;
import com.carefarm.prakrati.web.model.CreditCardForm;

@Service
public class UserServiceImpl extends PrakratiServiceImpl implements UserService {

	@Value("${notify.user.passwd.change.email}")
	private boolean notifyUserOnPasswdChange;
	
	@Autowired
	private UserHelper userHelper;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private AccountEmailHandler emailHandler;

	@Autowired
	private BankAccountRepository bankAccountRepository;
	
	@Autowired
	private CreditCardRepository creditCardRepository;
	
	@Autowired
	private OrderRepository orderRepository;

	@Autowired
	private BankAccountHelper bankAccountHelper;
	
	@Autowired
	private CreditCardHelper creditCardHelper;
	
	@Autowired
	private AmazonS3Helper amazonS3Helper;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public UserBean findDetail(String userNo) {
		User user = userRepository.findByUserNo(userNo);
		UserBean userBean = mapper.map(user, UserBean.class);
		if (Predicates.notNull.test(user.getDefaultImage())) {
			userBean.setDefaultImage(mapper.map(user.getDefaultImage(), ImageBean.class));
		}
		return userBean;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void forgotPassword(String emailAddress, String contextPath) {
		User user = userRepository.findByEmailAddress(emailAddress);
		if (user != null) {
			String resetPasswdCode = UUID.randomUUID().toString();
			String resetPasswdPath = contextPath + "#/ResetPasswd?" + "resetCode=" + resetPasswdCode;
			emailHandler.sendResetPassword(emailAddress, resetPasswdPath, user);
			user.setResetPasswdCode(resetPasswdCode);
		} else {
			throw new ServiceException("Email Address doesn't exists");
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void resetPasswd(String newPasswd, String resetPasswdCode) throws ServiceException {
		User user = userRepository.findByResetCode(resetPasswdCode);
		if (user == null) {
			throw new ServiceException("Invalid password reset code has been given");
		}
		
		user.setPassword(PasswordManager.encryptPasswd(newPasswd));
		user.setResetPasswdCode(Constants.BLANK);
		emailHandler.notifyOnSuccessfulReset(user.getEmail(), user);
	}

	@Override
	public Boolean validateCode(String resetPasswdCode) {
		User user = userRepository.findByResetCode(resetPasswdCode);
		if (user != null) {
			return Boolean.TRUE;
		}
		return Boolean.FALSE;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void changePasswd(ChangePasswdForm form) throws ServiceException {
		User user = userRepository.findByUserNo(form.getUserId());
		user.setPassword(PasswordManager.encryptPasswd(form.getNewPasswd()));
		
		if (notifyUserOnPasswdChange == Boolean.TRUE) {
			emailHandler.notifyOnSuccessfulReset(user.getEmail(), user);
        }
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<UserBean> findByMatchingKeywords(String keywords) {
		List<User> matchingUsers = userRepository.findByMatchingEmailAddressOrPhoneNoOrMobileNoOrUniqueId("%" + keywords + "%");
		List<UserBean> matchingUserBeans = new ArrayList<>();
		matchingUsers.forEach(user -> matchingUserBeans.add(mapper.map(user, UserBean.class)));
		return matchingUserBeans;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<UserBean> findByMatchingKeywords(String keywords, UserType... userTypes) {
		List<User> matchingUsers = userRepository.findByMatchingEmailAddressOrPhoneNoOrMobileNoOrUniqueIdAndUserType("%" + keywords + "%", userTypes);
		List<UserBean> matchingUserBeans = new ArrayList<>();
		matchingUsers.forEach(user -> matchingUserBeans.add(mapper.map(user, UserBean.class)));
		return matchingUserBeans;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<UserBean> findByMatchingKeywords(String keywords, String companyId, UserType... userTypes) {
		List<User> matchingUsers = userRepository.findByMatchingEmailAddressOrPhoneNoOrMobileNoOrUniqueIdAndCompanyIdAndUserType("%" + keywords + "%", companyId, userTypes);
		List<UserBean> matchingUserBeans = new ArrayList<>();
		matchingUsers.forEach(user -> matchingUserBeans.add(mapper.map(user, UserBean.class)));
		return matchingUserBeans;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<ReviewBean> findReviews(String uniqueId) {
		List<ReviewBean> reviews = new ArrayList<>();
		User user = userRepository.findByUserNo(uniqueId);
		
		user.getReviews().forEach(review -> {
			ReviewBean reviewBean = mapper.map(review, ReviewBean.class);
			reviewBean.setReviewBy(mapper.map(review.getReviewBy(), UserBean.class));
			UserType reviewerType = UserType.getUserType(review.getReviewBy().getGroup().getName());
			reviewBean.setReviewerType(reviewerType.getType());
			reviews.add(reviewBean);
		});
		return reviews;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<OrderBean> findOrders(String uniqueId) {
		List<OrderBean> orderBeans = new ArrayList<>();
		orderRepository.findByUniqueId(uniqueId)
			.forEach(order -> orderBeans.add(mapper.map(order, OrderBean.class)));
		return orderBeans;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Map<String, Object> findSettings(String uniqueId) {
		Map<String, Object> settings = new HashMap<>();
		User user = userRepository.findByUserNo(uniqueId);
		
		UserType userType = UserType.getUserType(user.getGroup().getName());
		UserBean userBean = mapper.map(user, UserBean.class);
		userHelper.extractBasicFields(user, userBean);
		settings.put("user", userBean);
		settings.put("userType", userType.getType());
		
		List<BankAccountBean> bankAccountBeans = new ArrayList<>();
		bankAccountRepository
			.findActiveAccountsByUserId(user.getId())
			.forEach(bankAccount -> bankAccountBeans.add(mapper.map(bankAccount, BankAccountBean.class)));
		
		List<CreditCardBean> creditCardBeans = new ArrayList<>();
		creditCardRepository
			.findActiveCardsByUserNo(user.getUserNo())
			.forEach(creditCard -> creditCardBeans.add(mapper.map(creditCard, CreditCardBean.class)));
		
		settings.put("bankAccounts", bankAccountBeans);
		settings.put("creditCards", creditCardBeans);
		return settings;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void addCreditCard(CreditCardForm creditCardForm, String userNo, User currentUser) {
		User user = userRepository.findByUserNo(userNo);
		
		CreditCard creditCard = mapper.map(creditCardForm, CreditCard.class);
		creditCard.setUniqueId(UUID.randomUUID().toString());
		creditCard.setUser(currentUser);
		creditCard.setCreatedBy(currentUser);
		creditCard.setDateCreated(Date.from(Environment.clock().instant()));
		creditCardHelper.setEntityOf(creditCard, user);
		
		if (creditCardForm.isDefaultBilling()) {
			if (user.isACompanyUser()) {
				user.getCompany().getBillingDetails()
					.forEach(billingDetail -> billingDetail.setDefaultBilling(Boolean.FALSE));
				user.getCompany().setDefaultBillingDetails(creditCard);
			} else {
				user.getBillingDetails()
					.forEach(billingDetail -> billingDetail.setDefaultBilling(Boolean.FALSE));
				user.setDefaultBillingDetails(creditCard);
			}
		}
		
		creditCardRepository.save(creditCard);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void addBankAccount(BankAccountForm bankAccountForm, String userNo, User currentUser) {
		User user = userRepository.findByUserNo(userNo);
		
		BankAccount bankAccount = mapper.map(bankAccountForm, BankAccount.class);
		bankAccount.setUniqueId(UUID.randomUUID().toString());
		bankAccount.setCreatedBy(currentUser);
		bankAccount.setDateCreated(Date.from(Environment.clock().instant()));
		bankAccountHelper.setEntityOf(bankAccount, currentUser);
		
		if (bankAccountForm.isDefaultBilling()) {
			if (user.isACompanyUser()) {
				user.getCompany().getBillingDetails()
					.forEach(billingDetail -> billingDetail.setDefaultBilling(Boolean.FALSE));
				user.getCompany().setDefaultBillingDetails(bankAccount);
			} else {
				user.getBillingDetails()
					.forEach(billingDetail -> billingDetail.setDefaultBilling(Boolean.FALSE));
				user.setDefaultBillingDetails(bankAccount);
			}
		}
		
		bankAccountRepository.save(bankAccount);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Boolean verifyContactNo(String uniqueCode, String oneTimePasswd) {
		User user = userRepository.findByUniqueCode(uniqueCode);
		if (StringUtils.equals(user.getMobileVerifyCode(), oneTimePasswd)) {
			user.setUniqueCode(StringUtils.EMPTY);
			user.setMobileVerifyCode(StringUtils.EMPTY);
			user.setMobileVerified(Boolean.TRUE);
			return Boolean.TRUE;
		} else {
			return Boolean.FALSE;
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public boolean verifyEmailAddress(String verifCode) {
		User user = userRepository.findByEmailVerifCode(verifCode);
		if (Predicates.notNull.test(user)) {
			user.setEmailVerified(Boolean.TRUE);
			user.setEmailVerifyCode(null);
			return Boolean.TRUE;
		}
		throw new ServiceException("Your email address cannot be verified. your verification code might have been expired or you have entered wrong verification code.");
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ImageBean changeProfilePicture(ImageVO userImage, String userNo, User modifiedBy) {
		ImageBean defaultImageBean = null;
		Clock clock = Environment.clock();
		User user = userRepository.findByUserNo(userNo);
		if (Predicates.notNull.test(userImage)) {
			user.setDefaultImage(amazonS3Helper.handleImage(userImage, user));
			user.setModifiedBy(modifiedBy);
			user.setDateModified(Date.from(clock.instant()));
			defaultImageBean = mapper.map(user.getDefaultImage(), ImageBean.class);
		}
		return defaultImageBean;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public Map<String, Object> findCountrySettings(String userNo) {
		Map<String, Object> settings = new HashMap<>();
		User user = userRepository.findByUserNo(userNo);
		settings.put("defaultLanguage", mapper.map(user.getDefaultLanguage(), LanguageBean.class));
		settings.put("defaultLocale", user.getDefaultLocale());
		settings.put("defaultCountry", mapper.map(user.getDefaultCountry(), CountryBean.class));
		return settings;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(UserBean userBean, String userNo, User currentUser) {
		User user = userRepository.findByUserNo(userNo);
		userHelper.convertBeanToEntity(user, userBean);
		user.setModifiedBy(currentUser);
		user.setDateModified(Date.from(Environment.clock().instant()));
	}

	@Override
	public UserBean findCurrentUser(Long userId) {
		User user = userRepository.findOne(userId);
		return userHelper.mapCurrentUser(user);
	}

}
