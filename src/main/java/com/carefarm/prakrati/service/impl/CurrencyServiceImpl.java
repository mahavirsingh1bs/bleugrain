package com.carefarm.prakrati.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.repository.CurrencyRepository;
import com.carefarm.prakrati.service.CurrencyService;
import com.carefarm.prakrati.service.helper.CurrencyHelper;
import com.carefarm.prakrati.web.bean.CurrencyBean;

@Service
public class CurrencyServiceImpl extends PrakratiServiceImpl implements CurrencyService {

	@Autowired
	private CurrencyHelper currencyHelper;
	
	@Autowired
	private CurrencyRepository currencyRepository;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<CurrencyBean> findAll() {
		return currencyHelper.convertEntitiesToBeans(currencyRepository.findAll(), CurrencyBean.class);
	}

}
