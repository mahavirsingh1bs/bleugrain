package com.carefarm.prakrati.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.entity.BuyerWishlist;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.repository.BuyerWishlistRepository;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.service.BuyerWishlistService;
import com.carefarm.prakrati.web.bean.BuyerWishlistBean;
import com.carefarm.prakrati.web.bean.OwnerBean;
import com.carefarm.prakrati.web.bean.ProductBean;

@Service
public class BuyerWishlistServiceImpl extends PrakratiServiceImpl implements BuyerWishlistService {

	private final static String AGENT = "Agent";
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BuyerWishlistRepository wishlistRepository;
	
	@Override
	public List<BuyerWishlist> findCompanyWishlist(Long companyId) {
		List<BuyerWishlist> buyerWishlist = new ArrayList<>();
		for (User agent : userRepository.findByCompanyIdAndGroupName(companyId, AGENT)) {
			buyerWishlist.addAll(wishlistRepository.findWishlistByBuyerId(agent.getId()));
		}
		return buyerWishlist;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Map<String, Object> findByKeyword(String keyword, Pageable pageable,
			Map<String, Object> params) {
		Integer draw = (Integer ) params.get("draw");
		Map<String, Object> data = new HashMap<>();
		Long totalRecord = 0L;
		List<BuyerWishlist> wishlist = null;
		if (params.get("companyId") != null) {
			Long companyId = (Long )params.get("companyId");
			totalRecord = wishlistRepository.countByCompanyId(companyId);
			wishlist = wishlistRepository.findByCompanyId(companyId, pageable);
		} else {
			Long userId = (Long )params.get("userId");
			totalRecord = wishlistRepository.countByUserId(userId);
			wishlist = wishlistRepository.findByUserId(userId, pageable);
		}
		
		List<BuyerWishlistBean> wishlistBeans = new ArrayList<>();
		for (BuyerWishlist buyerWishlist : wishlist) {
			BuyerWishlistBean wishlistBean = mapper.map(buyerWishlist, BuyerWishlistBean.class);
			ProductBean productBean = mapper.map(buyerWishlist.getProduct(), ProductBean.class);
			productBean.setOwner(mapper.map(buyerWishlist.getProduct().getOwner(), OwnerBean.class));
			wishlistBean.setProduct(productBean);
			wishlistBeans.add(wishlistBean);
		}
		data.put("recordsTotal", totalRecord);
		data.put("recordsFiltered", totalRecord);
		data.put("draw", draw);
		data.put("data", wishlistBeans);
		return data;
	}
}
