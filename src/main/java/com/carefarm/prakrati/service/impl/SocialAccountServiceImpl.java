package com.carefarm.prakrati.service.impl;

import java.sql.Date;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.entity.SocialAccount;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.repository.SocialAccountRepository;
import com.carefarm.prakrati.service.SocialAccountService;
import com.carefarm.prakrati.service.helper.SocialAccountHelper;
import com.carefarm.prakrati.web.bean.SocialAccountBean;
import com.carefarm.prakrati.web.model.SocialAccountForm;

@Service
public class SocialAccountServiceImpl extends PrakratiServiceImpl implements SocialAccountService {

	@Autowired
	private SocialAccountHelper socialAccountHelper;
	
	@Autowired
	private SocialAccountRepository socialAccountRepository;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void addSocialAccount(SocialAccountForm socialAccountForm, String userNo, User currentUser) {
		User user = userRepository.findByUserNo(userNo);
		SocialAccount socialAccount = mapper.map(socialAccountForm, SocialAccount.class);
		socialAccount.setUniqueId(UUID.randomUUID().toString());
		setSocialAccountOf(user, socialAccount);
		socialAccount.setCreatedBy(currentUser);
		socialAccount.setDateCreated(Date.from(Environment.clock().instant()));
		socialAccountRepository.save(socialAccount);
	}
	
	private void setSocialAccountOf(User user, SocialAccount socialAccount) {
		if (user.isACompanyUser()) {
			socialAccount.setCompany(user.getCompany());
			user.getCompany().addSocialAccount(socialAccount);
		} else {
			socialAccount.setUser(user);
			user.addSocialAccount(socialAccount);
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public SocialAccountBean find(String uniqueId) {
		SocialAccount socialAccount = socialAccountRepository.findByUniqueId(uniqueId);
		return mapper.map(socialAccount, SocialAccountBean.class);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(SocialAccountBean bean, User currentUser) {
		SocialAccount socialAccount = socialAccountRepository.findByUniqueId(bean.getUniqueId());
		socialAccountHelper.convertBeanToEntity(socialAccount, bean);
		socialAccount.setModifiedBy(currentUser);
		socialAccount.setDateModified(Date.from(Environment.clock().instant()));
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void remove(String uniqueId, User currentUser) {
		SocialAccount socialAccount = socialAccountRepository.findByUniqueId(uniqueId);
		socialAccountRepository.delete(socialAccount);
	}

}
