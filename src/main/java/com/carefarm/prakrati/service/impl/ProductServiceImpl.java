package com.carefarm.prakrati.service.impl;

import static com.carefarm.prakrati.util.ImageType.PRODUCTS;
import static com.carefarm.prakrati.util.ProductState.ADDED_NEW;
import static java.time.temporal.ChronoUnit.MINUTES;

import java.math.BigDecimal;
import java.time.Clock;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.carefarm.prakrati.common.entity.Currency;
import com.carefarm.prakrati.common.entity.ProductStatus;
import com.carefarm.prakrati.common.entity.Rating;
import com.carefarm.prakrati.common.entity.Unit;
import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.entity.Attribute;
import com.carefarm.prakrati.entity.Category;
import com.carefarm.prakrati.entity.Group;
import com.carefarm.prakrati.entity.Image;
import com.carefarm.prakrati.entity.Price;
import com.carefarm.prakrati.entity.Product;
import com.carefarm.prakrati.entity.Review;
import com.carefarm.prakrati.entity.Tag;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.generators.UserNoGenerator;
import com.carefarm.prakrati.repository.ActivityRepository;
import com.carefarm.prakrati.repository.BidRepository;
import com.carefarm.prakrati.repository.CategoryRepository;
import com.carefarm.prakrati.repository.CurrencyRepository;
import com.carefarm.prakrati.repository.GrainCartRepository;
import com.carefarm.prakrati.repository.GroupRepository;
import com.carefarm.prakrati.repository.ProductNoRepository;
import com.carefarm.prakrati.repository.ProductRepository;
import com.carefarm.prakrati.repository.ProductStatusRepository;
import com.carefarm.prakrati.repository.RatingRepository;
import com.carefarm.prakrati.repository.TagRepository;
import com.carefarm.prakrati.repository.UnitRepository;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.repository.WishlistRepository;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.search.repository.ProductSearchRepository;
import com.carefarm.prakrati.security.PasswordManager;
import com.carefarm.prakrati.service.ProductService;
import com.carefarm.prakrati.service.helper.AmazonS3Helper;
import com.carefarm.prakrati.service.helper.ProductEventHelper;
import com.carefarm.prakrati.service.helper.ProductHelper;
import com.carefarm.prakrati.util.ImageCategory;
import com.carefarm.prakrati.util.ImageType;
import com.carefarm.prakrati.util.ProductState;
import com.carefarm.prakrati.util.UserType;
import com.carefarm.prakrati.web.bean.CategoryBean;
import com.carefarm.prakrati.web.bean.CurrencyBean;
import com.carefarm.prakrati.web.bean.ImageBean;
import com.carefarm.prakrati.web.bean.OwnerBean;
import com.carefarm.prakrati.web.bean.ProductBean;
import com.carefarm.prakrati.web.bean.ProductEventBean;
import com.carefarm.prakrati.web.bean.ProductStatusBean;
import com.carefarm.prakrati.web.bean.ReviewBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.bean.TagBean;
import com.carefarm.prakrati.web.bean.UnitBean;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.model.EnableBiddingForm;
import com.carefarm.prakrati.web.model.PriceUpdateForm;
import com.carefarm.prakrati.web.model.ProductAssignForm;
import com.carefarm.prakrati.web.model.ProductForm;
import com.carefarm.prakrati.web.model.ReviewForm;
import com.carefarm.prakrati.web.model.UpdateDescForm;

@Service
public class ProductServiceImpl extends PrakratiServiceImpl implements ProductService {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProductServiceImpl.class);

	@Value("${product.bid.enabled.time.period}")
	private int bidEnableTimePeriod;
	
	@Autowired
	private ProductNoRepository productNoRepository;
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private GroupRepository groupRepository;
		
	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private TagRepository tagRepository;
	
	@Autowired
	private ProductStatusRepository productStatusRepository;
	
	@Autowired
	private ProductEventHelper productEventHelper;

	@Autowired
	private ProductHelper productHelper;
	
	@Autowired
	private BidRepository bidRepository;
	
	@Autowired
	private RatingRepository ratingRepository;
	
	@Autowired
	private WishlistRepository wishlistRepository;
	
	@Autowired
	private GrainCartRepository grainCartRepository;
	
	@Autowired
	private ActivityRepository activityRepository;
	
	@Autowired
	private CurrencyRepository currencyRepository;
	
	@Autowired
	private UnitRepository unitRepository;
	
	@Autowired
	private ProductSearchRepository productSearchRepository;
	
	@Autowired
	private AmazonS3Helper amazonS3Helper;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Map<String, Object> findByKeyword(SearchRequest searchRequest) {
		Map<String, Object> data = new HashMap<>();
		
		Pageable pageable = createPageRequest(searchRequest);
		
		Long totalRecord = 0L;
		List<Product> products = null;
		if (Predicates.notNull.test(searchRequest.getUserNo())) {
			if (!searchRequest.isHistory()) {
				totalRecord = productRepository.countByUserNoAndStatusCodes(searchRequest.getUserNo(), productStatuses);
				products = productRepository.findByUserNoAndStatusCodes(searchRequest.getUserNo(), productStatuses, pageable);
			} else {
				totalRecord = productRepository.countByUserNoAndStatus(searchRequest.getUserNo(), ProductState.SOLD_OUT.getCode());
				products = productRepository.findByUserNoAndStatus(searchRequest.getUserNo(), ProductState.SOLD_OUT.getCode(), pageable);
			}
		} else {
			if (!searchRequest.isConsumerProducts()) {
				totalRecord = productRepository.countByStatus(ProductState.ADDED_NEW.getName());
				products = productRepository.findByStatus(ProductState.ADDED_NEW.getName(), pageable);
			} else {
				totalRecord = productRepository.countConsumerProducts(searchRequest.getCategoryId());
				products = productRepository.findConsumerProducts(searchRequest.getCategoryId(), pageable);
			}
		}
		
		List<ProductBean> productBeans = new ArrayList<>();
		for (Product product : products) {
			List<ImageBean> images = new ArrayList<>();
			product.getImages().forEach(image -> images.add(mapper.map(image, ImageBean.class)));
			
			ProductBean productBean = mapper.map(product, ProductBean.class);
			
			productBean.setStatus(mapper.map(product.getStatus(), ProductStatusBean.class));
			productHelper.populateHighestBid(productBean, product);
			
			Optional<Currency> biddingCurrencyOpt = Optional.ofNullable(product.getBiddingCurrency());
			biddingCurrencyOpt.ifPresent(bc -> productBean.setBiddingCurrency(mapper.map(bc, CurrencyBean.class)));
			
			Optional<Unit> biddingUnitOpt = Optional.ofNullable(product.getBiddingUnit());
			biddingUnitOpt.ifPresent(bu -> productBean.setBiddingUnit(mapper.map(bu, UnitBean.class)));
			
			productBean.setImages(images);
			product.getTags().forEach(t -> productBean.addTag(mapper.map(t, TagBean.class)));
			productBean.setOwner(mapper.map(product.getOwner(), OwnerBean.class));
			productBean.setCreatedBy(mapper.map(product.getCreatedBy(), UserBean.class));
			productBeans.add(productBean);
		}
		data.put("recordsTotal", totalRecord);
		data.put("recordsFiltered", totalRecord);
		data.put("data", productBeans);
		return data;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void addProduct(ProductForm form, List<MultipartFile> files, MultipartFile ownerImage, User createdBy) throws ServiceException {
		LOGGER.info("START: Adding new product " + form.getName());
		Clock clock = Environment.clock();
		
		Product product = mapper.map(form, Product.class);
        product.setUniqueId(UUID.randomUUID().toString());
        product.setProductNo(productNoRepository.findNextProductNo());
        product.setMocked(Boolean.FALSE);
        product.setCreatedBy(createdBy);
        product.setDateCreated(Date.from(clock.instant()));
        
        ProductStatus productStatus = productStatusRepository.findByCode(ADDED_NEW.getCode());
        product.setStatus(productStatus);
        
        form.getCategoryIds().forEach(categoryId -> {
        	product.addCategory(categoryRepository.findByUniqueId(categoryId));
        });
        
        form.getTags().forEach(tagBean -> {
        	Tag tag = mapper.map(tagBean, Tag.class);
        	if (Predicates.isNull.test(tag.getId())) {
        		tag.setCreatedBy(createdBy);
        		tag.setDateCreated(Date.from(clock.instant()));
        		tagRepository.save(tag);
        	}
        	product.addTag(tag);
        });
     
        form.getAttributes()
        	.forEach(attributeBean -> product.addAttribute(mapper.map(attributeBean, Attribute.class)));
        
        if (form.isConsumerProduct()) {
        	product.setOwner(createdBy);
        	product.setAddress(createdBy.getAddress());
        } else {
	        if (Predicates.isNull.test(form.getOwnerId()) && 
	        		Predicates.isNull.test(form.getOwner())) {
	        	product.setOwner(createdBy);
	        	product.setAddress(createdBy.getAddress());
	        } else {
	        	if (Predicates.notNull.test(form.getOwnerId())) {
	        		product.setOwner(userRepository.findByUserNo(form.getOwnerId()));
	        	} else if (Predicates.notNull.test(form.getOwner())){
	        		Group group = groupRepository.findOne(form.getOwner().getGroupId());
	        		User owner = mapper.map(form.getOwner(), User.class);
	        		
	        		UserType userType = UserType.getUserType(group.getName());
	        		owner.setUserType(userType);
	        		owner.setUserNo(UserNoGenerator.generateUserNo());
	        		owner.setPassword(PasswordManager.encryptPasswd(DEFAULT_PASSWD));
	        		owner.setGroup(group);
	        		
	        		if (userType.equals(UserType.FARMER)) {
	        			owner.setMobileNo(form.getOwner().getPhoneNo());
	        		}
	        		
	        		if (Predicates.notNull.test(ownerImage)) {
	        			owner.setDefaultImage(amazonS3Helper.handleImage(ownerImage, ImageType.USERS, owner));
	            	} else {
	            		owner.setDefaultImage(imageRepository.findDefaultImageForCategory(ImageCategory.PRODUCT));
	            	}
	        		userRepository.save(owner);
	                product.setOwner(owner);
	                product.setAddress(owner.getAddress());
	        	}
	        }
        }
        
        productRepository.save(product);
        
        final Set<Image> productImages = new HashSet<>();
        files.forEach(file -> {
        	productImages.add(amazonS3Helper.handleImage(file, PRODUCTS, product.getUniqueId()));
        });
        
        product.setImages(productImages);
        
        if (productImages.size() > 0) {
	        Iterator<Image> iter = productImages.iterator();
	        if(iter.hasNext()) {
	        	product.setDefaultImage(iter.next());
	        }
        } else {
        	product.setDefaultImage(imageRepository.findOne(PRODUCTS.getDefaultImageId()));
        }
        
        productHelper.publishIndexEvent(product, EventType.UPDATE);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ProductBean findProductDetail(String uniqueId,
			Map<String, Object> params) {
		LOGGER.info("START: Getting product detail " + uniqueId);
		Product product = productRepository.findByUniqueId(uniqueId);
		ProductBean productBean = mapper.map(product, ProductBean.class);
		
		productBean.setStatus(mapper.map(product.getStatus(), ProductStatusBean.class));
		product.getCategories().forEach(c -> { 
			productBean.addCategoryId(c.getUniqueId());
			productBean.addCategory(mapper.map(c, CategoryBean.class));
		});
		productBean.setTotalRating(BigDecimal.valueOf(product.totalRating()));
		productBean.setTotalReviews(product.totalReviews());
		
		User owner = product.getOwner();
		OwnerBean ownerBean = mapper.map(owner, OwnerBean.class);
		if (Predicates.notNull.test(owner.getDefaultImage())) {
			ownerBean.setPhoto(owner.getDefaultImage().getMediumFilepath());
		}
		
		productBean.setOwner(ownerBean);	

		Optional.ofNullable(product.getAssignedBy())
			.ifPresent(assignedBy -> 
				productBean.setAssignedBy(mapper.map(assignedBy, UserBean.class)));
		
		Optional.ofNullable(product.getAssignedTo())
			.ifPresent(assignedTo -> 
				productBean.setAssignedTo(mapper.map(assignedTo, UserBean.class)));
		
		List<ImageBean> images = new ArrayList<>();
		product.getImages().forEach(image -> images.add(mapper.map(image, ImageBean.class)));
		productBean.setImages(images);
		
		List<TagBean> tags = new ArrayList<>();
		product.getTags().forEach(tag -> tags.add(mapper.map(tag, TagBean.class)));
		productBean.setTags(tags);
		
		List<ReviewBean> reviews = new ArrayList<>();
		product.getReviews().forEach(review -> reviews.add(mapper.map(review, ReviewBean.class)));
		productBean.setReviews(reviews);
		
		productBean.setTotalWishlists(wishlistRepository.countByProductId(product.getUniqueId()));
		productBean.setTotalGrainCarts(grainCartRepository.countByProductId(product.getUniqueId()));
		productBean.setTotalVisits(activityRepository.countByProductId(product.getUniqueId()));
		
		productBean.setEvents(productEventHelper.convertEntitiesToBeans(product.getEvents(), ProductEventBean.class));
		return productBean;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(ProductBean productBean, User currentUser) throws ServiceException {
		LOGGER.info("Updating product details");
		Product product = productRepository.findOne(productBean.getId());
		productHelper.convertBeanToEntity(product, productBean);
		
		
		Set<Category> categories = new HashSet<>();
		productBean.getCategoryIds().forEach(categoryId -> {
			categories.add(categoryRepository.findByUniqueId(categoryId));
		});
		
		product.setCategories(categories);
		
		Set<Tag> tags = new HashSet<>();
		productBean.getTags().forEach(tagBean -> {
        	Tag tag = mapper.map(tagBean, Tag.class);
        	if (Predicates.isNull.test(tag.getId())) {
        		tag.setCreatedBy(currentUser);
        		tag.setDateCreated(Date.from(Environment.clock().instant()));
        		tagRepository.save(tag);
        	}
        	tags.add(tag);
        });
        product.setTags(tags);
		
        Collection<Attribute> attributes = new ArrayList<>();
        productBean.getAttributes().forEach(bean -> attributes.add(mapper.map(bean, Attribute.class)));
        product.setAttributes(attributes);
        
		product.setModifiedBy(currentUser);
		product.setDateModified(Calendar.getInstance().getTime());
		
		productHelper.publishIndexEvent(product, EventType.UPDATE);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(String uniqueId, User currentUser) {
		Product product = productRepository.findByUniqueId(uniqueId);
		product.setArchived(Boolean.TRUE);
		product.setModifiedBy(currentUser);
		product.setDateModified(Date.from(Environment.clock().instant()));
		
		productHelper.publishIndexEvent(product, EventType.DELETE);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public List<ProductBean> findBiddableProducts(Long stateId, String uniqueId, Pageable pageable) {
		List<ProductBean> biddableProductBeans = new ArrayList<>();
		List<Product> biddableProducts = new ArrayList<>();
		if (stateId == -1) {
			biddableProducts = productRepository.findBiddableByCategoryId(uniqueId, pageable); 
		} else {
			biddableProducts = productRepository.findBiddableByStateIdAndCategoryId(stateId, uniqueId, pageable);
		}
		
		biddableProducts
			.forEach(p -> {
				ProductBean product = productHelper.mapBiddableProduct(p);
				biddableProductBeans.add(product);
			});
			
		return biddableProductBeans;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void enableBidding(EnableBiddingForm form, User currentUser) {
		Product product = productRepository.findByUniqueId(form.getUniqueId());
		product.setInitialBidAmount(form.getInitialBidAmount());
		product.setBidEnabled(Boolean.TRUE);
		product.setBiddingCurrency(mapper.map(form.getBiddingCurrency(), Currency.class));
		product.setBiddingUnit(mapper.map(form.getBiddingUnit(), Unit.class));
		ZonedDateTime zdt = ZonedDateTime.now(Environment.zoneId())
				.plus(bidEnableTimePeriod, MINUTES);
		product.setEnableTillDate(Date.from(zdt.toInstant()));
		product.setModifiedBy(currentUser);
		product.setDateModified(Date.from(Environment.clock().instant()));

		productHelper.sendBiddableProduct(product);
		
		productHelper.publishIndexEvent(product, EventType.UPDATE);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void submitReview(ReviewForm form, User currentUser) {
		Product product = productRepository.findByUniqueId(form.getUniqueId());
		Rating rating = ratingRepository.findByValue(form.getRating());
		
		Review review = mapper.map(form, Review.class);
		review.setRating(rating);
		review.setValid(Boolean.TRUE);
		review.setReviewBy(currentUser);
		review.setReviewDate(Date.from(Environment.clock().instant()));	
		product.addReview(review);
		
		Double finalRating = (product.getRating() * product.getTotalReviews() + rating.getValue()) / (product.getTotalReviews() + 1); 
		product.setRating(finalRating);
		product.incrementTotalReviews();
		
		productHelper.publishIndexEvent(product, EventType.UPDATE);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void assign(ProductAssignForm form, User currentUser) {
		Product product = productRepository.findByUniqueId(form.getUniqueId());
		product.setAssignedBy(currentUser);
		product.setAssignedTo(userRepository.findByUserNo(form.getAssignTo().getUserNo()));
		product.setDateAssigned(Date.from(Environment.clock().instant()));
		product.setComments(form.getComments());
		product.setModifiedBy(currentUser);
		product.setDateModified(Date.from(Environment.clock().instant()));
		
		productHelper.createAndSendMessages(product, ProductState.ASSIGNED.getCode());
		
		productHelper.publishIndexEvent(product, EventType.UPDATE);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void markAsSoldOut(String uniqueId, User currentUser) {
		Product product = productRepository.findByUniqueId(uniqueId);
		product.setStatus(productStatusRepository.findByCode(ProductState.SOLD_OUT.getCode()));
		product.setModifiedBy(currentUser);
		product.setDateModified(Date.from(Environment.clock().instant()));
		
		productHelper.createAndSendMessages(product, ProductState.SOLD_OUT.getCode());
		
		productHelper.publishIndexEvent(product, EventType.UPDATE);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void updatePrice(PriceUpdateForm form, User currentUser) {
		Product product = productRepository.findByUniqueId(form.getUniqueId());
		Currency currency = currencyRepository.findOne(form.getPriceCurrencyId());
		Unit unit = unitRepository.findOne(form.getPriceUnitId());
		Price latestPrice = new Price(form.getPrice(), currency, unit);
		latestPrice.setPriceDate(Date.from(Environment.clock().instant()));
		product.setLastPrice(product.getLatestPrice());
		product.setLatestPrice(latestPrice);
		product.addPrice(latestPrice);
		product.setModifiedBy(currentUser);
		product.setDateModified(Date.from(Environment.clock().instant()));
		
		productHelper.publishIndexEvent(product, EventType.UPDATE);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void uploadImages(List<MultipartFile> images, String productId,
			User currentUser) {
		Product product = productRepository.findByUniqueId(productId);
		images.forEach(file -> {
        	product.addImage(amazonS3Helper.handleImage(file, PRODUCTS, productId));
        });
		product.setModifiedBy(currentUser);
		product.setDateModified(Date.from(Environment.clock().instant()));
		
		productHelper.publishIndexEvent(product, EventType.UPDATE);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void setAsDefault(String productId, Long imageId, User currentUser) {
		Image defaultImage = imageRepository.findOne(imageId);
		
		Product product = productRepository.findByUniqueId(productId);
		product.getImages().forEach(img -> img.setDefault(Boolean.FALSE));
		
		defaultImage.setDefault(Boolean.TRUE);
		product.setDefaultImage(defaultImage);
		product.setModifiedBy(currentUser);
		product.setDateModified(Date.from(Environment.clock().instant()));
		
		productHelper.publishIndexEvent(product, EventType.UPDATE);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void addUpdateDesc(UpdateDescForm form, User currentUser) {
		Product product = productRepository.findByUniqueId(form.getProductId());
		product.setShortDesc(form.getShortDesc());
		product.setDesc(form.getDesc());
		
		productHelper.publishIndexEvent(product, EventType.UPDATE);
	}

}
