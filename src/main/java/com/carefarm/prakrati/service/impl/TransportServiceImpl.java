package com.carefarm.prakrati.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.delivery.entity.Delivery;
import com.carefarm.prakrati.delivery.entity.Transport;
import com.carefarm.prakrati.delivery.repository.DeliveryRepository;
import com.carefarm.prakrati.entity.AddressEntity;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.repository.CompanyRepository;
import com.carefarm.prakrati.repository.PermitRepository;
import com.carefarm.prakrati.repository.TransportRepository;
import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.search.repository.TransportSearchRepository;
import com.carefarm.prakrati.service.TransportService;
import com.carefarm.prakrati.service.helper.TransportHelper;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.bean.TransportBean;
import com.carefarm.prakrati.web.model.AssignDeliveryForm;
import com.carefarm.prakrati.web.model.DriverAssignForm;
import com.carefarm.prakrati.web.model.TransportForm;

@Service
public class TransportServiceImpl extends PrakratiServiceImpl implements TransportService {

	@Autowired
	private TransportSearchRepository transportSearchRepository;
	
	@Autowired
	private TransportRepository transportRepository;
	
	@Autowired
	private CompanyRepository companyRepository;
	
	@Autowired
	private PermitRepository permitRepository;
	
	@Autowired
	private DeliveryRepository deliveryRepository;
	
	@Autowired
	private TransportHelper transportHelper;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public Map<String, Object> findByKeyword(SearchRequest searchRequest) {
		Map<String, Object> data = new HashMap<>();
		PageRequest pageRequest = createPageRequest(searchRequest);

		SearchResult<Long> result = transportSearchRepository.findByKeywordContains(searchRequest, pageRequest);
		long totalRecord = result.getTotalHits();
		Iterable<Transport> transports = transportRepository.findAll(result.getResults());
		
		List<TransportBean> transportBeans = new ArrayList<>();
		for (Transport transport : transports) {
			TransportBean transportBean = mapper.map(transport, TransportBean.class);
			transportHelper.extractBasicFields(transport, transportBean);
			transportBeans.add(transportBean);
		}
		
		data.put("recordsTotal", totalRecord);
		data.put("recordsFiltered", totalRecord);
		data.put("data", transportBeans);
		return data;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(String uniqueId, User currentUser) {
		Transport transport = transportRepository.findByUniqueId(uniqueId);
		transport.setArchived(Boolean.TRUE);
		transport.setModifiedBy(currentUser);
		transport.setDateModified(Date.from(Environment.clock().instant()));
		transportHelper.publishIndexEvent(transport, EventType.DELETE);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(TransportForm form, User currentUser)
			throws ServiceException {
		Transport transport = mapper.map(form, Transport.class);
		transport.setUniqueId(UUID.randomUUID().toString());
		form.getSelectedPermitIds().forEach(id -> transport.addPermit(permitRepository.findOne(id)));
		transport.setCompany(companyRepository.findByUniqueId(form.getCompanyId()));
		transport.setCreatedBy(currentUser);
		transport.setDateCreated(Date.from(Environment.clock().instant()));
		transportRepository.save(transport);
		transportHelper.publishIndexEvent(transport, EventType.INSERT);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public TransportBean findDetail(String uniqueId) {
		Transport entity = transportRepository.findByUniqueId(uniqueId);
		TransportBean bean = mapper.map(entity,TransportBean.class);
		transportHelper.extractBasicFields(entity, bean);
		return bean;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(TransportBean bean, User modifiedBy)
			throws ServiceException {
		Transport entity = transportRepository.findByUniqueId(bean.getUniqueId());
		transportHelper.convertBeanToEntity(entity, bean);
		transportHelper.publishIndexEvent(entity, EventType.UPDATE);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void assignDriver(DriverAssignForm form, User currentUser) {
		Transport transport = transportRepository.findByUniqueId(form.getUniqueId());
		transport.setDriver(userRepository.findByUserNo(form.getUserNo()));
		transport.setAssignedBy(currentUser);
		transport.setDateAssigned(Date.from(Environment.clock().instant()));
		transport.setComments(form.getComments());
		transport.setModifiedBy(currentUser);
		transport.setDateModified(Date.from(Environment.clock().instant()));
		transportHelper.publishIndexEvent(transport, EventType.UPDATE);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void assignDelivery(AssignDeliveryForm form, User currentUser) {
		Transport transport = transportRepository.findByUniqueId(form.getUniqueId());
		Delivery delivery = mapper.map(form, Delivery.class);
		delivery.setPickupDate(super.getParseDate(form.getPickupDate()));
		delivery.setDeliveryDate(super.getParseDate(form.getDeliveryDate()));
		delivery.setPickupAddress(mapper.map(form.getPickupAddress(), AddressEntity.class));
		delivery.setDeliveryAddress(mapper.map(form.getDeliveryAddress(), AddressEntity.class));
		delivery.setTransport(transport);
		delivery.setDriver(transport.getDriver());
		delivery.setCreatedBy(currentUser);
		delivery.setDateCreated(Date.from(Environment.clock().instant()));
		deliveryRepository.save(delivery);
		transport.addDelivery(delivery);
	}

}
