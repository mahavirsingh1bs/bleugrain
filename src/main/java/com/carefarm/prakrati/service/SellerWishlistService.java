package com.carefarm.prakrati.service;

import java.util.Map;

import org.springframework.data.domain.PageRequest;

public interface SellerWishlistService {

	Map<String, Object> findByKeyword(String keyword, PageRequest pageRequest,
			Map<String, Object> params);

}
