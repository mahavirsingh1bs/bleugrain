package com.carefarm.prakrati.service;

import com.carefarm.prakrati.entity.User;

public interface UserContext {
	User getCurrentUser();
	void setCurrentUser(User user);
}
