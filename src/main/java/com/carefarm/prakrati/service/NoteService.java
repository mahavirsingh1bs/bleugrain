package com.carefarm.prakrati.service;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.web.model.NoteForm;

public interface NoteService {

	void addNote(NoteForm noteForm, String userNo, User currentUser);
	
	void delete(String uniqueId, User currentUser);
	
}
