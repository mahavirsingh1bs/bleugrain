package com.carefarm.prakrati.service;

import java.util.List;

import com.carefarm.prakrati.web.bean.TagBean;

public interface TagService {
	
	List<TagBean> findByMatchingKeywords(String keywords);
	
}
