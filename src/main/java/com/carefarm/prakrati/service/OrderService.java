package com.carefarm.prakrati.service;

import java.util.List;
import java.util.Map;

import com.carefarm.prakrati.entity.Order;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.web.bean.OrderBean;
import com.carefarm.prakrati.web.bean.OrderDetailsBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.OrderAssignForm;
import com.carefarm.prakrati.web.model.OrderDispatchForm;
import com.carefarm.prakrati.web.model.OrderUpdateForm;

public interface OrderService {
	
	List<Order> findOrdersByUser(Long userId) throws ServiceException;
	
	Map<String, Object> findByKeyword(SearchRequest searchRequest);
	
	void processOrder(OrderDetailsBean orderDetails, User orderedBy);
	
	OrderBean findDetail(String orderNo);
	
	void delete(String uniqueId);

	void updateOrder(OrderUpdateForm form, User currentUser);

	void assignOrder(OrderAssignForm form, User currentUser);

	void dispatchOrder(OrderDispatchForm form, User currentUser);

	void deliverOrder(String orderNo, User currentUser);

	void shipOrder(String orderNo, User currentUser);
}
