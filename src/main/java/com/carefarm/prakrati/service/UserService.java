package com.carefarm.prakrati.service;

import java.util.List;
import java.util.Map;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.util.UserType;
import com.carefarm.prakrati.vo.ImageVO;
import com.carefarm.prakrati.web.bean.ImageBean;
import com.carefarm.prakrati.web.bean.OrderBean;
import com.carefarm.prakrati.web.bean.ReviewBean;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.model.BankAccountForm;
import com.carefarm.prakrati.web.model.ChangePasswdForm;
import com.carefarm.prakrati.web.model.CreditCardForm;

public interface UserService {
	
	UserBean findDetail(String userNo);
	
	void forgotPassword(String emailAddress, String contextPath);
	
	void resetPasswd(String newPasswd, String resetCode) throws ServiceException;
	
	Boolean validateCode(String resetCode);
	
	void changePasswd(ChangePasswdForm form) throws ServiceException;
	
	List<UserBean> findByMatchingKeywords(String keywords);
	
	List<UserBean> findByMatchingKeywords(String keywords, UserType... userTypes);
	
	List<UserBean> findByMatchingKeywords(String keywords, String companyId, UserType... userTypes);
	
	List<ReviewBean> findReviews(String uniqueId);
	
	Map<String, Object> findSettings(String userNo);
	
	List<OrderBean> findOrders(String uniqueId);
	
	void addCreditCard(CreditCardForm creditCard, String userNo, User currentUser);
	
	void addBankAccount(BankAccountForm bankAccount, String userNo, User currentUser);
	
	Boolean verifyContactNo(String uniqueCode, String oneTimePasswd);
	
	boolean verifyEmailAddress(String verifCode);
	
	ImageBean changeProfilePicture(ImageVO userImage, String userId, User modifiedBy);
	
	Map<String, Object> findCountrySettings(String userNo);

	void update(UserBean userBean, String userNo, User currentUser);
	
	UserBean findCurrentUser(Long userId);
}
