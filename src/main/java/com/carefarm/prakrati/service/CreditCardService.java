package com.carefarm.prakrati.service;

import java.util.Map;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.payment.service.BillingDetailsService;
import com.carefarm.prakrati.web.bean.CreditCardBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.CreditCardForm;

public interface CreditCardService extends BillingDetailsService {
	Map<String, Object> findByKeyword(SearchRequest request);

	void delete(String uniqueId);

	CreditCardBean findDetail(String uniqueId);

	void create(CreditCardForm form, User createdBy) throws ServiceException;
	
	void update(CreditCardBean creditCardBean, User modifiedBy)  throws ServiceException;

	CreditCardBean create(CreditCardForm form, Map<String, Object> params, User user) throws ServiceException;
	
}
