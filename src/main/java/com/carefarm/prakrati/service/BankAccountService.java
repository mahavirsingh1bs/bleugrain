package com.carefarm.prakrati.service;

import java.util.List;
import java.util.Map;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.payment.service.BillingDetailsService;
import com.carefarm.prakrati.web.bean.BankAccountBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.BankAccountForm;

public interface BankAccountService extends BillingDetailsService {
	
	Map<String, Object> findByKeyword(SearchRequest request);

	List<BankAccountBean> findAllActiveOnes(Map<String, Object> params);
	
	BankAccountBean findDetail(String uniqueId);
	
	void update(BankAccountBean bankAccountBean, User currentUser) throws ServiceException;
	
	void delete(String uniqueId);

	void create(BankAccountForm form, User createdBy) throws ServiceException;
	
	BankAccountBean create(BankAccountForm form, Map<String, Object> params, User createdBy) throws ServiceException;
	
}
