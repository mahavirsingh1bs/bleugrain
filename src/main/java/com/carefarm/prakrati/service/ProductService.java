package com.carefarm.prakrati.service;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.web.bean.ProductBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.EnableBiddingForm;
import com.carefarm.prakrati.web.model.PriceUpdateForm;
import com.carefarm.prakrati.web.model.ProductAssignForm;
import com.carefarm.prakrati.web.model.ProductForm;
import com.carefarm.prakrati.web.model.ReviewForm;
import com.carefarm.prakrati.web.model.UpdateDescForm;

public interface ProductService {

	Map<String, Object> findByKeyword(SearchRequest searchRequest);
	
	void addProduct(ProductForm form, List<MultipartFile> files, MultipartFile ownerImage, User createdBy) throws ServiceException;
	
	ProductBean findProductDetail(String uniqueId, Map<String, Object> params);
	
	void update(ProductBean productBean, User modifiedBy) throws ServiceException;
	
	void delete(String uniqueId, User currentUser);

	List<ProductBean> findBiddableProducts(Long stateId, String uniqueId, Pageable pageable);

	void enableBidding(EnableBiddingForm form, User currentUser);

	void submitReview(ReviewForm form, User currentUser);

	void assign(ProductAssignForm form, User currentUser);

	void markAsSoldOut(String uniqueId, User currentUser);

	void updatePrice(PriceUpdateForm form, User currentUser);

	void uploadImages(List<MultipartFile> images, String productId, User currentUser);

	void setAsDefault(String productId, Long imageId, User currentUser);

	void addUpdateDesc(UpdateDescForm form, User currentUser);
	
}
