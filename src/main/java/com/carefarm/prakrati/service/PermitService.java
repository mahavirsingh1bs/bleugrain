package com.carefarm.prakrati.service;

import java.util.List;

import com.carefarm.prakrati.web.bean.PermitBean;

public interface PermitService extends PrakratiService {
	List<PermitBean> findAll();
}
