package com.carefarm.prakrati.service;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.web.bean.GrainCartBean;
import com.carefarm.prakrati.web.bean.ProductBean;

public interface GrainCartService {

	void addToCart(String uniqueId, User user);
	
	void addToConsumerCart(ProductBean productBean, User user);

	GrainCartBean findCartDetails(User user);

	void removeFromCart(String uniqueId, User user);
	
	void removeFromConsumerCart(String uniqueId, User user);

}
