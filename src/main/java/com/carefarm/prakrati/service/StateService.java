package com.carefarm.prakrati.service;

import java.util.List;
import java.util.Map;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.bean.StateBean;
import com.carefarm.prakrati.web.model.StateForm;

public interface StateService {
	List<StateBean> findAll();
	
	Map<String, Object> findByKeyword(SearchRequest searchRequest);
	
	void create(StateForm form, User user) throws ServiceException;
	
	StateBean findDetails(long stateId, Object object);

	void update(StateBean stateBean, User modifiedBy) throws ServiceException;
	
	void delete(String uniqueId);
	
}
