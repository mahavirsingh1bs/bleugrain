package com.carefarm.prakrati.service;

import java.util.Map;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.web.model.VerifyMobileNoForm;

public interface ProfileService {

	Map<String, Object> getDashboardDetails(User currentUser);
	
	void sendMobileVerificationCode(String userNo, User currentUser);
	
	void sendEmailVerificationLink(String userNo, User currentUser);

	void verifyOneTimePassword(VerifyMobileNoForm verifyMobileNoForm, User currentUser);

}
