package com.carefarm.prakrati.service;

import java.util.List;

import com.carefarm.prakrati.web.bean.ProductStatusBean;

public interface ProductStatusService {

	List<ProductStatusBean> findAll();
	
}
