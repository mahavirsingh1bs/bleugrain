package com.carefarm.prakrati.service;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.web.model.UpcomingEventForm;

public interface UpcomingEventService {

	void addUpcomingEvent(UpcomingEventForm upcomingEventForm, String userNo, User currentUser);
	
	void delete(String uniqueId, User currentUser);
	
}
