package com.carefarm.prakrati.service;

import java.util.List;

import com.carefarm.prakrati.web.bean.CurrencyBean;

public interface CurrencyService {
	List<CurrencyBean> findAll();
}
