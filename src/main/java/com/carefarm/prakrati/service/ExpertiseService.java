package com.carefarm.prakrati.service;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.web.bean.ExpertiseBean;
import com.carefarm.prakrati.web.model.ExpertiseForm;

public interface ExpertiseService {

	void addExpertise(ExpertiseForm expertiseForm, String userNo, User currentUser);

	ExpertiseBean find(String uniqueId);

	void update(ExpertiseBean bean, User currentUser);

	void remove(String uniqueId, User currentUser);

}
