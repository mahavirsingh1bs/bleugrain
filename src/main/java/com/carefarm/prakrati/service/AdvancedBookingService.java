package com.carefarm.prakrati.service;

import com.carefarm.prakrati.web.model.AdvancedBookingForm;

public interface AdvancedBookingService {
	
	void advanceBooking(AdvancedBookingForm form);
	
}
