package com.carefarm.prakrati.service;

import java.util.List;
import java.util.Map;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.web.bean.BankBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.BankForm;

public interface BankService {
	
	List<BankBean> findAll();
	
	Map<String, Object> findByKeyword(SearchRequest searchRequest);
	
	void create(BankForm form, User user) throws ServiceException;
	
	BankBean findDetails(long bankId, Object object);

	void update(BankBean bankBean, User modifiedBy) throws ServiceException;
	
	void delete(String uniqueId);

}
