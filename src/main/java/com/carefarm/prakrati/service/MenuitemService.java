package com.carefarm.prakrati.service;

import java.util.List;
import java.util.Map;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.web.bean.MenuitemBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.MenuitemForm;

public interface MenuitemService extends PrakratiService {
	
	Map<String, Object> findByKeyword(SearchRequest searchRequest);

	List<MenuitemBean> findMainMenuitems();
	
	void create(MenuitemForm form, User user) throws ServiceException;
	
	MenuitemBean findDetails(String uniqueId);

	void update(MenuitemBean menuitemBean, User modifiedBy) throws ServiceException;
	
	void delete(String uniqueId);
	
}
