package com.carefarm.prakrati.service;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.web.model.DiscussionForm;

public interface DiscussionService {
	
	void addDiscussion(DiscussionForm discussionForm, String userNo, User currentUser);
	
}
