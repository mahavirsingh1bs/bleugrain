package com.carefarm.prakrati.service;

import java.util.Map;

import org.springframework.data.domain.Pageable;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.web.bean.BranchBean;
import com.carefarm.prakrati.web.model.BranchForm;

public interface BranchService {

	Map<String, Object> findByKeyword(String keyword, Pageable pageable,
			Map<String, Object> params);

	void delete(String uniqueId);

	void create(BranchForm form, User user) throws ServiceException;

	BranchBean findDetail(String uniqueId);

	void update(BranchBean branchBean) throws ServiceException;

}
