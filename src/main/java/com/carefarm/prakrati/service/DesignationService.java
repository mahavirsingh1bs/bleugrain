package com.carefarm.prakrati.service;

import java.util.List;
import java.util.Map;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.web.bean.DesignationBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.DesignationForm;

public interface DesignationService {
	List<DesignationBean> findAll();
	
	Map<String, Object> findByKeyword(SearchRequest searchRequest);

	void create(DesignationForm form, User user) throws ServiceException;
	
	DesignationBean findDetails(long designationId, Object object);

	void update(DesignationBean designationBean, User modifiedBy) throws ServiceException;
	
	void delete(String uniqueId);
}
