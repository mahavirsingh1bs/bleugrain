package com.carefarm.prakrati.service;

import java.util.List;
import java.util.Map;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.web.bean.CountryBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.CountryForm;

public interface CountryService {
	List<CountryBean> findAll();
	
	Map<String, Object> findByKeyword(SearchRequest searchRequest);

	void create(CountryForm form, User user) throws ServiceException;
	
	CountryBean findDetails(long countryId, Object object);

	void update(CountryBean countryBean, User modifiedBy) throws ServiceException;
	
	void delete(String uniqueId);
}
