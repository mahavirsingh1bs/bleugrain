package com.carefarm.prakrati.service;

import java.util.List;
import java.util.Map;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.web.bean.GroupBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.GroupForm;

public interface GroupService {
	List<GroupBean> findAll();
	
	Map<String, Object> findByKeyword(SearchRequest searchRequest);
	
	void create(GroupForm form, User user) throws ServiceException;
	
	void delete(String uniqueId);
	
	GroupBean findDetails(long groupId, Object object);

	void update(GroupBean groupBean, User modifiedBy) throws ServiceException;
	
	List<GroupBean> findByMatchingKeywords(String keywords);
	
}
