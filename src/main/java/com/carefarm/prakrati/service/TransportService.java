package com.carefarm.prakrati.service;

import java.util.Map;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.bean.TransportBean;
import com.carefarm.prakrati.web.model.AssignDeliveryForm;
import com.carefarm.prakrati.web.model.DriverAssignForm;
import com.carefarm.prakrati.web.model.TransportForm;

public interface TransportService {
	Map<String, Object> findByKeyword(SearchRequest searchRequest);
	void delete(String uniqueId, User currentUser);
	void create(TransportForm form, User createdBy) throws ServiceException;
	TransportBean findDetail(String uniqueId);
	void update(TransportBean transportBean, User modifiedBy) throws ServiceException;
	void assignDriver(DriverAssignForm form, User currentUser);
	void assignDelivery(AssignDeliveryForm form, User currentUser);
}
