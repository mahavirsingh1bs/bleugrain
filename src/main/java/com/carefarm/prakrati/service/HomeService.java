package com.carefarm.prakrati.service;

import java.util.Map;

public interface HomeService extends PrakratiService {
	
	Map<String, Object> getApplicationDetails();
}
