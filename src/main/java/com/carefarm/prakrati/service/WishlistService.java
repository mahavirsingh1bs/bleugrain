package com.carefarm.prakrati.service;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.web.bean.WishlistBean;

public interface WishlistService {

	WishlistBean findWishlist(User user);

	void addProductToWishlist(String uniqueId, User user);

	void removeProductFromWishlist(String uniqueId, User user);

	void addDemandToWishlist(String uniqueId, User user);

	void removeDemandFromWishlist(String uniqueId, User user);

}
