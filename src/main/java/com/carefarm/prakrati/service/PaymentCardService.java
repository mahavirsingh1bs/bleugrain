package com.carefarm.prakrati.service;

import java.util.List;
import java.util.Map;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.web.bean.PaymentCardBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.PaymentCardForm;

public interface PaymentCardService {
	List<PaymentCardBean> findAll();
	
	Map<String, Object> findByKeyword(SearchRequest searchRequest);
	
	void create(PaymentCardForm form, User user) throws ServiceException;
	
	PaymentCardBean findDetails(long paymentMethodId, Object object);

	void update(PaymentCardBean paymentMethodBean, User modifiedBy) throws ServiceException;

	void delete(String uniqueId);
	
}
