package com.carefarm.prakrati.service;

import java.util.List;

import com.carefarm.prakrati.web.bean.UnitBean;

public interface UnitService {
	List<UnitBean> findAll();
}
