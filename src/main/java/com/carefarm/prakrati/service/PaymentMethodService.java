package com.carefarm.prakrati.service;

import java.util.List;
import java.util.Map;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.web.bean.PaymentMethodBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.PaymentMethodForm;

public interface PaymentMethodService {

	List<PaymentMethodBean> findAll();
	
	Map<String, Object> findByKeyword(SearchRequest searchRequest);
	
	void create(PaymentMethodForm form, User user) throws ServiceException;
	
	PaymentMethodBean findDetails(long paymentMethodId, Object object);

	void update(PaymentMethodBean paymentMethodBean, User modifiedBy) throws ServiceException;
	
	void delete(String uniqueId);

}
