package com.carefarm.prakrati.service;

import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.web.bean.DemandBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.DemandAssignForm;
import com.carefarm.prakrati.web.model.DemandForm;
import com.carefarm.prakrati.web.model.PriceUpdateForm;

public interface DemandService {
	Map<String, Object> findByKeyword(SearchRequest searchRequest);
	
	void delete(String uniqueId, User currentUser);

	void create(DemandForm form, List<MultipartFile> files, User createdBy) throws ServiceException;

	DemandBean findDetail(String uniqueId);

	void update(DemandBean demandBean, User modifiedBy) throws ServiceException;

	void assign(DemandAssignForm form, User currentUser);

	void markAsFulfilled(String uniqueId, User currentUser);

	void updatePrice(PriceUpdateForm form, User currentUser);
}
