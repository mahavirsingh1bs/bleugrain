package com.carefarm.prakrati.service;

import java.util.List;
import java.util.Map;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.web.bean.DepartmentBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.DepartmentForm;

public interface DepartmentService {
	List<DepartmentBean> findAll();
	
	Map<String, Object> findByKeyword(SearchRequest searchRequest);

	void create(DepartmentForm form, User user) throws ServiceException;
	
	DepartmentBean findDetails(long departmentId, Object object);

	void update(DepartmentBean departmentBean, User modifiedBy) throws ServiceException;
	
	void delete(String uniqueId);
}
