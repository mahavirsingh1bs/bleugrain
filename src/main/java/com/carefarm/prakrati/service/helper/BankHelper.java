package com.carefarm.prakrati.service.helper;

import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.Bank;
import com.carefarm.prakrati.search.events.BankEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.web.bean.BankBean;

@Component
public class BankHelper extends AbstractHelper<BankBean, Bank> {

	public void convertBeanToEntity(Bank bank, BankBean bankBean) {
		bank.setName(bankBean.getName());
	}
	
	@Override
	protected PrakratiEvent<Iterable<Bank>> createEvent(Iterable<Bank> entities,
			EventType event) {
		return new BankEvent(entities, event);
	}
	
}
