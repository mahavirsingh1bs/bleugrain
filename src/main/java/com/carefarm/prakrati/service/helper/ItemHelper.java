package com.carefarm.prakrati.service.helper;

import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.Item;
import com.carefarm.prakrati.web.bean.ImageBean;
import com.carefarm.prakrati.web.bean.ItemBean;

@Component
public class ItemHelper extends AbstractHelper<ItemBean, Item> {

	@Override
	public ItemBean convertEntityToBean(Item item, Class<ItemBean> beanClazz) {
		ItemBean itemBean = mapper.map(item, beanClazz);
		itemBean.setImage(mapper.map(item.getImage(), ImageBean.class));
		return itemBean;
	}
	
}
