package com.carefarm.prakrati.service.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.search.events.PrakratiEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.web.bean.UserBean;

@Component
public class SuperAdminHelper extends AbstractHelper<UserBean, User> {

	@Autowired
	private UserHelper userHelper;
	
	public void convertBeanToEntity(User superAdmin, UserBean superAdminBean) {
		userHelper.convertBeanToEntity(superAdmin, superAdminBean);
	}
	
	public void extractBasicFields(User superAdmin, UserBean superAdminBean) {
		userHelper.extractBasicFields(superAdmin, superAdminBean);
	}
	
	@Override
	protected PrakratiEvent<Iterable<User>> createEvent(Iterable<User> entities,
			EventType event) {
		return userHelper.createEvent(entities, event);
	}
	
}
