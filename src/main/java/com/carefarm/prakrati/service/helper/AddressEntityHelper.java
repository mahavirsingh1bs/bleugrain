package com.carefarm.prakrati.service.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.common.entity.Country;
import com.carefarm.prakrati.common.entity.State;
import com.carefarm.prakrati.entity.AddressEntity;
import com.carefarm.prakrati.repository.CountryRepository;
import com.carefarm.prakrati.repository.StateRepository;
import com.carefarm.prakrati.web.bean.AddressEntityBean;

@Component
public class AddressEntityHelper extends AbstractHelper<AddressEntityBean, AddressEntity> {

	@Autowired
	private StateRepository stateRepository;
	
	@Autowired
	private CountryRepository countryRepository;
	
	public void convertBeanToEntity(AddressEntityBean addressBean, AddressEntity address) {
		State state = stateRepository.findOne(addressBean.getState().getId());
		Country country = countryRepository.findOne(addressBean.getCountry().getId());
		address.setAddressLine1(addressBean.getAddressLine1());
		address.setAddressLine2(addressBean.getAddressLine2());
		address.setCity(addressBean.getCity());
		address.setState(state);
		address.setCountry(country);
		address.setZipcode(addressBean.getZipcode());
		address.setDefaultAddress(addressBean.isDefaultAddress());
	}
}
