package com.carefarm.prakrati.service.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.payment.repository.CreditCardRepository;
import com.carefarm.prakrati.repository.DemandRepository;
import com.carefarm.prakrati.repository.OrderRepository;
import com.carefarm.prakrati.search.events.PrakratiEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.web.bean.CreditCardBean;
import com.carefarm.prakrati.web.bean.CustomerBean;
import com.carefarm.prakrati.web.bean.DemandBean;
import com.carefarm.prakrati.web.bean.OrderBean;

@Component
public class CustomerHelper extends AbstractHelper<CustomerBean, User> {

	@Autowired
	private UserHelper userHelper;
	
	@Autowired
	private CreditCardRepository creditCardRepository;
	
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private DemandRepository demandRepository;
	
	public void convertBeanToEntity(User customer, CustomerBean customerBean) {
		userHelper.convertBeanToEntity(customer, customerBean);
	}
	
	public void extractBasicFields(User customer, CustomerBean customerBean) {
		userHelper.extractBasicFields(customer, customerBean);

		PageRequest pageRequest = new PageRequest(0, 3);
		
		List<CreditCardBean> creditCards = new ArrayList<>();
		creditCardRepository
			.findActiveCardsByUserNo(customer.getUserNo(), pageRequest)
			.forEach(creditCard -> creditCards.add(mapper.map(creditCard, CreditCardBean.class)));
		customerBean.setCreditCards(creditCards);
		
		List<OrderBean> orders = new ArrayList<>();
		orderRepository
			.findByUserNoAndStatusCodesNotIn(customer.getUserNo(), notInOrderStatuses, pageRequest)
			.forEach(order -> orders.add(mapper.map(order, OrderBean.class)));
		customerBean.setOrders(orders);
		
		List<DemandBean> demands = new ArrayList<>();
		demandRepository
			.findByUserId(customer.getId(), pageRequest)
			.forEach(demand -> demands.add(mapper.map(demand, DemandBean.class)));
		customerBean.setDemands(demands);
	}
	
	@Override
	protected PrakratiEvent<Iterable<User>> createEvent(Iterable<User> entities,
			EventType event) {
		return userHelper.createEvent(entities, event);
	}
	
}
