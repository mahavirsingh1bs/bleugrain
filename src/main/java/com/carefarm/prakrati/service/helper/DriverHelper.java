package com.carefarm.prakrati.service.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.repository.CompanyRepository;
import com.carefarm.prakrati.search.events.PrakratiEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.web.bean.CompanyBean;
import com.carefarm.prakrati.web.bean.DriverBean;

@Component
public class DriverHelper extends AbstractHelper<DriverBean, User> {
	
	@Autowired
	private CompanyRepository companyRepository;
	
	@Autowired
	private UserHelper userHelper;
	
	public void convertBeanToEntity(User entity, DriverBean bean) {
		userHelper.convertBeanToEntity(entity, bean);
		if (Predicates.notNull.test(bean.getCompanyId())) {
			entity.setCompany(companyRepository.findOne(bean.getCompanyId()));
		}
	}
	
	public void extractBasicFields(User driver, DriverBean driverBean) {
		userHelper.extractBasicFields(driver, driverBean);
		driverBean.setCompany(mapper.map(driver.getCompany(), CompanyBean.class));
	}

	@Override
	protected PrakratiEvent<Iterable<User>> createEvent(Iterable<User> entities,
			EventType event) {
		return userHelper.createEvent(entities, event);
	}
	
}
