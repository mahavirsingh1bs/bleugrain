package com.carefarm.prakrati.service.helper;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.entity.Bid;
import com.carefarm.prakrati.entity.Demand;
import com.carefarm.prakrati.entity.Order;
import com.carefarm.prakrati.entity.Product;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.repository.CompanyRepository;
import com.carefarm.prakrati.repository.DemandRepository;
import com.carefarm.prakrati.repository.OrderRepository;
import com.carefarm.prakrati.repository.ProductRepository;
import com.carefarm.prakrati.search.events.PrakratiEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.util.DemandStatus;
import com.carefarm.prakrati.web.bean.BidBean;
import com.carefarm.prakrati.web.bean.CompanyAdminBean;
import com.carefarm.prakrati.web.bean.CompanyBean;
import com.carefarm.prakrati.web.bean.CurrencyBean;
import com.carefarm.prakrati.web.bean.DemandBean;
import com.carefarm.prakrati.web.bean.OrderBean;
import com.carefarm.prakrati.web.bean.ProductBean;
import com.carefarm.prakrati.web.bean.UnitBean;

@Component
public class CompanyAdminHelper extends AbstractHelper<CompanyAdminBean, User> {

	@Autowired
	private UserHelper userHelper; 

	@Autowired
	private CompanyRepository companyRepository;
	
	@Autowired
	private DemandRepository demandRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private OrderRepository orderRepository;
	
	public void convertBeanToEntity(User companyAdmin, CompanyAdminBean compAdminBean) {
		userHelper.convertBeanToEntity(companyAdmin, compAdminBean);
		if (Predicates.notNull.test(compAdminBean.getCompanyId())) {
			companyAdmin.setCompany(companyRepository.findOne(compAdminBean.getCompanyId()));
		}
	}
	
	public void extractBasicFields(User companyAdmin, CompanyAdminBean companyAdminBean) {
		companyAdminBean.setCompany(mapper.map(companyAdmin.getCompany(), CompanyBean.class));
		
		Pageable pageable = new PageRequest(0, 10);
		List<Order> orders = orderRepository.findByUserNoAndStatusCodesNotIn(companyAdmin.getUserNo(), notInOrderStatuses, pageable);
		orders.forEach(order -> companyAdminBean.addOrder(mapper.map(order, OrderBean.class)));
		
		List<Demand> demands = demandRepository.findByCreatedByAndStatus(companyAdmin.getUserNo(), DemandStatus.OPEN.getCode());
		demands.forEach(demand -> companyAdminBean.addDemand(mapper.map(demand, DemandBean.class)));
		
		List<Product> products = productRepository.findProductsBiddedByUser(companyAdmin.getUserNo(), pageable);
		products.forEach(product -> {
			Optional<Bid> optionalBid = product.highestBidOfUser(companyAdmin.getUserNo());
			optionalBid.ifPresent(bid -> {
				BidBean bidBean = mapper.map(bid, BidBean.class);
				ProductBean productBean = mapper.map(product, ProductBean.class);
				productBean.setQuantityUnit(mapper.map(product.getQuantity().getUnit(), UnitBean.class));
				bidBean.setProduct(productBean);
				bidBean.setCurrency(mapper.map(bid.getCurrency(), CurrencyBean.class));
				bidBean.setUnit(mapper.map(bid.getUnit(), UnitBean.class));
				companyAdminBean.addBid(bidBean);
			});
		});
		userHelper.extractBasicFields(companyAdmin, companyAdminBean);
	}
	
	@Override
	protected PrakratiEvent<Iterable<User>> createEvent(Iterable<User> entities,
			EventType event) {
		return userHelper.createEvent(entities, event);
	}
	
}
