package com.carefarm.prakrati.service.helper;

import static com.carefarm.prakrati.util.NotificationCategory.USER_CREATED;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.camel.Produce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.common.util.Constants;
import com.carefarm.prakrati.constants.Endpoints;
import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.entity.Company;
import com.carefarm.prakrati.entity.Notification;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.messaging.listener.NotificationListener;
import com.carefarm.prakrati.repository.NotificationRepository;
import com.carefarm.prakrati.service.dto.NotificationDTO;
import com.carefarm.prakrati.util.NotificationCategory;
import com.carefarm.prakrati.util.UserType;
import com.carefarm.prakrati.web.bean.NotificationBean;

@Component
public class NotificationHelper extends AbstractHelper<NotificationBean, Notification>{
	
	@Produce(uri = Endpoints.PRAKRATI_NOTIFICATIONS)
	private NotificationListener notificationListener;

	@Autowired
	private NotificationRepository notificationRepository;
	
	public void sendCompanyRegistrationNotification(Company company) {
        Object[] params = new Object[] { company.getName() };
        String notification = context.getMessage(Constants.COMPANY_CREATED, params, Locale.US);
        this.sendNotification(notification, NotificationCategory.COMPANY_CREATED);
	}
	
	public void sendCompanyCreationNotification(Company company) {
        Object[] params = new Object[] { company.getName() };
        String notification = context.getMessage(Constants.COMPANY_REGISTERED, params, Locale.US);
        this.sendNotification(notification, NotificationCategory.COMPANY_REGISTERED);
	}
	
	public void sendUserRegistrationNotification(User user, UserType userType) {
        Object[] params = new Object[] { user.getFirstName(), user.getLastName(), userType };
        String notification = context.getMessage(Constants.USER_REGISTERED, params, Locale.US);
        this.sendNotification(notification, NotificationCategory.USER_REGISTERED);
	}
	
	public void sendUserCreationNotification(User user, UserType userType) {
        Object[] params = new Object[] { user.getFirstName(), user.getLastName(), userType };
        String notification = context.getMessage(Constants.USER_CREATED, params, Locale.US);
        this.sendNotification(notification, USER_CREATED);
	}
	
	public void sendNotification(String notification, NotificationCategory category) {
		NotificationDTO notificationDTO = NotificationDTO.Builder.getInstance()
				.category(category)
				.notification(notification)
				.groups(Arrays.asList(UserType.SUPER_ADMIN.getType()))
				.startDate(Date.from(Environment.clock().instant()))
				.expiryDate(Date.from(Environment.clock().instant().plus(5L, ChronoUnit.DAYS)))
				.build();;
		notificationListener.sendNotification(notificationDTO);
	}
	
	public void sendNotification(NotificationDTO notification) {
		notificationListener.sendNotification(notification);
	}
	
	public List<NotificationBean> findNotificationBy(String groupId, String userNo) {
		List<NotificationBean> notifications = new ArrayList<>();
		notificationRepository.findByGroupIdAndUserNo(groupId, userNo, Date.from(Environment.clock().instant()))
			.forEach((notification) -> {
				notifications.add(mapper.map(notification, NotificationBean.class));
			});
		return notifications;
	}
		
}
