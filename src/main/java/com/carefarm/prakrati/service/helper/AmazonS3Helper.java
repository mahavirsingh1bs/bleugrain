package com.carefarm.prakrati.service.helper;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.Random;

import javax.imageio.ImageIO;

import org.apache.commons.lang3.StringUtils;
import org.fusesource.hawtbuf.ByteArrayInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.carefarm.prakrati.entity.Company;
import com.carefarm.prakrati.entity.Image;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.util.ImageType;
import com.carefarm.prakrati.vo.ImageVO;

@Component
public class AmazonS3Helper extends ImageHelper {

	private static final Logger LOGGER = LoggerFactory.getLogger(AmazonS3Helper.class);
	
	@Autowired
	private AmazonS3Client amazonS3Client;
	
	@Value("${cloud.aws.s3.bucket}")
	private String bucket;
	
	public Image handleImage(MultipartFile file, ImageType imageType, User user) {
		String extension = StringUtils.substringAfter(file.getContentType(), SLASH);
		String imageName = "" + Math.abs(new Random().nextInt());
		String imageKey = MessageFormat.format(DEFAULT_IMAGE_URL, imageType.getType(), user.getUserNo(), imageName, extension);
		try {
			return handleImageInternal(file.getBytes(), file.getContentType(), imageName, imageKey);
		} catch (IOException e) {
			LOGGER.info("Got An error while saving the images");
			throw new RuntimeException();
		}
	}
	
	public Image handleImage(MultipartFile file, Company company) {
		String extension = StringUtils.substringAfter(file.getContentType(), SLASH);
		String imageName = "" + Math.abs(new Random().nextInt());
		String imageKey = MessageFormat.format(DEFAULT_IMAGE_URL, ImageType.COMPANIES.getType(), company.getUniqueId(), imageName, extension);
		try {
			return handleImageInternal(file.getBytes(), file.getContentType(), imageName, imageKey);
		} catch (IOException e) {
			LOGGER.info("Got An error while saving the images");
			throw new RuntimeException();
		}
	}
	
	public Image handleImage(ImageVO imageVO, Company company) {
		String extension = StringUtils.substringAfter(imageVO.getContentType(), SLASH);
		String imageName = "" + Math.abs(new Random().nextInt());
		String imageKey = MessageFormat.format(DEFAULT_IMAGE_URL, ImageType.COMPANIES.getType(), company.getUniqueId(), imageName, extension);
		return handleImageInternal(imageVO.getData(), imageVO.getContentType(), imageName, imageKey);
	}

	public Image handleImage(ImageVO imageVO, User user) {
		String extension = StringUtils.substringAfter(imageVO.getContentType(), SLASH);
		String imageName = "" + Math.abs(new Random().nextInt());
		String imageKey = MessageFormat.format(DEFAULT_IMAGE_URL, ImageType.USERS.getType(), user.getUserNo(), imageName, extension);
		return handleImageInternal(imageVO.getData(), imageVO.getContentType(), imageName, imageKey);
	}
	
	private Image handleImageInternal(byte[] data, String contentType, String imageName,
			String imageKey) {
		try {
			BufferedImage image = createImageFromBytes(data);
			this.saveImageIntoS3(image, imageKey, contentType);
			return this.createInstance(imageName, imageKey);
		} catch(IOException ex) {
			LOGGER.info("Got An error while saving the images");
			throw new RuntimeException();
		}
	}

	public Image createImage(Company company, String filepath) {
		String imageName = company.getName() + "_" + Math.abs(new Random().nextInt());
		return this.createInstance(imageName, filepath);
	}
	
	/**
	 * Method to handle MultipartFile upload for products and demands
	 *  
	 * @param file
	 * @param imageType
	 * @param uniqueId
	 * @return
	 */
	public Image handleImage(MultipartFile file, ImageType imageType, String uniqueId) {
		String extension = StringUtils.substringAfter(file.getContentType(), SLASH);
		String imageName = StringUtils.replaceChars(file.getName(), SPACE, UNDERSCORE) + "_" + Math.abs(new Random().nextInt());
		
		String originalImageKey = MessageFormat.format(DEFAULT_IMAGE_URL, imageType.getType(), uniqueId, imageName, extension);
		String bigImageKey = MessageFormat.format(BIG_IMAGE_URL, imageType.getType(), uniqueId, imageName, extension);
		String mediumImageKey = MessageFormat.format(MEDIUM_IMAGE_URL, imageType.getType(), uniqueId, imageName, extension);
		String smallImageKey = MessageFormat.format(SMALL_IMAGE_URL, imageType.getType(), uniqueId, imageName, extension);

		try {	    	
			BufferedImage originalImage = createImageFromBytes(file.getBytes());
			int type = originalImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
			
			BufferedImage bigImage = resizeImage(originalImage, BIG_WIDTH, BIG_HEIGHT, type);
			BufferedImage mediumImage = resizeImage(originalImage, MEDIUM_WIDTH, MEDIUM_HEIGHT, type);
			BufferedImage smallImage = resizeImage(originalImage, SMALL_WIDTH, SMALL_HEIGHT, type);
			
			this.saveImageIntoS3(originalImage, originalImageKey, file.getContentType());
			this.saveImageIntoS3(bigImage, bigImageKey, file.getContentType());
			this.saveImageIntoS3(mediumImage, mediumImageKey, file.getContentType());
			this.saveImageIntoS3(smallImage, smallImageKey, file.getContentType());
			return this.createInstance(imageName, bigImageKey, smallImageKey, mediumImageKey, bigImageKey);
		} catch (IOException e) {
			LOGGER.info("Got An error while saving the images");
			throw new RuntimeException();
		}
	}
		
	/**
	 * Method to save the image into the Amazon S3
	 * 
	 * @param image which we want to save in S3
	 * @param key unique key of the image
	 * @param contentType of the image like image/png
	 * @throws IOException when there is a connection error with S3
	 */
	private void saveImageIntoS3(BufferedImage image, String key, String contentType) throws IOException {
		String extension = StringUtils.substringAfter(contentType, SLASH);
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		ImageIO.write(image, extension, os);
		byte[] buffer = os.toByteArray();
		InputStream is = new ByteArrayInputStream(buffer);
		ObjectMetadata meta = new ObjectMetadata();
		meta.setContentLength(buffer.length);
		meta.setContentType(contentType);
		amazonS3Client.putObject(new PutObjectRequest(bucket, key, is, meta));
	}
}
