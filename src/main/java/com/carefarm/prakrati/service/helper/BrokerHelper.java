package com.carefarm.prakrati.service.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.payment.repository.BankAccountRepository;
import com.carefarm.prakrati.repository.DemandRepository;
import com.carefarm.prakrati.repository.ProductRepository;
import com.carefarm.prakrati.search.events.PrakratiEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.web.bean.BankAccountBean;
import com.carefarm.prakrati.web.bean.BrokerBean;
import com.carefarm.prakrati.web.bean.DemandBean;
import com.carefarm.prakrati.web.bean.ProductBean;

@Component
public class BrokerHelper extends AbstractHelper<BrokerBean, User> {
	
	@Autowired
	private UserHelper userHelper;
	
	@Autowired
	private BankAccountRepository bankAccountRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private DemandRepository demandRepository;
	
	public void convertBeanToEntity(User broker, BrokerBean brokerBean) {
		userHelper.convertBeanToEntity(broker, brokerBean);
	}
	
	public void extractBasicFields(User broker, BrokerBean brokerBean) {
		userHelper.extractBasicFields(broker, brokerBean);
		
		PageRequest pageRequest = new PageRequest(0, 3);
		
		List<BankAccountBean> bankAccounts = new ArrayList<>();
		bankAccountRepository
			.findActiveAccountsByUserNo(broker.getUserNo(), pageRequest)
			.forEach(bankAccount -> bankAccounts.add(mapper.map(bankAccount, BankAccountBean.class)));
		brokerBean.setBankAccounts(bankAccounts);
		
		List<ProductBean> products = new ArrayList<>();
		productRepository
			.findByUserId(broker.getId(), pageRequest)
			.forEach(product -> products.add(mapper.map(product, ProductBean.class)));
		brokerBean.setProducts(products);
		
		List<DemandBean> demands = new ArrayList<>();
		demandRepository
			.findByUserId(broker.getId(), pageRequest)
			.forEach(demand -> demands.add(mapper.map(demand, DemandBean.class)));
		brokerBean.setDemands(demands);
	}
	
	public void setBasicProperties(User broker, String groupName) {
		userHelper.setBasicProperties(broker, groupName);
	}
	
	@Override
	protected PrakratiEvent<Iterable<User>> createEvent(Iterable<User> entities,
			EventType event) {
		return userHelper.createEvent(entities, event);
	}
	
}
