package com.carefarm.prakrati.service.helper;

import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.Designation;
import com.carefarm.prakrati.search.events.DesignationEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.web.bean.DesignationBean;

@Component
public class DesignationHelper extends AbstractHelper<DesignationBean, Designation> {
	
	public void convertBeanToEntity(Designation designation, DesignationBean designationBean) {
		designation.setName(designationBean.getName());
	}
	
	@Override
	protected PrakratiEvent<Iterable<Designation>> createEvent(Iterable<Designation> entities,
			EventType event) {
		return new DesignationEvent(entities, event);
	}

}
