package com.carefarm.prakrati.service.helper;

import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.Role;
import com.carefarm.prakrati.search.events.PrakratiEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.search.events.RoleEvent;
import com.carefarm.prakrati.web.bean.RoleBean;

@Component
public class RoleHelper extends AbstractHelper<RoleBean, Role> {
	
	public void convertBeanToEntity(Role role, RoleBean roleBean) {
		role.setName(roleBean.getName());
	}
	
	@Override
	protected PrakratiEvent<Iterable<Role>> createEvent(Iterable<Role> entities,
			EventType event) {
		return new RoleEvent(entities, event);
	}

}
