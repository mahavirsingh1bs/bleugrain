package com.carefarm.prakrati.service.helper;

import org.springframework.stereotype.Component;

import com.carefarm.prakrati.common.entity.Unit;
import com.carefarm.prakrati.web.bean.UnitBean;

@Component
public class UnitHelper extends AbstractHelper<UnitBean, Unit> {

}
