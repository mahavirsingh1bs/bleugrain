package com.carefarm.prakrati.service.helper;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.springframework.beans.factory.annotation.Value;

import com.carefarm.prakrati.entity.Image;

public abstract class ImageHelper {

	protected static final String SLASH = "/";
	protected static final String SPACE = " ";
	protected static final String UNDERSCORE = "_";
	
	protected final static String DEFAULT_IMAGE_URL = "assets/resources/data/{0}/{1}/defaults/{2}.{3}";
	protected final static String BIG_IMAGE_URL = "assets/resources/data/{0}/{1}/big/{2}.{3}";
	protected final static String MEDIUM_IMAGE_URL = "assets/resources/data/{0}/{1}/medium/{2}.{3}";
	protected final static String SMALL_IMAGE_URL = "assets/resources/data/{0}/{1}/small/{2}.{3}";
	
	protected static final int BIG_WIDTH = 1024;
	protected static final int BIG_HEIGHT = 680;
	
	protected static final int MEDIUM_WIDTH = 240;
	protected static final int MEDIUM_HEIGHT = 160;
	
	protected static final int SMALL_WIDTH = 75;
	protected static final int SMALL_HEIGHT = 50;
	
	@Value("${cloud.aws.s3.bucket.prefix}")
	private String prefix;
	
	protected static BufferedImage resizeImage(BufferedImage originalImage, int width, int height, int type){
		BufferedImage resizedImage = new BufferedImage(width, height, type);
		Graphics2D g = resizedImage.createGraphics();
		g.drawImage(originalImage, 0, 0, width, height, null);
		g.dispose();
		
		return resizedImage;
    }
	
	protected static BufferedImage createImageFromBytes(byte[] imageData) {
	    ByteArrayInputStream bais = new ByteArrayInputStream(imageData);
	    try {
	        return ImageIO.read(bais);
	    } catch (IOException e) {
	        throw new RuntimeException(e);
	    }
	}
	
	public Image createInstance(String filename, String filepath, String...otherPaths) {
		Image image = new Image(filename, prefix + filepath);
		
		if (otherPaths.length > 0 && otherPaths[0] != null) {
			image.setSmallFilepath(prefix + otherPaths[0]);
		} else {
			image.setSmallFilepath(prefix + filepath);
		}
		
		if (otherPaths.length > 0 && otherPaths[1] != null) {
			image.setMediumFilepath(prefix + otherPaths[1]);
		} else {
			image.setMediumFilepath(prefix + filepath);
		}
		
		if (otherPaths.length > 0 && otherPaths[2] != null) {
			image.setLargeFilepath(prefix + otherPaths[2]);
		} else {
			image.setLargeFilepath(prefix + filepath);
		}
		
		return image;
	}
	
}
