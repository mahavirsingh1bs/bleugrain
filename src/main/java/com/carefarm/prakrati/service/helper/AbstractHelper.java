package com.carefarm.prakrati.service.helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEventPublisher;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.delivery.util.OrderStatus;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.search.events.PrakratiEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.util.GlobalConstants;
import com.carefarm.prakrati.util.ReflectionUtil;
import com.carefarm.prakrati.web.util.PrakratiConstants;

public abstract class AbstractHelper<B, E> {
	private static final Logger LOGGER = LoggerFactory.getLogger(AbstractHelper.class);
	
	protected static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat(GlobalConstants.DATE_FORMAT.getValue());
	
	protected final String[] notInOrderStatuses = { OrderStatus.DELIVERED.getCode() };
	
	@Autowired
	protected DozerBeanMapper mapper;
	
	@Autowired
	protected ApplicationContext context;
	
	@Autowired
    protected ApplicationEventPublisher eventPublisher;
	
	public List<B> convertEntitiesToBeans(Iterable<E> entities, Class<B> beanClazz) {
		List<B> beans = new ArrayList<>();
		if (Predicates.notNull.test(entities)) {
			for (E entity : entities) {
				beans.add(convertEntityToBean(entity, beanClazz));
			}
		}
		return beans;
	}
	
	public B convertEntityToBean(E entity, Class<B> beanClazz) {
		return mapper.map(entity, beanClazz);
	}
	
	public List<E> convertBeansToEntities(List<B> beans, Class<E> entityClazz) {
		List<E> entities = new ArrayList<>();
		for (B bean : beans) {
			entities.add(mapper.map(bean, entityClazz));
		}
		return entities;
	}
	
	public E convertBeanToEntity(B bean, Class<E> entityClazz) {
		return mapper.map(bean, entityClazz);
	}
	
	public void convertBeanToEntity(E entity, B bean) {
		// Sub classes can override this.
	}
	
	public void publishIndexEvent(E entity, EventType event) {
		eventPublisher.publishEvent(createEvent(iterable(entity), event));
	}
	
	protected Iterable<E> iterable(E entity) {
		List<E> entities = new ArrayList<>();
		entities.add(entity);
		return entities;
	}
	
	protected PrakratiEvent<Iterable<E>> createEvent(Iterable<E> entities, EventType event) {
		return null;
	}
	protected Date getParseDate(String date) {
		try {
			return DATE_FORMAT.parse(date);
		} catch (ParseException e) {
			LOGGER.error("Got an error while trying to format date: " + date);
			throw new ServiceException("Got an error while trying to format date: " + date);
		}
	}
	
	protected String getFormatDate(Date date) {
		return DATE_FORMAT.format(date);
	}
	
	public void setEntityOf(E entity, User user) {
		if (user.isACompanyUser()) {
			ReflectionUtil.setField(PrakratiConstants.COMPANY, entity, user.getCompany());
		}
		ReflectionUtil.setField(PrakratiConstants.USER, entity, user);
	}
}