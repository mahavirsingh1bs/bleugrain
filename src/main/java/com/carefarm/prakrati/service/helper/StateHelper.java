package com.carefarm.prakrati.service.helper;

import org.springframework.stereotype.Component;

import com.carefarm.prakrati.common.entity.State;
import com.carefarm.prakrati.search.events.PrakratiEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.search.events.StateEvent;
import com.carefarm.prakrati.web.bean.StateBean;

@Component
public class StateHelper extends AbstractHelper<StateBean, State> {
	
	public void convertBeanToEntity(State state, StateBean stateBean) {
		state.setName(stateBean.getName());
	}
	
	@Override
	protected PrakratiEvent<Iterable<State>> createEvent(Iterable<State> entities,
			EventType event) {
		return new StateEvent(entities, event);
	}

}
