package com.carefarm.prakrati.service.helper;

import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.PaymentMethod;
import com.carefarm.prakrati.search.events.PaymentMethodEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.web.bean.PaymentMethodBean;

@Component
public class PaymentMethodHelper extends AbstractHelper<PaymentMethodBean, PaymentMethod> {

	public void convertBeanToEntity(PaymentMethod paymentMethod, PaymentMethodBean paymentMethodBean) {
		paymentMethod.setName(paymentMethodBean.getName());
	}
	
	@Override
	protected PrakratiEvent<Iterable<PaymentMethod>> createEvent(Iterable<PaymentMethod> entities,
			EventType event) {
		return new PaymentMethodEvent(entities, event);
	}

}
