package com.carefarm.prakrati.service.helper;

import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.PaymentCard;
import com.carefarm.prakrati.search.events.PaymentCardEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.web.bean.PaymentCardBean;

@Component
public class PaymentCardHelper extends AbstractHelper<PaymentCardBean, PaymentCard> {

	public void convertBeanToEntity(PaymentCard paymentCard, PaymentCardBean paymentCardBean) {
		paymentCard.setName(paymentCardBean.getName());
	}
	
	@Override
	protected PrakratiEvent<Iterable<PaymentCard>> createEvent(Iterable<PaymentCard> entities,
			EventType event) {
		return new PaymentCardEvent(entities, event);
	}

}
