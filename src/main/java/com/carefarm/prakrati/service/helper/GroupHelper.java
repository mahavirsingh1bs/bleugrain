package com.carefarm.prakrati.service.helper;

import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.Group;
import com.carefarm.prakrati.search.events.GroupEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.web.bean.GroupBean;

@Component
public class GroupHelper extends AbstractHelper<GroupBean, Group> {
	
	public void convertBeanToEntity(Group group, GroupBean groupBean) {
		group.setName(groupBean.getName());
	}
	
	@Override
	protected PrakratiEvent<Iterable<Group>> createEvent(Iterable<Group> entities,
			EventType event) {
		return new GroupEvent(entities, event);
	}

}
