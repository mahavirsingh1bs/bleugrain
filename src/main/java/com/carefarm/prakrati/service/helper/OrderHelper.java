package com.carefarm.prakrati.service.helper;

import java.util.Optional;

import org.apache.camel.Produce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.constants.Endpoints;
import com.carefarm.prakrati.delivery.util.OrderStatus;
import com.carefarm.prakrati.entity.Order;
import com.carefarm.prakrati.messaging.listener.OrderAssignedListener;
import com.carefarm.prakrati.messaging.listener.OrderDeliveredListener;
import com.carefarm.prakrati.messaging.listener.OrderDispatchListener;
import com.carefarm.prakrati.messaging.listener.OrderPlacementListener;
import com.carefarm.prakrati.messaging.listener.OrderShipmentListener;
import com.carefarm.prakrati.search.events.OrderEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.web.bean.AddressEntityBean;
import com.carefarm.prakrati.web.bean.CompanyBean;
import com.carefarm.prakrati.web.bean.ItemBean;
import com.carefarm.prakrati.web.bean.OrderBean;
import com.carefarm.prakrati.web.bean.ProductBean;
import com.carefarm.prakrati.web.bean.UserBean;

@Component
public class OrderHelper extends AbstractHelper<OrderBean, Order> {

	@Autowired
	private ProductHelper productHelper;
	
	@Autowired
	private ItemHelper itemHelper;
	
	@Produce(uri = Endpoints.ORDER_PLACED)
	private OrderPlacementListener orderPlacementListener;

	@Produce(uri = Endpoints.ORDER_SHIPPED)
	private OrderShipmentListener orderShipmentListener;

	@Produce(uri = Endpoints.ORDER_ASSIGNED)
	private OrderAssignedListener orderAssignedListener;
	
	@Produce(uri = Endpoints.ORDER_DISPATCHED)
	private OrderDispatchListener orderDispatchListener;

	@Produce(uri = Endpoints.ORDER_DELIVERED)
	private OrderDeliveredListener orderDeliveredListener;
	
	public void createAndSendMessages(Order order, String status) {
		OrderBean orderBean = this.convertEntityToBean(order, OrderBean.class);
		if (status.equals(OrderStatus.PLACED.getCode())) {
			orderPlacementListener.notify(orderBean);
		} else if (status.equals(OrderStatus.SHIPPED.getCode())) {
			orderShipmentListener.notify(orderBean);
		} else if (status.equals(OrderStatus.DISPATCHED.getCode())) {
			orderDispatchListener.notify(orderBean);
		} else if (status.equals(OrderStatus.DELIVERED.getCode())) {
			orderDeliveredListener.notify(orderBean);
		} else if (status.equals(OrderStatus.ASSIGNED.getCode())) {
			orderAssignedListener.notify(orderBean);
		}
	}
	
	public OrderBean convertEntityToBean(Order order, Class<OrderBean> beanClazz) {
		OrderBean orderBean = mapper.map(order, beanClazz);
		orderBean.setBillingAddress(mapper.map(order.getBillingAddress(), AddressEntityBean.class));
		orderBean.setShippingAddress(mapper.map(order.getShippingAddress(), AddressEntityBean.class));
		orderBean.setProducts(productHelper.convertEntitiesToBeans(order.getProducts(), ProductBean.class));
		orderBean.setItems(itemHelper.convertEntitiesToBeans(order.getItems(), ItemBean.class));
		orderBean.setDeliveryDate(getFormatDate(order.getDeliveryDate()));
		orderBean.setCreatedBy(mapper.map(order.getCreatedBy(), UserBean.class));
		Optional.ofNullable(order.getUser())
			.ifPresent(user -> orderBean.setUser(mapper.map(user, UserBean.class)));
		Optional.ofNullable(order.getCompany())
			.ifPresent(company -> orderBean.setCompany(mapper.map(company, CompanyBean.class)));
		Optional.ofNullable(order.getDeliverBy())
			.ifPresent(deliverBy -> orderBean.setDeliverBy(mapper.map(deliverBy, UserBean.class)));
		return orderBean;
	}

	@Override
	protected PrakratiEvent<Iterable<Order>> createEvent(Iterable<Order> entities,
			EventType event) {
		return new OrderEvent(entities, event);
	}
	
}
