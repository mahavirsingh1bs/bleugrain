package com.carefarm.prakrati.service.helper;

import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.Department;
import com.carefarm.prakrati.search.events.DepartmentEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.web.bean.DepartmentBean;

@Component
public class DepartmentHelper extends AbstractHelper<DepartmentBean, Department> {
	
	public void convertBeanToEntity(Department department, DepartmentBean departmentBean) {
		department.setName(departmentBean.getName());
	}
	
	@Override
	protected PrakratiEvent<Iterable<Department>> createEvent(Iterable<Department> entities,
			EventType event) {
		return new DepartmentEvent(entities, event);
	}

}
