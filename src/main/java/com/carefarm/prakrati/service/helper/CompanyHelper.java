package com.carefarm.prakrati.service.helper;

import static com.carefarm.prakrati.common.util.Constants.PLUS;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.common.entity.Country;
import com.carefarm.prakrati.common.entity.State;
import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.entity.Address;
import com.carefarm.prakrati.entity.Company;
import com.carefarm.prakrati.repository.CountryRepository;
import com.carefarm.prakrati.repository.StateRepository;
import com.carefarm.prakrati.search.events.CompanyEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.util.BusinessCategory;
import com.carefarm.prakrati.web.bean.AddressBean;
import com.carefarm.prakrati.web.bean.CompanyBean;

@Component
public class CompanyHelper extends AbstractHelper<CompanyBean, Company> {
	
	@Autowired
	private StateRepository stateRepository;
	
	@Autowired
	private CountryRepository countryRepository;
	
	public void convertBeanToEntity(Company company, CompanyBean companyBean) {
		AddressBean addressBean = companyBean.getAddress();
		Address address = company.getAddress();
		State state = stateRepository.findOne(addressBean.getState().getId());
		Country country = countryRepository.findOne(addressBean.getCountry().getId());
		address.setAddressLine1(addressBean.getAddressLine1());
		address.setAddressLine2(addressBean.getAddressLine2());
		address.setCity(addressBean.getCity());
		address.setState(state);
		address.setCountry(country);
		address.setZipcode(addressBean.getZipcode());
		company.setName(companyBean.getName());
		company.setBusinessCategory(BusinessCategory.getCategory(companyBean.getBusinessCategory()));
		company.setShortDesc(companyBean.getShortDesc());
		company.setRegistrNo(companyBean.getRegistrNo());
		company.setEmailId(companyBean.getEmailId());
		
		if (Predicates.notNull.test(companyBean.getPhoneNo()) && 
				!companyBean.getPhoneNo().startsWith(PLUS)) {
			company.setPhoneNo(PLUS + companyBean.getPhoneNo());
		}
	}
	
	@Override
	protected PrakratiEvent<Iterable<Company>> createEvent(Iterable<Company> entities,
			EventType event) {
		return new CompanyEvent(entities, event);
	}

}
