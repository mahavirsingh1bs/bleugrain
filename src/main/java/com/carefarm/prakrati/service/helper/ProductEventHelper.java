package com.carefarm.prakrati.service.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.ProductEvent;
import com.carefarm.prakrati.web.bean.ProductEventBean;
import com.carefarm.prakrati.web.bean.ProductStatusBean;
import com.carefarm.prakrati.web.bean.UserBean;

@Component
public class ProductEventHelper extends AbstractHelper<ProductEventBean, ProductEvent> {
	
	@Override
	public List<ProductEventBean> convertEntitiesToBeans(Iterable<ProductEvent> productEvents, Class<ProductEventBean> beanClazz) {
		List<ProductEventBean> productEventBeans = new ArrayList<>();
		for (ProductEvent productEvent : productEvents) {
			ProductEventBean productEventBean = mapper.map(productEvent, beanClazz);
			productEventBean.setEventBy(mapper.map(productEvent.getEventBy(), UserBean.class));
			productEventBean.setProductStatus(mapper.map(productEvent.getProductStatus(), ProductStatusBean.class));
			productEventBeans.add(productEventBean);
		}
		return productEventBeans;
	}
}
