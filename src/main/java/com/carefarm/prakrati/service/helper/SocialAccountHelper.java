package com.carefarm.prakrati.service.helper;

import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.SocialAccount;
import com.carefarm.prakrati.web.bean.SocialAccountBean;

@Component
public class SocialAccountHelper extends AbstractHelper<SocialAccountBean, SocialAccount> {

	@Override
	public void convertBeanToEntity(SocialAccount entity, SocialAccountBean bean) {
		entity.setDisplayName(bean.getDisplayName());
		entity.setUsername(bean.getUsername());
		entity.setAccountUrl(bean.getAccountUrl());
		entity.setSocialSite(bean.getSocialSite());
	}
	
}
