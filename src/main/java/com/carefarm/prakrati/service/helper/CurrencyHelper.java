package com.carefarm.prakrati.service.helper;

import org.springframework.stereotype.Component;

import com.carefarm.prakrati.common.entity.Currency;
import com.carefarm.prakrati.web.bean.CurrencyBean;

@Component
public class CurrencyHelper extends AbstractHelper<CurrencyBean, Currency> {

}
