package com.carefarm.prakrati.service.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.payment.repository.BankAccountRepository;
import com.carefarm.prakrati.repository.ProductRepository;
import com.carefarm.prakrati.search.events.PrakratiEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.web.bean.BankAccountBean;
import com.carefarm.prakrati.web.bean.FarmerBean;
import com.carefarm.prakrati.web.bean.ProductBean;

@Component
public class FarmerHelper extends AbstractHelper<FarmerBean, User> {
	
	@Autowired
	private UserHelper userHelper;
	
	@Autowired
	private BankAccountRepository bankAccountRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	public void convertBeanToEntity(User farmer, FarmerBean farmerBean) {
		userHelper.convertBeanToEntity(farmer, farmerBean);
	}
	
	public void extractBasicFields(User farmer, FarmerBean farmerBean) {
		userHelper.extractBasicFields(farmer, farmerBean);
		
		PageRequest pageRequest = new PageRequest(0, 3);
		
		List<BankAccountBean> bankAccounts = new ArrayList<>();
		bankAccountRepository
			.findActiveAccountsByUserNo(farmer.getUserNo(), pageRequest)
			.forEach(bankAccount -> bankAccounts.add(mapper.map(bankAccount, BankAccountBean.class)));
		farmerBean.setBankAccounts(bankAccounts);
		
		List<ProductBean> products = new ArrayList<>();
		productRepository
			.findByUserId(farmer.getId(), pageRequest)
			.forEach(product -> products.add(mapper.map(product, ProductBean.class)));
		farmerBean.setProducts(products);
	}
	
	public void setBasicProperties(User farmer, String groupName) {
		userHelper.setBasicProperties(farmer, groupName);
	}
	
	@Override
	protected PrakratiEvent<Iterable<User>> createEvent(Iterable<User> entities,
			EventType event) {
		return userHelper.createEvent(entities, event);
	}
	
}
