package com.carefarm.prakrati.service.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.entity.Category;
import com.carefarm.prakrati.repository.CategoryRepository;
import com.carefarm.prakrati.search.events.CategoryEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.web.bean.CategoryBean;
import com.carefarm.prakrati.web.bean.UserBean;

@Component
public class CategoryHelper extends AbstractHelper<CategoryBean, Category>{

	@Autowired
	private CategoryRepository categoryRepository;
	
	public void convertBeanToEntity(Category category, CategoryBean categoryBean) {
		category.setName(categoryBean.getName());
		category.setParentCategory(categoryRepository.findByUniqueId(categoryBean.getParentCategoryId()));
	}
	
	public void mapBasicFields(Category category, CategoryBean categoryBean) {
		if (Predicates.notNull.test(category.getParentCategory())) {
			categoryBean.setParentCategoryId(category.getParentCategory().getUniqueId());
			categoryBean.setParentCategory(mapper.map(category.getParentCategory(), CategoryBean.class));
		}
		
		if (Predicates.notNull.test(category.getCreatedBy())) {
			categoryBean.setCreatedBy(mapper.map(category.getCreatedBy(), UserBean.class));
		}
		
		if (Predicates.notNull.test(category.getModifiedBy())) {
			categoryBean.setModifiedBy(mapper.map(category.getModifiedBy(), UserBean.class));
		}
	}

	@Override
	protected PrakratiEvent<Iterable<Category>> createEvent(Iterable<Category> entities,
			EventType event) {
		return new CategoryEvent(entities, event);
	}
	
}