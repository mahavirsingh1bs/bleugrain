package com.carefarm.prakrati.service.helper;

import org.springframework.stereotype.Component;

import com.carefarm.prakrati.common.entity.Country;
import com.carefarm.prakrati.search.events.CountryEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.web.bean.CountryBean;

@Component
public class CountryHelper extends AbstractHelper<CountryBean, Country> {

	public void convertBeanToEntity(Country country, CountryBean countryBean) {
		country.setName(countryBean.getName());
	}
	
	@Override
	protected PrakratiEvent<Iterable<Country>> createEvent(Iterable<Country> entities,
			EventType event) {
		return new CountryEvent(entities, event);
	}

}
