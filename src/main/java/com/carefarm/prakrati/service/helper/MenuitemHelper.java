package com.carefarm.prakrati.service.helper;

import java.util.Optional;

import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.Menuitem;
import com.carefarm.prakrati.search.events.MenuitemEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.web.bean.CategoryBean;
import com.carefarm.prakrati.web.bean.MenuitemBean;
import com.carefarm.prakrati.web.bean.UserBean;

@Component
public class MenuitemHelper extends AbstractHelper<MenuitemBean, Menuitem> {
	
	public void mapBasicFields(Menuitem menuitem, MenuitemBean menuitemBean) {
		Optional.ofNullable(menuitem.getParentMenuitem())
			.ifPresent(parentMenuitem -> 
				menuitemBean.setParentMenuitem(mapper.map(parentMenuitem, MenuitemBean.class)));
		
		Optional.ofNullable(menuitem.getCategory())
			.ifPresent(category -> 
				menuitemBean.setCategory(mapper.map(category, CategoryBean.class)));
		
		Optional.ofNullable(menuitem.getCreatedBy())
			.ifPresent(createdBy -> 
				menuitemBean.setCreatedBy(mapper.map(createdBy, UserBean.class)));
	
		Optional.ofNullable(menuitem.getModifiedBy())
			.ifPresent(modifiedBy -> 
				menuitemBean.setModifiedBy(mapper.map(modifiedBy, UserBean.class)));
	}

	@Override
	protected PrakratiEvent<Iterable<Menuitem>> createEvent(Iterable<Menuitem> entities,
			EventType event) {
		return new MenuitemEvent(entities, event);
	}

}