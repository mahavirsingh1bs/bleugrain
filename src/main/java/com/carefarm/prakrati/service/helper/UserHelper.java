package com.carefarm.prakrati.service.helper;

import static com.carefarm.prakrati.common.util.Constants.PLUS;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.common.entity.Country;
import com.carefarm.prakrati.common.entity.State;
import com.carefarm.prakrati.common.util.Constants;
import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.entity.Address;
import com.carefarm.prakrati.entity.Group;
import com.carefarm.prakrati.entity.Language;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.repository.CountryRepository;
import com.carefarm.prakrati.repository.GroupRepository;
import com.carefarm.prakrati.repository.LanguageRepository;
import com.carefarm.prakrati.repository.StateRepository;
import com.carefarm.prakrati.search.events.PrakratiEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.search.events.UserEvent;
import com.carefarm.prakrati.web.bean.AddressBean;
import com.carefarm.prakrati.web.bean.CompanyBean;
import com.carefarm.prakrati.web.bean.CountryBean;
import com.carefarm.prakrati.web.bean.GroupBean;
import com.carefarm.prakrati.web.bean.ImageBean;
import com.carefarm.prakrati.web.bean.LanguageBean;
import com.carefarm.prakrati.web.bean.RoleBean;
import com.carefarm.prakrati.web.bean.UserBean;

@Component
public class UserHelper extends AbstractHelper<UserBean, User>{
	
	@Autowired
	private StateRepository stateRepository;
	
	@Autowired
	private CountryRepository countryRepository;
	
	@Autowired
	private LanguageRepository languageRepository;
	
	@Autowired
	private GroupRepository groupRepository;
	
	public void convertBeanToEntity(User user, UserBean userBean) {
		AddressBean addressBean = userBean.getAddress();
		Address address = user.getAddress();
		if (Predicates.isNull.test(user.getAddress())) {
			address = new Address();
		}
		
		State state = stateRepository.findOne(addressBean.getState().getId());
		Country country = countryRepository.findOne(addressBean.getCountry().getId());
		address.setAddressLine1(addressBean.getAddressLine1());
		address.setAddressLine2(addressBean.getAddressLine2());
		address.setCity(addressBean.getCity());
		address.setState(state);
		address.setCountry(country);
		address.setZipcode(addressBean.getZipcode());
		user.setFirstName(userBean.getFirstName());
		user.setLastName(userBean.getLastName());
		
		if (Predicates.notNull.test(userBean.getMobileNo()) && 
				!userBean.getMobileNo().startsWith(PLUS)) {
			user.setMobileNo(PLUS + userBean.getMobileNo());
		}
		
		if (Predicates.notNull.test(userBean.getPhoneNo()) && 
				!userBean.getPhoneNo().startsWith(PLUS)) {
			user.setPhoneNo(PLUS + userBean.getPhoneNo());
		}
		
		user.setAddress(address);
		
		if (Predicates.notNull.test(userBean.getDefaultLanguage())) {
			user.setDefaultLanguage(mapper.map(userBean.getDefaultLanguage(), Language.class));
		}
	}
	
	public void extractBasicFields(User user, UserBean userBean) {
		userBean.setEmailVerified(user.isEmailVerified());
		userBean.setMobileVerified(user.isMobileVerified());
		userBean.setGroup(mapper.map(user.getGroup(), GroupBean.class));
		user.getGroup().getRoles()
			.forEach(role -> userBean.addRole(mapper.map(role, RoleBean.class)));
		
		if (Predicates.notNull.test(user.getDefaultImage())) {
			userBean.setDefaultImage(mapper.map(user.getDefaultImage(), ImageBean.class));
		}
		
		if (Predicates.notNull.test(user.getDefaultCountry())) {
			userBean.setDefaultCountry(mapper.map(user.getDefaultCountry(), CountryBean.class));
		}
		
		if (Predicates.notNull.test(user.getDefaultLanguage())) {
			userBean.setDefaultLanguage(mapper.map(user.getDefaultLanguage(), LanguageBean.class));
		}
		
		if (Predicates.notNull.test(user.getAddress())) {
			userBean.setAddress(mapper.map(user.getAddress(), AddressBean.class));
		}
		
		if (Predicates.notNull.test(user.getCreatedBy())) {
			userBean.setCreatedBy(mapper.map(user.getCreatedBy(), UserBean.class));
		}
		
		if (Predicates.notNull.test(user.getModifiedBy())) {
			userBean.setModifiedBy(mapper.map(user.getModifiedBy(), UserBean.class));
		}
	}
	
	public void setBasicProperties(User user, String groupName) {
		Country defaultCountry = countryRepository.findByCode(Constants.DEFAULT_COUNTRY);
		user.setDefaultCountry(defaultCountry);
		
		Language defaultLanguage = languageRepository.findByCode(Constants.DEFAULT_LANGUAGE);
		user.setDefaultLanguage(defaultLanguage);
		
		Group defaultGroup = groupRepository.findGroupByName(groupName);
        user.setGroup(defaultGroup);
	}
	
	public UserBean mapCurrentUser(User user) {
		UserBean userBean = mapper.map(user, UserBean.class);
		userBean.setGroup(mapper.map(user.getGroup(), GroupBean.class));
		userBean.setDefaultImage(mapper.map(user.getDefaultImage(), ImageBean.class));
		userBean.setDefaultLanguage(mapper.map(user.getDefaultLanguage(), LanguageBean.class));
		userBean.setDefaultCountry(mapper.map(user.getDefaultCountry(), CountryBean.class));
		if (Predicates.notNull.test(user.getAddress())) {
			userBean.setAddress(mapper.map(user.getAddress(), AddressBean.class));
		}
		
		if (Predicates.notNull.test(user.getCompany())) {
			userBean.setCompany(mapper.map(user.getCompany(), CompanyBean.class));
		}
		
		return userBean;
	}
	
	@Override
	protected PrakratiEvent<Iterable<User>> createEvent(Iterable<User> entities,
			EventType event) {
		return new UserEvent(entities, event);
	}
	
}
