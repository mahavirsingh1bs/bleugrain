package com.carefarm.prakrati.service.helper;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.common.entity.Currency;
import com.carefarm.prakrati.common.entity.PricePerUnit;
import com.carefarm.prakrati.common.entity.Unit;
import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.delivery.entity.Permit;
import com.carefarm.prakrati.delivery.entity.Transport;
import com.carefarm.prakrati.entity.Capacity;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.repository.PermitRepository;
import com.carefarm.prakrati.search.events.PrakratiEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.search.events.TransportEvent;
import com.carefarm.prakrati.web.bean.CompanyBean;
import com.carefarm.prakrati.web.bean.PermitBean;
import com.carefarm.prakrati.web.bean.TransportBean;
import com.carefarm.prakrati.web.bean.UserBean;

@Component
public class TransportHelper extends AbstractHelper<TransportBean, Transport> {

	@Autowired
	private PermitRepository permitRepository;
	
	public void extractBasicFields(Transport entity, TransportBean bean) {
		bean.setCompany(mapper.map(entity.getCompany(), CompanyBean.class));
		Optional<User> driverOpt = Optional.ofNullable(entity.getDriver());
		driverOpt.ifPresent(driver -> bean.setDriver(mapper.map(driver, UserBean.class)));
		if (Predicates.notNull.test(entity.getCreatedBy())) {
			bean.setCreatedBy(mapper.map(entity.getCreatedBy(), UserBean.class));
		}
		
		if (Predicates.notNull.test(entity.getModifiedBy())) {
			bean.setModifiedBy(mapper.map(entity.getModifiedBy(), UserBean.class));
		}

		entity.getPermits().forEach(permit -> {
			bean.addSelectedPermitId(permit.getId());
			bean.addPermit(mapper.map(permit, PermitBean.class));
		});
	}
	
	public void convertBeanToEntity(Transport entity, TransportBean bean) {
		entity.setName(bean.getName());
		entity.setVehicleNo(bean.getVehicleNo());
		Capacity capacity = new Capacity(bean.getCapacity());
		capacity.setUnit(mapper.map(bean.getCapacityUnit(), Unit.class));
		
		PricePerUnit pricePerUnit = new PricePerUnit(bean.getPrice());
		pricePerUnit.setCurrency(mapper.map(bean.getPriceCurrency(), Currency.class));
		pricePerUnit.setUnit(mapper.map(bean.getPriceUnit(), Unit.class));
		
		List<Permit> permits = new ArrayList<>();
		bean.getSelectedPermitIds().forEach(id -> permits.add(permitRepository.findOne(id)));
		entity.setPermits(permits);
	}
	
	@Override
	protected PrakratiEvent<Iterable<Transport>> createEvent(
			Iterable<Transport> entities, EventType event) {
		return new TransportEvent(entities, event);
	}

}
