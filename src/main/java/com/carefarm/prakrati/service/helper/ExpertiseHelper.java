package com.carefarm.prakrati.service.helper;

import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.Expertise;
import com.carefarm.prakrati.web.bean.ExpertiseBean;

@Component
public class ExpertiseHelper extends AbstractHelper<ExpertiseBean, Expertise> {
	
	@Override
	public void convertBeanToEntity(Expertise entity, ExpertiseBean bean) {
		entity.setName(bean.getName());
		entity.setExperience(bean.getExperience());
		entity.setExperLabel(bean.getExperLabel());
	}
	
}
