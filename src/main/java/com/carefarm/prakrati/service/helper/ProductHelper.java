package com.carefarm.prakrati.service.helper;

import java.util.Optional;

import org.apache.camel.Produce;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.common.entity.Currency;
import com.carefarm.prakrati.common.entity.ProductStatus;
import com.carefarm.prakrati.common.entity.Unit;
import com.carefarm.prakrati.constants.Endpoints;
import com.carefarm.prakrati.entity.Address;
import com.carefarm.prakrati.entity.Bid;
import com.carefarm.prakrati.entity.Company;
import com.carefarm.prakrati.entity.Price;
import com.carefarm.prakrati.entity.Product;
import com.carefarm.prakrati.entity.Quantity;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.messaging.listener.ProductAssignedListener;
import com.carefarm.prakrati.messaging.listener.ProductListener;
import com.carefarm.prakrati.messaging.listener.ProductSoldOutListener;
import com.carefarm.prakrati.repository.BidRepository;
import com.carefarm.prakrati.search.events.PrakratiEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.search.events.ProductEvent;
import com.carefarm.prakrati.util.ProductState;
import com.carefarm.prakrati.web.bean.AddressBean;
import com.carefarm.prakrati.web.bean.BidBean;
import com.carefarm.prakrati.web.bean.CategoryBean;
import com.carefarm.prakrati.web.bean.CompanyBean;
import com.carefarm.prakrati.web.bean.CountryBean;
import com.carefarm.prakrati.web.bean.CurrencyBean;
import com.carefarm.prakrati.web.bean.ImageBean;
import com.carefarm.prakrati.web.bean.OwnerBean;
import com.carefarm.prakrati.web.bean.ProductBean;
import com.carefarm.prakrati.web.bean.StateBean;
import com.carefarm.prakrati.web.bean.UnitBean;
import com.carefarm.prakrati.web.bean.UserBean;

@Component
public class ProductHelper extends AbstractHelper<ProductBean, Product> {

	@Produce(uri = Endpoints.BIDDABLE_PRODUCTS)
	private ProductListener productListener;
	
	@Produce(uri = Endpoints.PRODUCT_ASSIGNED)
	private ProductAssignedListener productAssignedListener;
	
	@Produce(uri = Endpoints.PRODUCT_SOLDOUT)
	private ProductSoldOutListener productSoldOutListener;
	
	@Autowired
	private BidRepository bidRepository;
	
	@Override
	public ProductBean convertEntityToBean(Product product, Class<ProductBean> beanClazz) {
		ProductBean productBean = mapper.map(product, beanClazz);
		productBean.setDefaultImage(mapper.map(product.getDefaultImage(), ImageBean.class));
		productBean.setOwner(mapper.map(product.getOwner(), OwnerBean.class));
		Optional.ofNullable(product.getAssignedTo())
			.ifPresent(at -> productBean.setAssignedTo(mapper.map(at, UserBean.class)));
		return productBean;
	}
	
	public void convertBeanToEntity(Product product, ProductBean productBean) {
		product.setName(productBean.getName());
		product.setShortDesc(productBean.getShortDesc());
		product.setDesc(productBean.getDesc());
		Quantity quantity = new Quantity();
		quantity.setQuantity(productBean.getQuantity().doubleValue());
		quantity.setUnit(mapper.map(productBean.getQuantityUnit(), Unit.class));
		
		if (!quantity.equals(product.getQuantity())) {
			product.setQuantity(quantity);
		}
		
		Price currentPrice = new Price();
		currentPrice.setPrice(productBean.getPrice().doubleValue());
		currentPrice.setCurrency(mapper.map(productBean.getPriceCurrency(), Currency.class));
		currentPrice.setUnit(mapper.map(productBean.getPriceUnit(), Unit.class));
		
		if (!currentPrice.equals(product.getLatestPrice())) {
			product.addPrice(product.getLatestPrice());
			product.setLastPrice(product.getLatestPrice());
			product.setLatestPrice(currentPrice);
		}
		
		product.setStatus(mapper.map(productBean.getStatus(), ProductStatus.class));
		product.setOwner(mapper.map(productBean.getOwner(), User.class));
	}
	
	public void populateHighestBid(ProductBean entityBean, Product entity) {
		Optional<Bid> highestBidOpt = Optional.ofNullable(entity.getHighestBid());
		highestBidOpt.ifPresent(hb -> {
			BidBean highestBid = mapper.map(hb, BidBean.class);
			
			Optional<Company> companyOpt = Optional.ofNullable(hb.getCompany());
			companyOpt.ifPresent(c -> highestBid.setCompany(mapper.map(c, CompanyBean.class)));
			
			Optional<User> userOpt = Optional.ofNullable(hb.getUser());
			userOpt.ifPresent(u -> highestBid.setUser(mapper.map(u, UserBean.class)));
			
			highestBid.setCurrency(mapper.map(hb.getCurrency(), CurrencyBean.class));
			highestBid.setUnit(mapper.map(hb.getUnit(), UnitBean.class));
			entityBean.setHighestBid(highestBid);
		});		
	}
	
	public void sendBiddableProduct(Product p) {
		ProductBean product = this.mapBiddableProduct(p);
		productListener.sendBiddableProduct(product);
	}
	
	public ProductBean mapBiddableProduct(Product p) {
		ProductBean product = mapper.map(p, ProductBean.class);
		p.getCategories().forEach(c -> product.addCategory(mapper.map(c, CategoryBean.class)));
		
		Optional<Address> addressOpt = Optional.ofNullable(p.getAddress());
		addressOpt.ifPresent(a -> {
			AddressBean address = mapper.map(a, AddressBean.class);
			address.setState(mapper.map(a.getState(), StateBean.class));
			address.setCountry(mapper.map(a.getCountry(), CountryBean.class));
			product.setAddress(address);
		});
		
		Optional<Bid> highestBidOpt = Optional.ofNullable(p.getHighestBid());
		highestBidOpt.ifPresent(hb -> {
			this.populateHighestBid(product, p);
		});
		
		product.setOwner(mapper.map(p.getOwner(), OwnerBean.class));
		
		product.setBiddingCurrency(mapper.map(p.getBiddingCurrency(), CurrencyBean.class));
		product.setBiddingUnit(mapper.map(p.getBiddingUnit(), UnitBean.class));
		return product;
	}
	
	public void createAndSendMessages(Product product, String status) {
		ProductBean productBean = this.convertEntityToBean(product, ProductBean.class);
		if (status.equals(ProductState.ASSIGNED.getCode())) {
			productAssignedListener.notify(productBean);
		} else if (status.equals(ProductState.SOLD_OUT.getCode())) {
			productSoldOutListener.notify(productBean);
		}
	}
	
	@Override
	protected PrakratiEvent<Iterable<Product>> createEvent(Iterable<Product> entities,
			EventType event) {
		return new ProductEvent(entities, event);
	}
}
