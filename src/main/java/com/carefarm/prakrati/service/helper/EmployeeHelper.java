package com.carefarm.prakrati.service.helper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.repository.DepartmentRepository;
import com.carefarm.prakrati.repository.DesignationRepository;
import com.carefarm.prakrati.search.events.PrakratiEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.web.bean.DepartmentBean;
import com.carefarm.prakrati.web.bean.DesignationBean;
import com.carefarm.prakrati.web.bean.EmployeeBean;

@Component
public class EmployeeHelper extends AbstractHelper<EmployeeBean, User> {
	
	@Autowired
	private UserHelper userHelper;
	
	@Autowired
	private DepartmentRepository departmentRepository;
	
	@Autowired
	private DesignationRepository designationRepository;
	
	public void convertBeanToEntity(User employee, EmployeeBean employeeBean) {
		userHelper.convertBeanToEntity(employee, employeeBean);
		employee.setEmployeeId(employeeBean.getEmployeeId());
		employee.setDepartment(departmentRepository.findOne(employeeBean.getDepartmentId()));
		employee.setDesignation(designationRepository.findOne(employeeBean.getDesignationId()));
	}
	
	public void extractBasicFields(User employee, EmployeeBean employeeBean) {
		userHelper.extractBasicFields(employee, employeeBean);
		if (Predicates.notNull.test(employee.getDesignation())) {
			employeeBean.setDesignation(mapper.map(employee.getDesignation(), DesignationBean.class));
		}
		
		if (Predicates.notNull.test(employee.getDepartment())) {
			employeeBean.setDepartment(mapper.map(employee.getDepartment(), DepartmentBean.class));
		}
	}
	
	@Override
	protected PrakratiEvent<Iterable<User>> createEvent(Iterable<User> entities,
			EventType event) {
		return userHelper.createEvent(entities, event);
	}
	
}
