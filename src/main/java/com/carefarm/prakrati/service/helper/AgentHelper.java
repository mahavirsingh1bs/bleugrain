package com.carefarm.prakrati.service.helper;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.entity.Bid;
import com.carefarm.prakrati.entity.Demand;
import com.carefarm.prakrati.entity.Order;
import com.carefarm.prakrati.entity.Product;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.repository.CompanyRepository;
import com.carefarm.prakrati.repository.DemandRepository;
import com.carefarm.prakrati.repository.OrderRepository;
import com.carefarm.prakrati.repository.ProductRepository;
import com.carefarm.prakrati.search.events.PrakratiEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.util.DemandStatus;
import com.carefarm.prakrati.web.bean.AgentBean;
import com.carefarm.prakrati.web.bean.BidBean;
import com.carefarm.prakrati.web.bean.CompanyBean;
import com.carefarm.prakrati.web.bean.CurrencyBean;
import com.carefarm.prakrati.web.bean.DemandBean;
import com.carefarm.prakrati.web.bean.OrderBean;
import com.carefarm.prakrati.web.bean.ProductBean;
import com.carefarm.prakrati.web.bean.UnitBean;

@Component
public class AgentHelper extends AbstractHelper<AgentBean, User> {
	
	@Autowired
	private UserHelper userHelper;
	
	@Autowired
	private CompanyRepository companyRepository;

	@Autowired
	private DemandRepository demandRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private OrderRepository orderRepository;
	
	public void convertBeanToEntity(User agent, AgentBean agentBean) {
		userHelper.convertBeanToEntity(agent, agentBean);
		if (Predicates.notNull.test(agentBean.getCompanyId())) {
			agent.setCompany(companyRepository.findOne(agentBean.getCompanyId()));
		}
	}
	
	public void extractBasicFields(User agent, AgentBean agentBean) {
		userHelper.extractBasicFields(agent, agentBean);
		agentBean.setCompany(mapper.map(agent.getCompany(), CompanyBean.class));
		
		Pageable pageable = new PageRequest(0, 10);
		List<Order> orders = orderRepository.findByUserNoAndStatusCodesNotIn(agent.getUserNo(), notInOrderStatuses, pageable);
		orders.forEach(order -> agentBean.addOrder(mapper.map(order, OrderBean.class)));
		
		List<Demand> demands = demandRepository.findByCreatedByAndStatus(agent.getUserNo(), DemandStatus.OPEN.getCode());
		demands.forEach(demand -> agentBean.addDemand(mapper.map(demand, DemandBean.class)));
		
		List<Product> products = productRepository.findProductsBiddedByUser(agent.getUserNo(), pageable);
		products.forEach(product -> {
			Optional<Bid> optionalBid = product.highestBidOfUser(agent.getUserNo());
			optionalBid.ifPresent(bid -> {
				BidBean bidBean = mapper.map(bid, BidBean.class);
				ProductBean productBean = mapper.map(product, ProductBean.class);
				productBean.setQuantityUnit(mapper.map(product.getQuantity().getUnit(), UnitBean.class));
				bidBean.setProduct(productBean);
				bidBean.setCurrency(mapper.map(bid.getCurrency(), CurrencyBean.class));
				bidBean.setUnit(mapper.map(bid.getUnit(), UnitBean.class));
				agentBean.addBid(bidBean);
			});
		});
	}
	
	@Override
	protected PrakratiEvent<Iterable<User>> createEvent(Iterable<User> entities,
			EventType event) {
		return userHelper.createEvent(entities, event);
	}
	
}
