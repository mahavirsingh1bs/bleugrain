package com.carefarm.prakrati.service.helper;

import java.util.Optional;

import org.apache.camel.Produce;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.constants.Endpoints;
import com.carefarm.prakrati.entity.Demand;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.messaging.listener.DemandAssignedListener;
import com.carefarm.prakrati.messaging.listener.DemandFulfilledListener;
import com.carefarm.prakrati.search.events.DemandEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.util.DemandStatus;
import com.carefarm.prakrati.web.bean.DemandBean;
import com.carefarm.prakrati.web.bean.UserBean;

@Component
public class DemandHelper extends AbstractHelper<DemandBean, Demand> {
	
	private static final String COMPANY = "Company";
	
	@Produce(uri = Endpoints.DEMAND_ASSIGNED)
	private DemandAssignedListener demandAssignedListener;
	
	@Produce(uri = Endpoints.DEMAND_FULFILLED)
	private DemandFulfilledListener demandFulfilledListener;
	
	@Override
	public DemandBean convertEntityToBean(Demand entity, Class<DemandBean> beanClazz) {
		DemandBean demandBean = mapper.map(entity, beanClazz);
		demandBean.setCreatedBy(mapper.map(entity.getCreatedBy(), UserBean.class));
		Optional.ofNullable(entity.getAssignedTo())
			.ifPresent(at -> demandBean.setAssignedTo(mapper.map(at, UserBean.class)));
		return demandBean;
	}
	
	public void createAndSendMessages(Demand demand, String status) {
		DemandBean demandBean = this.convertEntityToBean(demand, DemandBean.class);
		if (status.equals(DemandStatus.ASSIGNED.getCode())) {
			demandAssignedListener.notify(demandBean);
		} else if (status.equals(DemandStatus.FULFILLED.getCode())) {
			demandFulfilledListener.notify(demandBean);
		}
	}

	public void setBuyerCategory(Demand demand, User currentUser) {
		if (currentUser.isACompanyUser()) {
			demand.setBuyerCategory(COMPANY);
		} else {
			demand.setBuyerCategory(currentUser.getGroup().getName());
		}
	}

	@Override
	protected PrakratiEvent<Iterable<Demand>> createEvent(Iterable<Demand> entities,
			EventType event) {
		return new DemandEvent(entities, event);
	}

}
