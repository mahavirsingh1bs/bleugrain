package com.carefarm.prakrati.service.helper;

import java.util.Optional;

import org.apache.camel.Produce;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.constants.Endpoints;
import com.carefarm.prakrati.entity.Bid;
import com.carefarm.prakrati.entity.Company;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.messaging.listener.BidListener;
import com.carefarm.prakrati.web.bean.BidBean;
import com.carefarm.prakrati.web.bean.CompanyBean;
import com.carefarm.prakrati.web.bean.CurrencyBean;
import com.carefarm.prakrati.web.bean.UnitBean;
import com.carefarm.prakrati.web.bean.UserBean;

@Component
public class BidHelper extends AbstractHelper<BidBean, Bid> {

	@Produce(uri = Endpoints.PRAKRATI_BIDS)
	private BidListener bidListener;

	public void sendNotification(Bid b) {
		BidBean bid = mapper.map(b, BidBean.class);
		
		Optional<User> userOpt = Optional.ofNullable(b.getUser());
		userOpt.ifPresent(u -> bid.setUser(mapper.map(u, UserBean.class)));
		
		Optional<Company> companyOpt = Optional.ofNullable(b.getCompany());
		companyOpt.ifPresent(c -> bid.setCompany(mapper.map(c, CompanyBean.class)));
		
		bid.setCurrency(mapper.map(b.getCurrency(), CurrencyBean.class));
		bid.setUnit(mapper.map(b.getUnit(), UnitBean.class));
		bid.setCreatedBy(mapper.map(b.getCreatedBy(), UserBean.class));
		bidListener.sendNotification(bid);
	}
}