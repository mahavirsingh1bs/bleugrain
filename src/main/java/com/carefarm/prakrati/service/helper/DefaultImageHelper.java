package com.carefarm.prakrati.service.helper;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.MessageFormat;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.ServletContext;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.carefarm.prakrati.creators.ImageCreator;
import com.carefarm.prakrati.entity.Image;
import com.carefarm.prakrati.service.dto.ImageDTO;
import com.carefarm.prakrati.util.ImageType;
import com.carefarm.prakrati.vo.ImageVO;

public class DefaultImageHelper extends ImageHelper {

	private static final Logger LOGGER = LoggerFactory.getLogger(DefaultImageHelper.class);
	
	public static Image handleImage (ImageDTO imageDTO) {
		ImageVO imageVO = imageDTO.getImageVO();
		String extension = StringUtils.substringAfter(imageVO.getContentType(), SLASH);
		String imageName = StringUtils.replaceChars(imageDTO.getName(), SPACE, UNDERSCORE) + "_" + Math.abs(new Random().nextInt());
		
		ImageType imageType = imageDTO.getImageType();
		String defaultImageUrl = MessageFormat.format(DEFAULT_IMAGE_URL, imageType.getType(), imageDTO.getId(), imageName, extension);
		String bigImageUrl = MessageFormat.format(BIG_IMAGE_URL, imageType.getType(), imageDTO.getId(), imageName, extension);
		String mediumImageUrl = MessageFormat.format(MEDIUM_IMAGE_URL, imageType.getType(), imageDTO.getId(), imageName, extension);
		String smallImageUrl = MessageFormat.format(SMALL_IMAGE_URL, imageType.getType(), imageDTO.getId(), imageName, extension);
    	
		ServletContext servletContext = imageDTO.getServletContext();
		String defaultFilePath = servletContext.getRealPath(SLASH) + defaultImageUrl;
		String bigFilePath = servletContext.getRealPath(SLASH) + bigImageUrl;
    	String mediumFilePath = servletContext.getRealPath(SLASH) + mediumImageUrl;
    	String smallFilePath = servletContext.getRealPath(SLASH) + smallImageUrl;

		try {	    	
	    	
			saveAndCreateImage(imageName, defaultImageUrl, defaultFilePath, imageVO);
			BufferedImage defaultImage = ImageIO.read(new File(defaultFilePath));
			int type = defaultImage.getType() == 0? BufferedImage.TYPE_INT_ARGB : defaultImage.getType();
			
			BufferedImage bigImageJpg = resizeImage(defaultImage, BIG_WIDTH, BIG_HEIGHT, type);
			BufferedImage mediumImageJpg = resizeImage(defaultImage, MEDIUM_WIDTH, MEDIUM_HEIGHT, type);
			BufferedImage smallImageJpg = resizeImage(defaultImage, SMALL_WIDTH, SMALL_HEIGHT, type);
			
			checkAndCreateParentDirectory(bigFilePath);
			ImageIO.write(bigImageJpg, extension, new File(bigFilePath));
			
			checkAndCreateParentDirectory(mediumFilePath);
			ImageIO.write(mediumImageJpg, extension, new File(mediumFilePath));
			
			checkAndCreateParentDirectory(smallFilePath);
			ImageIO.write(smallImageJpg, extension, new File(smallFilePath));
	    	
	    	return ImageCreator.createInstance(imageName, bigImageUrl, smallImageUrl, mediumImageUrl, bigImageUrl);
		} catch (IOException e) {
			LOGGER.info("Got An error while saving the images");
			throw new RuntimeException();
		}
	}
	
	private static void saveAndCreateImage(String filename, String imageUrl, String filePath, ImageVO imageVO) {
		try {
    		if (Files.notExists(Paths.get(filePath).getParent())) {
    			Files.createDirectories(Paths.get(filePath).getParent());
    		}
    		Files.write(Paths.get(filePath), imageVO.getData(), StandardOpenOption.CREATE);
		} catch (Exception e) {
			LOGGER.error("An error occurred while saving image" + e.getMessage(), e.getCause());
		}
	}
	
	private static void checkAndCreateParentDirectory(String filePath) {
		try {
    		if (Files.notExists(Paths.get(filePath).getParent())) {
    			Files.createDirectories(Paths.get(filePath).getParent());
    		}
		} catch (Exception e) {
			LOGGER.error("An error occurred while creating parent directory " + e.getMessage(), e.getCause());
		}
	}
	
}
