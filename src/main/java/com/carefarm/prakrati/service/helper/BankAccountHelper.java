package com.carefarm.prakrati.service.helper;

import org.springframework.stereotype.Component;

import com.carefarm.prakrati.payment.entity.BankAccount;
import com.carefarm.prakrati.web.bean.BankAccountBean;

@Component
public class BankAccountHelper extends AbstractHelper<BankAccountBean, BankAccount>{
	
}
