package com.carefarm.prakrati.service.helper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.payment.repository.BankAccountRepository;
import com.carefarm.prakrati.repository.DemandRepository;
import com.carefarm.prakrati.search.events.PrakratiEvent;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.web.bean.BankAccountBean;
import com.carefarm.prakrati.web.bean.DemandBean;
import com.carefarm.prakrati.web.bean.RetailerBean;

@Component
public class RetailerHelper extends AbstractHelper<RetailerBean, User> {
	
	@Autowired
	private UserHelper userHelper;
	
	@Autowired
	private BankAccountRepository bankAccountRepository;
	
	@Autowired
	private DemandRepository demandRepository;
	
	public void convertBeanToEntity(User retailer, RetailerBean retailerBean) {
		userHelper.convertBeanToEntity(retailer, retailerBean);
	}

	public void extractBasicFields(User retailer, RetailerBean retailerBean) {
		userHelper.extractBasicFields(retailer, retailerBean);
		
		PageRequest pageRequest = new PageRequest(0, 3);
		
		List<BankAccountBean> bankAccounts = new ArrayList<>();
		bankAccountRepository
			.findActiveAccountsByUserNo(retailer.getUserNo(), pageRequest)
			.forEach(bankAccount -> bankAccounts.add(mapper.map(bankAccount, BankAccountBean.class)));
		retailerBean.setBankAccounts(bankAccounts);
		
		List<DemandBean> demands = new ArrayList<>();
		demandRepository
			.findByUserId(retailer.getId(), pageRequest)
			.forEach(demand -> demands.add(mapper.map(demand, DemandBean.class)));
		retailerBean.setDemands(demands);
	}
	
	public void setBasicProperties(User retailer, String groupName) {
		userHelper.setBasicProperties(retailer, groupName);
	}

	@Override
	protected PrakratiEvent<Iterable<User>> createEvent(Iterable<User> entities,
			EventType event) {
		return userHelper.createEvent(entities, event);
	}
	
}
