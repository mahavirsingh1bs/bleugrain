package com.carefarm.prakrati.service.helper;

import org.springframework.stereotype.Component;

import com.carefarm.prakrati.payment.entity.CreditCard;
import com.carefarm.prakrati.web.bean.CreditCardBean;

@Component
public class CreditCardHelper extends AbstractHelper<CreditCardBean, CreditCard> {
	
}
