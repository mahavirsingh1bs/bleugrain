package com.carefarm.prakrati.service;

import java.util.List;
import java.util.Map;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.web.bean.AddressBean;
import com.carefarm.prakrati.web.bean.AddressEntityBean;
import com.carefarm.prakrati.web.model.AddressEntityForm;
import com.carefarm.prakrati.web.model.AddressForm;

public interface AddressService {
	
	List<AddressEntityBean> findDeliveryAddresses(Map<String, Object> params);
	
	List<AddressEntityBean> findBillingAddresses(Map<String, Object> params);
	
	AddressEntityBean findDefaultDeliveryAddress(Map<String, Object> params);
	
	AddressEntityBean findDefaultBillingAddress(Map<String, Object> params);

	void addAddress(AddressForm addressForm, String userNo, User currentUser);
	
	void editAddress(AddressBean addressBean, String userNo, User currentUser);
	
	void addBillingAddress(AddressEntityForm billingAddress, String userNo, User currentUser);
	
	void addDeliveryAddress(AddressEntityForm deliveryAddress, String userNo, User currentUser);
	
	void editAddress(AddressEntityBean addressBean, String userNo, User currentUser);

	void delete(String uniqueId, User modifiedBy);

	AddressEntityBean findAddressDetails(String uniqueId);

	boolean updateAddress(AddressEntityBean address, Map<String, Object> params, User updatedBy);

	void setDefaultAddress(Long id, String category, String userNo, User currentUser);
	
}
