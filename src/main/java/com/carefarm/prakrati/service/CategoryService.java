package com.carefarm.prakrati.service;

import java.util.List;
import java.util.Map;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.web.bean.CategoryBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.CategoryForm;

public interface CategoryService {
	
	List<CategoryBean> findMainCategories();
	
	List<CategoryBean> findAll();
	
	Map<String, Object> findByKeyword(SearchRequest searchRequest);

	void create(CategoryForm form, User user) throws ServiceException;
	
	CategoryBean findDetails(long categoryId, Object object);

	void update(CategoryBean categoryBean, User modifiedBy) throws ServiceException;
	
	void delete(String uniqueId);

	List<CategoryBean> findByMatchingKeywords(String keywords);
}
