package com.carefarm.prakrati.service.dto;

import java.util.Date;
import java.util.List;

import lombok.Getter;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.util.NotificationCategory;

@Getter
public class NotificationDTO {
	private NotificationCategory category;
	private String notification;
	private Date startDate;
	private Date expiryDate;
	private List<String> groups;
	private List<String> companies;
	private List<String> users;
	private User currentUser;
	
	public static class Builder {
		private NotificationDTO notification = new NotificationDTO();
		
		public static Builder getInstance() {
			return new Builder();
		}
		
		public Builder category(NotificationCategory category) {
			notification.category = category;
			return this;
		}

		public Builder notification(String notif) {
			notification.notification = notif;
			return this;
		}

		public Builder startDate(Date startDate) {
			notification.startDate = startDate;
			return this;
		}

		public Builder expiryDate(Date expiryDate) {
			notification.expiryDate = expiryDate;
			return this;
		}
		
		public Builder groups(List<String> groups) {
			notification.groups = groups;
			return this;
		}

		public Builder companies(List<String> companies) {
			notification.companies = companies;
			return this;
		}

		public Builder users(List<String> users) {
			notification.users = users;
			return this;
		}

		public Builder currentUser(User currentUser) {
			notification.currentUser = currentUser;
			return this;
		}

		public NotificationDTO build() {
			return notification;
		}
	}

	private NotificationDTO() { }

}
