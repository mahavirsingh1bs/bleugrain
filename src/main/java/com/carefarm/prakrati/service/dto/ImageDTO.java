package com.carefarm.prakrati.service.dto;

import javax.servlet.ServletContext;

import lombok.Getter;

import com.carefarm.prakrati.util.ImageType;
import com.carefarm.prakrati.vo.ImageVO;

@Getter
public class ImageDTO {
	private Long id;
	private String name;
	private ImageType imageType;
	private ImageVO imageVO;
	private ServletContext servletContext;

	public static class Builder {
		private Long id;
		private String name;
		private ImageType imageType;
		private ImageVO imageVO;
		private ServletContext servletContext;

		public static Builder getInstance() {
			return new Builder();
		}
		
		public Builder id(Long id) {
			this.id = id;
			return this;
		}

		public Builder name(String name) {
			this.name = name;
			return this;
		}

		public Builder imageType(ImageType imageType) {
			this.imageType = imageType;
			return this;
		}

		public Builder imageVO(ImageVO imageVO) {
			this.imageVO = imageVO;
			return this;
		}

		public Builder servletContext(ServletContext servletContext) {
			this.servletContext = servletContext;
			return this;
		}

		public ImageDTO build() {
			return new ImageDTO(this);
		}
	}

	private ImageDTO(Builder builder) {
		this.id = builder.id;
		this.name = builder.name;
		this.imageType = builder.imageType;
		this.imageVO = builder.imageVO;
		this.servletContext = builder.servletContext;
	}
}
