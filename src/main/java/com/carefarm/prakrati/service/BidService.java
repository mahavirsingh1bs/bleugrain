package com.carefarm.prakrati.service;

import java.util.List;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.web.bean.BidBean;
import com.carefarm.prakrati.web.bean.BidsRequest;
import com.carefarm.prakrati.web.model.BidForm;

public interface BidService {
	
	List<BidBean> findByUniqueId(BidsRequest bidsRequest);
	
	public List<BidBean> findByUser(BidsRequest bidsRequest);
	
	public List<BidBean> findByCompany(BidsRequest bidsRequest);
	
	void place(BidForm form, User currentUser);
	
}
