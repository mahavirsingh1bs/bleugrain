package com.carefarm.prakrati.service;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.web.bean.SocialAccountBean;
import com.carefarm.prakrati.web.model.SocialAccountForm;

public interface SocialAccountService {

	void addSocialAccount(SocialAccountForm socialAccountForm, String userNo, User currentUser);

	SocialAccountBean find(String uniqueId);

	void update(SocialAccountBean bean, User currentUser);

	void remove(String uniqueId, User currentUser);
	
}
