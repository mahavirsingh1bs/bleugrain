package com.carefarm.prakrati.service;

import java.util.List;

import com.carefarm.prakrati.web.bean.OrderStatusBean;

public interface OrderStatusService {
	
	List<OrderStatusBean> findAll();
}
