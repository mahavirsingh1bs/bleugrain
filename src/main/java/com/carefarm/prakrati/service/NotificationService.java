package com.carefarm.prakrati.service;

import java.util.List;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.service.dto.NotificationDTO;
import com.carefarm.prakrati.web.bean.NotificationBean;
import com.carefarm.prakrati.web.model.NotificationForm;

public interface NotificationService {

	void sendNotification(NotificationForm notificationForm, String userNo, User currentUser);
	
	void sendNotification(String topic, NotificationDTO notification, User currentUser);
	
	void sendNotification(String topic, NotificationDTO notification);

	List<NotificationBean> findAll(User currentUser);
	
}
