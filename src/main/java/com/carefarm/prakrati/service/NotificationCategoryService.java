package com.carefarm.prakrati.service;

import java.util.List;

import com.carefarm.prakrati.web.bean.NotificationCategoryBean;

public interface NotificationCategoryService {
	
	List<NotificationCategoryBean> findAll();
	
}
