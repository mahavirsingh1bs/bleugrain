package com.carefarm.prakrati.service;

import java.util.Map;

import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.web.model.ContactUsForm;
import com.carefarm.prakrati.web.model.FeedbackForm;

public interface CustomerCareService {
	void processConcern(ContactUsForm form, Map<String, Object> params) throws ServiceException;
	void processFeedback(FeedbackForm form, Map<String, Object> params) throws ServiceException;
}
