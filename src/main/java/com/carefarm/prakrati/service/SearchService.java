package com.carefarm.prakrati.service;

import java.util.Map;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.search.dto.ModelMap;
import com.carefarm.prakrati.search.dto.SearchCriteria;
import com.carefarm.prakrati.web.bean.DemandBean;
import com.carefarm.prakrati.web.bean.ProductBean;

public interface SearchService {

	ModelMap<ProductBean> search(SearchCriteria criteria);

	ModelMap<DemandBean> searchDemands(SearchCriteria criteria);

	Map<String, Object> findProductDetail(String uniqueId, User currentUser);
}
