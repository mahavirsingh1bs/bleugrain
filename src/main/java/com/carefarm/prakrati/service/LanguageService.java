package com.carefarm.prakrati.service;

import java.util.List;

import com.carefarm.prakrati.web.bean.LanguageBean;

public interface LanguageService {

	List<LanguageBean> findByMatchingKeywords(String keywords);
	
}
