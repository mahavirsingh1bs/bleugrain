package com.carefarm.prakrati.service;

import java.util.Map;

import com.carefarm.prakrati.web.bean.SearchRequest;

public interface PricingService {

	Map<String, Object> findProducts(SearchRequest searchRequest);

	Map<String, Object> findDemands(SearchRequest searchRequest);

}
