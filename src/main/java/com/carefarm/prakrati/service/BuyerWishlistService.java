package com.carefarm.prakrati.service;

import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Pageable;

import com.carefarm.prakrati.entity.BuyerWishlist;

public interface BuyerWishlistService {
	
	List<BuyerWishlist> findCompanyWishlist(Long companyId);
	
	Map<String, Object> findByKeyword(String keyword, Pageable pageable,
			Map<String, Object> params);
}
