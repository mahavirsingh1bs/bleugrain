package com.carefarm.prakrati.accounts.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.carefarm.prakrati.accounts.service.DriverService;
import com.carefarm.prakrati.entity.Company;
import com.carefarm.prakrati.entity.Order;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.repository.DemandRepository;
import com.carefarm.prakrati.repository.GroupRepository;
import com.carefarm.prakrati.repository.OrderRepository;
import com.carefarm.prakrati.repository.ProductRepository;
import com.carefarm.prakrati.service.helper.DriverHelper;
import com.carefarm.prakrati.web.bean.DriverBean;
import com.carefarm.prakrati.web.bean.OrderBean;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.model.DriverForm;
import com.carefarm.prakrati.web.model.UserForm;

@Service
public class DriverServiceImpl extends UserProfileServiceImpl implements DriverService {

	private static final Logger LOGGER = LoggerFactory.getLogger(DriverServiceImpl.class);
	
	@Value("${driver.company.image.default.url}")
	private String defaultDriverImageUrl;
	
	@Autowired
	private DozerBeanMapper mapper;
	
	@Autowired
	private GroupRepository groupRepository;
		
	@Autowired
	private DemandRepository demandRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private DriverHelper driverHelper;

	@Override
	public List<UserBean> findByKeyword(Iterable<User> drivers) {
		List<UserBean> driverBeans = new ArrayList<>();
		for (User driver : drivers) {
			DriverBean driverBean = mapper.map(driver, DriverBean.class);
			driverHelper.extractBasicFields(driver, driverBean);
			driverBeans.add(driverBean);
		}
		return driverBeans;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public DriverBean findDetails(String userNo) {
		LOGGER.info("Finding driver detail");
		User entity = userRepository.findByUserNo(userNo);
		DriverBean bean = mapper.map(entity, DriverBean.class);
		driverHelper.extractBasicFields(entity, bean);
		return bean;
	}
	
	@Override
	public void createInternal(UserForm form, MultipartFile image, User user) {
		DriverForm driverForm = (DriverForm )form;
		Company company = companyRepository.findByUniqueId(driverForm.getCompanyId());
		user.setCompany(company);
		user.setBusinessCategory(company.getBusinessCategory());
	}

	@Override
	public Map<String, Object> getProfileDetailsInternal(Map<String, Object> profileDetails, User user) {
		Pageable pageable = new PageRequest(0, 10);
		
		List<OrderBean> orderBeans = new ArrayList<>();
		List<Order> orders = orderRepository.findByUserNoAndStatusCodesNotIn(user.getUserNo(), notInOrderStatuses, pageable);
		orders.forEach(order -> { 
			orderBeans.add(mapper.map(order, OrderBean.class)); 
		});
		profileDetails.put("orders", orderBeans);
		
		return profileDetails;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(DriverBean driverBean, User currentUser) throws ServiceException {
		LOGGER.info("Updating the driver detail");
		User driver = userRepository.findByUserNo(driverBean.getUserNo());
		driverHelper.convertBeanToEntity(driver, driverBean);
		setModifiedAndPublishEvents(driver, currentUser);
	}

}
