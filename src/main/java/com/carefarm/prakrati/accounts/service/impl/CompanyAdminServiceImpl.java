package com.carefarm.prakrati.accounts.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.carefarm.prakrati.accounts.service.CompanyAdminService;
import com.carefarm.prakrati.entity.Bid;
import com.carefarm.prakrati.entity.Demand;
import com.carefarm.prakrati.entity.Order;
import com.carefarm.prakrati.entity.Product;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.repository.DemandRepository;
import com.carefarm.prakrati.repository.GroupRepository;
import com.carefarm.prakrati.repository.OrderRepository;
import com.carefarm.prakrati.repository.ProductRepository;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.service.helper.CompanyAdminHelper;
import com.carefarm.prakrati.util.DemandStatus;
import com.carefarm.prakrati.web.bean.BidBean;
import com.carefarm.prakrati.web.bean.CompanyAdminBean;
import com.carefarm.prakrati.web.bean.CurrencyBean;
import com.carefarm.prakrati.web.bean.DemandBean;
import com.carefarm.prakrati.web.bean.OrderBean;
import com.carefarm.prakrati.web.bean.ProductBean;
import com.carefarm.prakrati.web.bean.UnitBean;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.model.CompanyAdminForm;
import com.carefarm.prakrati.web.model.UserForm;

@Service
public class CompanyAdminServiceImpl extends UserProfileServiceImpl implements CompanyAdminService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CompanyAdminServiceImpl.class);
	
	@Value("${admin.company.image.default.url}")
	private String defaultCompanyAdminImageUrl;
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private GroupRepository groupRepository;

	@Autowired
	private DemandRepository demandRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private CompanyAdminHelper companyAdminHelper;
	
	@Override
	public List<UserBean> findByKeyword(Iterable<User> users) {
		List<UserBean> userBeans = new ArrayList<>();
		for (User user : users) {
			CompanyAdminBean userBean = mapper.map(user, CompanyAdminBean.class);
			companyAdminHelper.extractBasicFields(user, userBean);
			userBeans.add(userBean);
		}
		return userBeans;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public CompanyAdminBean findDetail(String userNo) {
		LOGGER.info("Finding the company admin detail");
		User companyAdmin = userRepository.findByUserNo(userNo);
		CompanyAdminBean companyAdminBean = mapper.map(companyAdmin, CompanyAdminBean.class);
		companyAdminHelper.extractBasicFields(companyAdmin, companyAdminBean);
		return companyAdminBean;
	}

	@Override
	public void createInternal(UserForm form, MultipartFile image, User user) {
		CompanyAdminForm adminForm = (CompanyAdminForm )form;
		user.setCompany(companyRepository.findByUniqueId(adminForm.getCompanyId()));
	}
	
	@Override
	public Map<String, Object> getProfileDetailsInternal(Map<String, Object> profileDetails, User user) {
		Pageable pageable = new PageRequest(0, 10);
		
		List<OrderBean> orderBeans = new ArrayList<>();
		List<Order> orders = orderRepository.findByUserNoAndStatusCodesNotIn(user.getUserNo(), notInOrderStatuses, pageable);
		orders.forEach(order -> { 
			orderBeans.add(mapper.map(order, OrderBean.class)); 
		});
		profileDetails.put("orders", orderBeans);
		
		List<DemandBean> demandBeans = new ArrayList<>();
		List<Demand> demands = demandRepository.findByCreatedByAndStatus(user.getUserNo(), DemandStatus.OPEN.getCode());
		demands.forEach(demand -> { 
			demandBeans.add(mapper.map(demand, DemandBean.class));
		});
		profileDetails.put("demands", demandBeans);
		
		List<BidBean> bidBeans = new ArrayList<>();
		
		List<Product> products = productRepository.findProductsBiddedByUser(user.getUserNo(), pageable);
		products.forEach(product -> {
			Optional<Bid> optionalBid = product.highestBidOfUser(user.getUserNo());
			optionalBid.ifPresent(bid -> {
				BidBean bidBean = mapper.map(bid, BidBean.class);
				ProductBean productBean = mapper.map(product, ProductBean.class);
				productBean.setQuantityUnit(mapper.map(product.getQuantity().getUnit(), UnitBean.class));
				bidBean.setProduct(productBean);
				bidBean.setCurrency(mapper.map(bid.getCurrency(), CurrencyBean.class));
				bidBean.setUnit(mapper.map(bid.getUnit(), UnitBean.class));
				bidBeans.add(bidBean);
			});
		});
		profileDetails.put("bids", bidBeans);
		
		return profileDetails;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(CompanyAdminBean compAdminBean, User currentUser)
			throws ServiceException {
		LOGGER.info("Updating the company admin detail");
		User companyAdmin = userRepository.findByUserNo(compAdminBean.getUserNo());
		companyAdminHelper.convertBeanToEntity(companyAdmin, compAdminBean);
		setModifiedAndPublishEvents(companyAdmin, currentUser);
	}

}
