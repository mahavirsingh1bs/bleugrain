package com.carefarm.prakrati.accounts.service;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.web.model.RegisterForm;

public interface RegisterableService {
	User register(RegisterForm form) throws ServiceException;
}
