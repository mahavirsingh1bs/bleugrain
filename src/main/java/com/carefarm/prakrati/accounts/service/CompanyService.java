package com.carefarm.prakrati.accounts.service;

import java.util.List;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.vo.ImageVO;
import com.carefarm.prakrati.web.bean.CompanyBean;
import com.carefarm.prakrati.web.bean.ImageBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.CompanyForm;
import com.carefarm.prakrati.web.model.RegisterForm;

public interface CompanyService {
	
	Map<String, Object> findByKeyword(SearchRequest searchRequest);
	
	void register(RegisterForm form) throws ServiceException;
	
	void create(CompanyForm form, MultipartFile companyLogo, MultipartFile adminImage, User user) throws ServiceException;
	
	CompanyBean findDetails(String uniqueId, Map<String, Object> params);

	void update(CompanyBean companyBean, User modifiedBy) throws ServiceException;
	
	void delete(String uniqueId);
	
	List<CompanyBean> findByMatchingKeywords(String keywords);
	
	ImageBean changeCompanyLogo(ImageVO companyLogo, String uniqueId, User modifiedBy);
	
}
