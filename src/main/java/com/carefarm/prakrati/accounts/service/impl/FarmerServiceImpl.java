package com.carefarm.prakrati.accounts.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.accounts.service.FarmerService;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.payment.repository.BankAccountRepository;
import com.carefarm.prakrati.repository.ProductRepository;
import com.carefarm.prakrati.service.helper.FarmerHelper;
import com.carefarm.prakrati.util.ProductState;
import com.carefarm.prakrati.web.bean.ExpertiseBean;
import com.carefarm.prakrati.web.bean.FarmerBean;
import com.carefarm.prakrati.web.bean.ProductBean;
import com.carefarm.prakrati.web.bean.ReviewBean;
import com.carefarm.prakrati.web.bean.UserBean;

@Service
public class FarmerServiceImpl extends UserProfileServiceImpl implements FarmerService {

	private static final Logger LOGGER = LoggerFactory.getLogger(FarmerServiceImpl.class);
	
	@Value("${farmer.image.default.url}")
	private String defaultFarmerImageUrl;

	@Value("${farmer.verify.mobile.enabled}")
	private boolean isVerifyMobileEnabled;
	
	@Value("${farmer.verify.email.enabled}")
	private boolean isVerifyEmailEnabled;
	
	@Autowired
	private BankAccountRepository bankAccountRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private FarmerHelper farmerHelper;

	@Override
	public List<UserBean> findByKeyword(Iterable<User> users) {
		List<UserBean> farmerBeans = new ArrayList<>();
		for (User user : users) {
			FarmerBean farmerBean = mapper.map(user, FarmerBean.class);
			farmerHelper.extractBasicFields(user, farmerBean);
			farmerBeans.add(farmerBean);
		}
		return farmerBeans;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public FarmerBean findDetails(String userNo, Object object) {
		User farmer = userRepository.findByUserNo(userNo);
		FarmerBean farmerBean = mapper.map(farmer, FarmerBean.class);
		farmerHelper.extractBasicFields(farmer, farmerBean);
		return farmerBean;
	}

	@Override
	public Map<String, Object> getProfileDetailsInternal(Map<String, Object> profileDetails, User user) {
		List<ReviewBean> reviewBeans = new ArrayList<>();
		user.getReviews()
			.forEach(review -> 
				reviewBeans.add(mapper.map(review, ReviewBean.class)));
		profileDetails.put("reviews", reviewBeans);
		
		List<ProductBean> productBeans = new ArrayList<>();
		productRepository
			.findByUserNoAndStatus(user.getUserNo(), ProductState.ADDED_NEW.getCode())
			.forEach(product -> 
					productBeans.add(mapper.map(product, ProductBean.class)));
		profileDetails.put("products", productBeans);
		
		List<ExpertiseBean> expertiseBeans = new ArrayList<>();
		user.getExpertises()
			.forEach(expertise -> 
				expertiseBeans.add(mapper.map(expertise, ExpertiseBean.class)));
		
		profileDetails.put("expertises", expertiseBeans);
		return profileDetails;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(FarmerBean farmerBean, User currentUser) throws ServiceException {
		LOGGER.info("Updating farmer details");
		User farmer = userRepository.findByUserNo(farmerBean.getUserNo());
		farmerHelper.convertBeanToEntity(farmer, farmerBean);
		setModifiedAndPublishEvents(farmer, currentUser);
	}

}
