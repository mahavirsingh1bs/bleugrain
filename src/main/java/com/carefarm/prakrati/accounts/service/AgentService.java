package com.carefarm.prakrati.accounts.service;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.web.bean.AgentBean;

public interface AgentService extends UserProfileService {
	
	AgentBean findDetail(String userNo);
	
	void update(AgentBean agentBean, User currentUser) throws ServiceException;
	
}
