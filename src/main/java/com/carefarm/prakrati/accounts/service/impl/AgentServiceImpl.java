package com.carefarm.prakrati.accounts.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.carefarm.prakrati.accounts.service.AgentService;
import com.carefarm.prakrati.entity.Bid;
import com.carefarm.prakrati.entity.Company;
import com.carefarm.prakrati.entity.Demand;
import com.carefarm.prakrati.entity.Order;
import com.carefarm.prakrati.entity.Product;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.repository.DemandRepository;
import com.carefarm.prakrati.repository.GroupRepository;
import com.carefarm.prakrati.repository.OrderRepository;
import com.carefarm.prakrati.repository.ProductRepository;
import com.carefarm.prakrati.service.helper.AgentHelper;
import com.carefarm.prakrati.util.DemandStatus;
import com.carefarm.prakrati.web.bean.AgentBean;
import com.carefarm.prakrati.web.bean.BidBean;
import com.carefarm.prakrati.web.bean.CurrencyBean;
import com.carefarm.prakrati.web.bean.DemandBean;
import com.carefarm.prakrati.web.bean.OrderBean;
import com.carefarm.prakrati.web.bean.ProductBean;
import com.carefarm.prakrati.web.bean.UnitBean;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.model.AgentForm;
import com.carefarm.prakrati.web.model.UserForm;

@Service
public class AgentServiceImpl extends UserProfileServiceImpl implements AgentService {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AgentServiceImpl.class);
	
	@Value("${agent.company.image.default.url}")
	private String defaultAgentImageUrl;
	
	@Autowired
	private DozerBeanMapper mapper;
	
	@Autowired
	private GroupRepository groupRepository;
		
	@Autowired
	private DemandRepository demandRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private AgentHelper agentHelper;

	@Override
	public List<UserBean> findByKeyword(Iterable<User> users) {
		List<UserBean> userBeans = new ArrayList<>();
		for (User user : users) {
			AgentBean userBean = mapper.map(user, AgentBean.class);
			agentHelper.extractBasicFields(user, userBean);
			userBeans.add(userBean);
		}
		return userBeans;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public AgentBean findDetail(String userNo) {
		LOGGER.info("Finding agent detail");
		User agent = userRepository.findByUserNo(userNo);
		AgentBean agentBean = mapper.map(agent, AgentBean.class);
		agentHelper.extractBasicFields(agent, agentBean);
		
		return agentBean;
	}
	
	@Override
	public void createInternal(UserForm form, MultipartFile image, User user) {
		AgentForm agentForm = (AgentForm )form;
		Company company = companyRepository.findByUniqueId(agentForm.getCompanyId());
		user.setCompany(company);
		user.setBusinessCategory(company.getBusinessCategory());
	}

	@Override
	public Map<String, Object> getProfileDetailsInternal(Map<String, Object> profileDetails, User user) {
		Pageable pageable = new PageRequest(0, 10);
		
		List<OrderBean> orderBeans = new ArrayList<>();
		List<Order> orders = orderRepository.findByUserNoAndStatusCodesNotIn(user.getUserNo(), notInOrderStatuses, pageable);
		orders.forEach(order -> { 
			orderBeans.add(mapper.map(order, OrderBean.class)); 
		});
		profileDetails.put("orders", orderBeans);
		
		List<DemandBean> demandBeans = new ArrayList<>();
		List<Demand> demands = demandRepository.findByCreatedByAndStatus(user.getUserNo(), DemandStatus.OPEN.getCode());
		demands.forEach(demand -> { 
			demandBeans.add(mapper.map(demand, DemandBean.class));
		});
		profileDetails.put("demands", demandBeans);
		
		List<BidBean> bidBeans = new ArrayList<>();
		
		List<Product> products = productRepository.findProductsBiddedByUser(user.getUserNo(), pageable);
		products.forEach(product -> {
			Optional<Bid> optionalBid = product.highestBidOfUser(user.getUserNo());
			optionalBid.ifPresent(bid -> {
				BidBean bidBean = mapper.map(bid, BidBean.class);
				ProductBean productBean = mapper.map(product, ProductBean.class);
				productBean.setQuantityUnit(mapper.map(product.getQuantity().getUnit(), UnitBean.class));
				bidBean.setProduct(productBean);
				bidBean.setCurrency(mapper.map(bid.getCurrency(), CurrencyBean.class));
				bidBean.setUnit(mapper.map(bid.getUnit(), UnitBean.class));
				bidBeans.add(bidBean);
			});
		});
		profileDetails.put("bids", bidBeans);
		
		return profileDetails;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(AgentBean agentBean, User currentUser) throws ServiceException {
		LOGGER.info("Updating the agent detail");
		User agent = userRepository.findByUserNo(agentBean.getUserNo());
		agentHelper.convertBeanToEntity(agent, agentBean);
		setModifiedAndPublishEvents(agent, currentUser);
	}

}
