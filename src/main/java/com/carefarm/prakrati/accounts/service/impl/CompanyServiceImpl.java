package com.carefarm.prakrati.accounts.service.impl;

import static com.carefarm.prakrati.common.util.Constants.PLUS;
import static com.carefarm.prakrati.util.ImageCategory.COMPANY;
import static com.carefarm.prakrati.util.UserType.AGENT;
import static com.carefarm.prakrati.util.UserType.COMPANY_ADMIN;

import java.sql.Date;
import java.time.Clock;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.carefarm.prakrati.accounts.service.CompanyService;
import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.entity.Company;
import com.carefarm.prakrati.entity.Group;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.generators.UserNoGenerator;
import com.carefarm.prakrati.repository.CompanyRepository;
import com.carefarm.prakrati.repository.GroupRepository;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.search.repository.CompanySearchRepository;
import com.carefarm.prakrati.security.PasswordManager;
import com.carefarm.prakrati.service.helper.AmazonS3Helper;
import com.carefarm.prakrati.service.helper.CompanyHelper;
import com.carefarm.prakrati.service.helper.UserHelper;
import com.carefarm.prakrati.service.impl.PrakratiServiceImpl;
import com.carefarm.prakrati.util.BusinessCategory;
import com.carefarm.prakrati.util.ImageCategory;
import com.carefarm.prakrati.util.UserType;
import com.carefarm.prakrati.vo.ImageVO;
import com.carefarm.prakrati.web.bean.AddressBean;
import com.carefarm.prakrati.web.bean.AddressEntityBean;
import com.carefarm.prakrati.web.bean.AgentBean;
import com.carefarm.prakrati.web.bean.CompanyAdminBean;
import com.carefarm.prakrati.web.bean.CompanyBean;
import com.carefarm.prakrati.web.bean.ExpertiseBean;
import com.carefarm.prakrati.web.bean.ImageBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.bean.SocialAccountBean;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.model.CompanyForm;
import com.carefarm.prakrati.web.model.RegisterForm;
import com.carefarm.prakrati.web.util.PrakratiConstants;

@Service
public class CompanyServiceImpl extends PrakratiServiceImpl implements CompanyService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CompanyServiceImpl.class);
	
	private static final String COMPANY_ADMIN_GROUP = "Company Admin";

	@Value("${company.image.default.url}")
	private String defaultCompanyLogoUrl;

	@Value("${admin.company.image.default.url}")
	private String defaultCompanyAdminImageUrl;
	
	@Autowired
	private CompanyRepository companyRepository;
	
	@Autowired
	private CompanySearchRepository companySearchRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private GroupRepository groupRepository;
	
	@Autowired
	private UserHelper userHelper;
	
	@Autowired
	private CompanyHelper companyHelper;
	
	@Autowired
	private AmazonS3Helper amazonS3Helper;
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Map<String, Object> findByKeyword(SearchRequest searchRequest) {
		LOGGER.info("Finding the all active companies");
		Map<String, Object> data = new HashMap<>();
		
		PageRequest pageRequest = createPageRequest(searchRequest);
		
    	SearchResult<Long> searchResult = companySearchRepository.findByKeywordContains(searchRequest, pageRequest);
    	long totalRecord = searchResult.getTotalHits();
    	Iterable<Company> companies = companyRepository.findAll(searchResult.getResults());
    	
		List<CompanyBean> companyBeans = new ArrayList<>();
		for (Company company : companies) {
			if (!company.isMocked()) {
				CompanyBean companyBean = mapper.map(company, CompanyBean.class);
				this.extractCommonProperties(company, companyBean);
				companyBeans.add(companyBean);
			}
		}
		
		data.put("recordsTotal", totalRecord);
		data.put("recordsFiltered", totalRecord);
		data.put("data", companyBeans);
		return data;
	}

	private void extractCommonProperties(Company company, CompanyBean companyBean) {
		Optional.ofNullable(company.getAddress())
			.ifPresent(address -> 
				companyBean.setAddress(
					mapper.map(address, AddressBean.class)));
		
		Optional.ofNullable(company.getDefaultImage())
			.ifPresent(defaultImage -> 
				companyBean.setDefaultImage(
					mapper.map(defaultImage, ImageBean.class)));
		
		Optional.ofNullable(company.getCreatedBy())
			.ifPresent(createdBy -> 
				companyBean.setCreatedBy(
					mapper.map(createdBy, UserBean.class)));
		
		Optional.ofNullable(company.getModifiedBy())
			.ifPresent(modifiedBy -> 
				companyBean.setModifiedBy(
					mapper.map(modifiedBy, UserBean.class)));
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void register(RegisterForm form) throws ServiceException {

		String domain = form.getEmailAddress().substring(form.getEmailAddress().indexOf("@") + 1);
		
		User companyAdmin = mapper.map(form, User.class);
		companyAdmin.setUserNo(UserNoGenerator.generateUserNo());
		companyAdmin.setUserType(UserType.COMPANY_ADMIN);
		companyAdmin.setPassword(PasswordManager.encryptPasswd(DEFAULT_PASSWD));
		companyAdmin.setDefaultLocale(PrakratiConstants.DEFAULT_LOCALE);
		companyAdmin.setMocked(Boolean.FALSE);
		companyAdmin.setBusinessCategory(BusinessCategory.getCategory(form.getBusinessCategory()));
        companyAdmin.setGroup(groupRepository.findGroupByName(COMPANY_ADMIN_GROUP));
        companyAdmin.setDefaultLanguage(languageRepository.findByCode(PrakratiConstants.DEFAULT_LANGUAGE));
		companyAdmin.setDefaultCountry(countryRepository.findByCode(PrakratiConstants.DEFAULT_COUNTRY));
		companyAdmin.setDefaultImage(imageRepository.findDefaultImageForCategory(ImageCategory.COMPANY_ADMIN));
		companyAdmin.setDateCreated(Date.from(Environment.clock().instant()));
        
		Company company = mapper.map(form, Company.class);
		company.setUniqueId(UUID.randomUUID().toString());
		company.setPhoneNo(PLUS + form.getPhoneNo());
		company.setMocked(Boolean.FALSE);
		company.setBusinessCategory(BusinessCategory.getCategory(form.getBusinessCategory()));
		company.setDomain(domain);
        company.setDefaultImage(imageRepository.findDefaultImageForCategory(ImageCategory.COMPANY));
        company.setCreatedBy(companyAdmin);
        company.setDateCreated(Date.from(Environment.clock().instant()));
        companyRepository.save(company);
        
        companyAdmin.setCompany(company);
        userRepository.save(companyAdmin);
        
        notificationHelper.sendCompanyRegistrationNotification(company);
        
        userHelper.publishIndexEvent(companyAdmin, EventType.INSERT);
        companyHelper.publishIndexEvent(company, EventType.INSERT);
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(CompanyForm form, MultipartFile companyLogo, MultipartFile adminImage, User createdBy) throws ServiceException {
		LOGGER.info("Create a new company account");
		Clock clock = Environment.clock();
		User companyAdmin = mapper.map(form, User.class);
		companyAdmin.setUserNo(UserNoGenerator.generateUserNo());
		companyAdmin.setUserType(UserType.COMPANY_ADMIN);
		companyAdmin.setPassword(PasswordManager.encryptPasswd(DEFAULT_PASSWD));
		companyAdmin.setPhoneNo(form.getMobileNo());
		companyAdmin.setMocked(Boolean.FALSE);
        companyAdmin.setBusinessCategory(BusinessCategory.getCategory(form.getBusinessCategory()));
        companyAdmin.setDefaultLocale(PrakratiConstants.DEFAULT_LOCALE);
		companyAdmin.setDefaultLanguage(languageRepository.findByCode(PrakratiConstants.DEFAULT_LANGUAGE));
		companyAdmin.setDefaultCountry(countryRepository.findByCode(PrakratiConstants.DEFAULT_COUNTRY));
		String domain = form.getEmail().substring(form.getEmail().indexOf("@") + 1);
        
        Group defaultGroup = groupRepository.findGroupByName(COMPANY_ADMIN_GROUP);
        companyAdmin.setGroup(defaultGroup);
        companyAdmin.setCreatedBy(createdBy);
        companyAdmin.setDateCreated(Date.from(clock.instant()));
        
        Company company = mapper.map(form, Company.class);
        company.setUniqueId(UUID.randomUUID().toString());
        company.setBusinessCategory(BusinessCategory.getCategory(form.getBusinessCategory()));
        company.setPhoneNo(PLUS + form.getPhoneNo());
        company.setMocked(Boolean.FALSE);
        company.setDomain(domain);
        company.setCreatedBy(createdBy);
        company.setDateCreated(Date.from(clock.instant()));
        
        companyAdmin.setCompany(company);
        userRepository.save(companyAdmin);
        companyRepository.save(company);
        
        handleImageInternal(companyLogo, company);
        handleImageInternal(adminImage, companyAdmin, ImageCategory.getCategory(companyAdmin.getUserType()));
        companyHelper.publishIndexEvent(company, EventType.INSERT);
        notificationHelper.sendCompanyRegistrationNotification(company);
	}

	private void handleImageInternal(MultipartFile companyLogo, Company company) {
		if (Predicates.notNull.test(companyLogo)) {
        	company.setDefaultImage(amazonS3Helper.handleImage(companyLogo, company));
        } else {
        	company.setDefaultImage(imageRepository.findDefaultImageForCategory(COMPANY));
        }
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public CompanyBean findDetails(String uniqueId, Map<String, Object> params) {
		LOGGER.info("Finding the company details");
		try {
			Company company = companyRepository.findByUniqueId(uniqueId);
			CompanyBean companyBean = mapper.map(company, CompanyBean.class);
			Optional.ofNullable(company.getDefaultBillingAddress())
				.ifPresent(defaultBillingAddress -> 
					companyBean.setDefaultBillingAddress(
						mapper.map(defaultBillingAddress, AddressEntityBean.class)));
			
			Optional.ofNullable(company.getDefaultDeliveryAddress())
				.ifPresent(defaultDeliveryAddress -> 
					companyBean.setDefaultDeliveryAddress(
						mapper.map(defaultDeliveryAddress, AddressEntityBean.class)));
			
			company.getExpertises()
				.forEach(expertise -> 
					companyBean.addExpertise(
						mapper.map(expertise, ExpertiseBean.class)));
			
			company.getSocialAccounts()
				.forEach(socialAccount -> 
					companyBean.addSocialAccount(
						mapper.map(socialAccount, SocialAccountBean.class)));
			
			userRepository.findByCompanyIdAndUserType(company.getUniqueId(), COMPANY_ADMIN)
				.forEach(admin -> 
					companyBean.addAdmin(
						mapper.map(admin, CompanyAdminBean.class)));
			
			userRepository.findByCompanyIdAndUserType(company.getUniqueId(), AGENT)
				.forEach(agent -> 
					companyBean.addAgent(
						mapper.map(agent, AgentBean.class)));
		
			this.extractCommonProperties(company, companyBean);
			return companyBean;
		} catch (DataAccessException e) {
			LOGGER.error("Got error while getting company detail", e);
			return null;
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(CompanyBean companyBean, User modifiedBy) throws ServiceException {
		LOGGER.info("Updating the company detail");
		Clock clock = Environment.clock();
		
		Company company = companyRepository.findByUniqueId(companyBean.getUniqueId());
		companyHelper.convertBeanToEntity(company, companyBean);
		company.setModifiedBy(modifiedBy);
		company.setDateModified(Date.from(clock.instant()));
		companyHelper.publishIndexEvent(company, EventType.UPDATE);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(String uniqueId) {
		LOGGER.info("Delete the company account " + uniqueId);
		Company company = companyRepository.findByUniqueId(uniqueId);
		company.setArchived(Boolean.TRUE);
		companyHelper.publishIndexEvent(company, EventType.DELETE);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public List<CompanyBean> findByMatchingKeywords(String keywords) {
		List<Company> matchingCompanies = companyRepository.findByMatchingName("%" + keywords + "%");
		List<CompanyBean> matchingCompanyBeans = new ArrayList<>();
		matchingCompanies.forEach(user -> matchingCompanyBeans.add(mapper.map(user, CompanyBean.class)));
		return matchingCompanyBeans;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public ImageBean changeCompanyLogo(ImageVO companyLogo, String uniqueId, User modifiedBy) {
		ImageBean defaultImageBean = null;
		Clock clock = Environment.clock();
		Company company = companyRepository.findByUniqueId(uniqueId);
		if (Predicates.notNull.test(companyLogo)) {
			company.setDefaultImage(amazonS3Helper.handleImage(companyLogo, company));
			company.setModifiedBy(modifiedBy);
			company.setDateModified(Date.from(clock.instant()));
			defaultImageBean = mapper.map(company.getDefaultImage(), ImageBean.class);
		}
		return defaultImageBean;
	}
}
