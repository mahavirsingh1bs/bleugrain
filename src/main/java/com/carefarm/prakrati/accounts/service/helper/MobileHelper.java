package com.carefarm.prakrati.accounts.service.helper;

import java.text.MessageFormat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.messaging.MobileMessanger;
import com.carefarm.prakrati.util.VerifCodeGenerator;

@Component
public class MobileHelper {
	private static final Logger LOGGER = LoggerFactory.getLogger(MobileHelper.class);
	
	@Value("${vocac.sms.register.mobile.verification}")
	protected String verificMessage;
	
	@Value("${verify.mobile.enabled}")
	protected boolean isVerifyMobileEnabled;
	
	@Autowired
	protected MobileMessanger mobileMessanger;
	
	public void sendMessageToVerifyMobileNo(User user) {
		if (isVerifyMobileEnabled && Predicates.notNull.test(user.getMobileNo())) {
			LOGGER.info("Sending verification message to Mobile No.: {}", user.getMobileNo());
			String verificCode = VerifCodeGenerator.generateVerifCode();
			String message = MessageFormat.format(verificMessage, verificCode);
        	mobileMessanger.sendMessage(user.getMobileNo(), message);
        	user.setMobileVerifyCode(verificCode);
        }
	}

}
