package com.carefarm.prakrati.accounts.service;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.web.bean.EmployeeBean;

public interface EmployeeService extends UserProfileService {
	
	EmployeeBean findDetails(String userNo, Object object);

	void update(EmployeeBean employeeBean, User modifiedBy) throws ServiceException;

}
