package com.carefarm.prakrati.accounts.service;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.web.bean.BrokerBean;

public interface BrokerService extends UserProfileService, RegisterableService {

	BrokerBean findDetails(String userNo, Object object);
	
	void update(BrokerBean brokerBean, User modifiedBy) throws ServiceException;
	
}
