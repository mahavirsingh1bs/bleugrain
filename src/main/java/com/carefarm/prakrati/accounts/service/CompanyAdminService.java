package com.carefarm.prakrati.accounts.service;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.web.bean.CompanyAdminBean;

public interface CompanyAdminService extends UserProfileService {
	
	CompanyAdminBean findDetail(String userNo);

	void update(CompanyAdminBean companyAdminBean, User currentUser) throws ServiceException;

}
