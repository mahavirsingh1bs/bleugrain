package com.carefarm.prakrati.accounts.service;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.web.bean.DriverBean;

public interface DriverService extends UserProfileService, RegisterableService {
	DriverBean findDetails(String userNo);
	void update(DriverBean driverBean, User modifiedBy) throws ServiceException;
}
