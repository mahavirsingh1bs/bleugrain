package com.carefarm.prakrati.accounts.service;

import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.util.UserType;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.UserForm;

public interface UserProfileService {
	Map<String, Object> findByKeyword(SearchRequest searchRequest, UserType userType);
	Map<String, Object> getProfileDetails(String uniqueId);
	void create(UserForm form, MultipartFile image, User currentUser) throws ServiceException;
	void delete(String userNo, User currentUser);
}
