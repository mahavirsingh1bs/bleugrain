package com.carefarm.prakrati.accounts.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.accounts.service.CustomerService;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.messaging.EmailHandler;
import com.carefarm.prakrati.payment.repository.CreditCardRepository;
import com.carefarm.prakrati.repository.DemandRepository;
import com.carefarm.prakrati.repository.OrderRepository;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.service.helper.AmazonS3Helper;
import com.carefarm.prakrati.service.helper.CustomerHelper;
import com.carefarm.prakrati.web.bean.CustomerBean;
import com.carefarm.prakrati.web.bean.UserBean;

@Service
public class CustomerServiceImpl extends UserProfileServiceImpl implements CustomerService {

	private static final Logger LOGGER = LoggerFactory.getLogger(CustomerServiceImpl.class);

	@Value("${application.context.path}")
	private String contextPath;
	
	@Value("${customer.image.default.url}")
	private String defaultCustomerImageUrl;

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private DemandRepository demandRepository;
	
	@Autowired
	private CreditCardRepository creditCardRepository;
	
	@Autowired
	private CustomerHelper customerHelper;

	@Autowired
	private EmailHandler messageHandler;

	@Autowired
	private AmazonS3Helper amazonS3Helper;
	
	@Override
	public List<UserBean> findByKeyword(Iterable<User> users) {
		List<UserBean> userBeans = new ArrayList<>();
		for (User user : users) {
			CustomerBean userBean = mapper.map(user, CustomerBean.class);
			customerHelper.extractBasicFields(user, userBean);
			userBeans.add(userBean);
		}
		return userBeans;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public CustomerBean findDetails(String userNo, Object object) {
		User customer = userRepository.findByUserNo(userNo);
		CustomerBean customerBean = mapper.map(customer, CustomerBean.class);
		customerHelper.extractBasicFields(customer, customerBean);
		return customerBean;
	}

	@Override
	public Map<String, Object> getProfileDetailsInternal(Map<String, Object> profileDetails, User user) {
		return profileDetails;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(CustomerBean customerBean, User currentUser) throws ServiceException {
		LOGGER.info("Updating customer details");
		User customer = userRepository.findByUserNo(customerBean.getUserNo());
		customerHelper.convertBeanToEntity(customer, customerBean);
		setModifiedAndPublishEvents(customer, currentUser);
	}

}
