package com.carefarm.prakrati.accounts.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.accounts.service.BrokerService;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.payment.repository.BankAccountRepository;
import com.carefarm.prakrati.repository.DemandRepository;
import com.carefarm.prakrati.repository.ProductRepository;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.service.helper.BrokerHelper;
import com.carefarm.prakrati.web.bean.BrokerBean;
import com.carefarm.prakrati.web.bean.UserBean;

@Service
public class BrokerServiceImpl extends UserProfileServiceImpl implements BrokerService {

	private static final Logger LOGGER = LoggerFactory.getLogger(BrokerServiceImpl.class);

	@Value("${broker.image.default.url}")
	private String defaultBrokerImageUrl;	

	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BankAccountRepository bankAccountRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private DemandRepository demandRepository;
	
	@Autowired
	private BrokerHelper brokerHelper;

	@Override
	public List<UserBean> findByKeyword(Iterable<User> users) {
		List<UserBean> userBeans = new ArrayList<>();
		for (User user : users) {
			BrokerBean userBean = mapper.map(user, BrokerBean.class);
			brokerHelper.extractBasicFields(user, userBean);
			userBeans.add(userBean);
		}
		return userBeans;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public BrokerBean findDetails(String userNo, Object object) {
		User broker = userRepository.findByUserNo(userNo);
		BrokerBean brokerBean = mapper.map(broker, BrokerBean.class);
		brokerHelper.extractBasicFields(broker, brokerBean);
		return brokerBean;
	}

	@Override
	public Map<String, Object> getProfileDetailsInternal(Map<String, Object> profileDetails, User user) {
		return profileDetails;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(BrokerBean brokerBean, User currentUser) throws ServiceException {
		LOGGER.info("Updating broker details");
		User broker = userRepository.findByUserNo(brokerBean.getUserNo());
		brokerHelper.convertBeanToEntity(broker, brokerBean);
		setModifiedAndPublishEvents(broker, currentUser);
	}

}
