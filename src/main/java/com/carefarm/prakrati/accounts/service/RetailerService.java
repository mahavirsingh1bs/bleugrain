package com.carefarm.prakrati.accounts.service;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.web.bean.RetailerBean;

public interface RetailerService extends UserProfileService, RegisterableService {

	RetailerBean findDetails(String userNo, Object object);

	void update(RetailerBean retailerBean, User modifiedBy) throws ServiceException;
	
}
