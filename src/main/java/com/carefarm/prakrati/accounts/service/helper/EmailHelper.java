package com.carefarm.prakrati.accounts.service.helper;

import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.messaging.AccountEmailHandler;

@Component
public class EmailHelper {
	private static final Logger LOGGER = LoggerFactory.getLogger(EmailHelper.class);

	@Value("${verify.email.enabled}")
	protected boolean isVerifyEmailEnabled;
	
	@Value("${application.context.path}")
	protected String contextPath;
	
	@Autowired
	protected AccountEmailHandler emailHandler;
	
	public void sendEmailToVerifyEmailAddress(User user) {
		if (isVerifyEmailEnabled && Predicates.notNull.test(user.getEmail())) {
			LOGGER.info("Sending verification email to Email Address.: {}", user.getEmail());
        	String emailVerifyCode = UUID.randomUUID().toString();
			String verifyEmailPath = contextPath + "#/VerifyEmailAddress?" + "verificationCode=" + emailVerifyCode;
			emailHandler.verifyEmailAddress(user.getEmail(), verifyEmailPath, user);
			user.setEmailVerifyCode(emailVerifyCode);
        }
	}
	
	public void sendEmailToVerifyEmailAddressAndResetPassword(User user) {
		if (isVerifyEmailEnabled && Predicates.notNull.test(user.getEmail())) {
        	String emailVerifyCode = UUID.randomUUID().toString();
			String verifyEmailPath = contextPath + "#/VerifyEmailAddress?" + "verificationCode=" + emailVerifyCode;
			
			String resetPasswdCode = UUID.randomUUID().toString();
			String resetPasswdPath = contextPath + "#/ResetPasswd?" + "resetCode=" + resetPasswdCode;
			
			emailHandler.accountCreatedEmail(user.getEmail(), verifyEmailPath, resetPasswdPath, user);
			user.setEmailVerifyCode(emailVerifyCode);
			user.setResetPasswdCode(resetPasswdCode);
        }
	}

}
