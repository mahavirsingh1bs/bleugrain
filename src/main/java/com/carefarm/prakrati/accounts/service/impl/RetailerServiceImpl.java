package com.carefarm.prakrati.accounts.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.accounts.service.RetailerService;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.payment.repository.BankAccountRepository;
import com.carefarm.prakrati.repository.DemandRepository;
import com.carefarm.prakrati.repository.ProductRepository;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.repository.results.CategoryWiseReviews;
import com.carefarm.prakrati.service.helper.RetailerHelper;
import com.carefarm.prakrati.web.bean.ExpertiseBean;
import com.carefarm.prakrati.web.bean.RetailerBean;
import com.carefarm.prakrati.web.bean.UserBean;

@Service
public class RetailerServiceImpl extends UserProfileServiceImpl implements RetailerService {
	private static final Logger LOGGER = LoggerFactory.getLogger(RetailerServiceImpl.class);
	
	@Value("${retailer.image.default.url}")
	private String defaultRetailerImageUrl;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private BankAccountRepository bankAccountRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private DemandRepository demandRepository;
	
	@Autowired
	private RetailerHelper retailerHelper;
		
	@Override
	public List<UserBean> findByKeyword(Iterable<User> users) {
		List<UserBean> userBeans = new ArrayList<>();
		for (User user : users) {
			RetailerBean userBean = mapper.map(user, RetailerBean.class);
			retailerHelper.extractBasicFields(user, userBean);
			userBeans.add(userBean);
		}
		return userBeans;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public RetailerBean findDetails(String userNo, Object object) {
		User retailer = userRepository.findByUserNo(userNo);
		RetailerBean retailerBean = mapper.map(retailer, RetailerBean.class);
		retailerHelper.extractBasicFields(retailer, retailerBean);
		return retailerBean;
	}
	
	@Override
	public Map<String, Object> getProfileDetailsInternal(Map<String, Object> profileDetails, User user) {
		List<ExpertiseBean> expertiseBeans = new ArrayList<>();
		user.getExpertises().forEach(expertise -> {
			ExpertiseBean expertiseBean = mapper.map(expertise, ExpertiseBean.class);
			expertiseBean.setRatingPercent(expertise.getRatingPercent().intValue());
			expertiseBeans.add(expertiseBean);
		});
		profileDetails.put("expertises", expertiseBeans);
		
		List<CategoryWiseReviews> categoryWiseReviews = productRepository.findCategoryWiseReviews();
		profileDetails.put("categoryWiseReviews", categoryWiseReviews);
		return profileDetails;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(RetailerBean retailerBean, User currentUser) throws ServiceException {
		LOGGER.info("Updating retailer details");
		User retailer = userRepository.findByUserNo(retailerBean.getUserNo());
		retailerHelper.convertBeanToEntity(retailer, retailerBean);
		setModifiedAndPublishEvents(retailer, currentUser);
	}

}
