package com.carefarm.prakrati.accounts.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.accounts.service.SuperAdminService;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.repository.GroupRepository;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.service.helper.SuperAdminHelper;
import com.carefarm.prakrati.web.bean.UserBean;

@Service
public class SuperAdminServiceImpl extends UserProfileServiceImpl implements SuperAdminService {

	private static final Logger LOGGER = LoggerFactory.getLogger(SuperAdminServiceImpl.class);
	
	@Value("${superAdmin.image.default.url}")
	private String defaultSuperAdminImageUrl;
	
	@Autowired
	private UserRepository userRepository;

	@Autowired
	private GroupRepository groupRepository;
	
	@Autowired
	private SuperAdminHelper superAdminHelper;
	
	@Override
	public List<UserBean> findByKeyword(Iterable<User> users) {
		List<UserBean> userBeans = new ArrayList<>();
		for (User user : users) {
			UserBean userBean = mapper.map(user, UserBean.class);
			superAdminHelper.extractBasicFields(user, userBean);
			userBeans.add(userBean);
		}
		return userBeans;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public UserBean findDetails(String userNo, Object object) {
		User superAdmin = userRepository.findByUserNo(userNo);
		UserBean superAdminBean = mapper.map(superAdmin, UserBean.class);
		superAdminHelper.extractBasicFields(superAdmin, superAdminBean);
		return superAdminBean;
	}

	@Override
	public Map<String, Object> getProfileDetailsInternal(Map<String, Object> profileDetails, User user) {
		return profileDetails;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(UserBean superAdminBean, User currentUser) throws ServiceException {
		LOGGER.info("Updating superAdmin details");
		User superAdmin = userRepository.findByUserNo(superAdminBean.getUserNo());
		superAdminHelper.convertBeanToEntity(superAdmin, superAdminBean);
		setModifiedAndPublishEvents(superAdmin, currentUser);
	}

}
