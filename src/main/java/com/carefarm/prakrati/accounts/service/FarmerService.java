package com.carefarm.prakrati.accounts.service;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.web.bean.FarmerBean;

public interface FarmerService extends UserProfileService, RegisterableService {
	
	FarmerBean findDetails(String userNo, Object object);

	void update(FarmerBean farmerBean, User modifiedBy) throws ServiceException;
	
}
