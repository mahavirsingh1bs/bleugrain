package com.carefarm.prakrati.accounts.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.carefarm.prakrati.accounts.service.EmployeeService;
import com.carefarm.prakrati.entity.Department;
import com.carefarm.prakrati.entity.Designation;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.repository.DepartmentRepository;
import com.carefarm.prakrati.repository.DesignationRepository;
import com.carefarm.prakrati.repository.DiscussionRepository;
import com.carefarm.prakrati.repository.GroupRepository;
import com.carefarm.prakrati.repository.NoteRepository;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.service.helper.EmployeeHelper;
import com.carefarm.prakrati.web.bean.DiscussionBean;
import com.carefarm.prakrati.web.bean.EmployeeBean;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.model.EmployeeForm;
import com.carefarm.prakrati.web.model.UserForm;

@Service
public class EmployeeServiceImpl extends UserProfileServiceImpl implements EmployeeService {

	private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeServiceImpl.class);
	
	private static final String SALES_DEPARTMENT = "Sales";
	
	private static final String CONTENT_DEPARTMENT = "Content";
	
	private static final String FILED_DEPARTMENT = "Field";
	
	private static final String EXECUTIVE = "Executive";
	
	private static final String CTMEMBER = "CTMember";
	
	private static final String STMEMBER = "STMember";
	
	@Value("${employee.image.default.url}")
	private String defaultEmployeeImageUrl;
	
	@Autowired
	private DepartmentRepository departmentRepository;
	
	@Autowired
	private DesignationRepository designationRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private DiscussionRepository discussionRepository;
	
	@Autowired
	private NoteRepository noteRepository;
	
	@Autowired
	private GroupRepository groupRepository;
	
	@Autowired
	private EmployeeHelper employeeHelper;
	
	@Override
	public List<UserBean> findByKeyword(Iterable<User> users) {
		List<UserBean> userBeans = new ArrayList<>();
		for (User user : users) {
			EmployeeBean userBean = mapper.map(user, EmployeeBean.class);
			employeeHelper.extractBasicFields(user, userBean);
			userBeans.add(userBean);
		}
		return userBeans;
	}
	
	@Override
	public void createInternal(UserForm form, MultipartFile image, User employee) {
		EmployeeForm employeeForm = (EmployeeForm )form;
		Department department = departmentRepository.findOne(employeeForm.getDepartmentId());
		Designation designation = designationRepository.findOne(employeeForm.getDesignationId());
		employee.setDepartment(department);
		employee.setDesignation(designation);
		
		handleGroupInternal(employee, department);
	}

	private void handleGroupInternal(User employee, Department department) {
		if (StringUtils.equals(department.getName(), FILED_DEPARTMENT)) {
			employee.setGroup(groupRepository.findGroupByName(EXECUTIVE));
		} else if (StringUtils.equals(department.getName(), CONTENT_DEPARTMENT)) {
			employee.setGroup(groupRepository.findGroupByName(CTMEMBER));
		} else if (StringUtils.equals(department.getName(), SALES_DEPARTMENT)) {
			employee.setGroup(groupRepository.findGroupByName(STMEMBER));
		}
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public EmployeeBean findDetails(String userNo, Object object) {
		User employee = userRepository.findByUserNo(userNo);
		EmployeeBean employeeBean = mapper.map(employee, EmployeeBean.class);
		employeeHelper.extractBasicFields(employee, employeeBean);
		return employeeBean;
	}

	@Override
	public Map<String, Object> getProfileDetailsInternal(Map<String, Object> profileDetails, User user) {
		List<DiscussionBean> discussions = new ArrayList<>();
		discussionRepository.findByUserNo(user.getUserNo())
			.forEach((discussion) -> {
				discussions.add(mapper.map(discussion, DiscussionBean.class));
			});
		profileDetails.put("discussions", discussions);
		return profileDetails;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void update(EmployeeBean employeeBean, User currentUser) throws ServiceException {
		LOGGER.info("Updating employee details");
		User employee = userRepository.findByUserNo(employeeBean.getUserNo());
		employeeHelper.convertBeanToEntity(employee, employeeBean);
		setModifiedAndPublishEvents(employee, currentUser);
	}

}
