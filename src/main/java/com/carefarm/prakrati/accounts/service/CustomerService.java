package com.carefarm.prakrati.accounts.service;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.web.bean.CustomerBean;

public interface CustomerService extends UserProfileService, RegisterableService {
	CustomerBean findDetails(String userNo, Object object);
	void update(CustomerBean customerBean, User modifiedBy) throws ServiceException;
}