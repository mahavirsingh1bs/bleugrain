package com.carefarm.prakrati.accounts.service.impl;

import static com.carefarm.prakrati.common.util.Constants.DEFAULT_COUNTRY;
import static com.carefarm.prakrati.common.util.Constants.DEFAULT_LANGUAGE;
import static com.carefarm.prakrati.common.util.Constants.PLUS;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.carefarm.prakrati.accounts.service.UserProfileService;
import com.carefarm.prakrati.accounts.service.helper.EmailHelper;
import com.carefarm.prakrati.accounts.service.helper.MobileHelper;
import com.carefarm.prakrati.common.util.Constants;
import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.entity.Address;
import com.carefarm.prakrati.entity.AddressEntity;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.generators.UserNoGenerator;
import com.carefarm.prakrati.repository.CompanyRepository;
import com.carefarm.prakrati.repository.NoteRepository;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.events.PrakratiEvent.EventType;
import com.carefarm.prakrati.search.repository.UserSearchRepository;
import com.carefarm.prakrati.security.PasswordManager;
import com.carefarm.prakrati.service.helper.UserHelper;
import com.carefarm.prakrati.service.impl.PrakratiServiceImpl;
import com.carefarm.prakrati.util.AddressCategory;
import com.carefarm.prakrati.util.ImageCategory;
import com.carefarm.prakrati.util.UserType;
import com.carefarm.prakrati.web.bean.AddressBean;
import com.carefarm.prakrati.web.bean.AddressEntityBean;
import com.carefarm.prakrati.web.bean.ExpertiseBean;
import com.carefarm.prakrati.web.bean.NoteBean;
import com.carefarm.prakrati.web.bean.NotificationBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.bean.SocialAccountBean;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.model.RegisterForm;
import com.carefarm.prakrati.web.model.UserForm;

public abstract class UserProfileServiceImpl extends PrakratiServiceImpl implements UserProfileService {
	
	@Autowired
	protected UserRepository userRepository;
	
	@Autowired
	protected UserSearchRepository userSearchRepository;
	
	@Autowired
	protected CompanyRepository companyRepository;
	
	@Autowired
	protected NoteRepository noteRepository;
	
	@Autowired
	private MobileHelper mobileHelper;
	
	@Autowired
	private EmailHelper emailHelper;
	
	@Autowired
	private UserHelper driverHelper;

	@Transactional(propagation = Propagation.REQUIRES_NEW, readOnly = true)
	public Map<String, Object> findByKeyword(SearchRequest searchRequest, UserType userType) {
		Map<String, Object> data = new HashMap<>();
		PageRequest pageRequest = createPageRequest(searchRequest);
		
		long totalRecords = 0;
		Iterable<User> users = null;
		if (Predicates.notNull.test(searchRequest.getCompanyId())) {
			SearchResult<Long> result = userSearchRepository.findByKeywordContains(searchRequest.getKeyword(), searchRequest.getCompanyId(), userType, pageRequest);
			totalRecords = result.getTotalHits();
			users = userRepository.findAll(result.getResults());
		} else {
			SearchResult<Long> result = userSearchRepository.findByKeywordContains(searchRequest.getKeyword(), userType, pageRequest);
			totalRecords = result.getTotalHits();
			users = userRepository.findAll(result.getResults());
		}
		
		List<UserBean> userBeans = findByKeyword(users);
		data.put("recordsTotal", totalRecords);
		data.put("recordsFiltered", totalRecords);
		data.put("data", userBeans);
		return data;
	}
	
	public abstract List<UserBean> findByKeyword(Iterable<User> users);
	
	public User register(RegisterForm form) throws ServiceException {
		User user = mapper.map(form, User.class);
		user.setAlternateUsername(PLUS + form.getMobileNo());
		user.setPassword(PasswordManager.encryptPasswd(form.getPassword()));
		user.setUserNo(UserNoGenerator.generateUserNo());
		user.setDefaultCountry(countryRepository.findByCode(Constants.DEFAULT_COUNTRY));
		user.setDefaultLanguage(languageRepository.findByCode(Constants.DEFAULT_LANGUAGE));
		user.setGroup(groupRepository.findGroupByName(form.getUserType().getType()));
        user.setEmailVerified(Boolean.FALSE);
		user.setMobileVerified(Boolean.FALSE);
		user.setMocked(Boolean.FALSE);
		user.setArchived(Boolean.FALSE);
		user.setUniqueCode(UUID.randomUUID().toString());
		user.setDefaultImage(imageRepository.findDefaultImageForCategory(ImageCategory.getCategory(form.getUserType())));

		userRepository.save(user);
		driverHelper.publishIndexEvent(user, EventType.INSERT);
		mobileHelper.sendMessageToVerifyMobileNo(user);
		emailHelper.sendEmailToVerifyEmailAddress(user);
		notificationHelper.sendUserRegistrationNotification(user, form.getUserType());
		return user;
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void create(UserForm form, MultipartFile image, User currentUser) {
		User user = mapper.map(form, User.class);
		user.setAlternateUsername(form.getMobileNo());
		user.setUserType(form.getUserType());
		user.setUserNo(UserNoGenerator.generateUserNo());
        user.setPassword(PasswordManager.encryptPasswd(DEFAULT_PASSWD));
        user.setDefaultCountry(countryRepository.findByCode(DEFAULT_COUNTRY));
        user.setDefaultLanguage(languageRepository.findByCode(DEFAULT_LANGUAGE));
        user.setGroup(groupRepository.findGroupByName(form.getUserType().getType()));
		user.setMocked(Boolean.FALSE);
        user.setCreatedBy(currentUser);
        user.setDateCreated(Date.from(Environment.clock().instant()));
        handleImageInternal(image, user, ImageCategory.getCategory(form.getUserType()));
        
        this.createInternal(form, image, user);
        userRepository.save(user);
        driverHelper.publishIndexEvent(user, EventType.INSERT);
        emailHelper.sendEmailToVerifyEmailAddressAndResetPassword(user);
        notificationHelper.sendUserCreationNotification(user, form.getUserType());
	}
	
	protected void createInternal(UserForm form, MultipartFile image, User user) { }

	protected void setModifiedAndPublishEvents(User user, User currentUser) {
		user.setModifiedBy(currentUser);
		user.setDateModified(Calendar.getInstance().getTime());
		driverHelper.publishIndexEvent(user, EventType.UPDATE);
	}

	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public Map<String, Object> getProfileDetails(String userNo) {
		Map<String, Object> profileDetails = new HashMap<>();
		User user = userRepository.findByUserNo(userNo);
		UserBean userBean = mapper.map(user, UserBean.class);
		userBean.setEmailVerified(user.isEmailVerified());
		userBean.setMobileVerified(user.isMobileVerified());
		
		Optional<Address> addressOpt = Optional.ofNullable(user.getAddress());
		addressOpt.ifPresent(address -> userBean.setAddress(mapper.map(address, AddressBean.class)));
		
		Optional<AddressEntity> defaultBillingAddressOpt = Optional.ofNullable(user.getDefaultBillingAddress());
		defaultBillingAddressOpt.ifPresent(defaultBillingAddress -> { 
				if (!defaultBillingAddress.isArchived()) { 
					userBean.setDefaultBillingAddress(mapper.map(defaultBillingAddress, AddressEntityBean.class));
				}
			});
		
		Optional<AddressEntity> defaultDeliveryAddressOpt = Optional.ofNullable(user.getDefaultDeliveryAddress());
		defaultDeliveryAddressOpt.ifPresent(defaultDeliveryAddress -> {
				if (!defaultDeliveryAddress.isArchived()) {
					userBean.setDefaultDeliveryAddress(mapper.map(defaultDeliveryAddress, AddressEntityBean.class));
				}
			});
		
		user.getBillingAddresses().stream()
			.filter(address -> !address.isArchived() && address.getAddressCategory().equals(AddressCategory.BILLING))
			.forEach(billingAddress -> userBean.addBillingAddress(mapper.map(billingAddress, AddressEntityBean.class)));
		
		user.getDeliveryAddresses().stream()
			.filter(address -> !address.isArchived() && address.getAddressCategory().equals(AddressCategory.DELIVERY))
			.forEach(deliveryAddress -> userBean.addDeliveryAddress(mapper.map(deliveryAddress, AddressEntityBean.class)));
		
		profileDetails.put("user", userBean);
		profileDetails.put("userType", user.getGroup().getName());
		
		List<NoteBean> notes = new ArrayList<>();
		noteRepository.findByUserNo(userNo)
			.forEach((note) -> {
				notes.add(mapper.map(note, NoteBean.class));
			});
		profileDetails.put("notes", notes);
		
		List<NotificationBean> notifications = new ArrayList<>();
		notificationRepository.findByUser(user)
			.forEach((notification) -> {
				notifications.add(mapper.map(notification, NotificationBean.class));
			});
		profileDetails.put("notifications", notifications);
		
		List<SocialAccountBean> socialAccountBeans = new ArrayList<>();
		user.getSocialAccounts().forEach(socialAccount -> 
			socialAccountBeans.add(mapper.map(socialAccount, SocialAccountBean.class)));
		profileDetails.put("socialAccounts", socialAccountBeans);
		
		List<ExpertiseBean> expertiseBeans = new ArrayList<>();
		user.getExpertises().forEach(expertise -> 
			expertiseBeans.add(mapper.map(expertise, ExpertiseBean.class)));
		profileDetails.put("expertises", expertiseBeans);
		
		return this.getProfileDetailsInternal(profileDetails, user);
	}
	
	public abstract Map<String, Object> getProfileDetailsInternal(Map<String, Object> profileDetails, User user);
	
	@Override
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void delete(String userNo, User currentUser) {
		User user = userRepository.findByUserNo(userNo);
		user.setArchived(Boolean.TRUE);
		setModifiedAndPublishEvents(user, currentUser);
	}

}
