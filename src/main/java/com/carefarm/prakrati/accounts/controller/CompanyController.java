package com.carefarm.prakrati.accounts.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.carefarm.prakrati.accounts.service.CompanyService;
import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.web.bean.CompanyBean;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.CompanyForm;

@Controller
@RequestMapping("/companies")
public class CompanyController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CompanyController.class);
		
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private CompanyService companyService;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> getAllCompanies(SearchRequest searchRequest) {
		return companyService.findByKeyword(searchRequest);
	}
	
	@RequestMapping(value="/data/{uniqueId}", method=RequestMethod.GET)
	public @ResponseBody CompanyBean getData(@PathVariable("uniqueId") String uniqueId, ModelMap modelMap){
		return companyService.findDetails(uniqueId, null);
	}
	
	@RequestMapping(value = "/delete/{uniqueId}", method = RequestMethod.DELETE)
	public @ResponseBody MessageBean deleteCompany(@PathVariable("uniqueId") String uniqueId) {
		companyService.delete(uniqueId);
		return new MessageBean("success", "Company has been deleted successfully");
	}
	
	@RequestMapping(value="/new", method=RequestMethod.POST)
	public ResponseEntity<?> newCompany(@Valid @RequestPart("company") CompanyForm form, BindingResult result, 
			@RequestPart(value = "companyLogo", required = false) MultipartFile companyLogo, 
			@RequestPart(value = "adminImage", required = false) MultipartFile adminImage, 
			Authentication authentication) {
		if(result.hasErrors()) {
            MessageBean messageBean = new MessageBean("error", "form has validation errors", result.getFieldErrors(), result.getGlobalErrors());
            return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
        }
		
		// ImageVO companyLogo = ImageUtils.getImageVO(compLogo);
		// ImageVO adminImage = ImageUtils.getImageVO(adminImg);
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
        
    	String email = form.getEmail();
        if(userRepository.findByEmailAddress(email) != null) {
        	LOGGER.error("A user already exists with the same email address");
            MessageBean messageBean = new MessageBean("error", "Email address is already in use.");
            return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
        }

        try {
			companyService.create(form, companyLogo, adminImage, user);
			MessageBean messageBean = new MessageBean("success", "Company has been added successfully.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
		} catch (ServiceException e) {
			LOGGER.error("Got error while creating company", e);
			MessageBean messageBean = new MessageBean("error", "Got error while creating company");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(value="/update", method=RequestMethod.POST)
	public @ResponseBody MessageBean newFarmer(@Valid @RequestBody CompanyBean companyBean, BindingResult result, 
			Authentication authentication, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
			companyService.update(companyBean, user);
			redirectAttributes.addFlashAttribute("message", "You have successfully signed up and logged in.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while creating farmer", e);
			redirectAttributes.addFlashAttribute("message", "Got error while updating company");
			return new MessageBean("error", "Got error while updating company");
		}
        
        return new MessageBean("success", "Company has been updated successfully");
	}
	
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public @ResponseBody List<CompanyBean> findCompanies(@RequestParam("keywords") String keywords) {
		return companyService.findByMatchingKeywords(keywords);
	}
}
