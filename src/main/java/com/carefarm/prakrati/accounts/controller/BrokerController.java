package com.carefarm.prakrati.accounts.controller;

import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.carefarm.prakrati.accounts.service.BrokerService;
import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.util.UserType;
import com.carefarm.prakrati.validators.BrokerValidator;
import com.carefarm.prakrati.web.bean.BrokerBean;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.BrokerForm;

@Controller
@RequestMapping("/brokers")
public class BrokerController {
	private static final Logger LOGGER = LoggerFactory.getLogger(BrokerController.class);
		
	@Autowired
	private BrokerService brokerService;
	
	@Autowired
	private BrokerValidator brokerValidator;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> getAllBrokers(SearchRequest searchRequest) {
		return brokerService.findByKeyword(searchRequest, UserType.BROKER);
	}
	
	@RequestMapping(value="/data/{userNo}", method=RequestMethod.GET)
	public @ResponseBody BrokerBean getData(@PathVariable("userNo") String userNo, ModelMap modelMap) {
		return brokerService.findDetails(userNo, null);
	}
	
	@RequestMapping(value="/new", method=RequestMethod.POST)
	public ResponseEntity<?> newBroker(@Valid @RequestPart("broker") BrokerForm form, BindingResult result, 
			@RequestPart(value = "brokerImage", required = false) MultipartFile brokerImage, Authentication authentication, RedirectAttributes redirectAttributes) {
		brokerValidator.validate(form, result);
		if(result.hasErrors()) {
            MessageBean messageBean = new MessageBean("error", "Form has validation errors", result.getFieldErrors(), result.getGlobalErrors());
            return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
        }

   		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
    	
        try {
        	form.setUserType(UserType.BROKER);
			brokerService.create(form, brokerImage, user);
			MessageBean messageBean = new MessageBean("success", "Broker has been added successfully");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
		} catch (ServiceException e) {
			LOGGER.error("An error occurred while trying to create broker: " + e.getMessage(), e.getCause());
			MessageBean messageBean = new MessageBean("error", "An server occurred when adding broker");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public @ResponseBody MessageBean update(@Valid @RequestBody BrokerBean brokerBean, BindingResult result, 
			Authentication authentication, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
			brokerService.update(brokerBean, user);
			redirectAttributes.addFlashAttribute("message", "Broker has been updated successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while creating broker", e);
			redirectAttributes.addFlashAttribute("message", "Got error while updating broker");
			return new MessageBean("error", "Got error while updating broker");
		}
        
        return new MessageBean("success", "Broker has been updated successfully");
	}

	@RequestMapping(value = "/delete/{userNo}", method = RequestMethod.DELETE)
	public @ResponseBody MessageBean delete(@PathVariable("userNo") String uniqueId, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();

		brokerService.delete(uniqueId, currentUser);
		return new MessageBean("success", "Broker has been deleted successfully");
	}

}
