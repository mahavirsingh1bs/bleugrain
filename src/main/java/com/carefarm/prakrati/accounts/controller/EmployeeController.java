package com.carefarm.prakrati.accounts.controller;

import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.carefarm.prakrati.accounts.service.EmployeeService;
import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.util.UserType;
import com.carefarm.prakrati.web.bean.EmployeeBean;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.EmployeeForm;

@Controller
@RequestMapping("/employees")
public class EmployeeController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeController.class);
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private UserRepository userRepository;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> getAllEmployees(SearchRequest searchRequest) {
		return employeeService.findByKeyword(searchRequest, UserType.EMPLOYEE);
	}
	
	@RequestMapping(value="/data/{userNo}", method=RequestMethod.GET)
	public @ResponseBody EmployeeBean getData(@PathVariable("userNo") String userNo, ModelMap modelMap) {
		return employeeService.findDetails(userNo, null);
	}
	
	@RequestMapping(value="/new", method=RequestMethod.POST)
	public @ResponseBody MessageBean newEmployee(@Valid @RequestPart("employee") EmployeeForm form, BindingResult result, 
			@RequestPart(value = "employeeImage", required = false) MultipartFile employeeImage, Authentication authentication, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
            return new MessageBean("error", "Form has validation errors", result.getFieldErrors(), result.getGlobalErrors());
        }

        String email = form.getEmail();
        if(userRepository.findByEmailAddress(email) != null) {
            result.rejectValue("email", "errors.signup.email", "Email address is already in use.");
            return new MessageBean("error", "Email address is already in use.");
        }


		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
    	
        try {
        	form.setUserType(UserType.EMPLOYEE);
			employeeService.create(form, employeeImage, user);
			redirectAttributes.addFlashAttribute("message", "Employee has been added successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while creating employee", e);
			redirectAttributes.addFlashAttribute("message", "Got error while creating employee");
			return new MessageBean("error", "Got error while creating employee");
		}
        
        return new MessageBean("success", "Employee has created successfully");
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public @ResponseBody MessageBean update(@Valid @RequestBody EmployeeBean employeeBean, BindingResult result, 
			Authentication authentication, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
			employeeService.update(employeeBean, user);
			redirectAttributes.addFlashAttribute("message", "You have successfully signed up and logged in.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while creating employee", e);
			redirectAttributes.addFlashAttribute("message", "Got error while creating employee");
			return new MessageBean("error", "Got error while creating employee");
		}
        
        return new MessageBean("success", "Employee has been updated successfully");
	}

	@RequestMapping(value = "/delete/{userNo}", method = RequestMethod.DELETE)
	public @ResponseBody MessageBean delete(@PathVariable("userNo") String userNo, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();

		employeeService.delete(userNo, currentUser);
		return new MessageBean("success", "Employee has been deleted successfully");
	}
	
}
