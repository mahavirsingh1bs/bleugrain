package com.carefarm.prakrati.accounts.controller;

import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.carefarm.prakrati.accounts.service.SuperAdminService;
import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.util.UserType;
import com.carefarm.prakrati.validators.SuperAdminValidator;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.model.SuperAdminForm;

@Controller
@RequestMapping("/superAdmins")
public class SuperAdminController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SuperAdminController.class);
	
	@Autowired
	private SuperAdminService superAdminService;

	@Autowired
	private SuperAdminValidator superAdminValidator;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> getAllSuperAdmins(SearchRequest searchRequest) {
		return superAdminService.findByKeyword(searchRequest, UserType.SUPER_ADMIN);
	}
	
	@RequestMapping(value="/data/{userNo}", method=RequestMethod.GET)
	public @ResponseBody UserBean getData(@PathVariable("userNo") String userNo, ModelMap modelMap) {
		return superAdminService.findDetails(userNo, null);
	}
	
	@RequestMapping(value="/new", method=RequestMethod.POST)
	public ResponseEntity<?> newSuperAdmin(@Valid @RequestPart("superAdmin") SuperAdminForm form, BindingResult result, 
			@RequestPart(value = "adminImage", required = false) MultipartFile adminImage, Authentication authentication, RedirectAttributes redirectAttributes) {
		superAdminValidator.validate(form, result);
		if(result.hasErrors()) {
            MessageBean messageBean = new MessageBean("error", "Form has validation errors", result.getFieldErrors(), result.getGlobalErrors());
            return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
    	
        try {
        	form.setUserType(UserType.SUPER_ADMIN);
			superAdminService.create(form, adminImage, user);
			MessageBean messageBean = new MessageBean("success", "SuperAdmin has been added successfully.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
		} catch (ServiceException e) {
			LOGGER.error("Got error while creating superAdmin", e);
			MessageBean messageBean = new MessageBean("error", "Got error while creating superAdmin");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public @ResponseBody MessageBean update(@Valid @RequestBody UserBean superAdminBean, BindingResult result, 
			Authentication authentication, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
			superAdminService.update(superAdminBean, user);
			redirectAttributes.addFlashAttribute("message", "You have successfully signed up and logged in.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while creating superAdmin", e);
			redirectAttributes.addFlashAttribute("message", "Got error while creating superAdmin");
			return new MessageBean("error", "Got error while creating superAdmin");
		}
        
        return new MessageBean("success", "SuperAdmin has been updated successfully");
	}

	@RequestMapping(value = "/delete/{userNo}", method = RequestMethod.DELETE)
	public @ResponseBody MessageBean delete(@PathVariable("userNo") String userNo, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();

		superAdminService.delete(userNo, currentUser);
		return new MessageBean("success", "SuperAdmin has been deleted successfully");
	}
	
}
