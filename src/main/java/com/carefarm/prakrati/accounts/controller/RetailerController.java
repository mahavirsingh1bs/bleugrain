package com.carefarm.prakrati.accounts.controller;

import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.carefarm.prakrati.accounts.service.RetailerService;
import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.util.UserType;
import com.carefarm.prakrati.validators.RetailerValidator;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.bean.RetailerBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.UserForm;

@Controller
@RequestMapping("/retailers")
public class RetailerController {
	private static final Logger LOGGER = LoggerFactory.getLogger(CompanyController.class);
	
	@Autowired
	private RetailerService retailerService;
	
	@Autowired
	private RetailerValidator retailerValidator;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> getAllRetailers(SearchRequest searchRequest) {
		return retailerService.findByKeyword(searchRequest, UserType.RETAILER);
	}
	
	@RequestMapping(value="/data/{userNo}", method=RequestMethod.GET)
	public @ResponseBody RetailerBean getData(@PathVariable("userNo") String userNo, ModelMap modelMap) {
		return retailerService.findDetails(userNo, null);
	}
	
	@RequestMapping(value="/new", method=RequestMethod.POST)
	public ResponseEntity<?> newRetailer(@Valid @RequestPart("retailer") UserForm form, BindingResult result, 
			@RequestPart(value = "retailerImage", required = false) MultipartFile retailerImage, Authentication authentication) {
		retailerValidator.validate(form, result);
		if(result.hasErrors()) {
            MessageBean messageBean = new MessageBean("error", "Form has validation errors", result.getFieldErrors(), result.getGlobalErrors());
            return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
    	
        try {
        	form.setUserType(UserType.RETAILER);
			retailerService.create(form, retailerImage, user);
			MessageBean messageBean = new MessageBean("success", "Retailer has been added successfully.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
		} catch (ServiceException e) {
			LOGGER.error("Got error while creating retailer", e);
			MessageBean messageBean = new MessageBean("error", "Got error while creating retailer");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public @ResponseBody MessageBean update(@Valid @RequestBody RetailerBean retailerBean, BindingResult result, 
			Authentication authentication, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
			retailerService.update(retailerBean, user);
			redirectAttributes.addFlashAttribute("message", "Retailer has been updated successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while updating retailer", e);
			redirectAttributes.addFlashAttribute("message", "Got error while updating retailer");
			return new MessageBean("error", "Got error while updating retailer");
		}
        
        return new MessageBean("success", "Retailer has been updated successfully");
	}

	@RequestMapping(value = "/delete/{userNo}", method = RequestMethod.DELETE)
	public @ResponseBody MessageBean delete(@PathVariable("userNo") String userNo, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();

		retailerService.delete(userNo, currentUser);
		return new MessageBean("success", "Retailer has been deleted successfully");
	}

}
