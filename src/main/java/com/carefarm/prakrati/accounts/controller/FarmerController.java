package com.carefarm.prakrati.accounts.controller;

import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.carefarm.prakrati.accounts.service.FarmerService;
import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.util.UserType;
import com.carefarm.prakrati.validators.FarmerValidator;
import com.carefarm.prakrati.web.bean.FarmerBean;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.FarmerForm;

@Controller
@RequestMapping("/farmers")
public class FarmerController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CompanyController.class);

	@Autowired
	private FarmerValidator farmerValidator;
	
	@Autowired
	private FarmerService farmerService;

	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> getAllFarmers(SearchRequest searchRequest) {
		return farmerService.findByKeyword(searchRequest, UserType.FARMER);
	}
	
	@RequestMapping(value="/data/{userNo}", method=RequestMethod.GET)
	public @ResponseBody FarmerBean getData(@PathVariable("userNo") String userNo, ModelMap modelMap) {
		return farmerService.findDetails(userNo, null);
	}
	
	@RequestMapping(value="/new", method=RequestMethod.POST)
	public ResponseEntity<?> newFarmer(@Valid @RequestPart("farmer") FarmerForm form, BindingResult result, 
			@RequestPart(value = "farmerImage", required = false) MultipartFile farmerImage, Authentication authentication, RedirectAttributes redirectAttributes) {
		farmerValidator.validate(form, result);
		if(result.hasErrors()) {
            MessageBean messageBean = new MessageBean("error", "Form has validation errors", result.getFieldErrors(), result.getGlobalErrors());
            return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
        try {
			farmerService.create(form, farmerImage, currentUser);
			MessageBean messageBean = new MessageBean("success", "Farmer has created successfully");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
		} catch (ServiceException e) {
			LOGGER.error("Got error while creating farmer", e);
			result.reject("form.farmer.invalid", "Got error while trying to add farmer details");
			MessageBean messageBean = new MessageBean("error", "Got error while creating farmer", result.getFieldErrors(), result.getGlobalErrors());
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
        
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public @ResponseBody MessageBean update(@Valid @RequestBody FarmerBean farmerBean, BindingResult result, 
			Authentication authentication, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
			farmerService.update(farmerBean, user);
			redirectAttributes.addFlashAttribute("message", "You have successfully signed up and logged in.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while creating farmer", e);
			redirectAttributes.addFlashAttribute("message", "Got error while creating farmer");
			return new MessageBean("error", "Got error while creating farmer");
		}
        
        return new MessageBean("success", "Farmer has been updated successfully");
	}

	@RequestMapping(value = "/delete/{userNo}", method = RequestMethod.DELETE)
	public @ResponseBody MessageBean delete(@PathVariable("userNo") String userNo, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();

		farmerService.delete(userNo, currentUser);
		return new MessageBean("success", "Farmer has been deleted successfully");
	}

}
