package com.carefarm.prakrati.delivery.repository;

import com.carefarm.prakrati.delivery.entity.Delivery;
import com.carefarm.prakrati.repository.PrakratiRepository;

public interface DeliveryRepository extends PrakratiRepository<Delivery, Long> {

}
