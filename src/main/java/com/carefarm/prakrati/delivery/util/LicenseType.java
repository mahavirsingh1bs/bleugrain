package com.carefarm.prakrati.delivery.util;

public enum LicenseType {
	TWO_WHEELER, FOUR_WHEELER, COMMERCIAL;
}
