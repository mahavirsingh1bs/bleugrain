package com.carefarm.prakrati.delivery.util;

public enum TransportType {
	TRACTOR, TRUCK, MINI_TRUCK;
}