package com.carefarm.prakrati.delivery.util;

public enum OrderStatus {
	PLACED("PLACED", "Placed"), SHIPPED("SHIPPED", "Shipped"), ASSIGNED("ASSIGNED", "Assigned"), DISPATCHED("DISPATCHED", "Dispatched"), DELIVERED("DELIVERED", "Delivered");
	
	private String code;
	private String value;
	
	private OrderStatus(String code, String value) {
		this.code = code;
		this.value = value;
	}
	
	public String getCode() {
		return code;
	}

	public String getValue() {
		return value;
	}

	public static OrderStatus getStatus(String status) {
		for (OrderStatus orderStatus : OrderStatus.values()) {
			if (orderStatus.value.equals(status)) {
				return orderStatus;
			}
		}
		return null;
	}
}
