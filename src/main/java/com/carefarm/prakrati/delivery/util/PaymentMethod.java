package com.carefarm.prakrati.delivery.util;

public enum PaymentMethod {
	CREDIT_CARD, DEBIT_CARD, CHEQUE, CASH;
}
