package com.carefarm.prakrati.delivery.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

import com.carefarm.prakrati.delivery.util.LicenseType;

@Entity
@Table(name = "CF_LICENSE")
@Getter
@Setter
public class License {
	@Id
	@GenericGenerator(name = "license_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_LICENSE"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "license_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;

	@Version
    @Column(name = "OBJ_VERSION")
    private int version = 0;
	
	@Column(name = "LICENSE_NO")
	private String licenseNo;
	
	@Column(name = "LICENSE_TYPE")
	private LicenseType type;
	
	@Column(name = "VALID_FOR")
	private String validFor;
	
}
