package com.carefarm.prakrati.delivery.entity;

import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import org.hibernate.annotations.GenericGenerator;

import com.carefarm.prakrati.delivery.util.PaymentMethod;

@Entity
@Table(name = "CF_PAYMENT")
public class Payment {
	@Id
	@GenericGenerator(name = "payment_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_PAYMENT"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "payment_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;

	@Version
    @Column(name = "OBJ_VERSION")
    private int version = 0;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "PAYMENT_METHOD")
	private PaymentMethod method;
	
	@Column(name = "AMOUNT")
	private BigDecimal amount;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "PAYMENT_DATE")
	private Date paymentDate;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public PaymentMethod getMethod() {
		return method;
	}
	public void setMethod(PaymentMethod method) {
		this.method = method;
	}
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public Date getPaymentDate() {
		return paymentDate;
	}
	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}
	
}
