package com.carefarm.prakrati.delivery.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

import com.carefarm.prakrati.common.entity.PricePerUnit;
import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.entity.AbstractEntity;
import com.carefarm.prakrati.entity.Capacity;
import com.carefarm.prakrati.entity.Company;
import com.carefarm.prakrati.entity.User;

@Entity
@Table(name = "CF_TRANSPORT")
@Getter
@Setter
public class Transport extends AbstractEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "transport_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_TRANSPORT"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "transport_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;

	@Column(name = "UNIQUE_ID", unique = true, nullable = false)
	private String uniqueId;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "VEHICLE_NO")
	private String vehicleNo;
	
	@Embedded
	private Capacity capacity;

	@Embedded
	private PricePerUnit pricePerUnit;
	
	@Column(name = "IS_ARCHIVED")
	private boolean archived;
	
	@ManyToOne
	@JoinColumn(name = "DRIVER_ID")
	private User driver;
	
	// Driver assignment properties
	@ManyToOne
	@JoinColumn(name = "ASSIGNED_BY")
	private User assignedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ASSIGNED_ON")
	private Date dateAssigned;
	
	@Column(name = "COMMENTS")
	private String comments;
	
	@ManyToOne
	@JoinColumn(name = "COMPANY_ID")
	private Company company;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(
			name = "CF_TRANSPORT_PERMIT", 
			joinColumns = { @JoinColumn(name = "TRANSPORT_ID")},
			inverseJoinColumns = @JoinColumn(name = "PERMIT_ID"))
	private List<Permit> permits;

	@OneToMany
	@JoinColumn(name = "TRANSPORT_ID")
	private List<Delivery> deliveries;
	
	public void addPermit(Permit permit) {
		if (Predicates.isNull.test(this.permits)) {
			this.permits = new ArrayList<>();
		}
		this.permits.add(permit);
	}
	
	public void addDelivery(Delivery delivery) {
		if (Predicates.isNull.test(this.deliveries)) {
			this.deliveries = new ArrayList<>();
		}
		this.deliveries.add(delivery);
	}
}
