package com.carefarm.prakrati.delivery.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

import com.carefarm.prakrati.entity.AbstractEntity;
import com.carefarm.prakrati.entity.AddressEntity;
import com.carefarm.prakrati.entity.User;

@Entity
@Table(name = "CF_DELIVERY")
@Getter
@Setter
public class Delivery extends AbstractEntity {
	@Id
	@GenericGenerator(name = "delivery_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_DELIVERY"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "delivery_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@Column(name = "PICKUP_DATE")
	private Date pickupDate;
	
	@Column(name = "DELIVERY_DATE")
	private Date deliveryDate;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "PICKUP_ADDRESS_ID")
	private AddressEntity pickupAddress;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "DELIVERY_ADDRESS_ID")
	private AddressEntity deliveryAddress;
	
	@ManyToOne
	@JoinColumn(name = "DRIVER_ID")
	private User driver;
	
	@ManyToOne
	@JoinColumn(name = "TRANSPORT_ID")
	private Transport transport;
	
}
