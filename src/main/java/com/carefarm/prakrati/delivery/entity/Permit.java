package com.carefarm.prakrati.delivery.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

import com.carefarm.prakrati.entity.AbstractEntity;

@Entity
@Table(name = "CF_PERMIT")
@Getter
@Setter
public class Permit extends AbstractEntity {
	@Id
	@GenericGenerator(name = "permit_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_PERMIT"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "permit_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "IS_ARCHIVED")
	private boolean archived;
	
}
