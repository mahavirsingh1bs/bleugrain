package com.carefarm.prakrati.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

import com.carefarm.prakrati.util.ImageCategory;


@Entity
@Table(name = "CF_IMAGE")
@Getter
@Setter
public class Image implements Serializable {

	private static final long serialVersionUID = 2379853838375073188L;

	@Id
	@GenericGenerator(name = "image_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_IMAGE"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "image_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;

	@Version
    @Column(name = "OBJ_VERSION")
	@Setter(AccessLevel.NONE)
    private int version = 0;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "FILENAME")
    private String filename;
	
	@Column(name = "FILEPATH")
    private String filepath;
	
	@Column(name = "SMALL_FILEPATH")
	private String smallFilepath;
	
	@Column(name = "MEDIUM_FILEPATH")
	private String mediumFilepath;
	
	@Column(name = "LARGE_FILEPATH")
	private String largeFilepath;

	@Enumerated(EnumType.STRING)
	@Column(name = "IMAGE_CATEGORY")
	private ImageCategory category;
	
	@Column(name = "IS_DEFAULT")
	private boolean isDefault;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY_ID")
	private Company company;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRODUCT_ID")
	private Product product;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID")
	private User user;

	public Image() { }
	
	public Image(String filename, String filepath) {
		this.filename = filename;
		this.filepath = filepath;
	}
		
}
