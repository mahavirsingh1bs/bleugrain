package com.carefarm.prakrati.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

import com.carefarm.prakrati.constants.Predicates;

@Entity
@Table(name = "CF_UPCOMING_EVENT")
@Getter
@Setter
public class UpcomingEvent extends AbstractEntity {
	
	@Id
	@GenericGenerator(name = "upcoming_event_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_UPCOMING_EVENT"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "upcoming_event_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@Column(name = "UNIQUE_ID", unique = true, nullable = false)
	private String uniqueId;
	
	@Column(name = "EVENT")
	private String event;
	
	@Column(name = "EVENT_DETAIL", length = 1000)
	private String eventDetail;

	@Column(name = "IS_ARCHIVED")
	private boolean archived;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "EVENT_DATE")
	private Date eventDate;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
			name = "CF_UPCOMING_EVENT_GROUP", 
			joinColumns = { @JoinColumn(name = "UPCOMING_EVENT_ID")},
			inverseJoinColumns = @JoinColumn(name = "GROUP_ID"))
	private Set<Group> groups;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
			name = "CF_UPCOMING_EVENT_USER", 
			joinColumns = { @JoinColumn(name = "UPCOMING_EVENT_ID")},
			inverseJoinColumns = @JoinColumn(name = "USER_ID"))
	private Set<User> users;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
			name = "CF_UPCOMING_EVENT_COMPANY", 
			joinColumns = { @JoinColumn(name = "UPCOMING_EVENT_ID")},
			inverseJoinColumns = @JoinColumn(name = "COMPANY_ID"))
	private Set<Company> companies;
	
	@ManyToOne
	@JoinColumn(name = "USER_ID")
	private User user;

	@ManyToOne
	@JoinColumn(name = "COMPANY_ID")
	private Company company;
	
	public void addGroup(Group group) {
		if (Predicates.isNull.test(this.groups)) {
			this.groups = new HashSet<>();
		}
		this.groups.add(group);
	}
	
	public void addUser(User user) {
		if (Predicates.isNull.test(this.users)) {
			this.users = new HashSet<>();
		}
		this.users.add(user);
	}
	
	public void addCompany(Company company) {
		if (Predicates.isNull.test(this.companies)) {
			this.companies = new HashSet<>();
		}
		this.companies.add(company);
	}
		
}
