package com.carefarm.prakrati.entity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

import com.carefarm.prakrati.common.entity.OrderStatus;
import com.carefarm.prakrati.constants.Predicates;

@Entity
@Table(name = "CF_ORDER")
@Getter
@Setter
public class Order extends AbstractEntity {
	@Id
	@GenericGenerator(name = "order_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_ORDER"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "order_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@Column(name = "ORDER_NO", unique = true, nullable = false)
	private String orderNo;

	@OneToMany
	@JoinTable(
			name = "CF_ORDER_PRODUCTS",
			joinColumns = @JoinColumn(name = "ORDER_ID"),
			inverseJoinColumns = @JoinColumn(name = "PRODUCT_ID")
	)
	private Set<Product> products;
	
	@OneToMany(cascade = { CascadeType.MERGE, CascadeType.PERSIST })
	@JoinColumn(name = "ORDER_ID")
	private Set<Item> items;
	
	@Column(name = "SUBTOTAL")
	private BigDecimal subtotal;
	
	@Column(name = "SHIPPING_COST")
	private BigDecimal shippingCost;
	
	@Column(name = "TOTAL_PRICE")
	private BigDecimal totalPrice;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ORDER_DATE")
	private Date orderDate;
	
	@Column(name = "COMMENTS")
	private String comments;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "DELIVER_BY")
	private User deliverBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DELIVERY_DATE")
	private Date deliveryDate;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "BILLING_ADDRESS_ID")
	private AddressEntity billingAddress;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "SHIPPING_ADDRESS_ID")
	private AddressEntity shippingAddress;
	
	@Column(name = "AMOUNT_PAID")
	private BigDecimal amountPaid;
	
	@ManyToOne
	@JoinColumn(name = "STATUS_ID")
	private OrderStatus status;

	@Column(name = "IS_ARCHIVED")
	private boolean archived;
	
	@Column(name = "IS_MOCKED")
	private boolean mocked;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID")
	private User user;
	
	/**
	 * If this is a company order than this field got assigned company
	 */
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY_ID")
	private Company company;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FULFILLED_BY")
	private Company fullfilledBy;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ASSIGNED_BY")
	private User assignedBy;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ASSIGNED_TO")
	private User assignedTo;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ASSIGNED_ON")
	private Date dateAssigned;

	public void addProduct(Product product) {
		if (this.products == null) {
			this.products = new HashSet<>();
		}
		this.products.add(product);
	}

	public void addItem(Item item) {
		if (Predicates.isNull.test(this.items)) {
			this.items = new HashSet<>();
		}
		this.items.add(item);
	}
	
	public Integer getTotalItems() {
		return this.products.size();
	}

}
