package com.carefarm.prakrati.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;

import com.carefarm.prakrati.common.entity.Currency;
import com.carefarm.prakrati.common.entity.ProductStatus;
import com.carefarm.prakrati.common.entity.Unit;
import com.carefarm.prakrati.constants.Predicates;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "CF_PRODUCT")
@Getter
@Setter
public class Product extends AbstractEntity {
	@Id
	@GenericGenerator(name = "product_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_PRODUCT"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "product_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@Column(name = "NAME")
	private String name;

	@Column(name = "SHORT_DESC", length = 600)
	private String shortDesc;
	
	@Lob
	@Column(name = "DESCRIPTION", columnDefinition = "TEXT")
	private String desc;
	
	@Column(name = "UNIQUE_ID", unique = true, nullable = false)
	private String uniqueId;
	
	@Column(name = "PRODUCT_NO", unique = true, nullable = false)
	private String productNo;
	
	@Embedded
	private Quantity quantity;
	
	@ManyToOne
	@JoinColumn(name = "STATUS_ID")
	private ProductStatus status;

	@Column(name = "IS_ARCHIVED")
	private boolean archived;
	
	@Column(name = "IS_MOCKED")
	private boolean mocked;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "OWNER_ID")
	private User owner;

	@Embedded
	private Address address;
	
	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(
			name = "CF_PRODUCT_CATEGORY", 
			joinColumns = { @JoinColumn(name = "PRODUCT_ID")},
			inverseJoinColumns = @JoinColumn(name = "CATEGORY_ID"))
	private Set<Category> categories;

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(
			name = "CF_PRODUCT_TAG", 
			joinColumns = { @JoinColumn(name = "PRODUCT_ID")},
			inverseJoinColumns = @JoinColumn(name = "TAG_ID"))
	private Set<Tag> tags;

	@ElementCollection
	@CollectionTable(name = "CF_PROD_ATTRS", joinColumns = @JoinColumn(name = "PRODUCT_ID"))
	private Collection<Attribute> attributes;
	
	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRODUCT_ID")
	private List<Price> prices = new ArrayList<Price>();
	
	@OneToOne(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@JoinColumn(name = "MIN_PRICE_ID")
	private Price minimumPrice;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@JoinColumn(name = "MAX_PRICE_ID")
	private Price maximumPrice;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@JoinColumn(name = "LATEST_PRICE_ID")
	private Price latestPrice;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@JoinColumn(name = "LAST_PRICE_ID")
	private Price lastPrice;
	
	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "PRODUCT_ID")
	private List<Bid> bids = new ArrayList<Bid>();
	
	@OneToOne(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@JoinColumn(name = "HIGHEST_BID_ID")
	private Bid highestBid;
	
	@Column(name = "IS_BID_ENABLED")
	private boolean bidEnabled;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ENABLE_TILL_DATE")
	private Date enableTillDate;

	@Column(name = "INITIAL_BID_AMOUNT")
	private BigDecimal initialBidAmount;
	
	@ManyToOne
	@JoinColumn(name = "BIDDING_CURRENCY_ID")
	private Currency biddingCurrency;
	
	@ManyToOne
	@JoinColumn(name = "BIDDING_UNIT_ID")
	private Unit biddingUnit;
	
	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(table = "CF_PRODUCT", name = "DEFAULT_IMAGE_ID")
	private Image defaultImage;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "PRODUCT_ID")
	private Set<Image> images = new HashSet<Image>();
	
	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(
		name = "CF_PRODUCT_REVIEW",
		joinColumns = @JoinColumn(name = "PRODUCT_ID")
	)
	@Fetch(FetchMode.SUBSELECT)
	private Set<Review> reviews;
	
	@Column(name = "TOTAL_REVIEWS")
	private int totalReviews;
	
	@Column(name = "RATING")
	private double rating;
	
	@Column(name = "IS_CONSUMER_PRODUCT")
	private boolean consumerProduct;
	
	@OneToMany(mappedBy = "product", fetch = FetchType.LAZY)
	private Set<ProductEvent> events;
	
	// Properties of deliverable products
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ASSIGNED_BY")
	private User assignedBy;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ASSIGNED_TO")
	private User assignedTo;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ASSIGNED_ON")
	private Date dateAssigned;
	
	@Column(name = "COMMENTS")
	private String comments;

	public Product() { }
	
	public Product(String name, Quantity quantity, ProductStatus status, User owner) {
		this.name = name;
		this.quantity = quantity;
		this.status = status;
		this.owner = owner;
	}
	
	public void addCategory(Category category) {
		if (Predicates.isNull.test(this.categories)) {
			this.categories = new HashSet<>();
		}
		this.categories.add(category);
	}
	
	public void addTag(Tag tag) {
		if (Predicates.isNull.test(this.tags)) {
			this.tags = new HashSet<>();
		}
		this.tags.add(tag);
	}
	
	public void addAttribute(Attribute attribute) {
		if (Predicates.isNull.test(this.attributes)) {
			this.attributes = new ArrayList<>();
		}
		this.attributes.add(attribute);
	}
	
	public void addPrice(Price price) {
		this.prices.add(price);
	}

	@JsonIgnore
	public Double getTotalPrice() {
		return this.latestPrice.getPrice() * this.quantity.getQuantity();
	}

	public void clearBids() {
		this.bids = null;
	}
	
	public void addBid(Bid bid) {
		if (Predicates.isNull.test(this.bids)) {
			this.bids = new ArrayList<>();
		}
		this.bids.add(bid);
	}

	public void addImage(Image image) {
		if (Predicates.isNull.test(this.images)) {
			this.images = new HashSet<>();
		}
		this.images.add(image);
	}

	public void addEvent(ProductEvent event) {
		if (this.events == null) {
			this.events = new HashSet<>();
		}
		this.events.add(event);
	}

	@JsonIgnore
	public Double totalRating() {
		Double sumOfRating = 0D;
		for (Review review : this.reviews) {
			if (Predicates.notNull.test(review.getRating())) {
				sumOfRating = sumOfRating + review.getRating().getValue();
			}
		}
		if (this.reviews.size() > 0) {
			return sumOfRating / reviews.size();
		}
		return 0D;
	}

	@JsonIgnore
	public int totalReviews() {
		return this.reviews.size();
	}

	public void addReview(Review review) {
		if (Predicates.isNull.test(this.reviews)) {
			this.reviews = new HashSet<>();
		}
		this.reviews.add(review);
	}

	public void incrementTotalReviews() {
		this.totalReviews += 1;
	}

	public void addRating(double rating) {
		this.rating += rating;
	}

	@Override
	public String toString() {
		return "Product [id=" + id + ", name=" + name + ", quantity="
				+ quantity + ", owner=" + owner + "]";
	}

	@JsonIgnore
	public Optional<Bid> highestBidOfUser(String userNo) {
		return this.bids.stream()
			.filter(b -> 
				b.getUser().getUserNo().equalsIgnoreCase(userNo)
			)
			.sorted((b1, b2) -> 
				b2.getPrice().compareTo(b1.getPrice()))
			.findFirst();
	}
	
	@JsonIgnore
	public Optional<Bid> highestBidOfCompany(String uniqueId) {
		return this.bids.stream()
			.filter(b -> Predicates.notNull.test(b.getCompany()) && 
						b.getCompany().getUniqueId().equalsIgnoreCase(uniqueId))
			.sorted((b1, b2) -> 
				b2.getPrice().compareTo(b1.getPrice()))
			.findFirst();
	}
	
	@JsonIgnore
	public boolean isRecent() {
		String pattern = "dd/MM/yyyy";
		java.text.DateFormat dateFormat = new java.text.SimpleDateFormat(pattern);
		LocalDate currentDate = LocalDate.now();
		
		DateTimeFormatter indianFormatter = DateTimeFormatter.ofPattern(pattern);
		LocalDate createdDate = LocalDate.parse(dateFormat.format(this.getDateCreated()), indianFormatter);
		
		long numberOfDays = ChronoUnit.DAYS.between(createdDate, currentDate);
		if (numberOfDays <= 5) {
			return true;
		}
		return false;
	}
}
