package com.carefarm.prakrati.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.common.entity.Country;
import com.carefarm.prakrati.common.entity.State;

@Embeddable
@Getter
@Setter
public class Address implements Serializable {
	
	private static final long serialVersionUID = -6839083840206043514L;
	
	@Column(name = "ADDRESS_LINE_1")
	private String addressLine1;
	
	@Column(name = "ADDRESS_LINE_2")
	private String addressLine2;
	
	@Column(name = "CITY")
	private String city;
	
	@ManyToOne
	@JoinColumn(name = "STATE_ID")
	private State state;
	
	@ManyToOne
	@JoinColumn(name = "COUNTRY_ID")
	private Country country;
	
	@Column(name = "ZIPCODE")
	private String zipcode;
	
}
