package com.carefarm.prakrati.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "CF_MENUITEM")
@Getter
@Setter
public class Menuitem extends AbstractEntity {
	
	@Id
	@GenericGenerator(name = "menuitem_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_MENUITEM"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "menuitem_sequence_generator", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID")
	private Long id;

	@Column(name = "UNIQUE_ID", unique = true, nullable = false)
	private String uniqueId;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "KEYWORDS")
	private String keywords;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "CATEGORY_ID")
	private Category category;
	
	@Column(name = "DISPLAY_INDEX")
	private int displayIndex;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "PARENT_MENUITEM_ID")
	private Menuitem parentMenuitem;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ROOT_MENUITEM_ID")
	private Menuitem rootMenuitem;
	
	@OneToMany(mappedBy = "parentMenuitem", fetch = FetchType.EAGER)
	@Fetch(FetchMode.SUBSELECT)
	private List<Menuitem> childMenuitems;
	
	@Column(name = "IS_ACTIVE")
	private boolean active;
	
	@Column(name = "IS_ARCHIVED")
	private boolean archived;

	@Override
	public String toString() {
		return "Menuitem [id=" + id + ", version=" + this.getVersion() + ", uniqueId="
				+ uniqueId + ", name=" + name + ", keywords=" + keywords
				+ ", category=" + category + ", displayIndex=" + displayIndex
				+ ", childMenuitems=" + childMenuitems + ", archived="
				+ archived + "]";
	}
	
}
