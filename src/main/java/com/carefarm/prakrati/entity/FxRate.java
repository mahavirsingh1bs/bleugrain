package com.carefarm.prakrati.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

import com.carefarm.prakrati.common.entity.Currency;

@Entity
@Table(name = "CF_FX_RATE")
@Getter
@Setter
public class FxRate extends AbstractEntity implements Serializable {
	
	private static final long serialVersionUID = -8180895306782943352L;

	@Id
	@GenericGenerator(name = "fx_rate_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_FX_RATE"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "fx_rate_sequence_generator", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID")
	private Long id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FROM_CURRENCY_ID")
	private Currency fromCurrency;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "TO_CURRENCY_ID")
	private Currency toCurrency;
	
	@Column(name = "FX_RATE")
	private BigDecimal fxRate;	
	
	@Column(name = "IS_ARCHIVED")
	private boolean archived;
	
	@Column(name = "IS_ACTIVE")
	private boolean active;
	
}
