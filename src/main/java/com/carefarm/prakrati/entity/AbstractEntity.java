package com.carefarm.prakrati.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;

@MappedSuperclass
@Getter
@Setter
public class AbstractEntity {

	@Version
    @Column(name = "OBJ_VERSION")
	@Setter(AccessLevel.NONE)
    private int version = 0;
	
	@ManyToOne
	@JoinColumn(name = "CREATED_BY")
	@CreatedBy
	private User createdBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_ON")
	@CreatedDate
	private Date dateCreated;
	
	@ManyToOne
	@JoinColumn(name = "MODIFIED_BY")
	@LastModifiedBy
	private User modifiedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "MODIFIED_ON")
	@LastModifiedDate
	private Date dateModified;
	
	
}
