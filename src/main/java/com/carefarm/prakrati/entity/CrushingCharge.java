package com.carefarm.prakrati.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

import com.carefarm.prakrati.common.entity.Unit;

@Entity
@Table(name = "CF_FLOOR_CHARGE")
@Getter
@Setter
public class CrushingCharge {
	@Id
	@GenericGenerator(name = "crushing_charge_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_FLOOR_CHARGE"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "crushing_charge_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@Version
    @Column(name = "OBJ_VERSION")
	@Setter(AccessLevel.NONE)
    private int version = 0;

	@Column(name = "CHARGE")
	private double charge;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "UNIT_ID")
	private Unit unit;
	
}
