package com.carefarm.prakrati.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

import com.carefarm.prakrati.constants.Predicates;

@Entity
@Table(name = "CF_DISCUSSION")
@Getter
@Setter
public class Discussion extends AbstractEntity implements Serializable {
	
	private static final long serialVersionUID = 3358609481263405212L;

	@Id
	@GenericGenerator(name = "discussion_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_DISCUSSION"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "discussion_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;

	@Column(name = "UNIQUE_ID", unique = true, nullable = false)
	private String uniqueId;
	
	@Column(name = "TITLE")
	private String title;
	
	@Column(name = "DESCRIPTION")
	private String description;
	
	@Column(name = "IS_ARCHIVED")
	private boolean archived;
	
	@ManyToMany
	@JoinTable(
			name = "CF_DISCUSSION_GROUP", 
			joinColumns = { @JoinColumn(name = "DISCUSSION_ID")},
			inverseJoinColumns = @JoinColumn(name = "GROUP_ID"))
	private Set<Group> groups;
	
	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "DISCUSSION_ID")
	private List<Comment> comments;
	
	@ManyToMany
	@JoinTable(
			name = "CF_DISCUSSION_TAG", 
			joinColumns = { @JoinColumn(name = "DISCUSSION_ID")},
			inverseJoinColumns = @JoinColumn(name = "TAG_ID"))
	private Set<Tag> tags;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_COMMENT_DATE")
	private Date lastCommentDate;

	@ManyToOne
	@JoinColumn(name = "USER_ID")
	private User user;
	
	@ManyToOne
	@JoinColumn(name = "COMPANY_ID")
	private User company;
	
	public void addGroup(Group group) {
		if (Predicates.isNull.test(this.groups)) {
			this.groups = new HashSet<>();
		}
		this.groups.add(group);
	}

	public void addTag(Tag tag) {
		if (Predicates.isNull.test(this.tags)) {
			this.tags = new HashSet<>();
		}
		this.tags.add(tag);
	}
	
}
