package com.carefarm.prakrati.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.carefarm.prakrati.common.entity.Country;

@Entity
@Table(name = "CF_CONTACT")
@Getter
@Setter
public class Contact extends AbstractEntity implements Serializable {

	private static final long serialVersionUID = 4482891556427755971L;

	@Embeddable
	@EqualsAndHashCode
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Id implements Serializable {
		
		private static final long serialVersionUID = -2089040898670946051L;

		@Column(name = "GROUP_ID")
		private Long groupId;
		
		@Column(name = "USER_ID")
		private Long userId;
	
	}
	
	@EmbeddedId
	private Id id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "GROUP_ID", insertable = false, updatable = false)
	private Group group;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID", insertable = false, updatable = false)
	private User user;

	@Column(name = "AREA")
	private String area;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COUNTRY_ID", insertable = false, updatable = false)
	private Country country;

	@Column(name = "MESSAGE")
	private String message;
	
	@Column(name = "IS_ACTIVE")
	private boolean isActive;
	
}
