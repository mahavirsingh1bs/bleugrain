package com.carefarm.prakrati.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

import com.carefarm.prakrati.util.Currency;
import com.carefarm.prakrati.util.PricingPeriod;

@Entity
@Table(name = "CF_PLAN")
@Getter
@Setter
public class Plan implements Serializable {
	
	private static final long serialVersionUID = -3980738639037217571L;

	@Id
	@GenericGenerator(name = "plan_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_PLAN"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "plan_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;

	@Version
    @Column(name = "OBJ_VERSION")
	@Setter(AccessLevel.NONE)
    private int version = 0;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "NO_OF_USERS")
	private int noOfUsers;
	
	@Column(name = "PRICE")
	private BigDecimal price;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "CURRENCY")
	private Currency currency;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "PRICING_BY")
	private PricingPeriod pricingPeriod;

}
