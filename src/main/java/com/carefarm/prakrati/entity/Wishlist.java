package com.carefarm.prakrati.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

import com.carefarm.prakrati.constants.Predicates;

@Entity
@Table(name = "CF_WISHLIST")
@Getter
@Setter
public class Wishlist extends AbstractEntity {
	
	@Id
	@GenericGenerator(name = "wishlist_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_WISHLIST"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "wishlist_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;

	@Column(name = "UNIQUE_ID")
	private String uniqueId;
	
	@ManyToMany
	@JoinTable(name = "CF_WISHLIST_PRODUCT", 
		joinColumns = @JoinColumn(name = "WISHLIST_ID"),
		inverseJoinColumns = @JoinColumn(name = "PRODUCT_ID"))
	private Set<Product> products;
	
	@ManyToMany
	@JoinTable(name = "CF_WISHLIST_DEMAND", 
		joinColumns = @JoinColumn(name = "WISHLIST_ID"),
		inverseJoinColumns = @JoinColumn(name = "DEMAND_ID"))
	private Set<Demand> demands;
	
	@Column(name = "IS_ACTIVE")
	private boolean archived;

	@Column(name = "IS_MOCKED")
	private boolean mocked;
	
	@ManyToOne
	@JoinColumn(name = "USER_ID")
	private User user;
	
	@ManyToOne
	@JoinColumn(name = "COMPANY_ID")
	private Company company;
	
	public int totalItems() {
		int totalItems = 0;
		if (Predicates.notNull.test(this.products)) {
			totalItems = this.products.size();
		}
		
		if (Predicates.notNull.test(this.demands)) {
			totalItems += this.demands.size();
		} 
		return totalItems;
	}

	public void addProduct(Product product) {
		if (Predicates.isNull.test(this.products)) {
			this.products = new HashSet<>();
		}
		this.products.add(product);
	}
	
	public void removeProduct(String uniqueId) {
		if (Predicates.isNull.test(uniqueId)) {
			throw new IllegalArgumentException("Invalid unique id");
		}
		
		Set<Product> newProducts = new HashSet<>();  
		
		this.products
			.stream()
			.filter((p) -> !p.getUniqueId().equals(uniqueId))
			.forEach(newProducts::add);
		
		this.setProducts(newProducts);
	}

	public void addDemand(Demand demand) {
		if (Predicates.isNull.test(this.demands)) {
			this.demands = new HashSet<>();
		}
		this.demands.add(demand);
	}
	
	public void removeDemand(String uniqueId) {
		if (Predicates.isNull.test(uniqueId)) {
			throw new IllegalArgumentException("Invalid unique id");
		}
		
		Set<Demand> newDemands = new HashSet<>();  
		
		this.demands
			.stream()
			.filter((p) -> !p.getUniqueId().equals(uniqueId))
			.forEach(newDemands::add);
		
		this.setDemands(newDemands);
	}
		
}
