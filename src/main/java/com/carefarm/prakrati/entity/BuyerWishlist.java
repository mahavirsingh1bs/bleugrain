package com.carefarm.prakrati.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Entity
@Table(name = "CF_BUYER_WISHLIST")
@Getter
@Setter
public class BuyerWishlist extends AbstractEntity implements Serializable {
	private static final long serialVersionUID = 1L;

	@Embeddable
	@Getter
	@EqualsAndHashCode
	@NoArgsConstructor
	@AllArgsConstructor
	public static class Id implements Serializable {
		
		private static final long serialVersionUID = 1L;
		
		@Column(name = "PRODUCT_ID")
		private Long productId;
		
		@Column(name = "BUYER_ID")
		private Long buyerId;
		
	}
	
	@EmbeddedId
	private Id id;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "PRODUCT_ID", insertable = false, updatable = false)
	private Product product;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "BUYER_ID", insertable = false, updatable = false)
	private User buyer;

	public BuyerWishlist() { }
	
	public BuyerWishlist(Product product, User buyer) {
		this.product = product;
		this.buyer = buyer;
		
		this.id = new Id(product.getId(), buyer.getId());
	}
	
}
