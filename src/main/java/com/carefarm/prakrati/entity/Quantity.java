package com.carefarm.prakrati.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.common.entity.Unit;

@Embeddable
@Getter
@Setter
@EqualsAndHashCode
public class Quantity {

	@Column(name = "QUANTITY")
	private Double quantity;
	
	@ManyToOne
	@JoinColumn(name = "QUNIT_ID")
	private Unit unit;

	public Quantity() { }
	
	public Quantity(Double quantity, Unit unit) {
		this.quantity = quantity;
		this.unit = unit;
	}
		
}
