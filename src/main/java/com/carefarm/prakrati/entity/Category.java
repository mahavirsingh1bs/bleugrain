package com.carefarm.prakrati.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;

import com.carefarm.prakrati.constants.Predicates;

@Entity
@Table(name = "CF_CATEGORY")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Getter
@Setter
public class Category extends AbstractEntity implements Serializable {
	
	private static final long serialVersionUID = 4024689042363532489L;

	@Id
	@GenericGenerator(name = "category_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_CATEGORY"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "category_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@Column(name = "UNIQUE_ID", unique = true, nullable = false)
	private String uniqueId;

	private String name;

	@Column(name = "SHORT_DESC", length = 4000)
	private String desc;
	
	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(table = "CF_CATEGORY", name = "DEFAULT_IMAGE_ID")
	private Image defaultImage;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "PARENT_CATEGORY_ID")
	private Category parentCategory;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ROOT_CATEGORY_ID")
	private Category rootCategory;
	
	@OneToMany(mappedBy = "parentCategory", fetch = FetchType.LAZY)
	@Fetch(FetchMode.SUBSELECT)
	private List<Category> childCategories = new ArrayList<Category>();

	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(
		name = "CF_CATEGORY_REVIEW",
		joinColumns = @JoinColumn(name = "CATEGORY_ID")
	)
	@Fetch(FetchMode.SUBSELECT)
	private List<Review> reviews;
	
	@Column(name = "IS_ACTIVE")
	private boolean active;
	
	@Column(name = "IS_ARCHIVED")
	private boolean archived;

	public Category() { }
	
	public Category(String name) { 
		this.name = name;
	}
	
	public boolean isRootNode() {
		if (Predicates.isNull.test(this.parentCategory)) {
			return true;
		}
		return false;
	}
}
