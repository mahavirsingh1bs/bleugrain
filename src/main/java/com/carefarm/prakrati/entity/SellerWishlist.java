package com.carefarm.prakrati.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "CF_SELLER_WISHLIST")
@Getter
@Setter
public class SellerWishlist extends AbstractEntity implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Embeddable
	@Getter
	@NoArgsConstructor
	@AllArgsConstructor
	@EqualsAndHashCode
	public static class Id implements Serializable {
		private static final long serialVersionUID = 1L;
		
		@Column(name = "DEMAND_ID")
		private Long demandId;
		
		@Column(name = "SELLER_ID")
		private Long sellerId;
		
	}
	
	@EmbeddedId
	private Id id;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "DEMAND_ID", insertable = false, updatable = false)
	private Demand demand;
	
	@ManyToOne
	@JoinColumn(name = "SELLER_ID", insertable = false, updatable = false)
	private User seller;
	
	public SellerWishlist() { }
	
	public SellerWishlist(Demand demand, User seller) {
		this.demand = demand;
		this.seller = seller;
		
		this.id = new Id(demand.getId(), seller.getId());
	}
		
}