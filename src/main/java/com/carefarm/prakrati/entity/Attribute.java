package com.carefarm.prakrati.entity;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import lombok.Getter;
import lombok.Setter;

@Embeddable
@Getter
@Setter
public class Attribute {
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "VALUE")
	private String value;
		
}
