package com.carefarm.prakrati.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;

import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "CF_EXPERTISE")
@Getter
@Setter
public class Expertise extends AbstractEntity implements Serializable {
	
	private static final long serialVersionUID = -7683294381023559008L;

	@Id
	@GenericGenerator(name = "expertise_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_EXPERTISE"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "expertise_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;

	@Column(name = "UNIQUE_ID", unique = true, nullable = false)
	private String uniqueId;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "EXPERIENCE")
	private String experience;
	
	@Column(name = "EXPR_LABEL")
	private String experLabel;
	
	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(
		name = "CF_EXPERTISE_REVIEW",
		joinColumns = @JoinColumn(name = "EXPERTISE_ID")
	)
	private Set<Review> reviews;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID")
	private User user;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY_ID")
	private Company company;
	
	public Double getRatingPercent() {
		int totalReviews = this.reviews.size();
		Double totalRating = 0D;
		for (Review review : this.reviews) {
			totalRating = totalRating + review.getRating().getValue();
		}
		
		if (!totalRating.equals(BigDecimal.ZERO)) {
			return (totalRating / totalReviews * 5) * 100;
		} else {
			return 0D;
		}
	}
	
}
