package com.carefarm.prakrati.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "CF_ACCOUNT")
@Getter
@Setter
public class Account implements Serializable {
	
	private static final long serialVersionUID = 6739588642387072751L;

	@Id
	@GenericGenerator(name = "account_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_ACCOUNT"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "account_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;

	@Version
    @Column(name = "OBJ_VERSION")
	@Setter(AccessLevel.NONE)
    private int version = 0;
	
	@Column(name = "ACCOUNT_NO")
	private String accountNo;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "BRANCH")
	private String branch;
	
	@Column(name = "BANK_NAME")
	private String bankName;
	
	@Column(name = "IFSC_CODE")
	private String ifscCode;
	
	@Column(name = "IS_PRIMARY")
	private boolean isPrimary;
	
	@Column(name = "IS_ACTIVE")
	private boolean isActive;
	
	@ManyToOne
	@JoinColumn(name = "OWNER_ID")
	private User owner;
	
}
