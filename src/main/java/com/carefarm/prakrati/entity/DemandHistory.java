package com.carefarm.prakrati.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

import com.carefarm.prakrati.common.entity.DemandStatus;
import com.carefarm.prakrati.common.entity.PricePerUnit;

@Entity
@Table(name = "CF_DEMAND_HISTORY")
@Getter
@Setter
public class DemandHistory extends AbstractEntity implements Serializable {

	private static final long serialVersionUID = -4417944901710943507L;

	@Id
	@GenericGenerator(name = "demand_history_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_DEMAND_HISTORY"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "demand_history_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name = "DEMAND_ID")
	private Demand demand;
	
	@Column(name = "NAME")
	private String name;
	
	@Embedded
	private Quantity quantity;

	@Embedded
	private PricePerUnit pricePerUnit;
		
	@Column(name = "DELIVERY_DATE")
	private Date deliveryDate;
	
	@Embedded
	private Address deliveryAddress;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REQUEST_DATE")
	private Date requestDate;
	
	@ManyToOne
	@JoinColumn(name = "STATUS_ID")
	private DemandStatus status;

	@Column(name = "IS_ARCHIVED")
	private boolean archived;
	
	@ManyToOne
	@JoinColumn(name = "USER_ID")
	private User user;
	
	@ManyToOne
	@JoinColumn(name = "COMPANY_ID")
	private Company company;
		
}
