package com.carefarm.prakrati.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.common.entity.Rating;

@Embeddable
@Getter
@Setter
public class Review implements Serializable {

	private static final long serialVersionUID = 2404027798377031225L;

	@Column(name = "NAME")
	private String name;
	
	@Column(name = "EMAIL_ADDRESS")
	private String emailAddress;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "RATING_ID")
	private Rating rating;

	@Column(name = "COMMENT")
	private String comment;

	@Column(name = "IS_VALID")
	private Boolean valid;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "REVIEW_BY")
	private User reviewBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REVIEW_DATE")
	private Date reviewDate;

	public Review() { }
	
	public Review(Rating rating) {
		this.rating = rating;
	}
		
}
