package com.carefarm.prakrati.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "CF_NOTIF_CATEGORY")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Getter
@Setter
public class NotificationCategory extends AbstractEntity {
	
	@Id
	@GenericGenerator(name = "notification_category_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_NOTIFICATION_CATEGORY"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "notification_category_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@Column(name = "CODE")
	private String code;
	
	@Column(name = "NAME")
	private String name;

	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(table = "CF_NOTIF_CATEGORY", name = "DEFAULT_IMAGE_ID")
	private Image defaultImage;
	
	@Column(name = "IS_ARCHIVED")
	private boolean archived;

}
