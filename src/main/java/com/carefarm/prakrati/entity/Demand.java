package com.carefarm.prakrati.entity;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

import com.carefarm.prakrati.common.entity.Currency;
import com.carefarm.prakrati.common.entity.DemandStatus;
import com.carefarm.prakrati.common.entity.Unit;
import com.carefarm.prakrati.constants.Predicates;

@Entity
@Table(name = "CF_DEMAND")
@Getter
@Setter
public class Demand extends AbstractEntity implements Serializable {

	private static final long serialVersionUID = 1901296914934791772L;

	@Id
	@GenericGenerator(name = "demand_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_DEMAND"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "demand_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;

	@Column(name = "UNIQUE_ID", unique = true, nullable = false)
	private String uniqueId;

	@Column(name = "NAME")
	private String name;

	@Column(name = "SHORT_DESC")
	private String shortDesc;
	
	@Lob
	@Column(name = "DESCRIPTION", columnDefinition = "TEXT")
	private String desc;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CATEGORY_ID")
	private Category category;
	
	@Embedded
	private Quantity quantity;

	@Column(name = "MIN_PRICE_PER_UNIT")
	private Double minPricePerUnit;
	
	@Column(name = "MAX_PRICE_PER_UNIT")
	private Double maxPricePerUnit;
	
	@Column(name = "PRICE_PER_UNIT")
	private Double pricePerUnit;
	
	@ManyToOne
	@JoinColumn(name = "CURRENCY_ID")
	private Currency currency;
	
	@ManyToOne
	@JoinColumn(name = "PUNIT_ID")
	private Unit unit;
		
	@Column(name = "DELIVERY_DATE")
	private Date deliveryDate;
	
	@Embedded
	private Address deliveryAddress;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "REQUEST_DATE")
	private Date requestDate;
	
	@ManyToOne
	@JoinColumn(name = "STATUS_ID")
	private DemandStatus status;

	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(table = "CF_DEMAND", name = "DEFAULT_IMAGE_ID")
	private Image defaultImage;
	
	@Column(name = "IS_ARCHIVED")
	private boolean archived;

	@Column(name = "IS_MOCKED")
	private boolean mocked;
	
	@Column(name = "BUYER_CATEGORY")
	private String buyerCategory;
	
	@ManyToMany
	@JoinTable(
			name = "CF_DEMAND_TAG", 
			joinColumns = { @JoinColumn(name = "DEMAND_ID")},
			inverseJoinColumns = @JoinColumn(name = "TAG_ID"))
	private Set<Tag> tags;

	@ManyToOne
	@JoinColumn(name = "USER_ID")
	private User user;
	
	/**
	 * This field assigned a value if this is a company demand
	 */
	@ManyToOne
	@JoinColumn(name = "COMPANY_ID")
	private Company company;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ASSIGNED_BY")
	private User assignedBy;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "ASSIGNED_TO")
	private User assignedTo;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ASSIGNED_ON")
	private Date dateAssigned;

	@Column(name = "COMMENTS")
	private String comments;

	public void addTag(Tag tag) {
		if (Predicates.isNull.test(this.tags)) {
			this.tags = new HashSet<>();
		}
		this.tags.add(tag);
	}
	
	public boolean isRecent() {
		String pattern = "dd/MM/yyyy";
		DateFormat dateFormat = new SimpleDateFormat(pattern);
		LocalDate currentDate = LocalDate.now();
		
		DateTimeFormatter indianFormatter = DateTimeFormatter.ofPattern(pattern);
		LocalDate createdDate = LocalDate.parse(dateFormat.format(this.getDateCreated()), indianFormatter);
		
		long numberOfDays = ChronoUnit.DAYS.between(createdDate, currentDate);
		if (numberOfDays <= 5) {
			return true;
		}
		return false;
	}
}
