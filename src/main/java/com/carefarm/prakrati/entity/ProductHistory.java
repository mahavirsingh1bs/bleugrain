package com.carefarm.prakrati.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

import com.carefarm.prakrati.common.entity.ProductStatus;

@Entity
@Table(name = "CF_PRODUCT_HISTORY")
@Getter
@Setter
public class ProductHistory extends AbstractEntity implements Serializable {

	private static final long serialVersionUID = -5872567331221012833L;

	@Id
	@GenericGenerator(name = "product_history_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_PRODUCT_HISTORY"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "product_history_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "PRODUCT_ID")
	private Product product;
	
	@Column(name = "NAME")
	private String name;

	@Lob
	@Column(name = "DESCRIPTION", 
		columnDefinition = "TEXT", 
		table = "CF_PRODUCT_HISTORY")
	private String desc;
	
	@Embedded
	private Quantity quantity;
	
	@ManyToOne
	@JoinColumn(name = "STATUS_ID")
	private ProductStatus status;

	@Column(name = "IS_ARCHIVED")
	private boolean archived;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "OWNER_ID")
	private User owner;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "CATEGORY_ID")
	private Category category;

	@Column(name = "TAGS")
	private String tags;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@JoinColumn(name = "LATEST_PRICE_ID")
	private Price latestPrice;
	
	@OneToOne(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@JoinColumn(name = "LAST_PRICE_ID")
	private Price lastPrice;
	
	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(table = "CF_PRODUCT_HISTORY", name = "DEFAULT_IMAGE_ID")
	private Image defaultImage;
		
}
