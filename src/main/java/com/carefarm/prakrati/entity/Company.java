package com.carefarm.prakrati.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.payment.entity.BillingDetails;
import com.carefarm.prakrati.util.BusinessCategory;

@Entity
@Table(name = "CF_COMPANY")
@NamedQueries({
	@NamedQuery(
			name = "Company.findByNameContains", 
			query = "SELECT c FROM Company c WHERE c.name LIKE :name"),
	@NamedQuery(
			name = "Company.findByKeywordContains", 
			query = "SELECT c FROM Company c WHERE c.name LIKE :keyword OR c.address.addressLine1 LIKE :keyword OR "
					+ "c.address.addressLine2 LIKE :keyword OR c.address.city LIKE :keyword OR "
					+ "c.address.state.name LIKE :keyword OR c.address.country.name LIKE :keyword OR "
					+ "c.address.zipcode LIKE :keyword")
})
@Getter
@Setter
public class Company extends AbstractEntity implements Serializable {
	
	private static final long serialVersionUID = 4074095433475747408L;

	@Id
	@GenericGenerator(name = "company_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_COMPANY"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "company_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@Column(name = "UNIQUE_ID")
	private String uniqueId;
	
	@Column(name = "NAME")
	private String name;

	@Enumerated(EnumType.STRING)
	@Column(name = "BUSINESS_CATEGORY", nullable = false)
	private BusinessCategory businessCategory;
	
	@Column(name = "SHORT_DESC", length = 4000)
	private String shortDesc;
	
	@Column(name = "REGISTR_NO")
	private String registrNo;
	
	@Column(name = "DOMAIN")
	private String domain;
	
	@Column(name = "IS_ARCHIVED")
	private boolean archived;
	
	@Column(name = "IS_MOCKED")
	private boolean mocked;

	@Column(name = "VERIFIED")
	private boolean verified;
	
	@Embedded
	private Address address;
	
	@Column(name = "WEBSITE_URL")
	private String websiteUrl;
	
	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(table = "CF_COMPANY", name = "DEFAULT_BILLING_ADDRESS_ID")
	private AddressEntity defaultBillingAddress;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@JoinColumn(name = "COMPANY_ID")
	private List<AddressEntity> billingAddresses;
	
	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(table = "CF_COMPANY", name = "DEFAULT_DELIVERY_ADDRESS_ID")
	private AddressEntity defaultDeliveryAddress;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "COMPANY_ID")
	private List<AddressEntity> deliveryAddresses;
	
	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(table = "CF_COMPANY", name = "DEFAULT_BILLING_DETAILS_ID")
	private BillingDetails defaultBillingDetails;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "COMPANY_ID")
	private List<BillingDetails> billingDetails;
	
	@Column(name = "PHONE_NO")
	private String phoneNo;

	@Column(name = "EMAIL_ID")
	private String emailId;
	
	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(table = "CF_COMPANY", name = "DEFAULT_IMAGE_ID")
	private Image defaultImage;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "COMPANY_ID")
	private Set<Image> images = new HashSet<>();
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
	private List<User> agents = new ArrayList<>();
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "company")
	private List<User> admins = new ArrayList<>();
	
	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY_ID")
	private List<PaymentMethod> paymentMethods = new ArrayList<>();
	
	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY_ID")
	private List<Branch> branches = new ArrayList<Branch>();

	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(
		name = "CF_COMPANY_REVIEW",
		joinColumns = @JoinColumn(name = "COMPANY_ID")
	)
	private Set<Review> reviews;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "COMPANY_ID")
	private List<SocialAccount> socialAccounts;

	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "COMPANY_ID")
	private List<Expertise> expertises;
	
	public Company() { }
	
	public Company(String name) {
		this.name = name;
	}
	
	public Company(String name, String registrNo) {
		this.name = name;
		this.registrNo = registrNo;
	}
	
	public void addBillingAddress(AddressEntity billingAddress) {
		if (Predicates.isNull.test(this.billingAddresses)) {
			this.billingAddresses = new ArrayList<>();
		}
		this.billingAddresses.add(billingAddress);
	}
	
	public void addDeliveryAddress(AddressEntity deliveryAddress) {
		if (Predicates.isNull.test(this.deliveryAddresses)) {
			this.deliveryAddresses = new ArrayList<>();
		}
		this.deliveryAddresses.add(deliveryAddress);
	}
	
	public void addImage(Image image) {
		if (this.images == null) {
			this.images = new HashSet<>();
		}
		this.images.add(image);
	}
	public void addAgent(User agent) {
		if (Predicates.isNull.test(this.agents)) {
			this.agents = new ArrayList<>();
		}
		this.agents.add(agent);
	}

	public void addAdmin(User admin) {
		if (Predicates.isNull.test(this.admins)) {
			this.admins = new ArrayList<>();
		}
		this.admins.add(admin);
	}
	
	public void addPaymentMethod(PaymentMethod paymentMethod) {
		if (Predicates.isNull.test(this.paymentMethods)) {
			this.paymentMethods = new ArrayList<>();
		}
		this.paymentMethods.add(paymentMethod);
	}

	public void addBranch(Branch branch) {
		this.branches.add(branch);
	}
	
	public void addReview(Review review) {
		if (reviews == null) {
			reviews = new HashSet<Review>();
		}
		this.reviews.add(review);
	}
	
	public void addSocialAccount(SocialAccount socialAccount) {
		if (Predicates.isNull.test(this.socialAccounts)) {
			this.socialAccounts = new ArrayList<>();
		}
		this.socialAccounts.add(socialAccount);
	}

	public void addExpertise(Expertise expertise) {
		if (Predicates.isNull.test(this.expertises)) {
			this.expertises = new ArrayList<>();
		}
		this.expertises.add(expertise);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Company other = (Company) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Company [id=" + id + ", name=" + name + "]";
	}
	
}
