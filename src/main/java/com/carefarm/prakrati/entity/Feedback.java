package com.carefarm.prakrati.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

import com.carefarm.prakrati.common.entity.Rating;

@Entity
@Table(name = "CF_FEEDBACK")
@Getter
@Setter
public class Feedback implements Serializable {
	
	private static final long serialVersionUID = -2654443520086116667L;

	@Id
	@GenericGenerator(name = "feedback_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_FEEDBACK"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "feedback_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;

	@Version
    @Column(name = "OBJ_VERSION")
	@Setter(AccessLevel.NONE)
    private int version = 0;
	
	@ManyToOne
	@JoinColumn(name = "SERVICES_RATING")
	private Rating servicesRating;
	
	@ManyToOne
	@JoinColumn(name = "EXECUTIVES_RATING")
	private Rating executivesRating;
	
	@ManyToOne
	@JoinColumn(name = "SITE_RATING")
	private Rating siteRating;
	
	@Column(name = "FEEDBACK")
	private String feedback;

	@ManyToOne
	@JoinColumn(name = "USER_ID")
	private User user;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CREATED_ON")
	private Date createdOn;
		
}
