package com.carefarm.prakrati.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

import com.carefarm.prakrati.common.entity.OrderStatus;

@Entity
@Table(name = "CF_ORDER_HISTORY")
@Getter
@Setter
public class OrderHistory extends AbstractEntity implements Serializable {

	private static final long serialVersionUID = 5738420404759487969L;

	@Id
	@GenericGenerator(name = "order_history_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_ORDER_HISTORY"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "order_history_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "ORDER_ID")
	private Order order;
	
	@Column(name = "ORDER_NO", unique = true, nullable = false)
	private String orderNo;

	@Column(name = "SUBTOTAL")
	private BigDecimal subtotal;
	
	@Column(name = "SHIPPING_COST")
	private BigDecimal shippingCost;
	
	@Column(name = "TOTAL_PRICE")
	private BigDecimal totalPrice;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "ORDER_DATE")
	private Date orderDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DELIVERY_DATE")
	private Date deliveryDate;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "BILLING_ADDRESS_ID")
	private AddressEntity billingAddress;
	
	@ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	@JoinColumn(name = "SHIPPING_ADDRESS_ID")
	private AddressEntity shippingAddress;
	
	@Column(name = "AMOUNT_PAID")
	private BigDecimal amountPaid;
	
	@ManyToOne
	@JoinColumn(name = "STATUS_ID")
	private OrderStatus status;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "BUYER_ID")
	private User buyer;
	
	@Column(name = "IS_ARCHIVED")
	private boolean archived;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY_ID")
	private Company company;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "FULFILLED_BY")
	private Company fullfilledBy;
	
}
