
package com.carefarm.prakrati.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "CF_INDIVIDUAL_PLAN")
@Getter
@Setter
public class IndividualPlan implements Serializable {

	private static final long serialVersionUID = 3579720093461188030L;
	
	@Embeddable
	public static class Id implements Serializable {
		
		private static final long serialVersionUID = -2089040898670946051L;

		@Column(name = "USER_ID")
		private Long userId;
		
		@Column(name = "PLAN_ID")
		private Long planId;
		
		public Id() { }
		
		public Id(Long userId, Long planId) {
			this.userId = userId;
			this.planId = planId;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result
					+ ((planId == null) ? 0 : planId.hashCode());
			result = prime * result
					+ ((userId == null) ? 0 : userId.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Id other = (Id) obj;
			if (planId == null) {
				if (other.planId != null)
					return false;
			} else if (!planId.equals(other.planId))
				return false;
			if (userId == null) {
				if (other.userId != null)
					return false;
			} else if (!userId.equals(other.userId))
				return false;
			return true;
		}
		
	}
	
	@EmbeddedId
	private Id id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID", insertable = false, updatable = false)
	private User user;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PLAN_ID", insertable = false, updatable = false)
	private Plan plan;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "START_DATE")
	private Date startDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "EXPIRY_DATE")
	private Date expiryDate;
	
	@Column(name = "IS_ACTIVE")
	private boolean isActive;
	
	public IndividualPlan() { }
	
	public IndividualPlan(User user, Plan plan) { 
		this.user = user;
		this.plan = plan;
		
		this.id = new Id(user.getId(), plan.getId());
	}

}
