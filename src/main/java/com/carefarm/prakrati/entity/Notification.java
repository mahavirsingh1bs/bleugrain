package com.carefarm.prakrati.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

import com.carefarm.prakrati.constants.Predicates;

@Entity
@Table(name = "CF_NOTIFICATION")
@Getter
@Setter
public class Notification extends AbstractEntity {
	
	@Id
	@GenericGenerator(name = "notification_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_NOTIFICATION"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "notification_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "CATEGORY_ID")
	private NotificationCategory category;
	
	@Column(name = "TITLE")
	private String title;
	
	@Column(name = "NOTIFICATION")
	private String notification;
	
	@Column(name = "IS_ARCHIVED")
	private boolean archived;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "START_DATE")
	private Date startDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "EXPIRY_DATE")
	private Date expiryDate;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
			name = "CF_NOTIFICATION_GROUP", 
			joinColumns = { @JoinColumn(name = "NOTIFICATION_ID")},
			inverseJoinColumns = @JoinColumn(name = "GROUP_ID"))
	private Set<Group> groups;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
			name = "CF_NOTIFICATION_USER", 
			joinColumns = { @JoinColumn(name = "NOTIFICATION_ID")},
			inverseJoinColumns = @JoinColumn(name = "USER_ID"))
	private Set<User> users;
	
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(
			name = "CF_NOTIFICATION_COMPANY", 
			joinColumns = { @JoinColumn(name = "NOTIFICATION_ID")},
			inverseJoinColumns = @JoinColumn(name = "COMPANY_ID"))
	private Set<Company> companies;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID")
	private User user;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY_ID")
	private Company company;
	
	public void addGroup(Group group) {
		if (Predicates.isNull.test(this.groups)) {
			this.groups = new HashSet<>();
		}
		this.groups.add(group);
	}
	
	public void addUser(User user) {
		if (Predicates.isNull.test(this.users)) {
			this.users = new HashSet<>();
		}
		this.users.add(user);
	}
	
	public void addCompany(Company company) {
		if (Predicates.isNull.test(this.companies)) {
			this.companies = new HashSet<>();
		}
		this.companies.add(company);
	}
		
}
