package com.carefarm.prakrati.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.GenericGenerator;

import com.carefarm.prakrati.common.entity.Country;
import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.delivery.entity.License;
import com.carefarm.prakrati.payment.entity.BillingDetails;
import com.carefarm.prakrati.util.BusinessCategory;
import com.carefarm.prakrati.util.UserType;
import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "CF_USER")
@NamedQuery(name = "User.findByEmailAddress", 
	query = "SELECT u FROM User u WHERE u.email = :emailAddress")
@Getter
@Setter
public class User extends AbstractEntity implements Comparable<User>, Serializable {
	
	private static final long serialVersionUID = -8960987227708511099L;

	@Id
	@GenericGenerator(name = "user_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_USER"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "user_sequence_generator", strategy = GenerationType.SEQUENCE)
	@Column(name = "ID")
	private Long id;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "USER_TYPE", nullable = false)
	private UserType userType;
	
	@Column(name = "USER_NO", unique = true, nullable = false)
	private String userNo;
	
	@Column(name = "AADHAR_ID", unique = true)
	private String aadharId;

	@Column(name = "FIRST_NAME")
	private String firstName;
	
	@Column(name = "MIDDLE_INITIAL")
	private String middleInitial;
	
	@Column(name = "LAST_NAME")
	private String lastName;
	
	@Column(name = "SHORT_DESC", length = 4000)
	private String shortDesc;
	
	@Column(name = "USERNAME", unique = true)
	private String username;
	
	@Column(name = "ALTERNATE_USERNAME", unique = true)
	private String alternateUsername;
	
	@Column(name = "EMAIL")
	private String email;
	
	@Embedded
	protected Address address;
	
	// Broker License
	@Column(name = "LICENSE_NO")
	private String licenseNo;
	
	@Column(name = "PHONE_NO")
	private String phoneNo;
	
	@Column(name = "MOBILE_NO")
	private String mobileNo;
	
	@Column(name = "CF_PASSWD")
	private String password;
	
	@Column(name = "IS_TRANSPORT_AVAILABLE")
	private boolean transportAvailable;

	@Column(name = "UNIQUE_CODE")
	private String uniqueCode;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "LAST_LOGIN_ON")
	private Date lastLoginOn;
	
	@Column(name = "FAILED_LOGIN_ATTEMPT")
	private Integer failedLoginAttempt;

	@Column(name = "MOBILE_VERIFY_CODE")
	private String mobileVerifyCode;
	
	@Column(name = "MOBILE_VERIFIED")
	private boolean mobileVerified;
	
	@Column(name = "EMAIL_VERIFY_CODE")
	private String emailVerifyCode;
	
	@Column(name = "EMAIL_VERIFIED")
	private boolean emailVerified;
	
	@Column(name = "RESET_PASSWD_CODE")
	private String resetPasswdCode;
	
	@Column(name = "IS_ARCHIVED")
	private boolean archived;
	
	@Column(name = "IS_MOCKED")
	private boolean mocked;
	
	@Column(name = "VERIFIED")
	private boolean verified;

	@Column(name = "WEBSITE_URL")
	private String websiteUrl;
	
	@Column(name = "DEFAULT_LOCALE")
	private String defaultLocale;
	
	@OneToOne
	@JoinColumn(name = "DEFAULT_LANGUAGE_ID")
	private Language defaultLanguage;
	
	@OneToOne
	@JoinColumn(name = "DEFAULT_COUNTRY_ID")
	private Country defaultCountry;
	
	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(table = "CF_USER", name = "DEFAULT_BILLING_ADDRESS_ID")
	private AddressEntity defaultBillingAddress;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = { CascadeType.ALL })
	@JoinColumn(name = "USER_ID")
	private List<AddressEntity> billingAddresses;
	
	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(table = "CF_USER", name = "DEFAULT_DELIVERY_ADDRESS_ID")
	private AddressEntity defaultDeliveryAddress;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "USER_ID")
	private List<AddressEntity> deliveryAddresses;
	
	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(table = "CF_USER", name = "DEFAULT_BILLING_DETAILS_ID")
	private BillingDetails defaultBillingDetails;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "USER_ID")
	private List<BillingDetails> billingDetails;
	
	@ManyToOne
	@JoinColumn(name = "GROUP_ID")
	@Fetch(FetchMode.SELECT)
	private Group group;

	@OneToOne(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinColumn(table = "CF_USER", name = "DEFAULT_IMAGE_ID")
	private Image defaultImage;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinColumn(name = "USER_ID")
	private Set<Image> images = new HashSet<>();
	
	@ElementCollection(fetch = FetchType.LAZY)
	@CollectionTable(
		name = "CF_USER_REVIEW",
		joinColumns = @JoinColumn(name = "USER_ID")
	)
	private Set<Review> reviews;
	
	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID")
	private List<SocialAccount> socialAccounts = new ArrayList<>();

	@OneToMany(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID")
	private List<Expertise> expertises = new ArrayList<>();

	// Licenses of the driver
	@OneToMany
	@JoinColumn(name = "DRIVER_ID")
	private List<License> licenses = new ArrayList<License>();
	
	// Company Admin/Agent's company
	@ManyToOne
	@JoinColumn(name = "COMPANY_ID")
	private Company company;
	
	// Company's business category
	@Enumerated(EnumType.STRING)
	@Column(name = "BUSINESS_CATEGORY", nullable = false)
	private BusinessCategory businessCategory;
	
	// Employee's employee id
	@Column(name = "EMPLOYEE_ID")
	private String employeeId;
	
	// Employee's designation
	@ManyToOne
	@JoinColumn(name = "DESIGNATION_ID")
	@Fetch(FetchMode.SELECT)
	private Designation designation;
	
	// Employee's department
	@ManyToOne
	@JoinColumn(name = "DEPARTMENT_ID")
	@Fetch(FetchMode.SELECT)
	private Department department;
	
	public User() { }
	
	public User(String firstName, String lastName, String username) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.username = username;
	}
	
	public User(String firstName, String lastName, String username, 
			String email, String password, String mobileNo) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.username = username;
		this.email = email;
		this.password = password;
		this.mobileNo = mobileNo;
	}
	
	public void addBillingAddress(AddressEntity billingAddress) {
		if (Predicates.isNull.test(this.billingAddresses)) {
			this.billingAddresses = new ArrayList<>();
		}
		this.billingAddresses.add(billingAddress);
	}
	
	public void addDeliveryAddress(AddressEntity deliveryAddress) {
		if (Predicates.isNull.test(this.deliveryAddresses)) {
			this.deliveryAddresses = new ArrayList<>();
		}
		this.deliveryAddresses.add(deliveryAddress);
	}
	
	public void addImage(Image image) {
		if (Predicates.isNull.test(this.images)) {
			this.images.add(image);
		}
	}
	
	public void addReview(Review review) {
		if (Predicates.isNull.test(reviews)) {
			reviews = new HashSet<Review>();
		}
		this.reviews.add(review);
	}

	public void addSocialAccount(SocialAccount socialAccount) {
		if (Predicates.isNull.test(this.socialAccounts)) {
			this.socialAccounts = new ArrayList<>();
		}
		this.socialAccounts.add(socialAccount);
	}

	public void addExpertise(Expertise expertise) {
		if (Predicates.isNull.test(this.expertises)) {
			this.expertises = new ArrayList<>();
		}
		this.expertises.add(expertise);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", firstName=" + firstName + ", lastName="
				+ lastName + ", username=" + username + "]";
	}

	@Override
	public int compareTo(User other) {
		int result = this.firstName.compareTo(other.firstName);
		if (result != 0) {
			return result;
		}
		
		result = this.lastName.compareTo(other.lastName);
		
		return result;
	}
	
	@JsonIgnore
	public boolean hasRole(String roleName) {
		return this.group.hasRole(roleName);
	}

	@JsonIgnore
	public boolean isACompanyUser() {
		return this.group.hasRole("ROLE_COMPANY_ADMIN") || 
				this.group.hasRole("ROLE_AGENT") ;
	}
	
}
