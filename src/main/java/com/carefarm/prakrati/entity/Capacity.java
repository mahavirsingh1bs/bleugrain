package com.carefarm.prakrati.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import com.carefarm.prakrati.common.entity.Unit;

@Embeddable
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class Capacity {
	@Column(name = "CAPACITY")
	private BigDecimal capacity;
	
	@ManyToOne
	@JoinColumn(name = "CUNIT_ID")
	private Unit unit;

	public Capacity(BigDecimal capacity) {
		this.capacity = capacity;
	}
	
}
