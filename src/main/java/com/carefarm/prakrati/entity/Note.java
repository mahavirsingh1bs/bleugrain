package com.carefarm.prakrati.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "CF_NOTE")
@Getter
@Setter
public class Note extends AbstractEntity {
	
	@Id
	@GenericGenerator(name = "note_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_NOTE"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "note_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;

	@Column(name = "UNIQUE_ID", unique = true, nullable = false)
	private String uniqueId;
	
	@Column(name = "TITLE")
	private String title;
	
	@Column(name = "NOTE", length = 1000)
	private String note;
	
	@Column(name = "IS_ARCHIVED")
	private boolean archived;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID")
	private User user;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY_ID")
	private User company;
	
}
