package com.carefarm.prakrati.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

import com.carefarm.prakrati.common.entity.Country;
import com.carefarm.prakrati.common.entity.State;
import com.carefarm.prakrati.util.AddressCategory;

@Entity
@Table(name = "CF_ADDRESS")
@Getter
@Setter
public class AddressEntity extends AbstractEntity implements Serializable {
	
	private static final long serialVersionUID = 8190082857118972593L;

	@Id
	@GenericGenerator(name = "address_entity_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_ADDRESS"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "address_entity_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@Column(name = "FIRST_NAME")
	private String firstName;
	
	@Column(name = "LAST_NAME")
	private String lastName;
	
	@Column(name = "EMAIL_ADDRESS")
	private String emailAddress;
	
	@Column(name = "PHONE_NO")
	private String phoneNo;
	
	@Column(name = "ALTERNATE_PHONE_NO")
	private String alternatePhoneNo;
	
	@Column(name = "ADDRESS_LINE_1")
	private String addressLine1;
	
	@Column(name = "ADDRESS_LINE_2")
	private String addressLine2;
	
	@Column(name = "CITY")
	private String city;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "ADDRESS_CATEGORY")
	private AddressCategory addressCategory;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "STATE_ID")
	private State state;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COUNTRY_ID")
	private Country country;
	
	@Column(name = "ZIPCODE")
	private String zipcode;

	@Column(name = "IS_ARCHIVED")
	private boolean archived;
	
	@Column(name = "IS_DEFAULT_ADDRESS")
	private boolean defaultAddress;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID")
	private User user;

	@ManyToOne
	@JoinColumn(name = "COMPANY_ID")
	private Company company;
	
	@Override
	public String toString() {
		return "AddressEntity [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", emailAddress="
				+ emailAddress + ", phoneNo=" + phoneNo + ", addressLine1=" + addressLine1 + ", addressLine2="
				+ addressLine2 + ", city=" + city + ", addressCategory=" + addressCategory + ", state=" + state
				+ ", country=" + country + ", zipcode=" + zipcode + ", archived=" + archived + ", defaultAddress="
				+ defaultAddress + "]";
	}
	
}
