package com.carefarm.prakrati.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

import com.carefarm.prakrati.constants.Predicates;

@Entity
@Table(name = "CF_GRAIN_CART")
@Getter
@Setter
public class GrainCart extends AbstractEntity implements Serializable {
	
	private static final long serialVersionUID = 8459359211524018677L;

	@Id
	@GenericGenerator(name = "grain_cart_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_GRAIN_CART"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "grain_cart_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;

	@ManyToMany
	@JoinTable(name = "CF_CART_PRODUCT", 
		joinColumns = @JoinColumn(name = "GRAIN_CART_ID"),
		inverseJoinColumns = @JoinColumn(name = "PRODUCT_ID"))
	private Set<Product> products;
	
	@OneToMany(cascade = { CascadeType.MERGE, CascadeType.PERSIST })
	@JoinColumn(name = "GRAIN_CART_ID")
	private Set<Item> items;
	
	@Column(name = "IS_CONSUMER_CART")
	private boolean consumerCart;
	
	@OneToOne
	@JoinColumn(name = "USER_ID")
	private User user;
	
	@ManyToOne
	@JoinColumn(name = "COMPANY_ID")
	private Company company;

	public int totalItems() {
		if (Predicates.notNull.test(this.products) && this.products.size() > 0) {
			return this.products.size();
		} else if (Predicates.notNull.test(this.items) && this.items.size() > 0) {
			return this.items.size();
		} else {
			return 0;
		}
	}

	public void addProduct(Product product) {
		if (Predicates.isNull.test(this.products)) {
			this.products = new HashSet<>();
		}
		this.products.add(product);
	}
	
	public void removeProduct(String uniqueId) {
		if (Predicates.isNull.test(uniqueId)) {
			throw new IllegalArgumentException("Invalid unique id");
		}
		
		Set<Product> newProducts = new HashSet<>();  
		
		this.products
			.stream()
			.filter((p) -> !p.getUniqueId().equals(uniqueId))
			.forEach(newProducts::add);
		
		this.setProducts(newProducts);
	}
	
	public void addItem(Item item) {
		if (Predicates.isNull.test(this.items)) {
			this.items = new HashSet<>();
		}
		this.items.add(item);
	}

	public void removeItem(String uniqueId) {
		if (Predicates.isNull.test(uniqueId)) {
			throw new IllegalArgumentException("Invalid unique id");
		}
		
		Set<Item> newItems = new HashSet<>();  
		
		this.items
			.stream()
			.filter((i) -> !i.getUniqueId().equals(uniqueId))
			.forEach(newItems::add);
		
		this.setItems(newItems);
	}

	public Double getSubtotal() {
		Double totalCost = 0D;
	
		if (!consumerCart) {
			for (Product product : this.products) {
				totalCost = totalCost + (product.getQuantity().getQuantity() * product.getLatestPrice().getPrice());
			}
		} else {
			for (Item item : this.items) {
				totalCost = totalCost + (item.getQuantity().getQuantity() * item.getPrice().getPrice());
			}
		}
		
		return totalCost;
	}
	
	public Double getShippingCost() {
		Double subTotal = 0D;
		if (this.getSubtotal().equals(BigDecimal.ZERO)) {
			return subTotal;
		}
		
		if (this.getSubtotal().compareTo(300D) > 0) {
			return subTotal;
		}
		
		return 99.00D;
	}
	
}
