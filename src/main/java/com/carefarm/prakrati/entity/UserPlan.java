package com.carefarm.prakrati.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "CF_USER_PLAN")
@Getter
@Setter
public class UserPlan implements Serializable {
	
	private static final long serialVersionUID = -6120098008616121230L;

	@Embeddable
	@Getter
	@NoArgsConstructor
	@AllArgsConstructor
	@EqualsAndHashCode
	public static class Id implements Serializable {
		
		private static final long serialVersionUID = -2089040898670946051L;

		@Column(name = "USER_ID")
		private Long userId;
		
		@Column(name = "PLAN_ID")
		private Long planId;
				
	}
	
	@EmbeddedId
	private Id id;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID", insertable = false, updatable = false)
	private User user;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "PLAN_ID", insertable = false, updatable = false)
	private Plan plan;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "START_DATE")
	private Date startDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "EXPIRY_DATE")
	private Date expiryDate;
	
	@Column(name = "IS_ACTIVE")
	private boolean isActive;
	
	public UserPlan() { }
	
	public UserPlan(User user, Plan plan) { 
		this.user = user;
		this.plan = plan;
		
		this.id = new Id(user.getId(), plan.getId());
		
		//user.addUserPlan(this);
	}
	
}
