package com.carefarm.prakrati.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

import com.carefarm.prakrati.util.SocialSite;

@Entity
@Table(name = "CF_SOCIAL_ACCOUNTS")
@Getter
@Setter
public class SocialAccount extends AbstractEntity {
	
	@Id
	@GenericGenerator(name = "social_account_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_SOCIAL_ACCOUNTS"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "social_account_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@Column(name = "UNIQUE_ID", unique = true, nullable = false)
	private String uniqueId;
	
	@Column(name = "DISPLAY_NAME")
	private String displayName;
	
	@Column(name = "USERNAME")
	private String username;
	
	@Column(name = "ACCOUNT_URL")
	private String accountUrl;
	
	@Enumerated(EnumType.STRING)
	@Column(name = "SOCIAL_SITE")
	private SocialSite socialSite;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "USER_ID")
	private User user;
	
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "COMPANY_ID")
	private Company company;
	
	@Override
	public String toString() {
		return "SocialAccount [id=" + id + ", version=" + this.getVersion() + ", displayName=" + displayName + ", username="
				+ username + ", accountUrl=" + accountUrl + ", socialSite=" + socialSite + "]";
	}
	
}
