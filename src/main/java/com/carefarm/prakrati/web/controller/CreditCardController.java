package com.carefarm.prakrati.web.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.service.CreditCardService;
import com.carefarm.prakrati.web.bean.CreditCardBean;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.CreditCardForm;

@Controller
@RequestMapping("/creditCards")
public class CreditCardController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(CreditCardController.class);
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private CreditCardService creditCardService;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showCreditCards(SearchRequest searchRequest) {
		return creditCardService.findByKeyword(searchRequest);
	}
	
	@RequestMapping(value="/new", method=RequestMethod.POST)
	public @ResponseBody MessageBean addCreditCard(@Valid @RequestBody CreditCardForm form, Authentication authentication, BindingResult result, RedirectAttributes redirectAttributes){
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
    	try {
			creditCardService.create(form, user);
			redirectAttributes.addFlashAttribute("message", "Credit Card has been added successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while add credit card", e);
			redirectAttributes.addFlashAttribute("message", "Got error while adding credit card");
			return new MessageBean("error", "Got error while adding credit card");
		}
        
        return new MessageBean("success", "Credit Card has added successfully");
	}
	
	@RequestMapping(value="/newBillingDetails", method=RequestMethod.POST)
	public @ResponseBody CreditCardBean addBillingDetails(@Valid @RequestBody CreditCardForm form, Authentication authentication, BindingResult result, RedirectAttributes redirectAttributes){
		if(result.hasErrors()) {
			LOGGER.error("Cannot add the billing details. Got some validation errors");;
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
    	Map<String, Object> params = new HashMap<>();
    	if (httpSession.getAttribute("companyId") != null) {
    		Long companyId = (Long )httpSession.getAttribute("companyId");
    		params.put("companyId", companyId);
    	} else if (httpSession.getAttribute("userId") != null) {
    		Long userId = (Long ) httpSession.getAttribute("userId");
    		params.put("userId", userId);
    	}
    	
        try {
			CreditCardBean creditCardBean = creditCardService.create(form, params, user);
			redirectAttributes.addFlashAttribute("message", "Credit Card has been added successfully.");
			return creditCardBean;
		} catch (ServiceException e) {
			LOGGER.error("Got error while trying to add credit card", e);
			redirectAttributes.addFlashAttribute("message", "Got error while adding credit card");
		}
        
        return null;
	}
	
	@RequestMapping(value="/data/{uniqueId}", method=RequestMethod.GET)
	public @ResponseBody CreditCardBean getData(@PathVariable("uniqueId") String uniqueId, ModelMap modelMap) {
		return creditCardService.findDetail(uniqueId);
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public @ResponseBody MessageBean update(@Valid @RequestBody CreditCardBean creditCardBean, Authentication authentication, 
			BindingResult result, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
			creditCardService.update(creditCardBean, user);
			redirectAttributes.addFlashAttribute("message", "Credit card has been updated successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while updating credit card", e);
			redirectAttributes.addFlashAttribute("message", "Got error while updating credit card");
			return new MessageBean("error", "Got error while updating credit card");
		}
        
        return new MessageBean("success", "Credit card has been updated successfully.");
	}
	
	@RequestMapping(value = "/delete/{uniqueId}", method = RequestMethod.DELETE)
	public @ResponseBody MessageBean delete(@PathVariable("uniqueId") String uniqueId) {
		creditCardService.delete(uniqueId);
		return new MessageBean("success", "Credit Card has been deleted successfully");
	}
}
