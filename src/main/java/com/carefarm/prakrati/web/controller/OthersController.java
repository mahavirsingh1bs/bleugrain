package com.carefarm.prakrati.web.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.service.CustomerCareService;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.model.ContactUsForm;
import com.carefarm.prakrati.web.model.FeedbackForm;

@Controller
@RequestMapping("/others")
public class OthersController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

	@Autowired
	private CustomerCareService customerCareService;
	
	@Autowired
	private HttpSession httpSession;
	
	@RequestMapping(value = "/contactUs", method = RequestMethod.POST)
	public @ResponseBody MessageBean contactUs(@Valid @RequestBody ContactUsForm form, BindingResult result) {
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

		Map<String, Object> params = new HashMap<>();
		if(ObjectUtils.notEqual(httpSession.getAttribute("userId"), null)) { 
			params.put("userId", httpSession.getAttribute("userId"));
		}
		
        try {
        	customerCareService.processConcern(form, params);
		} catch (ServiceException e) {
			LOGGER.error("Got some error while handling concern", e);
			return new MessageBean("error", "Got some error while handling concern");
		}
        
        return new MessageBean("success", "Your concern has been got submitted successfully.");
	}
	
	@RequestMapping(value = "/feedback", method = RequestMethod.POST)
	public @ResponseBody MessageBean feedback(@Valid @RequestBody FeedbackForm form, BindingResult result) {
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

		Map<String, Object> params = new HashMap<>();
		if(ObjectUtils.notEqual(httpSession.getAttribute("userId"), null)) { 
			params.put("userId", httpSession.getAttribute("userId"));
		}
		
        try {
        	customerCareService.processFeedback(form, params);
		} catch (ServiceException e) {
			LOGGER.error("Got some error while handling concern", e);
			return new MessageBean("error", "Got some error while handling feedback");
		}
        
        return new MessageBean("success", "We successfully received your feedback. Thank you very much for your precious time.");
	}
	
	
}
