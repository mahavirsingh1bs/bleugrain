package com.carefarm.prakrati.web.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.service.BankAccountService;
import com.carefarm.prakrati.web.bean.BankAccountBean;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.BankAccountForm;

@Controller
@RequestMapping("/bankAccounts")
public class BankAccountController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BankAccountController.class);
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private BankAccountService bankAccountService;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showUserCreditCards(SearchRequest request) {
		return bankAccountService.findByKeyword(request);
	}
	
	@RequestMapping(value="/new", method=RequestMethod.POST)
	public @ResponseBody MessageBean newBankAccount(@Valid @RequestBody BankAccountForm form, 
			Authentication authentication, BindingResult result, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
    	
        try {
			bankAccountService.create(form, user);
			redirectAttributes.addFlashAttribute("message", "Bank Account has been added successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while add bank account", e);
			redirectAttributes.addFlashAttribute("message", "Got error while adding bank account");
			return new MessageBean("error", "Got error while adding bank account");
		}
        
        return new MessageBean("success", "Bank account has created successfully");
	}
	
	@RequestMapping(value="/newBillingDetails", method=RequestMethod.POST)
	public @ResponseBody BankAccountBean addBillingDetails(@Valid @RequestBody BankAccountForm form, Authentication authentication, BindingResult result, RedirectAttributes redirectAttributes){
		if(result.hasErrors()) {
			LOGGER.error("Cannot add the billing details. Got some validation errors");;
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
    	Map<String, Object> params = new HashMap<>();
    	if (httpSession.getAttribute("companyId") != null) {
    		Long companyId = (Long )httpSession.getAttribute("companyId");
    		params.put("companyId", companyId);
    	} else if (httpSession.getAttribute("userId") != null) {
    		Long userId = (Long ) httpSession.getAttribute("userId");
    		params.put("userId", userId);
    	}
    	
        try {
			BankAccountBean bankAccountBean = bankAccountService.create(form, params, user);
			redirectAttributes.addFlashAttribute("message", "Bank Account has been added successfully.");
			return bankAccountBean;
		} catch (ServiceException e) {
			LOGGER.error("Got error while trying to add bank account", e);
			redirectAttributes.addFlashAttribute("message", "Got error while adding bank account");
		}
        
        return null;
	}
	
	@RequestMapping(value="/data/{uniqueId}", method=RequestMethod.GET)
	public @ResponseBody BankAccountBean getData(@PathVariable("uniqueId") String uniqueId, ModelMap modelMap) {
		return bankAccountService.findDetail(uniqueId);
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public @ResponseBody MessageBean update(@Valid @RequestBody BankAccountBean bankAccountBean, BindingResult result, 
			RedirectAttributes redirectAttributes, Authentication authentication) {
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
			bankAccountService.update(bankAccountBean, user);
			redirectAttributes.addFlashAttribute("message", "Bank Account has been updated successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while updating bank account", e);
			redirectAttributes.addFlashAttribute("message", "Got error while updating bank account");
			return new MessageBean("error", "Got error while updating bank account");
		}
        
        return new MessageBean("success", "Bank Account has been updated successfully.");
	}
	
	@RequestMapping(value = "/delete/{uniqueId}", method = RequestMethod.DELETE)
	public @ResponseBody MessageBean delete(@PathVariable("uniqueId") String uniqueId) {
		bankAccountService.delete(uniqueId);
		return new MessageBean("success", "Bank Account has been deleted successfully");
	}
}
