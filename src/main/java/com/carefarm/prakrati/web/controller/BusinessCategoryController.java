package com.carefarm.prakrati.web.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.carefarm.prakrati.util.BusinessCategory;
import com.carefarm.prakrati.web.bean.BusinessCategoryBean;

@Controller
@RequestMapping("/businessCategories")
public class BusinessCategoryController {
	
	@RequestMapping(value = "/all", method = RequestMethod.GET)
    public @ResponseBody List<BusinessCategoryBean> allBusinessCategories() {
		List<BusinessCategoryBean> businessCategories = new ArrayList<>();
		for (BusinessCategory businessCategory : BusinessCategory.values()) {
			businessCategories.add(new BusinessCategoryBean(businessCategory.name(), businessCategory.getName()));
		}
    	return businessCategories;
    }

}
