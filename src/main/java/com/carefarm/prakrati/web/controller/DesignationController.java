package com.carefarm.prakrati.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.service.DesignationService;
import com.carefarm.prakrati.web.bean.DesignationBean;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.DesignationForm;

@Controller
@RequestMapping("/designations")
public class DesignationController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(DesignationController.class);
	
	@Autowired
	private DesignationService designationService;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showDesignations(SearchRequest searchRequest) {
		LOGGER.info("Finding all the designations");
		if (searchRequest.getPageSize() > 0) {
	    	return designationService.findByKeyword(searchRequest);
    	} else {
    		Map<String, Object> data = new HashMap<>();
    		List<DesignationBean> designations = designationService.findAll();
    		data.put("designations", designations);
    		return data;
    	}
	}

	@RequestMapping(value="/new", method=RequestMethod.POST)
	public @ResponseBody MessageBean newDesignation(@Valid @RequestBody DesignationForm form, BindingResult result, 
			Authentication authentication, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
            return new MessageBean("failure", "Form has validation errors.");
        }
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
        	designationService.create(form, user);
			redirectAttributes.addFlashAttribute("message", "Designation has been added successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got a server error. So designation cannot be added now.", e);
			return new MessageBean("error", "Got server error while adding designation.");
		}
        
        return new MessageBean("success", "Designation has been added successfully");
	}
	
	@RequestMapping(value="/data/{designationId}", method=RequestMethod.GET)
	public @ResponseBody DesignationBean getDesignationData(@PathVariable("designationId") long designationId, ModelMap modelMap){
		return designationService.findDetails(designationId, null);
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public @ResponseBody MessageBean update(@Valid @RequestBody DesignationBean designationBean, BindingResult result, 
			RedirectAttributes redirectAttributes, Authentication authentication) {
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
			designationService.update(designationBean, user);
			redirectAttributes.addFlashAttribute("message", "Designation has been updated successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while creating designation", e);
			redirectAttributes.addFlashAttribute("message", "Got error while updating designation");
			return new MessageBean("error", "Got error while updating designation");
		}
        
        return new MessageBean("success", "Designation has been updated successfully.");
	}
	
	@RequestMapping(value = "/delete/{uniqueId}", method = RequestMethod.DELETE)
	public @ResponseBody MessageBean delete(@PathVariable("uniqueId") String uniqueId) {
		LOGGER.info("Deleting the designation " + uniqueId);
		designationService.delete(uniqueId);
		return new MessageBean("success", "Designation has been deleted successfully");
	}
}
