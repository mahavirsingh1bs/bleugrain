package com.carefarm.prakrati.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.service.AddressService;
import com.carefarm.prakrati.util.AddressCategory;
import com.carefarm.prakrati.web.bean.AddressEntityBean;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.model.AddressEntityForm;

@Controller
@RequestMapping("/addresses")
public class AddressController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AddressController.class);
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private AddressService addressService;
	
	@RequestMapping(value="/delivery", method=RequestMethod.GET)
	public @ResponseBody List<AddressEntityBean> showDeliveryAddresses(HttpServletRequest request) {
		Map<String, Object> params = new HashMap<>();
    	if (httpSession.getAttribute("companyId") != null) {
    		Long companyId = (Long )httpSession.getAttribute("companyId");
    		params.put("companyId", companyId);
    	} else if (httpSession.getAttribute("userId") != null) {
    		Long userId = (Long )httpSession.getAttribute("userId");
    		params.put("userId", userId);
    	}
    	
		return addressService.findDeliveryAddresses(params);
	}
	
	@RequestMapping(value="/{uniqueId}", method=RequestMethod.GET)
	public @ResponseBody AddressEntityBean editDeliveryAddress(@PathVariable("uniqueId") String uniqueId) {
		return addressService.findAddressDetails(uniqueId);
	}
	
	@RequestMapping(value="/billing/update", method=RequestMethod.POST)
	public @ResponseBody MessageBean updateBillingAddress(@Valid @RequestBody AddressEntityBean address, 
			Authentication authentication, BindingResult result, RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			return new MessageBean("failure", "Billing address has not been updated successfully");
		}
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User loggedInUser = (User )token.getPrincipal();
		
		Map<String, Object> params = new HashMap<>();
    	if (httpSession.getAttribute("companyId") != null) {
    		Long companyId = (Long )httpSession.getAttribute("companyId");
    		params.put("companyId", companyId);
    	} else if (httpSession.getAttribute("userId") != null) {
    		Long userId = (Long )httpSession.getAttribute("userId");
    		params.put("userId", userId);
    	}
    	
    	params.put("category", AddressCategory.BILLING);
    	
		if (addressService.updateAddress(address, params, loggedInUser)) {
			return new MessageBean("success", "Billing address has been updated successfully");
		} else {
			return new MessageBean("failure", "Billing address has not been updated successfully");
		}
	}
	
	@RequestMapping(value="/delivery/update", method=RequestMethod.POST)
	public @ResponseBody MessageBean updateDeliveryAddress(@Valid @RequestBody AddressEntityBean address, 
			Authentication authentication, BindingResult result, RedirectAttributes redirectAttributes) {
		if (result.hasErrors()) {
			return new MessageBean("failure", "Delivery address has not been updated successfully");
		}
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User loggedInUser = (User )token.getPrincipal();
		
		Map<String, Object> params = new HashMap<>();
    	if (httpSession.getAttribute("companyId") != null) {
    		Long companyId = (Long )httpSession.getAttribute("companyId");
    		params.put("companyId", companyId);
    	} else if (httpSession.getAttribute("userId") != null) {
    		Long userId = (Long )httpSession.getAttribute("userId");
    		params.put("userId", userId);
    	}
    	
    	params.put("category", AddressCategory.DELIVERY);
    	
		if (addressService.updateAddress(address, params, loggedInUser)) {
			return new MessageBean("success", "Delivery address has been updated successfully");
		} else {
			return new MessageBean("failure", "Delivery address has not been updated successfully");
		}
	}
	
	@RequestMapping(value="/billing", method=RequestMethod.GET)
	public @ResponseBody List<AddressEntityBean> showBillingAddresses(HttpServletRequest request) {
		Map<String, Object> params = new HashMap<>();
    	if (httpSession.getAttribute("companyId") != null) {
    		Long companyId = (Long )httpSession.getAttribute("companyId");
    		params.put("companyId", companyId);
    	} else if (httpSession.getAttribute("userId") != null) {
    		Long userId = (Long )httpSession.getAttribute("userId");
    		params.put("userId", userId);
    	}
    	
		return addressService.findBillingAddresses(params);
	}
	
	@RequestMapping(value="/defaultBilling", method=RequestMethod.GET)
	public @ResponseBody AddressEntityBean showDefaultBillingAddress(HttpServletRequest request) {
		Map<String, Object> params = new HashMap<>();
    	if (httpSession.getAttribute("companyId") != null) {
    		Long companyId = (Long )httpSession.getAttribute("companyId");
    		params.put("companyId", companyId);
    	} else if (httpSession.getAttribute("userId") != null) {
    		Long userId = (Long )httpSession.getAttribute("userId");
    		params.put("userId", userId);
    	}
    	
		return addressService.findDefaultBillingAddress(params);
	}
	
	@RequestMapping(value="/defaultDelivery", method=RequestMethod.GET)
	public @ResponseBody AddressEntityBean showDefaultDeliveryAddress(HttpServletRequest request) {
		Map<String, Object> params = new HashMap<>();
    	if (httpSession.getAttribute("companyId") != null) {
    		Long companyId = (Long )httpSession.getAttribute("companyId");
    		params.put("companyId", companyId);
    	} else if (httpSession.getAttribute("userId") != null) {
    		Long userId = (Long )httpSession.getAttribute("userId");
    		params.put("userId", userId);
    	}
    	
		return addressService.findDefaultDeliveryAddress(params);
	}
	
	@RequestMapping(value="/delivery/new", method=RequestMethod.POST)
	public @ResponseBody ResponseEntity<MessageBean> newDeliveryAddress(@RequestParam("userNo") String userNo, @Valid @RequestBody AddressEntityForm addressForm, BindingResult result, Authentication authentication) {
		if(result.hasErrors()) {
            LOGGER.error("form has some validation errors.");
        }
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		addressService.addDeliveryAddress(addressForm, userNo, currentUser);
    		MessageBean messageBean = new MessageBean("success", "Your delivery address has been added successfully.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException exception) {
			LOGGER.error("Got error while trying to add delivery address.", exception);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to add delivery address.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/billing/new", method=RequestMethod.POST)
	public @ResponseBody ResponseEntity<MessageBean> newBillingAddress(@RequestParam("userNo") String userNo, @Valid @RequestBody AddressEntityForm addressForm, BindingResult result, Authentication authentication){
		if(result.hasErrors()) {
            LOGGER.error("form has some validation errors.");
        }
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		addressService.addDeliveryAddress(addressForm, userNo, currentUser);
    		MessageBean messageBean = new MessageBean("success", "Your billing address has been added successfully.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException exception) {
			LOGGER.error("Got error while trying to add billing address.", exception);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to add billing address.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(value = "/delete/{uniqueId}", method = RequestMethod.DELETE)
	public @ResponseBody MessageBean delete(@PathVariable("uniqueId") String uniqueId, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User loggedInUser = (User )token.getPrincipal();
		addressService.delete(uniqueId, loggedInUser);
		return new MessageBean("success", "Address has been deleted successfully");
	}
}
