package com.carefarm.prakrati.web.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.service.MenuitemService;
import com.carefarm.prakrati.web.bean.MenuitemBean;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.MenuitemForm;

@Controller
@RequestMapping("/menuitems")
public class MenuitemController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(MenuitemController.class);
    
    @Autowired
    private MenuitemService menuitemService;
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public @ResponseBody List<MenuitemBean> getMenuitems() {
    	LOGGER.info("Finding the main categories");
    	return menuitemService.findMainMenuitems();
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public @ResponseBody Map<String, Object>  getAllMenuitems(SearchRequest searchRequest) {
    	LOGGER.info("Finding the all categories");
	    return menuitemService.findByKeyword(searchRequest);
    }

    @RequestMapping(value="/new", method=RequestMethod.POST)
	public @ResponseBody MessageBean newMenuitem(@Valid @RequestBody MenuitemForm form, BindingResult result, 
			Authentication authentication, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
            return new MessageBean("failure", "Form has validation errors.");
        }
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
			menuitemService.create(form, user);
			redirectAttributes.addFlashAttribute("message", "Menuitem has been added successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got a server error. So menuitem cannot be added now.", e);
			return new MessageBean("error", "Got server error while adding menuitem.");
		}
        
        return new MessageBean("success", "Menuitem has been added successfully");
	}
    
    @RequestMapping(value="/data/{menuitemId}", method=RequestMethod.GET)
	public @ResponseBody MenuitemBean getMenuitemData(@PathVariable("menuitemId") String menuitemId, ModelMap modelMap){
		return menuitemService.findDetails(menuitemId);
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public @ResponseBody MessageBean update(@Valid @RequestBody MenuitemBean menuitemBean, BindingResult result, 
			RedirectAttributes redirectAttributes, Authentication authentication) {
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
			menuitemService.update(menuitemBean, user);
			redirectAttributes.addFlashAttribute("message", "Menuitem has been updated successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while creating menuitem", e);
			redirectAttributes.addFlashAttribute("message", "Got error while updating menuitem");
			return new MessageBean("error", "Got error while updating menuitem");
		}
        
        return new MessageBean("success", "Menuitem has been updated successfully.");
	}
    
    @RequestMapping(value = "/delete/{uniqueId}", method = RequestMethod.DELETE)
	public @ResponseBody MessageBean delete(@PathVariable("uniqueId") String uniqueId) {
    	LOGGER.info("Deleting the menuitem with id " + uniqueId);
		menuitemService.delete(uniqueId);
		return new MessageBean("success", "Menuitem has been deleted successfully");
	}
    
}
