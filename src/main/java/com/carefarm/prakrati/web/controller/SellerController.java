package com.carefarm.prakrati.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.carefarm.prakrati.service.UserService;
import com.carefarm.prakrati.web.bean.UserBean;

@Controller
@RequestMapping("/sellers")
public class SellerController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SellerController.class);

	@Autowired
	private UserService userService;
	
	@RequestMapping(value="/data/{userNo}", method=RequestMethod.GET)
	public @ResponseBody UserBean getDetail(@PathVariable("userNo") String userNo) {
		LOGGER.info("Finding the sellet detail");
		return userService.findDetail(userNo);
	}
}
