package com.carefarm.prakrati.web.controller;

import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.service.NotificationService;
import com.carefarm.prakrati.web.bean.NotificationBean;
import com.carefarm.prakrati.web.model.Message;
import com.carefarm.prakrati.web.model.OutputMessage;

@Controller
public class NotificationController {

	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationController.class);
	
	@Autowired
	private SimpMessagingTemplate template;

	@Autowired
	private NotificationService notificationService;
	
	@RequestMapping("/notifications/all")
	public @ResponseBody List<NotificationBean> userNotifications(Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
		return notificationService.findAll(currentUser); 
	}
	
	@MessageMapping("/notification/send")
    @SendTo("/topic/notifications")
    public OutputMessage notifications(Message message) {
		LOGGER.info("Message sent");
		OutputMessage outputMessage = new OutputMessage(message, new Date());
		template.convertAndSend("/topic/notifications", outputMessage);
	    return outputMessage;
	}
}
