package com.carefarm.prakrati.web.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.service.DemandService;
import com.carefarm.prakrati.web.bean.DemandBean;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.DemandAssignForm;
import com.carefarm.prakrati.web.model.DemandForm;
import com.carefarm.prakrati.web.model.PriceUpdateForm;

@Controller
@RequestMapping("/demands")
public class DemandController {

	private static final Logger LOGGER = LoggerFactory.getLogger(DemandController.class);
	
	@Autowired
	private DemandService demandService;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showAllDemands(SearchRequest searchRequest) {
		LOGGER.info("START: Finding all the demands");
		return demandService.findByKeyword(searchRequest);
	}
	
	@RequestMapping(value="/user", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showUserDemands(SearchRequest searchRequest) {
		LOGGER.info("START: Finding all the user demands");
		return demandService.findByKeyword(searchRequest);
	}
	
	@RequestMapping(value="/company", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showCompanyDemands(SearchRequest searchRequest) {
		LOGGER.info("START: Finding all the company demands");
		return demandService.findByKeyword(searchRequest);
	}
	
	@RequestMapping(value="/new", method=RequestMethod.POST)
	public @ResponseBody MessageBean newDemand(@Valid @RequestPart("demand") DemandForm form, BindingResult result,
			@RequestPart("file") List<MultipartFile> files, Authentication authentication, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
			demandService.create(form, files, user);
			redirectAttributes.addFlashAttribute("message", "Demand has been added successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while add demand", e);
			redirectAttributes.addFlashAttribute("message", "Got error while adding demand");
			return new MessageBean("error", "Got error while adding demand");
		}
        
        return new MessageBean("success", "Demand has created successfully");
	}
	
	@RequestMapping(value="/data/{uniqueId}", method=RequestMethod.GET)
	public @ResponseBody DemandBean getData(@PathVariable("uniqueId") String uniqueId, ModelMap modelMap) {
		return demandService.findDetail(uniqueId);
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public @ResponseBody MessageBean update(@Valid @RequestBody DemandBean demandBean, Authentication authentication, 
			BindingResult result, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
			demandService.update(demandBean, user);
			redirectAttributes.addFlashAttribute("message", "Demand has been updated successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while updating demand", e);
			redirectAttributes.addFlashAttribute("message", "Got error while updating demand");
			return new MessageBean("error", "Got error while updating demand");
		}
        
        return new MessageBean("success", "Demand has been updated successfully.");
	}

	@RequestMapping(value="/assign", method=RequestMethod.POST)
	public @ResponseBody MessageBean assign(@RequestBody DemandAssignForm form,
			Authentication authentication) {
		LOGGER.info("START: assigning the demand to the " + form.getAssignTo().getFullName());
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
		demandService.assign(form, currentUser);
		return new MessageBean("success", "Demand has been assigned successfully");
	}
	
	@RequestMapping(value="/fulfilled", method=RequestMethod.POST)
	public @ResponseBody MessageBean markAsFulfilled(@RequestParam("uniqueId") String uniqueId,
			Authentication authentication) {
		LOGGER.info("START: marking demand as fulfilled");
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
		demandService.markAsFulfilled(uniqueId, currentUser);
		return new MessageBean("success", "Demand has been marked as fulfilled");
	}
	
	@RequestMapping(value = "/delete/{uniqueId}", method = RequestMethod.DELETE)
	public @ResponseBody MessageBean delete(@PathVariable("uniqueId") String uniqueId, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
		
		demandService.delete(uniqueId, currentUser);
		return new MessageBean("success", "Demand has been deleted successfully");
	}
	
	@RequestMapping(value="/update/price", method=RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> updatePrice(@Valid @RequestBody PriceUpdateForm form, BindingResult result, 
			Authentication authentication) {
		if(result.hasErrors()) {
			MessageBean messageBean = new MessageBean("error", "form has errors");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
        }
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		demandService.updatePrice(form, currentUser);
			MessageBean messageBean = new MessageBean("success", "Price has been updated successfully");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
		} catch (ServiceException exception) {
			LOGGER.error("Got error while trying to update price.", exception);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to update prices.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
}
