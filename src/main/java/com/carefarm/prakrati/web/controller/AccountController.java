package com.carefarm.prakrati.web.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.service.UserService;
import com.carefarm.prakrati.validators.ChangePasswdValidator;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.model.ChangePasswdForm;

@Controller
@RequestMapping("/account")
public class AccountController {
	private static final Logger LOGGER = LoggerFactory.getLogger(AccountController.class);
	
	@Value("${application.context.path}")
	private String contextPath;
	
	@Autowired
	private UserService userService;

	@Autowired
	private ChangePasswdValidator changePasswdValidator;
	
	@RequestMapping(value="/forgotPasswd", method=RequestMethod.POST, consumes = { "application/json" })
	public @ResponseBody MessageBean forgotPassword(@RequestBody String emailAddress, HttpServletRequest request) {
		LOGGER.info("Got a forgot password request for email address " + emailAddress);
		try {
			userService.forgotPassword(emailAddress, contextPath);
			return new MessageBean("success", "Password reset link has been sent to your email address");
		} catch (ServiceException ex) {
			LOGGER.error("Got some exception while trying to send reset link to Email Address: " + emailAddress, ex);
			return new MessageBean("error", ex.getMessage());
		}
	}
	
	@RequestMapping(value="/resetPasswd", method=RequestMethod.POST)
	public @ResponseBody MessageBean resetPasswd(@RequestParam("resetCode") String resetCode, @RequestBody String newPasswd, HttpServletRequest request) {
		LOGGER.info("Got a reset password request for reset code " + resetCode);
		try {
			userService.resetPasswd(newPasswd, resetCode);
		} catch (ServiceException e) {
			LOGGER.error("Got some exception while trying to reset the password", e);
			return new MessageBean("failure", "Got an exception while trying to reset the password");
		}
		return new MessageBean("success", "Password got reset successfully");
	}
	
	@RequestMapping(value="/validateResetCode", method=RequestMethod.POST)
	public @ResponseBody MessageBean validateResetCode(@RequestBody String resetCode, HttpServletRequest request) {
		LOGGER.info("Validating the reset code " + resetCode);
		if (!userService.validateCode(resetCode)) {
			return new MessageBean("failure", "Reset code is not valid");
		}
		return new MessageBean("success", "Reset code is valid");
	}
	
	@RequestMapping(value="/changePasswd", method=RequestMethod.POST)
	public ResponseEntity<?> changePasswd(@RequestBody @Valid ChangePasswdForm form, BindingResult result) {
		changePasswdValidator.validate(form, result);
		if(result.hasErrors()) {
			MessageBean messageBean = new MessageBean("error", "Form has validation errors", result.getFieldErrors(), result.getGlobalErrors());
            return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
        }
		try {
			userService.changePasswd(form);
			MessageBean messageBean = new MessageBean("success", "Your password have got changed successfully.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
		} catch (ServiceException e) {
			MessageBean messageBean = new MessageBean("failure", "We cannot process your password change request now.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
}
