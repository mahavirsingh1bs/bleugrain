package com.carefarm.prakrati.web.controller;

import static com.carefarm.prakrati.web.util.PrakratiConstants.DEFAULT_PAGE_SIZE;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.web.bean.UserBean;

@RestController
@RequestMapping("/users")
public class UserRestController {

	@Autowired
	private DozerBeanMapper mapper;
	
	@Autowired
	private UserRepository userRepository;

	@RequestMapping(value="/all", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<UserBean> getAllBrokers(@PageableDefault(size = DEFAULT_PAGE_SIZE) Pageable pageable) {
		List<UserBean> users = new ArrayList<>();
		for (User user : userRepository.findAll(pageable)) {
			UserBean userBean = mapper.map(user, UserBean.class);
			users.add(userBean);
		}
		return users;
	}
	
}
