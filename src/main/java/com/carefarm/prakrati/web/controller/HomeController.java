package com.carefarm.prakrati.web.controller;

import java.util.Map;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.Company;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.service.HomeService;

@Controller
public class HomeController {

	private static final Logger LOGGER = LoggerFactory.getLogger(HomeController.class);
	
	private static final String HOME = "home";
	
	private static final String COMPANY_ADMIN = "CompanyAdmin";
	
	private static final String AGENT = "Agent";
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private HomeService homeService;
	
	@RequestMapping(value="/home", method=RequestMethod.GET)
	public String handleAfterAuthentication(Authentication authentication, ModelMap modelMap){
		LOGGER.info("Handling request authentication got complete");
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
    	httpSession.setAttribute("userId", user.getId());
    	httpSession.setAttribute("userType", user.getClass().getSimpleName());
    	if (user.getGroup().getName().equals(COMPANY_ADMIN) || 
    			user.getGroup().getName().equals(AGENT)) {
    		Company company = user.getCompany();
    		httpSession.setAttribute("companyId", company.getId());
    	}
		return HOME;
	}
	
	@RequestMapping(value="/welcome", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> welcomeDetails() {
		LOGGER.info("Finding the welcome details");
		return homeService.getApplicationDetails();
	}
	
}
