package com.carefarm.prakrati.web.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;

import com.carefarm.prakrati.accounts.service.AgentService;
import com.carefarm.prakrati.accounts.service.BrokerService;
import com.carefarm.prakrati.accounts.service.CompanyAdminService;
import com.carefarm.prakrati.accounts.service.CompanyService;
import com.carefarm.prakrati.accounts.service.CustomerService;
import com.carefarm.prakrati.accounts.service.EmployeeService;
import com.carefarm.prakrati.accounts.service.FarmerService;
import com.carefarm.prakrati.accounts.service.RetailerService;
import com.carefarm.prakrati.accounts.service.SuperAdminService;
import com.carefarm.prakrati.accounts.service.UserProfileService;
import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.OTPVerificationException;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.service.AddressService;
import com.carefarm.prakrati.service.DiscussionService;
import com.carefarm.prakrati.service.NoteService;
import com.carefarm.prakrati.service.NotificationService;
import com.carefarm.prakrati.service.ProfileService;
import com.carefarm.prakrati.service.UpcomingEventService;
import com.carefarm.prakrati.service.UserService;
import com.carefarm.prakrati.util.ImageUtils;
import com.carefarm.prakrati.vo.ImageVO;
import com.carefarm.prakrati.web.bean.AddressBean;
import com.carefarm.prakrati.web.bean.AddressEntityBean;
import com.carefarm.prakrati.web.bean.CompanyBean;
import com.carefarm.prakrati.web.bean.ImageBean;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.bean.OrderBean;
import com.carefarm.prakrati.web.bean.ReviewBean;
import com.carefarm.prakrati.web.model.AddressEntityForm;
import com.carefarm.prakrati.web.model.AddressForm;
import com.carefarm.prakrati.web.model.BankAccountForm;
import com.carefarm.prakrati.web.model.CreditCardForm;
import com.carefarm.prakrati.web.model.DiscussionForm;
import com.carefarm.prakrati.web.model.NoteForm;
import com.carefarm.prakrati.web.model.NotificationForm;
import com.carefarm.prakrati.web.model.UpcomingEventForm;
import com.carefarm.prakrati.web.model.VerifyMobileNoForm;

@Controller
@RequestMapping("/profile")
public class ProfileController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProfileController.class);
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private AddressService addressService;
	
	@Autowired
	private NoteService noteService;
	
	@Autowired
	private UpcomingEventService upcomingEventService;
	
	@Autowired
	private DiscussionService discussionService;
	
	@Autowired
	private NotificationService notificationService;
	
	@Autowired
	private CompanyService companyService;
	
	@Autowired
	private ProfileService profileService;
	
	@Autowired
	private FarmerService farmerService;
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private BrokerService brokerService;
	
	@Autowired
	private RetailerService retailerService;
	
	@Autowired
	private AgentService agentService;
	
	@Autowired
	private CompanyAdminService companyAdminService;
	
	@Autowired
	private SuperAdminService superAdminService;
	
	@Autowired
	private EmployeeService employeeService;
	
	@RequestMapping("/dashboard")
	public @ResponseBody Map<String, Object> dashboard(Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
		return profileService.getDashboardDetails(currentUser); 
	}
	
	@RequestMapping("/me")
	public @ResponseBody Map<String, Object> myProfile(
			@RequestParam("uniqueId") String uniqueId, Authentication authentication) {
		return getUserProfileService(authentication).getProfileDetails(uniqueId);
	}
	
	@RequestMapping("/company")
	public @ResponseBody CompanyBean companyProfile(@RequestParam("uniqueId") String uniqueId) {
		return companyService.findDetails(uniqueId, null);
	}
	
	@RequestMapping("/reviews")
	public @ResponseBody List<ReviewBean> showReviews(@RequestParam("uniqueId") String uniqueId) {
		return userService.findReviews(uniqueId);
	}
	
	@RequestMapping("/orders")
	public @ResponseBody List<OrderBean> showOrders(@RequestParam("uniqueId") String uniqueId) {
		return userService.findOrders(uniqueId);
	}
	
	@RequestMapping("/settings")
	public @ResponseBody Map<String, Object> settings(@RequestParam("uniqueId") String uniqueId) {
		return userService.findSettings(uniqueId);
	}
	
	@RequestMapping("/changePicture")
	public ResponseEntity<?> changePicture(@RequestPart("userId") String userId, 
			@RequestPart(value = "userImage") String imageDataUrl, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
    	
    	ImageVO image = ImageUtils.extractImage(imageDataUrl);
    	
    	try {
    		ImageBean defaultImageBean = userService.changeProfilePicture(image, userId, user);
    		return new ResponseEntity<ImageBean>(defaultImageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Got error while trying to upload picture", e);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to upload profile picture");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping("/changeLogo")
	public ResponseEntity<?> changeCompanyLogo(@RequestPart("uniqueId") String uniqueId, 
			@RequestPart(value = "companyLogo") String companyLogoDataUrl, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
    	
    	ImageVO companyLogo = ImageUtils.extractImage(companyLogoDataUrl);
    	
    	try {
    		ImageBean defaultImageBean = companyService.changeCompanyLogo(companyLogo, uniqueId, user);
    		return new ResponseEntity<ImageBean>(defaultImageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Got error while trying to upload company logo", e);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to upload company logo");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(value = "/addNote", method = { RequestMethod.POST })
	public ResponseEntity<?> addNote(@RequestParam("userNo") String userNo, @Valid @RequestBody NoteForm noteForm, BindingResult result, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		noteService.addNote(noteForm, userNo, currentUser);
    		MessageBean messageBean = new MessageBean("success", "Your address has been added successfully.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Got error while trying to upload company logo", e);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to add address.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/addUpcomingEvent", method = { RequestMethod.POST })
	public ResponseEntity<?> addUpcomingEvent(@RequestParam("userNo") String userNo, @Valid @RequestBody UpcomingEventForm upcomingEventForm, BindingResult result, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		upcomingEventService.addUpcomingEvent(upcomingEventForm, userNo, currentUser);
    		MessageBean messageBean = new MessageBean("success", "Upcoming Event has been added successfully.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Got error while trying to add upcoming event.", e);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to add upcoming event.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/addDiscussion", method = { RequestMethod.POST })
	public ResponseEntity<?> addDiscussion(@RequestParam("userNo") String userNo, @Valid @RequestBody DiscussionForm discussionForm, BindingResult result, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		discussionService.addDiscussion(discussionForm, userNo, currentUser);
    		MessageBean messageBean = new MessageBean("success", "Upcoming Event has been added successfully.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Got error while trying to add upcoming event.", e);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to add upcoming event.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/sendNotification", method = { RequestMethod.POST })
	public ResponseEntity<?> sendNotification(@RequestParam("userNo") String userNo, @Valid @RequestBody NotificationForm notificationForm, BindingResult result, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		notificationService.sendNotification(notificationForm, userNo, currentUser);
    		MessageBean messageBean = new MessageBean("success", "Notification has been sent successfully.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Got error while trying to send notification.", e);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to send notification.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/sendMobileVerificationCode", method = { RequestMethod.POST })
	public ResponseEntity<?> sendMobileVerificationCode(@RequestParam("userNo") String userNo, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		profileService.sendMobileVerificationCode(userNo, currentUser);
    		MessageBean messageBean = new MessageBean("success", "An OTP (One Time Password) has been sent on your mobile no. Please verify your mobile no. by entering OTP.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("An error occured while we were trying to send an OTP on your mobile no.", e);
			MessageBean messageBean = new MessageBean("error", "An error occured while we were trying to send an OTP on your mobile no.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/verifyOneTimePassword", method = { RequestMethod.POST })
	public ResponseEntity<?> verifyOneTimePassword(@Valid @RequestBody VerifyMobileNoForm verifyMobileNoForm, BindingResult result, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		profileService.verifyOneTimePassword(verifyMobileNoForm, currentUser);
    		MessageBean messageBean = new MessageBean("success", "Your mobile no. has been verified successfully. Thank you very much for mobile no. verification.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (OTPVerificationException exception) {
			LOGGER.error(exception.getMessage());
			MessageBean messageBean = new MessageBean("error", exception.getMessage());
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		} catch (ServiceException e) {
			LOGGER.error("An error occured while we were trying to verify your mobile no.", e);
			MessageBean messageBean = new MessageBean("error", "An error occured while we were trying to verify your mobile no.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/sendEmailVerificationLink", method = { RequestMethod.POST })
	public ResponseEntity<?> sendEmailVerificationLink(@RequestParam("userNo") String userNo, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		profileService.sendEmailVerificationLink(userNo, currentUser);
    		MessageBean messageBean = new MessageBean("success", "A verification link has been sent to you. Please verify your email address by following that link.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("An error occured while we were trying to send an email to you.", e);
			MessageBean messageBean = new MessageBean("error", "An error occured while we were trying to send an email to you.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/addAddress", method = { RequestMethod.POST })
	public ResponseEntity<?> addAddress(@RequestParam("userNo") String userNo, @Valid @RequestBody AddressForm addressForm, BindingResult result, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		addressService.addAddress(addressForm, userNo, currentUser);
    		MessageBean messageBean = new MessageBean("success", "Your address has been added successfully.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Got error while trying to upload company logo", e);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to add address.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/editAddress", method = { RequestMethod.POST })
	public ResponseEntity<?> editAddress(@RequestParam("userNo") String userNo, @Valid @RequestBody AddressBean addressBean, BindingResult result, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		addressService.editAddress(addressBean, userNo, currentUser);
    		MessageBean messageBean = new MessageBean("success", "Your address has been updated successfully.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Got error while trying to upload company logo", e);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to update address.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/addBillingAddress", method = { RequestMethod.POST })
	public ResponseEntity<?> addBillingAddress(@RequestParam("userNo") String userNo, @Valid @RequestBody AddressEntityForm addressForm, BindingResult result, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		addressService.addBillingAddress(addressForm, userNo, currentUser);
    		MessageBean messageBean = new MessageBean("success", "Your billing address has been added successfully.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Got error while trying to upload company logo", e);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to add billing address.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/addDeliveryAddress", method = { RequestMethod.POST })
	public ResponseEntity<?> addDeliveryAddress(@RequestParam("userNo") String userNo, @Valid @RequestBody AddressEntityForm addressForm, BindingResult result, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		addressService.addDeliveryAddress(addressForm, userNo, currentUser);
    		MessageBean messageBean = new MessageBean("success", "Your delivery address has been added successfully.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Got error while trying to upload company logo", e);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to add delivery address.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/editAddressEntity", method = { RequestMethod.POST })
	public ResponseEntity<?> editAddress(@RequestParam("userNo") String userNo, @Valid @RequestBody AddressEntityBean addressBean, BindingResult result, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		addressService.editAddress(addressBean, userNo, currentUser);
    		MessageBean messageBean = new MessageBean("success", "Your address has been modified successfully.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Got error while trying to modify delivery address.", e);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to modify delivery address.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(value = "/setDefaultAddress", method = { RequestMethod.POST })
	public ResponseEntity<?> setDefaultAddress(@RequestParam("id") Long id, @RequestParam("category") String category, 
			@RequestParam("userNo") String userNo, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		addressService.setDefaultAddress(id, category, userNo, currentUser);
    		MessageBean messageBean = new MessageBean("success", "Your address has been modified successfully.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Got error while trying to modify your address.", e);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to modify your address.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/addCreditCard", method = { RequestMethod.POST })
	public ResponseEntity<?> addCreditCard(@RequestParam("userNo") String userNo, @Valid @RequestBody CreditCardForm creditCardForm, BindingResult result, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		userService.addCreditCard(creditCardForm, userNo, currentUser);
    		MessageBean messageBean = new MessageBean("success", "Your billing details has been added successfully.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Got error while trying to upload company logo", e);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to add billing details.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/addBankAccount", method = { RequestMethod.POST })
	public ResponseEntity<?> addBankAccount(@RequestParam("userNo") String userNo, @Valid @RequestBody BankAccountForm bankAccountForm, BindingResult result, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		userService.addBankAccount(bankAccountForm, userNo, currentUser);
    		MessageBean messageBean = new MessageBean("success", "Your billing details has been added successfully.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Got error while trying to upload company logo", e);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to add billing details.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	public UserProfileService getUserProfileService(Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
		if (currentUser.hasRole("ROLE_FARMER")) {
			return farmerService;
		} else if (currentUser.hasRole("ROLE_CUSTOMER")) {
			return customerService;
		} else if (currentUser.hasRole("ROLE_BROKER")) {
			return brokerService;
		} else if (currentUser.hasRole("ROLE_RETAILER")) {
			return retailerService;
		} else if (currentUser.hasRole("ROLE_AGENT")) {
			return agentService;
		} else if (currentUser.hasRole("ROLE_COMPANY_ADMIN")) {
			return companyAdminService;
		} else if (currentUser.hasRole("ROLE_ADMIN")) {
			return superAdminService;
		} else {
			return employeeService;
		}
	}
}
