package com.carefarm.prakrati.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.carefarm.prakrati.service.PermitService;
import com.carefarm.prakrati.web.bean.PermitBean;

@Controller
@RequestMapping("/permits")
public class PermitController {

	@Autowired
	private PermitService permitService;
	
	@RequestMapping("/all")
	public @ResponseBody List<PermitBean> allPermits() {
		return permitService.findAll();
	}
	
}
