package com.carefarm.prakrati.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.carefarm.prakrati.service.CurrencyService;
import com.carefarm.prakrati.web.bean.CurrencyBean;

@Controller
@RequestMapping("/currencies")
public class CurrencyController {
	
	@Autowired
	private CurrencyService currencyService;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody List<CurrencyBean> showAllCurrencies(HttpServletRequest request) {
		return currencyService.findAll();
	}
}
