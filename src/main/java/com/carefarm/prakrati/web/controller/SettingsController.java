package com.carefarm.prakrati.web.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.service.UserService;
import com.carefarm.prakrati.web.bean.UserBean;

@Controller
@RequestMapping("/setting")
public class SettingsController {

    private static final String MY_ACCOUNT = "setting/account";
    
    private static final String MY_PLAN = "setting/plan";
    
    private static final String MY_PAYMENT_METHOD = "setting/payment_method";
    
    private static final String PASSWD_MASK = "*********";

    @Autowired
    private UserService userService;
    
    @RequestMapping(value="/country", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> userSettings(Authentication authentication){
    	DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
    	return userService.findCountrySettings(user.getUserNo());
	}
    
    @RequestMapping(value="/account", method=RequestMethod.GET)
	public String myAccount(Authentication authentication, ModelMap modelMap){
    	DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
    	String name = user.getFirstName() + " " + user.getLastName();
    	UserBean userBean = new UserBean(name, user.getEmail(), 
    			PASSWD_MASK, user.getMobileNo());
    	modelMap.put("user", userBean);
		return MY_ACCOUNT;
	}
    
    @RequestMapping(value="/plan", method=RequestMethod.GET)
	public String myPlan(Authentication authentication, ModelMap modelMap){
    	DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
    	String name = user.getFirstName() + " " + user.getLastName();
    	UserBean userBean = new UserBean(name, user.getEmail(), 
    			PASSWD_MASK, user.getMobileNo());
    	modelMap.put("user", userBean);
		return MY_PLAN;
	}
    
    @RequestMapping(value="/paymentMethod", method=RequestMethod.GET)
	public String myPaymentMethod(Authentication authentication, ModelMap modelMap){
    	DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
    	String name = user.getFirstName() + " " + user.getLastName();
    	UserBean userBean = new UserBean(name, user.getEmail(), 
    			PASSWD_MASK, user.getMobileNo());
    	modelMap.put("user", userBean);
		return MY_PAYMENT_METHOD;
	}
    
}
