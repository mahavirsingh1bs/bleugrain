package com.carefarm.prakrati.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.service.WishlistService;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.bean.WishlistBean;

@Controller
@RequestMapping("/wishlist")
public class WishlistController {
	private static final Logger LOGGER = LoggerFactory.getLogger(WishlistController.class);
	
	@Autowired
	private WishlistService wishlistService;
	
	/*
	@RequestMapping(value="/buyer/user", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showBuyerUserWishlist(HttpServletRequest request) {
		LOGGER.info("Finding the buyer wishlist");
		Integer draw = Integer.parseInt(request.getParameter("draw"));
		Integer length = Integer.valueOf(request.getParameter("length"));
		String keyword = request.getParameter("search[value]");
		
		Integer page = 0;
    	if (null != request.getParameter("start")) {
    		page = (Integer.valueOf(request.getParameter("start")) / length);
    	}
    	
    	Long userId = null;
    	if (httpSession.getAttribute("userId") != null) {
    		userId = (Long )httpSession.getAttribute("userId");
    	}
    	
    	PageRequest pageRequest = new PageRequest(page, length);
    	Map<String, Object> params = new HashMap<>();
    	params.put("draw", draw);
    	params.put("length", length);
    	params.put("userId", userId);
		return buyerWishlistService.findByKeyword(keyword, pageRequest, params);
	}
	
	@RequestMapping(value="/buyer/company", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showBuyerCompanyWishlist(HttpServletRequest request) {
		Integer draw = Integer.parseInt(request.getParameter("draw"));
		Integer length = Integer.valueOf(request.getParameter("length"));
		String keyword = request.getParameter("search[value]");
		
		Integer page = 0;
    	if (null != request.getParameter("start")) {
    		page = (Integer.valueOf(request.getParameter("start")) / length);
    	}
    	
    	Long companyId = null;
    	if (httpSession.getAttribute("companyId") != null) {
    		companyId = (Long )httpSession.getAttribute("companyId");
    	}
    	
    	PageRequest pageRequest = new PageRequest(page, length);
    	Map<String, Object> params = new HashMap<>();
    	params.put("draw", draw);
    	params.put("length", length);
    	params.put("companyId", companyId);
		return buyerWishlistService.findByKeyword(keyword, pageRequest, params);
	}
	
	@RequestMapping(value="/seller/user", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showSellerUserWishlist(HttpServletRequest request) {
		Integer draw = Integer.parseInt(request.getParameter("draw"));
		Integer length = Integer.valueOf(request.getParameter("length"));
		String keyword = request.getParameter("search[value]");
		
		Integer page = 0;
    	if (null != request.getParameter("start")) {
    		page = (Integer.valueOf(request.getParameter("start")) / length);
    	}
    	
    	Long userId = null;
    	if (httpSession.getAttribute("userId") != null) {
    		userId = (Long )httpSession.getAttribute("userId");
    	}
    	
    	PageRequest pageRequest = new PageRequest(page, length);
    	Map<String, Object> params = new HashMap<>();
    	params.put("draw", draw);
    	params.put("length", length);
    	params.put("userId", userId);
		return sellerWishlistService.findByKeyword(keyword, pageRequest, params);
	}
	
	@RequestMapping(value="/seller/company", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showSellerCompanyWishlist(HttpServletRequest request) {
		Integer draw = Integer.parseInt(request.getParameter("draw"));
		Integer length = Integer.valueOf(request.getParameter("length"));
		String keyword = request.getParameter("search[value]");
		
		Integer page = 0;
    	if (null != request.getParameter("start")) {
    		page = (Integer.valueOf(request.getParameter("start")) / length);
    	}
    	
    	Long companyId = null;
    	if (httpSession.getAttribute("companyId") != null) {
    		companyId = (Long )httpSession.getAttribute("companyId");
    	}
    	
    	PageRequest pageRequest = new PageRequest(page, length);
    	Map<String, Object> params = new HashMap<>();
    	params.put("draw", draw);
    	params.put("length", length);
    	params.put("companyId", companyId);
		return sellerWishlistService.findByKeyword(keyword, pageRequest, params);
	}
	
	@RequestMapping(value="/products/{buyerId}", method=RequestMethod.GET)
	public @ResponseBody Set<ProductBean> getWishlistOfProducts(@PathVariable("buyerId") long buyerId) {
		Set<ProductBean> buyingWishlist = new HashSet<>();
		for (BuyerWishlist buyerWishlist : buyerWishlistRepository.findWishlistByBuyerId(buyerId)) {
			Product product = buyerWishlist.getProduct();
			User owner = product.getOwner();
			ProductBean productBean = mapper.map(product, ProductBean.class);
			productBean.setOwnerType(CommonUtil.getType(owner));
			buyingWishlist.add(productBean);
		}
		return buyingWishlist;
	}
	
	@RequestMapping(value="/demands/{sellerId}", method=RequestMethod.GET)
	public @ResponseBody Set<DemandBean> getWishlistOfDemands(@PathVariable("sellerId") long sellerId) {
		Set<DemandBean> sellingWishlist = new HashSet<>();
		for (SellerWishlist sellerWishlist : sellerWishlistRepository.findWishlistBySellerId(sellerId)) {
			Demand demand = sellerWishlist.getDemand();
			if (demand.getCompany() != null) {
				Agent agent = (Agent )demand.getUser();
				Company company = demand.getCompany();
				DemandBean demandBean = mapper.map(demand, DemandBean.class);
				demandBean.setCustType(CommonUtil.getCustomerType(agent));
				demandBean.setCompany(mapper.map(company, CompanyBean.class));
				sellingWishlist.add(demandBean);
			} else {
				User customer = (User )demand.getUser();
				DemandBean demandBean = mapper.map(demand, DemandBean.class);
				demandBean.setCustType(CommonUtil.getCustomerType(customer));
				sellingWishlist.add(demandBean);
			}
		}
		return sellingWishlist;
	}
	*/
	
	@RequestMapping(value="/", method = RequestMethod.GET)
	public @ResponseBody WishlistBean findWishlist(Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		WishlistBean wishlist = wishlistService.findWishlist(user);
		return wishlist;
	}
	
	@RequestMapping(value="/addProduct", method = RequestMethod.POST)
	public @ResponseBody MessageBean addProductToWishlist(@RequestBody String uniqueId, Authentication authentication) {
		LOGGER.info("START: Adding product " + uniqueId + " to the wishlist");
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		wishlistService.addProductToWishlist(uniqueId, user);
		LOGGER.info("END: Added product " + uniqueId + " to the wishlist");
		return new MessageBean("success", "Item has been added successfully added to the wishlist");
	}
	
	@RequestMapping(value="/addDemand", method = RequestMethod.POST)
	public @ResponseBody MessageBean addDemandToWishlist(@RequestBody String uniqueId, Authentication authentication) {
		LOGGER.info("START: Adding demand " + uniqueId + " to the wishlist");
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		wishlistService.addDemandToWishlist(uniqueId, user);
		LOGGER.info("END: Added demand " + uniqueId + " to the wishlist");
		return new MessageBean("success", "Item has been added successfully added to the wishlist");
	}
	
	@RequestMapping(value="/removeProduct", method = RequestMethod.POST)
	public @ResponseBody MessageBean removeProductFromWishlist(@RequestBody String uniqueId, Authentication authentication) {
		LOGGER.info("START: Adding product " + uniqueId + " to the wishlist");
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		wishlistService.removeProductFromWishlist(uniqueId, user);
		LOGGER.info("END: Added product " + uniqueId + " to the wishlist");
		return new MessageBean("success", "Item has been deleted successfully from the wishlist");
	}
	
	@RequestMapping(value="/removeDemand", method = RequestMethod.POST)
	public @ResponseBody MessageBean removeDemandFromWishlist(@RequestBody String uniqueId, Authentication authentication) {
		LOGGER.info("START: Adding demand " + uniqueId + " to the wishlist");
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		wishlistService.removeDemandFromWishlist(uniqueId, user);
		LOGGER.info("END: Added demand " + uniqueId + " to the wishlist");
		return new MessageBean("success", "Item has been deleted successfully from the wishlist");
	}
}
