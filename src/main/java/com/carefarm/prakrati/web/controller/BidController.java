package com.carefarm.prakrati.web.controller;

import java.security.Principal;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.service.BidService;
import com.carefarm.prakrati.validators.BidValidator;
import com.carefarm.prakrati.web.bean.BidBean;
import com.carefarm.prakrati.web.bean.BidsRequest;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.model.BidForm;

@Controller
@RequestMapping("/bids")
public class BidController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BidController.class);
	
	@Autowired
	private BidService bidService;
	
	@Autowired
    private SimpMessagingTemplate messageTemplate;
	
	@Autowired
	private BidValidator bidValidator;

	@RequestMapping(value="/product", method=RequestMethod.GET)
	public @ResponseBody List<BidBean> showProductBids(BidsRequest bidsRequest) {
		return bidService.findByUniqueId(bidsRequest);
	}

	@RequestMapping(value="/user", method=RequestMethod.GET)
	public @ResponseBody List<BidBean> showUserBids(BidsRequest bidsRequest) {
		return bidService.findByUser(bidsRequest);
	}
	
	@RequestMapping(value="/company", method=RequestMethod.GET)
	public @ResponseBody List<BidBean> showCompanyBids(BidsRequest bidsRequest) {
		return bidService.findByCompany(bidsRequest);
	}
	
	@RequestMapping(value="/place", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> place(@Valid @RequestBody BidForm form, 
			BindingResult result, Authentication authentication) {
		bidValidator.validate(form, result);
		if(result.hasErrors()) {
            MessageBean messageBean = new MessageBean("error", "Form has validation errors", result.getFieldErrors(), result.getGlobalErrors());
            return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
        }
		
        DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
		try {
			bidService.place(form, currentUser);
			MessageBean messageBean = new MessageBean("success", "Bid has been placed successfully");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
		} catch (ServiceException exception) {
			LOGGER.error("Got error while placing a bid", exception);
			MessageBean messageBean = new MessageBean("error", "Your bid cannot be completed at the moment");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@MessageMapping("/bid/joins")
	public void showExistingBids(Message<Object> message) {
		// get the user associated with the message
        Principal user = message.getHeaders().get(SimpMessageHeaderAccessor.USER_HEADER, Principal.class);
        // notify all users that a user has joined the topic
        messageTemplate.convertAndSend("/topic/users", user.getName());
	}
	
}
