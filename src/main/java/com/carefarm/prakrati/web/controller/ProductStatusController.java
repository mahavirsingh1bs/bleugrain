package com.carefarm.prakrati.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.carefarm.prakrati.service.ProductStatusService;
import com.carefarm.prakrati.web.bean.ProductStatusBean;
import com.carefarm.prakrati.web.bean.SearchRequest;

@Controller
@RequestMapping("/productStatuses")
public class ProductStatusController {

	@Autowired
	private ProductStatusService productStatusService;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showProductStatuss(SearchRequest searchRequest) {
		Map<String, Object> data = new HashMap<>();
		List<ProductStatusBean> productStatuses = productStatusService.findAll();
		data.put("data", productStatuses);
		return data;
	}
}
