package com.carefarm.prakrati.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.carefarm.prakrati.service.OrderStatusService;
import com.carefarm.prakrati.web.bean.OrderStatusBean;
import com.carefarm.prakrati.web.bean.SearchRequest;

@Controller
@RequestMapping("/orderStatuses")
public class OrderStatusController {
	
	@Autowired
	private OrderStatusService orderStatusService;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showOrderStatuses(SearchRequest searchRequest) {
		Map<String, Object> data = new HashMap<>();
		List<OrderStatusBean> orderStatuses = orderStatusService.findAll();
		data.put("data", orderStatuses);
		return data;
	}

}
