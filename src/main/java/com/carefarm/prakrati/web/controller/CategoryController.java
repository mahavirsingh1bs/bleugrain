package com.carefarm.prakrati.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.service.CategoryService;
import com.carefarm.prakrati.web.bean.CategoryBean;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.CategoryForm;

@Controller
@RequestMapping("/categories")
public class CategoryController {

	private static final Logger LOGGER = LoggerFactory.getLogger(CategoryController.class);
    
    @Autowired
    private CategoryService categoryService;
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public @ResponseBody List<CategoryBean> getCategories() {
    	LOGGER.info("Finding the main categories");
    	return categoryService.findMainCategories();
    }
    
    @RequestMapping(value = "/allCategories", method = RequestMethod.GET)
    public @ResponseBody List<CategoryBean> allCategories() {
    	LOGGER.info("Finding the all categories");
    	return categoryService.findAll();
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
	public @ResponseBody List<CategoryBean> findTags(@RequestParam("keywords") String keywords) {
		return categoryService.findByMatchingKeywords(keywords);
	}
    
    @RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showCategories(SearchRequest searchRequest) {
    	LOGGER.info("Finding the all categories");
    	if (searchRequest.getPageSize() > 0) {
	    	return categoryService.findByKeyword(searchRequest);
    	} else {
    		Map<String, Object> data = new HashMap<>();
    		List<CategoryBean> categories = categoryService.findAll();
    		data.put("categories", categories);
    		return data;
    	}
	}

    @RequestMapping(value="/new", method=RequestMethod.POST)
	public @ResponseBody MessageBean newCategory(@Valid @RequestBody CategoryForm form, BindingResult result, 
			Authentication authentication, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
            return new MessageBean("failure", "Form has validation errors.");
        }
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
			categoryService.create(form, user);
			redirectAttributes.addFlashAttribute("message", "Category has been added successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got a server error. So category cannot be added now.", e);
			return new MessageBean("error", "Got server error while adding category.");
		}
        
        return new MessageBean("success", "Category has been added successfully");
	}
    
    @RequestMapping(value="/data/{categoryId}", method=RequestMethod.GET)
	public @ResponseBody CategoryBean getCategoryData(@PathVariable("categoryId") long categoryId, ModelMap modelMap){
		return categoryService.findDetails(categoryId, null);
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public @ResponseBody MessageBean update(@Valid @RequestBody CategoryBean categoryBean, BindingResult result, 
			RedirectAttributes redirectAttributes, Authentication authentication) {
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
			categoryService.update(categoryBean, user);
			redirectAttributes.addFlashAttribute("message", "Category has been updated successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while creating category", e);
			redirectAttributes.addFlashAttribute("message", "Got error while updating category");
			return new MessageBean("error", "Got error while updating category");
		}
        
        return new MessageBean("success", "Category has been updated successfully.");
	}
    
    @RequestMapping(value = "/delete/{uniqueId}", method = RequestMethod.DELETE)
	public @ResponseBody MessageBean delete(@PathVariable("uniqueId") String uniqueId) {
    	LOGGER.info("Deleting the category with id " + uniqueId);
		categoryService.delete(uniqueId);
		return new MessageBean("success", "Category has been deleted successfully");
	}
}
