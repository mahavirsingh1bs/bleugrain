package com.carefarm.prakrati.web.controller;

import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.service.PaymentMethodService;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.bean.PaymentMethodBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.PaymentMethodForm;

@Controller
@RequestMapping("/paymentMethods")
public class PaymentMethodController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentMethodController.class);
	
	@Autowired
	private PaymentMethodService paymentMethodService;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showPaymentMethods(SearchRequest searchRequest) {
		LOGGER.info("Finding all payment methods");
		return paymentMethodService.findByKeyword(searchRequest);
	}

	@RequestMapping(value="/new", method=RequestMethod.POST)
	public @ResponseBody MessageBean newPaymentMethod(@Valid @RequestBody PaymentMethodForm form, BindingResult result, 
			Authentication authentication, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
            return new MessageBean("failure", "Form has validation errors.");
        }
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
			paymentMethodService.create(form, user);
			redirectAttributes.addFlashAttribute("message", "Payment Method has been added successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got a server error. So paymentMethod cannot be added now.", e);
			return new MessageBean("error", "Got server error while adding payment method.");
		}
        
        return new MessageBean("success", "Payment Method has been added successfully");
	}
	
	@RequestMapping(value="/data/{paymentMethodId}", method=RequestMethod.GET)
	public @ResponseBody PaymentMethodBean getPaymentMethodData(@PathVariable("paymentMethodId") long paymentMethodId, ModelMap modelMap){
		return paymentMethodService.findDetails(paymentMethodId, null);
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public @ResponseBody MessageBean update(@Valid @RequestBody PaymentMethodBean paymentMethodBean, BindingResult result, 
			RedirectAttributes redirectAttributes, Authentication authentication) {
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
			paymentMethodService.update(paymentMethodBean, user);
			redirectAttributes.addFlashAttribute("message", "PaymentMethod has been updated successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while creating paymentMethod", e);
			redirectAttributes.addFlashAttribute("message", "Got error while updating paymentMethod");
			return new MessageBean("error", "Got error while updating paymentMethod");
		}
        
        return new MessageBean("success", "PaymentMethod has been updated successfully.");
	}
	
	@RequestMapping(value = "/delete/{uniqueId}", method = RequestMethod.DELETE)
	public @ResponseBody MessageBean delete(@PathVariable("uniqueId") String uniqueId) {
		paymentMethodService.delete(uniqueId);
		return new MessageBean("success", "Payment Method has been deleted successfully");
	}
}
