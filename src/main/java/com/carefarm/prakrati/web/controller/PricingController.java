package com.carefarm.prakrati.web.controller;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.carefarm.prakrati.service.PricingService;
import com.carefarm.prakrati.web.bean.SearchRequest;

@Controller
@RequestMapping("/pricing")
public class PricingController {
	
	@Autowired
	private PricingService pricingService;
	
	@RequestMapping(value="/products", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> findProducts(SearchRequest searchRequest) {
		return pricingService.findProducts(searchRequest);
	}
	
	@RequestMapping(value="/demands", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> findDemands(SearchRequest searchRequest) {
		return pricingService.findDemands(searchRequest);
	}
	
}
