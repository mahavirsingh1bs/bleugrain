package com.carefarm.prakrati.web.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.service.ExpertiseService;
import com.carefarm.prakrati.web.bean.ExpertiseBean;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.model.ExpertiseForm;

@Controller
@RequestMapping("/expertises")
public class ExpertiseController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ExpertiseController.class);
	
	@Autowired
	private ExpertiseService expertiseService;
	
	@RequestMapping(value = "/add", method = { RequestMethod.POST })
	public ResponseEntity<?> add(@RequestParam("userNo") String userNo, @Valid @RequestBody ExpertiseForm expertiseForm, 
			BindingResult result, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		expertiseService.addExpertise(expertiseForm, userNo, currentUser);
    		MessageBean messageBean = new MessageBean("success", "Your expertise has been added successfully.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Got error while trying to add expertise", e);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to add expertise.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/get", method = { RequestMethod.GET })
	public ResponseEntity<?> get(@RequestParam("uniqueId") String uniqueId, Authentication authentication) {
		try {
			ExpertiseBean expertiseBean = expertiseService.find(uniqueId);
    		return new ResponseEntity<ExpertiseBean>(expertiseBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Not able to find the specified expertise.", e);
			MessageBean messageBean = new MessageBean("error", "Not able to find the specified expertise.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/update", method = { RequestMethod.PUT })
	public ResponseEntity<?> update(@Valid @RequestBody ExpertiseBean expertiseBean, 
			BindingResult result, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		expertiseService.update(expertiseBean, currentUser);
    		MessageBean messageBean = new MessageBean("success", "Your expertise has been updated successfully.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Got error while trying to update expertise", e);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to update expertise.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/delete", method = { RequestMethod.DELETE })
	public ResponseEntity<?> add(@RequestParam("uniqueId") String uniqueId, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	try {
    		expertiseService.remove(uniqueId, currentUser);
    		MessageBean messageBean = new MessageBean("success", "Expertise has been deleted successfully.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Got error while trying to delete expertise", e);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to delete expertise.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
}
