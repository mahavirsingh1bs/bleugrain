package com.carefarm.prakrati.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.service.GroupService;
import com.carefarm.prakrati.web.bean.GroupBean;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.GroupForm;

@Controller
@RequestMapping("/groups")
public class GroupController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(GroupController.class);
	
	@Autowired
	private GroupService groupService;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showGroups(SearchRequest searchRequest) {
		LOGGER.info("Finding all the groups");
		if (searchRequest.getPageSize() > 0) {
	    	return groupService.findByKeyword(searchRequest);
    	} else {
    		Map<String, Object> data = new HashMap<>();
    		List<GroupBean> groups = groupService.findAll();
    		data.put("groups", groups);
    		return data;
    	}
	}

	@RequestMapping(value="/new", method=RequestMethod.POST)
	public @ResponseBody MessageBean newGroup(@Valid @RequestBody GroupForm form, BindingResult result, 
			Authentication authentication, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
            return new MessageBean("failure", "Form has validation errors.");
        }
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
        	groupService.create(form, user);
			redirectAttributes.addFlashAttribute("message", "Group has been added successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got a server error. So group cannot be added now.", e);
			return new MessageBean("error", "Got server error while adding group.");
		}
        
        return new MessageBean("success", "Group has been added successfully");
	}

	@RequestMapping(value="/data/{groupId}", method=RequestMethod.GET)
	public @ResponseBody GroupBean getGroupData(@PathVariable("groupId") long groupId, ModelMap modelMap){
		return groupService.findDetails(groupId, null);
	}

	@RequestMapping(value="/update", method=RequestMethod.POST)
	public @ResponseBody MessageBean update(@Valid @RequestBody GroupBean groupBean, BindingResult result, 
			RedirectAttributes redirectAttributes, Authentication authentication) {
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
        	groupService.update(groupBean, user);
			redirectAttributes.addFlashAttribute("message", "Group has been updated successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while updating group", e);
			redirectAttributes.addFlashAttribute("message", "Got error while updating group");
			return new MessageBean("error", "Got error while updating group");
		}
        
        return new MessageBean("success", "Group has been updated successfully.");
	}
	
	@RequestMapping(value = "/delete/{uniqueId}", method = RequestMethod.DELETE)
	public @ResponseBody MessageBean delete(@PathVariable("uniqueId") String uniqueId) {
		LOGGER.info("Deleting the group with id " + uniqueId);
		groupService.delete(uniqueId);
		return new MessageBean("success", "Group has been deleted successfully");
	}
	
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public @ResponseBody List<GroupBean> findGroups(@RequestParam("keywords") String keywords) {
		return groupService.findByMatchingKeywords(keywords);
	}
}
