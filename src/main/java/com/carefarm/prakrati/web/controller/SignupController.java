package com.carefarm.prakrati.web.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.integration.support.json.Jackson2JsonObjectMapper;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.carefarm.prakrati.accounts.service.BrokerService;
import com.carefarm.prakrati.accounts.service.CompanyService;
import com.carefarm.prakrati.accounts.service.CustomerService;
import com.carefarm.prakrati.accounts.service.FarmerService;
import com.carefarm.prakrati.accounts.service.RetailerService;
import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.service.UserService;
import com.carefarm.prakrati.util.UserType;
import com.carefarm.prakrati.validators.BrokerValidator;
import com.carefarm.prakrati.validators.CompanyValidator;
import com.carefarm.prakrati.validators.RuralUserValidator;
import com.carefarm.prakrati.validators.UrbanUserValidator;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.model.RegisterForm;
import com.carefarm.prakrati.web.model.VerifyContactNoForm;

@Controller
@RequestMapping("/register")
public class SignupController extends BaseController {
	private static final Logger LOGGER = LoggerFactory.getLogger(AccountController.class);

	@Autowired
	private UserService userService;
	
	@Autowired
	private FarmerService farmerService;

	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private BrokerService brokerService;
	
	@Autowired
	private RetailerService retailerService;
	
	@Autowired
	private CompanyService companyService;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	
	@Autowired
	private AuthenticationSuccessHandler authenticationSuccessHandler;
	
	@Autowired
	private RuralUserValidator ruralUserValidator;

	@Autowired
	private UrbanUserValidator urbanUserValidator;
	
	@Autowired
	private BrokerValidator brokerValidator;
	
	@Autowired
	private CompanyValidator companyValidator;
	
	@RequestMapping(value="/farmer", method=RequestMethod.POST)
	public void registerFarmer(@RequestBody RegisterForm form, BindingResult result, 
			RedirectAttributes redirectAttributes, HttpServletRequest httpServletRequest, 
			HttpServletResponse httpServletResponse) {
		Jackson2JsonObjectMapper jsonMapper = new Jackson2JsonObjectMapper();
		
		ruralUserValidator.validate(form, result);
		if(result.hasErrors()) {
			MessageBean messageBean = new MessageBean("error", "Form has validation errors", result.getFieldErrors(), result.getGlobalErrors());
			this.writerErrorsToResponse(httpServletResponse, messageBean, jsonMapper);
			return;
        }
		
        try {
        	form.setUserType(UserType.FARMER);
			farmerService.register(form);
			if (Predicates.notNull.test(form.getEmailAddress())) {
				this.authenticateUserAndSetSession(form.getEmailAddress(), form.getPassword(), 
						null, httpServletRequest, httpServletResponse);
			} else {
				this.authenticateUserAndSetSession(form.getMobileNo(), form.getPassword(), 
						null, httpServletRequest, httpServletResponse);
			}
			redirectAttributes.addFlashAttribute("message", "Your registeration is successful. Thank you for choosing us.");
		} catch (ServiceException e) {
			LOGGER.error("Got an error while doing your registering.", e);
			MessageBean messageBean = new MessageBean("error", "Got an error while doing your registering.");
            writerErrorsToResponse(httpServletResponse, messageBean, jsonMapper);
            return;
		}
	}
	
	@RequestMapping(value="/customer", method=RequestMethod.POST)
	public void registerCustomer(@RequestBody RegisterForm form, BindingResult result, 
			RedirectAttributes redirectAttributes, HttpServletRequest request, 
			HttpServletResponse response) {
		Jackson2JsonObjectMapper jsonMapper = new Jackson2JsonObjectMapper();
		
		urbanUserValidator.validate(form, result);
		if(result.hasErrors()) {
			MessageBean messageBean = new MessageBean("error", "Form has validation errors", result.getFieldErrors(), result.getGlobalErrors());
			this.writerErrorsToResponse(response, messageBean, jsonMapper);
			return;
        }
		
        try {
        	form.setUserType(UserType.CUSTOMER);
			customerService.register(form);
			if (Predicates.notNull.test(form.getEmailAddress())) {
				this.authenticateUserAndSetSession(form.getEmailAddress(), form.getPassword(), 
						null, request, response);
			} else {
				this.authenticateUserAndSetSession(form.getMobileNo(), form.getPassword(), 
						null, request, response);
			}
			redirectAttributes.addFlashAttribute("message", "Your registeration is successful. Thank you for choosing us.");
		} catch (ServiceException e) {
			LOGGER.error("Got an error while doing your registering.", e);
			MessageBean messageBean = new MessageBean("error", "Got an error while doing your registering.");
            writerErrorsToResponse(response, messageBean, jsonMapper);
            return;
		}
	}
	
	@RequestMapping(value="/broker", method=RequestMethod.POST, consumes = { "application/json" })
	public void registerBroker(@RequestBody RegisterForm form, BindingResult result, 
			RedirectAttributes redirectAttributes, HttpServletRequest httpServletRequest, 
			HttpServletResponse httpServletResponse) {
		Jackson2JsonObjectMapper jsonMapper = new Jackson2JsonObjectMapper();
		
		urbanUserValidator.validate(form, result);
		if(result.hasErrors()) {
			MessageBean messageBean = new MessageBean("error", "Form has validation errors", result.getFieldErrors(), result.getGlobalErrors());
			writerErrorsToResponse(httpServletResponse, messageBean, jsonMapper);
			return;
        }
		
        try {
        	form.setUserType(UserType.BROKER);
        	brokerService.register(form);
        	if (Predicates.notNull.test(form.getEmailAddress())) {
				this.authenticateUserAndSetSession(form.getEmailAddress(), form.getPassword(), 
						null, httpServletRequest, httpServletResponse);
			} else {
				this.authenticateUserAndSetSession(form.getPhoneNo(), form.getPassword(), 
						null, httpServletRequest, httpServletResponse);
			}
			redirectAttributes.addFlashAttribute("message", "Your registeration is successful. Thank you for choosing us.");
		} catch (ServiceException e) {
			LOGGER.error("Got an error while doing your registering.", e);
			MessageBean messageBean = new MessageBean("error", "Got an error while doing your registering.");
			writerErrorsToResponse(httpServletResponse, messageBean, jsonMapper);
			return;
		}
	}

	private void writerErrorsToResponse(HttpServletResponse httpServletResponse, MessageBean messageBean,
			Jackson2JsonObjectMapper jsonMapper) {
		try {
			httpServletResponse.getWriter().write(jsonMapper.toJson(messageBean));
		} catch (Exception e) {
			LOGGER.error("Got an error when trying to write message bean to response: " + e.getMessage(), e.getCause());
		}
	}
	
	@RequestMapping(value="/retailer", method=RequestMethod.POST, consumes = { "application/json" })
	public void registerRetailer(@RequestBody RegisterForm form, BindingResult result, 
			RedirectAttributes redirectAttributes, HttpServletRequest httpServletRequest, 
			HttpServletResponse httpServletResponse) {
		Jackson2JsonObjectMapper jsonMapper = new Jackson2JsonObjectMapper();
		
		urbanUserValidator.validate(form, result);
		if(result.hasErrors()) {
			MessageBean messageBean = new MessageBean("error", "Form has validation errors", result.getFieldErrors(), result.getGlobalErrors());
			writerErrorsToResponse(httpServletResponse, messageBean, jsonMapper);
			return;
        }
        
        try {
        	form.setUserType(UserType.RETAILER);
			retailerService.register(form);
			if (Predicates.notNull.test(form.getEmailAddress())) {
				this.authenticateUserAndSetSession(form.getEmailAddress(), form.getPassword(), 
						null, httpServletRequest, httpServletResponse);
			} else {
				this.authenticateUserAndSetSession(form.getPhoneNo(), form.getPassword(), 
						null, httpServletRequest, httpServletResponse);
			}
			redirectAttributes.addFlashAttribute("message", "Your registeration is successful. Thank you for choosing us.");
		} catch (ServiceException e) {
			LOGGER.error("Got an error while doing your registering.", e);
			MessageBean messageBean = new MessageBean("error", "Got an error while doing your registering.", result.getFieldErrors(), result.getGlobalErrors());
            writerErrorsToResponse(httpServletResponse, messageBean, jsonMapper);
            return;
		}
	}
	
	@RequestMapping(value="/company", method=RequestMethod.POST, consumes = { "application/json" })
	public @ResponseBody MessageBean registerCompany(@RequestBody RegisterForm form, BindingResult result, RedirectAttributes redirectAttributes) {
		
		companyValidator.validate(form, result);
		if(result.hasErrors()) {
            return new MessageBean("error", "Form has validation errors", result.getFieldErrors(), result.getGlobalErrors());
        }

		try {
			companyService.register(form);
			redirectAttributes.addFlashAttribute("message", "Your registeration is successful. Thank you for choosing us.");
			return new MessageBean("success", "Your registeration is successful. Thank you for choosing us.");
		} catch (ServiceException e) {
			LOGGER.error("Got an error while doing your registering.", e);
			redirectAttributes.addFlashAttribute("message", "Got an error while doing your registering.");
			return new MessageBean("error", "Got an error while doing your registering.");
		}
	}
	
	@RequestMapping(value="/verifyContactNo", method=RequestMethod.PUT, consumes = { "application/json" })
	public @ResponseBody MessageBean verifyContactNo(@RequestBody VerifyContactNoForm form) {
		if (StringUtils.isEmpty(form.getOneTimePasswd())) {
			return new MessageBean("error", "One-Time Password is Blank.");
		}
		
		if (userService.verifyContactNo(form.getUniqueCode(), form.getOneTimePasswd())) {
			return new MessageBean("success", "You have successfully verified your Contact No.");
		} else {
			return new MessageBean("error", "The verification code is not correct.");
		}
	}

	@RequestMapping("/verifyEmail")
	public ResponseEntity<?> verifyEmail(@RequestParam("verifCode") String verifCode) {
		try {
    		userService.verifyEmailAddress(verifCode);
    		MessageBean messageBean = new MessageBean("success", "Your Email address has been verified successfully.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Got error while trying to update User", e);
			MessageBean messageBean = new MessageBean("error", e.getMessage());
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	private void authenticateUserAndSetSession(String username, String password, String domain, 
			HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse) {
        DomainUsernamePasswordAuthenticationToken token = new DomainUsernamePasswordAuthenticationToken(username, password, domain);
        httpServletRequest.getSession();
        token.setDetails(new WebAuthenticationDetails(httpServletRequest));
        Authentication authenticatedUser = authenticationManager.authenticate(token);
        try {
			authenticationSuccessHandler.onAuthenticationSuccess(httpServletRequest, httpServletResponse, authenticatedUser);
		} catch (IOException | ServletException e) {
			LOGGER.error("Got an error when trying to call authentication success handler: " + e.getMessage(), e.getCause());
		}
        SecurityContextHolder.getContext().setAuthentication(authenticatedUser);
    }
}
