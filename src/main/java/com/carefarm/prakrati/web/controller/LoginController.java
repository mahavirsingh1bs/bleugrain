package com.carefarm.prakrati.web.controller;

import java.security.Principal;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class LoginController {

	private static final Logger LOGGER = LoggerFactory.getLogger(LoginController.class);
	
	private static final String LOGIN = "login";
	
	private static final String ERRORS_403 = "errors/403";
	
	@RequestMapping(value="/login", method=RequestMethod.GET)
	public String login(ModelMap modelMap){
		return LOGIN;
	}
	
	@RequestMapping(value="/user", method=RequestMethod.GET)
	public @ResponseBody Principal user(Principal user){
		LOGGER.info("Returning logged in user details");
		return user;
	}
	
	@RequestMapping(value="/errors/403", method=RequestMethod.GET)
	public String error(ModelMap modelMap){
		return ERRORS_403;
	}
}
