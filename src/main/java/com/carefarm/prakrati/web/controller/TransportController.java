package com.carefarm.prakrati.web.controller;

import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.service.TransportService;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.bean.TransportBean;
import com.carefarm.prakrati.web.model.AssignDeliveryForm;
import com.carefarm.prakrati.web.model.DriverAssignForm;
import com.carefarm.prakrati.web.model.TransportForm;

@Controller
@RequestMapping("/transports")
public class TransportController {
	private static final Logger LOGGER = LoggerFactory.getLogger(TransportController.class);
	
	@Autowired
	private TransportService transportService;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showAllTransports(SearchRequest searchRequest) {
		LOGGER.info("START: Finding all the transports");
		return transportService.findByKeyword(searchRequest);
	}
	
	@RequestMapping(value="/company", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showCompanyTransports(SearchRequest searchRequest) {
		LOGGER.info("START: Finding all the company transports");
		return transportService.findByKeyword(searchRequest);
	}
	
	@RequestMapping(value="/new", method=RequestMethod.POST)
	public @ResponseBody MessageBean newTransport(@Valid @RequestBody TransportForm form, BindingResult result, 
			Authentication authentication, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
		
        try {
			transportService.create(form, currentUser);
			redirectAttributes.addFlashAttribute("message", "Transport has been added successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while add transport", e);
			redirectAttributes.addFlashAttribute("message", "Got error while adding transport");
			return new MessageBean("error", "Got error while adding transport");
		}
        
        return new MessageBean("success", "Transport has created successfully");
	}
	
	@RequestMapping(value="/data/{uniqueId}", method=RequestMethod.GET)
	public @ResponseBody TransportBean getData(@PathVariable("uniqueId") String uniqueId, ModelMap modelMap) {
		return transportService.findDetail(uniqueId);
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public @ResponseBody MessageBean update(@Valid @RequestBody TransportBean transportBean, Authentication authentication, 
			BindingResult result, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
			transportService.update(transportBean, user);
			redirectAttributes.addFlashAttribute("message", "Transport has been updated successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while updating transport", e);
			redirectAttributes.addFlashAttribute("message", "Got error while updating transport");
			return new MessageBean("error", "Got error while updating transport");
		}
        
        return new MessageBean("success", "Transport has been updated successfully.");
	}
	
	@RequestMapping(value = "/delete/{uniqueId}", method = RequestMethod.DELETE)
	public @ResponseBody MessageBean delete(@PathVariable("uniqueId") String uniqueId, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
		
		transportService.delete(uniqueId, currentUser);
		return new MessageBean("success", "Transport has been deleted successfully");
	}
	
	@RequestMapping(value="/assignDriver", method=RequestMethod.POST)
	public @ResponseBody MessageBean assign(@RequestBody DriverAssignForm form,
			Authentication authentication) {
		LOGGER.info("START: assigning the driver to the " + form.getUniqueId());
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
		transportService.assignDriver(form, currentUser);
		return new MessageBean("success", "Driver has been assigned successfully");
	}
	
	@RequestMapping(value="/assignDelivery", method=RequestMethod.POST)
	public @ResponseBody MessageBean assign(@RequestBody AssignDeliveryForm form,
			Authentication authentication) {
		LOGGER.info("START: assigning the driver to the " + form.getUniqueId());
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
		transportService.assignDelivery(form, currentUser);
		return new MessageBean("success", "Driver has been assigned successfully");
	}
	
}
