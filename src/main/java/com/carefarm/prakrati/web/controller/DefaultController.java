package com.carefarm.prakrati.web.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.carefarm.prakrati.common.entity.Country;
import com.carefarm.prakrati.common.entity.State;
import com.carefarm.prakrati.repository.CountryRepository;
import com.carefarm.prakrati.repository.StateRepository;
import com.carefarm.prakrati.web.bean.CountryBean;
import com.carefarm.prakrati.web.bean.StateBean;

@Controller
public class DefaultController {

	@Autowired
	public StateRepository stateRepository;
	
	@Autowired
	private CountryRepository countryRepository;
	
	@ModelAttribute("allStates")
    public List<StateBean> populateStates() {
        List<StateBean> states = new ArrayList<StateBean>();
        for (State state : stateRepository.findAll()) {
        	states.add(new StateBean(state.getId(), state.getName()));
        }
        return states;
    }
	
	@ModelAttribute("allCountries")
    public List<CountryBean> populateCountries() {
        List<CountryBean> countries = new ArrayList<CountryBean>();
        for (Country country : countryRepository.findAll()) {
        	countries.add(new CountryBean(country.getId(), country.getName()));
        }
        return countries;
    }
	
	@RequestMapping("/default")
    public String defaultAfterLogin(HttpServletRequest request) {
        /*if (request.isUserInRole("ROLE_ADMIN")) {
            return "redirect:/farmers/form";
        } else if (request.isUserInRole("ROLE_FARMER")) {
        	return "redirect:/products/form";
        } else if (request.isUserInRole("ROLE_COMPANY")) {
        	return "redirect:/bids/form";
        }*/
        return "redirect:/home";
    }
}
