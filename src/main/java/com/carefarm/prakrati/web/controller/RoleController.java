package com.carefarm.prakrati.web.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.service.RoleService;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.bean.RoleBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.RoleForm;

@Controller
@RequestMapping("/roles")
public class RoleController {

	private static final Logger LOGGER = LoggerFactory.getLogger(RoleController.class);
	
	@Autowired
	private RoleService roleService;
	
	@RequestMapping(value="", method=RequestMethod.GET)
	public @ResponseBody List<RoleBean> showAllRoles() {
		return roleService.findAll();
	}
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showRoles(SearchRequest searchRequest) {
		return roleService.findByKeyword(searchRequest);
	}

	@RequestMapping(value="/new", method=RequestMethod.POST)
	public @ResponseBody MessageBean newRole(@Valid @RequestBody RoleForm form, BindingResult result, 
			Authentication authentication, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
            return new MessageBean("failure", "Form has validation errors.");
        }
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
        	roleService.create(form, user);
			redirectAttributes.addFlashAttribute("message", "Role has been added successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got a server error. So role cannot be added now.", e);
			return new MessageBean("error", "Got server error while adding role.");
		}
        
        return new MessageBean("success", "Role has been added successfully");
	}
	
	@RequestMapping(value="/data/{roleId}", method=RequestMethod.GET)
	public @ResponseBody RoleBean getRoleData(@PathVariable("roleId") long roleId, ModelMap modelMap){
		return roleService.findDetails(roleId, null);
	}

	@RequestMapping(value="/update", method=RequestMethod.POST)
	public @ResponseBody MessageBean update(@Valid @RequestBody RoleBean roleBean, BindingResult result, 
			RedirectAttributes redirectAttributes, Authentication authentication) {
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
			roleService.update(roleBean, user);
			redirectAttributes.addFlashAttribute("message", "Role has been updated successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while creating role", e);
			redirectAttributes.addFlashAttribute("message", "Got error while updating role");
			return new MessageBean("error", "Got error while updating role");
		}
        
        return new MessageBean("success", "Role has been updated successfully.");
	}
	
	@RequestMapping(value = "/delete/{uniqueId}", method = RequestMethod.DELETE)
	public @ResponseBody MessageBean delete(@PathVariable("uniqueId") String uniqueId) {
		roleService.delete(uniqueId);
		return new MessageBean("success", "Role has been deleted successfully");
	}
}
