package com.carefarm.prakrati.web.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.service.AdvancedBookingService;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.model.AdvancedBookingForm;

@Controller
public class AdvancedBookingController {

	private static final Logger LOGGER = LoggerFactory.getLogger(AdvancedBookingController.class);
	
	@Autowired
	private AdvancedBookingService advancedBookingService;
	
	@RequestMapping(value = "/advancedBooking", method = { RequestMethod.POST })
	public ResponseEntity<?> addBillingAddress(@Valid @RequestBody AdvancedBookingForm form, BindingResult result) {
		try {
			advancedBookingService.advanceBooking(form);
    		MessageBean messageBean = new MessageBean("success", "Advanced Booking has been added successfully.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Got error while trying to add advanced booking.", e);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to add advanced booking.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
}
