package com.carefarm.prakrati.web.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.carefarm.prakrati.service.UnitService;
import com.carefarm.prakrati.web.bean.UnitBean;

@Controller
@RequestMapping("/units")
public class UnitController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UnitController.class);
	
	@Autowired
	private UnitService unitService;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody List<UnitBean> showAllUnits(HttpServletRequest request) {
		LOGGER.info("Findint the units");
		return unitService.findAll();
	}
}
