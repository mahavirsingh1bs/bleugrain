package com.carefarm.prakrati.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.carefarm.prakrati.service.TagService;
import com.carefarm.prakrati.web.bean.TagBean;

@Controller
@RequestMapping(value = "/tags", method = RequestMethod.GET)
public class TagController {

	@Autowired
	private TagService tagService;
	
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public @ResponseBody List<TagBean> findTags(@RequestParam("keywords") String keywords) {
		return tagService.findByMatchingKeywords(keywords);
	}
}
