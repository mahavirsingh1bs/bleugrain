package com.carefarm.prakrati.web.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.service.SocialAccountService;
import com.carefarm.prakrati.web.bean.SocialAccountBean;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.model.SocialAccountForm;

@Controller
@RequestMapping("/socialAccounts")
public class SocialAccountController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SocialAccountController.class);

	@Autowired
	private SocialAccountService socialAccountService;
	
	@RequestMapping(value = "/add", method = { RequestMethod.POST })
	public ResponseEntity<?> addDeliveryAddress(@RequestParam("userNo") String userNo, @Valid @RequestBody SocialAccountForm socialAccountForm, 
			BindingResult result, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		socialAccountService.addSocialAccount(socialAccountForm, userNo, currentUser);
    		MessageBean messageBean = new MessageBean("success", "Your social account has been added successfully.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Got error while trying to add social account", e);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to add social account.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/get", method = { RequestMethod.GET })
	public ResponseEntity<?> get(@RequestParam("uniqueId") String uniqueId, Authentication authentication) {
		try {
			SocialAccountBean socialAccountBean = socialAccountService.find(uniqueId);
    		return new ResponseEntity<SocialAccountBean>(socialAccountBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Not able to find the specified socialAccount.", e);
			MessageBean messageBean = new MessageBean("error", "Not able to find the specified socialAccount.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/update", method = { RequestMethod.PUT })
	public ResponseEntity<?> update(@Valid @RequestBody SocialAccountBean socialAccountBean, 
			BindingResult result, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		socialAccountService.update(socialAccountBean, currentUser);
    		MessageBean messageBean = new MessageBean("success", "Your socialAccount has been updated successfully.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Got error while trying to update socialAccount", e);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to update socialAccount.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/delete", method = { RequestMethod.DELETE })
	public ResponseEntity<?> delete(@RequestParam("uniqueId") String uniqueId, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	try {
    		socialAccountService.remove(uniqueId, currentUser);
    		MessageBean messageBean = new MessageBean("success", "Social Account has been deleted successfully.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Got error while trying to delete social account", e);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to delete social account.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
}
