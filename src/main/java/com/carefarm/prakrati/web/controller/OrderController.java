package com.carefarm.prakrati.web.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.service.OrderService;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.bean.OrderBean;
import com.carefarm.prakrati.web.bean.OrderDetailsBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.OrderAssignForm;
import com.carefarm.prakrati.web.model.OrderDispatchForm;
import com.carefarm.prakrati.web.model.OrderUpdateForm;

@Controller
@RequestMapping("/orders")
public class OrderController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

	@Autowired
	private OrderService orderService;
		
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showOrders(SearchRequest searchRequest) {
		return orderService.findByKeyword(searchRequest);
	}
	
	@RequestMapping(value="/user", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showUserOrders(SearchRequest searchRequest) {
		LOGGER.info("Finding user orders");
		return orderService.findByKeyword(searchRequest);
	}
	
	@RequestMapping(value="/company", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showCompanyOrders(SearchRequest searchRequest) {
		LOGGER.info("Finding company users");
		return orderService.findByKeyword(searchRequest);
	}
	
	@RequestMapping(value="/processOrder", method=RequestMethod.POST)
	public @ResponseBody MessageBean processOrder(@RequestBody OrderDetailsBean orderDetails,
			Authentication authentication) {
		LOGGER.info("START: processing a new order request");
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		orderService.processOrder(orderDetails, user);
		return new MessageBean("success", "Order has been processed successfully");
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public @ResponseBody MessageBean updateOrder(@RequestBody OrderUpdateForm form,
			Authentication authentication) {
		LOGGER.info("START: processing a new order request");
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
		orderService.updateOrder(form, currentUser);
		return new MessageBean("success", "Order has been processed successfully");
	}
	
	@RequestMapping(value="/assign", method=RequestMethod.POST)
	public @ResponseBody MessageBean assignOrder(@RequestBody OrderAssignForm form,
			Authentication authentication) {
		LOGGER.info("START: assigning the order to the " + form.getAssignTo().getFullName());
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
		orderService.assignOrder(form, currentUser);
		return new MessageBean("success", "Order has been assigned successfully");
	}
	
	@RequestMapping(value="/dispatched", method=RequestMethod.POST)
	public @ResponseBody MessageBean dispatchOrder(@RequestBody OrderDispatchForm form,
			Authentication authentication) {
		LOGGER.info("START: order is dipatching");
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
		orderService.dispatchOrder(form, currentUser);
		return new MessageBean("success", "Order dispatch has been done");
	}
	
	@RequestMapping(value="/shipped", method=RequestMethod.POST)
	public @ResponseBody MessageBean shipOrder(@RequestParam("orderNo") String orderNo,
			Authentication authentication) {
		LOGGER.info("START: order is shipping");
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
		orderService.shipOrder(orderNo, currentUser);
		return new MessageBean("success", "Order ship has been done");
	}
	
	@RequestMapping(value="/delivered", method=RequestMethod.POST)
	public @ResponseBody MessageBean deliverOrder(@RequestParam("orderNo") String orderNo,
			Authentication authentication) {
		LOGGER.info("START: order is delivering");
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
		orderService.deliverOrder(orderNo, currentUser);
		return new MessageBean("success", "Order has been delivered");
	}
	
	@RequestMapping(value="/data/{orderNo}", method=RequestMethod.GET)
	public @ResponseBody OrderBean getData(@PathVariable("orderNo") String orderNo) {
		return orderService.findDetail(orderNo);
	}
	
}
