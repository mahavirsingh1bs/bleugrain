package com.carefarm.prakrati.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.service.NoteService;
import com.carefarm.prakrati.web.bean.MessageBean;

@Controller
@RequestMapping("/notes")
public class NoteController {

	private static final Logger LOGGER = LoggerFactory.getLogger(NoteController.class);
	
	@Autowired
	private NoteService noteService;
	
	@RequestMapping(value = "/delete", method = { RequestMethod.DELETE })
	public ResponseEntity<?> deleteNote(@RequestParam("uniqueId") String uniqueId, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		noteService.delete(uniqueId, currentUser);
    		MessageBean messageBean = new MessageBean("success", "Note has been deleted successfully.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Got error while trying to delete note.", e);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to delete note.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
}