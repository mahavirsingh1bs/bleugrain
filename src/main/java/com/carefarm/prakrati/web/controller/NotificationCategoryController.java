package com.carefarm.prakrati.web.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.carefarm.prakrati.service.NotificationCategoryService;
import com.carefarm.prakrati.web.bean.NotificationCategoryBean;
import com.carefarm.prakrati.web.bean.SearchRequest;

@Controller
@RequestMapping("/notifCategories")
public class NotificationCategoryController {

	private static final Logger LOGGER = LoggerFactory.getLogger(NotificationCategoryController.class);
    
    @Autowired
    private NotificationCategoryService categoryService;
    
    @RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody List<NotificationCategoryBean> showCountries(SearchRequest searchRequest) {
    	LOGGER.info("Finding all countries");
		return categoryService.findAll();
	}
}
