package com.carefarm.prakrati.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.service.GrainCartService;
import com.carefarm.prakrati.web.bean.GrainCartBean;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.bean.ProductBean;

@Controller
@RequestMapping("/graincart")
public class GrainCartController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(GrainCartController.class);
	
	@Autowired
	private GrainCartService grainCartService;
	
	@RequestMapping(value="/", method = RequestMethod.GET)
	public @ResponseBody GrainCartBean findCartDetails(Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		GrainCartBean grainCart = grainCartService.findCartDetails(user);
		return grainCart;
	}
	
	@RequestMapping(value="/addToCart", method = RequestMethod.POST)
	public @ResponseBody MessageBean addToCart(@RequestBody String uniqueId, Authentication authentication) {
		LOGGER.info("START: Adding product " + uniqueId + " to the cart");
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		grainCartService.addToCart(uniqueId, user);
		LOGGER.info("END: Added product " + uniqueId + " to the cart");
		return new MessageBean("success", "Item has been added successfully added to the cart");
	}
	
	@RequestMapping(value="/addToConsumerCart", method = RequestMethod.POST)
	public @ResponseBody MessageBean addToConsumerCart(@RequestBody ProductBean productBean, Authentication authentication) {
		LOGGER.info("START: Adding product " + productBean.getUniqueId() + " to the consumer cart");
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		grainCartService.addToConsumerCart(productBean, user);
		LOGGER.info("END: Added product " + productBean.getUniqueId() + " to the cart");
		return new MessageBean("success", "Item has been added successfully added to the consumer cart");
	}
	
	@RequestMapping(value="/removeFromCart", method = RequestMethod.POST)
	public @ResponseBody MessageBean removeFromCart(@RequestBody String uniqueId, Authentication authentication) {
		LOGGER.info("START: Adding product " + uniqueId + " to the cart");
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		grainCartService.removeFromCart(uniqueId, user);
		LOGGER.info("END: Added product " + uniqueId + " to the cart");
		return new MessageBean("success", "Item has been deleted successfully from the cart");
	}
	
	@RequestMapping(value="/removeFromConsumerCart", method = RequestMethod.POST)
	public @ResponseBody MessageBean removeFromConsumerCart(@RequestBody String uniqueId, Authentication authentication) {
		LOGGER.info("START: Adding product " + uniqueId + " to the cart");
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		grainCartService.removeFromConsumerCart(uniqueId, user);
		LOGGER.info("END: Added product " + uniqueId + " to the cart");
		return new MessageBean("success", "Item has been deleted successfully from the cart");
	}
	
}
