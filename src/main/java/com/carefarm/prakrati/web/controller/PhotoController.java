package com.carefarm.prakrati.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class PhotoController {

	@RequestMapping(value="/showPhotos", method=RequestMethod.GET)
	public String showPhotos(ModelMap modelMap){
		return "photos";
	}
}
