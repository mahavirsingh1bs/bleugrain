package com.carefarm.prakrati.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.service.PaymentCardService;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.bean.PaymentCardBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.PaymentCardForm;

@Controller
@RequestMapping("/paymentCards")
public class PaymentCardController {

	private static final Logger LOGGER = LoggerFactory.getLogger(PaymentCardController.class);
	
	@Autowired
	private PaymentCardService paymentCardService;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showPaymentCards(SearchRequest searchRequest) {
		LOGGER.info("Finding all payment cards");
    	if (searchRequest.getPageSize() > 0) {
	    	return paymentCardService.findByKeyword(searchRequest);
    	} else {
    		Map<String, Object> data = new HashMap<>();
    		List<PaymentCardBean> paymentCards = paymentCardService.findAll();
    		data.put("data", paymentCards);
    		return data;
    	}
	}

	@RequestMapping(value="/new", method=RequestMethod.POST)
	public @ResponseBody MessageBean newPaymentCard(@Valid @RequestBody PaymentCardForm form, BindingResult result, 
			Authentication authentication, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
            return new MessageBean("failure", "Form has validation errors.");
        }
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
			paymentCardService.create(form, user);
			redirectAttributes.addFlashAttribute("message", "Payment card has been added successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got a server error. So paymentCard cannot be added now.", e);
			return new MessageBean("error", "Got server error while adding payment card.");
		}
        
        return new MessageBean("success", "Payment card has been added successfully");
	}
	
	@RequestMapping(value="/data/{paymentCardId}", method=RequestMethod.GET)
	public @ResponseBody PaymentCardBean getPaymentCardData(@PathVariable("paymentCardId") long paymentCardId, ModelMap modelMap){
		return paymentCardService.findDetails(paymentCardId, null);
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public @ResponseBody MessageBean update(@Valid @RequestBody PaymentCardBean paymentCardBean, BindingResult result, 
			RedirectAttributes redirectAttributes, Authentication authentication) {
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
			paymentCardService.update(paymentCardBean, user);
			redirectAttributes.addFlashAttribute("message", "PaymentCard has been updated successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while creating paymentCard", e);
			redirectAttributes.addFlashAttribute("message", "Got error while updating paymentCard");
			return new MessageBean("error", "Got error while updating paymentCard");
		}
        
        return new MessageBean("success", "PaymentCard has been updated successfully.");
	}
	
	@RequestMapping(value = "/delete/{uniqueId}", method = RequestMethod.DELETE)
	public @ResponseBody MessageBean delete(@PathVariable("uniqueId") String uniqueId) {
		paymentCardService.delete(uniqueId);
		return new MessageBean("success", "Payment Card has been deleted successfully");
	}
}
