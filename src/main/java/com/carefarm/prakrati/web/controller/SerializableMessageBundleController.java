package com.carefarm.prakrati.web.controller;

import java.util.Locale;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.carefarm.prakrati.config.SerializableResourceBundleMessageSource;

@Controller
@RequestMapping("/Messages")
public class SerializableMessageBundleController {

	@Autowired
	private SerializableResourceBundleMessageSource messageBundle;

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	public Properties list(@RequestParam String lang) {
		return messageBundle.getAllProperties(new Locale(lang));
	}

}
