package com.carefarm.prakrati.web.controller;

import static com.carefarm.prakrati.common.util.Constants.USER_ID;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.common.util.Constants;
import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.service.UserService;
import com.carefarm.prakrati.util.UserType;
import com.carefarm.prakrati.web.bean.AddressBean;
import com.carefarm.prakrati.web.bean.CompanyBean;
import com.carefarm.prakrati.web.bean.GroupBean;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.bean.UserBean;

@Controller
public class UserController {

	private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);
	
	private static final String COMPANY_ADMIN = "CompanyAdmin";
	
	private static final String AGENT = "Agent";
	
	@Autowired
	private DozerBeanMapper mapper;
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private UserService userService;
	
	@Autowired
    @Qualifier("sessionRegistry")
    private SessionRegistry sessionRegistry;
	
	@RequestMapping(value="/user/data/{userNo}", method=RequestMethod.GET)
	public @ResponseBody UserBean getData(@PathVariable("userNo") String userNo, ModelMap modelMap) {
		return userService.findDetail(userNo);
	}
	
	@RequestMapping("/user")
	public Map<String, Object> user(Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		Map<String, Object> responseData = new HashMap<>();
		responseData.put("id", httpSession.getId());
		
		UserBean userBean = mapper.map(user, UserBean.class);
		userBean.setGroup(mapper.map(user.getGroup(), GroupBean.class));
		userBean.setAddress(mapper.map(user.getAddress(), AddressBean.class));
		
		if (user.getGroup().getName().equals(COMPANY_ADMIN) || 
    			user.getGroup().getName().equals(AGENT)) {
			userBean.setCompany(mapper.map(user.getCompany(), CompanyBean.class));
    	}
		responseData.put("user", userBean);
		return responseData;
	}
	
	@RequestMapping("/users/update")
	public ResponseEntity<?> update(@RequestParam("userNo") String userNo, @Valid @RequestBody UserBean userBean, BindingResult result, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		userService.update(userBean, userNo, currentUser);
    		MessageBean messageBean = new MessageBean("success", "User has been updated successfully.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Got error while trying to update User", e);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to update User.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/users/search", method = RequestMethod.GET)
	public @ResponseBody List<UserBean> findUsers(@RequestParam("keywords") String keywords) {
		return userService.findByMatchingKeywords(keywords);
	}
	
	@RequestMapping(value = "/farmers/search", method = RequestMethod.GET)
	public @ResponseBody List<UserBean> findFarmers(@RequestParam("keywords") String keywords) {
		return userService.findByMatchingKeywords(keywords, UserType.FARMER);
	}
	
	@RequestMapping(value = "/brokers/search", method = RequestMethod.GET)
	public @ResponseBody List<UserBean> findBrokers(@RequestParam("keywords") String keywords) {
		return userService.findByMatchingKeywords(keywords, UserType.BROKER);
	}
	
	@RequestMapping(value = "/retailers/search", method = RequestMethod.GET)
	public @ResponseBody List<UserBean> findRetailers(@RequestParam("keywords") String keywords) {
		return userService.findByMatchingKeywords(keywords, UserType.RETAILER);
	}
	
	@RequestMapping(value = "/agents/search", method = RequestMethod.GET)
	public @ResponseBody List<UserBean> findAgents(@RequestParam("keywords") String keywords) {
		return userService.findByMatchingKeywords(keywords, UserType.AGENT);
	}
	
	@RequestMapping(value = "/employees/search", method = RequestMethod.GET)
	public @ResponseBody List<UserBean> findEmployees(@RequestParam("keywords") String keywords) {
		return userService.findByMatchingKeywords(keywords, UserType.EMPLOYEE);
	}
	
	@RequestMapping(value = "/owners/search", method = RequestMethod.GET)
	public @ResponseBody List<UserBean> findOwners(@RequestParam("keywords") String keywords) {
		return userService.findByMatchingKeywords(keywords, UserType.FARMER, UserType.BROKER);
	}
	
	@RequestMapping(value = "/drivers/search", method = RequestMethod.GET)
	public @ResponseBody List<UserBean> findDrivers(@RequestParam("keywords") String keywords,
			@RequestParam("companyId") String companyId) {
		return userService.findByMatchingKeywords(keywords, companyId, UserType.DRIVER);
	}
	
	@RequestMapping(value = "/users", method = RequestMethod.GET)
    public @ResponseBody Set<String> getUsers() {
		// get the list of users from Spring Security's session registry
		return sessionRegistry.getAllPrincipals().stream().map(u -> ((User) u).getUsername()).collect(Collectors.toSet());
    }
	
	@RequestMapping(value = "/currentUser", method = RequestMethod.GET)
    public @ResponseBody ResponseEntity<?> getCurrentUser(HttpSession httpSession) {
		Long userId = (Long )httpSession.getAttribute(USER_ID);
		
		if (Predicates.isNull.test(userId)) {
			MessageBean messageBean = new MessageBean(Constants.SUCCESS, "Current User Session doesn't exist");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
		
		UserBean currentUser = userService.findCurrentUser(userId);
		return new ResponseEntity<UserBean>(currentUser, HttpStatus.OK);
    }

}
