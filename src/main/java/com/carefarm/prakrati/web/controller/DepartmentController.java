package com.carefarm.prakrati.web.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.service.DepartmentService;
import com.carefarm.prakrati.web.bean.DepartmentBean;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.DepartmentForm;

@Controller
@RequestMapping("/departments")
public class DepartmentController {

	private static final Logger LOGGER = LoggerFactory.getLogger(DepartmentController.class);
	
	@Autowired
	private DepartmentService departmentService;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showDepartments(SearchRequest searchRequest) {
		LOGGER.info("Finding the all departments");
		if (searchRequest.getPageSize() > 0) {
	    	return departmentService.findByKeyword(searchRequest);
    	} else {
    		Map<String, Object> data = new HashMap<>();
    		List<DepartmentBean> departments = departmentService.findAll();
    		data.put("departments", departments);
    		return data;
    	}
	}

	@RequestMapping(value="/new", method=RequestMethod.POST)
	public @ResponseBody MessageBean newDepartment(@Valid @RequestBody DepartmentForm form, BindingResult result, 
			Authentication authentication, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
            return new MessageBean("failure", "Form has validation errors.");
        }
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
        	departmentService.create(form, user);
			redirectAttributes.addFlashAttribute("message", "Department has been added successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got a server error. So department cannot be added now.", e);
			return new MessageBean("error", "Got server error while adding department.");
		}
        
        return new MessageBean("success", "Department has been added successfully");
	}

	@RequestMapping(value="/data/{departmentId}", method=RequestMethod.GET)
	public @ResponseBody DepartmentBean getDepartmentData(@PathVariable("departmentId") long departmentId, ModelMap modelMap){
		return departmentService.findDetails(departmentId, null);
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public @ResponseBody MessageBean update(@Valid @RequestBody DepartmentBean departmentBean, BindingResult result, 
			RedirectAttributes redirectAttributes, Authentication authentication) {
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
			departmentService.update(departmentBean, user);
			redirectAttributes.addFlashAttribute("message", "Department has been updated successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while creating department", e);
			redirectAttributes.addFlashAttribute("message", "Got error while updating department");
			return new MessageBean("error", "Got error while updating department");
		}
        
        return new MessageBean("success", "Department has been updated successfully.");
	}
	
	@RequestMapping(value = "/delete/{uniqueId}", method = RequestMethod.DELETE)
	public @ResponseBody MessageBean delete(@PathVariable("uniqueId") String uniqueId) {
		LOGGER.info("Deleting the department " + uniqueId);
		departmentService.delete(uniqueId);
		return new MessageBean("success", "Department has been deleted successfully");
	}
}
