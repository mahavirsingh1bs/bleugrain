package com.carefarm.prakrati.web.controller;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.service.ProductService;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.bean.ProductBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.EnableBiddingForm;
import com.carefarm.prakrati.web.model.PriceUpdateForm;
import com.carefarm.prakrati.web.model.ProductAssignForm;
import com.carefarm.prakrati.web.model.ProductForm;
import com.carefarm.prakrati.web.model.ReviewForm;
import com.carefarm.prakrati.web.model.UpdateDescForm;

@Controller
@RequestMapping("/products")
public class ProductController {

	private static final Logger LOGGER = LoggerFactory.getLogger(ProductController.class);

	@Autowired
	private ProductService productService;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showProducts(SearchRequest searchRequest) {
		return productService.findByKeyword(searchRequest);
	}
	
	@RequestMapping(value="/user", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showUserProducts(SearchRequest searchRequest) {
		return productService.findByKeyword(searchRequest);
	}

	@RequestMapping(value="/biddable", method=RequestMethod.GET)
	public @ResponseBody List<ProductBean> showBiddableProducts(@RequestParam(name = "uniqueId") String uniqueId,
			@RequestParam(name = "stateId") Long stateId,
			@RequestParam(name = "page") int page, @RequestParam(name = "pageSize") int pageSize) {
		return productService.findBiddableProducts(stateId, uniqueId, new PageRequest(page, pageSize));
	}
	
	@RequestMapping(value="/data/{uniqueId}", method=RequestMethod.GET)
	public @ResponseBody ProductBean productDetail(@PathVariable("uniqueId") String uniqueId) {
		return productService.findProductDetail(uniqueId, null);
	}
	
	@RequestMapping(value = "/new", method = RequestMethod.POST)
	public @ResponseBody MessageBean newProduct(@Valid @RequestPart("product") ProductForm form, BindingResult result, 
			@RequestPart(name = "file", required = false) List<MultipartFile> files, Authentication authentication, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
            return new MessageBean("error", "form has validation errors");
        }
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
		try {
			productService.addProduct(form, files, null, user);
			redirectAttributes.addFlashAttribute("message", "Product has been added successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while creating company", e);
			return new MessageBean("error", "Got error while adding product");
		}
        
    	return new MessageBean("success", "Product has been added successfully");
	}

	@RequestMapping(value = "/addUpdateDesc", method = RequestMethod.POST)
	public ResponseEntity<?> addUpdateDesc(@Valid @RequestBody UpdateDescForm form, BindingResult result, Authentication authentication) {
		if(result.hasErrors()) {
			MessageBean messageBean = new MessageBean("error", "form has validation errors");
            return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
        }
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		productService.addUpdateDesc(form, currentUser);
    		MessageBean messageBean = new MessageBean("success", "Description has been added successfully.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Got error while trying to add the description", e);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to add the description");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/setAsDefault", method = RequestMethod.POST)
	public ResponseEntity<?> setAsDefault(@RequestParam("productId") String productId, 
			@RequestParam("imageId") Long imageId, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		productService.setAsDefault(productId, imageId, currentUser);
    		MessageBean messageBean = new MessageBean("success", "Image has been set as default successfully.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Got error while trying to this image as default", e);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to this image as default");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value = "/uploadImages", method = RequestMethod.POST)
	public ResponseEntity<?> uploadImages(@RequestParam(value = "productId") String productId, 
			@RequestPart(value = "images", required = false) List<MultipartFile> images, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		productService.uploadImages(images, productId, currentUser);
    		MessageBean messageBean = new MessageBean("success", "Images has been uploaded successfully.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Got error while trying to upload picture", e);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to upload profile picture");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public @ResponseBody MessageBean update(@Valid @RequestBody ProductBean productBean, BindingResult result, 
			Authentication authentication, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
			productService.update(productBean, user);
			redirectAttributes.addFlashAttribute("message", "You have successfully signed up and logged in.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while creating product", e);
			redirectAttributes.addFlashAttribute("message", "Got error while creating product");
			return new MessageBean("error", "Got error while creating product");
		}
        
        return new MessageBean("success", "Product has been updated successfully");
	}

	@RequestMapping(value="/assign", method=RequestMethod.POST)
	public @ResponseBody MessageBean assign(@RequestBody ProductAssignForm form,
			Authentication authentication) {
		LOGGER.info("START: assigning the product to the " + form.getAssignTo().getFullName());
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
		productService.assign(form, currentUser);
		return new MessageBean("success", "Product has been assigned successfully");
	}
	
	@RequestMapping(value="/enableBidding", method=RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> place(@Valid @RequestBody EnableBiddingForm form, 
			BindingResult result, Authentication authentication) {
        DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
		try {
			productService.enableBidding(form, currentUser);
			MessageBean messageBean = new MessageBean("success", "Bidding has been enabled successfully");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
		} catch (ServiceException exception) {
			LOGGER.error("Got error while placing a bid", exception);
			MessageBean messageBean = new MessageBean("error", "Bidding cannot be enabled at the moment");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}

	@RequestMapping(value="/soldOut", method=RequestMethod.POST)
	public @ResponseBody MessageBean markAsSoldOut(@RequestParam("uniqueId") String uniqueId,
			Authentication authentication) {
		LOGGER.info("START: marking product as sold out");
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
		productService.markAsSoldOut(uniqueId, currentUser);
		return new MessageBean("success", "Product has been marked as sold out");
	}
	
	@RequestMapping(value = "/delete/{uniqueId}", method = RequestMethod.DELETE)
	public @ResponseBody MessageBean delete(@PathVariable("uniqueId") String uniqueId, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
		productService.delete(uniqueId, currentUser);
		return new MessageBean("success", "Product has been deleted successfully");
	}
	
	@RequestMapping(value = "/review", method = RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> submit(@RequestBody ReviewForm form, BindingResult result, Authentication authentication) {
		if(result.hasErrors()) {
			MessageBean messageBean = new MessageBean("error", "form has errors");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
        }
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		productService.submitReview(form, currentUser);
			MessageBean messageBean = new MessageBean("success", "Review has been submitted successfully");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
		} catch (ServiceException exception) {
			LOGGER.error("Got error while submitting review", exception);
			MessageBean messageBean = new MessageBean("error", "Review cannot be submitted at the moment");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	

	@RequestMapping(value="/update/price", method=RequestMethod.POST)
	public @ResponseBody ResponseEntity<?> updatePrice(@Valid @RequestBody PriceUpdateForm form, BindingResult result, 
			Authentication authentication) {
		if(result.hasErrors()) {
			MessageBean messageBean = new MessageBean("error", "form has errors");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
        }
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		productService.updatePrice(form, currentUser);
			MessageBean messageBean = new MessageBean("success", "Price has been updated successfully");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
		} catch (ServiceException exception) {
			LOGGER.error("Got error while trying to update price.", exception);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to update prices.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
}
