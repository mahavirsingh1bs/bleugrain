package com.carefarm.prakrati.web.controller;

import static com.carefarm.prakrati.web.util.PrakratiConstants.DEFAULT_PAGE_NO;
import static com.carefarm.prakrati.web.util.PrakratiConstants.DEFAULT_PAGE_SIZE;

import java.io.IOException;
import java.util.Map;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.search.dto.ModelMap;
import com.carefarm.prakrati.search.dto.SearchCriteria;
import com.carefarm.prakrati.service.SearchService;
import com.carefarm.prakrati.web.bean.DemandBean;
import com.carefarm.prakrati.web.bean.FiltersBean;
import com.carefarm.prakrati.web.bean.ProductBean;

@Controller
@RequestMapping("/search")
public class SearchController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SearchController.class);
	
	@Autowired
	private SearchService searchService;

	@RequestMapping(value="/", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ModelMap<ProductBean> search(@RequestParam(value = "uniqueId", required = false) String uniqueId, 
			@RequestParam(value = "keyword", required = false) String keywords,
			@RequestParam(value = "cnsmrPrdt", required = true) boolean cnsmrPrdt,
			@RequestParam(value = "facets", required = false) String facets,
			@PageableDefault(
				page = DEFAULT_PAGE_NO, 
				size = DEFAULT_PAGE_SIZE, 
				direction = Direction.DESC, 
				sort = { "dateModified" }) Pageable pageable,
			Authentication authentication) {
	
		ObjectMapper mapper = new ObjectMapper();
		FiltersBean filters = null;
		try {
			filters = mapper.readValue(facets, FiltersBean.class);
		} catch (IOException e) {
			LOGGER.error("An error occurred while mapping facets to filters");
		}
		
		SearchCriteria criteria = new SearchCriteria();
		criteria.setUniqueId(uniqueId);
    	criteria.setKeywords(keywords);
    	criteria.setCnsmrPrdt(cnsmrPrdt);
    	criteria.setFilters(filters);
    	criteria.setPageable(pageable);
    	
		if (Predicates.notNull.test(authentication)) {
			DomainUsernamePasswordAuthenticationToken token = 
	    			(DomainUsernamePasswordAuthenticationToken )authentication;
	    	User currentUser = (User )token.getPrincipal();
	    	criteria.setCurrentUser(currentUser);
		}
		
		return searchService.search(criteria);
	}
	
	@RequestMapping(value="/detail", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody Map<String, Object> searchDetail(@RequestParam("uniqueId") String uniqueId, 
			Authentication authentication) {
		User currentUser = null;
		if (Predicates.notNull.test(authentication)) {
			DomainUsernamePasswordAuthenticationToken token = 
	    			(DomainUsernamePasswordAuthenticationToken )authentication;
	    	currentUser = (User )token.getPrincipal();
		}
		return searchService.findProductDetail(uniqueId, currentUser);
	}
	
	@RequestMapping(value="/demands", method=RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public ModelMap<DemandBean> searchDemands(@RequestParam(value = "uniqueId", required = false) String uniqueId, 
			@RequestParam(value = "keyword", required = false) String keywords,
			@RequestParam(value = "facets", required = false) String facets,
			@PageableDefault(
				page = DEFAULT_PAGE_NO, 
				size = DEFAULT_PAGE_SIZE, 
				direction = Direction.DESC, 
				sort = { "dateModified" }) Pageable pageable, 
			Authentication authentication) {
	
		ObjectMapper mapper = new ObjectMapper();
		
		FiltersBean filters = null;
		try {
			filters = mapper.readValue(facets, FiltersBean.class);
		} catch (IOException e) {
			LOGGER.error("An error occurred while mapping facets to filters");
		}
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
		
    	SearchCriteria criteria = new SearchCriteria();
    	criteria.setUniqueId(uniqueId);
    	criteria.setKeywords(keywords);
    	criteria.setFilters(filters);
    	criteria.setCurrentUser(currentUser);
    	criteria.setPageable(pageable);
    	
    	return searchService.searchDemands(criteria);
	}
	
}
