package com.carefarm.prakrati.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.service.UpcomingEventService;
import com.carefarm.prakrati.web.bean.MessageBean;

@Controller
@RequestMapping("/upcomingEvents")
public class UpcomingEventController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(UpcomingEventController.class);
	
	@Autowired
	private UpcomingEventService upcomingEventService;
	
	@RequestMapping(value = "/delete", method = { RequestMethod.DELETE })
	public ResponseEntity<?> deleteUpcomingEvent(@RequestParam("uniqueId") String uniqueId, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	
    	try {
    		upcomingEventService.delete(uniqueId, currentUser);
    		MessageBean messageBean = new MessageBean("success", "Upcoming Event has been deleted successfully.");
    		return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
    	} catch (ServiceException e) {
			LOGGER.error("Got error while trying to delete upcoming event.", e);
			MessageBean messageBean = new MessageBean("error", "Got error while trying to delete upcoming event.");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
}
