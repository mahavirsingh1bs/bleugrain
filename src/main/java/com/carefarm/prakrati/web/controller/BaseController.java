package com.carefarm.prakrati.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.Authentication;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.core.PrakratiContext;
import com.carefarm.prakrati.entity.User;

public class BaseController {

	protected PrakratiContext getPrakratiContext(Authentication authentication, HttpServletRequest request) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
    	String contextPath = findContextPath(request);
		PrakratiContext context = new PrakratiContext();
		context.setContextPath(contextPath);
		context.setCurrentUser(currentUser);
		return context;
	}
	
	protected String findContextPath(HttpServletRequest request) {
		String contextPath = request.getScheme() + "://" + 
							request.getLocalAddr() + ":" + 
							request.getLocalPort() + 
							request.getContextPath() + "/#";
		return contextPath;
	}
	
	protected User currentUser(Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	return (User )token.getPrincipal();
	}
	
}
