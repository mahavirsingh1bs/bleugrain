package com.carefarm.prakrati.web.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.carefarm.prakrati.service.LanguageService;
import com.carefarm.prakrati.web.bean.LanguageBean;

@Controller
@RequestMapping("/languages")
public class LanguageController {

	@Autowired
	private LanguageService languageService;
	
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public @ResponseBody List<LanguageBean> findLanguages(@RequestParam("keywords") String keywords) {
		return languageService.findByMatchingKeywords(keywords);
	}
}
