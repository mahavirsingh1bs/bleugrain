package com.carefarm.prakrati.web.controller;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;

public class PrakratiBaseController {

	@Autowired
	protected DozerBeanMapper mapper;
}
