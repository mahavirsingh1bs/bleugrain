package com.carefarm.prakrati.web.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.service.BranchService;
import com.carefarm.prakrati.web.bean.BranchBean;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.model.BranchForm;

@Controller
@RequestMapping("/branches")
public class BranchController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(BranchController.class);
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private BranchService branchService;
	
	@RequestMapping(value="/company", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showCompanyBranches(HttpServletRequest request) {
		LOGGER.info("finding company branches");
		Integer draw = Integer.parseInt(request.getParameter("draw"));
		Integer length = Integer.valueOf(request.getParameter("length"));
		String keyword = request.getParameter("search[value]");
		
		Integer page = 0;
		if (StringUtils.isNotBlank(request.getParameter("start"))) {
    		page = (NumberUtils.toInt(request.getParameter("start")) / length);
    	}
    	
    	Long companyId = null;
    	if (StringUtils.isNotBlank(request.getParameter("companyId"))) {
    		companyId = NumberUtils.toLong(request.getParameter("companyId"));
    	} else if (httpSession.getAttribute("companyId") != null) {
    		companyId = (Long )httpSession.getAttribute("companyId");
    	}
    	
    	PageRequest pageRequest = new PageRequest(page, length);
    	Map<String, Object> params = new HashMap<>();
    	params.put("keyword", keyword);
    	params.put("draw", draw);
    	params.put("length", length);
    	params.put("companyId", companyId);
		return branchService.findByKeyword(keyword, pageRequest, params);
	}

	@RequestMapping(value="/new", method=RequestMethod.POST)
	public @ResponseBody MessageBean newBranch(@Valid @RequestBody BranchForm form, 
			Authentication authentication, BindingResult result, RedirectAttributes redirectAttributes) {
		LOGGER.info("Adding a new branch");
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
			branchService.create(form, user);
			redirectAttributes.addFlashAttribute("message", "Branch has been added successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while add branch", e);
			redirectAttributes.addFlashAttribute("message", "Got error while adding branch");
			return new MessageBean("error", "Got error while adding branch");
		}
        
        return new MessageBean("success", "Branch has created successfully");
	}
	
	@RequestMapping(value="/data/{uniqueId}", method=RequestMethod.GET)
	public @ResponseBody BranchBean getData(@PathVariable("uniqueId") String uniqueId, ModelMap modelMap) {
		LOGGER.info("Finding the branch details");
		return branchService.findDetail(uniqueId);
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public @ResponseBody MessageBean update(@Valid @RequestBody BranchBean branchBean, BindingResult result, RedirectAttributes redirectAttributes) {
		LOGGER.info("Update the branch details");
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

        try {
			branchService.update(branchBean);
			redirectAttributes.addFlashAttribute("message", "Branch has been updated successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while updating branch", e);
			redirectAttributes.addFlashAttribute("message", "Got error while updating branch");
			return new MessageBean("error", "Got error while updating branch");
		}
        
        return new MessageBean("success", "Branch has been updated successfully.");
	}
	
	@RequestMapping(value = "/delete/{uniqueId}", method = RequestMethod.DELETE)
	public @ResponseBody MessageBean delete(@PathVariable("uniqueId") String uniqueId) {
		LOGGER.info("Deleting the branch details");
		branchService.delete(uniqueId);
		return new MessageBean("success", "Branch has been deleted successfully");
	}
}
