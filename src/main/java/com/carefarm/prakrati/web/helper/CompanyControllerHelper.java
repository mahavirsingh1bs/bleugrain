package com.carefarm.prakrati.web.helper;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.vo.CompanyAdminVO;

@Component
public class CompanyControllerHelper {

	@Autowired
	private DozerBeanMapper mapper;
	
	public List<User> convertAdminVOToEntity(Set<CompanyAdminVO> admins) {
		List<User> companyAdmins = new ArrayList<>();
		for (CompanyAdminVO admin : admins) {
			companyAdmins.add(mapper.map(admin, User.class));
		}
		return companyAdmins;
	}
	
	public Set<CompanyAdminVO> convertAdminEntityToVO(List<User> companyAdmins) {
		Set<CompanyAdminVO> admins = new HashSet<CompanyAdminVO>();
		for (User admin : companyAdmins) {
			admins.add(mapper.map(admin, CompanyAdminVO.class));
		}
		return admins;
	}
}
