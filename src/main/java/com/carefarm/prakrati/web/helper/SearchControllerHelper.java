package com.carefarm.prakrati.web.helper;

import java.util.ArrayList;
import java.util.List;

import org.dozer.DozerBeanMapper;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.web.bean.UserBean;

public class SearchControllerHelper {
	
	public static List<UserBean> convertUserEntityToJson(List<User> users, DozerBeanMapper mapper) {
		List<UserBean> userBeans = new ArrayList<>();
		for (User user : users) {
			UserBean userBean = mapper.map(user, UserBean.class);
			userBeans.add(userBean);
		}
		return userBeans;
	}
}
