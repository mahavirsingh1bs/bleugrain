package com.carefarm.prakrati.web.util;

import org.springframework.data.domain.Sort.Direction;

public class PrakratiConstants {
	
	public static final int DEFAULT_PAGE_NO = 0;
	
	public static final int DEFAULT_PAGE_SIZE = 20;
	
	public static final Direction DEFAULT_DIRECTION = Direction.DESC;
	
	public static final String[] DEFAULT_SORT = { "dateModified" };
	
	public static final String DEMAND_STATUS_OPEN = "Open";
	
	public static final String STATUS_ADDED_NEW = "Newly Added";
	
	public static final String SOLD_OUT_STATUS = "Sold Out";
	
	public static final String COMPANY_ID = "companyId";
	
	public static final String USER_ID = "userId";
	
	public static final String CATEGORY = "category";
	
	public static final String DEFAULT_LANGUAGE = "en";

	public static final String DEFAULT_LOCALE = "en_IN";
	
	public static final String DEFAULT_COUNTRY = "IN";
	
	public static final String COMPANY = "company";
	
	public static final String USER = "user";

	public static final String SELECTED = "selected";
	
	public static final String ASTERIK = "*";
	
	public static final String HYPHEN = "-";
	
	public static final String DOT = "\\.";
	
	public static final int FIRST = 0;
	
	public static final int SECOND = 1;
	
}
