package com.carefarm.prakrati.web.util;

public enum SortBy {
	ASC(1, "asc"), DESC(2, "desc");
	
	private int code;
	private String name;
	
	private SortBy(int code, String name) {
		this.code = code;
		this.name = name;
	}
	
	public int getCode() {
		return code;
	}
	public String getName() {
		return name;
	}
	
}
