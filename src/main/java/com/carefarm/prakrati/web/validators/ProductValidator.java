package com.carefarm.prakrati.web.validators;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.carefarm.prakrati.web.model.ProductForm;

public class ProductValidator implements Validator {

	@Autowired
	private javax.validation.Validator validator;
	
	@Override
	public boolean supports(Class<?> clazz) {
		return ProductForm.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		ProductForm form = (ProductForm )target;
		validator.validate(form, ProductForm.class);
	}

}
