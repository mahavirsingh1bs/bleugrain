package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AdvancedBookingBean extends AbstractBean {
	
	private String uniqueId;
	private String firstName;
	private String lastName;
	private String emailAddress;
	private String phoneNo;
	private String addressLine1;
	private String addressLine2;
	private String city;
	private StateBean state;
	private CountryBean country;
	private String zipcode;
		
}
