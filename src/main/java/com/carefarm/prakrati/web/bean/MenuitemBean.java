package com.carefarm.prakrati.web.bean;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.constants.Predicates;

@Getter
@Setter
public class MenuitemBean extends AbstractBean {
	private String uniqueId;
	private String name;
	private String keywords;
	private int displayIndex;
	private boolean active;
	private String categoryId;
	private CategoryBean category;
	private String parentMenuitemId;
	private MenuitemBean parentMenuitem;
	private List<MenuitemBean> childMenuitems;
	
	public void addChildMenuitem(MenuitemBean childMenuitem) {
		if (Predicates.isNull.test(this.childMenuitems)) {
			this.childMenuitems = new ArrayList<>();
		}
		this.childMenuitems.add(childMenuitem);
	}

	@Override
	public String toString() {
		return "MenuitemBean [uniqueId=" + uniqueId + ", name=" + name
				+ ", keywords=" + keywords + ", childMenuitems=" + childMenuitems + "]";
	}
	
}
