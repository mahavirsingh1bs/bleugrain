package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class PermitBean {
	private Long id;
	private String name;
	
	public PermitBean(String name) {
		this.name = name;
	}
	
}
