package com.carefarm.prakrati.web.bean;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderDetailsBean {
	private Long grainCartId;
	private BigDecimal subtotal;
	private BigDecimal shippingCost;
	private BigDecimal advancedPayment;
	private AddressBean billingAddress;
	private AddressBean shippingAddress;
	private BankAccountBean bankAccount;
	private CreditCardBean creditCard;
		
}
