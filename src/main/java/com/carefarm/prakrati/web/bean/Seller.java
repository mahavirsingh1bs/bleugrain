package com.carefarm.prakrati.web.bean;

import java.util.List;

public interface Seller {
	List<ProductBean> getProducts();
	void setProducts(List<ProductBean> products);
	void addProduct(ProductBean product);
}
