package com.carefarm.prakrati.web.bean;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductStatusBean implements Serializable {
	private static final long serialVersionUID = -4559649648111210154L;
	
	private Long id;
	private String code;
	private String status;
	
}
