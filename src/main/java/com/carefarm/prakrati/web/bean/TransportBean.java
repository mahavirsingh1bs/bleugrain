package com.carefarm.prakrati.web.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.constants.Predicates;

@Getter
@Setter
public class TransportBean extends AbstractBean {
	private String uniqueId;
	private String name;
	private String vehicleNo;
	private BigDecimal capacity;
	private UnitBean capacityUnit;
	private BigDecimal price;
	private String priceWithCurrency;
	private CurrencyBean priceCurrency;
	private UnitBean priceUnit;
	private List<Long> selectedPermitIds;
	private List<PermitBean> permits;
	private UserBean driver;
	private CompanyBean company;
	
	public void addSelectedPermitId(Long selectedPermitId) {
		if (Predicates.isNull.test(this.selectedPermitIds)) {
			this.selectedPermitIds = new ArrayList<>();
		}
		this.selectedPermitIds.add(selectedPermitId);
	}
	
	public void addPermit(PermitBean permit) {
		if (Predicates.isNull.test(this.permits)) {
			this.permits = new ArrayList<>();
		}
		this.permits.add(permit);
	}
	
}
