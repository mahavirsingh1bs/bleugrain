package com.carefarm.prakrati.web.bean;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AttributeBean implements Serializable {
	private static final long serialVersionUID = 2086877881381230810L;
	
	private String name;
	private String value;
	
	public AttributeBean() { }
	
	public AttributeBean(String name, String value) {
		this.name = name;
		this.value = value;
	}
	
}
