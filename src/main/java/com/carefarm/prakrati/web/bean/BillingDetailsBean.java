package com.carefarm.prakrati.web.bean;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BillingDetailsBean extends AbstractBean implements Serializable {
	
	private static final long serialVersionUID = 7886154029815255126L;
	
	private String uniqueId;
	private String owner;
	private boolean defaultBilling;
	private UserBean user;
	private CompanyBean company;
		
}
