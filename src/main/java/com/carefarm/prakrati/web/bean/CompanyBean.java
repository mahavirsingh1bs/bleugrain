package com.carefarm.prakrati.web.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.constants.Predicates;

@Getter
@Setter
public class CompanyBean extends AbstractBean implements Serializable {
	
	private static final long serialVersionUID = 8244612160332515996L;
	
	private String uniqueId;
	private String name;
	private String businessCategory;
	private String shortDesc;
	private String registrNo;
	private String phoneNo;
	private String emailId;
	private AddressBean address;
	private String companyLogo;
	private ImageBean defaultImage;
	private AddressEntityBean defaultBillingAddress;
	private List<AddressEntityBean> billingAddresses;
	private AddressEntityBean defaultDeliveryAddress;
	private List<AddressEntityBean> deliveryAddresses;
	private List<CompanyAdminBean> admins;
	private List<AgentBean> agents;
	private List<PaymentMethodBean> paymentMethods;
	private List<BankAccountBean> bankAccounts;
	private List<CreditCardBean> creditCards;
	private List<BranchBean> branches;
	private List<ExpertiseBean> expertises;
	private List<SocialAccountBean> socialAccounts;
	
	public CompanyBean() { }
	
	public CompanyBean(String name) {
		this.name = name;
	}
	
	public CompanyBean(String name, String registrNo, String phoneNo, 
			String emailId, AddressBean address) {
		this.name = name;
		this.registrNo = registrNo;
		this.phoneNo = phoneNo;
		this.emailId = emailId;
		this.address = address;
	}

	public void addBillingAddress(AddressEntityBean billingAddress) {
		if (Predicates.isNull.test(this.billingAddresses)) {
			this.billingAddresses = new ArrayList<>();
		}
		this.billingAddresses.add(billingAddress);
	}
	
	public void addDeliveryAddress(AddressEntityBean deliveryAddress) {
		if (Predicates.isNull.test(this.deliveryAddresses)) {
			this.deliveryAddresses = new ArrayList<>();
		}
		this.deliveryAddresses.add(deliveryAddress);
	}
		
	public void addAdmin(CompanyAdminBean admin) {
		if (this.admins == null) {
			this.admins = new ArrayList<>();
		}
		this.admins.add(admin);
	}
	
	public void addAgent(AgentBean agent) {
		if (this.agents == null) {
			this.agents = new ArrayList<>();
		}
		this.agents.add(agent);
	}

	public void addPaymentMethod(PaymentMethodBean paymentMethod) {
		if (this.paymentMethods == null) {
			this.paymentMethods = new ArrayList<>();
		}
		this.paymentMethods.add(paymentMethod);
	}
	
	public void addBranch(BranchBean branch) {
		if (this.branches == null) {
			this.branches = new ArrayList<>();
		}
		this.branches.add(branch);
	}

	public void addExpertise(ExpertiseBean expertise) {
		if (Predicates.isNull.test(this.expertises)) {
			this.expertises = new ArrayList<>();
		}
		this.expertises.add(expertise);
	}

	public void addSocialAccount(SocialAccountBean socialAccount) {
		if (Predicates.isNull.test(this.socialAccounts)) {
			this.socialAccounts = new ArrayList<>();
		}
		this.socialAccounts.add(socialAccount);
	}
	
}
