package com.carefarm.prakrati.web.bean;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LanguageBean implements Serializable {
	private static final long serialVersionUID = -3586300267925390765L;
	
	private Long id;
	private String code;
	private String name;
	
	public LanguageBean() { }
	
	public LanguageBean(Long id, String name) { 
		this.id = id;
		this.name = name;
	}
	
}
