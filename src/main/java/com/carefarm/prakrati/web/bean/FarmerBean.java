package com.carefarm.prakrati.web.bean;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FarmerBean extends UserBean implements Seller {
	private static final long serialVersionUID = -8374558439837622773L;
	
	private List<ProductBean> products;
	private List<BankAccountBean> bankAccounts;
	
	public FarmerBean() { }
	
	public FarmerBean(String firstName, String lastName, 
			String emailId, String mobileNo, AddressBean address) {
		super(firstName, lastName, emailId, mobileNo);
		this.setAddress(address);
	}

	public void addProduct(ProductBean product) {
		if (this.products == null) {
			this.products = new ArrayList<>();
		}
		this.products.add(product);
	}
	
}
