package com.carefarm.prakrati.web.bean;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoleBean extends AbstractBean implements Serializable {
	
	private static final long serialVersionUID = -6417716424045648572L;
	
	private long id;
	private String name;
	private boolean frozen;
	
	public RoleBean() { }
	
	public RoleBean(long id, String name) {
		this.id = id;
		this.name = name;
	}
	
}
