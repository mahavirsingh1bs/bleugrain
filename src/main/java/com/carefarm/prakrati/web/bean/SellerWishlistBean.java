package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SellerWishlistBean {
	private Long demandId;
	private Long sellerId;
	private DemandBean demand;
	private UserBean seller;
		
}
