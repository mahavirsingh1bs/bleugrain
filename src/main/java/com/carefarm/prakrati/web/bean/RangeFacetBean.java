package com.carefarm.prakrati.web.bean;

import java.math.BigDecimal;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import org.apache.commons.lang3.StringUtils;

@Getter
@Setter
public class RangeFacetBean implements Comparable<RangeFacetBean> {
	private int id;
	private String name;
	
	@Setter(AccessLevel.NONE)
	private String startsFrom;
	
	@Setter(AccessLevel.NONE)
	private String endsWith;
	
	private int count;
	
	public RangeFacetBean() { }
	
	public RangeFacetBean(int id, String name, String startsFrom, String endsWith, int count) {
		this.id = id;
		this.name = name;
		if (!startsFrom.isEmpty()) {
			this.startsFrom = startsFrom;
		} else {
			this.startsFrom = StringUtils.EMPTY;
		}
		
		if (!endsWith.isEmpty()) {
			this.endsWith = endsWith;
		} else {
			this.endsWith = StringUtils.EMPTY;
		}
		this.count = count;
	}
	
	public void setStartsFrom(String startsFrom) {
		if (!startsFrom.isEmpty()) {
			this.startsFrom = new BigDecimal(startsFrom).divide(BigDecimal.valueOf(100)).toString();
		} else {
			this.startsFrom = StringUtils.EMPTY;
		}
	}
	
	public void setEndsWith(String endsWith) {
		if (!endsWith.isEmpty()) {
			this.endsWith = new BigDecimal(endsWith).divide(BigDecimal.valueOf(100)).toString();
		} else {
			this.endsWith = StringUtils.EMPTY;
		}
	}
	
	@Override
	public String toString() {
		StringBuilder rangeBuilder = new StringBuilder();
		if (this.startsFrom != null) {
			rangeBuilder.append(this.startsFrom);
			if (this.endsWith != null) {
				rangeBuilder.append(" < " + this.endsWith);
			} else {
				rangeBuilder.append(" and Above");
			}
		} else {
			rangeBuilder.append(" below " + this.endsWith);
		}
		return rangeBuilder.toString();
	}

	@Override
	public int compareTo(RangeFacetBean o) {
		if (this.startsFrom == null || "".equals(this.startsFrom.trim())) {
			return -1;
		} else if (this.endsWith == null || "".equals(this.endsWith.trim())) {
			return 1;
		} else {
			return this.startsFrom.compareTo(o.startsFrom) != 0 ? 
					this.startsFrom.compareTo(o.startsFrom) : this.endsWith.compareTo(o.endsWith);
		}
	}
	
}
