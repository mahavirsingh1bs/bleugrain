package com.carefarm.prakrati.web.bean;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.constants.Predicates;

@Getter
@Setter
public class CompanyAdminBean extends UserBean {
	
	private static final long serialVersionUID = -2672827272994550992L;
	
	private List<OrderBean> orders;
	private List<BidBean> bids;
	private List<DemandBean> demands;

	public void addDemand(DemandBean demand) {
		if (Predicates.isNull.test(this.demands)) {
			this.demands = new ArrayList<>();
		}
		this.demands.add(demand);
	}

	public void addOrder(OrderBean order) {
		if (Predicates.isNull.test(this.orders)) {
			this.orders = new ArrayList<>();
		}
		this.orders.add(order);
	}
	
	public void addBid(BidBean bid) {
		if (Predicates.isNull.test(this.bids)) {
			this.bids = new ArrayList<>();
		}
		this.bids.add(bid);
	}
	
}
