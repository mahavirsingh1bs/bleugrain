package com.carefarm.prakrati.web.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.entity.Review;

@Getter
@Setter
public class CategoryBean extends AbstractBean implements Serializable {
	private static final long serialVersionUID = -4941350130817157244L;
	
	private Long id;
	private String uniqueId;
	private String name;
	private String desc;
	private ImageBean defaultImage;
	private String parentCategoryId;
	private CategoryBean parentCategory;
	private List<CategoryBean> childCategories;
	private List<Review> reviews;
	
	public CategoryBean() { }
	
	public CategoryBean(String name) {
		this.name = name;
	}
	
	public CategoryBean(long id, String name) {
		this.id = id;
		this.name = name;
	}
		
	public void addChildCategory(CategoryBean childCategory) {
		if (this.childCategories == null) {
			this.childCategories = new ArrayList<>();
		}
		this.childCategories.add(childCategory);
	}
	
}
