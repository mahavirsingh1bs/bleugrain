package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WishlistByCategoryBean {
	private CategoryBean category;
	private long total;
	
	public WishlistByCategoryBean() { }

	public WishlistByCategoryBean(CategoryBean category, long total) {
		this.category = category;
		this.total = total;
	}
	
}