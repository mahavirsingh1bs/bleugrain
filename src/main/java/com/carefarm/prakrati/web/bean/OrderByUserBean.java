package com.carefarm.prakrati.web.bean;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderByUserBean {
	private UserBean user;
	private int totalOrders;
	private List<OrderBean> orders;
	
	public OrderByUserBean() { }
	
	public OrderByUserBean(UserBean user, int totalOrders, List<OrderBean> orders) {
		this.user = user;
		this.totalOrders = totalOrders;
		this.orders = orders;
	}
		
}
