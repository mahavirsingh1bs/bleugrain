package com.carefarm.prakrati.web.bean;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.constants.Predicates;

@Getter
@Setter
public class WishlistBean extends AbstractBean {
	private Long id;
	private int totalItems;
	private List<ProductBean> products;
	private List<DemandBean> demands;
	private UserBean user;
	private CompanyBean company;
	
	public void addProduct(ProductBean product) {
		if (Predicates.isNull.test(this.products)) {
			this.products = new ArrayList<>();
		}
		this.products.add(product);
	}

	public void addDemand(DemandBean demand) {
		if (Predicates.isNull.test(this.demands)) {
			this.demands = new ArrayList<>();
		}
		this.demands.add(demand);
	}
		
}
