package com.carefarm.prakrati.web.bean;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.constants.Predicates;

@Getter
@Setter
public class GrainCartBean extends AbstractBean {
	private Long id;
	private int totalItems;
	private String subtotal;
	private String shippingCost;
	private String totalCost;
	private List<ProductBean> products;
	private List<ItemBean> items;
	private UserBean user;
	private CompanyBean company;
	
	public void addProduct(ProductBean product) {
		if (Predicates.isNull.test(this.products)) {
			this.products = new ArrayList<>();
		}
		this.products.add(product);
	}
	public void addItem(ItemBean item) {
		if (Predicates.isNull.test(this.items)) {
			this.items = new ArrayList<>();
		}
		this.items.add(item);
	}
		
}
