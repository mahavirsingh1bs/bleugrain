package com.carefarm.prakrati.web.bean;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OwnerBean implements Serializable {
	private static final long serialVersionUID = 9108220589238095890L;
	
	private Long id;
	private String userNo;
	private String firstName;
	private String lastName;
	private String emailId;
	private boolean emailVerified;
	private String phoneNo;
	private String mobileNo;
	private boolean mobileVerified;
	private boolean verified;
	private String photo;
	private AddressBean address;
	
	public String getFullName() {
		return this.firstName + " " + this.lastName;
	}

}
