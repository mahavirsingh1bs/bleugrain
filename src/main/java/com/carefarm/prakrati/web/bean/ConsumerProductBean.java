package com.carefarm.prakrati.web.bean;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.entity.Review;

@Getter
@Setter
public class ConsumerProductBean extends AbstractBean {
	
	private Long id;
	private String name;
	private String desc;
	private ImageBean defaultImage;
	private CategoryBean category;
	private List<TagBean> tags;
	private BigDecimal totalRating;
	private int totalReviews;
	private List<Review> reviews;
	
	public void addTag(TagBean tag) {
		if (Predicates.isNull.test(this.tags)) {
			this.tags = new ArrayList<>();
		}
		this.tags.add(tag);
	}
		
}
