package com.carefarm.prakrati.web.bean;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BidByUserBean {
	private UserBean user;
	private List<ProductBean> bidProducts;
	private List<ProductBean> wonProducts;
	
	public BidByUserBean() { }
	
	public BidByUserBean(UserBean user, List<ProductBean> bidProducts, List<ProductBean> wonProducts) {
		super();
		this.user = user;
		this.bidProducts = bidProducts;
		this.wonProducts = wonProducts;
	}
	
}
