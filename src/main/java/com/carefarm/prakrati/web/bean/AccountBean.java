package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AccountBean {
	private Long id;
	private String accountNo;
	private String bankName;
	private String isfcCode;
	private boolean isPrimary;
	
	public AccountBean() { }
	
	public AccountBean(Long id, String accountNo, String bankName, 
			String isfcCode, boolean isPrimary) { 
		this.id = id;
		this.accountNo = accountNo;
		this.bankName = bankName;
		this.isfcCode = isfcCode;
		this.isPrimary = isPrimary;
	}
	
}
