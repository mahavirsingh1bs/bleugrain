package com.carefarm.prakrati.web.bean;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BidBean extends AbstractBean implements Serializable {
	private static final long serialVersionUID = -8638737545064021146L;
	
	private Long id;
	private UserBean user;
	private CompanyBean company;
	private String uniqueId;
	private ProductBean product;
	private BigDecimal price;
	private CurrencyBean currency;
	private UnitBean unit;
	
}
