package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NotificationCategoryBean extends AbstractBean {
	private long id;
	private String name;
		
}
