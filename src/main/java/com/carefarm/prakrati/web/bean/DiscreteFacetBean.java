package com.carefarm.prakrati.web.bean;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DiscreteFacetBean implements Serializable {
	
	private static final long serialVersionUID = 3464437409340686399L;
	
	private int id;
	private String name;
	private int count;
	
	public DiscreteFacetBean() { }
	
	public DiscreteFacetBean(String name, int count) {
		this.name = name;
		this.count = count;
	}
	
	public DiscreteFacetBean(int id, String name, int count) {
		this.id = id;
		this.name = name;
		this.count = count;
	}
	
}
