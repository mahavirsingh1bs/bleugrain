package com.carefarm.prakrati.web.bean;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductEventBean implements Serializable {
	private static final long serialVersionUID = -8643392401157578533L;
	
	private Long id;
	private String uniqueId;
	private ProductBean product;
	private ProductStatusBean productStatus;
	private UserBean eventBy;
	private String eventDate;
		
}
