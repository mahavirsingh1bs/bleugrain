package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.Setter;

import org.springframework.data.domain.Sort.Direction;

@Getter
@Setter
public class SearchRequest {
	private String keyword;
	private String sortFields;
	private Direction direction;
	private int page;
	private int pageSize;
	private boolean history;
	private boolean consumerProducts;
	private boolean mainMenuitems;
	private boolean mainCategories;
	private String mainMenuitemId;
	private String mainCategoryId;
	private String categoryId;
	private String userNo;
	private String companyId;
	
}
