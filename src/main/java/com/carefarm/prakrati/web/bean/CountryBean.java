package com.carefarm.prakrati.web.bean;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CountryBean extends AbstractBean implements Serializable {
	private static final long serialVersionUID = 7757415603921849526L;
	
	private long id;
	private String code;
	private String name;
	
	public CountryBean() { }
	
	public CountryBean(long id, String name) { 
		this.id = id;
		this.name = name;
	}
		
}
