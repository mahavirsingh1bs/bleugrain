package com.carefarm.prakrati.web.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.constants.Predicates;

@Getter
@Setter
public class ProductBean extends AbstractBean implements Serializable {
	
	private static final long serialVersionUID = -5367760611270518332L;
	
	private Long id;
	private String uniqueId;
	private String name;
	private String shortDesc;
	private String desc;
	private BigDecimal quantity;
	private UnitBean quantityUnit;
	private BigDecimal price;
	private String priceWithCurrency;
	private CurrencyBean priceCurrency;
	private UnitBean priceUnit;
	private String totalPrice;
	private AddressBean address;
	private boolean recent;
	private List<BidBean> bids;
	private boolean bidEnabled;
	private String enableTillDate;
	private BidBean highestBid;
	private BigDecimal initialBidAmount;
	private CurrencyBean biddingCurrency;
	private UnitBean biddingUnit;
	private ProductStatusBean status;
	private OwnerBean owner;
	private String ownerType;
	private boolean consumerProduct;
	private List<String> categoryIds = new ArrayList<>();
	private List<CategoryBean> categories = new ArrayList<>();
	private ImageBean defaultImage;
	private List<ImageBean> images;
	private List<ProductEventBean> events;
	private List<TagBean> tags = new ArrayList<>();
	private List<AttributeBean> attributes = new ArrayList<>();
	private BigDecimal totalRating;
	private int totalReviews;
	private List<ReviewBean> reviews;
	private Long totalWishlists;
	private Long totalGrainCarts;
	private Long totalVisits;
	private UserBean assignedBy;
	private UserBean assignedTo;
	private String dateAssigned;
	private String comments;
	
	public void addBid(BidBean bid) {
		if (Predicates.isNull.test(this.bids)) {
			this.bids = new ArrayList<>();
		}
		this.bids.add(bid);
	}
	public void addCategoryId(String categoryId) {
		if (Predicates.isNull.test(this.categoryIds)) {
			this.categoryIds.add(categoryId);
		}
		this.categoryIds.add(categoryId);
	}
	public void addCategory(CategoryBean category) {
		if (Predicates.isNull.test(this.categories)) {
			this.categories = new ArrayList<>();
		}
		this.categories.add(category);
	}

	public void addImage(ImageBean image) {
		if (this.images == null) {
			this.images = new ArrayList<>();
		}
		this.images.add(image);
	}
	
	public void addTag(TagBean tag) {
		if (Predicates.isNull.test(this.tags)) {
			this.tags = new ArrayList<>();
		}
		this.tags.add(tag);
	}
	
	public void addAttribute(AttributeBean attribute) {
		if (Predicates.isNull.test(this.attributes)) {
			this.attributes = new ArrayList<>();
		}
		this.attributes.add(attribute);
	}
	
}
