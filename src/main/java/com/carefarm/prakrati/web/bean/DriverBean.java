package com.carefarm.prakrati.web.bean;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DriverBean extends UserBean {
	private static final long serialVersionUID = -1783551875146968766L;
	private List<LicenseBean> licenses;
	
}
