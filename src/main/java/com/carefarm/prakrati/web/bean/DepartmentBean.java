package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DepartmentBean extends AbstractBean {
	private long id;
	private String name;
	
	public DepartmentBean() { }
	
	public DepartmentBean(long id, String name) {
		this.id = id;
		this.name = name;
	}
	
}
