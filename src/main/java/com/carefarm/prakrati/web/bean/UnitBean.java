package com.carefarm.prakrati.web.bean;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UnitBean implements Serializable {
	private static final long serialVersionUID = -2903832641712344420L;
	
	private Long id;
	private String name;
	private String symbol;
	private String mneomic;
	private String countryCode;
	private boolean isDefault;
	
	public UnitBean() { }
	
	public UnitBean(Long id, String name) {
		this.id = id;
		this.name = name;
	}
		
}
