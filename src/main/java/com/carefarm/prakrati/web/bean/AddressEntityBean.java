package com.carefarm.prakrati.web.bean;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.util.AddressCategory;

@Getter
@Setter
public class AddressEntityBean extends AbstractBean implements Serializable {
	
	private static final long serialVersionUID = -2295948983264176623L;
	
	private Long id;
	private String firstName;
	private String lastName;
	private String emailAddress;
	private String phoneNo;
	private String alternatePhoneNo;
	private String addressLine1;
	private String addressLine2;
	private String city;
	private AddressCategory addressCategory;
	private StateBean state;
	private CountryBean country;
	private String zipcode;
	private boolean defaultAddress;
	private UserBean user;
	private CompanyBean company;
	
}
