package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CommentBean {

	private Long id;
	private String comment;
	private UserBean commentBy;
	private String commentDate;
	
}
