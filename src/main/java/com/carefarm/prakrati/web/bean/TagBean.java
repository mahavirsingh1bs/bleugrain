package com.carefarm.prakrati.web.bean;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class TagBean implements Serializable {
	private static final long serialVersionUID = -8342123277127211762L;
	
	private Long id;
	private String name;
	
	public TagBean() { }
	
	public TagBean(String name) {
		this.name = name;
	}	
}
