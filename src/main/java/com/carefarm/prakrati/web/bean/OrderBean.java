package com.carefarm.prakrati.web.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderBean extends AbstractBean implements Serializable {

	private static final long serialVersionUID = 3745859162612878411L;
	
	private Long id;
	private String orderNo;
	private List<ProductBean> products;
	private List<ItemBean> items;
	private String subtotal;
	private String shippingCost;
	private String totalPrice;
	private String orderDate;
	private UserBean assignedBy;
	private UserBean assignedTo;
	private String dateAssigned;
	private String comments;
	private UserBean deliverBy;
	private String deliveryDate;
	private AddressEntityBean billingAddress;
	private AddressEntityBean shippingAddress;
	private BigDecimal amountPaid;
	private String status;
	private UserBean user;
	private CompanyBean company;
	private CompanyBean fullfilledBy;
	
}
