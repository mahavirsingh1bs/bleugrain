package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DemandByUserBean {
	private UserBean user;
	private long total;
	
	public DemandByUserBean() { }
	
	public DemandByUserBean(UserBean user, long total) {
		this.user = user;
		this.total = total;
	}
		
}
