package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.Setter;

import org.springframework.data.domain.Sort;

@Getter
@Setter
public class SortOrder {
	private String name;
	private Sort.Direction direction;
		
}
