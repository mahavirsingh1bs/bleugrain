package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NoteBean extends AbstractBean {

	private String uniqueId;
	private String title;
	private String note;
	private UserBean noteBy;
	private String noteDate;
		
}
