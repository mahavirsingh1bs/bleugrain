package com.carefarm.prakrati.web.bean;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RetailerBean extends UserBean {
	private static final long serialVersionUID = -7045569997013871581L;
	
	private AddressBean address;
	private List<OrderBean> orders;
	private List<DemandBean> demands;
	private List<BankAccountBean> bankAccounts;
	private List<CreditCardBean> creditCards;
	private List<ProductBean> products;
	
	public RetailerBean() { }
	
	public RetailerBean(String firstName, String lastName, 
			String emailId, String mobileNo, AddressBean address) {
		super(firstName, lastName, emailId, mobileNo);
		this.address = address;
	}

	public void addProduct(ProductBean product) {
		if (this.products == null) {
			this.products = new ArrayList<>();
		}
		this.products.add(product);
	}
	
}
