package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductByCategoryBean {
	private CategoryBean category;
	private long total;
	
	public ProductByCategoryBean() { }
	
	public ProductByCategoryBean(CategoryBean category, long total) {
		this.category = category;
		this.total = total;
	}
	
}
