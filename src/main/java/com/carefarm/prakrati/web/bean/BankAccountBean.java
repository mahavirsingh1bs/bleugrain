package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BankAccountBean extends BillingDetailsBean {
	
	private static final long serialVersionUID = -7552491824819950678L;
	
	private BankBean bank;
	private String account;
	private String swift;
		
}
