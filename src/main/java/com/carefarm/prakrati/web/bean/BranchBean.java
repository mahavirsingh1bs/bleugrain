package com.carefarm.prakrati.web.bean;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BranchBean extends AbstractBean implements Serializable {
	
	private static final long serialVersionUID = -754235386820292894L;
	
	private Long id;
	private String name;
	private AddressBean address;
	
	public BranchBean() { }
	
	public BranchBean(Long id, String name) {
		this.id = id;
		this.name = name;
	}
	
	public BranchBean(Long id, String name, AddressBean address) {
		this.id = id;
		this.name = name;
		this.address = address;
	}
	
}
