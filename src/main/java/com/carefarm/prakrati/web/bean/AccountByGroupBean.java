package com.carefarm.prakrati.web.bean;

public class AccountByGroupBean {
	private GroupBean group;
	private long total;
	
	public AccountByGroupBean() { }
	
	public AccountByGroupBean(GroupBean group, long total) {
		super();
		this.group = group;
		this.total = total;
	}
	public GroupBean getGroup() {
		return group;
	}
	public void setGroup(GroupBean group) {
		this.group = group;
	}
	public long getTotal() {
		return total;
	}
	public void setTotal(long total) {
		this.total = total;
	}
	
}
