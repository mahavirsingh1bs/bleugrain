package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BidByCategoryBean {
	private CategoryBean category;
	private long total;
	
	public BidByCategoryBean() { }
	
	public BidByCategoryBean(CategoryBean category, long total) {
		this.category = category;
		this.total = total;
	}
		
}
