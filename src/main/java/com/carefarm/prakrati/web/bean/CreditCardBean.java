package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.web.payment.bean.ExpiryMonthBean;
import com.carefarm.prakrati.web.payment.bean.ExpiryYearBean;

@Getter
@Setter
public class CreditCardBean extends BillingDetailsBean {
	
	private static final long serialVersionUID = -2856785465950008818L;
	
	private PaymentCardBean paymentCard;
	private String number;
	private ExpiryMonthBean expiryMonth;
	private ExpiryYearBean expiryYear;
	private String csv;
	
}
