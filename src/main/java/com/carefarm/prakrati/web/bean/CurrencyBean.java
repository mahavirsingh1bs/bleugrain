package com.carefarm.prakrati.web.bean;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CurrencyBean implements Serializable {
	
	private static final long serialVersionUID = -2745658856560887708L;
	
	private Long id;
	private String name;
	private String symbol;
	private String mneomic;
	private String countryCode;
	
	public CurrencyBean() { }
	
	public CurrencyBean(Long id, String name) {
		this.id = id;
		this.name = name;
	}
		
}
