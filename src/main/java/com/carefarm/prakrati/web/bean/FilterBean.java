package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FilterBean implements Comparable<FilterBean> {
	private Integer id;
	private String name;
	
	public FilterBean() { }
	
	public FilterBean(Integer id, String name) {
		this.id = id;
		this.name = name;
	}
	
	@Override
	public int compareTo(FilterBean o) {
		return o.id.compareTo(this.id);
	}
	
}
