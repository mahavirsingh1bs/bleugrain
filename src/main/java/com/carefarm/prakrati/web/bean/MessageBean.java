package com.carefarm.prakrati.web.bean;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;

@Getter
@Setter
public class MessageBean implements Serializable {
	private static final long serialVersionUID = 7990165268025630086L;
	
	private String code;
	private String message;
	private List<FieldError> fieldErrors;
	private List<ObjectError> globalErrors;
	
	public MessageBean(String code, String message) {
		this.code = code;
		this.message = message;
	}
	
	public MessageBean(String code, List<FieldError> fieldErrors) {
		this.code = code;
		this.fieldErrors = fieldErrors;
	}
	
	public MessageBean(String code, String message, List<FieldError> fieldErrors) {
		this.code = code;
		this.message = message;
		this.fieldErrors = fieldErrors;
	}
	
	public MessageBean(String code, List<FieldError> fieldErrors, List<ObjectError> globalErrors) {
		this.code = code;
		this.fieldErrors = fieldErrors;
		this.globalErrors = globalErrors;
	}
	
	public MessageBean(String code, String message, List<FieldError> fieldErrors, List<ObjectError> globalErrors) {
		this.code = code;
		this.message = message;
		this.fieldErrors = fieldErrors;
		this.globalErrors = globalErrors;
	}
	
}
