package com.carefarm.prakrati.web.bean;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.constants.Predicates;

@Getter
@Setter
public class DemandBean extends AbstractBean implements Serializable {

	private static final long serialVersionUID = -4274329588480781713L;
	
	private String uniqueId;
	private String name;
	private String categoryId;
	private CategoryBean category;
	private String quantity;
	private UnitBean quantityUnit;
	private BigDecimal minPricePerUnit;
	private BigDecimal maxPricePerUnit;
	private BigDecimal price;
	private String priceWithCurrency;
	private CurrencyBean priceCurrency;
	private UnitBean priceUnit;
	private String status;
	private boolean recent;
	private ImageBean defaultImage;
	private String deliveryDate;
	private List<TagBean> tags;
	private AddressBean deliveryAddress;
	private String requestDate;
	private UserBean user;
	private String custType;
	private CompanyBean company;
	private UserBean assignedBy;
	private UserBean assignedTo;
	private String dateAssigned;
	private String comments;

	public void addTag(TagBean tag) {
		if (Predicates.isNull.test(this.tags)) {
			this.tags = new ArrayList<>();
		}
		this.tags.add(tag);
	}

}
