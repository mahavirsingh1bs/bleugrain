package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WishlistByUserBean {
	private UserBean user;
	private long total;
	
	public WishlistByUserBean() { }
	
	public WishlistByUserBean(UserBean user, long total) {
		this.user = user;
		this.total = total;
	}
		
}
