package com.carefarm.prakrati.web.bean;

import java.util.HashSet;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.constants.Predicates;

@Getter
@Setter
public class NotificationBean extends AbstractBean {

	private Long id;
	private String code;
	private String category;
	private String notification;
	private Set<GroupBean> groups;
	private Set<CompanyBean> companies;
	private Set<UserBean> users;
	private String startDate;
	private String expiryDate;

	public void addGroup(GroupBean group) {
		if (Predicates.isNull.test(this.groups)) {
			this.groups = new HashSet<>();
		}
		this.groups.add(group);
	}
	
	public void addCompany(CompanyBean company) {
		if (Predicates.isNull.test(this.companies)) {
			this.companies = new HashSet<>();
		}
		this.companies.add(company);
	}
	
	public void addUser(UserBean user) {
		if (Predicates.isNull.test(this.users)) {
			this.users = new HashSet<>();
		}
		this.users.add(user);
	}
	
}
