package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AbstractBean {
	private UserBean createdBy;
	private String createdOn;
	private UserBean modifiedBy;
	private String modifiedOn;
	
}
