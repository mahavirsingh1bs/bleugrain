package com.carefarm.prakrati.web.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.constants.Predicates;

@Getter
@Setter
public class GroupBean extends AbstractBean implements Serializable {
	
	private static final long serialVersionUID = 2337320642829561833L;
	
	private long id;
	private String name;
	private List<RoleBean> roles;
	private List<Long> selectedRoleIds;
	private List<RoleBean> selectedRoles;
	
	public GroupBean() { }
	
	public GroupBean(long id, String name) {
		this.id = id;
		this.name = name;
	}
		
	public void addRole(RoleBean role) {
		if (this.roles == null) {
			this.roles = new ArrayList<>();
		}
		this.roles.add(role);
	}

	public void addSelectedRoleId(Long selectedRoleId) {
		if (Predicates.isNull.test(this.selectedRoleIds)) {
			this.selectedRoleIds = new ArrayList<>();
		}
		this.selectedRoleIds.add(selectedRoleId);
	}
		
	public void addSelectedRole(RoleBean selectedRole) {
		if (Predicates.isNull.test(selectedRoles)) {
			this.selectedRoles = new ArrayList<>();
		}
		this.selectedRoles.add(selectedRole);
	}
	
}
