package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DesignationBean extends AbstractBean {
	private long id;
	private String name;
	
	public DesignationBean() { }
	
	public DesignationBean(long id, String name) {
		this.id = id;
		this.name = name;
	}

}
