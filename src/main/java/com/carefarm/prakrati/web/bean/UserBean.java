package com.carefarm.prakrati.web.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.constants.Predicates;

@Getter
@Setter
public class UserBean extends AbstractBean implements Serializable {
	
	private static final long serialVersionUID = -1323529672651933121L;
	
	private String userNo;
	private String firstName;
	private String lastName;
	private String shortDesc;
	private String emailId;
	private boolean emailVerified;
	private String phoneNo;
	private String mobileNo;
	private String lastLoginOn;
	private boolean mobileVerified;
	private String uniqueCode;
	private String defaultLocale;
	private LanguageBean defaultLanguage;
	private CountryBean defaultCountry;
	private ImageBean defaultImage;
	private AddressBean address;
	private boolean transportAvailable;
	private AddressEntityBean defaultBillingAddress;
	private List<AddressEntityBean> billingAddresses;
	private AddressEntityBean defaultDeliveryAddress;
	private List<AddressEntityBean> deliveryAddresses;
	private GroupBean group;
	private List<RoleBean> roles;
	private Long companyId;
	private CompanyBean company;
	private String businessCategory;
	
	public UserBean() { }
	
	public UserBean(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	public UserBean(String firstName, String lastName, String emailId, String mobileNo) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.emailId = emailId;
		this.mobileNo = mobileNo;
	}

	public String getFullName() {
		return this.firstName + " " + this.lastName;
	}

	public void addBillingAddress(AddressEntityBean billingAddress) {
		if (Predicates.isNull.test(this.billingAddresses)) {
			this.billingAddresses = new ArrayList<>();
		}
		this.billingAddresses.add(billingAddress);
	}
	
	public void addDeliveryAddress(AddressEntityBean deliveryAddress) {
		if (Predicates.isNull.test(this.deliveryAddresses)) {
			this.deliveryAddresses = new ArrayList<>();
		}
		this.deliveryAddresses.add(deliveryAddress);
	}
	
	public void addRole(RoleBean role) {
		if (Predicates.isNull.test(this.roles)) {
			this.roles = new ArrayList<>();
		}
		this.roles.add(role);
	}
	
}
