package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WishlistByProductBean {
	private ProductBean product;
	private long total;
	
	public WishlistByProductBean() { }
	
	public WishlistByProductBean(ProductBean product, long total) {
		this.product = product;
		this.total = total;
	}
		
}
