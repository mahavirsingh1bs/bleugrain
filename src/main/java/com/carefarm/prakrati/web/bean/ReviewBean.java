package com.carefarm.prakrati.web.bean;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReviewBean implements Serializable {

	private static final long serialVersionUID = -2223521411448489311L;
	private String name;
	private String emailAddress;
	private BigDecimal rating;
	private String comment;
	private Boolean valid;
	private UserBean reviewBy;
	private String reviewerType;
	private String reviewDate;
	
}
