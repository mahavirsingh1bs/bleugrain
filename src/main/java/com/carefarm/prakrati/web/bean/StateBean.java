package com.carefarm.prakrati.web.bean;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StateBean extends AbstractBean implements Serializable {
	
	private static final long serialVersionUID = 4721401959846192126L;
	
	private long id;
	private String name;
	private Long countryId;
	private CountryBean country;
	
	public StateBean() { }
	
	public StateBean(long id, String name) {
		this.id = id;
		this.name = name;
	}

}
