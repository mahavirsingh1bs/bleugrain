package com.carefarm.prakrati.web.bean;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DiscussionBean extends AbstractBean {
	
	private Long id;
	private String title;
	private String description;
	private List<CommentBean> comments;
	private List<TagBean> tags;
	private UserBean user;
	private String initiatedOn;
	private String lastCommentOn;
	
}
