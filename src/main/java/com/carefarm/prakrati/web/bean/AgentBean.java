package com.carefarm.prakrati.web.bean;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.constants.Predicates;

@Getter
@Setter
public class AgentBean extends UserBean {
	
	private static final long serialVersionUID = 2138125578506964798L;
	
	private List<DemandBean> demands;
	private List<BidBean> bids;
	private List<OrderBean> orders;
	
	public void addDemand(DemandBean demand) {
		if (Predicates.isNull.test(this.demands)) {
			this.demands = new ArrayList<>();
		}
		this.demands.add(demand);
	}
	
	public void addOrder(OrderBean order) {
		if (Predicates.isNull.test(this.orders)) {
			this.orders = new ArrayList<>();
		}
		this.orders.add(order);
	}
		
	public void addBid(BidBean bid) {
		if (Predicates.isNull.test(this.bids)) {
			this.bids = new ArrayList<>();
		}
		this.bids.add(bid);
	}
	
}
