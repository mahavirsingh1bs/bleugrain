package com.carefarm.prakrati.web.bean;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderStatusBean implements Serializable {
	private static final long serialVersionUID = 2180870130848629832L;
	
	private Long id;
	private String code;
	private String status;
	
	public OrderStatusBean() { }
	
	public OrderStatusBean(Long id, String code) { 
		this.id = id;
		this.code = code;
	}
		
}
