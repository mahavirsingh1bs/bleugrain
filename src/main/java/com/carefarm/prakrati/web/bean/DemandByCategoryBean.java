package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DemandByCategoryBean {
	private CategoryBean category;
	private long total;
	
	public DemandByCategoryBean() { }

	public DemandByCategoryBean(CategoryBean category, long total) {
		this.category = category;
		this.total = total;
	}
		
}
