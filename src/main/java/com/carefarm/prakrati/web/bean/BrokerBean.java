package com.carefarm.prakrati.web.bean;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BrokerBean extends UserBean {
	private static final long serialVersionUID = -4144708377573279410L;
	
	private String licenseNo;
	private List<OrderBean> orders;
	private List<DemandBean> demands;
	private List<ProductBean> products;
	private List<BankAccountBean> bankAccounts;
	private List<CreditCardBean> creditCards;
	
	public BrokerBean() { }
	
	public BrokerBean(String firstName, String lastName, 
			String emailId, String mobileNo, AddressBean address) {
		super(firstName, lastName, emailId, mobileNo);
	}

	public void addProduct(ProductBean product) {
		if (this.products == null) {
			this.products = new ArrayList<>();
		}
		this.products.add(product);
	}
	
}
