package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BidsRequest {
	private String uniqueId;
	private String userNo;
	private String companyId;
	private int page;
	private int pageSize;
		
}
