package com.carefarm.prakrati.web.bean;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentMethodBean extends AbstractBean implements Serializable {
	
	private static final long serialVersionUID = -8131599056000830321L;
	
	private Long id;
	private String name;
	private boolean primary;
	
	public PaymentMethodBean() { }
	
	public PaymentMethodBean(Long id, String name, boolean primary) {
		this.id = id;
		this.name = name;
		this.primary = primary;
	}
		
}
