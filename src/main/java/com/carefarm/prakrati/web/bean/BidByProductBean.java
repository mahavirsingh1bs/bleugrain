package com.carefarm.prakrati.web.bean;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BidByProductBean {
	private ProductBean product;
	private BigDecimal initialBidAmount;
	private CurrencyBean biddingCurrency;
	private UnitBean biddingUnit;
	private BidBean highestBid;
	
	public BidByProductBean() { }
	
	public BidByProductBean(ProductBean product) {
		this.product = product;
	}
	
}
