package com.carefarm.prakrati.web.bean;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomerBean extends UserBean {
	private static final long serialVersionUID = 2444043868120291363L;
	
	private List<OrderBean> orders;
	private List<DemandBean> demands;
	private List<CreditCardBean> creditCards;
		
}
