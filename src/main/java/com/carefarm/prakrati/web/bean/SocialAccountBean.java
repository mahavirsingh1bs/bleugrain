package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.util.SocialSite;

@Getter
@Setter
public class SocialAccountBean {
	private String uniqueId;
	private String displayName;
	private String username;
	private String accountUrl;
	private SocialSite socialSite;
	
}
