package com.carefarm.prakrati.web.bean;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ItemBean implements Serializable {
	private static final long serialVersionUID = 6392976671955298380L;
	
	private Long id;
	private String uniqueId;
	private String name;
	private BigDecimal quantity;
	private UnitBean quantityUnit;
	private String price;
	private String totalPrice;
	private CurrencyBean priceCurrency;
	private UnitBean priceUnit;
	private ImageBean image;
	
}
