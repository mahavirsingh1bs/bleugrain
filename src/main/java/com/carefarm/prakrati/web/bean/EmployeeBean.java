package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class EmployeeBean extends UserBean {
	private static final long serialVersionUID = -7860615758855693602L;
	
	private String employeeId;
	private Long designationId;
	private DesignationBean designation;
	private Long departmentId;
	private DepartmentBean department;
	
	public EmployeeBean() { }
	
	public EmployeeBean(String firstName, String lastName, String employeeId) {
		super(firstName, lastName);
		this.employeeId = employeeId;
	}
	
	public EmployeeBean(String firstName, String lastName, 
			String employeeId, Long deptId, String deptName, Long desigId, 
			String desigName) {
		super(firstName, lastName);
		this.employeeId = employeeId;
		this.department = new DepartmentBean(deptId, deptName);
		this.designation = new DesignationBean(desigId, desigName);
	}
	
	public EmployeeBean(String firstName, String lastName, 
			String emailId, String mobileNo, String employeeId, Long deptId, 
			String deptName, Long desigId, String desigName) {
		super(firstName, lastName, emailId, mobileNo);
		this.employeeId = employeeId;
		this.department = new DepartmentBean(deptId, deptName);
		this.designation = new DesignationBean(desigId, desigName);
	}
		
}
