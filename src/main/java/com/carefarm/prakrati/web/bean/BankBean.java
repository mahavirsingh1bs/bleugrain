package com.carefarm.prakrati.web.bean;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BankBean extends AbstractBean implements Serializable {
	
	private static final long serialVersionUID = 307768180003939232L;
	
	private long id;
	private String name;
	
	public BankBean() { }
	
	public BankBean(long id, String name) {
		this.id = id;
		this.name = name;
	}

}
