package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BuyerWishlistBean {
	private Long productId;
	private Long buyerId;
	private ProductBean product;
	private UserBean buyer;
	
}
