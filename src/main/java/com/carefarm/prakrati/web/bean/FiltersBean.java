package com.carefarm.prakrati.web.bean;

import java.util.Map;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FiltersBean {
	private Map<String, String> sellers;
	private Map<String, String> categories;
	private Map<String, String> quantities;
	private Map<String, String> prices;
	private Map<String, String> reviews;
	
}
