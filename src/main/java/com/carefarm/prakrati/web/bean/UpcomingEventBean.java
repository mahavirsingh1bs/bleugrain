package com.carefarm.prakrati.web.bean;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpcomingEventBean extends AbstractBean {

	private String uniqueId;
	private String event;
	private String eventDetail;
	private UserBean organiser;
	private Date eventDate;

}
