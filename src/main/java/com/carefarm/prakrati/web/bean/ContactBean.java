package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ContactBean extends AbstractBean {
	private UserBean user;
	private String area;
	private String message;
	private CountryBean country;
	
}
