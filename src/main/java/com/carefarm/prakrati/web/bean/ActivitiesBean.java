package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ActivitiesBean {
	private long totalOrders;
	private long totalProducts;
	private long totalDemands;
	private long totalSellingProducts;
	private long totalBuyingDemands;
		
}
