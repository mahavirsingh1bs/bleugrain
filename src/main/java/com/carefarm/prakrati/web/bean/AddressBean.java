package com.carefarm.prakrati.web.bean;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AddressBean implements Serializable {
	private static final long serialVersionUID = -6353961345547121012L;
	
	private String firstName;
	private String lastName;
	private String emailAddress;
	private String phoneNo;
	private String addressLine1;
	private String addressLine2;
	private String city;
	private StateBean state;
	private CountryBean country;
	private String zipcode;
		
}
