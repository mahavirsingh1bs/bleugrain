package com.carefarm.prakrati.web.bean;

import java.io.Serializable;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExpertiseBean extends AbstractBean implements Serializable {

	private static final long serialVersionUID = -6929983304651005644L;
	
	private String uniqueId;
	private String name;
	private String experience;
	private String experLabel;
	private int ratingPercent;
	private Set<ReviewBean> reviews;
	private UserBean user;
	private CompanyBean company;
		
}
