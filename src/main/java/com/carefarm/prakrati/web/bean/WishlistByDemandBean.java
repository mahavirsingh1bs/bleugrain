package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WishlistByDemandBean {
	private DemandBean demand;
	private long total;
	
	public WishlistByDemandBean() { }

	public WishlistByDemandBean(DemandBean demand, long total) {
		this.demand = demand;
		this.total = total;
	}
		
}
