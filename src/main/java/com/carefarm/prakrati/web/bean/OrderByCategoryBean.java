package com.carefarm.prakrati.web.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderByCategoryBean {
	private CategoryBean category;
	private long total;
	
	public OrderByCategoryBean() { }
	
	public OrderByCategoryBean(CategoryBean category, long total) {
		this.category = category;
		this.total = total;
	}
	
}
