package com.carefarm.prakrati.web.bean;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ImageBean implements Serializable {

	private static final long serialVersionUID = -5058706976171462132L;
	
	private Long id;
	private String name;
	private boolean isDefault;
	private String filename;
	private String filepath;
	private String smallImgPath;
	private String mediumImgPath;
	private String bigImgPath;
	
}
