package com.carefarm.prakrati.web.bean;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PaymentCardBean extends AbstractBean implements Serializable {
	
	private static final long serialVersionUID = -4467174920369010998L;
	
	private long id;
	private String name;
	private ImageBean defaultImage;
	
	public PaymentCardBean() { }
	
	public PaymentCardBean(long id, String name) { 
		this.id = id;
		this.name = name;
	}
	
}
