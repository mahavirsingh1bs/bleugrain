package com.carefarm.prakrati.web.common.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.common.entity.State;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.repository.StateRepository;
import com.carefarm.prakrati.service.StateService;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.bean.StateBean;
import com.carefarm.prakrati.web.model.StateForm;

@Controller
@RequestMapping("/states")
public class StateController {
	
    private static final Logger LOGGER = LoggerFactory.getLogger(StateController.class);
    
    @Autowired
    private StateService stateService;
    
    @Autowired
    private StateRepository stateRepository;
    
    @RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showStates(SearchRequest searchRequest) {
    	LOGGER.info("Finding all active agents");
		
    	if (searchRequest.getPageSize() > 0) {
	    	return stateService.findByKeyword(searchRequest);
    	} else {
    		Map<String, Object> data = new HashMap<>();
    		List<StateBean> states = stateService.findAll();
    		data.put("states", states);
    		return data;
    	}
	}
    
    @RequestMapping(value = "/{countryId}", method = RequestMethod.GET)
    public @ResponseBody List<StateBean> statesByState(@PathVariable long countryId) {
    	if (countryId <= 0) {
    		throw new IllegalArgumentException("stateId is not valid");
    	}
        List<StateBean> states = new ArrayList<StateBean>();
        for (State state : stateRepository.findStatesByCountryId(countryId)) {
        	states.add(new StateBean(state.getId(), state.getName()));
        }
        return states;
    }

    @RequestMapping(value="/new", method=RequestMethod.POST)
	public @ResponseBody MessageBean newState(@Valid @RequestBody StateForm form, BindingResult result, 
			Authentication authentication, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
            return new MessageBean("failure", "Form has validation errors.");
        }
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
        	stateService.create(form, user);
			redirectAttributes.addFlashAttribute("message", "State has been added successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got a server error. So state cannot be added now.", e);
			return new MessageBean("error", "Got server error while adding state.");
		}
        
        return new MessageBean("success", "State has been added successfully");
	}
    
    @RequestMapping(value="/data/{stateId}", method=RequestMethod.GET)
	public @ResponseBody StateBean getStateData(@PathVariable("stateId") long stateId, ModelMap modelMap){
		return stateService.findDetails(stateId, null);
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public @ResponseBody MessageBean update(@Valid @RequestBody StateBean stateBean, BindingResult result, 
			RedirectAttributes redirectAttributes, Authentication authentication) {
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
			stateService.update(stateBean, user);
			redirectAttributes.addFlashAttribute("message", "State has been updated successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while creating state", e);
			redirectAttributes.addFlashAttribute("message", "Got error while updating state");
			return new MessageBean("error", "Got error while updating state");
		}
        
        return new MessageBean("success", "State has been updated successfully.");
	}
    
    @RequestMapping(value = "/delete/{uniqueId}", method = RequestMethod.DELETE)
	public @ResponseBody MessageBean delete(@PathVariable("uniqueId") String uniqueId) {
		stateService.delete(uniqueId);
		return new MessageBean("success", "State has been deleted successfully");
	}
}
