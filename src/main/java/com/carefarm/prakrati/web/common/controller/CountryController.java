package com.carefarm.prakrati.web.common.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.service.CountryService;
import com.carefarm.prakrati.web.bean.CountryBean;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.CountryForm;

@Controller
@RequestMapping("/countries")
public class CountryController {
	
    private static final Logger LOGGER = LoggerFactory.getLogger(CountryController.class);
    
    @Autowired
    private CountryService countryService;
    
    @RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showCountries(SearchRequest searchRequest) {
    	LOGGER.info("Finding all countries");
    	if (searchRequest.getPageSize() > 0) {
			return countryService.findByKeyword(searchRequest);
    	} else {
    		Map<String, Object> data = new HashMap<>();
    		List<CountryBean> countries = countryService.findAll();
    		data.put("countries", countries);
    		return data;
    	}
	}

    @RequestMapping(value="/new", method=RequestMethod.POST)
	public @ResponseBody MessageBean newCountry(@Valid @RequestBody CountryForm form, BindingResult result, 
			Authentication authentication, RedirectAttributes redirectAttributes) {
		if(result.hasErrors()) {
            return new MessageBean("failure", "Form has validation errors.");
        }
		
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
        	countryService.create(form, user);
			redirectAttributes.addFlashAttribute("message", "Country has been added successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got a server error. So country cannot be added now.", e);
			return new MessageBean("error", "Got server error while adding country.");
		}
        
        return new MessageBean("success", "Country has been added successfully");
	}
    
    @RequestMapping(value="/data/{countryId}", method=RequestMethod.GET)
	public @ResponseBody CountryBean getCountryData(@PathVariable("countryId") long countryId, ModelMap modelMap){
		return countryService.findDetails(countryId, null);
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public @ResponseBody MessageBean update(@Valid @RequestBody CountryBean countryBean, BindingResult result, 
			RedirectAttributes redirectAttributes, Authentication authentication) {
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
		
        try {
			countryService.update(countryBean, user);
			redirectAttributes.addFlashAttribute("message", "Country has been updated successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while creating country", e);
			redirectAttributes.addFlashAttribute("message", "Got error while updating country");
			return new MessageBean("error", "Got error while updating country");
		}
        
        return new MessageBean("success", "Country has been updated successfully.");
	}
    
    @RequestMapping(value = "/delete/{uniqueId}", method = RequestMethod.DELETE)
	public @ResponseBody MessageBean delete(@PathVariable("uniqueId") String uniqueId) {
		countryService.delete(uniqueId);
		return new MessageBean("success", "Country has been deleted successfully");
	}
}
