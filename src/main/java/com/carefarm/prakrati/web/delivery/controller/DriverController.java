package com.carefarm.prakrati.web.delivery.controller;

import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.carefarm.prakrati.accounts.service.DriverService;
import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.util.UserType;
import com.carefarm.prakrati.validators.DriverValidator;
import com.carefarm.prakrati.web.bean.DriverBean;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.model.DriverForm;

@Controller
@RequestMapping("/drivers")
public class DriverController {
	private static final Logger LOGGER = LoggerFactory.getLogger(DriverController.class);
	
	@Autowired
	private DriverService driverService;
	
	@Autowired
	private DriverValidator driverValidator;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showAllDrivers(SearchRequest searchRequest) {
		return driverService.findByKeyword(searchRequest, UserType.DRIVER);
	}
	
	@RequestMapping(value="/company", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showCompanyDrivers(SearchRequest searchRequest) {
		return driverService.findByKeyword(searchRequest, UserType.DRIVER);
	}
	
	@RequestMapping(value="/data/{userNo}", method=RequestMethod.GET)
	public @ResponseBody DriverBean getData(@PathVariable("userNo") String userNo, ModelMap modelMap) {
		return driverService.findDetails(userNo);
	}
	
	@RequestMapping(value="/new", method=RequestMethod.POST)
	public ResponseEntity<?> newDriver(@Valid @RequestPart("driver") DriverForm form, BindingResult result, 
			@RequestPart(value = "driverImage", required = false) MultipartFile driverImage, Authentication authentication) {
		driverValidator.validate(form, result);
		if(result.hasErrors()) {
			MessageBean messageBean = new MessageBean("error", "Form has validation errors", result.getFieldErrors(), result.getGlobalErrors());
            return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
        }

        DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
    	
        try {
        	form.setUserType(UserType.DRIVER);
			driverService.create(form, driverImage, user);
			MessageBean messageBean = new MessageBean("success", "Driver has been registered successfully");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.OK);
		} catch (ServiceException e) {
			LOGGER.error("Got error while creating driver", e);
			MessageBean messageBean = new MessageBean("error", "Got error while creating driver");
			return new ResponseEntity<MessageBean>(messageBean, HttpStatus.NOT_FOUND);
		}
	}
	
	@RequestMapping(value="/update", method=RequestMethod.POST)
	public @ResponseBody MessageBean update(@Valid @RequestBody DriverBean driverBean, BindingResult result, 
			RedirectAttributes redirectAttributes, Authentication authentication) {
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();

        try {
			driverService.update(driverBean, currentUser);
			redirectAttributes.addFlashAttribute("message", "You have successfully signed up and logged in.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while creating farmer", e);
			redirectAttributes.addFlashAttribute("message", "Got error while creating farmer");
			return new MessageBean("error", "Got error while creating farmer");
		}
        
        return new MessageBean("success", "Driver has been updated successfully");
	}
	
	@RequestMapping(value = "/delete/{userNo}", method = RequestMethod.DELETE)
	public @ResponseBody MessageBean delete(@PathVariable("userNo") String userNo, Authentication authentication) {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();

		driverService.delete(userNo, currentUser);
		return new MessageBean("success", "Driver has been deleted successfully");
	}
	
}
