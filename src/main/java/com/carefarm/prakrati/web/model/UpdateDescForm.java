package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateDescForm {
	private String productId;
	private String shortDesc;
	private String desc;
	
}
