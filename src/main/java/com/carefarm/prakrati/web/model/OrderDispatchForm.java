package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.web.bean.UserBean;

@Getter
@Setter
public class OrderDispatchForm {
	private String orderNo;
	private UserBean deliverBy;
	private String deliveryDate;
	private String userNo;
		
}
