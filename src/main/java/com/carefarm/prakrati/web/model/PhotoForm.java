package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PhotoForm {
	private Long id;
	private String name;
	private String path;
	
	public PhotoForm() { }
	
	public PhotoForm(Long id, String name, String path) {
		this.id = id;
		this.name = name;
		this.path = path;
	}
		
}
