package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.web.bean.OrderStatusBean;
import com.carefarm.prakrati.web.bean.UserBean;

@Getter
@Setter
public class OrderUpdateForm {
	private String orderNo;
	private OrderStatusBean status;
	private UserBean deliverBy;
	private String deliveryDate;
	private String userNo;
		
}
