package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OwnerForm extends UserForm {
	private Long groupId;
	
}
