package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VerifyContactNoForm {
	private String uniqueCode;
	private String oneTimePasswd;
		
}
