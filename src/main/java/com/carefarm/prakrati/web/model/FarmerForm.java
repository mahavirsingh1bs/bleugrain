package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FarmerForm extends UserForm {
	
	private boolean transportAvailable;
		
	public FarmerForm() { }
	
	public FarmerForm(String firstName, String lastName, String mobileNo) {
		super(firstName, lastName, mobileNo);
	}
	
}
