package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExpertiseForm {
	private String name;
	private String experience;
	private String experLabel;
		
}
