package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VerifyMobileNoForm {
	private String userNo;
	private String oneTimePassword;
		
}
