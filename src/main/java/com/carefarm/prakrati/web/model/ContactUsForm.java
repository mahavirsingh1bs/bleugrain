package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.NotEmpty;

@Getter
@Setter
public class ContactUsForm {
	@NotEmpty(message="Name is required")
	private String name;
	
	@NotEmpty(message="Email address is required")
	private String emailId;
	private String phoneNo;
	
	@NotEmpty(message="Message is required")
	private String message;
		
}
