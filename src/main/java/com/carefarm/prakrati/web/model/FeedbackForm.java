package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class FeedbackForm {
	private Long servicesRating;
	private Long executivesRating;
	private Long siteRating;
	private String feedback;
		
}
