package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.NotEmpty;

@Getter
@Setter
public class MenuitemForm {
	@NotEmpty(message = "Name is required")
	private String name;

	private String keywords;
	
	private String categoryId;
	
	private String parentMenuitemId;

	private int displayIndex;
	
	private boolean active;
		
}
