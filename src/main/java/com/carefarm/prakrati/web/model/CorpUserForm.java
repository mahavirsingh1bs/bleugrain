package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.NotEmpty;

@Getter
@Setter
public class CorpUserForm extends UserForm {
	@NotEmpty(message="EmpId is required")
	private String empId;
	
	private long designationId;
	
}
