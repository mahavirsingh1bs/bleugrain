package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.NotEmpty;

@Getter
@Setter
public class IndUserForm extends UserForm {
	@NotEmpty(message="Address Line 1 is required")
	private String addressLine1;
	
	private String addressLine2;
	
	@NotEmpty(message="City is required")
	private String city;
	
	private long stateId;
	
	private long countryId;
	
	@NotEmpty(message="Zipcode is required")
	private String zipcode;
	
}
