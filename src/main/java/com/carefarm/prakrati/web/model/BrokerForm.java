package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BrokerForm extends UserForm {

	private String licenseNo;
	private boolean transportAvailable;
	
	public BrokerForm() { }
	
	public BrokerForm(String firstName, String lastName, String mobileNo, String emailAddress) {
		super(firstName, lastName, mobileNo, emailAddress);
	}
}
