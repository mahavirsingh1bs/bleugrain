package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.web.bean.AddressEntityBean;

@Getter
@Setter
public class AssignDeliveryForm {
	private String uniqueId;
	private String userNo;
	private String pickupDate;
	private String deliveryDate;
	private AddressEntityBean pickupAddress;
	private AddressEntityBean deliveryAddress;
	
}
