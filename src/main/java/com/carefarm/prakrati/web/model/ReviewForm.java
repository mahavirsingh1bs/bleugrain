package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ReviewForm {
	private String uniqueId;
	private String name;
	private String emailAddress;
	private String comment;
	private double rating;
		
}
