package com.carefarm.prakrati.web.model;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BidForm {
	private BigDecimal price;
	private String uniqueId;
	private String userNo;
	
}
