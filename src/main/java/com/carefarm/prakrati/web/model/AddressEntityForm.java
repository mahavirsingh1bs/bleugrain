package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.NotEmpty;

@Getter
@Setter
public class AddressEntityForm {
	@NotEmpty(message = "First Name is required")
	private String firstName;
	
	private String lastName;
	
	private String emailAddress;
	
	@NotEmpty(message = "Phone No is required")
	private String phoneNo;
	
	@NotEmpty(message = "Address Line 1 is required")
	private String addressLine1;
	
	private String addressLine2;
	
	@NotEmpty(message="City is required")
	private String city;
	
	private long stateId;
	
	private long countryId;
	
	@NotEmpty(message="Zipcode is required")
	private String zipcode;

	private boolean defaultAddress;

}
