package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.web.bean.UserBean;

@Getter
@Setter
public class DriverAssignForm {
	private UserBean driver;
	private String comments;
	private String uniqueId;
	private String userNo;
	
}
