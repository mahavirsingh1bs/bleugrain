package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BankAccountForm {
	private Long bankId;
	private String account;
	private String owner;
	private String swift;
	private boolean defaultBilling;
	private String userId;
	private String companyId;
		
}
