package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Message {
	
	private int id;
	private String message;
	
	public Message() { }

	public Message(int id, String message) {
		this.id = id;
		this.message = message;
	}
	
}
