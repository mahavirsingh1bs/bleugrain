package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

@Getter
@Setter
public class SignupForm {
	@NotEmpty(message="First Name is required")
    private String firstName;
    @NotEmpty(message="Last Name is required")
    private String lastName;
    @Email(message="Please provide a valid email address")
    @NotEmpty(message="Email is required")
    private String email;
    @NotEmpty(message="Password is required")
    private String password;
       
}
