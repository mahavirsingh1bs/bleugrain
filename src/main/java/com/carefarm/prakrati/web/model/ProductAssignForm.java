package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.web.bean.UserBean;

@Getter
@Setter
public class ProductAssignForm {
	private UserBean assignTo;
	private String comments;
	private String uniqueId;
	private String userNo;
		
}
