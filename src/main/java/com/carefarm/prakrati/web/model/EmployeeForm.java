package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.util.EmployeeGroup;

@Getter
@Setter
public class EmployeeForm extends UserForm {
	private Long groupId;
	private String employeeId;
	private Long designationId;
	private Long departmentId;
	private EmployeeGroup employeeGroup;

}
