package com.carefarm.prakrati.web.model;

import java.util.HashSet;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.web.bean.CompanyBean;
import com.carefarm.prakrati.web.bean.GroupBean;
import com.carefarm.prakrati.web.bean.UserBean;

@Getter
@Setter
public class NotificationForm {
	private Long categoryId;
	private String title;
	private String notification;
	private String startDate;
	private String expiryDate;
	private Set<GroupBean> groups = new HashSet<>();
	private Set<UserBean> users = new HashSet<>();
	private Set<CompanyBean> companies = new HashSet<>();
	
	public void addGroup(GroupBean group) {
		if (Predicates.isNull.test(this.groups)) {
			this.groups = new HashSet<>();
		}
		this.groups.add(group);
	}
	
}
