package com.carefarm.prakrati.web.model;

import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.NotEmpty;

import com.carefarm.prakrati.util.UserType;

@Getter
@Setter
public class RegisterForm {
	
	private UserType userType;
	
	@Size(min = 4, max = 30)
	@NotEmpty(message="First Name is required")
	private String firstName;
	private String lastName;
	
	// Company Name
	private String name;
	private String businessCategory;
	
	private String mobileNo;
	private String licenseNo;
	
	private String phoneNo;
	private String emailAddress;
	private String password;
	private String confirmPassword;
	
}
