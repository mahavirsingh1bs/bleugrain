package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChangePasswdForm {
	private String userId;
	private String oldPasswd;
	private String newPasswd;
	private String confirmNewPasswd;
	
}
