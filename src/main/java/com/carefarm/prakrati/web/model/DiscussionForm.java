package com.carefarm.prakrati.web.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.web.bean.GroupBean;
import com.carefarm.prakrati.web.bean.TagBean;

@Getter
@Setter
public class DiscussionForm {
	private String title;
	private String description;
	private List<GroupBean> groups = new ArrayList<>();
	private List<TagBean> tags = new ArrayList<>();
	
	public void addGroup(GroupBean group) {
		if (Predicates.isNull.test(this.groups)) {
			this.groups = new ArrayList<>();
		}
		this.groups.add(group);
	}
	
}
