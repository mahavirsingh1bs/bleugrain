package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NoteForm {
	private String title;
	private String note;
		
}
