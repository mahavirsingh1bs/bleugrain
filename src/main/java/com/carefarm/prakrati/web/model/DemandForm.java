package com.carefarm.prakrati.web.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.web.bean.TagBean;

@Getter
@Setter
public class DemandForm {
	private String name;
	private String categoryId;
	private BigDecimal quantity;
	private Long quantityUnitId;
	private BigDecimal pricePerUnit;
	private Long priceCurrencyId;
	private Long priceUnitId;
	private List<TagBean> tags = new ArrayList<>();
	private String deliveryDate;
	private AddressForm deliveryAddress;
	private String userId;
	private String companyId;
		
}
