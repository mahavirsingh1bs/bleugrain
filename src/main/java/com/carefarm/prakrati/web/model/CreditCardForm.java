package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreditCardForm {
	private Long paymentCardId;
	private String number;
	private String owner;
	private Long expiryMonthId;
	private Long expiryYearId;
	private boolean defaultBilling;
	private Long userId;
	private Long companyId;
	
}
