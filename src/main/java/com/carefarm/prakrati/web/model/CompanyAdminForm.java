package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CompanyAdminForm extends UserForm {
	private String companyId;
		
}
