package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.NotEmpty;

@Getter
@Setter
public class BankForm {
	@NotEmpty(message="Bank Name is required")
	private String name;
	
	@NotEmpty(message="Authentication URL is required")
	private String authURL;
	
	@NotEmpty(message="Username is required")
	private String username;
	
	@NotEmpty(message="Password is required")
	private String password;
		
}
