package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

@Getter
@Setter
public class CompanyForm {
	@NotEmpty(message="Company Name is required")
	private String name;
	
	private String businessCategory;
	
	@NotEmpty(message="Registr No. is required")
	private String registrNo;
	
	private String phoneNo;
	
	private String email;
	
	@NotEmpty(message="First Name is required")
	private String firstName;
	private String middleInitial;
	private String lastName;
	
	@NotEmpty(message="Phone No. is required")
	private String mobileNo;
	
	@Email(message="Please provide a valid email address")
    @NotEmpty(message="Email Address is required")
	private String userEmail;
	
	@NotEmpty(message="Address Line 1 is required")
	private String addressLine1;
	
	private String addressLine2;
	
	@NotEmpty(message="City is required")
	private String city;
	
	private long stateId;
	private long countryId;
	
	@NotEmpty(message="Zipcode is required")
	private String zipcode;
	
	public CompanyForm() { }
	
	public CompanyForm(String name, String registrNo, String phoneNo, String email) {
		this.name = name;
		this.registrNo = registrNo;
		this.phoneNo = phoneNo;
		this.email = email;
	}
	
	public CompanyForm(String name, String registrNo, String phoneNo, String email, 
			String firstName, String lastName, String mobileNo, String userEmail) {
		this.name = name;
		this.registrNo = registrNo;
		this.phoneNo = phoneNo;
		this.email = email;
		this.firstName = firstName;
		this.lastName = lastName;
		this.mobileNo = mobileNo;
		this.userEmail = userEmail;
	}
		
}
