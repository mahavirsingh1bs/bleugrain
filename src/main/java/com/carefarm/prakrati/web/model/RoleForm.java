package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.NotEmpty;

@Getter
@Setter
public class RoleForm {
	@NotEmpty(message = "Name is required")
	private String name;
	
}
