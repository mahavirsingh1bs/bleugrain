package com.carefarm.prakrati.web.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.web.bean.PermitBean;

@Getter
@Setter
public class TransportForm {
	@NotNull(message = "Name is required")
	private String name;
	
	@NotNull(message = "Vehicle No. is required")
	private String vehicleNo;
	
	@NotNull(message = "Capacity is required")
	private BigDecimal capacity;
	private Long capacityUnitId = 1L;
	
	@NotNull(message = "Price is required")
	private BigDecimal price;
	private Long priceCurrencyId = 1L;
	private Long priceUnitId = 1L;
	private List<Long> selectedPermitIds = new ArrayList<>();
	private List<PermitBean> permits = new ArrayList<>();
	private String companyId;
	
}
