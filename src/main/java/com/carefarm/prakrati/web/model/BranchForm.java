package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BranchForm {
	private String name;
	private AddressForm address;
	private Long companyId;
		
}
