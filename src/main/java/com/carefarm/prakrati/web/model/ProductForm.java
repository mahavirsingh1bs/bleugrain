package com.carefarm.prakrati.web.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.web.bean.AttributeBean;
import com.carefarm.prakrati.web.bean.CategoryBean;
import com.carefarm.prakrati.web.bean.TagBean;

@Getter
@Setter
public class ProductForm implements Serializable {
	
	private static final long serialVersionUID = 1831550862825712351L;
	
	@NotNull(message = "Name is required")
	private String name;
	
	private String desc;
	
	@NotNull(message = "Quantity is required")
	private BigDecimal quantity;
	private Long quantityUnitId = 1L;
	
	@NotNull(message = "Price is required")
	private BigDecimal price;
	private Long priceCurrencyId = 1L;
	private Long priceUnitId = 1L;
	private List<String> categoryIds = new ArrayList<>();
	private List<CategoryBean> categories = new ArrayList<>();
	private boolean consumerProduct;
	private List<TagBean> tags = new ArrayList<>();
	private String ownerId;
	private OwnerForm owner;
	private List<AttributeBean> attributes = new ArrayList<>();
		
}
