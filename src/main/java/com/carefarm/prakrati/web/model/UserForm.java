package com.carefarm.prakrati.web.model;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.NotEmpty;

import com.carefarm.prakrati.util.UserType;

@Getter
@Setter
public class UserForm {
	private UserType userType;
	
	@Size(min = 3, message = "First Name is too short")
	@Pattern(regexp = "[A-Za-z ]+[A-Za-z0-9._ ]+", message = "Only alphanumerics are allowed.")
	@NotEmpty(message="First Name is required")
	private String firstName;
	private String middleInitial;
	private String lastName;

	@Pattern(regexp = "([0-9]{12,13})", message = "Mobile No. is invalid")
	@NotEmpty(message="Mobile No. is required")
	private String mobileNo;

	private String phoneNo;
	
	private String email;
	
	@NotEmpty(message="Address Line 1 is required")
	private String addressLine1;
	
	private String addressLine2;
	
	@NotEmpty(message="City is required")
	private String city;
	
	private long stateId;
	
	private long countryId;
	
	@Pattern(regexp = "[0-9]{5,6}", message = "Please enter a valid zipcode")
	@NotEmpty(message="Zipcode is required")
	private String zipcode;
	
	public UserForm() { }
	
	public UserForm(String firstName, String lastName) {
		this.firstName = firstName;
		this.lastName = lastName;
	}
	
	public UserForm(String firstName, String lastName, String mobileNo) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.mobileNo = mobileNo;
	}
	
	public UserForm(String firstName, String lastName, String mobileNo, String emailAddress) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.mobileNo = mobileNo;
		this.email = emailAddress;
	}
}
