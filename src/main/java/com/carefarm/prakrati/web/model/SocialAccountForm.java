package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.util.SocialSite;

@Getter
@Setter
public class SocialAccountForm {
	private String displayName;
	private String username;
	private String accountUrl;
	private SocialSite socialSite;
	
}
