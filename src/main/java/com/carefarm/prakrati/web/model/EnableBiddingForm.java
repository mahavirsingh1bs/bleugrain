package com.carefarm.prakrati.web.model;

import java.math.BigDecimal;

import lombok.Getter;
import lombok.Setter;

import com.carefarm.prakrati.web.bean.CurrencyBean;
import com.carefarm.prakrati.web.bean.UnitBean;

@Getter
@Setter
public class EnableBiddingForm {
	private BigDecimal initialBidAmount;
	private CurrencyBean biddingCurrency;
	private UnitBean biddingUnit;
	private String enableTillDate;
	private String uniqueId;
	private String userNo;
		
}
