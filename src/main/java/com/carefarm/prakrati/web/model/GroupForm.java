package com.carefarm.prakrati.web.model;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.validator.constraints.NotEmpty;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.web.bean.RoleBean;

@Getter
@Setter
public class GroupForm {
	@NotEmpty(message = "Name is required")
	private String name;
	private List<Long> selectedRoleIds;
	private List<RoleBean> selectedRoles;

	public void addSelectedRoleId(Long selectedRoleId) {
		if (Predicates.isNull.test(this.selectedRoleIds)) {
			this.selectedRoleIds = new ArrayList<>();
		}
		this.selectedRoleIds.add(selectedRoleId);
	}
		
	public void addSelectedRole(RoleBean selectedRole) {
		if (Predicates.isNull.test(selectedRoles)) {
			this.selectedRoles = new ArrayList<>();
		}
		this.selectedRoles.add(selectedRole);
	}

}
