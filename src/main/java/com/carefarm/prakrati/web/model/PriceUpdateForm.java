package com.carefarm.prakrati.web.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PriceUpdateForm {
	private String uniqueId;
	private double price;
	private Long priceCurrencyId;
	private Long priceUnitId;
	private String userNo;
	
}
