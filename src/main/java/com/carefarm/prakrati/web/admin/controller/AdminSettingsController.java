package com.carefarm.prakrati.web.admin.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.dozer.DozerBeanMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.carefarm.prakrati.accounts.service.EmployeeService;
import com.carefarm.prakrati.entity.Department;
import com.carefarm.prakrati.entity.Designation;
import com.carefarm.prakrati.repository.DepartmentRepository;
import com.carefarm.prakrati.repository.DesignationRepository;
import com.carefarm.prakrati.util.UserType;
import com.carefarm.prakrati.web.bean.DepartmentBean;
import com.carefarm.prakrati.web.bean.DesignationBean;
import com.carefarm.prakrati.web.bean.SearchRequest;

@Controller
@RequestMapping("/admin")
public class AdminSettingsController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(AdminSettingsController.class);
    
    @Autowired
    private DozerBeanMapper mapper;
    
    @Autowired
    private EmployeeService employeeService;
    
    @Autowired
	public DesignationRepository designationRepository;
	
	@Autowired
	private DepartmentRepository departmentRepository;
	
    @ModelAttribute("allDesignations")
    public List<DesignationBean> populateDesignations() {
        List<DesignationBean> designations = new ArrayList<>();
        for (Designation designation : designationRepository.findAll()) {
        	designations.add(mapper.map(designation, DesignationBean.class));
        }
        return designations;
    }
	
	@ModelAttribute("allDepartments")
    public List<DepartmentBean> populateDepartments() {
        List<DepartmentBean> departments = new ArrayList<>();	
        for (Department department : departmentRepository.findAll()) {
        	departments.add(mapper.map(department, DepartmentBean.class));
        }
        return departments;
    }
    
    @RequestMapping(value="/executives", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showExecutives(SearchRequest searchRequest) {
    	LOGGER.info("Getting all executives");
    	return employeeService.findByKeyword(searchRequest, UserType.EMPLOYEE);
	}
        
    @RequestMapping(value="/salesTeam", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showSalesTeam(SearchRequest searchRequest) {
    	LOGGER.info("Getting all sales team");
		return employeeService.findByKeyword(searchRequest, UserType.EMPLOYEE);
	}
        
    @RequestMapping(value="/contentTeam", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showContentTeam(SearchRequest searchRequest){
    	LOGGER.info("Getting all content team");
    	return employeeService.findByKeyword(searchRequest, UserType.EMPLOYEE);
	}
    
}
