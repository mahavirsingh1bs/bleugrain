package com.carefarm.prakrati.web.admin.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.carefarm.prakrati.service.DepartmentService;
import com.carefarm.prakrati.service.DesignationService;
import com.carefarm.prakrati.service.GroupService;
import com.carefarm.prakrati.service.RoleService;
import com.carefarm.prakrati.web.bean.SearchRequest;

@Controller
@RequestMapping("admin")
public class OrgAdminController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(OrgAdminController.class);

	@Autowired
	private DepartmentService departmentService;
	
	@Autowired
	private DesignationService designationService;
	
	@Autowired
	private GroupService groupService;
	
	@Autowired
	private RoleService roleService;
	
	@RequestMapping(value="/depts", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showDepartments(SearchRequest searchRequest) {
		LOGGER.info("Finding all departments");
		return departmentService.findByKeyword(searchRequest);
	}
	
	@RequestMapping(value="/desigs", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showDesignations(SearchRequest searchRequest) {
		return designationService.findByKeyword(searchRequest);
	}
	
	@RequestMapping(value="/groups", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showGroups(SearchRequest searchRequest) {
		return groupService.findByKeyword(searchRequest);
	}
	
	@RequestMapping(value="/roles", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showRoles(SearchRequest searchRequest) {
		return roleService.findByKeyword(searchRequest);
	}
}
