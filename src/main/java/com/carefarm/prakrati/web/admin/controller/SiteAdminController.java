package com.carefarm.prakrati.web.admin.controller;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.carefarm.prakrati.service.BankService;
import com.carefarm.prakrati.service.CategoryService;
import com.carefarm.prakrati.service.CountryService;
import com.carefarm.prakrati.service.PaymentCardService;
import com.carefarm.prakrati.service.PaymentMethodService;
import com.carefarm.prakrati.service.ProductService;
import com.carefarm.prakrati.service.StateService;
import com.carefarm.prakrati.web.bean.SearchRequest;

@Controller
@RequestMapping("admin")
public class SiteAdminController {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(SiteAdminController.class);
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private BankService bankService;
	
	@Autowired
	private CountryService countryService;
	
	@Autowired
	private StateService stateService;
	
	@Autowired
	private PaymentMethodService paymentMethodService;
	
	@Autowired
	private PaymentCardService paymentCardService;
	
	
	@RequestMapping(value="/categories", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showCategories(SearchRequest searchRequest) {
		LOGGER.info("Finding all categories");
		return categoryService.findByKeyword(searchRequest);
	}
	
	@RequestMapping(value="/banks", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showBanks(SearchRequest searchRequest) {
		return bankService.findByKeyword(searchRequest);
	}
	
	@RequestMapping(value="/products", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showProducts(SearchRequest searchRequest) {
		return productService.findByKeyword(searchRequest);
	}
	
	@RequestMapping(value="/countries", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showCountries(SearchRequest searchRequest) {
		return countryService.findByKeyword(searchRequest);
	}
	
	@RequestMapping(value="/states", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showStates(SearchRequest searchRequest) {
		return stateService.findByKeyword(searchRequest);
	}
	
	@RequestMapping(value="/pmethods", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showPaymentMethods(SearchRequest searchRequest) {
		return paymentMethodService.findByKeyword(searchRequest);
	}
	
	@RequestMapping(value="/validCards", method=RequestMethod.GET)
	public @ResponseBody Map<String, Object> showCards(SearchRequest searchRequest) {
		return paymentCardService.findByKeyword(searchRequest);
	}
}
