package com.carefarm.prakrati.web.payment.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BillingDetailsForm implements Serializable {
	
	private static final long serialVersionUID = -8264162886119323415L;

	private String discrType;
	
	// common properties
	private String owner;
	private boolean defaultBilling;
	
	// properties for bank account
	private Long bankId;
	private String account;
	private String swift;
	
	// properties for credit card
	private Long paymentCardId;
	private String number;
	private Long expiryMonthId;
	private Long expiryYearId;
	
	private String userNo;
	private String companyId;
	
}
