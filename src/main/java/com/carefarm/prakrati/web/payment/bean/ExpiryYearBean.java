package com.carefarm.prakrati.web.payment.bean;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExpiryYearBean implements Serializable {
	
	private static final long serialVersionUID = 3666246152223297263L;
	
	private Long id;
	private String name;
		
}
