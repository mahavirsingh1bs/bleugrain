package com.carefarm.prakrati.web.payment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.carefarm.prakrati.payment.service.ExpiryYearService;
import com.carefarm.prakrati.web.payment.bean.ExpiryYearBean;

@Controller
@RequestMapping("/expiryYears")
public class ExpiryYearController {
	
	@Autowired
	private ExpiryYearService expiryYearService;
	
	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody List<ExpiryYearBean> expiryYears() {
    	return expiryYearService.findAll();
	}
	
}
