package com.carefarm.prakrati.web.payment.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.carefarm.prakrati.payment.service.ExpiryMonthService;
import com.carefarm.prakrati.web.payment.bean.ExpiryMonthBean;

@Controller
@RequestMapping("/expiryMonths")
public class ExpiryMonthController {

	@Autowired
	private ExpiryMonthService expiryMonthService;

	@RequestMapping(value="/all", method=RequestMethod.GET)
	public @ResponseBody List<ExpiryMonthBean> expiryMonths() {
    	return expiryMonthService.findAll();
	}
	
}
