package com.carefarm.prakrati.web.payment.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.exception.ServiceException;
import com.carefarm.prakrati.payment.service.BillingDetailsService;
import com.carefarm.prakrati.web.bean.MessageBean;
import com.carefarm.prakrati.web.payment.model.BillingDetailsForm;

@Controller
@RequestMapping("/billing")
public class BillingDetailsController implements BeanFactoryAware {
	private static final Logger LOGGER = LoggerFactory.getLogger(BillingDetailsController.class);
	
	private static final String SERVICE = "Service";
	
	private BeanFactory beanFactory;
	
	private BillingDetailsService billingDetailsService;
		
	@RequestMapping(value="/new", method=RequestMethod.POST)
	public @ResponseBody MessageBean newBillingDetails(@RequestBody @Valid BillingDetailsForm form, 
			Authentication authentication, BindingResult result, RedirectAttributes redirectAttributes){
		LOGGER.info("Adding a new billing details");
		if(result.hasErrors()) {
            return new MessageBean("error", "form has errors");
        }

		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User currentUser = (User )token.getPrincipal();
		
        try {
        	billingDetailsService = beanFactory.getBean(form.getDiscrType() + SERVICE, BillingDetailsService.class);
			billingDetailsService.create(form, currentUser);
			redirectAttributes.addFlashAttribute("message", "Billing Details has been added successfully.");
		} catch (ServiceException e) {
			LOGGER.error("Got error while creating driver", e);
			redirectAttributes.addFlashAttribute("message", "Got error while adding billing details");
			return new MessageBean("error", "Got error while adding billing details");
		}
        
        return new MessageBean("success", "Billing Details has been added successfully");
	}
		
	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		this.beanFactory = beanFactory;
	}
}
