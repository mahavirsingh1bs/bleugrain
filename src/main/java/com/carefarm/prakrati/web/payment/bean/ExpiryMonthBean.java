package com.carefarm.prakrati.web.payment.bean;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ExpiryMonthBean  implements Serializable {
	
	private static final long serialVersionUID = 130309405475680978L;
	
	private Long id;
	private String name;
	private String month;
		
}
