package com.carefarm.prakrati.common.util;

public class Constants {
	
	public static final String BLANK = "";
	
	public static final String PLUS = "+";
	
	public static final String SEMICOLON = ";";
	
	public static final int FIRST_PAGE = 0;
	public static final int PAGE_SIZE_10 = 10;
	
	
	public static final String SUCCESS = "success";
	public static final String ERROR = "error";

	public static final String ADDR_CAT_BILLING = "billing";
	public static final String ADDR_CAT_DELIVERY = "delivery";
	
	public static final String ID = "id";
	public static final String USER = "user";
	public static final String USER_ID = "userId";
	public static final String USER_NO = "userNo";
	public static final String USER_TYPE = "userType";
	public static final String COMPANY_ID = "companyId";
	public static final String CURRENT_USER = "currentUser";
	
	public static final String DEFAULT_COUNTRY = "IN";
	
	public static final String DEFAULT_LANGUAGE = "en";
	
	public static final String DEFAULT_MOBILE_CODE = "448866";
	
	public static final String DATA_HEADER = "data:";
	
	public static final String BASE64_HEADER = "base64,";
	
	public static final String CONTENT_TYPE_IMAGE_HEADER = "image/";
	
	public static final String USER_CREATED = "user.created";
	public static final String USER_REGISTERED = "user.registered";
	
	public static final String COMPANY_CREATED = "company.created";
	public static final String COMPANY_REGISTERED = "company.registered";
	
	public static final String ORDER_PLACED = "customer.order.placed";
	public static final String ORDER_SHIPPED = "customer.order.shipped";
	public static final String ORDER_DISPATCHED = "customer.order.dispatched";
	public static final String ORDER_DELIVERED = "customer.order.delivered";
	public static final String ORDER_ASSIGNED = "customer.order.assigned";
	
	public static final String PRODUCT_ASSIGNED = "user.product.assigned";
	public static final String PRODUCT_SOLDOUT = "user.product.soldout";
	
	public static final String DEMAND_ASSIGNED = "user.demand.assigned";
	public static final String DEMAND_FULFILLED = "user.demand.fulfilled";
	
	public static final String ADVANCED_BOOKING = "advanced.booking.request";
	
	// Topics
	public static final String NOTIFICATIONS_TOPIC = "/topic/notifications";
	public static final String BIDS_TOPIC = "/topic/bids";
	public static final String PRODUCTS_TOPIC = "/topic/products";
	
	// Socket Events
	public static final String NOTIFICATION = "notification";
	public static final String BID = "bid";
	public static final String PRODUCT = "product";
	
	public static final String ROLE_COMPANY_ADMIN = "ROLE_COMPANY_ADMIN"; 

}
