package com.carefarm.prakrati.common.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "CF_RATING")
@Getter
@Setter
public class Rating implements Serializable {
	
	private static final long serialVersionUID = 3191346867956534401L;

	@Id
	@GenericGenerator(name = "rating_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_RATING"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "rating_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;

	@Version
    @Column(name = "OBJ_VERSION")
	@Setter(AccessLevel.NONE)
    private int version = 0;
	
	@Column(name = "VALUE")
	private Double value;
	
	@Column(name = "IS_ARCHIVED")
	private boolean archived;

	public Rating() { }
	
	public Rating(double value) {
		this.value = value;
	}
	
}
