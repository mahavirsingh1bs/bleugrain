package com.carefarm.prakrati.common.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.GenericGenerator;

import com.carefarm.prakrati.entity.AbstractEntity;

@Entity
@Table(name = "CF_COUNTRY")
@NamedQueries(
	@NamedQuery(
			name = "Country.findByNameContains", 
			query = "SELECT c FROM Country c WHERE c.name LIKE :name")
)
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Getter 
@Setter
public class Country extends AbstractEntity implements Serializable {
	
	private static final long serialVersionUID = -2129033453296601548L;

	@Id
	@GenericGenerator(name = "country_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_COUNTRY"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "country_sequence_generator", strategy = GenerationType.SEQUENCE)
	private long id;
	
	@Column(name = "CODE")
	private String code;
	
	@Column(name = "NAME")
	private String name;
	
	@Column(name = "IS_ARCHIVED")
	private boolean archived;
	
	@OneToMany(mappedBy = "country", fetch = FetchType.LAZY)
	private Set<State> states;

	public Country() { }
	
	public Country(String name) {
		this.name = name;
	}
	
	public void addState(State state) {
		if (this.states == null) {
			this.states = new HashSet<>();
		}
		this.states.add(state);
	}

	public boolean hasState(String stateName) {
		for (State state : states) {
			if (state.getName().equals(stateName)) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Country other = (Country) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Country [id=" + id + ", name=" + name + "]";
	}
	
}
