package com.carefarm.prakrati.common.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Version;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "CF_PRODUCT_STATUS")
@Getter
@Setter
public class ProductStatus implements Serializable {
	private static final long serialVersionUID = -2098423376226635554L;

	@Id
	@GenericGenerator(name = "product_status_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_PRODUCT_STATUS"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "product_status_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@Version
    @Column(name = "OBJ_VERSION")
	@Setter(AccessLevel.NONE)
    private int version = 0;
	
	@Column(name = "CODE")
	private String code;
	
	@Column(name = "STATUS")
	private String status;
	
	@Column(name = "IS_ARCHIVED")
	private boolean archived;

	public ProductStatus() { }
	
	public ProductStatus(String status) {
		this.status = status;
	}
		
}
