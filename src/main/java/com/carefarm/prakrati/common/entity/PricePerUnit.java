package com.carefarm.prakrati.common.entity;

import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Embeddable
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PricePerUnit {
	
	@Column(name = "PRICE_PER_UNIT")
	private BigDecimal pricePerUnit;

	@ManyToOne
	@JoinColumn(name = "CURRENCY_ID")
	private Currency currency;
	
	@ManyToOne
	@JoinColumn(name = "PUNIT_ID")
	private Unit unit;
	
	public PricePerUnit(BigDecimal pricePerUnit) {
		this.pricePerUnit = pricePerUnit;
	}
	
}
