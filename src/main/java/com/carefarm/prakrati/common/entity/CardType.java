package com.carefarm.prakrati.common.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "CF_CARD_TYPE")
@Getter 
@Setter
public class CardType implements Serializable {
	
	private static final long serialVersionUID = 3485364813757382773L;

	@Id
	@GenericGenerator(name = "card_type_sequence_generator", strategy = "enhanced-table", parameters = {
			@org.hibernate.annotations.Parameter(name = "table_name", value = "CF_PRAKRATI_SEQUENCES"),
			@org.hibernate.annotations.Parameter(name = "value_column_name", value = "NEXT_VALUE"),
			@org.hibernate.annotations.Parameter(name = "segment_column_name", value = "SEQUENCE_NAME"),
			@org.hibernate.annotations.Parameter(name = "segment_value", value = "CF_CARD_TYPE"),
			@org.hibernate.annotations.Parameter(name = "allocationSize", value = "1"), })
	@GeneratedValue(generator = "card_type_sequence_generator", strategy = GenerationType.SEQUENCE)
	private Long id;
	
	@Column(name = "TYPE")
	private String type;

	@Column(name = "IS_ARCHIVED")
	private boolean archived;
	
}
