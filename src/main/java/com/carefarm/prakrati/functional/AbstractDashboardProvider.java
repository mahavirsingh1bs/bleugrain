package com.carefarm.prakrati.functional;

import static com.carefarm.prakrati.common.util.Constants.FIRST_PAGE;
import static com.carefarm.prakrati.common.util.Constants.PAGE_SIZE_10;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.apache.commons.codec.binary.StringUtils;
import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;

import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.delivery.util.OrderStatus;
import com.carefarm.prakrati.repository.AdvancedBookingRepository;
import com.carefarm.prakrati.repository.CategoryRepository;
import com.carefarm.prakrati.repository.CompanyRepository;
import com.carefarm.prakrati.repository.ContactRepository;
import com.carefarm.prakrati.repository.DemandRepository;
import com.carefarm.prakrati.repository.DiscussionRepository;
import com.carefarm.prakrati.repository.GroupRepository;
import com.carefarm.prakrati.repository.NoteRepository;
import com.carefarm.prakrati.repository.NotificationRepository;
import com.carefarm.prakrati.repository.OrderRepository;
import com.carefarm.prakrati.repository.ProductRepository;
import com.carefarm.prakrati.repository.UpcomingEventRepository;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.repository.WishlistRepository;
import com.carefarm.prakrati.util.DemandStatus;
import com.carefarm.prakrati.util.ProductState;
import com.carefarm.prakrati.util.UserType;
import com.carefarm.prakrati.web.bean.AccountByGroupBean;
import com.carefarm.prakrati.web.bean.BidBean;
import com.carefarm.prakrati.web.bean.BidByCategoryBean;
import com.carefarm.prakrati.web.bean.BidByProductBean;
import com.carefarm.prakrati.web.bean.CategoryBean;
import com.carefarm.prakrati.web.bean.CurrencyBean;
import com.carefarm.prakrati.web.bean.DemandBean;
import com.carefarm.prakrati.web.bean.DemandByCategoryBean;
import com.carefarm.prakrati.web.bean.GroupBean;
import com.carefarm.prakrati.web.bean.OrderByCategoryBean;
import com.carefarm.prakrati.web.bean.ProductBean;
import com.carefarm.prakrati.web.bean.ProductByCategoryBean;
import com.carefarm.prakrati.web.bean.UnitBean;
import com.carefarm.prakrati.web.bean.WishlistByCategoryBean;
import com.carefarm.prakrati.web.bean.WishlistByDemandBean;
import com.carefarm.prakrati.web.bean.WishlistByProductBean;

public abstract class AbstractDashboardProvider implements DashboardProvider {
	protected final String[] notInOrderStatuses = { OrderStatus.DELIVERED.getCode() };
	protected final String[] productStatuses = { ProductState.ADDED_NEW.getCode(), ProductState.ONCE_PRICED.getCode() };
	protected final String[] demandStatuses = { DemandStatus.OPEN.getCode(), DemandStatus.TENTATIVE.getCode() };
	
	@Autowired
	protected DozerBeanMapper mapper;
	
	@Autowired
	protected UserRepository userRepository;
	
	@Autowired
	protected GroupRepository groupRepository;
	
	@Autowired
	protected CategoryRepository categoryRepository;
	
	@Autowired
	protected ProductRepository productRepository;
	
	@Autowired
	protected WishlistRepository wishlistRepository;
	
	@Autowired
	protected DemandRepository demandRepository;

	@Autowired
	protected OrderRepository orderRepository;
	
	@Autowired
	protected CompanyRepository companyRepository;
	
	@Autowired
	protected NotificationRepository notificationRepository;
	
	@Autowired
	protected NoteRepository noteRepository;

	@Autowired
	protected ContactRepository contactRepository;
	
	@Autowired
	protected UpcomingEventRepository upcomingEventRepository;
	
	@Autowired
	protected DiscussionRepository discussionRepository;
	
	@Autowired
	protected AdvancedBookingRepository advancedBookingRepository;
	
	protected List<AccountByGroupBean> accountsByGroup() {
		List<AccountByGroupBean> accountsByGroup = new ArrayList<>();
		groupRepository.findAll()
			.forEach(group -> {
				if (StringUtils.equals(group.getName(), UserType.BROKER.getType()) || 
						StringUtils.equals(group.getName(), UserType.FARMER.getType()) ||
						StringUtils.equals(group.getName(), UserType.RETAILER.getType()) ||
						StringUtils.equals(group.getName(), UserType.CUSTOMER.getType())) {
					AccountByGroupBean accountByGroup = new AccountByGroupBean(mapper.map(group, GroupBean.class), userRepository.countByGroupName(group.getName()));
					accountsByGroup.add(accountByGroup);
				} else if (StringUtils.equals(group.getName(), UserType.COMPANY.getType())) {
					AccountByGroupBean accountByGroup = new AccountByGroupBean(mapper.map(group, GroupBean.class), companyRepository.countAllActiveCompanies());
					accountsByGroup.add(accountByGroup);
				}
			});
		return accountsByGroup;
	}
	
	protected List<WishlistByProductBean> wishlistsByProduct(String userNo) {
		List<WishlistByProductBean> wishlistsByProduct = new ArrayList<>();
		productRepository.findByUserNoAndStatusCodes(userNo, productStatuses)
			.forEach(product -> {
				WishlistByProductBean wishlistByProduct = new WishlistByProductBean(
						mapper.map(product, ProductBean.class), wishlistRepository.countByProductId(product.getUniqueId()));
				wishlistsByProduct.add(wishlistByProduct);
			});
		return wishlistsByProduct;
	}
	
	protected List<WishlistByDemandBean> wishlistsByDemand(String userNo) {
		List<WishlistByDemandBean> wishlistsByDemand = new ArrayList<>();
		demandRepository.findByUserNoAndStatusCodes(userNo, demandStatuses)
			.forEach(demand -> {
				WishlistByDemandBean wishlistByDemand = new WishlistByDemandBean(
						mapper.map(demand, DemandBean.class), wishlistRepository.countByDemandId(demand.getUniqueId()));
				wishlistsByDemand.add(wishlistByDemand);
			});
		return wishlistsByDemand;
	}
	
	protected List<ProductByCategoryBean> productsByCategory() {
		List<ProductByCategoryBean> productsByCategory = new ArrayList<>();
		categoryRepository.findMainCategories()
			.forEach(mainCategory -> {
				ProductByCategoryBean productByCategory = new ProductByCategoryBean(
						mapper.map(mainCategory, CategoryBean.class), productRepository.countByMainCategoryIdAndStatusCodes(mainCategory.getUniqueId(), productStatuses));
				productsByCategory.add(productByCategory);
			});
		
		return productsByCategory;
	}
	
	protected List<OrderByCategoryBean> ordersByCategory() {
		List<OrderByCategoryBean> ordersByCategory = new ArrayList<>();
		categoryRepository.findMainCategories()
			.forEach(mainCategory -> {
				OrderByCategoryBean orderByCategory = new OrderByCategoryBean(
						mapper.map(mainCategory, CategoryBean.class), orderRepository.countByCategoryIdAndDate(mainCategory.getUniqueId(), Date.from(Environment.clock().instant())));
				ordersByCategory.add(orderByCategory);
			});
		
		return ordersByCategory;
	}
	
	protected List<DemandByCategoryBean> demandsByCategory() {
		List<DemandByCategoryBean> demandsByCategory = new ArrayList<>();
		categoryRepository.findMainCategories()
			.forEach(mainCategory -> {
				DemandByCategoryBean demandByCategory = new DemandByCategoryBean(
						mapper.map(mainCategory, CategoryBean.class), demandRepository.countByCategoryIdAndStatusCodes(mainCategory.getUniqueId(), demandStatuses));
				demandsByCategory.add(demandByCategory);
			});
		
		return demandsByCategory;
	}
	
	protected List<WishlistByCategoryBean> wishlistsByCategory() {
		List<WishlistByCategoryBean> wishlistsByCategory = new ArrayList<>();
		categoryRepository.findMainCategories()
			.forEach(mainCategory -> {
				WishlistByCategoryBean wishlistByCategory = new WishlistByCategoryBean(
						mapper.map(mainCategory, CategoryBean.class), wishlistRepository.countByMainCategoryId(mainCategory.getUniqueId()));
				wishlistsByCategory.add(wishlistByCategory);
			});
		return wishlistsByCategory;
	}
	
	protected List<BidByCategoryBean> bidsByCategory() {
		List<BidByCategoryBean> bidsByCategory = new ArrayList<>();
		categoryRepository.findMainCategories()
			.forEach(mainCategory -> {
				BidByCategoryBean bidByCategory = new BidByCategoryBean(
						mapper.map(mainCategory, CategoryBean.class), productRepository.countBiddableByCategoryId(mainCategory.getUniqueId()));
				bidsByCategory.add(bidByCategory);
			});
		return bidsByCategory;
	}
	
	protected List<BidByProductBean> bidsByProduct(String userNo) {
		List<BidByProductBean> bidsByProduct = new ArrayList<>();
		productRepository.findBiddableByUserNo(userNo)
			.forEach(product -> {
				BidByProductBean bidByProduct = new BidByProductBean(mapper.map(product, ProductBean.class));
				bidByProduct.setInitialBidAmount(product.getInitialBidAmount());
				bidByProduct.setBiddingCurrency(mapper.map(product.getBiddingCurrency(), CurrencyBean.class));
				bidByProduct.setBiddingUnit(mapper.map(product.getBiddingUnit(), UnitBean.class));
				Optional.ofNullable(product.getHighestBid())
					.ifPresent(highestBid -> bidByProduct.setHighestBid(mapper.map(highestBid, BidBean.class)));
				bidsByProduct.add(bidByProduct);
			});
		
		return bidsByProduct;
	}
	
	protected List<ProductBean> sellingProducts(String userNo) {
		List<ProductBean> sellingProducts = new ArrayList<>();
		wishlistRepository.findProductsByUserNo(userNo, new PageRequest(FIRST_PAGE, PAGE_SIZE_10))
			.forEach(p -> sellingProducts.add(mapper.map(p, ProductBean.class)));
		
		return sellingProducts;
	}
	
	
	
	
	
}
