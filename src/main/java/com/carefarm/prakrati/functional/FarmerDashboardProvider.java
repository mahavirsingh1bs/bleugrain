package com.carefarm.prakrati.functional;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.carefarm.prakrati.web.bean.ActivitiesBean;

@Component
public class FarmerDashboardProvider extends AbstractDashboardProvider {

	@Override
	public Map<String, Object> provideDashboard(
			Map<String, Object> dashboardDetails, String userNo) {
		ActivitiesBean activities = new ActivitiesBean();
		activities.setTotalProducts(productRepository.countByUserNoAndStatusCodes(userNo, productStatuses));
		activities.setTotalSellingProducts(wishlistRepository.countProductsByUserNo(userNo));
		
		dashboardDetails.put("wishlistsByProduct", wishlistsByProduct(userNo));
		dashboardDetails.put("demandsByCategory", demandsByCategory());
		dashboardDetails.put("bidsByProduct", bidsByProduct(userNo));
		dashboardDetails.put("activities", activities);
		return dashboardDetails;
	}

}
