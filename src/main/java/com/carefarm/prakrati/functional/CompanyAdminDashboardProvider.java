package com.carefarm.prakrati.functional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.web.bean.BidByUserBean;
import com.carefarm.prakrati.web.bean.DemandByUserBean;
import com.carefarm.prakrati.web.bean.OrderBean;
import com.carefarm.prakrati.web.bean.OrderByUserBean;
import com.carefarm.prakrati.web.bean.ProductBean;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.bean.WishlistByUserBean;

@Component
public class CompanyAdminDashboardProvider extends AbstractDashboardProvider {

	@Override
	public Map<String, Object> provideDashboard(
			Map<String, Object> dashboardDetails, String uniqueId) {
		List<OrderByUserBean> ordersByUser = new ArrayList<>();
		List<BidByUserBean> bidsByUser = new ArrayList<>();
		List<WishlistByUserBean> wishlistsByUser = new ArrayList<>();
		List<DemandByUserBean> demandsByUser = new ArrayList<>();
		
		userRepository.findByCompanyId(uniqueId)
			.forEach(user -> {
				List<OrderBean> orderBeans = new ArrayList<>();
				orderRepository.findByUserIdAndDate(user.getUserNo(), Date.from(Environment.clock().instant()))
					.forEach(order -> orderBeans.add(mapper.map(order, OrderBean.class)));;
				OrderByUserBean ordersByUserBean = new OrderByUserBean(mapper.map(user, UserBean.class), orderBeans.size(), orderBeans);
				ordersByUser.add(ordersByUserBean);
				
				List<ProductBean> productBeans = new ArrayList<>();
				productRepository.findProductsForWhichBidPlaceByUserAndDate(user.getUserNo(), Date.from(Environment.clock().instant()))
					.forEach(product -> productBeans.add(mapper.map(product, ProductBean.class)));
				BidByUserBean bidsByUserBean = new BidByUserBean(mapper.map(user, UserBean.class), productBeans, null);
				bidsByUser.add(bidsByUserBean);
				
				WishlistByUserBean wishlistByUser = new WishlistByUserBean(mapper.map(user, UserBean.class), wishlistRepository.countProductsByUserNo(user.getUserNo()));
				wishlistsByUser.add(wishlistByUser);
				
				DemandByUserBean demandByUser = new DemandByUserBean(mapper.map(user, UserBean.class), wishlistRepository.countDemandsByUserNo(user.getUserNo()));
				demandsByUser.add(demandByUser);
			});
		
		dashboardDetails.put("ordersByUser", ordersByUser);
		dashboardDetails.put("bidsByUser", bidsByUser);
		dashboardDetails.put("wishlistsByUser", wishlistsByUser);
		dashboardDetails.put("demandsByUser", demandsByUser);
		return dashboardDetails;
	}

}
