package com.carefarm.prakrati.functional;

import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class CustomerDashboardProvider extends AbstractDashboardProvider {

	@Override
	public Map<String, Object> provideDashboard(
			Map<String, Object> dashboardDetails, String userNo) {
		dashboardDetails.put("totalOrders", orderRepository.countByUserNoAndStatusCodesNotIn(userNo, notInOrderStatuses));
		dashboardDetails.put("totalDemands", demandRepository.countByUserNoAndStatusCodes(userNo, demandStatuses));
		return dashboardDetails;
	}

}
