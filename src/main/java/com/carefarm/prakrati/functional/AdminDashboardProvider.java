package com.carefarm.prakrati.functional;

import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.web.bean.AdvancedBookingBean;

@Component
public class AdminDashboardProvider extends AbstractDashboardProvider {

	private static final String BROKER = "Broker";
	
	private static final String RETAILER = "Retailer";
	
	private static final String FARMER = "Farmer";
	
	private static final String CUSTOMER = "Customer";
	
	@Override
	public Map<String, Object> provideDashboard(
			Map<String, Object> dashboardDetails, String userNo) {
		Date lastWeek = Date.from(Environment.clock().instant().minus(7, ChronoUnit.DAYS));
		Date lastMonth = Date.from(Environment.clock().instant().minus(30, ChronoUnit.DAYS));
		String[] groupNames = { BROKER, RETAILER, FARMER, CUSTOMER };
		
		dashboardDetails.put("totalProducts", productRepository.countByStatusCodes(productStatuses));
		dashboardDetails.put("totalProductsLastWeek", productRepository.countByDateCreatedAndStatusCodes(lastWeek, productStatuses));
		dashboardDetails.put("totalProductsLastMonth", productRepository.countByDateCreatedAndStatusCodes(lastMonth, productStatuses));
		
		dashboardDetails.put("totalCompanies", companyRepository.countAllActiveCompanies());
		dashboardDetails.put("totalCompaniesLastWeek", companyRepository.countByDateCreated(lastWeek));
		dashboardDetails.put("totalCompaniesLastMonth", companyRepository.countByDateCreated(lastMonth));
		
		dashboardDetails.put("totalBrokers", userRepository.countByGroupName(BROKER));
		dashboardDetails.put("totalBrokersLastWeek", userRepository.countByDateAndGroupName(lastWeek, BROKER));
		dashboardDetails.put("totalBrokersLastMonth", userRepository.countByDateAndGroupName(lastMonth, BROKER));
		
		dashboardDetails.put("totalRetailers", userRepository.countByGroupName(RETAILER));
		dashboardDetails.put("totalRetailersLastWeek", userRepository.countByDateAndGroupName(lastWeek, RETAILER));
		dashboardDetails.put("totalRetailersLastMonth", userRepository.countByDateAndGroupName(lastMonth, RETAILER));
		
		dashboardDetails.put("totalFarmers", userRepository.countByGroupName(FARMER));
		dashboardDetails.put("totalFarmersLastWeek", userRepository.countByDateAndGroupName(lastWeek, FARMER));
		dashboardDetails.put("totalFarmersLastMonth", userRepository.countByDateAndGroupName(lastMonth, FARMER));
		
		dashboardDetails.put("totalCustomers", userRepository.countByGroupName(CUSTOMER));
		dashboardDetails.put("totalCustomersLastWeek", userRepository.countByDateAndGroupName(lastWeek, CUSTOMER));
		dashboardDetails.put("totalCustomersLastMonth", userRepository.countByDateAndGroupName(lastMonth, CUSTOMER));
		
		dashboardDetails.put("totalAccounts", userRepository.countByGroupNames(groupNames));
		dashboardDetails.put("totalAccountsLastWeek", userRepository.countByGroupsAndDate(lastWeek, groupNames));
		dashboardDetails.put("totalAccountsLastMonth", userRepository.countByGroupsAndDate(lastMonth, groupNames));
		
		dashboardDetails.put("accountsByGroup", accountsByGroup());
		dashboardDetails.put("productsByCategory", productsByCategory());
		dashboardDetails.put("ordersByCategory", ordersByCategory());
		dashboardDetails.put("wishlistsByCategory", wishlistsByCategory());
		dashboardDetails.put("bidsByCategory", bidsByCategory());
		
		List<AdvancedBookingBean> advancedBookings = new ArrayList<>();
		advancedBookingRepository.findAllOrderByDateCreatedDesc()
			.forEach(advancedBooking -> 
				advancedBookings.add(mapper.map(advancedBooking, AdvancedBookingBean.class)));
		dashboardDetails.put("advancedBookings", advancedBookings);
		
		return dashboardDetails;
	}

}
