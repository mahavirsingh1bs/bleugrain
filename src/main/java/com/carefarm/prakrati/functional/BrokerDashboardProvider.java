package com.carefarm.prakrati.functional;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.carefarm.prakrati.web.bean.ActivitiesBean;

@Component
public class BrokerDashboardProvider extends AbstractDashboardProvider {
		
	@Override
	public Map<String, Object> provideDashboard(
			Map<String, Object> dashboardDetails, String userNo) {
		ActivitiesBean activities = new ActivitiesBean();
		activities.setTotalOrders(orderRepository.countByUserNoAndStatusCodesNotIn(userNo, notInOrderStatuses));
		activities.setTotalProducts(productRepository.countByUserNoAndStatusCodes(userNo, productStatuses));
		activities.setTotalDemands(demandRepository.countByUserNoAndStatusCodes(userNo, demandStatuses));
		activities.setTotalBuyingDemands(wishlistRepository.countDemandsByUserNo(userNo));
		activities.setTotalSellingProducts(wishlistRepository.countProductsByUserNo(userNo));
		
		dashboardDetails.put("wishlistsByProduct", wishlistsByProduct(userNo));
		dashboardDetails.put("wishlistsByDemand", wishlistsByDemand(userNo));
		dashboardDetails.put("productsByCategory", productsByCategory());
		dashboardDetails.put("demandsByCategory", demandsByCategory());
		dashboardDetails.put("bidsByProduct", bidsByProduct(userNo));
		dashboardDetails.put("activities", activities);
		return dashboardDetails;
	}

}
