package com.carefarm.prakrati.functional;

import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Map;

import org.springframework.stereotype.Component;

import com.carefarm.prakrati.core.Environment;

@Component
public class EmployeeDashboardProvider extends AbstractDashboardProvider {

	@Override
	public Map<String, Object> provideDashboard(
			Map<String, Object> dashboardDetails, String userNo) {
		Date lastWeek = Date.from(Environment.clock().instant().minus(7, ChronoUnit.DAYS));
		Date lastMonth = Date.from(Environment.clock().instant().minus(30, ChronoUnit.DAYS));
		
		dashboardDetails.put("totalOrderAssigned", orderRepository.countOrdersAssignedToUserWhereStatusCodesNotIn(userNo, notInOrderStatuses));
		dashboardDetails.put("totalOrderAssignedLastWeek", orderRepository.countOrdersAssignedToUserWhereAssignDateIsAndStatusCodesNotIn(lastWeek, userNo, notInOrderStatuses));
		dashboardDetails.put("totalOrderAssignedLastMonth", orderRepository.countOrdersAssignedToUserWhereAssignDateIsAndStatusCodesNotIn(lastMonth, userNo, notInOrderStatuses));
		
		dashboardDetails.put("totalProductAssigned", productRepository.countProductsAssignedToUserWhereStatusCodes(userNo, productStatuses));
		dashboardDetails.put("totalProductAssignedLastWeek", productRepository.countProductsAssignedToUserWhereAssignDateAndStatusCodes(lastWeek, userNo, productStatuses));
		dashboardDetails.put("totalProductAssignedLastMonth", productRepository.countProductsAssignedToUserWhereAssignDateAndStatusCodes(lastMonth, userNo, productStatuses));
		
		dashboardDetails.put("totalDemandAssigned", demandRepository.countDemandsAssignedToUserWhereStatusCodes(userNo, demandStatuses));
		dashboardDetails.put("totalDemandAssignedLastWeek", demandRepository.countDemandsAssignedToUserWhereAssignDateAndStatusCodes(lastWeek, userNo, demandStatuses));
		dashboardDetails.put("totalDemandAssignedLastMonth", demandRepository.countDemandsAssignedToUserWhereAssignDateAndStatusCodes(lastMonth, userNo, demandStatuses));
		return dashboardDetails;
	}

}
