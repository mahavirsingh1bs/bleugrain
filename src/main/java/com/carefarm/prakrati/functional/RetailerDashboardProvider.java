package com.carefarm.prakrati.functional;

import java.util.Map;

import org.springframework.stereotype.Component;

import com.carefarm.prakrati.web.bean.ActivitiesBean;

@Component
public class RetailerDashboardProvider extends AbstractDashboardProvider {

	@Override
	public Map<String, Object> provideDashboard(
			Map<String, Object> dashboardDetails, String userNo) {
		ActivitiesBean activities = new ActivitiesBean();
		activities.setTotalOrders(orderRepository.countByUserNoAndStatusCodesNotIn(userNo, notInOrderStatuses));
		activities.setTotalDemands(demandRepository.countByUserNoAndStatusCodes(userNo, demandStatuses));
		activities.setTotalSellingProducts(wishlistRepository.countProductsByUserNo(userNo));
		
		dashboardDetails.put("wishlistsByDemand", wishlistsByDemand(userNo));
		dashboardDetails.put("sellingProducts", sellingProducts(userNo));
		dashboardDetails.put("productsByCategory", productsByCategory());
		dashboardDetails.put("activities", activities);
		return dashboardDetails;
	}

}
