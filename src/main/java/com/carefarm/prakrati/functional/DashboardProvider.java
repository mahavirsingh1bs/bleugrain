package com.carefarm.prakrati.functional;

import java.util.Map;

@FunctionalInterface
public interface DashboardProvider {
	
	Map<String, Object> provideDashboard(Map<String, Object> dashboardDetails, String uniqueId);
	
}
