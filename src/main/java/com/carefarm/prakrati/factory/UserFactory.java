package com.carefarm.prakrati.factory;

import com.carefarm.prakrati.entity.User;

public interface UserFactory<U extends User> {
	U create(String firstName, String lastName, String username, 
			String email, String password, String mobileNo);
}
