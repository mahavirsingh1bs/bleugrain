package com.carefarm.prakrati.factory;

import com.carefarm.prakrati.entity.Group;

public interface GroupFactory<G extends Group> {
	G create(String name);
}
