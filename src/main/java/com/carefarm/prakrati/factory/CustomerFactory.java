package com.carefarm.prakrati.factory;

import com.carefarm.prakrati.web.model.UserForm;

public interface CustomerFactory<U extends UserForm> {
	U create(String firstName, String lastName, String mobileNo, String emailAddress);
}
