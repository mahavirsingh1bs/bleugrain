package com.carefarm.prakrati.factory;

import org.springframework.beans.factory.annotation.Autowired;

import com.carefarm.prakrati.entity.Product;
import com.carefarm.prakrati.events.ProductAddEvent;
import com.carefarm.prakrati.events.handler.ProductAddEventHandler;
import com.carefarm.prakrati.events.router.DynamicRouter;

public class DynamicRouterFactory {
	
	private static final DynamicRouterFactory INSTANCE = new DynamicRouterFactory();
	
	@Autowired
	private ProductAddEventHandler productAddEventHandler;

	private DynamicRouterFactory() { }
	
	public static DynamicRouterFactory getInstance() {
		return INSTANCE; 
	}
	
	public DynamicRouter<ProductAddEvent, Product> createProductAddEventRouter() {
		DynamicRouter<ProductAddEvent, Product> productAddEventRouter = new DynamicRouter<>();
		productAddEventRouter.registerHandler(ProductAddEvent.class, productAddEventHandler);
		return productAddEventRouter;
	}
}
