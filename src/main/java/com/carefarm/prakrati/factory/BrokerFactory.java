package com.carefarm.prakrati.factory;

import com.carefarm.prakrati.web.model.UserForm;

public interface BrokerFactory<U extends UserForm> {
	U create(String firstName, String lastName, String phoneNo, String emailAddress);
}
