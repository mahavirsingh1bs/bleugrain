package com.carefarm.prakrati.factory;

import com.carefarm.prakrati.entity.Category;

public interface CategoryFactory<C extends Category> {
	C create(String name);
}
