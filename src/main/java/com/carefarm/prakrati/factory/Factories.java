package com.carefarm.prakrati.factory;

import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.web.model.BrokerForm;
import com.carefarm.prakrati.web.model.FarmerForm;
import com.carefarm.prakrati.web.model.UserForm;

public class Factories {
	
	public static final BrokerFactory<BrokerForm> BROKER_FACTORY = BrokerForm::new;
	public static final RetailerFactory<UserForm> RETAILER_FACTORY = UserForm::new;
	public static final FarmerFactory<FarmerForm> FARMER_FACTORY = FarmerForm::new;
	public static final CustomerFactory<UserForm> CUSTOMER_FACTORY = UserForm::new;
	
	public static final UserFactory<User> USER_FACTORY = User::new;
	
}
