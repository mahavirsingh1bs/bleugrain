package com.carefarm.prakrati.factory;

import com.carefarm.prakrati.web.model.UserForm;

public interface FarmerFactory<U extends UserForm> {
	U create(String firstName, String lastName, String mobileNo);
}
