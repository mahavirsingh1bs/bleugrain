package com.carefarm.prakrati.events;

public class SocketEvent {
	private String category;
	private Object object;
	
	private SocketEvent() { }
	
	private SocketEvent(String category, Object object) {
		this.category = category;
		this.object = object;
	}
	
	public static SocketEvent createEvent(String category, Object object) {
		return new SocketEvent(category, object);
	}
	
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public Object getObject() {
		return object;
	}
	public void setObject(Object object) {
		this.object = object;
	}
	
}
