package com.carefarm.prakrati.events;

public interface PrakratiEvent<T> {
	
	public Class<? extends PrakratiEvent<T>> getType();
	
	public T getPayload();
	
}
