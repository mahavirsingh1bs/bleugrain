package com.carefarm.prakrati.events.handler;

import com.carefarm.prakrati.events.PrakratiEvent;

public interface EventHandler<E extends PrakratiEvent<T>, T> {
	
	public void handle(E prakratiEvent);
}
