package com.carefarm.prakrati.events.handler;

import java.util.Date;
import java.util.function.Supplier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.entity.Product;
import com.carefarm.prakrati.entity.ProductEvent;
import com.carefarm.prakrati.events.ProductAddEvent;
import com.carefarm.prakrati.generators.UniqueIdBuilder;
import com.carefarm.prakrati.repository.ProductEventRepository;

@Component
public class ProductAddEventHandler implements EventHandler<ProductAddEvent, Product> {

	@Autowired
	private ProductEventRepository productEventRepository;
	
	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public void handle(ProductAddEvent event) {
		Product product = event.getPayload();
		
		Supplier<ProductEvent> productEventSupplier = ProductEvent::new;
		ProductEvent productEvent = productEventSupplier.get();
		productEvent.setUniqueId(UniqueIdBuilder.generateUniqueId());
		productEvent.setProduct(product);
		productEvent.setProductStatus(product.getStatus());
		productEvent.setEventBy(product.getCreatedBy());
		
		productEvent.setEventDate(Date.from(Environment.clock().instant()));
		productEvent.setProduct(product);
		productEventRepository.save(productEvent);
	}

	public ProductEventRepository getProductEventRepository() {
		return productEventRepository;
	}

	public void setProductEventRepository(ProductEventRepository productEventRepository) {
		this.productEventRepository = productEventRepository;
	}

}
