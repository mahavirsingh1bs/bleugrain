package com.carefarm.prakrati.events;

import com.carefarm.prakrati.entity.Product;

public class ProductAddEvent implements PrakratiEvent<Product> {

	private Product product;
	
	public ProductAddEvent(Product product) {
		this.product = product;
	}
	
	@Override
	public Class<ProductAddEvent> getType() {
		return ProductAddEvent.class;
	}

	@Override
	public Product getPayload() {
		return product;
	}

}
