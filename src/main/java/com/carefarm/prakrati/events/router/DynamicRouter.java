package com.carefarm.prakrati.events.router;

import java.util.HashMap;
import java.util.Map;

import com.carefarm.prakrati.events.PrakratiEvent;
import com.carefarm.prakrati.events.handler.EventHandler;

public class DynamicRouter<E extends PrakratiEvent<T>, T> {
	
	private Map<Class<E>, EventHandler<E, T>> handlers;
	
	public DynamicRouter() {
		handlers = new HashMap<>();
	}
	
	public void registerHandler(Class<E> eventClass, EventHandler<E, T> eventHandler) {
		handlers.put(eventClass, eventHandler);
	}
	
	public void dispatch(E event) {
		handlers.get(event.getClass()).handle(event);
	}
}
