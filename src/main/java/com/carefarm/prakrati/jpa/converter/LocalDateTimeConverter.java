package com.carefarm.prakrati.jpa.converter;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class LocalDateTimeConverter implements AttributeConverter<LocalDateTime, Date> {

	@Override
    public Date convertToDatabaseColumn(LocalDateTime date) {
        ZonedDateTime zonedDateTime = ZonedDateTime.of(date, ZoneId.systemDefault());
        return Date.from(Instant.from(zonedDateTime));
    }

    @Override
    public LocalDateTime convertToEntityAttribute(Date value) {
    	return LocalDateTime.ofInstant(value.toInstant(), ZoneId.systemDefault());
    }

}
