package com.carefarm.prakrati;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BleugrainApplication {

	public static void main(String[] args) throws Exception {
		SpringApplication.run(BleugrainApplication.class, args);
	}
	
}