package com.carefarm.prakrati.generators;

import org.apache.commons.lang3.RandomStringUtils;

public class RandomPasswdGenerator {
	private static final String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]}\\|;:\'\",<.>/?";
	
	public static String nextRandomPasswd() {
		return RandomStringUtils.random(15, characters);
	}
	
}
