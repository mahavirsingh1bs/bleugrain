package com.carefarm.prakrati.generators;

import java.util.Calendar;
import java.util.Random;

public class UserNoGenerator {
	
	public static String generateUserNo() {
		Calendar calendar = Calendar.getInstance();
		Random randomGenrtr = new Random(calendar.getTimeInMillis());
		Long userNo = Math.abs(randomGenrtr.nextLong());
		return userNo.toString();
	}
}
