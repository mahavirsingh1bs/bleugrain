package com.carefarm.prakrati.generators;

import java.util.Calendar;
import java.util.Random;

public class UniqueIdBuilder {

	public static String generateUniqueId() {
		Calendar calendar = Calendar.getInstance();
		Random randomGenrtr = new Random(calendar.getTimeInMillis());
		Long uniqueId = Math.abs(randomGenrtr.nextLong());
		return uniqueId.toString();
	}
}
