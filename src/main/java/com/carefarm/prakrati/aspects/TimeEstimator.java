package com.carefarm.prakrati.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.util.StopWatch;

@Aspect
public class TimeEstimator {
	
	@Around("com.carefarm.prakrati.aspects.SystemArchitecture.dataAccessOperation()")
	public Object estimateProcessingTime(ProceedingJoinPoint pjp) throws Throwable {
		Object retVal = null;
		StopWatch stopWatch = new StopWatch(getClass().getName());
		try {
			stopWatch.start(pjp.toString());
			retVal = pjp.proceed();
		} finally {
			stopWatch.stop();
			System.out.println(stopWatch.prettyPrint());
		}
		return retVal;
	}
	
}
