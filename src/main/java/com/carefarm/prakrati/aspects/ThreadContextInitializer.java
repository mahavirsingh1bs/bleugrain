package com.carefarm.prakrati.aspects;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.holder.ThreadContext;

@Aspect
public class ThreadContextInitializer {
	
	@Autowired
	private Authentication authentication;
	
	@Around("com.carefarm.prakrati.aspects.SystemArchitecture.businessService()")
	public Object estimateProcessingTime(ProceedingJoinPoint pjp) throws Throwable {
		DomainUsernamePasswordAuthenticationToken token = 
    			(DomainUsernamePasswordAuthenticationToken )authentication;
    	User user = (User )token.getPrincipal();
    	ThreadContext.setCurrentUser(user);
		Object retVal = pjp.proceed();
		ThreadContext.setCurrentUser(null);
		return retVal;
	}
}
