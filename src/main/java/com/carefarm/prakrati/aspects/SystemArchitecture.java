package com.carefarm.prakrati.aspects;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;

@Aspect
public class SystemArchitecture {
	
	@Pointcut("within(com.carefarm.prakrati.web..*)")
	public void inWebLayer() { }
	
	@Pointcut("within(com.carefarm.prakrati.service..*)")
	public void inServiceLayer() { }
	
	@Pointcut("within(com.carefarm.prakrati.repository..*)")
	public void inRepositoryLayer() { }
	
	@Pointcut("execution(* com.carefarm.prakrati..service..*(..))")
	public void businessService() { }
	
	@Pointcut("execution(* com.carefarm.prakrati..repository..*(..))")
	public void dataAccessOperation() { }
	
	@Pointcut("execution(* com.carefarm.prakrati..service.SearchService.findProductDetail(..))")
	public void findProductDetails() { }
}
