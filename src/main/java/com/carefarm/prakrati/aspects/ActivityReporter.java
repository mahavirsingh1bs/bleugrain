package com.carefarm.prakrati.aspects;

import static com.carefarm.prakrati.common.util.Constants.USER_NO;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.carefarm.prakrati.constants.Predicates;
import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.entity.Activity;
import com.carefarm.prakrati.repository.ActivityRepository;
import com.carefarm.prakrati.repository.ProductRepository;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.util.ActivityType;
import com.carefarm.prakrati.web.bean.ProductBean;

@Component
@Aspect
public class ActivityReporter {
	
	@Autowired
	private HttpSession httpSession;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ActivityRepository activityRepository;
	
	@AfterReturning(
			pointcut = "com.carefarm.prakrati.aspects.SystemArchitecture.findProductDetails()",
			returning = "productDetail")
	public void logActivityThatProductHasBeenVisited(Map<String, Object> productDetail) throws Throwable {
		String userNo = (String)httpSession.getAttribute(USER_NO);
		ProductBean product = (ProductBean)productDetail.get("product");
		if (Predicates.notNull.test(userNo) && 
				!StringUtils.equalsIgnoreCase(userNo, product.getOwner().getUserNo())) {
			Activity activity = new Activity();
			activity.setActivity(ActivityType.VIEWD_PRODUCT);
			activity.setProduct(productRepository.findByUniqueId(product.getUniqueId()));
			activity.setUser(userRepository.findByUserNo(userNo));
			activity.setActivityDate(Date.from(Environment.clock().instant()));
			activityRepository.save(activity);
		}
	}
}
