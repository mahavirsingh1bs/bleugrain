ALTER TABLE CF_PRODUCT_HISTORY
	ADD COLUMN IS_TRANSPORT BOOLEAN DEFAULT FALSE,
	ADD COLUMN VEHICLE_NO VARCHAR(50) DEFAULT NULL,
	ADD COLUMN DRIVER_ID BIGINT DEFAULT NULL;