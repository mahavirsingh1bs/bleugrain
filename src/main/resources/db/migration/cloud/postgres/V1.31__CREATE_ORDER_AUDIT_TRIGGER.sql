DROP TABLE CF_ORDER_HISTORY;

create table CF_ORDER_HISTORY (
    operation           CHAR(1) NOT NULL, 
    stamp               TIMESTAMP NOT NULL, 
    userid              TEXT NOT NULL, 
     id    bigint not null,
     amount_paid         numeric(192) default NULL,
     is_archived         boolean,
     comments            varchar(255) default NULL,
     assigned_on         timestamp,
     created_on          timestamp,
     modified_on         timestamp,
     delivery_date       timestamp,
     order_date          timestamp,
     order_no            varchar(255) not null,
     shipping_cost       numeric(192) default NULL,
     subtotal            numeric(192) default NULL,
     total_price         numeric(192) default NULL,
     assigned_to         bigint,
     billing_address_id  bigint,
     company_id          bigint,
     created_by          bigint,
     deliver_by          bigint,
     fulfilled_by        bigint,
     modified_by         bigint,
     shipping_address_id bigint,
     status_id           bigint,
     user_id             bigint,
     is_mocked           boolean,
     assigned_by         bigint,
     obj_version         integer default 0
	);

CREATE OR REPLACE FUNCTION audit_order_details() RETURNS TRIGGER
AS 
$body$
	BEGIN
		IF (TG_OP = 'DELETE') THEN
			INSERT INTO CF_ORDER_HISTORY SELECT 'D', now(), user, OLD.*;
			RETURN OLD;
		ELSIF (TG_OP = 'UPDATE') THEN
			INSERT INTO CF_ORDER_HISTORY SELECT 'U', now(), user, NEW.*;
			RETURN NEW;
		ELSIF (TG_OP = 'INSERT') THEN
			INSERT INTO CF_ORDER_HISTORY SELECT 'I', now(), user, NEW.*;
			RETURN NEW;
		END IF;
		RETURN NULL;
	END;
$body$ 
LANGUAGE plpgsql;

CREATE TRIGGER audit_order 
	AFTER INSERT OR UPDATE OR DELETE ON CF_ORDER
	FOR EACH ROW EXECUTE PROCEDURE audit_order_details();