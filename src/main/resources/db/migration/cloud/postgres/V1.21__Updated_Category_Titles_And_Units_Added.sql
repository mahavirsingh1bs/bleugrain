UPDATE CF_CATEGORY SET NAME = 'Chemical Fertilizer' WHERE ID = 90;
UPDATE CF_CATEGORY SET NAME = 'Organic Fertilizer' WHERE ID = 91;
UPDATE CF_CATEGORY SET NAME = 'Liquid Fertilizer' WHERE ID = 92;
UPDATE CF_CATEGORY SET NAME = 'Special Fertilizer' WHERE ID = 93;
UPDATE CF_CATEGORY SET NAME = 'Insecticide Fertilizer' WHERE ID = 94;
UPDATE CF_CATEGORY SET NAME = 'Organic Manure' WHERE ID = 95;
UPDATE CF_CATEGORY SET NAME = 'Compost' WHERE ID = 96;

UPDATE CF_MENUITEM SET NAME = 'Chemical Fertilizer' WHERE ID = 90;
UPDATE CF_MENUITEM SET NAME = 'Organic Fertilizer' WHERE ID = 91;
UPDATE CF_MENUITEM SET NAME = 'Liquid Fertilizer' WHERE ID = 92;
UPDATE CF_MENUITEM SET NAME = 'Special Fertilizer' WHERE ID = 93;
UPDATE CF_MENUITEM SET NAME = 'Insecticide Fertilizer' WHERE ID = 94;
UPDATE CF_MENUITEM SET NAME = 'Organic Manure' WHERE ID = 95;
UPDATE CF_MENUITEM SET NAME = 'Compost' WHERE ID = 96;

-- A New Menuitem in Machinery Category
INSERT INTO CF_CATEGORY(id, is_archived, created_on, modified_on, short_desc, name, unique_id, created_by, default_image_id, modified_by, parent_category_id) 
	VALUES 
	(202,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Fertilizing and Hay Making','f5182149-e88c-6912-12f6-a433c89d903d',1,214,1,74);

INSERT INTO CF_MENUITEM(ID, OBJ_VERSION, UNIQUE_ID, NAME, KEYWORDS, CATEGORY_ID, PARENT_MENUITEM_ID, DISPLAY_INDEX, IS_ACTIVE, IS_ARCHIVED, CREATED_BY, CREATED_ON, MODIFIED_BY, MODIFIED_ON)
	VALUES
	(202, 0, 'd01d8239-6eb2-ed3f-ca06-7801cbdad148', 'Fertilizing and Hay Making', NULL, 202, 74, 3, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL);
	
UPDATE CF_MENUITEM SET DISPLAY_INDEX = DISPLAY_INDEX + 1 WHERE ID >= 192 AND ID <= 201;

INSERT INTO CF_UNIT 
	VALUES 
	(5,FALSE,'IN',FALSE,'Acre','Acre'),
	(6,FALSE,'IN',FALSE,'Hectare','Hectare'),
	(7,FALSE,'IN',FALSE,'Inch','inch'),
	(8,FALSE,'IN',FALSE,'Foot','foot'),
	(9,FALSE,'IN',FALSE,'Millimetre','mm'),
	(10,FALSE,'IN',FALSE,'Centimetre','cm'),
	(11,FALSE,'IN',FALSE,'Metre','mtr'),
	(12,FALSE,'IN',FALSE,'Kilometre','Km'),
	(13,FALSE,'IN',FALSE,'Milligram','Mg'),
	(14,FALSE,'IN',FALSE,'Gram','Gm'),
	(-1,FALSE,'IN',FALSE,'None','None');
