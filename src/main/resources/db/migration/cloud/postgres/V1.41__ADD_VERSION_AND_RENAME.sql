ALTER TABLE CF_BUYER_WISHLIST ADD COLUMN OBJ_VERSION INTEGER DEFAULT 0;
ALTER TABLE CF_NOTIF_CATEGORY ADD COLUMN OBJ_VERSION INTEGER DEFAULT 0;
ALTER TABLE CF_SELLER_WISHLIST ADD COLUMN OBJ_VERSION INTEGER DEFAULT 0;