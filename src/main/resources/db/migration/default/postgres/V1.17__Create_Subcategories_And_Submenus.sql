ALTER TABLE CF_MENUITEM DROP CONSTRAINT cf_menuitem_name_key;

-- Cereals & Pulses subcategories & submenus
INSERT INTO CF_CATEGORY(id, is_archived, created_on, modified_on, short_desc, name, unique_id, created_by, default_image_id, modified_by, parent_category_id) 
	VALUES 
	(15,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Rices','541a69b0-63c9-49de-a995-f64ef7ea0987',1,214,1,1),
	(16,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Wheats','54298c67-537d-4601-b975-c56ca4053e84',1,215,1,1),
	(17,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Pulses','54395745-29ba-413f-a117-ffdb87a99e5a',1,216,1,1),
	(18,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Paddies','544d3e14-01f0-4c75-aa0e-8d74b245cb6e',1,217,1,1),
	(19,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Others','5450b7a6-bf08-4c27-b4a4-410b3d2212b2',1,218,1,1);

INSERT INTO CF_MENUITEM(ID, OBJ_VERSION, UNIQUE_ID, NAME, KEYWORDS, CATEGORY_ID, PARENT_MENUITEM_ID, DISPLAY_INDEX, IS_ACTIVE, IS_ARCHIVED, CREATED_BY, CREATED_ON, MODIFIED_BY, MODIFIED_ON)
	VALUES
	(11, 0, '392a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Rices', NULL, 15, 1, 3, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(12, 0, '356a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Wheats', NULL, 16, 1, 4, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(13, 0, '357a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Pulses', NULL, 17, 1, 5, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(14, 0, '358a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Paddies', NULL, 18, 1, 6, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(15, 0, '359a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Others', NULL, 19, 1, 7, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL);

-- Dry Fruits submenus
INSERT INTO CF_CATEGORY(id, is_archived, created_on, modified_on, short_desc, name, unique_id, created_by, default_image_id, modified_by, parent_category_id) 
	VALUES 
	(20,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Mixed Dry Fruits','546a69b0-63c9-49de-a995-f64ef7ea0987',1,214,1,13),
	(21,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Raisins','54798c67-537d-4601-b975-c56ca4053e84',1,215,1,13),
	(22,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Dates','548a69b0-63c9-49de-a995-f64ef7ea0987',1,214,1,13),
	(23,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Exotic Nuts','54998c67-537d-4601-b975-c56ca4053e84',1,215,1,13),
	(24,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Almonds','55095745-29ba-413f-a117-ffdb87a99e5a',1,216,1,13),
	(25,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Apricots','551d3e14-01f0-4c75-aa0e-8d74b245cb6e',1,217,1,13),
	(26,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Others','5520b7a6-bf08-4c27-b4a4-410b3d2212b2',1,218,1,13);

INSERT INTO CF_MENUITEM(ID, OBJ_VERSION, UNIQUE_ID, NAME, KEYWORDS, CATEGORY_ID, PARENT_MENUITEM_ID, DISPLAY_INDEX, IS_ACTIVE, IS_ARCHIVED, CREATED_BY, CREATED_ON, MODIFIED_BY, MODIFIED_ON)
	VALUES
	(16, 0, '360a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Mixed Dry Fruits', NULL, 20, 2, 1, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(17, 0, '361a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Raisins', NULL, 21, 2, 2, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(18, 0, '362a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Dates', NULL, 22, 2, 3, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(19, 0, '363a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Exotic Nuts', NULL, 23, 2, 4, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(20, 0, '364a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Almonds', NULL, 24, 2, 5, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(21, 0, '365a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Apricots', NULL, 25, 2, 6, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(22, 0, '366a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Others', NULL, 26, 2, 7, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL);

-- Fruits submenus
INSERT INTO CF_CATEGORY(id, is_archived, created_on, modified_on, short_desc, name, unique_id, created_by, default_image_id, modified_by, parent_category_id) 
	VALUES 
	(29,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Berries','555a69b0-63c9-49de-a995-f64ef7ea0987',1,214,1,3),
	(30,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Core Fruits','55698c67-537d-4601-b975-c56ca4053e84',1,215,1,3),
	(31,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Pits','55795745-29ba-413f-a117-ffdb87a99e5a',1,216,1,3),
	(32,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Citrus Fruits','558d3e14-01f0-4c75-aa0e-8d74b245cb6e',1,217,1,3),
	(33,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Melons','5590b7a6-bf08-4c27-b4a4-410b3d2212b2',1,218,1,3),
	(34,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Tropical Fruits','5600b7a6-bf08-4c27-b4a4-410b3d2212b2',1,218,1,3),
	(35,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Others','5610b7a6-bf08-4c27-b4a4-410b3d2212b2',1,218,1,3);

INSERT INTO CF_MENUITEM(ID, OBJ_VERSION, UNIQUE_ID, NAME, KEYWORDS, CATEGORY_ID, PARENT_MENUITEM_ID, DISPLAY_INDEX, IS_ACTIVE, IS_ARCHIVED, CREATED_BY, CREATED_ON, MODIFIED_BY, MODIFIED_ON)
	VALUES
	(23, 0, '367a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Berries', NULL, 29, 3, 3, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(24, 0, '368a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Core Fruits', NULL, 30, 3, 4, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(25, 0, '369a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Pits', NULL, 31, 3, 5, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(26, 0, '370a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Citrus Fruits', NULL, 32, 3, 6, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(27, 0, '371a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Melons', NULL, 33, 3, 7, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(28, 0, '372a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Tropical Fruits', NULL, 34, 3, 8, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(29, 0, '373a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Others', NULL, 35, 3, 9, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL);

-- Vegetables submenus
INSERT INTO CF_CATEGORY(id, is_archived, created_on, modified_on, short_desc, name, unique_id, created_by, default_image_id, modified_by, parent_category_id) 
	VALUES 
	(36,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Bulbs','562a69b0-63c9-49de-a995-f64ef7ea0987',1,214,1,4),
	(37,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Flowers','56398c67-537d-4601-b975-c56ca4053e84',1,215,1,4),
	(38,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Fruits','564a69b0-63c9-49de-a995-f64ef7ea0987',1,214,1,4),
	(39,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Fungi','56598c67-537d-4601-b975-c56ca4053e84',1,215,1,4),
	(40,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Leaves','56695745-29ba-413f-a117-ffdb87a99e5a',1,216,1,4),
	(41,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Roots','567d3e14-01f0-4c75-aa0e-8d74b245cb6e',1,217,1,4),
	(42,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Seeds','5680b7a6-bf08-4c27-b4a4-410b3d2212b2',1,218,1,4),
	(43,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Stems','5690b7a6-bf08-4c27-b4a4-410b3d2212b2',1,218,1,4),
	(44,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Tubers','5700b7a6-bf08-4c27-b4a4-410b3d2212b2',1,218,1,4),
	(45,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Others','5710b7a6-bf08-4c27-b4a4-410b3d2212b2',1,218,1,4);

INSERT INTO CF_MENUITEM(ID, OBJ_VERSION, UNIQUE_ID, NAME, KEYWORDS, CATEGORY_ID, PARENT_MENUITEM_ID, DISPLAY_INDEX, IS_ACTIVE, IS_ARCHIVED, CREATED_BY, CREATED_ON, MODIFIED_BY, MODIFIED_ON)
	VALUES
	(30, 0, '374a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Bulbs', NULL, 36, 5, 1, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(31, 0, '375a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Flowers', NULL, 37, 5, 2, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(32, 0, '376a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Fruits', NULL, 38, 5, 3, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(33, 0, '377a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Fungi', NULL, 39, 5, 4, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(34, 0, '378a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Leaves', NULL, 40, 5, 5, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(35, 0, '379a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Roots', NULL, 41, 5, 6, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(36, 0, '380a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Seeds', NULL, 42, 5, 7, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(37, 0, '381a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Stems', NULL, 43, 5, 8, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(38, 0, '382a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Tubers', NULL, 44, 5, 9, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(39, 0, '383a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Others', NULL, 45, 5, 10, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL);

-- Spices & Herbs Submenus
INSERT INTO CF_CATEGORY(id, is_archived, created_on, modified_on, short_desc, name, unique_id, created_by, default_image_id, modified_by, parent_category_id) 
	VALUES 
	(46,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Bulbs','572a69b0-63c9-49de-a995-f64ef7ea0987',1,214,1,3),
	(47,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Flowers','57398c67-537d-4601-b975-c56ca4053e84',1,215,1,3),
	(48,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Fruits','574a69b0-63c9-49de-a995-f64ef7ea0987',1,214,1,3),
	(49,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Fungi','57598c67-537d-4601-b975-c56ca4053e84',1,215,1,3);

INSERT INTO CF_MENUITEM(ID, OBJ_VERSION, UNIQUE_ID, NAME, KEYWORDS, CATEGORY_ID, PARENT_MENUITEM_ID, DISPLAY_INDEX, IS_ACTIVE, IS_ARCHIVED, CREATED_BY, CREATED_ON, MODIFIED_BY, MODIFIED_ON)
	VALUES
	(40, 0, '384a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Whole Spices', NULL, 46, 6, 1, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(41, 0, '385a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Ground Spices', NULL, 47, 6, 2, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(42, 0, '386a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Blended Spices', NULL, 48, 6, 3, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(43, 0, '387a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Other Powders', NULL, 49, 6, 4, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL);

-- Nuts & Oilseeds Submenus
INSERT INTO CF_CATEGORY(id, is_archived, created_on, modified_on, short_desc, name, unique_id, created_by, default_image_id, modified_by, parent_category_id) 
	VALUES 
	(50,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Bulbs','576a69b0-63c9-49de-a995-f64ef7ea0987',1,214,1,2),
	(51,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Flowers','57798c67-537d-4601-b975-c56ca4053e84',1,215,1,2),
	(52,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Fruits','578a69b0-63c9-49de-a995-f64ef7ea0987',1,214,1,2),
	(53,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Fungi','57998c67-537d-4601-b975-c56ca4053e84',1,215,1,2);

INSERT INTO CF_MENUITEM(ID, OBJ_VERSION, UNIQUE_ID, NAME, KEYWORDS, CATEGORY_ID, PARENT_MENUITEM_ID, DISPLAY_INDEX, IS_ACTIVE, IS_ARCHIVED, CREATED_BY, CREATED_ON, MODIFIED_BY, MODIFIED_ON)
	VALUES
	(44, 0, '388a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Culinary Nuts', NULL, 50, 4, 1, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(45, 0, '389a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Edible Seeds', NULL, 51, 4, 2, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(46, 0, '390a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Soyabeans', NULL, 52, 4, 3, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL),
	(47, 0, '391a9ddb-b9e8-a668-a296-a644d6bbaf79', 'Oats', NULL, 53, 4, 4, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL);

INSERT INTO CF_CATEGORY(id, is_archived, created_on, modified_on, short_desc, name, unique_id, created_by, default_image_id, modified_by, parent_category_id) 
	VALUES 
	(54,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Seasonal Grains','589a69b0-63c9-49de-a995-f64ef7ea0987',1,214,1,1),
	(55,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Non-Seasonal Grains','59098c67-537d-4601-b975-c56ca4053e84',1,215,1,1);

INSERT INTO CF_CATEGORY(id, is_archived, created_on, modified_on, short_desc, name, unique_id, created_by, default_image_id, modified_by, parent_category_id) 
	VALUES 
	(56,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Kashmiri Fruits','553a69b0-63c9-49de-a995-f64ef7ea0987',1,214,1,3),
	(57,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Punjabi Fruits','55498c67-537d-4601-b975-c56ca4053e84',1,215,1,3);

UPDATE CF_MENUITEM SET CATEGORY_ID = 54 WHERE ID = 7;
UPDATE CF_MENUITEM SET CATEGORY_ID = 55 WHERE ID = 8;

UPDATE CF_MENUITEM SET CATEGORY_ID = 56 WHERE ID = 9;
UPDATE CF_MENUITEM SET CATEGORY_ID = 57 WHERE ID = 10;

UPDATE CF_PRAKRATI_SEQUENCES SET NEXT_VALUE = 58 WHERE SEQUENCE_NAME = 'CF_CATEGORY';
UPDATE CF_PRAKRATI_SEQUENCES SET NEXT_VALUE = 47 WHERE SEQUENCE_NAME = 'CF_MENUITEM';