CREATE SEQUENCE IF NOT EXISTS PRODUCT_ID_SEQ 
	INCREMENT BY 1
	MINVALUE 0 
	MAXVALUE 9999999999
	START WITH 1
	CYCLE;

ALTER TABLE CF_PRODUCT ADD COLUMN PRODUCT_NO varchar(50);
UPDATE CF_PRODUCT SET PRODUCT_NO = CONCAT(TO_CHAR(NOW(), 'YYYYMMDD'), '-', LPAD(NEXTVAL('PRODUCT_ID_SEQ')::text, 10, '0'), '-', TO_CHAR(NOW(), 'HH24MISSMS'));
ALTER TABLE CF_PRODUCT ADD CONSTRAINT CF_PRODUCT_UNIQ_PROD_NO UNIQUE (PRODUCT_NO);