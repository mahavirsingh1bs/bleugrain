-- MySQL dump 10.13  Distrib 5.5.37, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: prakrati
-- ------------------------------------------------------
-- Server version	5.5.37-0ubuntu0.13.10.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

SET FOREIGN_KEY_CHECKS=0;

--
-- Dumping data for table `CF_GROUP`
--

LOCK TABLES `CF_GROUP` WRITE;
/*!40000 ALTER TABLE `CF_GROUP` DISABLE KEYS */;
INSERT INTO `CF_GROUP` VALUES (1,'\0','2016-07-25 00:00:00','2016-09-14 00:00:00','Admin','6ba87efa-c9ec-4a25-bcab-b8cbd69ff302',1,6),(2,'\0','2016-07-24 00:00:00','2016-08-15 00:00:00','Farmer','7527ca3b-3c16-4cd6-bda1-53ceca0f8ba5',2,9),(3,'\0','2016-07-23 00:00:00','2016-08-17 00:00:00','Retailer','48b340ad-d69e-43b3-9ae8-6c6d5eb6e0a0',3,7),(4,'\0','2016-07-22 00:00:00','2016-09-13 00:00:00','Company','323babe2-8f58-46f5-804b-7b56e76b48aa',4,6),(5,'\0','2016-07-21 00:00:00','2016-09-12 00:00:00','User','6c86c892-9eaa-42c8-b300-cc562629702e',5,9),(6,'\0','2016-07-20 00:00:00','2016-09-11 00:00:00','Driver','0f26fcb1-f244-4088-b625-6269076c4ab1',6,7),(7,'\0','2016-07-19 00:00:00','2016-09-10 00:00:00','Broker','9fc04f69-0a9b-40ef-9473-929284b0de59',7,6),(8,'\0','2016-07-18 00:00:00','2016-09-09 00:00:00','Agent','69b091ce-ffb5-4f6a-b3c9-a9b9a446600d',8,9),(9,'\0','2016-07-17 00:00:00','2016-09-08 00:00:00','Company Admin','1bd0122b-4a68-48f1-9a49-77578ac949ed',9,7),(10,'\0','2016-07-16 00:00:00','2016-09-07 00:00:00','Executive','5bbd3cb9-42b9-4c00-b99f-ec40999e9fe0',10,6),(11,'\0','2016-07-15 00:00:00','2016-09-06 00:00:00','CTMember','223bce8a-cc1c-48f9-9ae5-c53271b92955',11,9),(12,'\0','2016-07-14 00:00:00','2016-09-05 00:00:00','STMember','2f97fafa-ff3a-4ab5-91c2-22aa5acd7799',12,7),(13,'\0','2016-07-13 00:00:00','2016-09-04 00:00:00','Customer','ebbc40ad-f6be-447a-9d91-0d4779bee08d',13,6),(14,'\0','2016-07-12 00:00:00','2016-09-03 00:00:00','Region Admin','ebbc40ad-f6be-447a-9d91-0d4779bee08e',14,9),(15,'\0','2016-07-11 00:00:00','2016-09-02 00:00:00','Region Manager','ebbc40ad-f6be-447a-9d91-0d4779bee08f',15,7),(16,'\0','2016-07-10 00:00:00','2016-09-01 00:00:00','Dispatcher','ebbc40ad-f6be-447a-9d91-0d4779bee08g',16,6);
/*!40000 ALTER TABLE `CF_GROUP` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CF_ROLE`
--

LOCK TABLES `CF_ROLE` WRITE;
/*!40000 ALTER TABLE `CF_ROLE` DISABLE KEYS */;
INSERT INTO `CF_ROLE` VALUES (1,'\0','2016-07-25 00:00:00','2016-09-14 00:00:00','','ROLE_ADMIN',1,6),(2,'\0','2016-07-24 00:00:00','2016-08-15 00:00:00','','ROLE_FARMER',2,9),(3,'\0','2016-07-23 00:00:00','2016-08-17 00:00:00','','ROLE_RETAILER',3,7),(4,'\0','2016-07-22 00:00:00','2016-09-13 00:00:00','','ROLE_COMPANY',4,6),(5,'\0','2016-07-21 00:00:00','2016-09-12 00:00:00','','ROLE_USER',5,9),(6,'\0','2016-07-20 00:00:00','2016-09-11 00:00:00','','ROLE_DRIVER',6,7),(7,'\0','2016-07-19 00:00:00','2016-09-10 00:00:00','','ROLE_BROKER',7,6),(8,'\0','2016-07-18 00:00:00','2016-09-09 00:00:00','','ROLE_AGENT',8,9),(9,'\0','2016-07-17 00:00:00','2016-09-08 00:00:00','','ROLE_COMPANY_ADMIN',9,7),(10,'\0','2016-07-16 00:00:00','2016-09-07 00:00:00','','ROLE_EXECUTIVE',10,6),(11,'\0','2016-07-15 00:00:00','2016-09-06 00:00:00','','ROLE_CTMEMBER',11,9),(12,'\0','2016-07-14 00:00:00','2016-09-05 00:00:00','','ROLE_STMEMBER',12,7),(13,'\0','2016-07-13 00:00:00','2016-09-04 00:00:00','','ROLE_CUSTOMER',13,6),(14,'\0','2016-07-12 00:00:00','2016-09-03 00:00:00','','ROLE_REGION_ADMIN',14,9),(15,'\0','2016-07-11 00:00:00','2016-09-02 00:00:00','','ROLE_REGION_MANAGER',15,7),(16,'\0','2016-07-10 00:00:00','2016-09-01 00:00:00','','ROLE_DISPATCHER',16,6),(17,'\0','2016-07-09 00:00:00','2016-08-31 00:00:00','','ROLE_EMPLOYEE',17,9);
/*!40000 ALTER TABLE `CF_ROLE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CF_GROUP_ROLE`
--

LOCK TABLES `CF_GROUP_ROLE` WRITE;
/*!40000 ALTER TABLE `CF_GROUP_ROLE` DISABLE KEYS */;
INSERT INTO `CF_GROUP_ROLE` VALUES (1,1),(2,2),(3,3),(4,4),(1,5),(2,5),(3,5),(4,5),(5,5),(6,5),(7,5),(8,5),(9,5),(10,5),(11,5),(12,5),(13,5),(14,5),(15,5),(16,5),(6,6),(7,7),(8,8),(9,9),(10,10),(11,11),(12,12),(13,13),(14,14),(15,15),(16,16),(10,17),(11,17),(12,17),(13,17),(14,17),(15,17),(16,17);
/*!40000 ALTER TABLE `CF_GROUP_ROLE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CF_LANGUAGE`
--

LOCK TABLES `CF_LANGUAGE` WRITE;
/*!40000 ALTER TABLE `CF_LANGUAGE` DISABLE KEYS */;
INSERT INTO `CF_LANGUAGE` VALUES (1,'\0','en',NULL,NULL,'English',NULL,NULL),(2,'\0','hi',NULL,NULL,'Hindi',NULL,NULL),(3,'\0','es',NULL,NULL,'Spanish',NULL,NULL);
/*!40000 ALTER TABLE `CF_LANGUAGE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CF_COUNTRY`
--

LOCK TABLES `CF_COUNTRY` WRITE;
/*!40000 ALTER TABLE `CF_COUNTRY` DISABLE KEYS */;
INSERT INTO `CF_COUNTRY` VALUES (1,'\0','IN',NULL,NULL,'India',NULL,NULL),(2,'\0','US',NULL,NULL,'United States',NULL,NULL),(3,'\0','ZA',NULL,NULL,'United Kingdom',NULL,NULL),(4,'\0','CH',NULL,NULL,'China',NULL,NULL),(5,'\0','IR',NULL,NULL,'Ireland',NULL,NULL);
/*!40000 ALTER TABLE `CF_COUNTRY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CF_STATE`
--

LOCK TABLES `CF_STATE` WRITE;
/*!40000 ALTER TABLE `CF_STATE` DISABLE KEYS */;
INSERT INTO `CF_STATE` VALUES (1,'\0',NULL,NULL,'Jammu & Kashmir',1,NULL,NULL),(2,'\0',NULL,NULL,'Himachal Pradesh',1,NULL,NULL),(3,'\0',NULL,NULL,'Punjab',1,NULL,NULL),(4,'\0',NULL,NULL,'Haryana',1,NULL,NULL),(5,'\0',NULL,NULL,'Delhi',1,NULL,NULL),(6,'\0',NULL,NULL,'Uttar Pradesh',1,NULL,NULL),(7,'\0',NULL,NULL,'Karnatka',1,NULL,NULL),(8,'\0',NULL,NULL,'New York',2,NULL,NULL),(9,'\0',NULL,NULL,'Washington',2,NULL,NULL);
/*!40000 ALTER TABLE `CF_STATE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CF_EXPIRY_MONTH`
--

LOCK TABLES `CF_EXPIRY_MONTH` WRITE;
/*!40000 ALTER TABLE `CF_EXPIRY_MONTH` DISABLE KEYS */;
INSERT INTO `CF_EXPIRY_MONTH` VALUES (1,'\0','01','JAN'),(2,'\0','02','FEB'),(3,'\0','03','MAR'),(4,'\0','04','APR'),(5,'\0','05','MAY'),(6,'\0','06','JUN'),(7,'\0','07','JUL'),(8,'\0','08','AUG'),(9,'\0','09','SEP'),(10,'\0','10','OCT'),(11,'\0','11','NOV'),(12,'\0','12','DEC');
/*!40000 ALTER TABLE `CF_EXPIRY_MONTH` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CF_EXPIRY_YEAR`
--

LOCK TABLES `CF_EXPIRY_YEAR` WRITE;
/*!40000 ALTER TABLE `CF_EXPIRY_YEAR` DISABLE KEYS */;
INSERT INTO `CF_EXPIRY_YEAR` VALUES (1,'\0','2015'),(2,'\0','2016'),(3,'\0','2017'),(4,'\0','2018'),(5,'\0','2019'),(6,'\0','2020'),(7,'\0','2021'),(8,'\0','2022'),(9,'\0','2023'),(10,'\0','2024'),(11,'\0','2025'),(12,'\0','2026'),(13,'\0','2027'),(14,'\0','2028');
/*!40000 ALTER TABLE `CF_EXPIRY_YEAR` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CF_PRODUCT_STATUS`
--

LOCK TABLES `CF_PRODUCT_STATUS` WRITE;
/*!40000 ALTER TABLE `CF_PRODUCT_STATUS` DISABLE KEYS */;
INSERT INTO `CF_PRODUCT_STATUS` VALUES (1,'\0','ADDED_NEW','Added New'),(2,'\0','ONCE_PRICED','Once Priced'),(3,'\0','SOLD_OUT','Sold Out'),(4,'\0','DELETED','Deleted'),(5,'\0','AVAILABLE','Available'),(6,'\0','OUT_OF_STOCK','Out of Stock');
/*!40000 ALTER TABLE `CF_PRODUCT_STATUS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CF_PRODUCT_TYPE`
--

LOCK TABLES `CF_PRODUCT_TYPE` WRITE;
/*!40000 ALTER TABLE `CF_PRODUCT_TYPE` DISABLE KEYS */;
INSERT INTO `CF_PRODUCT_TYPE` VALUES (1,'\0','Yearly','YEARLY'),(2,'\0','Half Yearly','HALF_YEARLY'),(3,'\0','Quarterly','QUARTERLY'),(4,'\0','Monthly','MONTHLY');
/*!40000 ALTER TABLE `CF_PRODUCT_TYPE` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CF_RATING`
--

LOCK TABLES `CF_RATING` WRITE;
/*!40000 ALTER TABLE `CF_RATING` DISABLE KEYS */;
INSERT INTO `CF_RATING` VALUES (1,'\0',0.00),(2,'\0',0.50),(3,'\0',1.00),(4,'\0',1.50),(5,'\0',2.00),(6,'\0',2.50),(7,'\0',3.00),(8,'\0',3.50),(9,'\0',4.00),(10,'\0',4.50),(11,'\0',5.00);
/*!40000 ALTER TABLE `CF_RATING` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CF_UNIT`
--

LOCK TABLES `CF_UNIT` WRITE;
/*!40000 ALTER TABLE `CF_UNIT` DISABLE KEYS */;
INSERT INTO `CF_UNIT` VALUES (1,'\0','IN','','Quintal','Q'),(2,'\0','IN','\0','Kilogram','Kg'),(3,'\0','IN','\0','Ton','T'),(4,'\0','IN','\0','Metric Ton','Mt');
/*!40000 ALTER TABLE `CF_UNIT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CF_CURRENCY`
--

LOCK TABLES `CF_CURRENCY` WRITE;
/*!40000 ALTER TABLE `CF_CURRENCY` DISABLE KEYS */;
INSERT INTO `CF_CURRENCY` VALUES (1,'\0','US','DLR','Dollar','$'),(2,'\0','IN','Rs.','Rupee','?'),(3,'\0','IR','PND','Pound','£'),(4,'\0','GB','EUR','Euro','€');
/*!40000 ALTER TABLE `CF_CURRENCY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CF_DEMAND_STATUS`
--

LOCK TABLES `CF_DEMAND_STATUS` WRITE;
/*!40000 ALTER TABLE `CF_DEMAND_STATUS` DISABLE KEYS */;
INSERT INTO `CF_DEMAND_STATUS` VALUES (1,'\0','OPEN','Open'),(2,'\0','CLOSED','Closed'),(3,'\0','TENTATIVE','Tentative'),(4,'\0','FULFILLED','Fulfilled');
/*!40000 ALTER TABLE `CF_DEMAND_STATUS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CF_ORDER_STATUS`
--

LOCK TABLES `CF_ORDER_STATUS` WRITE;
/*!40000 ALTER TABLE `CF_ORDER_STATUS` DISABLE KEYS */;
INSERT INTO `CF_ORDER_STATUS` VALUES (1,'\0','PLACED','Placed'),(2,'\0','SHIPPED','Shipped'),(3,'\0','DISPATCHED','Dispatched'),(4,'\0','DELIVERED','Delivered');
/*!40000 ALTER TABLE `CF_ORDER_STATUS` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CF_CATEGORY`
--

LOCK TABLES `CF_CATEGORY` WRITE;
/*!40000 ALTER TABLE `CF_CATEGORY` DISABLE KEYS */;
INSERT INTO `CF_CATEGORY` VALUES (1,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum.','Cereals and Pulses','539a69b0-63c9-49de-a995-f64ef7ea0987',1,214,1,NULL),(2,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum.','Nuts and Oilseeds','d1198c67-537d-4601-b975-c56ca4053e84',1,215,1,NULL),(3,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum.','Spices and Herbs','00c95745-29ba-413f-a117-ffdb87a99e5a',1,216,1,NULL),(4,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum.','Vegetables','ed5d3e14-01f0-4c75-aa0e-8d74b245cb6e',1,217,1,NULL),(5,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum.','Fruits','2710b7a6-bf08-4c27-b4a4-410b3d2212b2',1,218,1,NULL),(6,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum.','Cereals','8cf650be-f6d6-45e2-b3d4-2cd7991b1435',1,219,1,1),(7,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum.','Pulses','ad72ada3-935f-4439-bb84-83ededb204ff',1,220,1,1),(8,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum.','Nuts','e159103c-80f4-49f3-8497-9c043dc3d66d',1,221,1,1),(9,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum.','Oilseeds','383f0ebf-d608-42f8-8211-30dr5dcb08d7',1,222,1,1),(10,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum.','Spices','383f0ebf-d608-42f8-8211-30ec5dcb08d7',1,222,1,3),(11,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum.','Herbs','383f0ebf-d608-42f8-8211-3rhc5dcb08d7',1,222,1,3),(12,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum.','Flour','78824116-f4f5-4e0a-89ff-0d95a313cad9',1,220,1,1);
/*!40000 ALTER TABLE `CF_CATEGORY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CF_NOTIF_CATEGORY`
--

LOCK TABLES `CF_NOTIF_CATEGORY` WRITE;
/*!40000 ALTER TABLE `CF_NOTIF_CATEGORY` DISABLE KEYS */;
INSERT INTO `CF_NOTIF_CATEGORY` VALUES (1,'\0','EMAIL',NULL,NULL,'E-Mail',NULL,NULL,NULL),(2,'\0','FOLLOWING_YOU',NULL,NULL,'Following You',NULL,NULL,NULL),(3,'\0','PRODUCT_REVIEWED',NULL,NULL,'Product Reviewed',NULL,NULL,NULL),(4,'\0','DEMAND_RAISED',NULL,NULL,'Demand Raised',NULL,NULL,NULL),(5,'\0','PRODUCT_ADDED',NULL,NULL,'Product Added',NULL,NULL,NULL),(6,'\0','PRODUCT_UPDATED',NULL,NULL,'Product Updated',NULL,NULL,NULL),(7,'\0','PRODUCT_SOLDOUT',NULL,NULL,'Product SoldOut',NULL,NULL,NULL),(8,'\0','ORDER_PLACED',NULL,NULL,'Order Placed',NULL,NULL,NULL),(9,'\0','ORDER_SHIPPED',NULL,NULL,'Order Shipped',NULL,NULL,NULL),(10,'\0','ORDER_DELIVERED',NULL,NULL,'Order Delivered',NULL,NULL,NULL),(11,'\0','FOLLOWING_PRODUCT',NULL,NULL,'Following Product',NULL,NULL,NULL),(12,'\0','USER_REGISTERED',NULL,NULL,'User Registered',NULL,NULL,NULL),(13,'\0','USER_CREATED',NULL,NULL,'User Created',NULL,NULL,NULL),(14,'\0','ORDER_ASSIGNED',NULL,NULL,'Order Assigned',NULL,NULL,NULL),(15,'\0','ORDER_DISPATCHED',NULL,NULL,'Order Dispatched',NULL,NULL,NULL),(16,'\0','PRODUCT_ASSIGNED',NULL,NULL,'Product Assigned',NULL,NULL,NULL),(17,'\0','DEMAND_ASSIGNED',NULL,NULL,'Demand Assigned',NULL,NULL,NULL),(18,'\0','DEMAND_FULFILLED',NULL,NULL,'Demand Fulfilled',NULL,NULL,NULL);
/*!40000 ALTER TABLE `CF_NOTIF_CATEGORY` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CF_TAG`
--

LOCK TABLES `CF_TAG` WRITE;
/*!40000 ALTER TABLE `CF_TAG` DISABLE KEYS */;
INSERT INTO `CF_TAG` VALUES (1,'\0',NULL,NULL,NULL,'Cereals',NULL,NULL),(2,'\0',NULL,NULL,NULL,'Pulses',NULL,NULL),(3,'\0',NULL,NULL,NULL,'Nuts',NULL,NULL),(4,'\0',NULL,NULL,NULL,'Oilseeds',NULL,NULL),(5,'\0',NULL,NULL,NULL,'Dry Fruits',NULL,NULL),(6,'\0',NULL,NULL,NULL,'Fruits',NULL,NULL),(7,'\0',NULL,NULL,NULL,'Vegetables',NULL,NULL),(8,'\0',NULL,NULL,NULL,'Spices',NULL,NULL),(9,'\0',NULL,NULL,NULL,'Herbs',NULL,NULL),(10,'\0',NULL,NULL,NULL,'Wheat',NULL,NULL),(11,'\0',NULL,NULL,NULL,'Rice',NULL,NULL);
/*!40000 ALTER TABLE `CF_TAG` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CF_BANK`
--

LOCK TABLES `CF_BANK` WRITE;
/*!40000 ALTER TABLE `CF_BANK` DISABLE KEYS */;
INSERT INTO `CF_BANK` VALUES (1,'\0',NULL,'2015-02-15 00:00:00','2015-02-22 00:00:00','AXIS Bank',NULL,NULL,1,1),(2,'\0',NULL,'2015-02-15 00:00:00','2015-02-22 00:00:00','ICICI Bank',NULL,NULL,1,1),(3,'\0',NULL,'2015-02-15 00:00:00','2015-02-22 00:00:00','HDFC Bank',NULL,NULL,1,1),(4,'\0',NULL,'2015-02-15 00:00:00','2015-02-22 00:00:00','State Bank of India',NULL,NULL,1,1),(5,'\0',NULL,'2015-02-15 00:00:00','2015-02-22 00:00:00','Bank of Baroda',NULL,NULL,1,1),(6,'\0',NULL,'2015-02-15 00:00:00','2015-02-22 00:00:00','Citi Bank',NULL,NULL,1,1),(7,'\0',NULL,'2015-02-15 00:00:00','2015-02-22 00:00:00','Canara Bank',NULL,NULL,1,1),(8,'\0',NULL,'2015-02-15 00:00:00','2015-02-22 00:00:00','Bank of India',NULL,NULL,1,1),(9,'\0',NULL,'2015-02-15 00:00:00','2015-02-22 00:00:00','Syndicate Bank',NULL,NULL,1,1),(10,'\0',NULL,'2015-02-15 00:00:00','2015-02-22 00:00:00','Punjab National Bank',NULL,NULL,1,1);
/*!40000 ALTER TABLE `CF_BANK` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CF_PAYMENT_CARD`
--

LOCK TABLES `CF_PAYMENT_CARD` WRITE;
/*!40000 ALTER TABLE `CF_PAYMENT_CARD` DISABLE KEYS */;
INSERT INTO `CF_PAYMENT_CARD` VALUES (1,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','American Express',1,NULL,1),(2,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','Master Card',1,NULL,1),(3,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','VISA',1,NULL,1),(4,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','Rupay',1,NULL,1),(5,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','Maestro',1,NULL,1);
/*!40000 ALTER TABLE `CF_PAYMENT_CARD` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CF_PAYMENT_METHOD`
--

LOCK TABLES `CF_PAYMENT_METHOD` WRITE;
/*!40000 ALTER TABLE `CF_PAYMENT_METHOD` DISABLE KEYS */;
INSERT INTO `CF_PAYMENT_METHOD` VALUES (1,'\0','2015-02-12 00:00:00',NULL,'Credit Card','',1,NULL,NULL),(2,'\0','2015-02-12 00:00:00',NULL,'Debit Card','\0',1,NULL,NULL),(3,'\0','2015-02-12 00:00:00',NULL,'Mobile Banking','\0',1,NULL,NULL),(4,'\0','2015-02-12 00:00:00',NULL,'Net Banking','\0',1,NULL,NULL);
/*!40000 ALTER TABLE `CF_PAYMENT_METHOD` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CF_DESIGNATION`
--

LOCK TABLES `CF_DESIGNATION` WRITE;
/*!40000 ALTER TABLE `CF_DESIGNATION` DISABLE KEYS */;
INSERT INTO `CF_DESIGNATION` VALUES (1,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','Chair Person',1,1),(2,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','Head Chair Person',1,1),(3,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','Sales Head',1,1),(4,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','Sales Lead',1,1),(5,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','Content Head',1,1),(6,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','Content Lead',1,1),(7,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','Field Head',1,1),(8,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','Field Lead',1,1),(9,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','Region Administrator',1,1),(10,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','Delivery Manager',1,1),(11,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','Dispatcher',1,1);
/*!40000 ALTER TABLE `CF_DESIGNATION` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CF_DEPARTMENT`
--

LOCK TABLES `CF_DEPARTMENT` WRITE;
/*!40000 ALTER TABLE `CF_DEPARTMENT` DISABLE KEYS */;
INSERT INTO `CF_DEPARTMENT` VALUES (1,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','Sales',1,1),(2,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','Content',1,1),(3,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','Field',1,1),(4,'\0','2015-02-15 00:00:00','2015-02-22 00:00:00','Delivery',1,1);
/*!40000 ALTER TABLE `CF_DEPARTMENT` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping data for table `CF_PLAN`
--

LOCK TABLES `CF_PLAN` WRITE;
/*!40000 ALTER TABLE `CF_PLAN` DISABLE KEYS */;
INSERT INTO `CF_PLAN` VALUES (1,'DOLLAR','Prakrati Pro',1,39.00,'MONTHLY'),(2,'DOLLAR','Teams',10,399.00,'YEARLY'),(3,'DOLLAR','Enterprise',-1,999.00,'YEARLY');
/*!40000 ALTER TABLE `CF_PLAN` ENABLE KEYS */;
UNLOCK TABLES;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

SET FOREIGN_KEY_CHECKS=1;

-- Dump completed on 2016-09-25 16:34:13
