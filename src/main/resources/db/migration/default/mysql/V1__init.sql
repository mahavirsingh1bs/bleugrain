
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `CF_ACCOUNT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_ACCOUNT` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACCOUNT_NO` varchar(255) DEFAULT NULL,
  `BANK_NAME` varchar(255) DEFAULT NULL,
  `BRANCH` varchar(255) DEFAULT NULL,
  `IFSC_CODE` varchar(255) DEFAULT NULL,
  `IS_ACTIVE` bit(1) DEFAULT NULL,
  `IS_PRIMARY` bit(1) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `OWNER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_1qp1fs4e9offg0rojtkj3x3j7` (`OWNER_ID`),
  CONSTRAINT `FK_1qp1fs4e9offg0rojtkj3x3j7` FOREIGN KEY (`OWNER_ID`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_ACCOUNT_STATUS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_ACCOUNT_STATUS` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_ACTIVITY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_ACTIVITY` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACTIVITY` int(11) DEFAULT NULL,
  `ACTIVITY_DATE` datetime DEFAULT NULL,
  `DEMAND_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_ID` bigint(20) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_jvipj9mom81kqpv5ydv9upbjd` (`DEMAND_ID`),
  KEY `FK_3pu2p7fr3k7n53lu4a4sgllfs` (`PRODUCT_ID`),
  KEY `FK_1ur3n7mc963g6c7bqwsngjuex` (`USER_ID`),
  CONSTRAINT `FK_1ur3n7mc963g6c7bqwsngjuex` FOREIGN KEY (`USER_ID`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_3pu2p7fr3k7n53lu4a4sgllfs` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `CF_PRODUCT` (`id`),
  CONSTRAINT `FK_jvipj9mom81kqpv5ydv9upbjd` FOREIGN KEY (`DEMAND_ID`) REFERENCES `CF_DEMAND` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_ADDRESS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_ADDRESS` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ADDRESS_CATEGORY` varchar(255) DEFAULT NULL,
  `ADDRESS_LINE_1` varchar(255) DEFAULT NULL,
  `ADDRESS_LINE_2` varchar(255) DEFAULT NULL,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `CITY` varchar(255) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `IS_DEFAULT_ADDRESS` bit(1) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(255) DEFAULT NULL,
  `FIRST_NAME` varchar(255) DEFAULT NULL,
  `LAST_NAME` varchar(255) DEFAULT NULL,
  `PHONE_NO` varchar(255) DEFAULT NULL,
  `ZIPCODE` varchar(255) DEFAULT NULL,
  `COMPANY_ID` bigint(20) DEFAULT NULL,
  `COUNTRY_ID` bigint(20) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  `STATE_ID` bigint(20) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_97y259ihpusf470ftoqx8vcnc` (`COMPANY_ID`),
  KEY `FK_1fun6yxjcyusvtqxsig6fotvu` (`COUNTRY_ID`),
  KEY `FK_51gk7gjwd3v3mc2c59cnw7nd1` (`CREATED_BY`),
  KEY `FK_bb3cyt243dxp28w66bros7679` (`MODIFIED_BY`),
  KEY `FK_d3esmd6ahvhvqpol5p63ls0h9` (`STATE_ID`),
  KEY `FK_mdw3qjxvsijcpo9xhmv238ht1` (`USER_ID`),
  CONSTRAINT `FK_1fun6yxjcyusvtqxsig6fotvu` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `CF_COUNTRY` (`id`),
  CONSTRAINT `FK_51gk7gjwd3v3mc2c59cnw7nd1` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_97y259ihpusf470ftoqx8vcnc` FOREIGN KEY (`COMPANY_ID`) REFERENCES `CF_COMPANY` (`id`),
  CONSTRAINT `FK_bb3cyt243dxp28w66bros7679` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_d3esmd6ahvhvqpol5p63ls0h9` FOREIGN KEY (`STATE_ID`) REFERENCES `CF_STATE` (`id`),
  CONSTRAINT `FK_mdw3qjxvsijcpo9xhmv238ht1` FOREIGN KEY (`USER_ID`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_BANK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_BANK` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `AUTH_URL` varchar(255) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `PASSWORD` varchar(255) DEFAULT NULL,
  `USERNAME` varchar(255) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_6abe6mllnidk4ija3w8y1r23t` (`CREATED_BY`),
  KEY `FK_cr5g8f1gxepn0p1gp7i9qkqff` (`MODIFIED_BY`),
  CONSTRAINT `FK_6abe6mllnidk4ija3w8y1r23t` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_cr5g8f1gxepn0p1gp7i9qkqff` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_BID`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_BID` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `PRICE` decimal(19,2) DEFAULT NULL,
  `COMPANY_ID` bigint(20) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `CURRENCY_ID` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  `PRODUCT_ID` bigint(20) DEFAULT NULL,
  `UNIT_ID` bigint(20) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `FK_bhtjp1nb4cj3u0f7qlg5lrsph` (`COMPANY_ID`),
  KEY `FK_f3lwxq5eoe3xkx1tt5gpdw3bf` (`CREATED_BY`),
  KEY `FK_mrq7yj3flg0k1urhoeegm22j6` (`CURRENCY_ID`),
  KEY `FK_gk9xrn75pj5w7wusdcywqp0ew` (`MODIFIED_BY`),
  KEY `FK_qc0irepcqq449qmxqusmhoaab` (`PRODUCT_ID`),
  KEY `FK_65noijn1fwwim5vfnn0iahpns` (`UNIT_ID`),
  KEY `FK_lyk5pf8jb6sorq73qr5cwnut6` (`USER_ID`),
  CONSTRAINT `FK_65noijn1fwwim5vfnn0iahpns` FOREIGN KEY (`UNIT_ID`) REFERENCES `CF_UNIT` (`id`),
  CONSTRAINT `FK_bhtjp1nb4cj3u0f7qlg5lrsph` FOREIGN KEY (`COMPANY_ID`) REFERENCES `CF_COMPANY` (`id`),
  CONSTRAINT `FK_f3lwxq5eoe3xkx1tt5gpdw3bf` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_gk9xrn75pj5w7wusdcywqp0ew` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_lyk5pf8jb6sorq73qr5cwnut6` FOREIGN KEY (`USER_ID`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_mrq7yj3flg0k1urhoeegm22j6` FOREIGN KEY (`CURRENCY_ID`) REFERENCES `CF_CURRENCY` (`id`),
  CONSTRAINT `FK_qc0irepcqq449qmxqusmhoaab` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `CF_PRODUCT` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_BILLING_DETAILS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_BILLING_DETAILS` (
  `BILLING_DETAILS_TYPE` varchar(31) NOT NULL,
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `IS_DEFAULT_BILLING` bit(1) DEFAULT NULL,
  `OWNER` varchar(255) DEFAULT NULL,
  `UNIQUE_ID` varchar(255) NOT NULL,
  `BA_ACCOUNT` varchar(96) DEFAULT NULL,
  `BA_SWIFT` varchar(48) DEFAULT NULL,
  `COMPANY_ID` bigint(20) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  `BA_BANK_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_8h9fk02312dwkopgr7lr9g99p` (`UNIQUE_ID`),
  KEY `FK_a1etqbd5jd1fwap8fvlsr3vi8` (`COMPANY_ID`),
  KEY `FK_i7oww4m1ccesl14dh2u83ogh` (`CREATED_BY`),
  KEY `FK_c16rmdtd801rbvjby2fa3a0m0` (`MODIFIED_BY`),
  KEY `FK_qmlge0ddlgefsra9334eib4w7` (`USER_ID`),
  KEY `FK_brerho0x0j8b7br1sbjfapmuy` (`BA_BANK_ID`),
  CONSTRAINT `FK_a1etqbd5jd1fwap8fvlsr3vi8` FOREIGN KEY (`COMPANY_ID`) REFERENCES `CF_COMPANY` (`id`),
  CONSTRAINT `FK_brerho0x0j8b7br1sbjfapmuy` FOREIGN KEY (`BA_BANK_ID`) REFERENCES `CF_BANK` (`id`),
  CONSTRAINT `FK_c16rmdtd801rbvjby2fa3a0m0` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_i7oww4m1ccesl14dh2u83ogh` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_qmlge0ddlgefsra9334eib4w7` FOREIGN KEY (`USER_ID`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_BRANCH`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_BRANCH` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ADDRESS_LINE_1` varchar(255) DEFAULT NULL,
  `ADDRESS_LINE_2` varchar(255) DEFAULT NULL,
  `CITY` varchar(255) DEFAULT NULL,
  `ZIPCODE` varchar(255) DEFAULT NULL,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `COUNTRY_ID` bigint(20) DEFAULT NULL,
  `STATE_ID` bigint(20) DEFAULT NULL,
  `COMPANY_ID` bigint(20) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_g4nn8xvqufhpwef28wtc7itja` (`COUNTRY_ID`),
  KEY `FK_kvkcmqa34vmyepi2mpyfmjxtq` (`STATE_ID`),
  KEY `FK_f6qtfx0uevjsuc79b05rs4con` (`COMPANY_ID`),
  KEY `FK_o5u0gglpfkr3m5ts87lfqy3up` (`CREATED_BY`),
  KEY `FK_jiu2py41t9xo8jevv28ix3b95` (`MODIFIED_BY`),
  CONSTRAINT `FK_f6qtfx0uevjsuc79b05rs4con` FOREIGN KEY (`COMPANY_ID`) REFERENCES `CF_COMPANY` (`id`),
  CONSTRAINT `FK_g4nn8xvqufhpwef28wtc7itja` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `CF_COUNTRY` (`id`),
  CONSTRAINT `FK_jiu2py41t9xo8jevv28ix3b95` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_kvkcmqa34vmyepi2mpyfmjxtq` FOREIGN KEY (`STATE_ID`) REFERENCES `CF_STATE` (`id`),
  CONSTRAINT `FK_o5u0gglpfkr3m5ts87lfqy3up` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_BUYER_WISHLIST`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_BUYER_WISHLIST` (
  `BUYER_ID` bigint(20) NOT NULL,
  `PRODUCT_ID` bigint(20) NOT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`BUYER_ID`,`PRODUCT_ID`),
  KEY `FK_pl2qb3ord5folb6kw2cmvtdlh` (`CREATED_BY`),
  KEY `FK_j3omc8qhyvc3nkf81x40n7tjs` (`MODIFIED_BY`),
  KEY `FK_74sp4sr1lib94r3hpusik7l2t` (`PRODUCT_ID`),
  CONSTRAINT `FK_74sp4sr1lib94r3hpusik7l2t` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `CF_PRODUCT` (`id`),
  CONSTRAINT `FK_gk547lse6prnqx0myubywruhv` FOREIGN KEY (`BUYER_ID`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_j3omc8qhyvc3nkf81x40n7tjs` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_pl2qb3ord5folb6kw2cmvtdlh` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_CARD_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_CARD_TYPE` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `TYPE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_CART_PRODUCT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_CART_PRODUCT` (
  `GRAIN_CART_ID` bigint(20) NOT NULL,
  `PRODUCT_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`GRAIN_CART_ID`,`PRODUCT_ID`),
  KEY `FK_rgjweyc3advfpwn851enmbp5h` (`PRODUCT_ID`),
  CONSTRAINT `FK_jf7r3bjboqobrxsclm2ht2wdi` FOREIGN KEY (`GRAIN_CART_ID`) REFERENCES `CF_GRAIN_CART` (`id`),
  CONSTRAINT `FK_rgjweyc3advfpwn851enmbp5h` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `CF_PRODUCT` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_CATEGORY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_CATEGORY` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `SHORT_DESC` longtext,
  `name` varchar(255) DEFAULT NULL,
  `UNIQUE_ID` varchar(255) NOT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DEFAULT_IMAGE_ID` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  `PARENT_CATEGORY_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_jp4vmdexy6x2qgu404b045y3a` (`UNIQUE_ID`),
  KEY `FK_c609d76abo6w8l987pphotn29` (`CREATED_BY`),
  KEY `FK_kqjivdri0g6t2dbqpwdx0fdjv` (`DEFAULT_IMAGE_ID`),
  KEY `FK_gouodlnhcb8wuercsduuehvma` (`MODIFIED_BY`),
  KEY `FK_8yx3ajnile8ewcpqqge8ne7k1` (`PARENT_CATEGORY_ID`),
  CONSTRAINT `FK_8yx3ajnile8ewcpqqge8ne7k1` FOREIGN KEY (`PARENT_CATEGORY_ID`) REFERENCES `CF_CATEGORY` (`id`),
  CONSTRAINT `FK_c609d76abo6w8l987pphotn29` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_gouodlnhcb8wuercsduuehvma` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_kqjivdri0g6t2dbqpwdx0fdjv` FOREIGN KEY (`DEFAULT_IMAGE_ID`) REFERENCES `CF_IMAGE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_CATEGORY_REVIEW`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_CATEGORY_REVIEW` (
  `CATEGORY_ID` bigint(20) NOT NULL,
  `COMMENT` varchar(255) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `RATING_ID` bigint(20) DEFAULT NULL,
  `REVIEW_BY` bigint(20) DEFAULT NULL,
  `REVIEW_DATE` datetime DEFAULT NULL,
  `IS_VALID` bit(1) DEFAULT NULL,
  KEY `FK_41yaofmq1gkvwv1m2pcyx6qh` (`RATING_ID`),
  KEY `FK_9cetp8tpow2xbt95mqtsdfhxo` (`REVIEW_BY`),
  KEY `FK_do056pi957dfqdepve7hmaxrh` (`CATEGORY_ID`),
  CONSTRAINT `FK_41yaofmq1gkvwv1m2pcyx6qh` FOREIGN KEY (`RATING_ID`) REFERENCES `CF_RATING` (`id`),
  CONSTRAINT `FK_9cetp8tpow2xbt95mqtsdfhxo` FOREIGN KEY (`REVIEW_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_do056pi957dfqdepve7hmaxrh` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `CF_CATEGORY` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_COMMENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_COMMENT` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `COMMENT` varchar(255) DEFAULT NULL,
  `COMMENT_DATE` datetime DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  `DISCUSSION_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_8vdr5oq1ufbvvlu8wsxkk3w3x` (`USER_ID`),
  KEY `FK_rivkx7jp4t2wew94lkqklve6t` (`DISCUSSION_ID`),
  CONSTRAINT `FK_8vdr5oq1ufbvvlu8wsxkk3w3x` FOREIGN KEY (`USER_ID`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_rivkx7jp4t2wew94lkqklve6t` FOREIGN KEY (`DISCUSSION_ID`) REFERENCES `CF_DISCUSSION` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_COMPANY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_COMPANY` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ADDRESS_LINE_1` varchar(255) DEFAULT NULL,
  `ADDRESS_LINE_2` varchar(255) DEFAULT NULL,
  `CITY` varchar(255) DEFAULT NULL,
  `ZIPCODE` varchar(255) DEFAULT NULL,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `DOMAIN` varchar(255) DEFAULT NULL,
  `EMAIL_ID` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `PHONE_NO` varchar(255) DEFAULT NULL,
  `REGISTR_NO` varchar(255) DEFAULT NULL,
  `SHORT_DESC` longtext,
  `UNIQUE_ID` varchar(255) DEFAULT NULL,
  `VERIFIED` bit(1) DEFAULT NULL,
  `OBJ_VERSION` int(11) DEFAULT NULL,
  `WEBSITE_URL` varchar(255) DEFAULT NULL,
  `COUNTRY_ID` bigint(20) DEFAULT NULL,
  `STATE_ID` bigint(20) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DEFAULT_BILLING_ADDRESS_ID` bigint(20) DEFAULT NULL,
  `DEFAULT_BILLING_DETAILS_ID` bigint(20) DEFAULT NULL,
  `DEFAULT_DELIVERY_ADDRESS_ID` bigint(20) DEFAULT NULL,
  `DEFAULT_IMAGE_ID` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_bosacedlg057fh06udjpw8dkc` (`COUNTRY_ID`),
  KEY `FK_cny4sx7wd9h73vsk04it65bep` (`STATE_ID`),
  KEY `FK_ilsxdv4a2f2oqfglanrg9atme` (`CREATED_BY`),
  KEY `FK_paxg0q1pky8829oou1liy1et9` (`DEFAULT_BILLING_ADDRESS_ID`),
  KEY `FK_d5nkqr1jnoa9wmr5y31v35nfn` (`DEFAULT_BILLING_DETAILS_ID`),
  KEY `FK_iatxe6do6y2nks4tpe5ujbjde` (`DEFAULT_DELIVERY_ADDRESS_ID`),
  KEY `FK_b5qhf99m53v5g0pdkb7h7awsh` (`DEFAULT_IMAGE_ID`),
  KEY `FK_bari77n9xn1btubf5npuxfvky` (`MODIFIED_BY`),
  CONSTRAINT `FK_b5qhf99m53v5g0pdkb7h7awsh` FOREIGN KEY (`DEFAULT_IMAGE_ID`) REFERENCES `CF_IMAGE` (`id`),
  CONSTRAINT `FK_bari77n9xn1btubf5npuxfvky` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_bosacedlg057fh06udjpw8dkc` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `CF_COUNTRY` (`id`),
  CONSTRAINT `FK_cny4sx7wd9h73vsk04it65bep` FOREIGN KEY (`STATE_ID`) REFERENCES `CF_STATE` (`id`),
  CONSTRAINT `FK_d5nkqr1jnoa9wmr5y31v35nfn` FOREIGN KEY (`DEFAULT_BILLING_DETAILS_ID`) REFERENCES `CF_BILLING_DETAILS` (`id`),
  CONSTRAINT `FK_iatxe6do6y2nks4tpe5ujbjde` FOREIGN KEY (`DEFAULT_DELIVERY_ADDRESS_ID`) REFERENCES `CF_ADDRESS` (`id`),
  CONSTRAINT `FK_ilsxdv4a2f2oqfglanrg9atme` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_paxg0q1pky8829oou1liy1et9` FOREIGN KEY (`DEFAULT_BILLING_ADDRESS_ID`) REFERENCES `CF_ADDRESS` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_COMPANY_PLAN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_COMPANY_PLAN` (
  `COMPANY_ID` bigint(20) NOT NULL,
  `PLAN_ID` bigint(20) NOT NULL,
  `EXPIRY_DATE` datetime DEFAULT NULL,
  `IS_ACTIVE` bit(1) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`COMPANY_ID`,`PLAN_ID`),
  KEY `FK_6y24n1qu78d7t68yohk1bh8kx` (`PLAN_ID`),
  CONSTRAINT `FK_6y24n1qu78d7t68yohk1bh8kx` FOREIGN KEY (`PLAN_ID`) REFERENCES `CF_PLAN` (`id`),
  CONSTRAINT `FK_afal6s25s0l8jwkr8iro5y7cs` FOREIGN KEY (`COMPANY_ID`) REFERENCES `CF_COMPANY` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_COMPANY_REVIEW`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_COMPANY_REVIEW` (
  `COMPANY_ID` bigint(20) NOT NULL,
  `COMMENT` varchar(255) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `RATING_ID` bigint(20) DEFAULT NULL,
  `REVIEW_BY` bigint(20) DEFAULT NULL,
  `REVIEW_DATE` datetime DEFAULT NULL,
  `IS_VALID` bit(1) DEFAULT NULL,
  KEY `FK_jkxrjgflebqipjqapdgli693w` (`RATING_ID`),
  KEY `FK_r0qhexl6sq1x4iihymxuey02u` (`REVIEW_BY`),
  KEY `FK_p4l2wb9hyuhjd8rno2leawker` (`COMPANY_ID`),
  CONSTRAINT `FK_jkxrjgflebqipjqapdgli693w` FOREIGN KEY (`RATING_ID`) REFERENCES `CF_RATING` (`id`),
  CONSTRAINT `FK_p4l2wb9hyuhjd8rno2leawker` FOREIGN KEY (`COMPANY_ID`) REFERENCES `CF_COMPANY` (`id`),
  CONSTRAINT `FK_r0qhexl6sq1x4iihymxuey02u` FOREIGN KEY (`REVIEW_BY`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_CONSUMER_PRODUCT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_CONSUMER_PRODUCT` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `SHORT_DESC` longtext,
  `NAME` varchar(255) DEFAULT NULL,
  `CATEGORY_ID` bigint(20) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DEFAULT_IMAGE_ID` bigint(20) DEFAULT NULL,
  `LAST_PRICE_ID` bigint(20) DEFAULT NULL,
  `LATEST_PRICE_ID` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_trabhyhbx43a090syh46x5xql` (`CATEGORY_ID`),
  KEY `FK_qq16a8ucjwr8mh7no0qkq01iw` (`CREATED_BY`),
  KEY `FK_g3upe0lwftxaupkrqngag8668` (`DEFAULT_IMAGE_ID`),
  KEY `FK_8r85asi1avg7h69lqsthe5fbt` (`LAST_PRICE_ID`),
  KEY `FK_ou5q2awsposo9lilmfcl8hbyy` (`LATEST_PRICE_ID`),
  KEY `FK_762f0ci2g1wiqn8mnwixarly5` (`MODIFIED_BY`),
  CONSTRAINT `FK_762f0ci2g1wiqn8mnwixarly5` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_8r85asi1avg7h69lqsthe5fbt` FOREIGN KEY (`LAST_PRICE_ID`) REFERENCES `CF_PRICE` (`id`),
  CONSTRAINT `FK_g3upe0lwftxaupkrqngag8668` FOREIGN KEY (`DEFAULT_IMAGE_ID`) REFERENCES `CF_IMAGE` (`id`),
  CONSTRAINT `FK_ou5q2awsposo9lilmfcl8hbyy` FOREIGN KEY (`LATEST_PRICE_ID`) REFERENCES `CF_PRICE` (`id`),
  CONSTRAINT `FK_qq16a8ucjwr8mh7no0qkq01iw` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_trabhyhbx43a090syh46x5xql` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `CF_CATEGORY` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_CONSUMER_PRODUCT_REVIEW`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_CONSUMER_PRODUCT_REVIEW` (
  `PRODUCT_CATEGORY_ID` bigint(20) NOT NULL,
  `COMMENT` varchar(255) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `RATING_ID` bigint(20) DEFAULT NULL,
  `REVIEW_BY` bigint(20) DEFAULT NULL,
  `REVIEW_DATE` datetime DEFAULT NULL,
  `IS_VALID` bit(1) DEFAULT NULL,
  KEY `FK_9hd7i6lslraev7p5j17ni96pe` (`RATING_ID`),
  KEY `FK_6ebxys23np52ikq62qxsfjb9k` (`REVIEW_BY`),
  KEY `FK_ol7jtb45nbco8brlwk9jh1e2k` (`PRODUCT_CATEGORY_ID`),
  CONSTRAINT `FK_6ebxys23np52ikq62qxsfjb9k` FOREIGN KEY (`REVIEW_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_9hd7i6lslraev7p5j17ni96pe` FOREIGN KEY (`RATING_ID`) REFERENCES `CF_RATING` (`id`),
  CONSTRAINT `FK_ol7jtb45nbco8brlwk9jh1e2k` FOREIGN KEY (`PRODUCT_CATEGORY_ID`) REFERENCES `CF_CONSUMER_PRODUCT` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_CONTACT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_CONTACT` (
  `GROUP_ID` bigint(20) NOT NULL,
  `USER_ID` bigint(20) NOT NULL,
  `AREA` varchar(255) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `IS_ACTIVE` bit(1) DEFAULT NULL,
  `MESSAGE` varchar(255) DEFAULT NULL,
  `COUNTRY_ID` bigint(20) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`GROUP_ID`,`USER_ID`),
  KEY `FK_824eqi0tl0qc194ltn0sfo8pc` (`COUNTRY_ID`),
  KEY `FK_ggeuq946pil3i5qon5l9vxn47` (`CREATED_BY`),
  KEY `FK_b4ntgli0gk16oamm2v136rud5` (`MODIFIED_BY`),
  KEY `FK_rqeemngorjp6htcjwhmiqm9ht` (`USER_ID`),
  CONSTRAINT `FK_824eqi0tl0qc194ltn0sfo8pc` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `CF_COUNTRY` (`id`),
  CONSTRAINT `FK_anfcu12nxi1msm23b3ofj9ng1` FOREIGN KEY (`GROUP_ID`) REFERENCES `CF_GROUP` (`id`),
  CONSTRAINT `FK_b4ntgli0gk16oamm2v136rud5` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_ggeuq946pil3i5qon5l9vxn47` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_rqeemngorjp6htcjwhmiqm9ht` FOREIGN KEY (`USER_ID`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_COUNTRY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_COUNTRY` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_t4farrbu9qut6pfp8asby853e` (`CREATED_BY`),
  KEY `FK_hvqm967wiedgtb9dtqe34d9bg` (`MODIFIED_BY`),
  CONSTRAINT `FK_hvqm967wiedgtb9dtqe34d9bg` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_t4farrbu9qut6pfp8asby853e` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_CREDIT_CARD`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_CREDIT_CARD` (
  `CC_NUMBER` varchar(96) NOT NULL,
  `CC_EXP_MONTH_ID` bigint(20) DEFAULT NULL,
  `CC_EXP_YEAR_ID` bigint(20) DEFAULT NULL,
  `CC_PAYMENT_CARD_ID` bigint(20) DEFAULT NULL,
  `CREDIT_CARD_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`CREDIT_CARD_ID`),
  KEY `FK_5jesssljtmwg4l141y12t91nl` (`CC_EXP_MONTH_ID`),
  KEY `FK_lgn8xmvt72rn13w0ctdbg5nl` (`CC_EXP_YEAR_ID`),
  KEY `FK_aunqbwvoqcu748bkhxfk4v51o` (`CC_PAYMENT_CARD_ID`),
  CONSTRAINT `FK_5jesssljtmwg4l141y12t91nl` FOREIGN KEY (`CC_EXP_MONTH_ID`) REFERENCES `CF_EXPIRY_MONTH` (`id`),
  CONSTRAINT `FK_aunqbwvoqcu748bkhxfk4v51o` FOREIGN KEY (`CC_PAYMENT_CARD_ID`) REFERENCES `CF_PAYMENT_CARD` (`id`),
  CONSTRAINT `FK_jh6j2hxp4axh04a1xs6pq5h4b` FOREIGN KEY (`CREDIT_CARD_ID`) REFERENCES `CF_BILLING_DETAILS` (`id`),
  CONSTRAINT `FK_lgn8xmvt72rn13w0ctdbg5nl` FOREIGN KEY (`CC_EXP_YEAR_ID`) REFERENCES `CF_EXPIRY_YEAR` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_CURRENCY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_CURRENCY` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `COUNTRY_CODE` varchar(255) DEFAULT NULL,
  `MNEOMIC` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `SYMBOL` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_CUSTOMER_CONCERN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_CUSTOMER_CONCERN` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `CONCERN` varchar(255) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `EMAIL_ID` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `PHONE_NO` varchar(255) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_11f9e06frwhqtpo93bscuxm0j` (`USER_ID`),
  CONSTRAINT `FK_11f9e06frwhqtpo93bscuxm0j` FOREIGN KEY (`USER_ID`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_DELIVERY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_DELIVERY` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `Type` varchar(255) DEFAULT NULL,
  `ADDRESS_LINE_1` varchar(255) DEFAULT NULL,
  `ADDRESS_LINE_2` varchar(255) DEFAULT NULL,
  `CITY` varchar(255) DEFAULT NULL,
  `ZIPCODE` varchar(255) DEFAULT NULL,
  `deliveryDate` datetime DEFAULT NULL,
  `COUNTRY_ID` bigint(20) DEFAULT NULL,
  `STATE_ID` bigint(20) DEFAULT NULL,
  `DRIVER_ID` bigint(20) DEFAULT NULL,
  `TRANSPORT_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_9rt6ugwm9umc18ky72l5cpg5x` (`COUNTRY_ID`),
  KEY `FK_tra6m9eg3xiuftwby0y08uui6` (`STATE_ID`),
  KEY `FK_hxcfijne38sv3sesrdikj25w5` (`DRIVER_ID`),
  KEY `FK_mup2t2yjgyxm7459lfbxo2l81` (`TRANSPORT_ID`),
  CONSTRAINT `FK_9rt6ugwm9umc18ky72l5cpg5x` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `CF_COUNTRY` (`id`),
  CONSTRAINT `FK_hxcfijne38sv3sesrdikj25w5` FOREIGN KEY (`DRIVER_ID`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_mup2t2yjgyxm7459lfbxo2l81` FOREIGN KEY (`TRANSPORT_ID`) REFERENCES `CF_TRANSPORT` (`id`),
  CONSTRAINT `FK_tra6m9eg3xiuftwby0y08uui6` FOREIGN KEY (`STATE_ID`) REFERENCES `CF_STATE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_DELIVERY_CHARGE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_DELIVERY_CHARGE` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `charge` double NOT NULL,
  `unit` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_DEMAND`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_DEMAND` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `ASSIGNED_ON` datetime DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `ADDRESS_LINE_1` varchar(255) DEFAULT NULL,
  `ADDRESS_LINE_2` varchar(255) DEFAULT NULL,
  `CITY` varchar(255) DEFAULT NULL,
  `ZIPCODE` varchar(255) DEFAULT NULL,
  `DELIVERY_DATE` datetime DEFAULT NULL,
  `DESCRIPTION` text,
  `MAX_PRICE_PER_UNIT` decimal(19,2) DEFAULT NULL,
  `MIN_PRICE_PER_UNIT` decimal(19,2) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `PRICE_PER_UNIT` decimal(19,2) DEFAULT NULL,
  `QUANTITY` decimal(19,2) DEFAULT NULL,
  `REQUEST_DATE` datetime DEFAULT NULL,
  `SHORT_DESC` varchar(255) DEFAULT NULL,
  `UNIQUE_ID` varchar(255) NOT NULL,
  `ASSIGNED_TO` bigint(20) DEFAULT NULL,
  `CATEGORY_ID` bigint(20) DEFAULT NULL,
  `COMPANY_ID` bigint(20) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `CURRENCY_ID` bigint(20) DEFAULT NULL,
  `DEFAULT_IMAGE_ID` bigint(20) DEFAULT NULL,
  `COUNTRY_ID` bigint(20) DEFAULT NULL,
  `STATE_ID` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  `QUNIT_ID` bigint(20) DEFAULT NULL,
  `STATUS_ID` bigint(20) DEFAULT NULL,
  `PUNIT_ID` bigint(20) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_fbaoiw43crs6hhuh8rj7nxles` (`UNIQUE_ID`),
  KEY `FK_kbbasv7dgjj8x5oxj3lfef7dw` (`ASSIGNED_TO`),
  KEY `FK_ob148dxidsoww4jx6pb8l73tk` (`CATEGORY_ID`),
  KEY `FK_c6ybwrlapc2h62dih1grc56sc` (`COMPANY_ID`),
  KEY `FK_m0pa40rcvwcsdafkfs52s6j4r` (`CREATED_BY`),
  KEY `FK_6i2soow8cjlwfgs5dbax324i9` (`CURRENCY_ID`),
  KEY `FK_6ywq93wwpga3x05pru94t4c4j` (`DEFAULT_IMAGE_ID`),
  KEY `FK_4bw0efya5cum65jsgbrpmpnyy` (`COUNTRY_ID`),
  KEY `FK_berl0m2honk9sr0po7ul58fme` (`STATE_ID`),
  KEY `FK_hqkrwg86juuuukyeuk579r1o0` (`MODIFIED_BY`),
  KEY `FK_pur6dqov31xijbagdvx2av8j1` (`QUNIT_ID`),
  KEY `FK_t6epjk3cg8ce5xbuqfgabj216` (`STATUS_ID`),
  KEY `FK_g95sifr2nwohf0uo3s4199liu` (`PUNIT_ID`),
  KEY `FK_cbdy6tdbcgbm4fmwlyaryh7as` (`USER_ID`),
  CONSTRAINT `FK_4bw0efya5cum65jsgbrpmpnyy` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `CF_COUNTRY` (`id`),
  CONSTRAINT `FK_6i2soow8cjlwfgs5dbax324i9` FOREIGN KEY (`CURRENCY_ID`) REFERENCES `CF_CURRENCY` (`id`),
  CONSTRAINT `FK_6ywq93wwpga3x05pru94t4c4j` FOREIGN KEY (`DEFAULT_IMAGE_ID`) REFERENCES `CF_IMAGE` (`id`),
  CONSTRAINT `FK_berl0m2honk9sr0po7ul58fme` FOREIGN KEY (`STATE_ID`) REFERENCES `CF_STATE` (`id`),
  CONSTRAINT `FK_c6ybwrlapc2h62dih1grc56sc` FOREIGN KEY (`COMPANY_ID`) REFERENCES `CF_COMPANY` (`id`),
  CONSTRAINT `FK_cbdy6tdbcgbm4fmwlyaryh7as` FOREIGN KEY (`USER_ID`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_g95sifr2nwohf0uo3s4199liu` FOREIGN KEY (`PUNIT_ID`) REFERENCES `CF_UNIT` (`id`),
  CONSTRAINT `FK_hqkrwg86juuuukyeuk579r1o0` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_kbbasv7dgjj8x5oxj3lfef7dw` FOREIGN KEY (`ASSIGNED_TO`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_m0pa40rcvwcsdafkfs52s6j4r` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_ob148dxidsoww4jx6pb8l73tk` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `CF_CATEGORY` (`id`),
  CONSTRAINT `FK_pur6dqov31xijbagdvx2av8j1` FOREIGN KEY (`QUNIT_ID`) REFERENCES `CF_UNIT` (`id`),
  CONSTRAINT `FK_t6epjk3cg8ce5xbuqfgabj216` FOREIGN KEY (`STATUS_ID`) REFERENCES `CF_DEMAND_STATUS` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_DEMAND_HISTORY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_DEMAND_HISTORY` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `ADDRESS_LINE_1` varchar(255) DEFAULT NULL,
  `ADDRESS_LINE_2` varchar(255) DEFAULT NULL,
  `CITY` varchar(255) DEFAULT NULL,
  `ZIPCODE` varchar(255) DEFAULT NULL,
  `DELIVERY_DATE` datetime DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `PRICE_PER_UNIT` decimal(19,2) DEFAULT NULL,
  `QUANTITY` decimal(19,2) DEFAULT NULL,
  `REQUEST_DATE` datetime DEFAULT NULL,
  `COMPANY_ID` bigint(20) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `COUNTRY_ID` bigint(20) DEFAULT NULL,
  `STATE_ID` bigint(20) DEFAULT NULL,
  `DEMAND_ID` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  `CURRENCY_ID` bigint(20) DEFAULT NULL,
  `PUNIT_ID` bigint(20) DEFAULT NULL,
  `QUNIT_ID` bigint(20) DEFAULT NULL,
  `STATUS_ID` bigint(20) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_1fdbxbyrnkk48idq97xqcp944` (`COMPANY_ID`),
  KEY `FK_h8qrx06aioetyu1n0isqsvour` (`CREATED_BY`),
  KEY `FK_t1tkvu5fvf0ir3ecptkb8orgt` (`COUNTRY_ID`),
  KEY `FK_jrwyahui2qr30r5hd8pu9qu1c` (`STATE_ID`),
  KEY `FK_qbttke1u0p5jeof2rjymgnrcx` (`DEMAND_ID`),
  KEY `FK_foo18hbtmutp0lan2fwsiet3h` (`MODIFIED_BY`),
  KEY `FK_9a6fyatn370eqqa1hkna4yw5m` (`CURRENCY_ID`),
  KEY `FK_2udk4xu423d991xtkfckevy58` (`PUNIT_ID`),
  KEY `FK_8wyea3ewik1tn6nfargcbo4l8` (`QUNIT_ID`),
  KEY `FK_me8bjjsygvtj9jfxa1oblj2u5` (`STATUS_ID`),
  KEY `FK_7rc7hkw9xb6i0wddhh5suvv0k` (`USER_ID`),
  CONSTRAINT `FK_1fdbxbyrnkk48idq97xqcp944` FOREIGN KEY (`COMPANY_ID`) REFERENCES `CF_COMPANY` (`id`),
  CONSTRAINT `FK_2udk4xu423d991xtkfckevy58` FOREIGN KEY (`PUNIT_ID`) REFERENCES `CF_UNIT` (`id`),
  CONSTRAINT `FK_7rc7hkw9xb6i0wddhh5suvv0k` FOREIGN KEY (`USER_ID`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_8wyea3ewik1tn6nfargcbo4l8` FOREIGN KEY (`QUNIT_ID`) REFERENCES `CF_UNIT` (`id`),
  CONSTRAINT `FK_9a6fyatn370eqqa1hkna4yw5m` FOREIGN KEY (`CURRENCY_ID`) REFERENCES `CF_CURRENCY` (`id`),
  CONSTRAINT `FK_foo18hbtmutp0lan2fwsiet3h` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_h8qrx06aioetyu1n0isqsvour` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_jrwyahui2qr30r5hd8pu9qu1c` FOREIGN KEY (`STATE_ID`) REFERENCES `CF_STATE` (`id`),
  CONSTRAINT `FK_me8bjjsygvtj9jfxa1oblj2u5` FOREIGN KEY (`STATUS_ID`) REFERENCES `CF_DEMAND_STATUS` (`id`),
  CONSTRAINT `FK_qbttke1u0p5jeof2rjymgnrcx` FOREIGN KEY (`DEMAND_ID`) REFERENCES `CF_DEMAND` (`id`),
  CONSTRAINT `FK_t1tkvu5fvf0ir3ecptkb8orgt` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `CF_COUNTRY` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_DEMAND_STATUS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_DEMAND_STATUS` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_DEMAND_TAG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_DEMAND_TAG` (
  `DEMAND_ID` bigint(20) NOT NULL,
  `TAG_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`DEMAND_ID`,`TAG_ID`),
  KEY `FK_8w10i6asb6a93adm02kjv8y5j` (`TAG_ID`),
  CONSTRAINT `FK_8w10i6asb6a93adm02kjv8y5j` FOREIGN KEY (`TAG_ID`) REFERENCES `CF_TAG` (`id`),
  CONSTRAINT `FK_nikn44hd5o6o3h8d1u6vo54au` FOREIGN KEY (`DEMAND_ID`) REFERENCES `CF_DEMAND` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_DEPARTMENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_DEPARTMENT` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_9l6tciw9w2ulc6ni3m9tmf7ke` (`CREATED_BY`),
  KEY `FK_og83y4kgba9d907ab719pbm1x` (`MODIFIED_BY`),
  CONSTRAINT `FK_9l6tciw9w2ulc6ni3m9tmf7ke` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_og83y4kgba9d907ab719pbm1x` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_DESIGNATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_DESIGNATION` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_rhcutn31fj3etq6c3dyua3exm` (`CREATED_BY`),
  KEY `FK_k89e07ug19k2dj4i47xpekdir` (`MODIFIED_BY`),
  CONSTRAINT `FK_k89e07ug19k2dj4i47xpekdir` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_rhcutn31fj3etq6c3dyua3exm` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_DISCUSSION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_DISCUSSION` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `DESCRIPTION` varchar(255) DEFAULT NULL,
  `LAST_COMMENT_DATE` datetime DEFAULT NULL,
  `TITLE` varchar(255) DEFAULT NULL,
  `UNIQUE_ID` varchar(255) NOT NULL,
  `COMPANY_ID` bigint(20) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_m50ylmp0y6ucgsm50vhqojpac` (`UNIQUE_ID`),
  KEY `FK_cs5g3kn1x7r9fxp0p22ns2c1n` (`COMPANY_ID`),
  KEY `FK_a1sgg4m77vig7txb7v195xu2d` (`CREATED_BY`),
  KEY `FK_9nrydkgl2hq8qtepyf9sdjo3j` (`MODIFIED_BY`),
  KEY `FK_i8cm0ecy2p3helhtyxoa9q81l` (`USER_ID`),
  CONSTRAINT `FK_9nrydkgl2hq8qtepyf9sdjo3j` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_a1sgg4m77vig7txb7v195xu2d` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_cs5g3kn1x7r9fxp0p22ns2c1n` FOREIGN KEY (`COMPANY_ID`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_i8cm0ecy2p3helhtyxoa9q81l` FOREIGN KEY (`USER_ID`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_DISCUSSION_GROUP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_DISCUSSION_GROUP` (
  `DISCUSSION_ID` bigint(20) NOT NULL,
  `GROUP_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`DISCUSSION_ID`,`GROUP_ID`),
  KEY `FK_b0tfyw8e4bbqxgvfigtbqf0ke` (`GROUP_ID`),
  CONSTRAINT `FK_b0tfyw8e4bbqxgvfigtbqf0ke` FOREIGN KEY (`GROUP_ID`) REFERENCES `CF_GROUP` (`id`),
  CONSTRAINT `FK_tmb4bjob2mhasuf72burvccgc` FOREIGN KEY (`DISCUSSION_ID`) REFERENCES `CF_DISCUSSION` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_DISCUSSION_TAG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_DISCUSSION_TAG` (
  `DISCUSSION_ID` bigint(20) NOT NULL,
  `TAG_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`DISCUSSION_ID`,`TAG_ID`),
  KEY `FK_bc4c735u9jrued8295wrj6nhv` (`TAG_ID`),
  CONSTRAINT `FK_bc4c735u9jrued8295wrj6nhv` FOREIGN KEY (`TAG_ID`) REFERENCES `CF_TAG` (`id`),
  CONSTRAINT `FK_org30noymguuigx4tc5mqw8ej` FOREIGN KEY (`DISCUSSION_ID`) REFERENCES `CF_DISCUSSION` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_EVENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_EVENT` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `CODE` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_EXPERTISE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_EXPERTISE` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `EXPR_LABEL` varchar(255) DEFAULT NULL,
  `EXPERIENCE` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `UNIQUE_ID` varchar(255) NOT NULL,
  `OBJ_VERSION` int(11) DEFAULT NULL,
  `COMPANY_ID` bigint(20) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_e3wgvh2yqv70dkr496qlnc8oh` (`UNIQUE_ID`),
  KEY `FK_edj9ltc942nvn3nbl5mx0595r` (`COMPANY_ID`),
  KEY `FK_ahex7sxououbeeicwb6boq8ow` (`CREATED_BY`),
  KEY `FK_dls5yllrugh4pacnxqlusfxr3` (`MODIFIED_BY`),
  KEY `FK_iaj3np85tr94cnv8us64hm7lb` (`USER_ID`),
  CONSTRAINT `FK_ahex7sxououbeeicwb6boq8ow` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_dls5yllrugh4pacnxqlusfxr3` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_edj9ltc942nvn3nbl5mx0595r` FOREIGN KEY (`COMPANY_ID`) REFERENCES `CF_COMPANY` (`id`),
  CONSTRAINT `FK_iaj3np85tr94cnv8us64hm7lb` FOREIGN KEY (`USER_ID`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_EXPERTISE_REVIEW`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_EXPERTISE_REVIEW` (
  `EXPERTISE_ID` bigint(20) NOT NULL,
  `COMMENT` varchar(255) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `RATING_ID` bigint(20) DEFAULT NULL,
  `REVIEW_BY` bigint(20) DEFAULT NULL,
  `REVIEW_DATE` datetime DEFAULT NULL,
  `IS_VALID` bit(1) DEFAULT NULL,
  KEY `FK_6np2fuchf17cye013vhetq7a3` (`RATING_ID`),
  KEY `FK_9v9vi1tec9ppm54q8bu8stmfx` (`REVIEW_BY`),
  KEY `FK_6bgt8nv9ulm1j7q5rtphignho` (`EXPERTISE_ID`),
  CONSTRAINT `FK_6bgt8nv9ulm1j7q5rtphignho` FOREIGN KEY (`EXPERTISE_ID`) REFERENCES `CF_EXPERTISE` (`id`),
  CONSTRAINT `FK_6np2fuchf17cye013vhetq7a3` FOREIGN KEY (`RATING_ID`) REFERENCES `CF_RATING` (`id`),
  CONSTRAINT `FK_9v9vi1tec9ppm54q8bu8stmfx` FOREIGN KEY (`REVIEW_BY`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_EXPIRY_MONTH`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_EXPIRY_MONTH` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `MONTH` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_EXPIRY_YEAR`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_EXPIRY_YEAR` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_FEEDBACK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_FEEDBACK` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `CREATED_ON` datetime DEFAULT NULL,
  `FEEDBACK` varchar(255) DEFAULT NULL,
  `EXECUTIVES_RATING` bigint(20) DEFAULT NULL,
  `SERVICES_RATING` bigint(20) DEFAULT NULL,
  `SITE_RATING` bigint(20) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_ldql8m70skk1e39wapg7p590` (`EXECUTIVES_RATING`),
  KEY `FK_omjwi266cm3pp38swm9o08c9l` (`SERVICES_RATING`),
  KEY `FK_lbchtwpcl87q55nuw5fvbtcn4` (`SITE_RATING`),
  KEY `FK_2palm74eqbmeim6cblb99u4jl` (`USER_ID`),
  CONSTRAINT `FK_2palm74eqbmeim6cblb99u4jl` FOREIGN KEY (`USER_ID`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_lbchtwpcl87q55nuw5fvbtcn4` FOREIGN KEY (`SITE_RATING`) REFERENCES `CF_RATING` (`id`),
  CONSTRAINT `FK_ldql8m70skk1e39wapg7p590` FOREIGN KEY (`EXECUTIVES_RATING`) REFERENCES `CF_RATING` (`id`),
  CONSTRAINT `FK_omjwi266cm3pp38swm9o08c9l` FOREIGN KEY (`SERVICES_RATING`) REFERENCES `CF_RATING` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_FILTER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_FILTER` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `CATEGORY` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_FLOOR_CHARGE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_FLOOR_CHARGE` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `charge` double NOT NULL,
  `unit` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_GRAIN_CART`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_GRAIN_CART` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_CONSUMER_CART` bit(1) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `COMPANY_ID` bigint(20) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_n7wt2u7g752w50tctpkejsb8v` (`COMPANY_ID`),
  KEY `FK_pqo2xfrhhw89j6i76o9abf5qm` (`CREATED_BY`),
  KEY `FK_5jttlygwmnwgwsr72frxeic5f` (`MODIFIED_BY`),
  KEY `FK_o0sw7ocpi8fnlyo06hg6aly6d` (`USER_ID`),
  CONSTRAINT `FK_5jttlygwmnwgwsr72frxeic5f` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_n7wt2u7g752w50tctpkejsb8v` FOREIGN KEY (`COMPANY_ID`) REFERENCES `CF_COMPANY` (`id`),
  CONSTRAINT `FK_o0sw7ocpi8fnlyo06hg6aly6d` FOREIGN KEY (`USER_ID`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_pqo2xfrhhw89j6i76o9abf5qm` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_GROUP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_GROUP` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `UNIQUE_ID` varchar(255) NOT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_gf98itqa6b5wbvhax53awnt4y` (`UNIQUE_ID`),
  KEY `FK_d2b3v2h8jpiandrbl5lxi2lwo` (`CREATED_BY`),
  KEY `FK_773sgbu0n7b0rxiic139dwme2` (`MODIFIED_BY`),
  CONSTRAINT `FK_773sgbu0n7b0rxiic139dwme2` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_d2b3v2h8jpiandrbl5lxi2lwo` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_GROUP_ROLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_GROUP_ROLE` (
  `GROUP_ID` bigint(20) NOT NULL,
  `ROLE_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`GROUP_ID`,`ROLE_ID`),
  KEY `FK_spyclxuqw18pletaiweewvgd8` (`ROLE_ID`),
  CONSTRAINT `FK_cvhx93bpe79o7mjv8fv26i2x3` FOREIGN KEY (`GROUP_ID`) REFERENCES `CF_GROUP` (`id`),
  CONSTRAINT `FK_spyclxuqw18pletaiweewvgd8` FOREIGN KEY (`ROLE_ID`) REFERENCES `CF_ROLE` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_IMAGE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_IMAGE` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IMAGE_CATEGORY` varchar(255) DEFAULT NULL,
  `FILENAME` varchar(255) DEFAULT NULL,
  `FILEPATH` varchar(255) DEFAULT NULL,
  `IS_DEFAULT` bit(1) DEFAULT NULL,
  `LARGE_FILEPATH` varchar(255) DEFAULT NULL,
  `MEDIUM_FILEPATH` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `SMALL_FILEPATH` varchar(255) DEFAULT NULL,
  `COMPANY_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_ID` bigint(20) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_gx6of3aukqc6aqct6haia74l8` (`COMPANY_ID`),
  KEY `FK_9fb4ngtguew5lcc4xvqj2q558` (`PRODUCT_ID`),
  KEY `FK_n5kybs1q9sdoaeuja8fpdkc5u` (`USER_ID`),
  CONSTRAINT `FK_9fb4ngtguew5lcc4xvqj2q558` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `CF_PRODUCT` (`id`),
  CONSTRAINT `FK_gx6of3aukqc6aqct6haia74l8` FOREIGN KEY (`COMPANY_ID`) REFERENCES `CF_COMPANY` (`id`),
  CONSTRAINT `FK_n5kybs1q9sdoaeuja8fpdkc5u` FOREIGN KEY (`USER_ID`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=1020 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_INDIVIDUAL_PLAN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_INDIVIDUAL_PLAN` (
  `PLAN_ID` bigint(20) NOT NULL,
  `USER_ID` bigint(20) NOT NULL,
  `EXPIRY_DATE` datetime DEFAULT NULL,
  `IS_ACTIVE` bit(1) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`PLAN_ID`,`USER_ID`),
  KEY `FK_llrp42ftkmsy5qak8p7qlv88g` (`USER_ID`),
  CONSTRAINT `FK_llrp42ftkmsy5qak8p7qlv88g` FOREIGN KEY (`USER_ID`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_piyt0ni1jy7h5vespe1hy86g8` FOREIGN KEY (`PLAN_ID`) REFERENCES `CF_PLAN` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_ITEM`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_ITEM` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(255) DEFAULT NULL,
  `QUANTITY` decimal(19,2) DEFAULT NULL,
  `UNIQUE_ID` varchar(255) NOT NULL,
  `OBJ_VERSION` int(11) DEFAULT NULL,
  `IMAGE_ID` bigint(20) DEFAULT NULL,
  `PRICE_ID` bigint(20) DEFAULT NULL,
  `QUNIT_ID` bigint(20) DEFAULT NULL,
  `GRAIN_CART_ID` bigint(20) DEFAULT NULL,
  `ORDER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_7c2i76hqm3wnfo0o7l3t97s2q` (`UNIQUE_ID`),
  KEY `FK_67tl9e7o6k935li8rv9wt51e1` (`IMAGE_ID`),
  KEY `FK_1ddjh7315p4lyq8g82ra4jll9` (`PRICE_ID`),
  KEY `FK_g2avo1wtkfaqmbxsnkcat4ari` (`QUNIT_ID`),
  KEY `FK_4qewitb52tkeij04h0d0mq03k` (`GRAIN_CART_ID`),
  KEY `FK_c67pkt4jp1uvkpr8gsfwmgbo1` (`ORDER_ID`),
  CONSTRAINT `FK_1ddjh7315p4lyq8g82ra4jll9` FOREIGN KEY (`PRICE_ID`) REFERENCES `CF_PRICE` (`id`),
  CONSTRAINT `FK_4qewitb52tkeij04h0d0mq03k` FOREIGN KEY (`GRAIN_CART_ID`) REFERENCES `CF_GRAIN_CART` (`id`),
  CONSTRAINT `FK_67tl9e7o6k935li8rv9wt51e1` FOREIGN KEY (`IMAGE_ID`) REFERENCES `CF_IMAGE` (`id`),
  CONSTRAINT `FK_c67pkt4jp1uvkpr8gsfwmgbo1` FOREIGN KEY (`ORDER_ID`) REFERENCES `CF_ORDER` (`id`),
  CONSTRAINT `FK_g2avo1wtkfaqmbxsnkcat4ari` FOREIGN KEY (`QUNIT_ID`) REFERENCES `CF_UNIT` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_LANGUAGE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_LANGUAGE` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_djap23iuvmt2na7yov6kawrd2` (`CREATED_BY`),
  KEY `FK_50ew0gdb8kcmwyedcyxvwxvk9` (`MODIFIED_BY`),
  CONSTRAINT `FK_50ew0gdb8kcmwyedcyxvwxvk9` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_djap23iuvmt2na7yov6kawrd2` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_LICENSE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_LICENSE` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `licenseNo` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `validFor` varchar(255) DEFAULT NULL,
  `DRIVER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_h22rtqkm05fh1ktttn5ihashe` (`DRIVER_ID`),
  CONSTRAINT `FK_h22rtqkm05fh1ktttn5ihashe` FOREIGN KEY (`DRIVER_ID`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_NOTE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_NOTE` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `NOTE` longtext,
  `TITLE` varchar(255) DEFAULT NULL,
  `UNIQUE_ID` varchar(255) NOT NULL,
  `COMPANY_ID` bigint(20) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_he0n6xm1bvwat21nctaqfbugl` (`UNIQUE_ID`),
  KEY `FK_lddyjutt31h1irf1gpyjabs0` (`COMPANY_ID`),
  KEY `FK_3mpf68wo9ygxt16vwqtkj6l6y` (`CREATED_BY`),
  KEY `FK_ai1iwj70wb39cs9hft2y4vvt0` (`MODIFIED_BY`),
  KEY `FK_bpmg3fx3dwmkngsxvomtsyb3s` (`USER_ID`),
  CONSTRAINT `FK_3mpf68wo9ygxt16vwqtkj6l6y` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_ai1iwj70wb39cs9hft2y4vvt0` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_bpmg3fx3dwmkngsxvomtsyb3s` FOREIGN KEY (`USER_ID`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_lddyjutt31h1irf1gpyjabs0` FOREIGN KEY (`COMPANY_ID`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_NOTIFICATION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_NOTIFICATION` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `EXPIRY_DATE` datetime DEFAULT NULL,
  `NOTIFICATION` varchar(255) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  `TITLE` varchar(255) DEFAULT NULL,
  `CATEGORY_ID` bigint(20) DEFAULT NULL,
  `COMPANY_ID` bigint(20) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_9o4my5gxnssvekecg428846dm` (`CATEGORY_ID`),
  KEY `FK_hgpjsmoyggwec3ixw9rcf5h10` (`COMPANY_ID`),
  KEY `FK_ogr3xehg29ym4phwxnhfkvxab` (`CREATED_BY`),
  KEY `FK_rqsq8pthw3qf0rdksmaydgb2u` (`MODIFIED_BY`),
  KEY `FK_s7mgk2mhnsatcc4i8dhq3d4gq` (`USER_ID`),
  CONSTRAINT `FK_9o4my5gxnssvekecg428846dm` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `CF_NOTIF_CATEGORY` (`id`),
  CONSTRAINT `FK_hgpjsmoyggwec3ixw9rcf5h10` FOREIGN KEY (`COMPANY_ID`) REFERENCES `CF_COMPANY` (`id`),
  CONSTRAINT `FK_ogr3xehg29ym4phwxnhfkvxab` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_rqsq8pthw3qf0rdksmaydgb2u` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_s7mgk2mhnsatcc4i8dhq3d4gq` FOREIGN KEY (`USER_ID`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_NOTIFICATION_COMPANY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_NOTIFICATION_COMPANY` (
  `NOTIFICATION_ID` bigint(20) NOT NULL,
  `COMPANY_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`NOTIFICATION_ID`,`COMPANY_ID`),
  KEY `FK_j6d58qqndmtwbaofssfmb5hn2` (`COMPANY_ID`),
  CONSTRAINT `FK_j6d58qqndmtwbaofssfmb5hn2` FOREIGN KEY (`COMPANY_ID`) REFERENCES `CF_COMPANY` (`id`),
  CONSTRAINT `FK_voerjtfgii45lgu2lpkwycl0` FOREIGN KEY (`NOTIFICATION_ID`) REFERENCES `CF_NOTIFICATION` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_NOTIFICATION_GROUP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_NOTIFICATION_GROUP` (
  `NOTIFICATION_ID` bigint(20) NOT NULL,
  `GROUP_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`NOTIFICATION_ID`,`GROUP_ID`),
  KEY `FK_tcyjjv9w4wpfxts66k0bnwask` (`GROUP_ID`),
  CONSTRAINT `FK_bijy5bodmulexnhu7ydrhc9ov` FOREIGN KEY (`NOTIFICATION_ID`) REFERENCES `CF_NOTIFICATION` (`id`),
  CONSTRAINT `FK_tcyjjv9w4wpfxts66k0bnwask` FOREIGN KEY (`GROUP_ID`) REFERENCES `CF_GROUP` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_NOTIFICATION_USER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_NOTIFICATION_USER` (
  `NOTIFICATION_ID` bigint(20) NOT NULL,
  `USER_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`NOTIFICATION_ID`,`USER_ID`),
  KEY `FK_9tr83mupg5qsblch70086rwoo` (`USER_ID`),
  CONSTRAINT `FK_9tr83mupg5qsblch70086rwoo` FOREIGN KEY (`USER_ID`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_k949bi6n5xkq4t9f6mt3xbkqo` FOREIGN KEY (`NOTIFICATION_ID`) REFERENCES `CF_NOTIFICATION` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_NOTIF_CATEGORY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_NOTIF_CATEGORY` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DEFAULT_IMAGE_ID` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_borfd563q0fxhfaqny93fd5h6` (`CREATED_BY`),
  KEY `FK_5jf16s3f8ckk2eatgqi2vw3m3` (`DEFAULT_IMAGE_ID`),
  KEY `FK_nhl1g938m426gl5obiddelgin` (`MODIFIED_BY`),
  CONSTRAINT `FK_5jf16s3f8ckk2eatgqi2vw3m3` FOREIGN KEY (`DEFAULT_IMAGE_ID`) REFERENCES `CF_IMAGE` (`id`),
  CONSTRAINT `FK_borfd563q0fxhfaqny93fd5h6` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_nhl1g938m426gl5obiddelgin` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_ORDER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_ORDER` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `AMOUNT_PAID` decimal(19,2) DEFAULT NULL,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `ASSIGNED_ON` datetime DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `DELIVERY_DATE` datetime DEFAULT NULL,
  `ORDER_DATE` datetime DEFAULT NULL,
  `ORDER_NO` varchar(255) NOT NULL,
  `SHIPPING_COST` decimal(19,2) DEFAULT NULL,
  `SUBTOTAL` decimal(19,2) DEFAULT NULL,
  `TOTAL_PRICE` decimal(19,2) DEFAULT NULL,
  `ASSIGNED_TO` bigint(20) DEFAULT NULL,
  `BILLING_ADDRESS_ID` bigint(20) DEFAULT NULL,
  `COMPANY_ID` bigint(20) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DELIVER_BY` bigint(20) DEFAULT NULL,
  `FULFILLED_BY` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  `SHIPPING_ADDRESS_ID` bigint(20) DEFAULT NULL,
  `STATUS_ID` bigint(20) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_9p97tb3ehmkvu46xaap6rhwho` (`ORDER_NO`),
  KEY `FK_j4whodtcti19oty1850vjo5wj` (`ASSIGNED_TO`),
  KEY `FK_1yas1eoflw1km30xsrr24lg1u` (`BILLING_ADDRESS_ID`),
  KEY `FK_swk397scsjx7e5xohwda8l5es` (`COMPANY_ID`),
  KEY `FK_r1wxvlvap76kowucy9rjsbc8u` (`CREATED_BY`),
  KEY `FK_2adx4rcap7tw2ukxf1xm4ouff` (`DELIVER_BY`),
  KEY `FK_qm1asv8ei2u6puahvgkm16w62` (`FULFILLED_BY`),
  KEY `FK_k2svv08fhp53o65wtyd00egoj` (`MODIFIED_BY`),
  KEY `FK_2iugfp7wl5c4ixi521vyo8lt3` (`SHIPPING_ADDRESS_ID`),
  KEY `FK_5xnxv8ls0efw3oac0i14tlx3b` (`STATUS_ID`),
  KEY `FK_k15858pqfubjp5f2bhmolst95` (`USER_ID`),
  CONSTRAINT `FK_1yas1eoflw1km30xsrr24lg1u` FOREIGN KEY (`BILLING_ADDRESS_ID`) REFERENCES `CF_ADDRESS` (`id`),
  CONSTRAINT `FK_2adx4rcap7tw2ukxf1xm4ouff` FOREIGN KEY (`DELIVER_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_2iugfp7wl5c4ixi521vyo8lt3` FOREIGN KEY (`SHIPPING_ADDRESS_ID`) REFERENCES `CF_ADDRESS` (`id`),
  CONSTRAINT `FK_5xnxv8ls0efw3oac0i14tlx3b` FOREIGN KEY (`STATUS_ID`) REFERENCES `CF_ORDER_STATUS` (`id`),
  CONSTRAINT `FK_j4whodtcti19oty1850vjo5wj` FOREIGN KEY (`ASSIGNED_TO`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_k15858pqfubjp5f2bhmolst95` FOREIGN KEY (`USER_ID`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_k2svv08fhp53o65wtyd00egoj` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_qm1asv8ei2u6puahvgkm16w62` FOREIGN KEY (`FULFILLED_BY`) REFERENCES `CF_COMPANY` (`id`),
  CONSTRAINT `FK_r1wxvlvap76kowucy9rjsbc8u` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_swk397scsjx7e5xohwda8l5es` FOREIGN KEY (`COMPANY_ID`) REFERENCES `CF_COMPANY` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_ORDER_HISTORY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_ORDER_HISTORY` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `AMOUNT_PAID` decimal(19,2) DEFAULT NULL,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `DELIVERY_DATE` datetime DEFAULT NULL,
  `ORDER_DATE` datetime DEFAULT NULL,
  `ORDER_NO` varchar(255) NOT NULL,
  `SHIPPING_COST` decimal(19,2) DEFAULT NULL,
  `SUBTOTAL` decimal(19,2) DEFAULT NULL,
  `TOTAL_PRICE` decimal(19,2) DEFAULT NULL,
  `BILLING_ADDRESS_ID` bigint(20) DEFAULT NULL,
  `BUYER_ID` bigint(20) DEFAULT NULL,
  `COMPANY_ID` bigint(20) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `FULFILLED_BY` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  `ORDER_ID` bigint(20) DEFAULT NULL,
  `SHIPPING_ADDRESS_ID` bigint(20) DEFAULT NULL,
  `STATUS_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_4wcxd7jiap2vbnbgv8pm89iks` (`ORDER_NO`),
  KEY `FK_lfaxm7t696kjxvw00ynfrs4kw` (`BILLING_ADDRESS_ID`),
  KEY `FK_cahb0a1pk4lmdpskdltt72w50` (`BUYER_ID`),
  KEY `FK_pmccactxio42cwld25s9seuog` (`COMPANY_ID`),
  KEY `FK_65kkj7kmc65p6olv9ldhblh57` (`CREATED_BY`),
  KEY `FK_mlt9nrxynh62lggjq83g9lpd0` (`FULFILLED_BY`),
  KEY `FK_27odpf4l0jdd5oi6bvyiuot3p` (`MODIFIED_BY`),
  KEY `FK_17a1u0x9mdqgp0xvwawg891wo` (`ORDER_ID`),
  KEY `FK_mjdm6bhcb5r8c116ixco1i70e` (`SHIPPING_ADDRESS_ID`),
  KEY `FK_rurhdforvts556sochdr4sywr` (`STATUS_ID`),
  CONSTRAINT `FK_17a1u0x9mdqgp0xvwawg891wo` FOREIGN KEY (`ORDER_ID`) REFERENCES `CF_ORDER` (`id`),
  CONSTRAINT `FK_27odpf4l0jdd5oi6bvyiuot3p` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_65kkj7kmc65p6olv9ldhblh57` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_cahb0a1pk4lmdpskdltt72w50` FOREIGN KEY (`BUYER_ID`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_lfaxm7t696kjxvw00ynfrs4kw` FOREIGN KEY (`BILLING_ADDRESS_ID`) REFERENCES `CF_ADDRESS` (`id`),
  CONSTRAINT `FK_mjdm6bhcb5r8c116ixco1i70e` FOREIGN KEY (`SHIPPING_ADDRESS_ID`) REFERENCES `CF_ADDRESS` (`id`),
  CONSTRAINT `FK_mlt9nrxynh62lggjq83g9lpd0` FOREIGN KEY (`FULFILLED_BY`) REFERENCES `CF_COMPANY` (`id`),
  CONSTRAINT `FK_pmccactxio42cwld25s9seuog` FOREIGN KEY (`COMPANY_ID`) REFERENCES `CF_COMPANY` (`id`),
  CONSTRAINT `FK_rurhdforvts556sochdr4sywr` FOREIGN KEY (`STATUS_ID`) REFERENCES `CF_ORDER_STATUS` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_ORDER_PRODUCTS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_ORDER_PRODUCTS` (
  `ORDER_ID` bigint(20) NOT NULL,
  `PRODUCT_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`ORDER_ID`,`PRODUCT_ID`),
  UNIQUE KEY `UK_i5wyo0mn5sv1h9fj7msbw45qa` (`PRODUCT_ID`),
  CONSTRAINT `FK_i5wyo0mn5sv1h9fj7msbw45qa` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `CF_PRODUCT` (`id`),
  CONSTRAINT `FK_nqjp1i6nxt0b8n5im2koviku6` FOREIGN KEY (`ORDER_ID`) REFERENCES `CF_ORDER` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_ORDER_STATUS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_ORDER_STATUS` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_PAYMENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_PAYMENT` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` decimal(19,2) DEFAULT NULL,
  `method` varchar(255) DEFAULT NULL,
  `paymentDate` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_PAYMENT_CARD`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_PAYMENT_CARD` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DEFAULT_IMAGE_ID` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_ksb562njdmfrmaommrlf27pjg` (`CREATED_BY`),
  KEY `FK_p4cnuisf269hs5qg1otni0e0c` (`DEFAULT_IMAGE_ID`),
  KEY `FK_e0ab15i047tdnlsal57w2abrd` (`MODIFIED_BY`),
  CONSTRAINT `FK_e0ab15i047tdnlsal57w2abrd` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_ksb562njdmfrmaommrlf27pjg` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_p4cnuisf269hs5qg1otni0e0c` FOREIGN KEY (`DEFAULT_IMAGE_ID`) REFERENCES `CF_IMAGE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_PAYMENT_METHOD`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_PAYMENT_METHOD` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `IS_PRIMARY` bit(1) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  `COMPANY_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_30ippu2minl4tck51mm0m1hag` (`CREATED_BY`),
  KEY `FK_nqjn18658g31a2lcchupgl7u0` (`MODIFIED_BY`),
  KEY `FK_gvsw3tgmt4brsnqnsogbpno2d` (`COMPANY_ID`),
  CONSTRAINT `FK_30ippu2minl4tck51mm0m1hag` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_gvsw3tgmt4brsnqnsogbpno2d` FOREIGN KEY (`COMPANY_ID`) REFERENCES `CF_COMPANY` (`id`),
  CONSTRAINT `FK_nqjn18658g31a2lcchupgl7u0` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_PERMIT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_PERMIT` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `state` varchar(255) DEFAULT NULL,
  `TRANSPORT_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_hegxv316k9tg86l9wt6g8f8p9` (`TRANSPORT_ID`),
  CONSTRAINT `FK_hegxv316k9tg86l9wt6g8f8p9` FOREIGN KEY (`TRANSPORT_ID`) REFERENCES `CF_TRANSPORT` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_PERSON`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_PERSON` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ADDRESS_LINE_1` varchar(255) DEFAULT NULL,
  `ADDRESS_LINE_2` varchar(255) DEFAULT NULL,
  `CITY` varchar(255) DEFAULT NULL,
  `ZIPCODE` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `mobileNo` varchar(255) DEFAULT NULL,
  `phoneNo` varchar(255) DEFAULT NULL,
  `COUNTRY_ID` bigint(20) DEFAULT NULL,
  `STATE_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_jv2i3a7312pc0e4okv4eqftju` (`COUNTRY_ID`),
  KEY `FK_2og4xpk0hd8o2xxgp9alqctag` (`STATE_ID`),
  CONSTRAINT `FK_2og4xpk0hd8o2xxgp9alqctag` FOREIGN KEY (`STATE_ID`) REFERENCES `CF_STATE` (`id`),
  CONSTRAINT `FK_jv2i3a7312pc0e4okv4eqftju` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `CF_COUNTRY` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_PLAN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_PLAN` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `CURRENCY` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `NO_OF_USERS` int(11) DEFAULT NULL,
  `PRICE` decimal(19,2) DEFAULT NULL,
  `PRICING_BY` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_PRICE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_PRICE` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_INITIAL` bit(1) DEFAULT NULL,
  `PRICE` decimal(19,2) DEFAULT NULL,
  `PRICE_DATE` datetime DEFAULT NULL,
  `CURRENCY_ID` bigint(20) DEFAULT NULL,
  `UNIT_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_ID` bigint(20) DEFAULT NULL,
  `CONSUMER_PRODUCT_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_smdr8cqnm43tpor4w8p8dt4y1` (`CURRENCY_ID`),
  KEY `FK_gk5ib64186b7vklu93u9e33mk` (`UNIT_ID`),
  KEY `FK_py0nawwhdmah23sxd0yg7yqjs` (`PRODUCT_ID`),
  KEY `FK_7xp1md53nu2chc4ngyfvb6utw` (`CONSUMER_PRODUCT_ID`),
  CONSTRAINT `FK_7xp1md53nu2chc4ngyfvb6utw` FOREIGN KEY (`CONSUMER_PRODUCT_ID`) REFERENCES `CF_CONSUMER_PRODUCT` (`id`),
  CONSTRAINT `FK_gk5ib64186b7vklu93u9e33mk` FOREIGN KEY (`UNIT_ID`) REFERENCES `CF_UNIT` (`id`),
  CONSTRAINT `FK_py0nawwhdmah23sxd0yg7yqjs` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `CF_PRODUCT` (`id`),
  CONSTRAINT `FK_smdr8cqnm43tpor4w8p8dt4y1` FOREIGN KEY (`CURRENCY_ID`) REFERENCES `CF_CURRENCY` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=951 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_PRODUCT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_PRODUCT` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ADDRESS_LINE_1` varchar(255) DEFAULT NULL,
  `ADDRESS_LINE_2` varchar(255) DEFAULT NULL,
  `CITY` varchar(255) DEFAULT NULL,
  `ZIPCODE` varchar(255) DEFAULT NULL,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `IS_BID_ENABLED` bit(1) DEFAULT NULL,
  `COMMENTS` varchar(255) DEFAULT NULL,
  `IS_CONSUMER_PRODUCT` bit(1) DEFAULT NULL,
  `ASSIGNED_ON` datetime DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `DESCRIPTION` text,
  `ENABLE_TILL_DATE` datetime DEFAULT NULL,
  `INITIAL_BID_AMOUNT` decimal(19,2) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `QUANTITY` decimal(19,2) DEFAULT NULL,
  `SHORT_DESC` longtext,
  `UNIQUE_ID` varchar(255) NOT NULL,
  `OBJ_VERSION` int(11) DEFAULT NULL,
  `COUNTRY_ID` bigint(20) DEFAULT NULL,
  `STATE_ID` bigint(20) DEFAULT NULL,
  `ASSIGNED_TO` bigint(20) DEFAULT NULL,
  `BIDDING_CURRENCY_ID` bigint(20) DEFAULT NULL,
  `BIDDING_UNIT_ID` bigint(20) DEFAULT NULL,
  `CATEGORY_ID` bigint(20) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DEFAULT_IMAGE_ID` bigint(20) DEFAULT NULL,
  `HIGHEST_BID_ID` bigint(20) DEFAULT NULL,
  `LAST_PRICE_ID` bigint(20) DEFAULT NULL,
  `LATEST_PRICE_ID` bigint(20) DEFAULT NULL,
  `MAX_PRICE_ID` bigint(20) DEFAULT NULL,
  `MIN_PRICE_ID` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  `OWNER_ID` bigint(20) DEFAULT NULL,
  `QUNIT_ID` bigint(20) DEFAULT NULL,
  `STATUS_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_qkk822n1110dk98wlw5y77wsi` (`UNIQUE_ID`),
  KEY `FK_8bfpdeui1a4y8lddnai1f83op` (`COUNTRY_ID`),
  KEY `FK_kkosarf06ijbl420d0yj6lpkj` (`STATE_ID`),
  KEY `FK_ir6nn15xhxlojsytrwjr45ol8` (`ASSIGNED_TO`),
  KEY `FK_qhbg21eutui861yocktoomar7` (`BIDDING_CURRENCY_ID`),
  KEY `FK_plmeo7xjmy70lcv9mganduj36` (`BIDDING_UNIT_ID`),
  KEY `FK_nxesvse9am9lsw0vvaore8rtj` (`CATEGORY_ID`),
  KEY `FK_3xr515n4uigv1x3qewoucyg8h` (`CREATED_BY`),
  KEY `FK_8ola48mgy6mkvc9xfqucj3iwp` (`DEFAULT_IMAGE_ID`),
  KEY `FK_59lmo2i1n248p0ft1lwpb8180` (`HIGHEST_BID_ID`),
  KEY `FK_gkd6rosofs8i0hntnomt1u319` (`LAST_PRICE_ID`),
  KEY `FK_3d2p7x40fah0l4orqb7t2fehj` (`LATEST_PRICE_ID`),
  KEY `FK_srpp65uxqfjsc8qw4ts4bbxec` (`MAX_PRICE_ID`),
  KEY `FK_4rdjogalk64smm1nk3oxanrqx` (`MIN_PRICE_ID`),
  KEY `FK_hc0na9rwb4tr5b99yvl17d1ls` (`MODIFIED_BY`),
  KEY `FK_8tw3k3bglivl0xou2r856isfy` (`OWNER_ID`),
  KEY `FK_q52kki6epki20390cnrfcqulp` (`QUNIT_ID`),
  KEY `FK_j0c7m7fn6a4xebxujbn7xcjew` (`STATUS_ID`),
  CONSTRAINT `FK_3d2p7x40fah0l4orqb7t2fehj` FOREIGN KEY (`LATEST_PRICE_ID`) REFERENCES `CF_PRICE` (`id`),
  CONSTRAINT `FK_3xr515n4uigv1x3qewoucyg8h` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_4rdjogalk64smm1nk3oxanrqx` FOREIGN KEY (`MIN_PRICE_ID`) REFERENCES `CF_PRICE` (`id`),
  CONSTRAINT `FK_59lmo2i1n248p0ft1lwpb8180` FOREIGN KEY (`HIGHEST_BID_ID`) REFERENCES `CF_BID` (`ID`),
  CONSTRAINT `FK_8bfpdeui1a4y8lddnai1f83op` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `CF_COUNTRY` (`id`),
  CONSTRAINT `FK_8ola48mgy6mkvc9xfqucj3iwp` FOREIGN KEY (`DEFAULT_IMAGE_ID`) REFERENCES `CF_IMAGE` (`id`),
  CONSTRAINT `FK_8tw3k3bglivl0xou2r856isfy` FOREIGN KEY (`OWNER_ID`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_gkd6rosofs8i0hntnomt1u319` FOREIGN KEY (`LAST_PRICE_ID`) REFERENCES `CF_PRICE` (`id`),
  CONSTRAINT `FK_hc0na9rwb4tr5b99yvl17d1ls` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_ir6nn15xhxlojsytrwjr45ol8` FOREIGN KEY (`ASSIGNED_TO`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_j0c7m7fn6a4xebxujbn7xcjew` FOREIGN KEY (`STATUS_ID`) REFERENCES `CF_PRODUCT_STATUS` (`id`),
  CONSTRAINT `FK_kkosarf06ijbl420d0yj6lpkj` FOREIGN KEY (`STATE_ID`) REFERENCES `CF_STATE` (`id`),
  CONSTRAINT `FK_nxesvse9am9lsw0vvaore8rtj` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `CF_CATEGORY` (`id`),
  CONSTRAINT `FK_plmeo7xjmy70lcv9mganduj36` FOREIGN KEY (`BIDDING_UNIT_ID`) REFERENCES `CF_UNIT` (`id`),
  CONSTRAINT `FK_q52kki6epki20390cnrfcqulp` FOREIGN KEY (`QUNIT_ID`) REFERENCES `CF_UNIT` (`id`),
  CONSTRAINT `FK_qhbg21eutui861yocktoomar7` FOREIGN KEY (`BIDDING_CURRENCY_ID`) REFERENCES `CF_CURRENCY` (`id`),
  CONSTRAINT `FK_srpp65uxqfjsc8qw4ts4bbxec` FOREIGN KEY (`MAX_PRICE_ID`) REFERENCES `CF_PRICE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=951 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_PRODUCT_HISTORY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_PRODUCT_HISTORY` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `DESCRIPTION` text,
  `NAME` varchar(255) DEFAULT NULL,
  `QUANTITY` decimal(19,2) DEFAULT NULL,
  `TAGS` varchar(255) DEFAULT NULL,
  `OBJ_VERSION` int(11) DEFAULT NULL,
  `EVENT_DATE` datetime DEFAULT NULL,
  `UNIQUE_ID` varchar(255) DEFAULT NULL,
  `CATEGORY_ID` bigint(20) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DEFAULT_IMAGE_ID` bigint(20) DEFAULT NULL,
  `LAST_PRICE_ID` bigint(20) DEFAULT NULL,
  `LATEST_PRICE_ID` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  `OWNER_ID` bigint(20) DEFAULT NULL,
  `PRODUCT_ID` bigint(20) DEFAULT NULL,
  `QUNIT_ID` bigint(20) DEFAULT NULL,
  `STATUS_ID` bigint(20) DEFAULT NULL,
  `EVENT_BY` bigint(20) DEFAULT NULL,
  `PRODUCT_STATUS_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_nhlgwsory9v01amv618glqr5y` (`CATEGORY_ID`),
  KEY `FK_otn4kyyi75wkekxtnv9lhvqn1` (`CREATED_BY`),
  KEY `FK_12jf3sto51cx1ywt8y4n847wt` (`DEFAULT_IMAGE_ID`),
  KEY `FK_bc1drbsvjmyygfkos3u88cepa` (`LAST_PRICE_ID`),
  KEY `FK_86hgy9bdw2f0wb9hhtqdes9j8` (`LATEST_PRICE_ID`),
  KEY `FK_ejqynia6ockch817s3tol5jfx` (`MODIFIED_BY`),
  KEY `FK_imckt9ink5c13b2ps2lyv7evm` (`OWNER_ID`),
  KEY `FK_kimu5c65b0g8dh3nlmkb2fdat` (`PRODUCT_ID`),
  KEY `FK_88wbppv28m25ba55j20iriepn` (`QUNIT_ID`),
  KEY `FK_4pk4u6vsnjvoegoullyb0ktpf` (`STATUS_ID`),
  KEY `FK_fmmxjecwgi4ga6p44rbgg0u7u` (`EVENT_BY`),
  KEY `FK_147bqju737253kb5kegab969l` (`PRODUCT_STATUS_ID`),
  CONSTRAINT `FK_12jf3sto51cx1ywt8y4n847wt` FOREIGN KEY (`DEFAULT_IMAGE_ID`) REFERENCES `CF_IMAGE` (`id`),
  CONSTRAINT `FK_147bqju737253kb5kegab969l` FOREIGN KEY (`PRODUCT_STATUS_ID`) REFERENCES `CF_PRODUCT_STATUS` (`id`),
  CONSTRAINT `FK_4pk4u6vsnjvoegoullyb0ktpf` FOREIGN KEY (`STATUS_ID`) REFERENCES `CF_PRODUCT_STATUS` (`id`),
  CONSTRAINT `FK_86hgy9bdw2f0wb9hhtqdes9j8` FOREIGN KEY (`LATEST_PRICE_ID`) REFERENCES `CF_PRICE` (`id`),
  CONSTRAINT `FK_88wbppv28m25ba55j20iriepn` FOREIGN KEY (`QUNIT_ID`) REFERENCES `CF_UNIT` (`id`),
  CONSTRAINT `FK_bc1drbsvjmyygfkos3u88cepa` FOREIGN KEY (`LAST_PRICE_ID`) REFERENCES `CF_PRICE` (`id`),
  CONSTRAINT `FK_ejqynia6ockch817s3tol5jfx` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_fmmxjecwgi4ga6p44rbgg0u7u` FOREIGN KEY (`EVENT_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_imckt9ink5c13b2ps2lyv7evm` FOREIGN KEY (`OWNER_ID`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_kimu5c65b0g8dh3nlmkb2fdat` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `CF_PRODUCT` (`id`),
  CONSTRAINT `FK_nhlgwsory9v01amv618glqr5y` FOREIGN KEY (`CATEGORY_ID`) REFERENCES `CF_CATEGORY` (`id`),
  CONSTRAINT `FK_otn4kyyi75wkekxtnv9lhvqn1` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_PRODUCT_REVIEW`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_PRODUCT_REVIEW` (
  `PRODUCT_ID` bigint(20) NOT NULL,
  `COMMENT` varchar(255) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `RATING_ID` bigint(20) DEFAULT NULL,
  `REVIEW_BY` bigint(20) DEFAULT NULL,
  `REVIEW_DATE` datetime DEFAULT NULL,
  `IS_VALID` bit(1) DEFAULT NULL,
  KEY `FK_bv9q73s7wrjrx1r1f25lucfrp` (`RATING_ID`),
  KEY `FK_cws9xfavcaw0glycow5wpjtb0` (`REVIEW_BY`),
  KEY `FK_q0016xt1c2hnlli5myx93wb0v` (`PRODUCT_ID`),
  CONSTRAINT `FK_bv9q73s7wrjrx1r1f25lucfrp` FOREIGN KEY (`RATING_ID`) REFERENCES `CF_RATING` (`id`),
  CONSTRAINT `FK_cws9xfavcaw0glycow5wpjtb0` FOREIGN KEY (`REVIEW_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_q0016xt1c2hnlli5myx93wb0v` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `CF_PRODUCT` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_PRODUCT_STATUS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_PRODUCT_STATUS` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `CODE` varchar(255) DEFAULT NULL,
  `STATUS` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_PRODUCT_TAG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_PRODUCT_TAG` (
  `PRODUCT_ID` bigint(20) NOT NULL,
  `TAG_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`PRODUCT_ID`,`TAG_ID`),
  KEY `FK_h14qcqmb8byhj23q56w4y6yxl` (`TAG_ID`),
  CONSTRAINT `FK_h14qcqmb8byhj23q56w4y6yxl` FOREIGN KEY (`TAG_ID`) REFERENCES `CF_TAG` (`id`),
  CONSTRAINT `FK_s1gw5xrjpwrgxj81kpr5h8sd2` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `CF_PRODUCT` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_PRODUCT_TYPE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_PRODUCT_TYPE` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `TYPE` varchar(255) DEFAULT NULL,
  `TYPE_CODE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_PROD_ATTRS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_PROD_ATTRS` (
  `PRODUCT_ID` bigint(20) NOT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `VALUE` varchar(255) DEFAULT NULL,
  KEY `FK_9u5pu2cc9sx2eluudwpmf7xeh` (`PRODUCT_ID`),
  CONSTRAINT `FK_9u5pu2cc9sx2eluudwpmf7xeh` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `CF_PRODUCT` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_RATING`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_RATING` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `VALUE` decimal(19,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_ROLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_ROLE` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `IS_FROZEN` bit(1) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_lkrvsw20e2v4glqg2n6qvua2` (`CREATED_BY`),
  KEY `FK_9piaub4n1euv6m7qyshdc2xop` (`MODIFIED_BY`),
  CONSTRAINT `FK_9piaub4n1euv6m7qyshdc2xop` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_lkrvsw20e2v4glqg2n6qvua2` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_SELLER_WISHLIST`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_SELLER_WISHLIST` (
  `DEMAND_ID` bigint(20) NOT NULL,
  `SELLER_ID` bigint(20) NOT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`DEMAND_ID`,`SELLER_ID`),
  KEY `FK_dof6rlcgppesa7xgdroi21gk7` (`CREATED_BY`),
  KEY `FK_76skex1tpsv8icw3a8h1gigoa` (`MODIFIED_BY`),
  KEY `FK_inq1io16ni4ssekwe0gkjp6xv` (`SELLER_ID`),
  CONSTRAINT `FK_76skex1tpsv8icw3a8h1gigoa` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_ceb72mryi4djbmuka33m3o6d8` FOREIGN KEY (`DEMAND_ID`) REFERENCES `CF_DEMAND` (`id`),
  CONSTRAINT `FK_dof6rlcgppesa7xgdroi21gk7` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_inq1io16ni4ssekwe0gkjp6xv` FOREIGN KEY (`SELLER_ID`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_SOCIAL_ACCOUNTS`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_SOCIAL_ACCOUNTS` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ACCOUNT_URL` varchar(255) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `DISPLAY_NAME` varchar(255) DEFAULT NULL,
  `SOCIAL_SITE` varchar(255) DEFAULT NULL,
  `UNIQUE_ID` varchar(255) NOT NULL,
  `USERNAME` varchar(255) DEFAULT NULL,
  `OBJ_VERSION` int(11) DEFAULT NULL,
  `COMPANY_ID` bigint(20) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_o807myk1u6y4hwjgtd9djj9u1` (`UNIQUE_ID`),
  KEY `FK_qroyb7o228td6426l9yma0dve` (`COMPANY_ID`),
  KEY `FK_8fw71ps0w3ols6944egn84crh` (`CREATED_BY`),
  KEY `FK_89f1lhcqpij0aq05ymuyavfhg` (`MODIFIED_BY`),
  KEY `FK_2epj7vpsfaxckvdahgladxsk` (`USER_ID`),
  CONSTRAINT `FK_2epj7vpsfaxckvdahgladxsk` FOREIGN KEY (`USER_ID`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_89f1lhcqpij0aq05ymuyavfhg` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_8fw71ps0w3ols6944egn84crh` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_qroyb7o228td6426l9yma0dve` FOREIGN KEY (`COMPANY_ID`) REFERENCES `CF_COMPANY` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_STATE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_STATE` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `COUNTRY_ID` bigint(20) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_6i0gl98b7pln9jn6fswi3vnw7` (`COUNTRY_ID`),
  KEY `FK_qpiw7vfpyuva8unraia0q31ol` (`CREATED_BY`),
  KEY `FK_qbghv6p038qxrpiayxe2iqylr` (`MODIFIED_BY`),
  CONSTRAINT `FK_6i0gl98b7pln9jn6fswi3vnw7` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `CF_COUNTRY` (`id`),
  CONSTRAINT `FK_qbghv6p038qxrpiayxe2iqylr` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_qpiw7vfpyuva8unraia0q31ol` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_TAG`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_TAG` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `SHORT_DESC` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_d8llc7jhmc521ddgl3uimjetx` (`CREATED_BY`),
  KEY `FK_jkcvjkuse2pos4hvl0gjvjh72` (`MODIFIED_BY`),
  CONSTRAINT `FK_d8llc7jhmc521ddgl3uimjetx` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_jkcvjkuse2pos4hvl0gjvjh72` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_TRANSACTION`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_TRANSACTION` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `STATE` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_TRANSPORT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_TRANSPORT` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `vehicleNbr` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_UNIT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_UNIT` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `COUNTRY_CODE` varchar(255) DEFAULT NULL,
  `IS_DEFAULT` bit(1) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `SYMBOL` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_UPCOMING_EVENT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_UPCOMING_EVENT` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `EVENT` varchar(255) DEFAULT NULL,
  `EVENT_DATE` datetime DEFAULT NULL,
  `EVENT_DETAIL` varchar(255) DEFAULT NULL,
  `UNIQUE_ID` varchar(255) NOT NULL,
  `COMPANY_ID` bigint(20) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_osxeqs882h00krjx32tx9cbed` (`UNIQUE_ID`),
  KEY `FK_ojsc1iom321wg5l0orw0xg18d` (`COMPANY_ID`),
  KEY `FK_g7tpie8rhsx2for172dopalq7` (`CREATED_BY`),
  KEY `FK_rdrnjlkpcvpry6a9spbk50mg8` (`MODIFIED_BY`),
  KEY `FK_od84occ7loektjl56d9xuwuhi` (`USER_ID`),
  CONSTRAINT `FK_g7tpie8rhsx2for172dopalq7` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_od84occ7loektjl56d9xuwuhi` FOREIGN KEY (`USER_ID`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_ojsc1iom321wg5l0orw0xg18d` FOREIGN KEY (`COMPANY_ID`) REFERENCES `CF_COMPANY` (`id`),
  CONSTRAINT `FK_rdrnjlkpcvpry6a9spbk50mg8` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_UPCOMING_EVENT_COMPANY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_UPCOMING_EVENT_COMPANY` (
  `UPCOMING_EVENT_ID` bigint(20) NOT NULL,
  `COMPANY_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`UPCOMING_EVENT_ID`,`COMPANY_ID`),
  KEY `FK_had4yxubn6wvnuswgp3ydy9cg` (`COMPANY_ID`),
  CONSTRAINT `FK_had4yxubn6wvnuswgp3ydy9cg` FOREIGN KEY (`COMPANY_ID`) REFERENCES `CF_COMPANY` (`id`),
  CONSTRAINT `FK_pri7m5c2gufn8tg7cvr50homv` FOREIGN KEY (`UPCOMING_EVENT_ID`) REFERENCES `CF_UPCOMING_EVENT` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_UPCOMING_EVENT_GROUP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_UPCOMING_EVENT_GROUP` (
  `UPCOMING_EVENT_ID` bigint(20) NOT NULL,
  `GROUP_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`UPCOMING_EVENT_ID`,`GROUP_ID`),
  KEY `FK_ql0i5vmvudnx1n5jy0qunw7qd` (`GROUP_ID`),
  CONSTRAINT `FK_8b140cmw2soynrxq321e552ad` FOREIGN KEY (`UPCOMING_EVENT_ID`) REFERENCES `CF_UPCOMING_EVENT` (`id`),
  CONSTRAINT `FK_ql0i5vmvudnx1n5jy0qunw7qd` FOREIGN KEY (`GROUP_ID`) REFERENCES `CF_GROUP` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_UPCOMING_EVENT_USER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_UPCOMING_EVENT_USER` (
  `UPCOMING_EVENT_ID` bigint(20) NOT NULL,
  `USER_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`UPCOMING_EVENT_ID`,`USER_ID`),
  KEY `FK_7r09wt2dimsew5tujyvh4c24c` (`USER_ID`),
  CONSTRAINT `FK_2aimuno1hlhhmch97ylm5wtaj` FOREIGN KEY (`UPCOMING_EVENT_ID`) REFERENCES `CF_UPCOMING_EVENT` (`id`),
  CONSTRAINT `FK_7r09wt2dimsew5tujyvh4c24c` FOREIGN KEY (`USER_ID`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_USER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_USER` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `ADDRESS_LINE_1` varchar(255) DEFAULT NULL,
  `ADDRESS_LINE_2` varchar(255) DEFAULT NULL,
  `CITY` varchar(255) DEFAULT NULL,
  `ZIPCODE` varchar(255) DEFAULT NULL,
  `ALTERNATE_USERNAME` varchar(255) DEFAULT NULL,
  `IS_ARCHIVED` bit(1) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `DEFAULT_LOCALE` varchar(255) DEFAULT NULL,
  `EMAIL` varchar(255) DEFAULT NULL,
  `EMAIL_VERIFIED` bit(1) DEFAULT NULL,
  `EMAIL_VERIFY_CODE` varchar(255) DEFAULT NULL,
  `EMPLOYEE_ID` varchar(255) DEFAULT NULL,
  `FAILED_LOGIN_ATTEMPT` int(11) DEFAULT NULL,
  `FIRST_NAME` varchar(255) DEFAULT NULL,
  `LAST_NAME` varchar(255) DEFAULT NULL,
  `MIDDLE_INITIAL` varchar(255) DEFAULT NULL,
  `MOBILE_NO` varchar(255) DEFAULT NULL,
  `MOBILE_VERIFIED` bit(1) DEFAULT NULL,
  `MOBILE_VERIFY_CODE` varchar(255) DEFAULT NULL,
  `CF_PASSWD` varchar(255) DEFAULT NULL,
  `PHONE_NO` varchar(255) DEFAULT NULL,
  `RESET_PASSWD_CODE` varchar(255) DEFAULT NULL,
  `SHORT_DESC` longtext,
  `IS_TRANSPORT_AVAILABLE` bit(1) DEFAULT NULL,
  `UNIQUE_CODE` varchar(255) DEFAULT NULL,
  `USER_NO` varchar(255) NOT NULL,
  `USER_TYPE` varchar(255) NOT NULL,
  `USERNAME` varchar(255) DEFAULT NULL,
  `VERIFIED` bit(1) DEFAULT NULL,
  `OBJ_VERSION` int(11) DEFAULT NULL,
  `WEBSITE_URL` varchar(255) DEFAULT NULL,
  `COUNTRY_ID` bigint(20) DEFAULT NULL,
  `STATE_ID` bigint(20) DEFAULT NULL,
  `COMPANY_ID` bigint(20) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `DEFAULT_BILLING_ADDRESS_ID` bigint(20) DEFAULT NULL,
  `DEFAULT_BILLING_DETAILS_ID` bigint(20) DEFAULT NULL,
  `DEFAULT_COUNTRY_ID` bigint(20) DEFAULT NULL,
  `DEFAULT_DELIVERY_ADDRESS_ID` bigint(20) DEFAULT NULL,
  `DEFAULT_IMAGE_ID` bigint(20) DEFAULT NULL,
  `DEFAULT_LANGUAGE_ID` bigint(20) DEFAULT NULL,
  `DEPARTMENT_ID` bigint(20) DEFAULT NULL,
  `DESIGNATION_ID` bigint(20) DEFAULT NULL,
  `GROUP_ID` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `UK_qcmore2xd8y9l4b7n60f0nye4` (`USER_NO`),
  UNIQUE KEY `UK_ipmfxgb8xja6xaqlwh3s7q5cv` (`ALTERNATE_USERNAME`),
  UNIQUE KEY `UK_tmo2hvbcvvx4ifpjf41vfy6hr` (`USERNAME`),
  KEY `FK_gnscfqxmf42lpnk0rm9r6lwrd` (`COUNTRY_ID`),
  KEY `FK_p1vgpyplasg1ygxeoqkgj5kmx` (`STATE_ID`),
  KEY `FK_othik7vfg7mwxh6lvvhpc8te8` (`COMPANY_ID`),
  KEY `FK_9ik3s4nx2m5exchmih5f1x3fw` (`CREATED_BY`),
  KEY `FK_tjryipb8akr4n9tq67uf5v61w` (`DEFAULT_BILLING_ADDRESS_ID`),
  KEY `FK_mc3yjdw6duv64w6m7u1fcwvu9` (`DEFAULT_BILLING_DETAILS_ID`),
  KEY `FK_a9c7jdjkdmy223uuadjjgrija` (`DEFAULT_COUNTRY_ID`),
  KEY `FK_amxul0wmcm1m7x3qgy488gquv` (`DEFAULT_DELIVERY_ADDRESS_ID`),
  KEY `FK_5oa8xqd31b4lc1gu1i0qvvqmc` (`DEFAULT_IMAGE_ID`),
  KEY `FK_c5kclrbfrvpjxabkfgm6kxwf` (`DEFAULT_LANGUAGE_ID`),
  KEY `FK_bo006df6pr33oyp86bdgxcatt` (`DEPARTMENT_ID`),
  KEY `FK_8dx0uvj0hhvsvxy7uu0hgbwiq` (`DESIGNATION_ID`),
  KEY `FK_nfj16fr2m94lpsqu6cw5eplum` (`GROUP_ID`),
  KEY `FK_rloertderfbvnm4rv9oeovxqs` (`MODIFIED_BY`),
  CONSTRAINT `FK_5oa8xqd31b4lc1gu1i0qvvqmc` FOREIGN KEY (`DEFAULT_IMAGE_ID`) REFERENCES `CF_IMAGE` (`id`),
  CONSTRAINT `FK_8dx0uvj0hhvsvxy7uu0hgbwiq` FOREIGN KEY (`DESIGNATION_ID`) REFERENCES `CF_DESIGNATION` (`id`),
  CONSTRAINT `FK_9ik3s4nx2m5exchmih5f1x3fw` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_a9c7jdjkdmy223uuadjjgrija` FOREIGN KEY (`DEFAULT_COUNTRY_ID`) REFERENCES `CF_COUNTRY` (`id`),
  CONSTRAINT `FK_amxul0wmcm1m7x3qgy488gquv` FOREIGN KEY (`DEFAULT_DELIVERY_ADDRESS_ID`) REFERENCES `CF_ADDRESS` (`id`),
  CONSTRAINT `FK_bo006df6pr33oyp86bdgxcatt` FOREIGN KEY (`DEPARTMENT_ID`) REFERENCES `CF_DEPARTMENT` (`id`),
  CONSTRAINT `FK_c5kclrbfrvpjxabkfgm6kxwf` FOREIGN KEY (`DEFAULT_LANGUAGE_ID`) REFERENCES `CF_LANGUAGE` (`id`),
  CONSTRAINT `FK_gnscfqxmf42lpnk0rm9r6lwrd` FOREIGN KEY (`COUNTRY_ID`) REFERENCES `CF_COUNTRY` (`id`),
  CONSTRAINT `FK_mc3yjdw6duv64w6m7u1fcwvu9` FOREIGN KEY (`DEFAULT_BILLING_DETAILS_ID`) REFERENCES `CF_BILLING_DETAILS` (`id`),
  CONSTRAINT `FK_nfj16fr2m94lpsqu6cw5eplum` FOREIGN KEY (`GROUP_ID`) REFERENCES `CF_GROUP` (`id`),
  CONSTRAINT `FK_othik7vfg7mwxh6lvvhpc8te8` FOREIGN KEY (`COMPANY_ID`) REFERENCES `CF_COMPANY` (`id`),
  CONSTRAINT `FK_p1vgpyplasg1ygxeoqkgj5kmx` FOREIGN KEY (`STATE_ID`) REFERENCES `CF_STATE` (`id`),
  CONSTRAINT `FK_rloertderfbvnm4rv9oeovxqs` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_tjryipb8akr4n9tq67uf5v61w` FOREIGN KEY (`DEFAULT_BILLING_ADDRESS_ID`) REFERENCES `CF_ADDRESS` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_USER_PLAN`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_USER_PLAN` (
  `PLAN_ID` bigint(20) NOT NULL,
  `USER_ID` bigint(20) NOT NULL,
  `EXPIRY_DATE` datetime DEFAULT NULL,
  `IS_ACTIVE` bit(1) DEFAULT NULL,
  `START_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`PLAN_ID`,`USER_ID`),
  KEY `FK_e09dc5lqocv3ytqjlgecs5ahl` (`USER_ID`),
  CONSTRAINT `FK_39fjejb211waur63e4iwlnrf7` FOREIGN KEY (`PLAN_ID`) REFERENCES `CF_PLAN` (`id`),
  CONSTRAINT `FK_e09dc5lqocv3ytqjlgecs5ahl` FOREIGN KEY (`USER_ID`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_USER_REVIEW`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_USER_REVIEW` (
  `USER_ID` bigint(20) NOT NULL,
  `COMMENT` varchar(255) DEFAULT NULL,
  `EMAIL_ADDRESS` varchar(255) DEFAULT NULL,
  `NAME` varchar(255) DEFAULT NULL,
  `RATING_ID` bigint(20) DEFAULT NULL,
  `REVIEW_BY` bigint(20) DEFAULT NULL,
  `REVIEW_DATE` datetime DEFAULT NULL,
  `IS_VALID` bit(1) DEFAULT NULL,
  KEY `FK_2aekueo5okk8e8a58ts95m67m` (`RATING_ID`),
  KEY `FK_hjxbdv37i2dwf0epvl16wsdut` (`REVIEW_BY`),
  KEY `FK_m1gix9n9tql9k71re6t1835yh` (`USER_ID`),
  CONSTRAINT `FK_2aekueo5okk8e8a58ts95m67m` FOREIGN KEY (`RATING_ID`) REFERENCES `CF_RATING` (`id`),
  CONSTRAINT `FK_hjxbdv37i2dwf0epvl16wsdut` FOREIGN KEY (`REVIEW_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_m1gix9n9tql9k71re6t1835yh` FOREIGN KEY (`USER_ID`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_WISHLIST`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_WISHLIST` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `IS_ACTIVE` bit(1) DEFAULT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `MODIFIED_ON` datetime DEFAULT NULL,
  `UNIQUE_ID` varchar(255) DEFAULT NULL,
  `OBJ_VERSION` int(11) DEFAULT NULL,
  `COMPANY_ID` bigint(20) DEFAULT NULL,
  `CREATED_BY` bigint(20) DEFAULT NULL,
  `MODIFIED_BY` bigint(20) DEFAULT NULL,
  `USER_ID` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_1jp0h8x6ovxb6oh5fjy81q4ra` (`COMPANY_ID`),
  KEY `FK_mv88id588meucvxa8hggwy3g8` (`CREATED_BY`),
  KEY `FK_5vmi3lqukntdj3343dhvuk9ie` (`MODIFIED_BY`),
  KEY `FK_nhw367kysc6lxxgo2brkhmlt` (`USER_ID`),
  CONSTRAINT `FK_1jp0h8x6ovxb6oh5fjy81q4ra` FOREIGN KEY (`COMPANY_ID`) REFERENCES `CF_COMPANY` (`id`),
  CONSTRAINT `FK_5vmi3lqukntdj3343dhvuk9ie` FOREIGN KEY (`MODIFIED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_mv88id588meucvxa8hggwy3g8` FOREIGN KEY (`CREATED_BY`) REFERENCES `CF_USER` (`ID`),
  CONSTRAINT `FK_nhw367kysc6lxxgo2brkhmlt` FOREIGN KEY (`USER_ID`) REFERENCES `CF_USER` (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_WISHLIST_DEMAND`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_WISHLIST_DEMAND` (
  `WISHLIST_ID` bigint(20) NOT NULL,
  `DEMAND_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`WISHLIST_ID`,`DEMAND_ID`),
  KEY `FK_l5ald79tdpti2c0y86ot8voef` (`DEMAND_ID`),
  CONSTRAINT `FK_8w69tv57c59nxamr5wbyyssd4` FOREIGN KEY (`WISHLIST_ID`) REFERENCES `CF_WISHLIST` (`id`),
  CONSTRAINT `FK_l5ald79tdpti2c0y86ot8voef` FOREIGN KEY (`DEMAND_ID`) REFERENCES `CF_DEMAND` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
DROP TABLE IF EXISTS `CF_WISHLIST_PRODUCT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CF_WISHLIST_PRODUCT` (
  `WISHLIST_ID` bigint(20) NOT NULL,
  `PRODUCT_ID` bigint(20) NOT NULL,
  PRIMARY KEY (`WISHLIST_ID`,`PRODUCT_ID`),
  KEY `FK_82dr65wgvb05ru3q71rdv2xv9` (`PRODUCT_ID`),
  CONSTRAINT `FK_82dr65wgvb05ru3q71rdv2xv9` FOREIGN KEY (`PRODUCT_ID`) REFERENCES `CF_PRODUCT` (`id`),
  CONSTRAINT `FK_myqludsmsu6u34y2dlye4jwwp` FOREIGN KEY (`WISHLIST_ID`) REFERENCES `CF_WISHLIST` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
