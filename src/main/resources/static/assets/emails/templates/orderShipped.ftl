<!-- 
 * Template Name: Unify - Responsive Bootstrap Template
 * Description: Business, Corporate, Portfolio and Blog Theme.
 * Version: 1.6
 * Author: @htmlstream
 * Website: http://htmlstream.com
 -->
<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Responsive Email Template</title>
                                                                                                                                                                                                                                                                                                                                                                                                        
<style type="text/css">
    .ReadMsgBody {width: 100%; background-color: #ffffff;}
    .ExternalClass {width: 100%; background-color: #ffffff;}
    body     {width: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;font-family: Arial, Helvetica, sans-serif}
    table {border-collapse: collapse;}

     .vocac-text {
        color: #e67e22;
        font-weight: bold;
        margin-bottom: 40px;
        margin-top: 40px;
        text-transform: uppercase;
        font-family: "Open Sans",Arial,sans-serif;
    }
    
    .top-middle {
        color: #ddd;
    }
    
    @media only screen and (max-width: 640px)  {
                    body[yahoo] .deviceWidth {width:440px!important; padding:0;}    
                    body[yahoo] .center {text-align: center!important;}  
            }
            
    @media only screen and (max-width: 479px) {
                    body[yahoo] .deviceWidth {width:280px!important; padding:0;}    
                    body[yahoo] .center {text-align: center!important;}  
            }
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix" style="font-family: Arial, Helvetica, sans-serif">

<!-- Wrapper -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
    <tr>
        <td width="100%" valign="top" bgcolor="#ffffff" style="padding-top:20px">
            
            <!--Start Header-->
            <table width="700" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
                <tr>
                    <td style="padding: 6px 0px 0px">
                        <table width="680" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
                            <tr>
                                <td width="100%" >
                                    <!--Start logo-->
                                    <table  border="0" cellpadding="0" cellspacing="0" align="left" class="deviceWidth">
                                        <tr>
                                            <td class="center" style="padding: 10px 0px 10px 0px">
                                                <!--<h1 class="vocac-text">v<span class="top-middle">o</span>cac</h1>-->
                                                <!--<a href="#"><img src="img/logo/logo_orange.png"></a>-->
                                            </td>
                                        </tr>
                                    </table><!--End logo-->
                                </td>
                            </tr>
                        </table>
                   </td>
                </tr>
            </table> 
            <!--End Header-->

            <!-- Start Headliner-->
            <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
                <tr>
                    <td valign="top" style="padding: 0px " class="center">
                        <!--<a href="#"><img class="deviceWidth" src="img/headliner/headliner_orange.jpg"></a>-->
                        
                        <p class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 20px 10px; ">
                            Hi ${user.firstName} ${user.lastName},
                            
                            <br/>
                            <br/>
                            
                            Your order has been shipped. It will get delivered by ${order.deliveryDate}. You can track you order online via the following link.
                            <br/>
                            <br/>
                            
                            To track you order, click <a style="text-decoration: none; color: #e67e22;" href="${contextPath}/#/order/track?orderNo=${order.orderNo}">Track Order.</a>
                            
                            <br/>
                            <br/>
                            
                            Thank you for using our services.
                            <br/>
                            <br/>
                            <!--we are providing a wide range of services to our customer in agriculture domain. -->
                            
                            Thanks &amp; Regards, <br/>
                            Support Team, <br/>
                            Vocac International Farms <br/>
                            Ph. +91 971 781 9586
                        </p>
                    </td>
                </tr>
            </table>
            <!-- Start Headliner-->

            <!--Start Weekly Prize-->
            <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
                <tr>
                    <td width="100%" bgcolor="#a5d1da" class="center">
                        <table  border="0" cellpadding="0" cellspacing="0" align="center"> 
                            <tr>
                                <td valign="top" style="padding: 20px 10px " class="center">
                                    <a href="#"><img width="32" hight="32" src="cid:facebook"></a>
                                </td>
                                <td valign="top" style="padding: 20px 10px " class="center">
                                    <a href="#"><img width="32" hight="32" src="cid:twitter"></a>
                                </td>
                                <td valign="top" style="padding: 20px 10px " class="center">
                                    <a href="#"><img width="32" hight="32" src="cid:youtube"></a>
                                </td>
                            </tr>
                        </table>
                        <table  border="0" cellpadding="0" cellspacing="0" align="center"> 
                            <tr>
                                <td  class="center" style="font-size: 16px; color: #ffffff; font-weight: bold; text-align: center; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 0px 10px; ">
                                    Stay in touch with us                            
                                </td>
                            </tr>
                               <td  class="center" style="font-size: 12px; color: #ffffff; font-weight: bold; text-align: center; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 20px 10px; " >
                                   we are providing all categories of products like Cereals &amp; Pulse, Dry Fruits, Fruits etc. we are providing all categories of products like Cereals &amp; Pulse, Dry Fruits, Fruits etc. Either you can try to find out the products yourself or you can specify your demands online and we will manage it to you.
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
            <!--Weekly Prize-->

            <!-- Footer -->
            <table width="700"  border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth"  > 
                <tr>
                    <td  bgcolor="#ffffff" class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: center; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 20px 50px 0px 50px; " >
                        Copyright © Vocac International Farms. 2016                            
                    </td>

                </tr>
                <tr>
                    <td  bgcolor="#ffffff" class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: center; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 20px 0px 0px; " >
                        Vocac International is an online service provider to agriculture products. Vocac International is the fastest growing company of India in agricutural products. it provides services all over the India.
                    </td>
                </tr>

                  <tr>
                    <td  bgcolor="#ffffff" class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: center; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 20px 10px; " >
                         If you would prefer not to receive email communications from Vocac International click <a style="text-decoration: none; color: #e67e22;" href="#">here</a>  
                    </td>
                </tr>
            </table>
            <!--End Footer-->

        </td>
    </tr>
</table> 
<!-- End Wrapper -->
</body>
</html>