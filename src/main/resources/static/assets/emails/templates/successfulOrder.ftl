<!-- 
 * Template Name: Unify - Responsive Bootstrap Template
 * Description: Business, Corporate, Portfolio and Blog Theme.
 * Version: 1.6
 * Author: @htmlstream
 * Website: http://htmlstream.com
 -->
<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Responsive Email Template</title>
                                                                                                                                                                                                                                                                                                                                                                                                        
<style type="text/css">
    .ReadMsgBody {width: 100%; background-color: #ffffff;}
    .ExternalClass {width: 100%; background-color: #ffffff;}
    body     {width: 100%; background-color: #ffffff; margin:0; padding:0; -webkit-font-smoothing: antialiased;font-family: Arial, Helvetica, sans-serif}
    table {border-collapse: collapse;}

     .vocac-text {
        color: #e67e22;
        font-weight: bold;
        margin-bottom: 40px;
        margin-top: 40px;
        text-transform: uppercase;
        font-family: "Open Sans",Arial,sans-serif;
    }
    
    .top-middle {
        color: #ddd;
    }
    
    @media only screen and (max-width: 640px)  {
                    body[yahoo] .deviceWidth {width:440px!important; padding:0;}    
                    body[yahoo] .center {text-align: center!important;}  
            }
            
    @media only screen and (max-width: 479px) {
                    body[yahoo] .deviceWidth {width:280px!important; padding:0;}    
                    body[yahoo] .center {text-align: center!important;}  
            }
</style>
</head>
<body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" yahoo="fix" style="font-family: Arial, Helvetica, sans-serif">

<!-- Wrapper -->
<table width="100%" border="0" cellpadding="0" cellspacing="0" align="center">
    <tr>
        <td width="100%" valign="top" bgcolor="#ffffff" style="padding-top:20px">
            
            <!--Start Header-->
            <table width="700" bgcolor="#fff" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
                <tr>
                    <td style="padding: 6px 0px 0px">
                        <table width="680" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
                            <tr>
                                <td width="100%" >
                                    <!--Start logo-->
                                    <table  border="0" cellpadding="0" cellspacing="0" align="left" class="deviceWidth">
                                        <tr>
                                            <td class="center" style="padding: 10px 0px 10px 0px">
                                                <!--<h1 class="vocac-text">v<span class="top-middle">o</span>cac</h1>-->
                                                <!--<a href="#"><img src="img/logo/logo_orange.png"></a>-->
                                            </td>
                                        </tr>
                                    </table><!--End logo-->
                                </td>
                            </tr>
                        </table>
                   </td>
                </tr>
            </table> 
            <!--End Header-->

            <!-- Start Headliner-->
            <table width="700" border="0" cellpadding="0" cellspacing="0" align="center" class="deviceWidth">
                <tr>
                    <td valign="top" style="padding: 0px " class="center">
                        <!--<a href="#"><img class="deviceWidth" src="img/headliner/headliner_orange.jpg"></a>-->
                        
                        <p class="center" style="font-size: 12px; color: #687074; font-weight: bold; text-align: left; font-family: Arial, Helvetica, sans-serif; line-height: 25px; vertical-align: middle; padding: 20px 10px; ">
                            Hi ${user.firstName} ${user.lastName},
                            
                            <br/>
                            <br/>
                            
                            Thank you for using our services. Your order has been placed successfully on 
                            <a style="text-decoration: none; color: #e67e22;" href="${contextPath}/#/">vocac.com</a>. Please find below your order details. 
                            <br/>
                            <br/>
                            
                            <table style="width:100%; border-top:3px solid #e67e22; border-collapse:collapse">
                                <tbody>
                                    <tr>
                                        <td style="font-size:14px;padding:11px 18px 18px 18px;background-color:rgb(239,239,239);width:50%;vertical-align:top;line-height:18px;font-family:Arial,sans-serif">
                                            <p style="margin:2px 0 9px 0;font:14px Arial,sans-serif"> <span style="font-size:14px;color:rgb(102,102,102)">Billing Address:</span><br> 
                                                <b> ${billingAddress.firstName} ${billingAddress.lastName} <br> 
                                                    ${billingAddress.addressLine1} <br> 
                                                    ${billingAddress.addressLine2} <br> 
                                                    ${billingAddress.city} ${billingAddress.state.name} <br>
                                                    ${billingAddress.country.name} ${billingAddress.zipcode} <br>
                                                    Phone No: ${billingAddress.phoneNo}<br>
                                                </b> 
                                            </p>
                                        </td>
                                        <td style="font-size:14px; padding:11px 18px 18px 18px; background-color:rgb(239,239,239); width:50%;vertical-align:top;line-height:18px;font-family:Arial,sans-serif">
                                            <p style="margin:2px 0 9px 0;font:14px Arial,sans-serif"> <span style="font-size:14px;color:rgb(102,102,102)">Shipping Address:</span><br> 
                                                <b> ${shippingAddress.firstName} ${shippingAddress.lastName} <br> 
                                                    ${shippingAddress.addressLine1} <br> 
                                                    ${shippingAddress.addressLine2} <br> 
                                                    ${shippingAddress.city} ${shippingAddress.state.name} <br>
                                                    ${shippingAddress.country.name} ${shippingAddress.zipcode} <br>
                                                    Phone No: ${shippingAddress.phoneNo}<br>
                                                </b>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        
                            <br><br><br>
                            <div style="margin-bottom: 10px;">Order Details</div>
                            
                            <table style="width:100%; border-top:3px solid #e67e22; border-collapse:collapse">
                                <tbody>
                                    <#if order.products??>
	                                    <#list order.products as product>
	                                    	<tr style="font-size: 14px; line-height: 18px; font-family: Arial,sans-serif; ${product?has_next?string('border-bottom:1px dotted #e67e22;', '')}">
		                                        <td style="padding:11px 18px 18px 18px; width: 30%; vertical-align:top;">
		                                            <a href="#"><img width="120" hight="80" src="cid:${product.uniqueId}"></a>
		                                            <br/>
		                                            <div style="margin-top: 5px; font-size: 12px;">
		                                                <a style="text-decoration: none; color: #e67e22;" href="">${product.name}</a>
		                                            </div>
		                                        </td>
		                                        <td style="padding:11px 18px 38px 18px; width: 25%; vertical-align: bottom;">
		                                            ${product.priceWithCurrency} /${product.priceUnit.symbol}
		                                        </td>
		                                        <td style="padding: 11px 18px 38px 18px; width: 20%; vertical-align:bottom; ">
		                                            ${product.quantity} ${product.quantityUnit.symbol}
		                                        </td>
		                                        <td style="padding:11px 18px 38px 18px; width: 25%; vertical-align: bottom; text-align: right;">
		                                        	${product.totalPrice}
		                                        </td>
		                                    </tr>
		                                    <#assign orderCurrency = product.priceCurrency.mneomic>
	                                    </#list>
	                                </#if>
	                                <#if order.items??>
	                                    <#list order.items as item>
	                                    	<tr style="font-size: 14px; line-height: 18px; font-family: Arial,sans-serif; ${item?has_next?string('border-bottom:1px dotted #e67e22;', '')}">
		                                        <td style="padding:11px 18px 18px 18px; width: 30%; vertical-align:top;">
		                                            <a href="#"><img width="120" hight="80" src="cid:${item.uniqueId}"></a>
		                                            <br/>
		                                            <div style="margin-top: 5px; font-size: 12px;">
		                                                <a style="text-decoration: none; color: #e67e22;" href="">${item.name}</a>
		                                            </div>
		                                        </td>
		                                        <td style="padding:11px 18px 38px 18px; width: 25%; vertical-align: bottom;">
		                                            ${item.price} /${item.priceUnit.symbol}
		                                        </td>
		                                        <td style="padding: 11px 18px 38px 18px; width: 20%; vertical-align:bottom; ">
		                                            ${item.quantity} ${item.quantityUnit.symbol}
		                                        </td>
		                                        <td style="padding:11px 18px 38px 18px; width: 25%; vertical-align: bottom; text-align: right;">
		                                        	${item.totalPrice}
		                                        </td>
		                                    </tr>
		                                    <#assign orderCurrency = item.priceCurrency.mneomic>
	                                    </#list>
	                                </#if>
                                </tbody>
                            </table>
                            <table style="width:100%; border-top:3px solid #e67e22; border-collapse:collapse">
                                <tbody>
                                    <tr style="font-size:12px; line-height:18px; font-family:Arial,sans-serif; margin-top: 20px;">
                                        <td style="padding: 9px 8px 8px 8px; width: 55%; vertical-align: top;">
                                            &nbsp;
                                        </td>
                                        <td style="padding: 9px 8px 8px 8px; width: 25%; vertical-align: bottom;">
                                            Subtotal:
                                        </td>
                                        <td style="padding: 9px 8px 8px 8px; width: 20%; vertical-align: bottom; text-align: right;">
                                            ${order.subtotal}
                                        </td>
                                    </tr>
                                    <tr style="font-size:12px; line-height:18px; font-family:Arial,sans-serif; margin-top: 10px;">
                                        <td style="padding: 4px 8px 8px 8px; width: 55%; vertical-align: top;">
                                            &nbsp;
                                        </td>
                                        <td style="padding: 4px 8px 8px 8px; width: 25%; vertical-align: bottom;">
                                            Shipping Charges:
                                        </td>
                                        <td style="padding: 4px 8px 8px 8px; width: 20%; vertical-align: bottom; text-align: right;">
                                            ${order.shippingCost}
                                        </td>
                                    </tr>
                                    <tr style="font-size:12px; line-height:18px; font-family:Arial,sans-serif; margin-top: 10px; border-bottom:1px dotted #e67e22;">
                                        <td style="padding: 14px 8px 8px 8px; width: 55%; vertical-align: top;">
                                            &nbsp;
                                        </td>
                                        <td style="padding: 14px 8px 8px 8px; width: 25%; vertical-align: bottom;">
                                            <b>Total Cost:</b>
                                        </td>
                                        <td style="padding: 14px 8px 8px 8px; width: 20%; vertical-align: bottom; text-align: right;">
                                            <b>${order.totalPrice}</b>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            
                            <br/>
                            <br/>
                            
                            <p style="margin:0 0 4px 0;font:12px/16px Arial,sans-serif"> Need to make changes to your order? Visit our <a href="${contextPath}/#/others/faqs" style="text-decoration:none;color:rgb(0,102,153);font:12px/16px Arial,sans-serif" target="_blank">Help Page</a> for more information.<br> </p>
                            
                            <p style="padding:0 0 20px 0;border-bottom:1px solid rgb(234,234,234);margin:10px 0 0 0;font:12px/16px Arial,sans-serif">
                                Some products have a limited quantity available for purchase. Please see the product’s Detail Page 
                                for the available quantity. Any orders which exceed this quantity will be automatically canceled.<br><br> 
                                We hope to see you again soon.<br><br> 
                                <span style="font-size:16px;font-weight:bold"> <strong>Vocac International Farms</strong> </span> 
                            </p>
                        
                            <p style="font-size:10px;color:rgb(102,102,102);line-height:16px;margin:0 0 10px 0;font:10px"> 
                                This email was sent from a notification-only address that cannot accept incoming email. Please do not reply 
                                to this message. 
                            </p>
                        
                        </p>
                    </td>
                </tr>
            </table>
            <!-- Start Headliner-->
    
        </td>
    </tr>
</table> 
<!-- End Wrapper -->
</body>
</html>
