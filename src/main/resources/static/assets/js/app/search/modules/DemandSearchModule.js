'use strict';

var DemandSearchModule = angular.module("DemandSearchModule", ['SearchService']);

DemandSearchModule.controller("DemandSearchListCtrl", ['$rootScope', '$scope', '$http', '$location', '$stateParams', '$timeout', '$modal', 'GrainCartService', 'GRAINCART_EVENTS', 'WishlistService', 'WISHLIST_EVENTS', 'ngToast', 'SearchService', 'usSpinnerService',   
           function ($rootScope, $scope, $http, $location, $stateParams, $timeout, $modal, GrainCartService, GRAINCART_EVENTS, WishlistService, WISHLIST_EVENTS, ngToast, SearchService, usSpinnerService) {
   	$scope.pageNumber = 0;
   	$scope.totalPage = 0;
   	$scope.pageSize = 10;
   	$scope.sizeText = '10';
   	$scope.sortBy = "dateModified";
   	$scope.uniqueId = $stateParams.uniqueId;
   	$scope.keywords = $stateParams.keywords;
   	$scope.category = '-category';
	$scope.location = $location.url();
	$scope.sellerFacets = null;
   	$scope.pages = [];
   	$scope.facets = {};
   	
   	handleSearch();
   	
   	$scope.filterResults = function() {
   		handleSearch();
   	}
   	
   	$scope.updatePageSize = function(pageSize, sizeText) {
		$scope.pageSize = pageSize;
		$scope.sizeText = sizeText;
		handleSearch();
	}
   	
   	$scope.prevPage = function() {
   		$scope.pageNumber = $scope.pageNumber - 1;
   		handleSearch();
   	};
   	
   	$scope.moveTo = function(page) {
   		$scope.pageNumber = page - 1;
   		handleSearch();
   	};
   	
   	$scope.nextPage = function() {
   		$scope.pageNumber = $scope.pageNumber + 1;
   		handleSearch();
   	};

   	$scope.addToWishlist = function(uniqueId) {
   		if (angular.isUndefined($scope.currentUser) || angular.equals($scope.currentUser, null)) {
   			var modalInstance = $modal.open({
   				animation : $scope.animationsEnabled,
   				templateUrl : 'assets/js/app/account/login-register-modal.component.html',
   				controller : 'LoginRegisterCtrl',
   				size : 'md',
   				scope: $scope
   			});
   			
   			modalInstance.result.then(function() {
   				WishlistService.addDemandToWishlist(uniqueId)
	   	   			.then(function(response) {
	   	   			$scope.sendMessageAndEvent(WISHLIST_EVENTS.itemAdded, 'success', 'Your Item has been added to the wishlist.')
	   	   			});
   				$log.info('Modal closed at: ' + new Date());
   			});
   		} else {
   	   		WishlistService.addDemandToWishlist(uniqueId)
   	   			.then(function(response) {
   	   				$scope.sendMessageAndEvent(WISHLIST_EVENTS.itemAdded, 'success', 'Your Item has been added to the wishlist.')
   	   			});
   		}
   	};
   	
	$scope.sendMessageAndEvent = function(broadcastEvent, statusClass, message) {
		$rootScope.$broadcast(broadcastEvent);
		ngToast.create({ className: statusClass, content: message });
	};
	
   	function handleSearch() {
   		var params = {
   			uniqueId : $stateParams.uniqueId,
   			keyword : $stateParams.keywords, 
   			page : $scope.pageNumber, 
   			size : $scope.pageSize, 
   			sort : $scope.sortBy 
   		};
   	   	
   	   	if ($scope.facets) {
   	   		params['facets'] = $scope.facets;
   	   	}
   	   	
   	   	usSpinnerService.spin('prak-spinner');
   	   	SearchService.search('search/demands', params)
   	   		.then(function(response) {
   	   			usSpinnerService.stop('prak-spinner');
	   	   		$timeout(function() {
		   	   		$scope.filters = response.data.filters || {};
	   	   	   		$scope.demands = response.data.results || {};
	   	   	   		angular.extend($scope, SearchService.paginate(response.data.resultSize, response.data.results, $scope.pageSize));
		   	   	});
   	   		});
   	}
   	
}]);

DemandSearchModule.controller("DemandSearchGridCtrl", ['$rootScope', '$scope', '$http', '$location', '$stateParams', '$timeout', 'GrainCartService', 'GRAINCART_EVENTS', 'WishlistService', 'WISHLIST_EVENTS', 'ngToast', 'SearchService', 'usSpinnerService',  
        function ($rootScope, $scope, $http, $location, $stateParams, $timeout, GrainCartService, GRAINCART_EVENTS, WishlistService, WISHLIST_EVENTS, ngToast, SearchService, usSpinnerService) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.pageSize = 12;
	$scope.sizeText = '12';
	$scope.sortBy = "dateModified";
	$scope.uniqueId = $stateParams.uniqueId;
	$scope.keywords = $stateParams.keywords;
	$scope.category = '-category';
	$scope.location = $location.url();
	$scope.pages = [];
   	$scope.facets = {};
   	
	handleSearch();
	
	$scope.filterResults = function() {
   		handleSearch();
   	}
	
	$scope.updatePageSize = function(pageSize, sizeText) {
		$scope.pageSize = pageSize;
		$scope.sizeText = sizeText;
		handleSearch();
	}
	
	$scope.prevPage = function() {
		$scope.pageNumber = $scope.pageNumber - 1;
		handleSearch();
	};
	
	$scope.moveTo = function(page) {
		$scope.pageNumber = page - 1;
		handleSearch();
	};
	
	$scope.nextPage = function() {
		$scope.pageNumber = $scope.pageNumber + 1;
		handleSearch();
	};

   	$scope.subtractQty = function(product) {
   		product.quantity = product.quantity - 1;
   	};
   	
   	$scope.addQty = function(product) {
   		product.quantity = product.quantity + 1;
   	};
	
   	function parseDate(str) {
   	    var mdy = str.split('/');
   	    return new Date(mdy[2], mdy[1], mdy[0]-1);
   	}
   	
	$scope.addToWishlist = function(uniqueId) {
		if (angular.isUndefined($scope.currentUser) || angular.equals($scope.currentUser, null)) {
   			var modalInstance = $modal.open({
   				animation : $scope.animationsEnabled,
   				templateUrl : 'assets/js/app/account/login-register-modal.component.html',
   				controller : 'LoginRegisterCtrl',
   				size : 'md',
   				scope: $scope
   			});
   			
   			modalInstance.result.then(function() {
   				WishlistService.addDemandToWishlist(uniqueId)
	   	   			.then(function(response) {
	   	   			$scope.sendMessageAndEvent(WISHLIST_EVENTS.itemAdded, 'success', 'Your Item has been added to the wishlist.')
	   	   			});
   				$log.info('Modal closed at: ' + new Date());
   			});
   		} else {
			WishlistService.addDemandToWishlist(uniqueId)
				.then(function(response) {
					$scope.sendMessageAndEvent(WISHLIST_EVENTS.itemAdded, 'success', 'Your Item has been added to the wishlist.')
				});
   		}
	};
	
	$scope.sendMessageAndEvent = function(broadcastEvent, statusClass, message) {
		$rootScope.$broadcast(broadcastEvent);
		ngToast.create({ className: statusClass, content: message });
	};

	function handleSearch() {
		var params = {
			uniqueId : $stateParams.uniqueId, 
			keyword : $scope.keywords, 
			page : $scope.pageNumber, 
			size : $scope.pageSize, 
			sort : $scope.sortBy, 
			filters: $scope.facets 
		};
		
		if ($scope.facets) {
			params['facets'] = $scope.facets;
   	   	}
		
		usSpinnerService.spin('prak-spinner');
		SearchService.search('search/demands', params)
	   		.then(function(response) {
	   			usSpinnerService.stop('prak-spinner');
	   	   		$timeout(function() {
		   	   		$scope.filters = response.data.filters || {};
					$scope.matrix = convertToMatrix(response.data.results);
					angular.extend($scope, SearchService.paginate(response.data.resultSize, response.data.results, $scope.pageSize));
		   	   	});
	   		});
	};
	
	function convertToMatrix(demands) {
		var matrix = [];
		for (var i = 0; i < demands.length; i++) {
			if (i % 3 === 0) 
				matrix[Math.floor(i / 3)] = [];
			matrix[Math.floor(i / 3)][i % 3] = demands[i];
		}
		return matrix;
	}; 
}]);

DemandSearchModule.controller("SearchDemandDetailCtrl", ['$rootScope', '$scope', '$http', '$stateParams', 'GrainCartService', 'GRAINCART_EVENTS', 'WishlistService', 'WISHLIST_EVENTS', function ($rootScope, $scope, $http, $stateParams, GrainCartService, GRAINCART_EVENTS, WishlistService, WISHLIST_EVENTS) {
	$http.get('demands/data/' + $stateParams.uniqueId)
		.then(function(response) {
			$scope.demand = response.data;
		});
	
	$scope.addToWishlist = function(uniqueId) {
		WishlistService.addProductToWishlist(uniqueId)
			.then(function(response) {
				$rootScope.$broadcast(WISHLIST_EVENTS.itemAdded);
			});
	};
	
}]);