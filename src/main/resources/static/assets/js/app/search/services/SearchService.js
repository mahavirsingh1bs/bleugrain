var SearchService = angular.module('SearchService', []);

SearchService.factory('SearchService', ['$http', function($http) {
	var searchService = {};
	
	searchService.search = function(url, params) {
		return $http.get(url, { params : params })
			.then(function(response) {
				return response;
			});
	};
	
	searchService.paginate = function(resultSize, results, pageSize) {
		var result = {
			resultSize: resultSize,
			totalPage: Math.ceil(resultSize / pageSize)
		};
		
		result.pages = [];
		for (var index = 0; index < result.totalPage; index++) {
			result.pages.push(index + 1);
		}
		
		return result;
	}
	
	return searchService;
}]);