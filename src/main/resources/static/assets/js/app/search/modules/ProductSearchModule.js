'use strict';

var ProductSearchModule = angular.module("ProductSearchModule", ['SearchService']);

ProductSearchModule.controller("ProductSearchListCtrl", ['$rootScope', '$scope', '$http', '$location', '$stateParams', '$timeout', '$modal', 'GrainCartService', 'GRAINCART_EVENTS', 'WishlistService', 'WISHLIST_EVENTS', 'ngToast', 'SearchService', 'usSpinnerService', 
           function ($rootScope, $scope, $http, $location, $stateParams, $timeout, $modal, GrainCartService, GRAINCART_EVENTS, WishlistService, WISHLIST_EVENTS, ngToast, SearchService, usSpinnerService) {
   	$scope.pageNumber = 0;
   	$scope.totalPage = 0;
   	$scope.pageSize = 10;
   	$scope.sizeText = '10';
   	$scope.sortBy = "dateModified";
   	$scope.uniqueId = $stateParams.uniqueId;
   	$scope.keywords = $stateParams.keywords;
   	$scope.category = '-category';
	$scope.location = $location.url();
	$scope.searchParams = {
		type: 'product',
		layout: 'list',
		location: $scope.location,
		category: $scope.category,
		categoryId: $scope.uniqueId,
		keywords: $scope.keywords
	};
	$scope.sellerFacets = null;
   	$scope.pages = [];
   	$scope.facets = {};
   	
   	handleSearch();
   	
   	$scope.filterResults = function() {
   		$scope.pageNumber = 0;
   		handleSearch();
   	}
   	
   	$scope.updatePageSize = function(pageSize, sizeText) {
		$scope.pageSize = pageSize;
		$scope.sizeText = sizeText;
		handleSearch();
	}
   	
   	$scope.prevPage = function() {
   		$scope.pageNumber = $scope.pageNumber - 1;
   		handleSearch();
   	};
   	
   	$scope.moveTo = function(page) {
   		$scope.pageNumber = page - 1;
   		handleSearch();
   	};
   	
   	$scope.nextPage = function() {
   		$scope.pageNumber = $scope.pageNumber + 1;
   		handleSearch();
   	};
   	
   	$scope.addToCart = function(uniqueId) {
   		if (angular.isUndefined($scope.currentUser) || angular.equals($scope.currentUser, null)) {
   			var modalInstance = $modal.open({
   				animation : $scope.animationsEnabled,
   				templateUrl : 'assets/js/app/account/login-register-modal.component.html',
   				controller : 'LoginRegisterCtrl',
   				size : 'md',
   				scope: $scope
   			});
   			
   			modalInstance.result.then(function() {
   				GrainCartService.addToCart(uniqueId)
		   			.then(function(response) {
		   				$rootScope.$broadcast(GRAINCART_EVENTS.itemAdded);
		   			});
   				$log.info('Modal closed at: ' + new Date());
   			});
   		} else {
   			GrainCartService.addToCart(uniqueId)
	   			.then(function(response) {
	   				$rootScope.$broadcast(GRAINCART_EVENTS.itemAdded);
	   			});
   		}
   	};

	$scope.addToConsumerCart = function(product) {
		if (angular.isUndefined($scope.currentUser) || angular.equals($scope.currentUser, null)) {
   			var modalInstance = $modal.open({
   				animation : $scope.animationsEnabled,
   				templateUrl : 'assets/js/app/account/login-register-modal.component.html',
   				controller : 'LoginRegisterCtrl',
   				size : 'md',
   				scope: $scope
   			});
   			
   			modalInstance.result.then(function() {
   				GrainCartService.addToConsumerCart(product)
		   			.then(function(response) {
		   				$scope.sendMessageAndEvent(GRAINCART_EVENTS.itemAdded, 'success', 'Your Item has been added to the grain cart.')
		   			});
   				$log.info('Modal closed at: ' + new Date());
   			});
   		} else {
			GrainCartService.addToConsumerCart(product)
				.then(function(response) {
					$scope.sendMessageAndEvent(GRAINCART_EVENTS.itemAdded, 'success', 'Your Item has been added to the grain cart.')
				});
   		}
	};
	
   	$scope.addToWishlist = function(uniqueId) {
   		if (angular.isUndefined($scope.currentUser) || angular.equals($scope.currentUser, null)) {
   			var modalInstance = $modal.open({
   				animation : $scope.animationsEnabled,
   				templateUrl : 'assets/js/app/account/login-register-modal.component.html',
   				controller : 'LoginRegisterCtrl',
   				size : 'md',
   				scope: $scope
   			});
   			
   			modalInstance.result.then(function() {
   				WishlistService.addProductToWishlist(uniqueId)
	   	   			.then(function(response) {
	   	   			$scope.sendMessageAndEvent(WISHLIST_EVENTS.itemAdded, 'success', 'Your Item has been added to the wishlist.')
	   	   			});
   				$log.info('Modal closed at: ' + new Date());
   			});
   		} else {
   	   		WishlistService.addProductToWishlist(uniqueId)
   	   			.then(function(response) {
   	   				$scope.sendMessageAndEvent(WISHLIST_EVENTS.itemAdded, 'success', 'Your Item has been added to the wishlist.')
   	   			});
   		}
   	};
   	
	$scope.sendMessageAndEvent = function(broadcastEvent, statusClass, message) {
		$rootScope.$broadcast(broadcastEvent);
		ngToast.create({ className: statusClass, content: message });
	};
	
   	$scope.removeFromCart = function(uniqueId) {
   		GrainCartService.removeFromCart(uniqueId)
   			.then(function(response) {
   				$rootScope.$broadcast(GRAINCART_EVENTS.itemRemoved);
   			});
   	};
   	
   	function handleSearch() {
   		var cnsmrPrdt = false;
		/*if (!$scope.currentUser || $scope.isAuthorized('ROLE_CUSTOMER')) {
			cnsmrPrdt = true;
		}
		*/
   	   	var params = { 
   			uniqueId : $stateParams.uniqueId, 
   			keyword : $stateParams.keywords, 
   			cnsmrPrdt : cnsmrPrdt, 
   			page : $scope.pageNumber, 
   			size : $scope.pageSize, 
   			sort : $scope.sortBy 
   		};
   	   	
   	   	if ($scope.facets) {
   	   		params['facets'] = $scope.facets;
   	   	}
   	   	
   	 usSpinnerService.spin('prak-spinner');
   	   	SearchService.search('search/', params)
			.then(function(response) {
				$timeout(function() {
					handleSearchResult(response);
					usSpinnerService.stop('prak-spinner');
				});
		});
		
   	}
   	
   	function handleSearchResult(response) {
   		$scope.filters = response.data.filters || {};
   		$scope.products = response.data.results || {};
   		angular.extend($scope, SearchService.paginate(response.data.resultSize, response.data.results, $scope.pageSize));
   	}
   	
}]);

ProductSearchModule.controller("ProductSearchGridCtrl", ['$rootScope', '$scope', '$http', '$location', '$stateParams', '$timeout', '$modal', 'GrainCartService', 'GRAINCART_EVENTS', 'WishlistService', 'WISHLIST_EVENTS', 'ngToast', 'SearchService', 'UtilityService', 'usSpinnerService', 
        function ($rootScope, $scope, $http, $location, $stateParams, $timeout, $modal, GrainCartService, GRAINCART_EVENTS, WishlistService, WISHLIST_EVENTS, ngToast, SearchService, UtilityService, usSpinnerService) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.pageSize = 12;
	$scope.sizeText = '12';
	$scope.sortBy = "dateModified";
	$scope.uniqueId = $stateParams.uniqueId;
	$scope.keywords = $stateParams.keywords;
	$scope.category = '-category';
	$scope.location = $location.url();
	$scope.searchParams = {
		type: 'product',
		layout: 'grid',
		location: $scope.location,
		category: $scope.category,
		categoryId: $scope.uniqueId,
		keywords: $scope.keywords
	};
	$scope.pages = [];
   	$scope.facets = {};
   	
	handleSearch();
	
	$scope.filterResults = function() {
		$scope.pageNumber = 0;
   		handleSearch();
   	}
	
	$scope.updatePageSize = function(pageSize, sizeText) {
		$scope.pageSize = pageSize;
		$scope.sizeText = sizeText;
		handleSearch();
	}
	
	$scope.prevPage = function() {
		$scope.pageNumber = $scope.pageNumber - 1;
		handleSearch();
	};
	
	$scope.moveTo = function(page) {
		$scope.pageNumber = page - 1;
		handleSearch();
	};
	
	$scope.nextPage = function() {
		$scope.pageNumber = $scope.pageNumber + 1;
		handleSearch();
	};

   	$scope.subtractQty = function(product) {
   		product.quantity = product.quantity - 1;
   	};
   	
   	$scope.addQty = function(product) {
   		product.quantity = product.quantity + 1;
   	};
	
   	$scope.isANewProduct = function(product) {
   		var createdOn = parseDate(product.createdOn);
   		var currentDate = new Date();
   		var numberOfDays = Math.round((currentDate - createdOn) / (1000*60*60*24));
   		if (numberOfDays <= 5) {
   			return true;
   		}
   		return false;
   	};
	
   	$scope.makeProductDetailUrl = function(uniqueId) {
   		if ($scope.contains($scope.location, $scope.category)) {
   			return 'uniqueId=' + uniqueId + '&layout=grid-category&categoryId=' + $scope.uniqueId;
   		} else {
   			return 'uniqueId=' + uniqueId + '&layout=grid&keywords=' + $scope.keywords;
   		}
   	}
   	
   	function parseDate(str) {
   	    var mdy = str.split('/');
   	    return new Date(mdy[2], mdy[1], mdy[0]-1);
   	}
   	
	$scope.addToCart = function(uniqueId) {
		if (angular.isUndefined($scope.currentUser) || angular.equals($scope.currentUser, null)) {
   			var modalInstance = $modal.open({
   				animation : $scope.animationsEnabled,
   				templateUrl : 'assets/js/app/account/login-register-modal.component.html',
   				controller : 'LoginRegisterCtrl',
   				size : 'md',
   				scope: $scope
   			});
   			
   			modalInstance.result.then(function() {
   				GrainCartService.addToConsumerCart(product)
		   			.then(function(response) {
		   				$scope.sendMessageAndEvent(GRAINCART_EVENTS.itemAdded, 'success', 'Your Item has been added to the grain cart.')
		   			});
   				$log.info('Modal closed at: ' + new Date());
   			});
   		} else {
			GrainCartService.addToCart(uniqueId)
				.then(function(response) {
					$rootScope.$broadcast(GRAINCART_EVENTS.itemAdded);
				});
   		}
	};
	
	$scope.addToConsumerCart = function(product) {
		if (angular.isUndefined($scope.currentUser) || angular.equals($scope.currentUser, null)) {
   			var modalInstance = $modal.open({
   				animation : $scope.animationsEnabled,
   				templateUrl : 'assets/js/app/account/login-register-modal.component.html',
   				controller : 'LoginRegisterCtrl',
   				size : 'md',
   				scope: $scope
   			});
   			
   			modalInstance.result.then(function() {
   				GrainCartService.addToConsumerCart(product)
		   			.then(function(response) {
		   				$scope.sendMessageAndEvent(GRAINCART_EVENTS.itemAdded, 'success', 'Your Item has been added to the grain cart.')
		   			});
   				$log.info('Modal closed at: ' + new Date());
   			});
   		} else {
			GrainCartService.addToConsumerCart(product)
				.then(function(response) {
					$scope.sendMessageAndEvent(GRAINCART_EVENTS.itemAdded, 'success', 'Your Item has been added to the grain cart.')
				});
   		}
	};
	
	$scope.addToWishlist = function(uniqueId) {
		if (angular.isUndefined($scope.currentUser) || angular.equals($scope.currentUser, null)) {
   			var modalInstance = $modal.open({
   				animation : $scope.animationsEnabled,
   				templateUrl : 'assets/js/app/account/login-register-modal.component.html',
   				controller : 'LoginRegisterCtrl',
   				size : 'md',
   				scope: $scope
   			});
   			
   			modalInstance.result.then(function() {
   				WishlistService.addProductToWishlist(uniqueId)
	   	   			.then(function(response) {
	   	   			$scope.sendMessageAndEvent(WISHLIST_EVENTS.itemAdded, 'success', 'Your Item has been added to the wishlist.')
	   	   			});
   				$log.info('Modal closed at: ' + new Date());
   			});
   		} else {
			WishlistService.addProductToWishlist(uniqueId)
				.then(function(response) {
					$scope.sendMessageAndEvent(WISHLIST_EVENTS.itemAdded, 'success', 'Your Item has been added to the wishlist.')
				});
   		}
	};
	
	$scope.sendMessageAndEvent = function(broadcastEvent, statusClass, message) {
		$rootScope.$broadcast(broadcastEvent);
		ngToast.create({ className: statusClass, content: message });
	};
	
	$scope.removeFromCart = function(uniqueId) {
		GrainCartService.removeFromCart(uniqueId)
			.then(function(response) {
				$rootScope.$broadcast(GRAINCART_EVENTS.itemRemoved);
			});
	};

	function handleSearch() {
		var cnsmrPrdt = false;
		/*
		if (!$scope.currentUser || $scope.isAuthorized('ROLE_CUSTOMER')) {
			cnsmrPrdt = true;
		}
		*/
		var params = { uniqueId : $stateParams.uniqueId, keyword : $scope.keywords, cnsmrPrdt : cnsmrPrdt, page : $scope.pageNumber, size : $scope.pageSize, sort : $scope.sortBy, filters: $scope.facets };
		
		if ($scope.facets) {
   	   		params['facets'] = $scope.facets;
   	   	}
		
		usSpinnerService.spin('prak-spinner');
		SearchService.search('search/', params)
	   		.then(function(response) {
	   			$timeout(function() {
	   				handleSearchResult(response);
	   				usSpinnerService.stop('prak-spinner');
	   			});
	   	});
	   	
	};
	
	function handleSearchResult(response) {
		$scope.filters = response.data.filters || {};
		$scope.matrix = convertToMatrix(response.data.results, 3);
		angular.extend($scope, SearchService.paginate(response.data.resultSize, response.data.results, $scope.pageSize));
	}
	
	function convertToMatrix(products, grids) {
		var matrix = [];
		for (var index = 0; index < products.length; index++) {
			if (index % grids === 0) 
				matrix[Math.floor(index / grids)] = [];
			matrix[Math.floor(index / grids)][index % grids] = products[index];
		}
		return matrix;
	};
	
}]);

ProductSearchModule.controller("SearchProductDetailCtrl", ['$rootScope', '$scope', '$http', '$stateParams', '$modal', 'GrainCartService', 'GRAINCART_EVENTS', 'WishlistService', 'WISHLIST_EVENTS', function ($rootScope, $scope, $http, $stateParams, $modal, GrainCartService, GRAINCART_EVENTS, WishlistService, WISHLIST_EVENTS) {
	$scope.review = {};
	$scope.layout = $stateParams.layout;
	$scope.categoryId = $stateParams.categoryId;
	$scope.keywords = $stateParams.keywords;
	$scope.interval = 5000;
	$scope.noWrapSlides = true;
	$scope.active = 0;
	
	$scope.getProductDetail = function () {
		$http.get('search/detail?uniqueId=' + $stateParams.uniqueId)
			.then(function(response) {
				$scope.product = response.data.product;
				$scope.relatedProducts = response.data.relatedProducts;
			});
	};
	
	$scope.getProductDetail();
	
	$scope.addToCart = function(uniqueId) {
		if (angular.isUndefined($scope.currentUser) || angular.equals($scope.currentUser, null)) {
   			var modalInstance = $modal.open({
   				animation : $scope.animationsEnabled,
   				templateUrl : 'assets/js/app/account/login-register-modal.component.html',
   				controller : 'LoginRegisterCtrl',
   				size : 'md',
   				scope: $scope
   			});
   			
   			modalInstance.result.then(function() {
   				GrainCartService.addToCart(uniqueId)
		   			.then(function(response) {
		   				$scope.sendMessageAndEvent(GRAINCART_EVENTS.itemAdded, 'success', 'Your Item has been added to the grain cart.')
		   			});
   				$log.info('Modal closed at: ' + new Date());
   			});
   		} else {
			GrainCartService.addToCart(uniqueId)
				.then(function(response) {
					$rootScope.$broadcast(GRAINCART_EVENTS.itemAdded);
				});
   		}
	};
	
	$scope.addToWishlist = function(uniqueId) {
		if (angular.isUndefined($scope.currentUser) || angular.equals($scope.currentUser, null)) {
   			var modalInstance = $modal.open({
   				animation : $scope.animationsEnabled,
   				templateUrl : 'assets/js/app/account/login-register-modal.component.html',
   				controller : 'LoginRegisterCtrl',
   				size : 'md',
   				scope: $scope
   			});
   			
   			modalInstance.result.then(function() {
   				WishlistService.addProductToWishlist(uniqueId)
		   			.then(function(response) {
		   				$scope.sendMessageAndEvent(GRAINCART_EVENTS.itemAdded, 'success', 'Your Item has been added to the grain cart.')
		   			});
   				$log.info('Modal closed at: ' + new Date());
   			});
   		} else {
			WishlistService.addProductToWishlist(uniqueId)
				.then(function(response) {
					$rootScope.$broadcast(WISHLIST_EVENTS.itemAdded);
				});
   		}
	};
	
	$scope.removeFromCart = function(uniqueId) {
		GrainCartService.removeFromCart(uniqueId)
			.then(function(response) {
				$rootScope.$broadcast(GRAINCART_EVENTS.itemRemoved);
			});
	};
	
	$scope.makeBackUrl = function() {
		
	}
	
	$scope.setReview = function(rating) {
		$scope.review.rating = rating;
	}
	
	$scope.submitReview = function(isValid) {
		if (isValid) {
			$scope.review['uniqueId'] = $scope.product.uniqueId;
			$http.post('products/review', $scope.review)
				.then(function(response) {
					$scope.review = null;
					$scope.getProductDetail();
				});
		}
	}
}]);