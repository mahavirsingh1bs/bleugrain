'use strict';

var OthersModule = angular.module("OthersModule", []);

OthersModule.controller("ContactUsCtrl", ['$scope', '$http', 'ngToast', '$log', function ($scope, $http, ngToast, $log) {
	$scope.contactUs = {};
	
	$scope.submit = function(isValid) {
		if (isValid) {
			$http.post('others/contactUs', $scope.contactUs)
				.then(function(response) {
					if (response.data.code === 'success') {
						// ngToast.create({ className: 'success', content: response.data.message });
						$scope.contactUs = {};
					} else if (response.data.code === 'error') {
						// ngToast.create({ className: 'danger', content: response.data.message });
					}
				});
		}
	}
}]);

OthersModule.controller("FeedbackCtrl", ['$scope', '$http', '$location', '$log', function ($scope, $http, $location, $log) {
	$scope.feedback = { }
	
	$scope.submit = function(feedback) {
		$http.post('others/feedback', feedback).then(function() {
			$log.info('We have received your feedback. Thank you very much for valuable feedback.');
			$location.path('/dashboard');
		});
	};
}]);