'use strict';

var BrokerModule = angular.module("BrokerModule", []);

BrokerModule.controller("BrokerListCtrl", ['$scope', '$http', '$modal', '$log', 'HttpService', 'ngToast', function ($scope, $http, $modal, $log, HttpService, ngToast) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.keyword = null;
	$scope.pageSize = 10;
	$scope.history = false;
	$scope.brokers = null; 
	$scope.pages = [];
	$scope.sortOrders = {
		"fields": ["firstName,lastName", "phoneNo", "email", "createdBy.firstName,createdBy.lastName", "dateModified", "modifiedBy.firstName,modifiedBy.lastName", "dateModified"],
		"directions": ["asc", "asc", "asc", "asc", "asc" ]
	};
	
	$scope.sortFields = $scope.sortOrders['fields'][0];
	$scope.sortDirection = $scope.sortOrders['directions'][0];
	$scope.url = 'brokers/all';
	
	$scope.getBrokers = function () {
		var params = { 
			keyword: $scope.keyword,
			sortFields: $scope.sortFields,
			direction: $scope.direction,
			page: $scope.pageNumber, 
			pageSize: $scope.pageSize,
			history: $scope.history, 
		};
		
		$scope.readData($scope.url, params, 'brokers', $scope);
	};
	
	$scope.getBrokers();

	$scope.addBroker = function() {
		$scope.addEntityInternal('assets/js/app/brokers/add-broker.component.html', 'BrokerAddCtrl', 'brokers/all', 'brokers', $scope);
	};
	
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			broker: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('assets/js/app/brokers/detail-broker.component.html', 'BrokerDetailCtrl', 'brokers/all', 'brokers', $scope, resolveObj);
	};
	
	$scope.editDetail = function(uniqueId) {
		var resolveObj = { 
			broker: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.editEntityInternal('assets/js/app/brokers/edit-broker.component.html', 'BrokerEditCtrl', 'brokers/all', 'brokers', $scope, resolveObj);
	};
	
	$scope.remove = function(uniqueId) {
		var resolveObj = { 
			broker: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.removeEntityInternal('delete-broker.component.html', 'DeleteBrokerCtrl', 'brokers/all', 'brokers', $scope, resolveObj);
	};

	$scope.prevPage = function() {
		$scope.prevPageInternal('brokers/all', 'brokers', $scope);
	};
	
	$scope.moveTo = function(page) {
		$scope.moveToInternal('brokers/all', 'brokers', $scope, page);
	};
	
	$scope.nextPage = function() {
		$scope.nextPageInternal('brokers/all', 'brokers', $scope);
	};

	$scope.searchByKeyword = function() {
		$scope.searchByKeywordInternal('brokers/all', 'brokers', $scope);
	};
	
	$scope.sortBy = function(field, direction) {
		$scope.sortByInternal('brokers/all', 'brokers', $scope, field, direction);
	};
	
}]);

BrokerModule.controller("BrokerAddCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'CountryService', 'StateService', 'usSpinnerService', 'FormUtils', 
        function ($scope, $http, $location, $modalInstance, ngToast, CountryService, StateService, usSpinnerService, FormUtils) {
	CountryService.findCountries('countries/all')
		.then(function(countries) {
			$scope.countries = countries; 
		});
	
	StateService.findStates('states/all')
		.then(function(states) {
			$scope.states = states; 
		});
	
	$scope.ok = function(isValid) {
		if (isValid) {
			var formData = new FormData();
			
			formData.append('broker', new Blob([JSON.stringify($scope.broker)], { type: "application/json" }));
			formData.append('brokerImage', $scope.brokerImage);
			$scope.postCreateEntity('brokers/new', formData, $scope.brokerFrm, $modalInstance);
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

BrokerModule.controller("BrokerDetailCtrl", ['$scope', '$http', '$modalInstance', 'broker', function ($scope, $http, $modalInstance, broker) {
	$http.get('brokers/data/' + broker.uniqueId)
		.then(function(response) {
			$scope.broker = response.data;
		});
	
    $scope.close = function() {
		$modalInstance.close();
    };
}]);

BrokerModule.controller("BrokerEditCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', 'CountryService', 'StateService', 'broker', 
        function ($scope, $http, $modalInstance, ngToast, CountryService, StateService, broker) {
	
	CountryService.findCountries('countries/all')
		.then(function(countries) {
			$scope.countries = countries; 
		});
	
	StateService.findStates('states/all')
		.then(function(states) {
			$scope.states = states; 
		});
	
	$http.get('brokers/data/' + broker.uniqueId)
		.then(function(response) {
			$scope.broker = response.data;
		});
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$scope.postUpdateEntity('brokers/update/', $scope.broker, $scope.brokerFrm, $modalInstance);
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

BrokerModule.controller("DeleteBrokerCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'broker', function ($scope, $http, $location, $modalInstance, ngToast, broker) {
	$scope.ok = function() {
		$scope.deleteEntity('brokers/delete/' + broker.uniqueId, $modalInstance);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);