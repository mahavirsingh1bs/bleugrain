'use strict';

var CoreModule = angular.module("CoreModule", []);

CoreModule.controller('ApplicationCtrl', 
		['$rootScope', '$scope', '$http', '$location', 'Session', '$log', 'ngToast', '$timeout', 'USER_ROLES', 'AuthService', 'AUTH_EVENTS', 'HttpService', '$modal', 'GrainCartService', 'GRAINCART_EVENTS', 'WishlistService', 'WISHLIST_EVENTS', 'SettingsService', 'UtilityService', 'Idle', 'Keepalive', 'usSpinnerService', 'FormUtils',  
		 	function($rootScope, $scope, $http, $location, Session, $log, ngToast, $timeout, USER_ROLES, AuthService, AUTH_EVENTS, HttpService, $modal, GrainCartService, GRAINCART_EVENTS, WishlistService, WISHLIST_EVENTS, SettingsService, UtilityService, Idle, Keepalive, usSpinnerService, FormUtils) {
		
	Session.getCurrentUser().then(function(currentUser) {
		Idle.watch();
		$scope.currentUser = currentUser;
	}, function() {
		$location.path('/');
	});
	
	$scope.grainCart = {};
	$scope.wishlist = {};
	$scope.detail = { };
	$scope.userRoles = USER_ROLES;
	$scope.isLoginPage = false;
	$scope.isAuthorized = AuthService.isAuthorized;
	$scope.andIsAuthorized = AuthService.andIsAuthorized;
	$scope.orIsAuthorized = AuthService.orIsAuthorized;
	$scope.entities = null;

	$scope.isBusinessCategory = function(businessCategory) {
		if (!UtilityService.isUndefinedOrNull($scope.currentUser)) {
			if ($scope.currentUser.businessCategory === businessCategory) {
				return true;
			}
		}
		return false;
	};
	
	$scope.socialSites = {
		'FACEBOOK': 'fa-facebook',
		'TWITTER': 'fa-twitter',
		'LINKEDIN': 'fa-linkedin',
		'INSTAGRAM': 'fa-instagram'
	};
	
	$scope.socialCodes = {
		'FACEBOOK': 'fb',
		'TWITTER': 'tw',
		'LINKEDIN': 'li',
		'INSTAGRAM': 'ig'	
	};
	
	$scope.searchCategoryURL = function() {
		if (isAuthorized('ROLE_FARMER')) {
			return 'grid-category/demand'
		}
		return 'grid-category'
	};
	
	$scope.searchURL = function() {
		if (isAuthorized('ROLE_FARMER')) {
			return 'grid/demand'
		}
		return 'grid'
	};
	
	/*
	$http.get('http://ipinfo.io')
		.then(function(response) {
			$scope.countryCode = (response.data && response.data.country) ? response.data.country : "";
		});
	*/

	$scope.contains = function(str, token) {
		if (str.indexOf(token) !== -1) {
			return true;
		}
		return false
	};
	
	$scope.makeDetailUrl = function(uniqueId, params) {
   		if ($scope.contains(params.location, params.category)) {
   			return '/#/search/' + params.type + '?uniqueId=' + uniqueId + '&layout=' + params.layout + '-category&categoryId=' + params.categoryId;
   		} else {
   			return '/#/search/' + params.type + '?uniqueId=' + uniqueId + '&layout=' + params.layout + '&keywords=' + params.keywords;
   		}
   	}
   	
	$scope.modifyCountrySettings = function(response) {
		if (angular.isObject($scope.currentUser)) {
			$scope.currentUser['defaultLocale'] = response.defaultLocale;
			$scope.currentUser['defaultLanguage'] = response.defaultLanguage;
			$scope.currentUser['defaultCountry'] = response.defaultCountry;
		}
	};
	
	SettingsService.findCountrySettings().then(function(response) {
		$scope.modifyCountrySettings(response);
	});
	
	GrainCartService.findGrainCart().then(function(response) {
		$scope.grainCart = response;
	});
	
	WishlistService.findWishlist().then(function(response) {
		$scope.wishlist = response;
	});
	
    angular.element(document).ready(function() {
      	Unify.init();
        Unify.initCounter();
        Unify.initParallaxBg();
        OwlCarousel.initOwlCarousel();
    });
	
    $scope.setCurrentUser = function(user) {
		$scope.currentUser = user;
	};

	$scope.editEntityInternal = function(tmptUrl, ctrl, url, entities, childScope, resolveObj) {
		var modalInstance = $modal.open({
			animation : $scope.animationsEnabled,
			templateUrl : tmptUrl,
			controller : ctrl,
			size : 'lg',
			scope : childScope,
			resolve : resolveObj
		});
		
		modalInstance.result.then(function() {
			$scope.readDataInternal(url, entities, childScope);
		}, function() {
			$log.info('Modal dismissed at: ' + new Date());
		});
	}
	
	$scope.addEntityInternal = function(tmptUrl, ctrl, url, entities, childScope, resolveObj) {
		var modalInstance = $modal.open({
			animation : $scope.animationsEnabled,
			templateUrl : tmptUrl,
			controller : ctrl,
			size : 'lg',
			scope: childScope,
			resolve: resolveObj,
		});
		
		modalInstance.result.then(function() {
			$scope.readDataInternal(url, entities, childScope);
		}, function() {
			$log.info('Modal dismissed at: ' + new Date());
		});
	};
	
	$scope.viewEntityInternal = function(tmptUrl, ctrl, url, entities, childScope, resolveObj) {
		var modalInstance = $modal.open({
			animation : $scope.animationsEnabled,
			templateUrl : tmptUrl,
			controller : ctrl,
			size : 'lg',
			scope : childScope,
			resolve : resolveObj
		});
		
		modalInstance.result.then(function() {
			$log.info('Modal closed at: ' + new Date());
		});
	};
	
	$scope.removeEntityInternal = function(tmptUrl, ctrl, url, entities, childScope, resolveObj) {
		var modalInstance = $modal.open({
			animation : $scope.animationsEnabled,
			templateUrl : tmptUrl,
			controller : ctrl,
			size : 'lg',
			scope: childScope,
			resolve : resolveObj
		});
		
		modalInstance.result.then(function() {
			$scope.readDataInternal(url, entities, childScope);
		}, function() {
			$log.info('Modal dismissed at: ' + new Date());
		});
	};
	
	$scope.removeEntityInternal = function(tmptUrl, ctrl, url, entities, childScope, resolveObj) {
		var modalInstance = $modal.open({
			animation : $scope.animationsEnabled,
			templateUrl : tmptUrl,
			controller : ctrl,
			size : 'lg',
			scope: childScope,
			resolve: resolveObj
		});
		
		modalInstance.result.then(function() {
			$scope.readDataInternal(url, entities, childScope);
		}, function() {
			$log.info('Modal dismissed at: ' + new Date());
		});
	};
	
	$scope.prevPageInternal = function(url, entities, childScope) {
		if (childScope.pageNumber === 0) {
			return false;
		}
		childScope.pageNumber = childScope.pageNumber - 1;
		$scope.readDataInternal(url, entities, childScope);
	}
	
	$scope.moveToInternal = function(url, entities, childScope, page) {
		childScope.pageNumber = page - 1;
		$scope.readDataInternal(url, entities, childScope);
	}
	
	$scope.nextPageInternal = function(url, entities, childScope) {
		if ((childScope.pageNumber + 1) === childScope.totalPage) {
			return false;
		}
		
		childScope.pageNumber = childScope.pageNumber + 1;
		$scope.readDataInternal(url, entities, childScope);
	};
	
	$scope.searchByKeywordInternal = function(url, entities, childScope) {
		childScope.pageNumber = 0;
		$scope.readDataInternal(url, entities, childScope);
	}
	
	$scope.sortByInternal = function(url, entities, childScope, field, direction) {
		childScope.sortOrders.fields.forEach(function(item, index) {
			if (item === field) {
				childScope.sortOrders.directions[index] = direction;
				childScope.sortFields = field;
				childScope.sortDirection = direction;
			} else {
				childScope.sortOrders.directions[index] = 'asc';
			}
		});
		
		childScope.pageNumber = 0;
		$scope.readDataInternal(url, entities, childScope);
	}
	
	$scope.readData = function(url, params, entities, scope) {
		usSpinnerService.spin('prak-spinner');
		HttpService.readData(url, params)
			.then(function(response) {
				$scope.handleResponse(response, entities, scope);
				usSpinnerService.stop('prak-spinner');
			}, function() {
				console.log('error');
			});
	};
	
	$scope.readDataInternal = function (url, entities, childScope) {
		
		var params = { 
			keyword: childScope.keyword,
			sortFields: childScope.sortFields,
			direction: childScope.direction,
			page: childScope.pageNumber, 
			pageSize: childScope.pageSize,
			history: childScope.history, 
		};
		
		if (!UtilityService.isUndefinedOrNull(childScope.category)) {
			params.categoryId = childScope.category.uniqueId;
		}
		
		if (!UtilityService.isUndefinedOrNull(childScope.consumerProducts)) {
			params.consumerProducts = childScope.consumerProducts;
		}
		
		if (!UtilityService.isUndefinedOrNull(childScope.mainMenuitem)) {
			params.mainMenuitemId = childScope.mainMenuitem.uniqueId;
		} else {
			params.mainMenuitems = childScope.showMainMenuitems;
		}
		
		if (!UtilityService.isUndefinedOrNull(childScope.mainCategory)) {
			params.mainCategoryId = childScope.mainCategory.uniqueId;
		} else {
			params.mainCategories = childScope.showMainCategories;
		} 
		
		if (!UtilityService.isUndefinedOrNull(childScope.userNo)) {
			params.userNo = childScope.userNo;
		} else if (!UtilityService.isUndefinedOrNull(childScope.companyId)) {
			params.companyId = childScope.companyId;
		}
		
		usSpinnerService.spin('prak-spinner');
		HttpService.readData(url, params)
			.then(function(response) {
				$scope.handleResponse(response, entities, childScope);
				usSpinnerService.stop('prak-spinner');
			}, function() {
				console.log('error');
			});
	}
	
	$scope.handleResponse = function(response, entities, childScope) {
		setTimeout(function() {
			$scope.$apply(function() {
				childScope[entities] = response.data;
				childScope.totalPage = Math.ceil(response.recordsTotal / childScope.pageSize);
				childScope.pages = [];
				for (var i = 0; i < childScope.totalPage; i++) {
					childScope.pages.push(i + 1);
				}
			});
		}, 1);
	}

    $scope.postCreateEntity = function(url, formData, form, $modalInstance) {
    	$http.post(url, formData, {
				transformRequest: angular.identity,
				headers: { 'Content-Type': undefined }
			}).then(
				function(response) {
					if (response.data.code === 'success') {
						ngToast.create({ className: 'success', content: response.data.message });
						$modalInstance.close();
					} else if (response.data.code === 'failure') {
						ngToast.create({ className: 'danger', content: response.data.message });
					}
					usSpinnerService.stop('prak-spinner');
				}, function(response) {
					FormUtils.resetFormErrors(form);
					FormUtils.handleFormErrors(form, response.data.fieldErrors);
					usSpinnerService.stop('prak-spinner');
				});
    	usSpinnerService.spin('prak-spinner');
    }
    
    $scope.postUpdateEntity = function(url, entity, form, $modalInstance) {
    	$http.post(url, entity)
			.then(function(response) {
				if (response.data.code === 'success') {
					ngToast.create({ className: 'success', content: response.data.message });
					$modalInstance.close();
				} else {
					ngToast.create({ className: 'danger', content: response.data.message });
				}
				usSpinnerService.stop('prak-spinner');
			}, function(response) {
				FormUtils.resetFormErrors(form);
				FormUtils.handleFormErrors(form, response.data.fieldErrors);
				usSpinnerService.stop('prak-spinner');
			});
    	usSpinnerService.spin('prak-spinner');
    }
    
    $scope.deleteEntity = function(url, $modalInstance) {
    	$http.delete(url)
			.then(function(response) {
				if (response.data.code === 'success') {
					ngToast.create({ className: 'success', content: response.data.message });
				} else if (response.data.code === 'error') {
					ngToast.create({ className: 'danger', content: response.data.message });
				}
				usSpinnerService.stop('prak-spinner');
				$modalInstance.close();
			});
    	usSpinnerService.spin('prak-spinner');
    }
    
	$scope.getDirection = function(direction) {
		return direction === 'asc' ? "desc" : "asc";
	}

	$scope.socialSite = function (socialSite) {
		return $scope.socialSites[socialSite];
	};
	
	$scope.socialCode = function (socialSite) {
		return $scope.socialCodes[socialSite];
	};
	
	$scope.initAdvancedBooking = function () {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/booking/advanced-booking.component.html',
  			controller : 'AdvancedBookingCtrl',
  			size : 'md',
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }
	
	$scope.changePicture = function() {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/shared/profile-picture-crop.component.html',
  			controller : 'ChangePictureCtrl',
  			size : 'lg',
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			if (defaultImage !== null) {
  				$scope.currentUser.defaultImage = defaultImage;
  			}
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
	};
	
	$rootScope.$on(AUTH_EVENTS.notAuthenticated, function(event) {
		console.log('user is not authenticated');
	});
	
	$rootScope.$on(AUTH_EVENTS.loginFailed, function(event) {
		console.log('login got failed for the user');
	});
	
	$rootScope.$on(AUTH_EVENTS.loginSuccess, function(event) {
		console.log('user has been logged in');
		$scope.currentUser = Session.getCurrentUser();
		Idle.watch();
		
		SettingsService.findCountrySettings().then(function(response) {
			$scope.modifyCountrySettings(response);
		});
		
		GrainCartService.findGrainCart().then(function(response) {
			$scope.grainCart = response;
		});
		
		WishlistService.findWishlist().then(function(response) {
			$scope.wishlist = response;
		});
	});
	
	$rootScope.$on(AUTH_EVENTS.logoutSuccess, function(event) {
		console.log('user has been logged out');
		Idle.unwatch();
		$scope.currentUser = null;
		$scope.grainCart = null;
		$scope.wishlist = null;
	});
	
	$rootScope.$on(GRAINCART_EVENTS.itemAdded, function(event) {
		GrainCartService.findGrainCart().then(function(response) {
			$scope.grainCart = response;
		});
	});
	
	$rootScope.$on(GRAINCART_EVENTS.itemRemoved, function(event) {
		GrainCartService.findGrainCart().then(function(response) {
			$scope.grainCart = response;
		});
	});
	
	$rootScope.$on(GRAINCART_EVENTS.grainCartCleared, function(event) {
		GrainCartService.findGrainCart().then(function(response) {
			$scope.grainCart = response;
		});
	});
	
	$rootScope.$on(WISHLIST_EVENTS.itemAdded, function(event) {
		WishlistService.findWishlist().then(function(response) {
			$scope.wishlist = response;
		});
	});
	
	$rootScope.$on(WISHLIST_EVENTS.itemRemoved, function(event) {
		WishlistService.findWishlist().then(function(response) {
			$scope.wishlist = response;
		});
	});

	$scope.sendLoginSuccessAndWatch = function($modalInstance) {
		$rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
		$modalInstance.close();
	}
	
	$scope.createSessionAndWatch = function() {
		AuthService.createSession();
		$rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
		$location.path('/dashboard');
	}
	
	function closeModals() {
		if ($scope.warning) {
			$scope.warning.close();
			$scope.warning = null;
		}

		if ($scope.timedout) {
			$scope.timedout.close();
			$scope.timedout = null;
		}
	}

	$scope.$on('IdleStart', function() {
		closeModals();
		
		$rootScope.width = 100;
		
		$scope.warning = $modal.open({
			templateUrl : 'assets/js/app/session/session-timeout-warning.component.html',
			windowClass : 'modal-danger'
		});
		
	});

	$scope.$on('IdleEnd', function() {
		closeModals();
	});

	$scope.$on('IdleTimeout', function() {
		closeModals();
		
			$scope.timedout = $modal.open({
				templateUrl : 'assets/js/app/session/session-timeout.component.html',
				windowClass : 'modal-danger'
			});
			
			$timeout(function () { 
				
				AuthService.logout().then(function() {
					$scope.timedout.close();
					$rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
					$location.path("/login");
				});
	
			}, 5000);
		
	});
	
}]);

CoreModule.controller("LogoutCtrl", ['$rootScope', '$scope', '$location', 'AUTH_EVENTS', 'AuthService', 'Idle', function ($rootScope, $scope, $location, AUTH_EVENTS, AuthService, Idle) {
	$scope.logout = function() {
		AuthService.logout().then(function() {
			$rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
			$location.path("/");
		});
	};
}]);

CoreModule.controller("SessionWarningCtrl", ['Keepalive', '$modalInstance', function (Keepalive, $modalInstance) {
	$scope.width = "60";
	
	$scope.cancel = function() {
		Keepalive.ping();
	};
}]);

CoreModule.controller('LanguageController', ['$window', '$scope','$translate','$location', function($window, $scope, $translate, $location) {
	var lang = $window.navigator.language || $window.navigator.userLanguage;
	$scope.userLocale = lang
	$scope.userLanguage = 'English';
	$scope.changeLanguage = function (locale, language) {
		$scope.userLocale = locale;
		$scope.userLanguage = language;
		$translate.use(locale);
		$location.search('lang', locale);
	};
}]);

CoreModule.controller("ChangePictureCtrl", ['$scope', '$http', '$location', '$modalInstance', 'Upload', 'ngToast', 'Cropper', '$timeout', function ($scope, $http, $location, $modalInstance, Upload, ngToast, Cropper, $timeout) {
	  var file, data;
	
	  /**
	   * Method is called every time file input's value changes.
	   * Because of Angular has not ng-change for file inputs a hack is needed -
	   * call `angular.element(this).scope().onFile(this.files[0])`
	   * when input's event is fired.
	   */
	  $scope.onFile = function(blob) {
	    Cropper.encode((file = blob)).then(function(dataUrl) {
	      $scope.dataUrl = dataUrl;
	      $timeout(showCropper);  // wait for $digest to set image's src
	    });
	  };
	
	  /**
	   * Croppers container object should be created in controller's scope
	   * for updates by directive via prototypal inheritance.
	   * Pass a full proxy name to the `ng-cropper-proxy` directive attribute to
	   * enable proxing.
	   */
	  $scope.cropper = {};
	  $scope.cropperProxy = 'cropper.first';
	
	  /**
	   * When there is a cropped image to show encode it to base64 string and
	   * use as a source for an image element.
	   */
	  $scope.preview = function() {
	    if (!file || !data) return;
	    Cropper.crop(file, data).then(Cropper.encode).then(function(dataUrl) {
	      ($scope.preview || ($scope.preview = {})).dataUrl = dataUrl;
	    });
	  };
	
	  /**
	   * Use cropper function proxy to call methods of the plugin.
	   * See https://github.com/fengyuanchen/cropper#methods
	   */
	  $scope.clear = function(degrees) {
	    if (!$scope.cropper.first) return;
	    $scope.cropper.first('clear');
	  };
	
	  $scope.scale = function(width) {
	    Cropper.crop(file, data)
	      .then(function(blob) {
	        return Cropper.scale(blob, {width: width});
	      })
	      .then(Cropper.encode).then(function(dataUrl) {
	        ($scope.preview || ($scope.preview = {})).dataUrl = dataUrl;
	      });
	  }
	
	  /**
	   * Object is used to pass options to initalize a cropper.
	   * More on options - https://github.com/fengyuanchen/cropper#options
	   */
	  $scope.options = {
	    maximize: true,
	    aspectRatio: 1 / 1,
	    crop: function(dataNew) {
	      data = dataNew;
	    }
	  };
	
	  /**
	   * Showing (initializing) and hiding (destroying) of a cropper are started by
	   * events. The scope of the `ng-cropper` directive is derived from the scope of
	   * the controller. When initializing the `ng-cropper` directive adds two handlers
	   * listening to events passed by `ng-cropper-show` & `ng-cropper-hide` attributes.
	   * To show or hide a cropper `$broadcast` a proper event.
	   */
	  $scope.showEvent = 'show';
	  $scope.hideEvent = 'hide';
	
	  function showCropper() { $scope.$broadcast($scope.showEvent); }
	  function hideCropper() { $scope.$broadcast($scope.hideEvent); }
	  
	  $scope.uploadPicture = function() {
		  var formData = new FormData();
		  formData.append('userId', new Blob([$scope.currentUser.userNo], { type: "application/json" }));
		  var dataUrl = $scope.preview.dataUrl.substring(22);
		  formData.append('userImage', $scope.preview.dataUrl);
			
			$http.post('profile/changePicture', formData, {
					transformRequest: angular.identity,
					headers: { 'Content-Type': undefined }
				}).then(
					function(response) {
						$modalInstance.close(response.data);
					}, function(response) {
						ngToast.create({ className: 'danger', content: response.data.message });
					});
	  }
			
	  $scope.ok = function() {
		  $modalInstance.close({});
	  };
  
	  $scope.close = function() {
		  $modalInstance.dismiss('cancel');
	  };
}]);

CoreModule.controller("AdvancedBookingCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', function ($scope, $http, $location, $modalInstance, ngToast) {
	
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
		});
	
	$scope.submit = function(isValid) {
		if (isValid) {
			$http.post('advancedBooking', $scope.advancedBooking)
				.then(function(response) {
					if (response.data.code === 'success') {
						ngToast.create({ className: 'success', content: response.data.message });
						$modalInstance.close();
					} else if (response.data.code === 'error') {
						ngToast.create({ className: 'danger', content: response.data.message });
					}
				});
		}
    };
	
	$scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
    
}]);

CoreModule.controller("MenuitemCtrl", ['$scope', '$http', '$location', 'UtilityService', function ($scope, $http, $location, UtilityService) {
	
	$http.get('menuitems/')
		.then(function(response) {
			$scope.menuitems = response.data;
		});
	
	$scope.makeMenuUrl = function(menuitem) {
		if (!UtilityService.isUndefinedOrNull(menuitem.categoryId)) {
			if ($scope.isAuthorized('ROLE_FARMER')) {
				return 'grid-category/demand/' + menuitem.categoryId;
			} else {
				return 'grid-category/product/' + menuitem.categoryId;
			}
		} else {
			if ($scope.isAuthorized('ROLE_FARMER')) {
				return 'grid/demand/' + menuitem.keywords;
			} else {
				return 'grid/product/' + menuitem.keywords;
			}
		}
	}
	
	$scope.submenuClass = function(childMenuitem) {
		if (!UtilityService.isUndefinedOrNull(childMenuitem.childMenuitems)) {
			return childMenuitem.childMenuitems.length > 0 ? 'dropdown-submenu' : '';
		}
		return '';
	}
	
}]);