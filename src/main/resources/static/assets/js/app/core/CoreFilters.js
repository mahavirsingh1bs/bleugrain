var CoreFilters = angular.module("CoreFilters", []);

CoreFilters.filter('substring', function() {
	return function(input, start, end) {
		if (!input || !input.length) { return; }
		if (typeof end != 'undefined' && end != null) {
			return input.substring(start, end);
		}
		return input.substring(start);
	}
});

CoreFilters.filter('nameFormatter', function() {
	return function(input) {
		if (!input || !input.length) { return; }
		if (typeof end != 'undefined' && end != null) {
			return input.substring(start, end);
		}
		return input.substring(start);
	}
});

CoreFilters.filter('tel', function () {
	return function (phoneNumber) {
        if (!phoneNumber)
            return phoneNumber;

        return formatLocal('US', phoneNumber); 
    }
});

CoreFilters.filter('renderHtmlCorrectly', function ($sec) {
	return function (htmlString) {
		return $sce.trustAsHtml(htmlString); 
    }
});

CoreFilters.filter('reverse', function () {
	return function(items) {
	    return items.slice().reverse();
	};
});

CoreFilters.filter('startFrom', function() {
    return function(input, start) {
        if(input) {
            start = +start; //parse to int
            return input.slice(start);
        }
        return [];
    }
});