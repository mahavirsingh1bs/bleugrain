'use strict';

var CoreDirectives = angular.module('CoreDirectives', []);

CoreDirectives.controller('AppCtrl', function() {
	var app = this;
	app.message = "Hello";
})

CoreDirectives.directive('showtab', function () {
    return {
        link: function (scope, element, attrs) {
            element.click(function(e) {
                e.preventDefault();
                $(element).tab('show');
            });
        }
    };
});

CoreDirectives.directive('dialog', function() {
	return {
		restrict: 'E',
		template: '<div>This the dialog box creating helper objects</object>'
	};
});

CoreDirectives.directive('telephone', function() {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			$(element).formatter({
				'pattern': '({{999}}) {{999}}-{{9999}}',
				'persistent': true
			});
		}
	};
});

CoreDirectives.directive('registrNo', function() {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			$(element).formatter({
				'pattern': '{{999}}-{{999}}-{{9999}}',
				'persistent': true
			});
			$(element).on('change', function(){
		        scope.$eval(attrs.ngModel + "='" + element.val() + "'");
		      });
		}
	};
});

CoreDirectives.directive('zipcode', function() {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			$(element).formatter({
				'pattern': '{{999}} {{999}}',
				'persistent': true
			});
		}
	};
});

CoreDirectives.directive('accountNo', function() {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			$(element).formatter({
				'pattern': '{{9999}} {{9999}} {{9999}} {{9999}}',
				'persistent': true
			});
		}
	};
});

CoreDirectives.directive('creditCard', function() {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			$(element).formatter({
				'pattern': '{{9999}}-{{9999}}-{{9999}}-{{9999}}'
			});
			element.on('change', function() {
				console.log(element.val());
			});
		}
	};
});

CoreDirectives.directive('debitCard', function() {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			$(element).formatter({
				'pattern': '{{9999}}-{{9999}}-{{9999}}-{{9999}}'
			});
		}
	};
});

CoreDirectives.directive('selectedBox', function() {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			if (attrs.value ===  attrs.stateId) {
				attrs.selected = 'selected';
			}
		}
	};
});

CoreDirectives.directive('addButton', ['$location', 'SharedProperties', function($location, SharedProperties) {
	return {
		restrict: 'E',
		transclude: true,
		scope: {
			addUrl: '@',
			label: '@'
		},
		template: '<a href="javascript: void(0);" class="btn btn-default">{{label}}</a>',
		link: function(scope, element, attrs) {
			element.bind('click', function() {
				SharedProperties.setLastUrl($location.path());
				scope.$apply($location.path(scope.addUrl));
			});
		}
	};
}]);

CoreDirectives.directive('editButton', ['$location', 'SharedProperties', function($location, SharedProperties) {
	return {
		restrict: 'E',
		transclude: true,
		scope: {
			editUrl: '@',
			label: '@'
		},
		template: '<a class="btn btn-xs" href="javascript: void(0);" style="color: #5bc0de;"><span class="glyphicon glyphicon-edit"></span></a>',
		link: function(scope, element, attrs) {
			element.bind('click', function() {
				SharedProperties.setLastUrl($location.path());
				scope.$apply($location.path(scope.editUrl));
			});
		}
	};
}]);

CoreDirectives.directive('cancelButton', ['$timeout', '$location', 'SharedProperties', function($timeout, $location, SharedProperties) {
	return {
		restrict: 'E',
		transclude: true,
		scope: {
			label: '@'
		},
		template: '<button type="button" class="btn btn-default form-button" autocomplete="off">{{label}}</button>',
		link: function(scope, element, attrs) {
			element.bind('click', function() {
				var lastUrl = SharedProperties.getLastUrl();
				$timeout($location.path(lastUrl));
			});
		}
	};
}]);

CoreDirectives.directive('submitButton', ['$http', '$timeout', '$location', 'ngToast', 'SharedProperties', function($http, $timeout, $location, ngToast, SharedProperties) {
	return {
		restrict: 'E',
		transclude: true,
		scope: {
			submitUrl: '@',
			entity: '=',
			label: '@',
			successMessage: '@',
			errorMessage: '@'
		},
		template: '<button type="button" class="btn btn-success form-button" autocomplete="off">{{label}}</button>',
		link: function(scope, element, attrs) {
			element.bind('click', function() {
				if (SharedProperties.getRequestedBy() === 'company') {
					scope.entity.companyId = SharedProperties.getCompanyId(); 
				}
				
				var response = $http.post(contextPath + scope.submitUrl, scope.entity);
				response.success(function(data, status, headers, config) {
					var lastUrl = SharedProperties.getLastUrl();
					ngToast.create({ className: 'success', content: data.message });
					$timeout($location.path(lastUrl));
				});
				
				response.error(function(data, status, headers, config) {
					ngToast.create({ className: 'success', content: data.message });
				});
			});
		}
	};
}]);

CoreDirectives.directive('selectedBox', function() {
	return {
		restrict: 'A',
		link: function(scope, element, attrs) {
			if (attrs.value ===  attrs.stateId) {
				attrs.selected = 'selected';
			}
		}
	};
});

CoreDirectives.directive('loginDialog', function (AUTH_EVENTS) {
	  return {
	    restrict: 'A',
	    template: '<div ng-if="visible" ng-include="\'partials/login-form.html\'">',
	    link: function (scope) {
	      var showDialog = function () {
	        scope.visible = true;
	      };
	  
	      scope.visible = false;
	      scope.$on(AUTH_EVENTS.notAuthenticated, showDialog);
	      scope.$on(AUTH_EVENTS.sessionTimeout, showDialog)
	    }
	  };
	});

CoreDirectives.directive('fileUpload', function () {
    return {
        scope: true,        //create a new scope
        link: function (scope, el, attrs) {
            el.bind('change', function (event) {
                var files = event.target.files;
                //iterate files since 'multiple' may be specified on the element
                for (var i = 0;i<files.length;i++) {
                    //emit event upward
                    scope.$emit("fileSelected", { file: files[i] });
                }         
            });
        }
    };
});

CoreDirectives.directive('prakratiPager', ['$http', '$timeout', '$location', 'ngToast', 'SharedProperties', function($http, $timeout, $location, ngToast, SharedProperties) {
	return {
		restrict: 'E',
		templateUrl: 'partials/templates/pager_template.html',
		link: function(scope, element, attrs) {
			console.log(scope.entities)
		}
	};
}]);

CoreDirectives.directive('prakratiTable', ['$http', '$timeout', '$location', 'ngToast', 'SharedProperties', function($http, $timeout, $location, ngToast, SharedProperties) {
	return {
		restrict: 'E',
		templateUrl: 'partials/templates/pager_template.html',
		link: function(scope, element, attrs) {
			console.log(scope.entities)
		}
	};
}]);

CoreDirectives.directive('owlCarousel', function() {
	return {
		restrict: 'E',
        transclude: false,
        link: function(scope) {
        	scope.initCarousel = function(element) {
        		var defaultOptions = {
    				items:4,
                    itemsDesktop : [1000,3],
                    itemsTablet : [600,2],
                    itemsMobile : [479,1]
        		};
        		
                var customOptions = scope.$eval($(element).attr('data-options'));
                
                for(var key in customOptions) {
                    defaultOptions[key] = customOptions[key];
                }
                
                $(element).owlCarousel(defaultOptions);
        	}
       }
	}
});

CoreDirectives.directive('owlCarouselItem', [function() {
    return {
        restrict: 'A',
        transclude: false,
        link: function(scope, element) {
          // wait for the last item in the ng-repeat then call init
            if(scope.$last) {
                scope.initCarousel(element.parent());
            }
        }
    };
}]);

CoreDirectives.directive('cubePortfolio', [function() {
	return {
        restrict: 'A',
        link: function(scope, element) {
        	if (scope.$last) {
        		var gridContainer = jQuery('#grid-container'),
		            filtersContainer = jQuery('#filters-container'),
		            wrap, filtersCallback;
		
		
		        /*********************************
		            init cubeportfolio
		         *********************************/
		        gridContainer.cubeportfolio({
		            layoutMode: 'grid',
		            rewindNav: true,
		            scrollByPage: false,
		            defaultFilter: '*',
		            animationType: 'slideLeft',
		            gapHorizontal: 20,
		            gapVertical: 20,
		            gridAdjustment: 'responsive',
		        
		           
		            mediaQueries: [{
		                width: 800,
		                cols: 4
		            }, {
		                width: 500,
		                cols: 2
		            }, {
		                width: 320,
		                cols: 1
		            }],
		            caption: 'zoom',
		            displayType: 'lazyLoading',
		            displayTypeSpeed: 100
		        });
		
		
		        /*********************************
		            add listener for filters
		         *********************************/
		        if (filtersContainer.hasClass('cbp-l-filters-dropdown')) {
		            wrap = filtersContainer.find('.cbp-l-filters-dropdownWrap');
		
		            wrap.on({
		                'mouseover.cbp': function() {
		                    wrap.addClass('cbp-l-filters-dropdownWrap-open');
		                },
		                'mouseleave.cbp': function() {
		                    wrap.removeClass('cbp-l-filters-dropdownWrap-open');
		                }
		            });
		
		            filtersCallback = function(me) {
		                wrap.find('.cbp-filter-item').removeClass('cbp-filter-item-active');
		                wrap.find('.cbp-l-filters-dropdownHeader').text(me.text());
		                me.addClass('cbp-filter-item-active');
		                wrap.trigger('mouseleave.cbp');
		            };
		        } else {
		            filtersCallback = function(me) {
		                me.addClass('cbp-filter-item-active').siblings().removeClass('cbp-filter-item-active');
		            };
		        }
		
		        filtersContainer.on('click.cbp', '.cbp-filter-item', function() {
		            var me = $(this);
		
		            if (me.hasClass('cbp-filter-item-active')) {
		                return;
		            }
		
		            // get cubeportfolio data and check if is still animating (reposition) the items.
		            if (!jQuery.data(gridContainer[0], 'cubeportfolio').isAnimating) {
		                filtersCallback.call(null, me);
		            }
		
		            // filter the items
		            gridContainer.cubeportfolio('filter', me.data('filter'), function() {});
		        });
		
		
		        /*********************************
		            activate counter for filters
		         *********************************/
		        gridContainer.cubeportfolio('showCounter', filtersContainer.find('.cbp-filter-item'), function() {
		            // read from url and change filter active
		            var match = /#cbpf=(.*?)([#|?&]|$)/gi.exec(location.href),
		                item;
		            if (match !== null) {
		                item = filtersContainer.find('.cbp-filter-item').filter('[data-filter="' + match[1] + '"]');
		                if (item.length) {
		                    filtersCallback.call(null, item);
		                }
		            }
		        });
        	}
        }
    };
}]);

CoreDirectives.directive('uiWizardForm', ['$compile', function($compile) {
	return {
		scope: {
			onStepChanging: "&",
			onFinishing: '&',
			onFinished: '&'
		},
	    compile: function(tElement, tAttrs, transclude) {
	    	var steps = tElement.children("div").steps({
                headerTag: ".header-tags",
                bodyTag: "section",
                transitionEffect: "fade"
            });
	    	
	    	return {
	    		pre: function(scope, element, attrs) {
	    			
	    		},
	    		post: function(scope, element, attrs) {
	    			element.children("div").on('stepChanging', function (event, currentIndex, newIndex) {
	    				if (currentIndex > newIndex) {
	                        return true;
	                    }
	                    element.validate().settings.ignore = ":disabled,:hidden";
	                    return element.valid();
	    			});
	    			
	    			element.children("div").on('finishing', function (event, currentIndex) {
	    				element.validate().settings.ignore = ":disabled";
	                    return element.valid();
	    			});
	    			
	    			element.children("div").on('finished', function (event, currentIndex) {
	    				if (tAttrs.onFinished) {
	    					scope.$apply(scope.onFinished());
	    				}
	    			});
	    		}
	    	};
	    }
	}
}]);

CoreDirectives.directive('prakDatepicker', function () {
    return {
        link: function (scope, element, attrs) {
        	element.datepicker({
	            dateFormat: 'dd/mm/yy',
	            prevText: '<i class="fa fa-angle-left"></i>',
	            nextText: '<i class="fa fa-angle-right"></i>'
	        });
        }
    };
});

CoreDirectives.directive('prakCalendar', function () {
    return {
        link: function (scope, element, attrs) {
        	element.datepicker({
	            dateFormat: 'dd.mm.yy',
	            prevText: '<i class="fa fa-angle-left"></i>',
	            nextText: '<i class="fa fa-angle-right"></i>',
	            onSelect: function( selectedDate )
	            {
	                $('#inline-finish').datepicker('option', 'minDate', selectedDate);
	            }
	        });
        }
    };
});

CoreDirectives.directive('searchBox', function () {
    return {
        link: function (scope, element, attrs) {
        	element.on('click', function() {
        		if(element.hasClass('fa-search')){
                    jQuery('.search-open').fadeIn(500);
                    element.removeClass('fa-search');
                    element.addClass('fa-times');
                } else {
                    jQuery('.search-open').fadeOut(500);
                    element.addClass('fa-search');
                    element.removeClass('fa-times');
                }
        	});
        }
    };
});

CoreDirectives.directive('prakScrollbar', function () {
    return {
        link: function (scope, element, attrs) {
        	element.mCustomScrollbar({
                theme:"minimal",
                scrollInertia: 200,
                scrollEasing: "linear"
            });
        }
    };
});

CoreDirectives.directive('userImageCropper', function () {
    return {
        link: function (scope, element, attrs) {
        	element.cropper({
				aspectRatio : 9 / 16,
				crop : function(image) {
					console.log(image.x);
					console.log(image.y);
					console.log(image.width);
					console.log(image.height);
					console.log(image.rotate);
					console.log(image.scaleX);
					console.log(image.scaleY);
				}
			});
        }
    };
});

CoreDirectives.directive('productImageCropper', function () {
    return {
        link: function (scope, element, attrs) {
        	element.cropper({
				aspectRatio : 16 / 9,
				crop : function(image) {
					console.log(image.x);
					console.log(image.y);
					console.log(image.width);
					console.log(image.height);
					console.log(image.rotate);
					console.log(image.scaleX);
					console.log(image.scaleY);
				}
			});
        }
    };
});

CoreDirectives.directive('numberOnlyInput', function () {
    return {
        restrict: 'EA',
        scope: {
            prakName: '=',
            prakPlaceholder: '@',
            prakModel: '='
        },
        template: '<input type="text" name="{{prakName}}" ng-model="prakModel" class="form-control" placeholder="{{prakPlaceholder}}" required />',
        link: function (scope) {
            scope.$watch('prakModel', function(newValue, oldValue) {
                var arr = String(newValue).split("");
                if (arr.length === 0) return;
                if (arr.length === 1 && (arr[0] == '-' || arr[0] === '.' )) return;
                if (arr.length === 2 && newValue === '-.') return;
                if (isNaN(newValue)) {
                    scope.inputValue = oldValue;
                }
            });
        }
    };
});


CoreDirectives.directive('equals', function() {
  return {
    restrict: 'A', // only activate on element attribute
    require: '?ngModel', // get a hold of NgModelController
    link: function(scope, elem, attrs, ngModel) {
      if(!ngModel) return; // do nothing if no ng-model

      // watch own value and re-validate on change
      scope.$watch(attrs.ngModel, function() {
        validate();
      });

      // observe the other value and re-validate on change
      attrs.$observe('equals', function (val) {
        validate();
      });

      var validate = function() {
        // values
        var val1 = ngModel.$viewValue;
        var val2 = attrs.equals;

        // set validity
        ngModel.$setValidity('equals', ! val1 || ! val2 || val1 === val2);
      };
    }
  }
});

CoreDirectives.directive('ngElevateZoom', function() {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      console.log("Linking")

      //Will watch for changes on the attribute
      attrs.$observe('zoomImage',function(){
        linkElevateZoom();
      })
      
      function linkElevateZoom(){
        //Check if its not empty
        if (!attrs.zoomImage) return;
        element.attr('data-zoom-image',attrs.zoomImage);
        $(element).elevateZoom();
        //$(element).elevateZoom({constrainType:"height", constrainSize:274, zoomType: "lens", containLensZoom: true, gallery:'gallery_01', cursor: 'pointer', galleryActiveClass: "active"});
      }
      
      linkElevateZoom();

    }
  };
});

CoreDirectives.directive('prakCountdown', function () {
    return {
    	scope: {
			time: '=',
			message: '=',
			timeAsString: '=',
		},
		link: function (scope, element, attrs) {
			var momentDate = moment(scope.timeAsString, 'YYYY-MM-DD HH:mm:ss.SSS');
			element.countdown(momentDate.toDate())
				.on('update.countdown', function(event) {
				  var format = '%H:%M:%S';
				  $(this).html(event.strftime(format));
				})
				.on('finish.countdown', function(event) {
				  $(this).html(scope.message)
				    .parent().addClass('disabled');
	
				});
        }
    };
});

CoreDirectives.directive('loader', function () {
    return {
        restrict: 'E',
        replace: true,
        scope: {
            key: '@'
        },
        link: function (scope, element, attributes) {

            scope.$on('us-spinner:spin', function (event, key) {
                if (key === scope.key) {
                    element.addClass('loading');
                }
            });

            scope.$on('us-spinner:stop', function (event, key) {
                if (key === scope.key) {
                    element.removeClass('loading');
                }
            });

        },
        template: '<div class="us-spinner-wrapper"><div us-spinner="{radius:30, width:8, length: 16, color: \'#5fb611\'}" spinner-key="{{key}}"></div></div>'
    };
});

/**
 * Use Croppie for a simple image cropping.
 * @see  https://github.com/Foliotek/Croppie
 * @example
 *   <prak-croppie src="cropped.source" ng-model="cropped.image"></prak-croppie>
 */
CoreDirectives.directive('prakCroppie', function () {
  return {
    restrict: 'E',
    scope: {
      src: '=',
      ngModel: '='
    },
    link: function (scope, element, attrs) {
      if(!scope.src) { return; }

      var c = new Croppie(element[0], {
        viewport: {
          width: 200,
          height: 200
        },
        update: function () {
          c.result('canvas').then(function(img) {
            scope.$apply(function () {
              scope.ngModel = img;
            });
          });
        }
      });

      // bind an image to croppie
      c.bind({
        url: scope.src
      });
    }
  };
});