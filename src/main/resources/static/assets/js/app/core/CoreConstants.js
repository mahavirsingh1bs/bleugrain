'use strict';

var CoreConstants = angular.module("CoreConstants", []);

CoreConstants.constant('AUTH_EVENTS', {
	loginSuccess : 'auth-login-success',
	loginFailed : 'auth-login-failed',
	logoutSuccess : 'auth-logout-success',
	sessionTimeout : 'auth-session-timeout',
	notAuthenticated : 'auth-not-authenticated',
	notAuthorized : 'auth-not-authorized'
});

CoreConstants.constant('USER_ROLES', {
	  all: '*',
	  admin: 'admin',
	  editor: 'editor',
	  guest: 'guest'
});

CoreConstants.constant('ENTITY_TYPES', {
	  user: 'user',
	  company: 'company',
	  product: 'product'
});

CoreConstants.constant('HTTP_RESPONSE', {
	requestSuccess : 'request-success',
	requestFailed : 'request-failed',
	requestError : 'request-error'
});

CoreConstants.constant('GRAINCART_EVENTS', {
	itemAdded : 'item-added-to-cart',
	itemRemoved : 'item-removed-from-cart',
	grainCartCleared: 'grain-cart-cleared'
});

CoreConstants.constant('WISHLIST_EVENTS', {
	itemAdded : 'item-added-to-wishlist',
	itemRemoved : 'item-removed-from-wishlist'
});

CoreConstants.constant('CIPHER_PHRASE', {
	password : 'JamesBarkAtMeAt$',
	salt: 'SW9DtOp5JO27iOB0wSnyeTS2rQsGDxkP4iB693QRqD0HeodKoP/JP33WqpoIWg9ZkgFdnmAREPbL93u77d7WQljGK7eK1/0u+t5/jGnkZa9HqYccm9J+VnvSNCpSAPrK9AmB+yjzCYn4L9m0EvvedhZTUdOCkKnSCGemAghfOrc=',
	iv: 'Hack18YearsOld27'
});