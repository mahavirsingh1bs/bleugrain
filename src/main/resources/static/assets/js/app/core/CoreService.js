'use strict';

var CoreService = angular.module("CoreService", []);

CoreService.factory('searchService', function() {
	
});

CoreService.factory('UrlLanguageStorage', ['$location', function($location) {
    return {
        put: function (name, value) {},
        get: function (name) {
            return $location.search()['lang'];
        }
    };
}]);

CoreService.factory('FormUtils', ['StringUtils', function(StringUtils) {
	var formUtils = {};
	
	formUtils.resetFormErrors = function (form) {
		angular.forEach(form, function(value, field) {
			if (!StringUtils.startsWith(field, '$') && !angular.isUndefined(form[field].$error.server)) {
				form[field].$invalid = false;
				form[field].$error.server = false;
				form[field].$error.message = null;
			}
		});
	};
	
	formUtils.handleFormErrors = function (form, fieldErrors) {
		angular.forEach(fieldErrors, function(fieldError) {
			form[fieldError.field].$invalid = true;
			form[fieldError.field].$error.server = true;
			form[fieldError.field].$error.message = fieldError.defaultMessage;
		});
	}
	
	return formUtils;
}]);

CoreService.factory('StringUtils', function() {
	var stringUtils = {};
	
	stringUtils.startsWith = function (string, prefix) {
	    return string.slice(0, prefix.length) == prefix;
	};
	
	stringUtils.endsWith = function (string, suffix) {
	    return suffix == '' || string.slice(-suffix.length) == suffix;
	}
	
	return stringUtils;
});