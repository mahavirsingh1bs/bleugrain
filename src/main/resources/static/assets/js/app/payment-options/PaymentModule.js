'use strict';

var PaymentModule = angular.module("PaymentModule", []);

PaymentModule.controller("PaymentMethodListCtrl", ['$scope', '$http', '$modal', '$log', 'HttpService', 'ngToast', function ($scope, $http, $modal, $log, HttpService, ngToast) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.keyword = null;
	$scope.pageSize = 10;
	$scope.history = false;
	$scope.paymentMethods = null; 
	$scope.pages = [];
	$scope.sortOrders = {
		"fields": ["name", "createdBy.firstName,createdBy.lastName", "dateCreated", "modifiedBy.firstName,modifiedBy.lastName", "dateModified"],
		"directions": ["asc", "asc", "asc", "asc", "asc" ]
	};
	
	$scope.sortFields = null;
	$scope.sortDirection = null;
	$scope.url = 'paymentMethods/all';
	
	$scope.getPaymentMethods = function () {
		var params = { 
			keyword: $scope.keyword,
			sortFields: $scope.sortFields,
			direction: $scope.direction,
			page: $scope.pageNumber, 
			pageSize: $scope.pageSize,
			history: $scope.history, 
		};

		$scope.readData($scope.url, params, 'paymentMethods', $scope);
	};

	$scope.getPaymentMethods();
	
	$scope.addPaymentMethod = function() {
		$scope.addEntityInternal('add-payment-method.component.html', 'PaymentMethodAddCtrl', 'paymentMethods/all', 'paymentMethods', $scope);
	};
	
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			paymentMethod: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('detail-payment-method.component.html', 'PaymentMethodDetailCtrl', 'paymentMethods/all', 'paymentMethods', $scope, resolveObj);
	};
	
	$scope.editDetail = function(uniqueId) {
		var resolveObj = { 
			paymentMethod: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.editEntityInternal('edit-payment-method.component.html', 'PaymentMethodEditCtrl', 'paymentMethods/all', 'paymentMethods', $scope, resolveObj);
	};
	
	$scope.remove = function(uniqueId) {
		var resolveObj = { 
			paymentMethod: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.removeEntityInternal('delete-payment-method.component.html', 'DeletePaymentMethodCtrl', 'paymentMethods/all', 'paymentMethods', $scope, resolveObj);
	};

	$scope.prevPage = function() {
		$scope.prevPageInternal('paymentMethods/all', 'paymentMethods', $scope);
	};
	
	$scope.moveTo = function(page) {
		$scope.moveToInternal('paymentMethods/all', 'paymentMethods', $scope, page);
	};
	
	$scope.nextPage = function() {
		$scope.nextPageInternal('paymentMethods/all', 'paymentMethods', $scope);
	};

	$scope.searchByKeyword = function() {
		$scope.searchByKeywordInternal('paymentMethods/all', 'paymentMethods', $scope);
	};
	
	$scope.sortBy = function(field, direction) {
		$scope.sortByInternal('paymentMethods/all', 'paymentMethods', $scope, field, direction);
	};
	
}]);

PaymentModule.controller("PaymentMethodAddCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', function ($scope, $http, $modalInstance, ngToast) {
	$scope.ok = function(isValid) {
		if (isValid) {
			$http.post('paymentMethods/new/', $scope.paymentMethod)
				.then(function(response) {
					if (response.data.code === 'success') {
						ngToast.create({ className: 'success', content: response.data.message });
					} else if (response.data.code === 'failure') {
						ngToast.create({ className: 'danger', content: response.data.message });
					}
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

PaymentModule.controller("PaymentMethodDetailCtrl", ['$scope', '$http', '$modalInstance', 'paymentMethod', function ($scope, $http, $modalInstance, paymentMethod) {
	$http.get('paymentMethods/data/' + paymentMethod.uniqueId)
		.then(function(response) {
			$scope.paymentMethod = response.data;
		});
	
	$scope.ok = function() {
		$modalInstance.close();
    };
}]);

PaymentModule.controller("PaymentMethodEditCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', 'paymentMethod', function ($scope, $http, $modalInstance, ngToast, paymentMethod) {
	$http.get('paymentMethods/data/' + paymentMethod.uniqueId)
		.then(function(response) {
			$scope.paymentMethod = response.data;
		});
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$scope.postUpdateEntity('paymentMethods/update/', $scope.paymentMethod, $scope.paymentMethodFrm, $modalInstance);
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

PaymentModule.controller("DeletePaymentMethodCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'paymentMethod', function ($scope, $http, $location, $modalInstance, ngToast, paymentMethod) {
	$scope.ok = function() {
		$scope.deleteEntity('paymentMethods/delete/' + paymentMethod.uniqueId, $modalInstance);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

PaymentModule.controller("PaymentCardListCtrl", ['$scope', '$http', '$modal', '$log', 'HttpService', 'ngToast', function ($scope, $http, $modal, $log, HttpService, ngToast) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.keyword = null;
	$scope.pageSize = 10;
	$scope.history = false;
	$scope.paymentCards = null; 
	$scope.pages = [];
	$scope.sortOrders = {
		"fields": ["name", "createdBy.firstName,createdBy.lastName", "dateCreated", "modifiedBy.firstName,modifiedBy.lastName", "dateModified"],
		"directions": ["asc", "asc", "asc", "asc", "asc" ]
	};
	
	$scope.sortFields = null;
	$scope.sortDirection = null;
	$scope.url = 'paymentCards/all';
	
	$scope.getPaymentCards = function () {
		var params = { 
			keyword: $scope.keyword,
			sortFields: $scope.sortFields,
			direction: $scope.direction,
			page: $scope.pageNumber, 
			pageSize: $scope.pageSize,
			history: $scope.history, 
		};
	
		$scope.readData($scope.url, params, 'paymentCards', $scope);
	};

	$scope.getPaymentCards();
	
	$scope.addPaymentCard = function() {
		$scope.addEntityInternal('add-payment-card.component.html', 'PaymentCardAddCtrl', 'paymentCards/all', 'paymentCards', $scope);
	};
	
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			paymentCard: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('detail-payment-card.component.html', 'PaymentCardDetailCtrl', 'paymentCards/all', 'paymentCards', $scope, resolveObj);
	};
	
	$scope.editDetail = function(uniqueId) {
		var resolveObj = { 
			paymentCard: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.editEntityInternal('edit-payment-card.component.html', 'PaymentCardEditCtrl', 'paymentCards/all', 'paymentCards', $scope, resolveObj);
	};
	
	$scope.remove = function(uniqueId) {
		var resolveObj = { 
			paymentCard: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.removeEntityInternal('delete-payment-card.component.html', 'DeletePaymentCardCtrl', 'paymentCards/all', 'paymentCards', $scope, resolveObj);
	};

	$scope.prevPage = function() {
		$scope.prevPageInternal('paymentCards/all', 'paymentCards', $scope);
	};
	
	$scope.moveTo = function(page) {
		$scope.moveToInternal('paymentCards/all', 'paymentCards', $scope, page);
	};
	
	$scope.nextPage = function() {
		$scope.nextPageInternal('paymentCards/all', 'paymentCards', $scope);
	};

	$scope.searchByKeyword = function() {
		$scope.searchByKeywordInternal('paymentCards/all', 'paymentCards', $scope);
	};
	
	$scope.sortBy = function(field, direction) {
		$scope.sortByInternal('paymentCards/all', 'paymentCards', $scope, field, direction);
	};
	
}]);

PaymentModule.controller("PaymentCardAddCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', function ($scope, $http, $modalInstance, ngToast) {
	$scope.ok = function(isValid) {
		if (isValid) {
			$http.post('paymentCards/new/', $scope.paymentCard)
				.then(function(response) {
					if (response.data.code === 'success') {
						ngToast.create({ className: 'success', content: response.data.message });
					} else if (response.data.code === 'failure') {
						ngToast.create({ className: 'danger', content: response.data.message });
					}
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

PaymentModule.controller("PaymentCardDetailCtrl", ['$scope', '$http', '$modalInstance', 'paymentCard', function ($scope, $http, $modalInstance, paymentCard) {
	$http.get('paymentCards/data/' + paymentCard.uniqueId)
		.then(function(response) {
			$scope.paymentCard = response.data;
		});
	
	$scope.ok = function() {
		$modalInstance.close();
    };
}]);

PaymentModule.controller("PaymentCardEditCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', 'paymentCard', function ($scope, $http, $modalInstance, ngToast, paymentCard) {
	$http.get('paymentCards/data/' + paymentCard.uniqueId)
		.then(function(response) {
			$scope.paymentCard = response.data;
		});
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$scope.postUpdateEntity('paymentCards/update/', $scope.paymentCard, $scope.paymentCardFrm, $modalInstance);
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

PaymentModule.controller("DeletePaymentCardCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'paymentCard', function ($scope, $http, $location, $modalInstance, ngToast, paymentCard) {
	$scope.ok = function() {
		$scope.deleteEntity('paymentCards/delete/' + paymentCard.uniqueId, $modalInstance);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

PaymentModule.controller("CompanyBillingAndPaymentCtrl", ['$scope', '$http', function ($scope, $http) {
	$scope.bankAccount = {};
	$scope.bankAccount.pageNumber = 0;
	$scope.bankAccount.keywords = null;
	$scope.bankAccount.pageSize = 10;
	$scope.bankAccount.pages = [];
	
	var data = { keyword : null, draw: 0, start : $scope.bankAccount.pageNumber, length : $scope.bankAccount.pageSize};
	$http.get('bankAccounts/company', { params : data}).success(function(response) {
		$scope.bankAccounts = response.data || {};
		$scope.bankAccount.totalPage = Math.ceil(response.recordsTotal / $scope.pageSize);
		for (var i = 0; i < $scope.totalPage; i++) {
			$scope.bankAccount.pages.push(i + 1);
		}
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$scope.creditCard = {};
	$scope.creditCard.pageNumber = 0;
	$scope.creditCard.keywords = null;
	$scope.creditCard.pageSize = 10;
	$scope.creditCard.pages = [];
	
	var data = { keyword : null, draw: 0, start : $scope.creditCard.pageNumber, length : $scope.creditCard.pageSize};
	$http.get('creditCards/company', { params : data}).success(function(response) {
		$scope.creditCards = response.data || {};
		$scope.creditCard.totalPage = Math.ceil(response.recordsTotal / $scope.pageSize);
		for (var i = 0; i < $scope.totalPage; i++) {
			$scope.creditCard.pages.push(i + 1);
		}
	}).error(function(data) {
		console.log('error occurred');
	});
}]);

PaymentModule.controller("UserBillingAndPaymentCtrl", ['$scope', '$http', function ($scope, $http) {
	$scope.bankAccount = {};
	$scope.bankAccount.pageNumber = 0;
	$scope.bankAccount.keywords = null;
	$scope.bankAccount.pageSize = 10;
	$scope.bankAccount.pages = [];
	
	var data = { keyword : null, draw: 0, start : $scope.bankAccount.pageNumber, length : $scope.bankAccount.pageSize};
	$http.get('bankAccounts/user', { params : data}).success(function(response) {
		$scope.bankAccounts = response.data || {};
		$scope.bankAccount.totalPage = Math.ceil(response.recordsTotal / $scope.pageSize);
		for (var i = 0; i < $scope.totalPage; i++) {
			$scope.bankAccount.pages.push(i + 1);
		}
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$scope.creditCard = {};
	$scope.creditCard.pageNumber = 0;
	$scope.creditCard.keywords = null;
	$scope.creditCard.pageSize = 10;
	$scope.creditCard.pages = [];
	
	var data = { keyword : null, draw: 0, start : $scope.creditCard.pageNumber, length : $scope.creditCard.pageSize};
	$http.get('creditCards/user', { params : data}).success(function(response) {
		$scope.creditCards = response.data || {};
		$scope.creditCard.totalPage = Math.ceil(response.recordsTotal / $scope.pageSize);
		for (var i = 0; i < $scope.totalPage; i++) {
			$scope.creditCard.pages.push(i + 1);
		}
	}).error(function(data) {
		console.log('error occurred');
	});
}]);