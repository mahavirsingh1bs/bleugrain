'use strict';

var ReviewModule = angular.module("ReviewModule", []);

ReviewModule.controller("ReviewCtrl", ['$rootScope', '$scope', '$http', function ($rootScope, $scope, $http) {
	
	$http.get('profile/reviews?uniqueId=' + $scope.currentUser.userNo)
		.then(function(response) {
			$scope.reviews = response.data;
		});
		
}]);
