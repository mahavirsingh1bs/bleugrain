'use strict';

var HistoryModule = angular.module("HistoryModule", ['ui.bootstrap', 'ngFileUpload']);

HistoryModule.controller("HistoryCtrl", ['$scope', '$http', '$modal', '$log', 'HttpService', 'UtilityService', 'ENTITY_TYPES', 
           function ($scope, $http, $modal, $log, HttpService, UtilityService, ENTITY_TYPES) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.keyword = null;
	$scope.pageSize = 10;
	$scope.history = true;
	$scope.products = null; 
	$scope.pages = [];
	$scope.sortOrders = {
		"fields": ["name", "quantity.quantity", "latestPrice.price", "owner.firstName,owner.lastName", "createdBy.firstName,createdBy.lastName", "dateCreated"],
		"directions": ["asc", "asc", "asc", "asc", "asc", "asc", "asc" ]
	};
	
	$scope.sortFields = $scope.sortOrders['fields'][0];
	$scope.sortDirection = $scope.sortOrders['directions'][0];
	
	if (!$scope.isAuthorized('ROLE_ADMIN')) {
		$scope.url = 'products/user';
		$scope.userNo = $scope.currentUser.userNo;
	} else {
		$scope.url = 'products/all';
	}

	$scope.getProducts = function() {
		var params = { 
			keyword: $scope.keyword,
			sortFields: $scope.sortFields,
			direction: $scope.direction,
			page: $scope.pageNumber, 
			pageSize: $scope.pageSize,
			history: $scope.history, 
		};
		
		if (!UtilityService.isUndefinedOrNull($scope.userNo)) {
			params.userNo = $scope.userNo;
		}
		
		HttpService.readData($scope.url, params)
			.then(function(response) {
				$scope.handleResponse(response, 'products', $scope);
			}, function() {
				console.log('error');
			});
	};
	
	$scope.getProducts();
	
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			product: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('assets/js/app/products/detail-product.component.html', 'ProductDetailCtrl', 'products/all', 'products', $scope, resolveObj);
	};
	
	$scope.prevPage = function() {
		$scope.prevPageInternal('products/all', 'products', $scope);
	};
	
	$scope.moveTo = function(page) {
		$scope.moveToInternal('products/all', 'products', $scope, page);
	};
	
	$scope.nextPage = function() {
		$scope.nextPageInternal('products/all', 'products', $scope);
	};

	$scope.searchByKeyword = function() {
		$scope.searchByKeywordInternal('products/all', 'products', $scope);
	};
	
	$scope.sortBy = function(field, direction) {
		$scope.sortByInternal('products/all', 'products', $scope, field, direction);
	};

}]);