'use strict';

var ProfileModule = angular.module("ProfileModule", ['ngFileUpload', 'ngImgCrop']);

ProfileModule.controller("HomeCtrl", ['$rootScope', '$scope', '$http', '$location', 'usSpinnerService', function ($rootScope, $scope, $http, $location, usSpinnerService) {
	$scope.keywords = '';
	$scope.dashboardDetails = {};
	
	$scope.getDashboardDetails = function() {
		$http.get('profile/dashboard')
			.then(function(response) {
				$scope.dashboardDetails = response.data;
			}, function(response) {
				console.log('error');
			});
	}
	
	$scope.getDashboardDetails();
	
	$scope.go = function(path, keywords) {
		$location.path(path + '/' + keywords);
	};
}]);

ProfileModule.controller("ProfileCtrl", ['$rootScope', '$scope', '$http', '$location', '$modal', '$log', 'ngToast', 'usSpinnerService',  
         function ($rootScope, $scope, $http, $location, $modal, $log, ngToast, usSpinnerService) {
	$scope.keywords = null;
	$scope.dashboardDetails = {};
	$scope.noteStart = 0;
	$scope.noteMaxLimit = 110;
	$scope.upcomingEventStart = 0;
	$scope.upcomingEventMaxLimit = 140;
	
	$scope.noteColors = {
		0: 'color-one',
		1: 'color-two',
		2: 'color-three',
		3: 'color-four',
		4: 'color-five',
		5: 'color-six',
		6: 'color-seven'
	};
	
	$scope.notificationIcons = {
		'EMAIL': 'icon-envelope',
		'ORDER_PLACED': 'fa fa-shopping-cart',
		'ORDER_SHIPPED': 'fa fa-rocket',
		'ORDER_ASSIGNED': 'fa fa-shopping-cart',
		'ORDER_DISPATCHED': 'fa fa-truck',
		'ORDER_DELIVERED': 'fa fa-truck',
		'PRODUCT_REVIEWED': 'fa fa-comments',
		'USER_CREATED': 'fa fa-user-plus',
		'USER_REGISTERED': 'fa fa-sign-in',
		'PRODUCT_ASSIGNED': 'fa fa-list',
		'PRODUCT_SOLDOUT': 'fa fa-sign-out',
		'DEMAND_ASSIGNED': 'fa fa-list',
		'DEMAND_FULFILLED': 'fa fa-sign-out',
		'ADVANCED_BOOKING': 'fa fa-send',
		'COMPANY_CREATED': 'fa fa-user-plus',
		'COMPANY_REGISTERED': 'fa fa-sign-in'
	};
	
	$scope.notificationIconBgColors = {
		'EMAIL': 'icon-bg-blue',
		'ORDER_PLACED': 'icon-bg-brown',
		'ORDER_SHIPPED': 'icon-bg-orange',
		'ORDER_ASSIGNED': 'icon-bg-aqua',
		'ORDER_DISPATCHED': 'icon-bg-blue',
		'ORDER_DELIVERED': 'icon-bg-purple',
		'PRODUCT_REVIEWED': 'icon-bg-yellow',
		'USER_CREATED': 'icon-bg-aqua',
		'USER_REGISTERED': 'icon-bg-dark-blue',
		'PRODUCT_ASSIGNED': 'icon-bg-brown',
		'PRODUCT_SOLDOUT': 'icon-bg-blue',
		'DEMAND_ASSIGNED': 'icon-bg-aqua',
		'DEMAND_FULFILLED': 'icon-bg-purple',
		'ADVANCED_BOOKING': 'icon-bg-dark-blue',
		'COMPANY_CREATED': 'icon-bg-yellow',
		'COMPANY_REGISTERED': 'icon-bg-orange'
	};
	
	$scope.notificationColors = {
		'EMAIL': 'color-blue',
		'ORDER_PLACED': 'color-brown',
		'ORDER_SHIPPED': 'color-orange',
		'ORDER_ASSIGNED': 'color-aqua',
		'ORDER_DISPATCHED': 'color-blue',
		'ORDER_DELIVERED': 'color-purple',
		'PRODUCT_REVIEWED': 'color-yellow',
		'USER_CREATED': 'color-aqua',
		'USER_REGISTERED': 'color-dark-blue',
		'PRODUCT_ASSIGNED': 'color-brown',
		'PRODUCT_SOLDOUT': 'color-blue',
		'DEMAND_ASSIGNED': 'color-aqua',
		'DEMAND_FULFILLED': 'color-purple',
		'ADVANCED_BOOKING': 'color-orange',
		'COMPANY_CREATED': 'color-aqua',
		'COMPANY_REGISTERED': 'color-brown'
	};
	
	$scope.alertBlockColors = {
		0: 'alert-blocks-pending',
		1: 'alert-blocks-success',
		3: 'alert-blocks-error',
		4: 'alert-blocks-info'
	};
	
	$scope.getDashboardDetails = function() {
		usSpinnerService.spin('prak-spinner');
		$http.get('profile/dashboard')
			.then(function(response) {
				usSpinnerService.stop('prak-spinner');				
				$scope.dashboardDetails = response.data;
			}, function(response) {
				usSpinnerService.stop('prak-spinner');
			});
	}
	
	$scope.getDashboardDetails();

	$scope.addNote = function () {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/shared/note/add-note.component.html',
  			controller : 'NoteAddCtrl',
  			size : 'md',
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getDashboardDetails();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }
	
	$scope.deleteNote = function (uniqueId) {
		var params = { 
			uniqueId: uniqueId
		};
		
		$http.delete('notes/delete', { params : params })
			.then(function(response) {
				if (response.data.code === 'success') {
					ngToast.create({ className: 'success', content: response.data.message });
					$scope.getDashboardDetails();
				} else if (response.data.code === 'error') {
					ngToast.create({ className: 'danger', content: response.data.message });
				}
			});
    }
	
	$scope.noteColor = function (index) {
		return $scope.noteColors[index % 7];
	};
	
	$scope.notificationIcon = function (code) {
		return $scope.notificationIcons[code];
	};
	
	$scope.notificationIconBgColor = function (code) {
		return $scope.notificationIconBgColors[code];
	};
	
	$scope.notificationColor = function (code) {
		return $scope.notificationColors[code];
	};
	
	$scope.alertBlockColor = function (index) {
		return $scope.alertBlockColors[index % 7];
	};
	
	$scope.addUpcomingEvent = function () {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/shared/add-upcoming-event.component.html',
  			controller : 'UpcomingEventAddCtrl',
  			size : 'md',
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getDashboardDetails();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }
	
	$scope.deleteUpcomingEvent = function (uniqueId) {
		var params = { 
			uniqueId: uniqueId 
		};
		
		$http.delete('upcomingEvents/delete', { params : params })
			.then(function(response) {
				if (response.data.code === 'success') {
					ngToast.create({ className: 'success', content: response.data.message });
					$scope.getDashboardDetails();
				} else if (response.data.code === 'error') {
					ngToast.create({ className: 'danger', content: response.data.message });
				}
			});
    }
	
	$scope.parseDate = function(eventDate) {
		return new Date(eventDate);
	}
}]);

ProfileModule.controller("MyProfileCtrl", ['$rootScope', '$scope', '$http', '$location', '$timeout', '$log', '$modal', 'ngToast', 'usSpinnerService', function ($rootScope, $scope, $http, $location, $timeout, $log, $modal, ngToast, usSpinnerService) {
	$scope.keywords = null;
	$scope.uploaedImage = null;
	
	$scope.labels = ['2009', '2010', '2011', '2012'];
	$scope.series = ['First', 'Second'];

	$scope.data = [
	    [81, 56, 55, 40],
	    [19, 86, 27, 90]
	];
	
	$scope.getProfileDetails = function () {
		usSpinnerService.spin('prak-spinner');
		$http.get('profile/me?uniqueId=' + $scope.currentUser.userNo)
			.then(function(response) {
				usSpinnerService.stop('prak-spinner');
				$scope.profileDetails = response.data;
			}, function(response) {
				usSpinnerService.stop('prak-spinner');
			});
	}
	
	$scope.getProfileDetails();
	
	$scope.readImage = function (file) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$scope.uploadedImage = e.target.result;
        };
		reader.readAsDataURL(file);
    }
	
	$scope.sendMobileVerificationCode = function() {
		$http.post('profile/sendMobileVerificationCode?userNo=' + $scope.currentUser.userNo)
			.then(
				function(response) {
					if (response.data.code === 'success') {
						// ngToast.create({ className: 'success', content: response.data.message });
						$scope.verifyMobileNoPopup();
					} else if (response.data.code === 'failure') {
						ngToast.create({ className: 'danger', content: response.data.message });
					}
				}, function(response) {
					ngToast.create({ className: 'danger', content: 'An Internal server error occured while we were trying to send verification link to you.' });
				});
	}
	
	$scope.verifyMobileNoPopup = function() {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/profile/verify-mobile-no.component.html',
  			controller : 'VerifyMobileNoCtrl',
  			size : 'md',
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function() {
  			$scope.getProfileDetails();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
	}
	
	$scope.sendVerificationLink = function() {
		$http.post('profile/sendEmailVerificationLink?userNo=' + $scope.currentUser.userNo)
			.then(
				function(response) {
					if (response.data.code === 'success') {
						ngToast.create({ className: 'success', content: response.data.message });
					} else if (response.data.code === 'failure') {
						ngToast.create({ className: 'danger', content: response.data.message });
					}
				}, function(response) {
					ngToast.create({ className: 'danger', content: 'An Internal server error occured while we were trying to send verification link to you.' });
				});
	}
	
	$scope.addAddress = function () {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/shared/address/add-address.component.html',
  			controller : 'AddressAddCtrl',
  			size : 'md',
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getProfileDetails();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }
	
	$scope.addAddressEntity = function (title, category) {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/shared/address/add-address-entity.component.html',
  			controller : 'AddressEntityAddCtrl',
  			size : 'md',
  			scope : $scope,
  			resolve: {
  				options : function() {
  					return {
  						title: title,
  						category: category,
  					};
  				}
  			}
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getProfileDetails();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }
	
	$scope.showAddresses = function (title, category) {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/shared/address/addresses.component.html',
  			controller: 'AddressEntityListCtrl',
  			size : 'lg',
  			scope : $scope,
  			resolve: {
  				options : function() {
  					return {
  						title: title,
  						category: category,
  					};
  				}
  			}
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getProfileDetails();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }
	
	$scope.addExpertise = function () {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/shared/expertise/add-expertise.component.html',
  			controller : 'ExpertiseAddCtrl',
  			size : 'md',
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getProfileDetails();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }
	
	$scope.editExpertise = function (uniqueId) {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/shared/expertise/edit-expertise.component.html',
  			controller : 'ExpertiseEditCtrl',
  			size : 'md',
  			scope : $scope,
  			resolve: {
  				options: function() {
  					return {
  						uniqueId: uniqueId 
  					}
  				}
  			}
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getProfileDetails();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }
	
	$scope.deleteExpertise = function (uniqueId) {
		var params = { 
			uniqueId: uniqueId 
		};
		
		$http.delete('expertises/delete', { params : params })
			.then(function(response) {
				if (response.data.code === 'success') {
					ngToast.create({ className: 'success', content: response.data.message });
					$scope.getProfileDetails();
				} else if (response.data.code === 'error') {
					ngToast.create({ className: 'danger', content: response.data.message });
				}
			});
    }
	
	$scope.addSocialAccount = function () {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/shared/social-account/add-social-account.component.html',
  			controller : 'SocialAccountAddCtrl',
  			size : 'md',
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getProfileDetails();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }
	
	$scope.editSocialAccount = function (uniqueId) {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/shared/social-account/edit-social-account.component.html',
  			controller : 'SocialAccountEditCtrl',
  			size : 'md',
  			scope : $scope,
  			resolve: {
  				options: function() {
  					return {
  						uniqueId: uniqueId 
  					}
  				}
  			}
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getProfileDetails();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }

	$scope.deleteSocialAccount = function (uniqueId) {
		var params = { 
			uniqueId: uniqueId 
		};
		
		$http.delete('socialAccounts/delete', { params : params })
			.then(function(response) {
				if (response.data.code === 'success') {
					ngToast.create({ className: 'success', content: response.data.message });
					$scope.getProfileDetails();
				} else if (response.data.code === 'error') {
					ngToast.create({ className: 'danger', content: response.data.message });
				}
			});
    }
	
	$scope.addDiscussion = function () {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/shared/discussion/add-discussion.component.html',
  			controller : 'DiscussionAddCtrl',
  			size : 'md',
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getProfileDetails();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }

	$scope.sendNotification = function () {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/shared/send-notification.component.html',
  			controller : 'NotificationAddCtrl',
  			size : 'md',
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getProfileDetails();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }
	
	$scope.deleteDiscussion = function () {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/shared/discussion/delete-discussion.component.html',
  			controller : 'DiscussionDeleteCtrl',
  			size : 'md',
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getProfileDetails();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }
	
}]);

ProfileModule.controller("CompanyProfileCtrl", ['$rootScope', '$scope', '$http', '$location', '$timeout', '$log', '$modal', 'ngToast', function ($rootScope, $scope, $http, $location, $timeout, $log, $modal, ngToast) {
	$scope.maxlength = 1000;
	$scope.fields = {};
	$scope.backupCompany = null;
	$scope.keywords = null;
	$scope.uploaedImage = null;
	
	$scope.getCompanyProfile = function() {
		$http.get('profile/company?uniqueId=' + $scope.currentUser.company.uniqueId)
			.then(function(response) {
				$scope.company = response.data;
			});
	}
	
	$scope.getCompanyProfile();
	
	$scope.addAddressEntity = function (title, category) {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/shared/address/add-address-entity.component.html',
  			controller : 'AddressEntityAddCtrl',
  			size : 'md',
  			scope : $scope,
  			resolve: {
  				options : function() {
  					return {
  						title: title,
  						category: category,
  					};
  				}
  			}
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getCompanyProfile();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }
	
	$scope.editAddressEntity = function (title, category, address) {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/shared/address/edit-address-entity.component.html',
  			controller : 'AddressEntityEditCtrl',
  			size : 'md',
  			scope : $scope,
  			resolve : {
  				options : function() {
  					return {
  						id: address.id,
  						title: title,
  						category: category
  					};
  				}
  			}
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getCompanyProfile();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }
	
	$scope.showAddresses = function (title, category) {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/shared/address/addresses.component.html',
  			controller: 'AddressEntityListCtrl',
  			size : 'lg',
  			scope : $scope,
  			resolve: {
  				options : function() {
  					return {
  						title: title,
  						category: category,
  					};
  				}
  			}
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getCompanyProfile();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }

	$scope.addExpertise = function () {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/shared/expertise/add-expertise.component.html',
  			controller : 'ExpertiseAddCtrl',
  			size : 'md',
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getCompanyProfile();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }
	
	$scope.addSocialAccount = function () {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/shared/social-account/add-social-account.component.html',
  			controller : 'SocialAccountAddCtrl',
  			size : 'md',
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getCompanyProfile();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }
	
	$scope.editExpertise = function (uniqueId) {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/shared/expertise/edit-expertise.component.html',
  			controller : 'ExpertiseEditCtrl',
  			size : 'md',
  			scope : $scope,
  			resolve: {
  				options: function() {
  					return {
  						uniqueId: uniqueId 
  					}
  				}
  			}
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getCompanyProfile();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }
	
	$scope.editSocialAccount = function (uniqueId) {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/shared/social-account/edit-social-account.component.html',
  			controller : 'SocialAccountEditCtrl',
  			size : 'md',
  			scope : $scope,
  			resolve: {
  				options: function() {
  					return {
  						uniqueId: uniqueId 
  					}
  				}
  			}
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getCompanyProfile();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }

	$scope.deleteExpertise = function (uniqueId) {
		var params = { 
			uniqueId: uniqueId 
		};
		
		$http.delete('expertises/delete', { params : params })
			.then(function(response) {
				if (response.data.code === 'success') {
					ngToast.create({ className: 'success', content: response.data.message });
					$scope.getCompanyProfile();
				} else if (response.data.code === 'error') {
					ngToast.create({ className: 'danger', content: response.data.message });
				}
			});
    }
	
	$scope.deleteSocialAccount = function (uniqueId) {
		var params = { 
			uniqueId: uniqueId 
		};
		
		$http.delete('socialAccounts/delete', { params : params })
			.then(function(response) {
				if (response.data.code === 'success') {
					ngToast.create({ className: 'success', content: response.data.message });
					$scope.getCompanyProfile();
				} else if (response.data.code === 'error') {
					ngToast.create({ className: 'danger', content: response.data.message });
				}
			});
    }
	
	$scope.addAdmin = function () {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/companies/add-company-admin.component.html',
  			controller : 'CompanyAdminAddCtrl',
  			size : 'lg',
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getCompanyProfile();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }
	
	$scope.viewAdminDetail = function (uniqueId) {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/companies/detail-company-admin.component.html',
  			controller : 'CompanyAdminDetailCtrl',
  			size : 'lg',
  			scope : $scope,
  			resolve: {
  				companyAdmin: function() {
  					return {
  						uniqueId: uniqueId
  					}
  				}
  			}
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getCompanyProfile();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }
	
	$scope.editAdmin = function (uniqueId) {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/companies/edit-company-admin.component.html',
  			controller : 'CompanyAdminEditCtrl',
  			size : 'lg',
  			scope : $scope,
  			resolve: {
  				companyAdmin: function() {
  					return {
  						uniqueId: uniqueId
  					}
  				}
  			}
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getCompanyProfile();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }
	
	$scope.deleteAdmin = function (uniqueId) {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'delete-company-admin.component.html',
  			controller : 'DeleteCompanyAdminCtrl',
  			size : 'lg',
  			scope : $scope,
  			resolve: {
  				companyAdmin: function() {
  					return {
  						uniqueId: uniqueId
  					}
  				}
  			}
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getCompanyProfile();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }
	
	$scope.addAgent = function () {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/companies/add-agent.component.html',
  			controller : 'AgentAddCtrl',
  			size : 'lg',
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getCompanyProfile();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }
	
	$scope.viewAgentDetail = function (uniqueId) {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/companies/detail-agent.component.html',
  			controller : 'AgentDetailCtrl',
  			size : 'lg',
  			scope : $scope,
  			resolve: {
  				agent: function() {
  					return {
  						uniqueId: uniqueId
  					}
  				}
  			}
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getCompanyProfile();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }
	
	$scope.editAgent = function (uniqueId) {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/companies/edit-agent.component.html',
  			controller : 'AgentEditCtrl',
  			size : 'lg',
  			scope : $scope,
  			resolve: {
  				agent: function() {
  					return {
  						uniqueId: uniqueId
  					}
  				}
  			}
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getCompanyProfile();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }
	
	$scope.deleteAgent = function (uniqueId) {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'delete-agent.component.html',
  			controller : 'DeleteAgentCtrl',
  			size : 'lg',
  			scope : $scope,
  			resolve: {
  				agent: function() {
  					return {
  						uniqueId: uniqueId
  					}
  				}
  			}
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getCompanyProfile();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }
	
	$scope.editField = function (fieldName) {
		angular.forEach($scope.fields, function (value, fieldName) {
			$scope.fields[fieldName] = false;
		});
		$scope.fields[fieldName] = true;
		$scope.backupCompany = angular.copy($scope.company);
	}

	$scope.done = function (fieldName) {
		$http.post('companies/update', $scope.company)
			.then(function(response) {
				$scope.fields[fieldName] = false;
				$scope.getCompanyProfile();
			});
	}
	
	$scope.cancel = function (fieldName) {
		$scope.fields[fieldName] = false;
		$scope.company = $scope.backupCompany;
	}

	$scope.changeCompanyLogo = function() {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/shared/company-picture-crop.component.html',
  			controller : 'ChangeCompanyLogoCtrl',
  			size : 'lg',
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			if (defaultImage !== null) {
  				$scope.company.defaultImage = defaultImage;
  			}
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
	};
		
}]);

ProfileModule.controller("ProfileBidListCtrl", ['$rootScope', '$scope', '$http', '$location', function ($rootScope, $scope, $http, $location) {
	$scope.bids = [];
	$scope.pageNumber = 0;
	$scope.pageSize = 10;
	$scope.category = null;
	
	if (!$scope.isAuthorized('ROLE_COMPANY_ADMIN')) {
		$scope.url = 'bids/user';
		$scope.userNo = $scope.currentUser.userNo;
	} else {
		$scope.url = 'bids/company';
		$scope.companyId = $scope.currentUser.company.uniqueId;
	}
	
	$http.get('categories/')
		.then(function(response) {
			$scope.categories = response.data;
			$scope.category = $scope.categories[0];
			$scope.getBiddableProducts();
		});

	$scope.getBiddableProducts = function () {
		var params = {
			uniqueId: $scope.category.uniqueId,
			userNo: $scope.userNo,
			companyId: $scope.companyId,
			page: $scope.pageNumber,
			pageSize: $scope.pageSize
		};
		
		$http.get($scope.url, { params: params })
			.then(function(response) {
				$scope.bids = response.data;
			});
	};
	
	$scope.changeCategory = function(category) {
		$scope.category = category;
		$scope.getBiddableProducts();
	};
	
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			product: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('assets/js/app/products/detail-product.component.html', 'ProductDetailCtrl', 'products/all', 'products', $scope, resolveObj);
	};
	
}]);

ProfileModule.controller("OrdersCtrl", ['$rootScope', '$scope', '$http', '$location', function ($rootScope, $scope, $http, $location) {
	
}]);

ProfileModule.controller("SettingsCtrl", ['$rootScope', '$scope', '$http', '$location', 'ngToast', 'FormUtils', '$modal', function ($rootScope, $scope, $http, $location, ngToast, FormUtils, $modal) {
	$scope.fields = {};
	$scope.backupUser = null;
	$scope.changePasswd = {};
	
	$scope.getUserSettings = function() {
		$http.get('profile/settings?uniqueId=' + $scope.currentUser.userNo)
			.then(function(response) {
				$scope.settings = response.data;
				$scope.user = response.data.user;
			});
	};
	
	$scope.getUserSettings();
	
	$scope.editField = function (fieldName) {
		angular.forEach($scope.fields, function (value, fieldName) {
			$scope.fields[fieldName] = false;
		});
		$scope.fields[fieldName] = true;
		$scope.backupUser = angular.copy($scope.user);
	}

	$scope.editAddress = function () {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/shared/address/edit-address.component.html',
  			controller : 'AddressEditCtrl',
  			size : 'md',
  			scope : $scope,
  			resolve: { 
  				address: function() {
  					return $scope.user.address;
  				}
  			}
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getUserSettings();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }

	$scope.done = function (fieldName) {
		$http.post('users/update?userNo=' + $scope.currentUser.userNo, $scope.user)
			.then(function(response) {
				$scope.fields[fieldName] = false;
				$scope.getUserSettings();
			});
	}
	
	$scope.cancel = function (fieldName) {
		$scope.fields[fieldName] = false;
		$scope.user = $scope.backupUser;
	}
	
	$scope.searchLanguage = function(keywords) {
		return $http.get('languages/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.selectLanguage = function(item, model, label) {
		$scope.user.defaultLanguage = item;
	}
	
	$scope.changePassword = function (changePasswdFrm) {
		if (changePasswdFrm.$valid) {
			$scope.changePasswd.userId = $scope.user.userNo; 
			$http.post('account/changePasswd', $scope.changePasswd)
				.then(function(response) {
					changePasswdFrm.message = response.data.message;
					ngToast.create({ className: 'success', content: response.data.message });
				}, function(response) {
					FormUtils.resetFormErrors(changePasswdFrm);
					FormUtils.handleFormErrors(changePasswdFrm, response.data.fieldErrors);
				});
		}
	}
	
	$scope.reset = function (form) {
		if (form) {
	      form.$setPristine();
	      form.$setUntouched();
	    }
		$scope.changePasswd = {}
	}
	
}]);

ProfileModule.controller("WelcomeCtrl", ['$scope', '$http', function ($scope, $http) {
	$http.get('welcome').success(function(response) {
		$scope.bleugrain = response;
	}).error(function(data) {
		console.log('error occurred');
	});
}]);

ProfileModule.controller("LoginCtrl", ['$scope', '$rootScope', '$location', '$cookies', 'AUTH_EVENTS', 'AuthService', 'Idle', function ($scope, $rootScope, $location, $cookies, AUTH_EVENTS, AuthService, Idle) {
	
	$scope.credentials = {
		username: '',
		password: ''
	};
	
	$scope.login = function(isValid) {
		if (isValid) {
			AuthService.login($scope.credentials).then(function(user) {
				if (user !== null) {
					$rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
					$location.path("/dashboard");
				} else {
					$scope.errorMessage = "Please enter a valid username or password."
				}
			}, function() {
				$scope.errorMessage = "Server connection error. Please try again after some time."
				$rootScope.$broadcast(AUTH_EVENTS.loginFailed);
			});
		}
	};	
	
}]);

ProfileModule.controller("LoginRegisterCtrl", ['$scope', '$rootScope', '$location', '$cookies', '$modalInstance', 'AUTH_EVENTS', 'AuthService', 'Idle', function ($scope, $rootScope, $location, $cookies, $modalInstance, AUTH_EVENTS, AuthService, Idle) {
	
	$scope.activeTab = 'login';
	
	$scope.credentials = {
		username: '',
		password: ''
	};
	
	$scope.login = function(isValid) {
		if (isValid) {
			AuthService.login($scope.credentials).then(function(user) {
				$scope.sendLoginSuccessAndWatch($modalInstance);
			}, function() {
				$rootScope.$broadcast(AUTH_EVENTS.loginFailed);
			});
		}
	};	
	
	$scope.userType = 'customer';
	$scope.termsAndConditions = null;
	/*$scope.farmer = {};*/
	$scope.customer = {};
	/*$scope.broker = {};
	$scope.retailer = {};
	$scope.company = {};*/	
	
	$scope.register = function(isValid) {
		if (isValid) {
			/*if ($scope.userType === 'farmer') {
				AuthService.register('register/farmer', $scope.farmer)
					.then(function(response) {
						$scope.handleUserRegistr(response);
					}, function() {
						$rootScope.$broadcast(AUTH_EVENTS.loginFailed);
					});
			} else if ($scope.userType === 'customer') {*/
				AuthService.register('register/customer', $scope.customer)
					.then(function(response) {
						$scope.handleUserRegistr(registerFrm, response); 
					}, function() {
						$rootScope.$broadcast(AUTH_EVENTS.loginFailed);
					});
			/*} */
			
			/*
			else if ($scope.userType === 'trader') {
				AuthService.register('register/broker', $scope.broker)
					.then(function(response) {
						$scope.handleUserRegistr(response); 
					}, function() {
						$rootScope.$broadcast(AUTH_EVENTS.loginFailed);
					});
			} else if ($scope.userType === 'retailer') {
				AuthService.register('register/retailer', $scope.retailer)
					.then(function(response) {
						$scope.handleUserRegistr(response);
					}, function() {
						$rootScope.$broadcast(AUTH_EVENTS.loginFailed);
					});
			} 
			*/
			/*
			else if ($scope.userType === 'company') {
				$http.post('register/company', $scope.company)
					.then(function(response) {
						if (response.data.code === 'success') {
							ngToast.create({ className: 'success',content: response.data.message });
							$location.path('/thankYou');
						} else if (response.data.code === 'error') {
							ngToast.create({ className: 'success',content: response.data.message });
						}
					});
			}*/
		}
	};
	
	$scope.handleUserRegistr = function(registerFrm, response) {
		if (response.code === 'error' || response.code === 'failure') {
			angular.forEach(response.fieldErrors, function(fieldError, index) {
				registerFrm[fieldError.field].$invalid = true;
				registerFrm[fieldError.field].$error.server = true;
				registerFrm[fieldError.field].$error.message = fieldError.defaultMessage;
			});
		} else {
			/*$location.path("/verifyContactNo").search("code", response.user.uniqueCode);*/
			AuthService.createSession();
			$scope.sendLoginSuccessAndWatch($modalInstance);
		}
	};
	
	$scope.makeActive = function(tab) {
		if (angular.equals(tab, 'login')) {
			$scope.activeTab = 'login';
		} else {
			$scope.activeTab = 'register';
		}
	}
	
}]);

ProfileModule.controller("RegisterCtrl", ['$scope', '$rootScope', '$location', '$http', '$cookies', 'AUTH_EVENTS', 'AuthService', 'BusinessCategoryService', 'ngToast', function ($scope, $rootScope, $location, $http, $cookies, AUTH_EVENTS, AuthService, BusinessCategoryService, ngToast) {
	$scope.userType = 'farmer';
	$scope.termsAndConditions = null;
	$scope.farmer = {};
	$scope.customer = {};
	$scope.broker = {};
	$scope.retailer = {};
	$scope.company = {};

	$scope.businessCategoryConfig = BusinessCategoryService.config({}, function(value, $item) {
		$scope.company.businessCategory = value;
	});
	
	BusinessCategoryService.findBusinessCategories('businessCategories/all')
		.then(function(businessCategories) {
			$scope.businessCategories = businessCategories; 
		});
	
	$scope.register = function(isValid) {
		if (isValid) {
			if ($scope.userType === 'farmer') {
				AuthService.register('register/farmer', $scope.farmer)
					.then(function(response) {
						$scope.handleUserRegistr(response);
					}, function() {
						$rootScope.$broadcast(AUTH_EVENTS.loginFailed);
					});
			}
			/**
			else if ($scope.userType === 'customer') {
				AuthService.register('register/customer', $scope.customer)
					.then(function(response) {
						$scope.handleUserRegistr(response);
					}, function() {
						$rootScope.$broadcast(AUTH_EVENTS.loginFailed);
					});
			}
			*/
			else if ($scope.userType === 'broker') {
				AuthService.register('register/broker', $scope.broker)
					.then(function(response) {
						$scope.handleUserRegistr(response); 
					}, function() {
						$rootScope.$broadcast(AUTH_EVENTS.loginFailed);
					});
			} 
			/**
			else if ($scope.userType === 'retailer') {
				AuthService.register('register/retailer', $scope.retailer)
					.then(function(response) {
						$scope.handleUserRegistr(response);
					}, function() {
						$rootScope.$broadcast(AUTH_EVENTS.loginFailed);
					});
			} 
			*/
			else if ($scope.userType === 'company') {
				$http.post('register/company', $scope.company)
					.then(function(response) {
						$scope.handleCompanyRegistr(response);
					});
			}
		}
	};
	
	$scope.handleUserRegistr = function(response) {
		if (response.code === 'error' || response.code === 'failure') {
			angular.forEach(response.fieldErrors, function(fieldError, index) {
				$scope.registerFrm[fieldError.field].$invalid = true;
				$scope.registerFrm[fieldError.field].$error.server = true;
				$scope.registerFrm[fieldError.field].$error.message = fieldError.defaultMessage;
			});
		} else {
			$location.path("/verifyContactNo").search("code", response.user.uniqueCode);
		}
	};
	
	$scope.handleCompanyRegistr = function(response) {
		if (response.data.code === 'success') {
			ngToast.create({ className: 'success',content: response.data.message });
			$location.path('/thankYou');
		} else if (response.data.code === 'error') {
			ngToast.create({ className: 'success',content: response.data.message });
		}
	};
	
}]);

ProfileModule.controller("VerifyContactNoCtrl", ['$scope', '$rootScope', '$location', '$stateParams', 'AUTH_EVENTS', 'AuthService', 'ngToast', function ($scope, $rootScope, $location, $stateParams, AUTH_EVENTS, AuthService, ngToast) {
	$scope.oneTimePasswd = null;
	$scope.servererror = null;
	
	$scope.verifyAndProceed = function(verificationFrm) {
		if (verificationFrm.$valid) {
			var data = { uniqueCode: $stateParams.code, oneTimePasswd: $scope.oneTimePasswd };
			AuthService.verifyAndProceed('register/verifyContactNo', data)
				.then(function(response) {
					if (response.code === 'success') {
						$scope.createSessionAndWatch();
					} else if (response.code === 'error') {
						$scope.verificationFrm.oneTimePasswd.$error.server = true;
						$scope.verificationFrm.oneTimePasswd.$error.message = response.message;
					}
				});
		}
	};
	
	$scope.continueToHome = function() {
		$scope.createSessionAndWatch();
	};
	
}]);

ProfileModule.controller("VerifyEmailAddressCtrl", ['$scope', '$rootScope', '$location', '$stateParams', 'AUTH_EVENTS', 'AuthService', 'ngToast', function ($scope, $rootScope, $location, $stateParams, AUTH_EVENTS, AuthService, ngToast) {
	AuthService.verifyAndProceed('register/verifyEmail?verifCode=' + $stateParams.verificationCode)
		.then(function(response) {
			if (response.code === 'success') {
				$scope.emailVerificationSuccess = true;
				$scope.emailVerificationMessage = response.message;
			}
		}, function(response) {
			if (response.data.code === 'error') { 
				$scope.emailVerificationSuccess = false;
				$scope.emailVerificationMessage = 'Your email address cannot be verified. your verification code might have got expired or you may have entered a wrong verification code.';
			}
		});
	
}]);

ProfileModule.controller("LogoutCtrl", ['$rootScope', '$scope', '$location', 'AUTH_EVENTS', 'AuthService', 'NotificationListener', 'BidListener', 'ProductListener', function ($rootScope, $scope, $location, AUTH_EVENTS, AuthService, NotificationListener, BidListener, ProductListener) {
	$scope.logout = function() {
		NotificationListener.disconnect();
		BidListener.disconnect();
		ProductListener.disconnect();
		AuthService.logout().then(function() {
			$rootScope.$broadcast(AUTH_EVENTS.logoutSuccess);
			$location.path("/");
		});
	};
}]);

ProfileModule.controller("SignUpController", ['$scope', '$http', function ($scope, $http) {
	$scope.username = 'Mahavir Singh';
	$scope.password = 'James Peter';
	$scope.confirmPassword = 'James Peter';
}]);


ProfileModule.controller("AccountCtrl", ['$scope', '$http', '$log', '$location', function ($scope, $http, $log, $location) {
	$scope.resetLinkSent = false;
	
	$scope.submit = function(emailAddress) {
		$http
			.post('account/forgotPasswd', emailAddress)
			.then(function(response) {
				if (angular.equals(response.data.code, 'success')) {
					$log.info('Reset password link has been sent to your account');
					$scope.resetLinkSent = true;
				} else if (angular.equals(response.data.code, 'error')) {
					$log.info(response.message);
					$scope.resetLinkSent = false;
					$scope.errorMessage = response.data.message;
				}
			});
	}
	
	$scope.cancel = function() {
		
	}
}]);

ProfileModule.controller("ResetPasswdCtrl", ['$scope', '$http', '$log', '$location', '$stateParams', function ($scope, $http, $log, $location, $stateParams) {
	$scope.passwdGotReset = false;
	$scope.resetCodeValid = false;
	var resetCode = $stateParams.resetCode;
	$log.info('Reset code' + resetCode);
	
	$http.post('account/validateResetCode', resetCode)
		.then(function(response) {
			if (response.data.code === 'success') {
				$scope.resetCodeValid = true;
				$log.info('Reset code got validated successfully');
			} else {
				$log.info('Reset code is not valid or might be got expired');
			}
		});
	
	$scope.submit = function(passwd, confirmPasswd) {
		if (passwd == confirmPasswd) {
			$http.post('account/resetPasswd?resetCode=' + resetCode, passwd)
				.then(function(response) {
					if (response.data.code === 'success') {
						$scope.passwdGotReset = true;
						$log.info('Your password has been got reset successfully.');
					} else {
						$log.info('Your password has not been got reset.');
					}
				});
		}
	};
	
	$scope.cancel = function() { }
	
}]);

ProfileModule.controller("ChangeCompanyLogoCtrl", ['$scope', '$http', '$location', '$modalInstance', 'Upload', 'ngToast', 'Cropper', '$timeout', function ($scope, $http, $location, $modalInstance, Upload, ngToast, Cropper, $timeout) {
	
	  var file, data;
	
	  /**
	   * Method is called every time file input's value changes.
	   * Because of Angular has not ng-change for file inputs a hack is needed -
	   * call `angular.element(this).scope().onFile(this.files[0])`
	   * when input's event is fired.
	   */
	  $scope.onFile = function(blob) {
	    Cropper.encode((file = blob)).then(function(dataUrl) {
	      $scope.dataUrl = dataUrl;
	      $timeout(showCropper);  // wait for $digest to set image's src
	    });
	  };
	
	  /**
	   * Croppers container object should be created in controller's scope
	   * for updates by directive via prototypal inheritance.
	   * Pass a full proxy name to the `ng-cropper-proxy` directive attribute to
	   * enable proxing.
	   */
	  $scope.cropper = {};
	  $scope.cropperProxy = 'cropper.first';
	
	  /**
	   * When there is a cropped image to show encode it to base64 string and
	   * use as a source for an image element.
	   */
	  $scope.preview = function() {
	    if (!file || !data) return;
	    Cropper.crop(file, data).then(Cropper.encode).then(function(dataUrl) {
	      ($scope.preview || ($scope.preview = {})).dataUrl = dataUrl;
	    });
	  };
	
	  /**
	   * Use cropper function proxy to call methods of the plugin.
	   * See https://github.com/fengyuanchen/cropper#methods
	   */
	  $scope.clear = function(degrees) {
	    if (!$scope.cropper.first) return;
	    $scope.cropper.first('clear');
	  };
	
	  $scope.scale = function(width) {
	    Cropper.crop(file, data)
	      .then(function(blob) {
	        return Cropper.scale(blob, {width: width});
	      })
	      .then(Cropper.encode).then(function(dataUrl) {
	        ($scope.preview || ($scope.preview = {})).dataUrl = dataUrl;
	      });
	  }
	
	  /**
	   * Object is used to pass options to initalize a cropper.
	   * More on options - https://github.com/fengyuanchen/cropper#options
	   */
	  $scope.options = {
	    maximize: true,
	    aspectRatio: 1.5 / 1,
	    crop: function(dataNew) {
	      data = dataNew;
	    }
	  };
	
	  /**
	   * Showing (initializing) and hiding (destroying) of a cropper are started by
	   * events. The scope of the `ng-cropper` directive is derived from the scope of
	   * the controller. When initializing the `ng-cropper` directive adds two handlers
	   * listening to events passed by `ng-cropper-show` & `ng-cropper-hide` attributes.
	   * To show or hide a cropper `$broadcast` a proper event.
	   */
	  $scope.showEvent = 'show';
	  $scope.hideEvent = 'hide';
	
	  function showCropper() { $scope.$broadcast($scope.showEvent); }
	  function hideCropper() { $scope.$broadcast($scope.hideEvent); }
	  
	  $scope.uploadPicture = function() {
		  var formData = new FormData();
		  formData.append('uniqueId', new Blob([$scope.currentUser.company.uniqueId], { type: "application/json" }));
		  var dataUrl = $scope.preview.dataUrl.substring(22);
		  formData.append('companyLogo', $scope.preview.dataUrl);
			
		  $http.post('profile/changeLogo', formData, {
				transformRequest: angular.identity,
				headers: { 'Content-Type': undefined }
			}).then(
				function(response) {
					$modalInstance.close(response.data);
				}, function(response) {
					ngToast.create({ className: 'danger', content: response.data.message });
				});
	  }
			
	  $scope.ok = function() {
		  $modalInstance.close({});
	  };

	  $scope.close = function() {
		  $modalInstance.dismiss('cancel');
	  };
}]);

ProfileModule.controller("VerifyMobileNoCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', function ($scope, $http, $location, $modalInstance, ngToast) {
	$scope.verifyMobileNo = {};
	
	$scope.submit = function(isValid) {
		if (isValid) {
			$scope.verifyMobileNo['userNo'] = $scope.currentUser.userNo;
			$scope.verifyMobileNo['oneTimePassword'] = $scope.oneTimePassword;
			
			$http.post('profile/verifyOneTimePassword', $scope.verifyMobileNo)
				.then(function(response) {
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
    
}]);