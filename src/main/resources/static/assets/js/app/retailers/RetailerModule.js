'use strict';

var RetailerModule = angular.module("RetailerModule", []);

RetailerModule.controller("RetailerListCtrl", ['$scope', '$http', '$modal', '$log', 'HttpService', 'ngToast', function ($scope, $http, $modal, $log, HttpService, ngToast) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.keyword = null;
	$scope.pageSize = 10;
	$scope.history = false;
	$scope.retailers = null; 
	$scope.pages = [];
	$scope.sortOrders = {
		"fields": ["firstName,lastName", "phoneNo", "email", "createdBy.firstName,createdBy.lastName", "dateModified", "modifiedBy.firstName,modifiedBy.lastName", "dateModified"],
		"directions": ["asc", "asc", "asc", "asc", "asc" ]
	};
	
	$scope.sortFields = $scope.sortOrders['fields'][0];
	$scope.sortDirection = $scope.sortOrders['directions'][0];
	$scope.url = 'retailers/all';
	
	$scope.getRetailers = function () {
		var params = { 
			keyword: $scope.keyword,
			sortFields: $scope.sortFields,
			direction: $scope.direction,
			page: $scope.pageNumber, 
			pageSize: $scope.pageSize,
			history: $scope.history, 
		};

		$scope.readData($scope.url, params, 'retailers', $scope);
	};

	$scope.getRetailers();
	
	$scope.addRetailer = function() {
		$scope.addEntityInternal('assets/js/app/retailers/add-retailer.component.html', 'RetailerAddCtrl', 'retailers/all', 'retailers', $scope);
	};
	
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			retailer: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('assets/js/app/retailers/detail-retailer.component.html', 'RetailerDetailCtrl', 'retailers/all', 'retailers', $scope, resolveObj);
	};
	
	$scope.editDetail = function(uniqueId) {
		var resolveObj = { 
			retailer: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.editEntityInternal('assets/js/app/retailers/edit-retailer.component.html', 'RetailerEditCtrl', 'retailers/all', 'retailers', $scope, resolveObj);
	};
	
	$scope.remove = function(uniqueId) {
		var resolveObj = { 
			retailer: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.removeEntityInternal('delete-retailer.component.html', 'DeleteRetailerCtrl', 'retailers/all', 'retailers', $scope, resolveObj);
	};

	$scope.prevPage = function() {
		$scope.prevPageInternal('retailers/all', 'retailers', $scope);
	};
	
	$scope.moveTo = function(page) {
		$scope.moveToInternal('retailers/all', 'retailers', $scope, page);
	};
	
	$scope.nextPage = function() {
		$scope.nextPageInternal('retailers/all', 'retailers', $scope);
	};

	$scope.searchByKeyword = function() {
		$scope.searchByKeywordInternal('retailers/all', 'retailers', $scope);
	};
	
	$scope.sortBy = function(field, direction) {
		$scope.sortByInternal('retailers/all', 'retailers', $scope, field, direction);
	};
	
}]);

RetailerModule.controller("RetailerAddCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'usSpinnerService', 'FormUtils', function ($scope, $http, $location, $modalInstance, ngToast, usSpinnerService, FormUtils) {
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
		});
	
	$scope.ok = function(isValid) {
		if (isValid) {
			var formData = new FormData();
			
			formData.append('retailer', new Blob([JSON.stringify($scope.retailer)], { type: "application/json" }));
			formData.append('retailerImage', $scope.retailerImage);
			$scope.postCreateEntity('retailers/new', formData, $scope.retailerFrm, $modalInstance);
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

RetailerModule.controller("RetailerDetailCtrl", ['$scope', '$http', '$modalInstance', 'retailer', function ($scope, $http, $modalInstance, retailer) {
	$http.get('retailers/data/' + retailer.uniqueId)
		.then(function(response) {
			$scope.retailer = response.data;
		});
	
	$scope.close = function() {
		$modalInstance.close();
    };
}]);

RetailerModule.controller("RetailerEditCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', 'retailer', function ($scope, $http, $modalInstance, ngToast, retailer) {
	
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
		});
	
	$http.get('retailers/data/' + retailer.uniqueId)
		.then(function(response) {
			$scope.retailer = response.data;
		});
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$scope.postUpdateEntity('retailers/update/', $scope.retailer, $scope.retailerFrm, $modalInstance);
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

RetailerModule.controller("DeleteRetailerCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'retailer', function ($scope, $http, $location, $modalInstance, ngToast, retailer) {
	$scope.ok = function() {
		$scope.deleteEntity('retailers/delete/' + retailer.uniqueId, $modalInstance);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);