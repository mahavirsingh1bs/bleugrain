'use strict';

var RoleModule = angular.module("RoleModule")

RoleModule.service("RoleService", ['$http', function($http) {
	
	var roleService = {};
	
	roleService.config = function(options) {
		var defaultOptions = {
		  create: false,
		  valueField: 'id',
		  labelField: 'name',
		  searchField: 'name',
		  delimiter: '|',
		  placeholder: 'Select roles',
		  maxItems: 10
		};
		
		return angular.extend({}, defaultOptions, options);
	};
	
	roleService.findRoles = function(url) {
		return $http.get(url)
			.then(function(response) {
				return response.data;
			});
	};
	
	return roleService;
}]);