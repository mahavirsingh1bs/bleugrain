'use strict';

var CategoryModule = angular.module("CategoryModule")

CategoryModule.service("CategoryService", ['$http', 'UtilityService', function($http, UtilityService) {
	
	var categoryService = {};
	
	categoryService.config = function(options) {
		var defaultOptions = {
		  create: false,
		  valueField: 'uniqueId',
		  labelField: 'name',
		  searchField: 'name',
		  delimiter: '|',
		  placeholder: 'Select parent category',
		  maxItems: 1
		};
		
		return angular.extend({}, defaultOptions, options);
	};
	
	categoryService.findCategories = function(url) {
		return categoryService.findParentCategories(url);
	};
	
	categoryService.findParentCategories = function(url) {
		return $http.get(url)
			.then(function(response) {
				var parentCategories = [];
				response.data.forEach(function(rootCategory) {
					parentCategories.push(rootCategory);
					categoryService.processParentCategory(rootCategory, parentCategories);
				});
				return parentCategories;
			});
	};
	
	categoryService.findMatchingCategories = function(url, params) {
		return $http.get(url, { params: params })
			.then(function(response) {
				return response.data;
			});
	};
	
	categoryService.processParentCategory = function(category, parentCategories) {
		if (!UtilityService.isUndefinedOrNull(category.childCategories)) {
			category.childCategories.forEach(function(childCategory) {
				childCategory.name = category.name + ' > ' + childCategory.name;
				parentCategories.push(childCategory);
				categoryService.processParentCategory(childCategory, parentCategories);
			});
		}
	} 
	
	return categoryService;
}]);