'use strict';

var CompanyModule = angular.module("CompanyModule")

CompanyModule.service("BusinessCategoryService", ['$http', 'UtilityService', function($http, UtilityService) {
	
	var businessCategoryService = {};
	
	businessCategoryService.config = function(options, callback) {
		var defaultOptions = {
		  create: false,
		  valueField: 'code',
		  labelField: 'name',
		  searchField: ['name'],
		  delimiter: '|',
		  placeholder: 'Select business category',
		  onItemAdd: function(value, $item) {
			callback(value, $item);
		  },
		  maxItems: 1
		};
		
		return angular.extend({}, defaultOptions, options);
	};
	
	businessCategoryService.findBusinessCategories = function(url) {
		return $http.get(url)
			.then(function(response) {
				return response.data;
			});
	};
	
	return businessCategoryService;
}]);