'use strict';

var MenuitemModule = angular.module("MenuitemModule")

MenuitemModule.service("MenuitemService", ['$http', 'UtilityService', function($http, UtilityService) {
	
	var menuitemService = {};
	
	menuitemService.config = function(options) {
		var defaultOptions = {
		  create: false,
		  valueField: 'uniqueId',
		  labelField: 'name',
		  searchField: ['name'],
		  delimiter: '|',
		  placeholder: 'Select parent menuitem',
		  maxItems: 1
		};
		
		return angular.extend({}, defaultOptions, options);
	};
	
	menuitemService.findParentMenuitems = function(url) {
		return $http.get(url)
			.then(function(response) {
				var parentMenuitems = [];
				response.data.forEach(function(rootMenuitem) {
					parentMenuitems.push(rootMenuitem);
					menuitemService.processParentMenuitem(rootMenuitem, parentMenuitems);
				});
				return parentMenuitems;
			});
	};
	
	menuitemService.processParentMenuitem = function(menuitem, parentMenuitems) {
		if (!UtilityService.isUndefinedOrNull(menuitem.childMenuitems)) {
			menuitem.childMenuitems.forEach(function(childMenuitem) {
				childMenuitem.name = menuitem.name + ' > ' + childMenuitem.name;
				parentMenuitems.push(childMenuitem);
				menuitemService.processParentMenuitem(childMenuitem, parentMenuitems);
			});
		}
	} 
	
	return menuitemService;
}]);