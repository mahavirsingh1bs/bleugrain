'use strict';

var PermitModule = angular.module("PermitModule", [])

PermitModule.service("PermitService", ['$http', function($http) {
	
	var permitService = {};
	
	permitService.config = function(options, callback) {
		var defaultOptions = {
		  create: false,
		  valueField: 'id',
		  labelField: 'name',
		  searchField: 'name',
		  delimiter: '|',
		  placeholder: 'Select permits',
		  onItemAdd: function(value, $item) {
			callback(value, $item);
		  },
		  maxItems: 10
		};
		
		return angular.extend({}, defaultOptions, options);
	};
	
	permitService.findPermits = function(url) {
		return $http.get(url)
			.then(function(response) {
				return response.data;
			});
	};
	
	return permitService;
}]);