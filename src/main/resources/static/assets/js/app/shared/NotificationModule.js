'use strict';

var NotificationModule = angular.module("NotificationModule", ['ngFileUpload', 'ngImgCrop']);

NotificationModule.controller("NotificationListCtrl", ['$scope', '$http', '$location', 'NotificationListener', function ($scope, $http, $location, NotificationListener) {
	$scope.notifications = [];
	NotificationListener.connect();
	
	$scope.notificationIcons = {
		'EMAIL': 'icon-envelope',
		'ORDER_PLACED': 'fa fa-shopping-cart',
		'ORDER_SHIPPED': 'fa fa-rocket',
		'ORDER_ASSIGNED': 'fa fa-shopping-cart',
		'ORDER_DISPATCHED': 'fa fa-truck',
		'ORDER_DELIVERED': 'fa fa-truck',
		'PRODUCT_REVIEWED': 'fa fa-comments',
		'USER_CREATED': 'fa fa-user-plus',
		'USER_REGISTERED': 'fa fa-sign-in',
		'PRODUCT_ASSIGNED': 'fa fa-list',
		'PRODUCT_SOLDOUT': 'fa fa-sign-out',
		'DEMAND_ASSIGNED': 'fa fa-list',
		'DEMAND_FULFILLED': 'fa fa-sign-out',
		'ADVANCED_BOOKING': 'fa fa-send',
		'COMPANY_CREATED': 'fa fa-user-plus',
		'COMPANY_REGISTERED': 'fa fa-sign-in',
	};
	
	$scope.notificationIconBgColors = {
		'EMAIL': 'icon-bg-blue',
		'ORDER_PLACED': 'icon-bg-brown',
		'ORDER_SHIPPED': 'icon-bg-orange',
		'ORDER_ASSIGNED': 'icon-bg-aqua',
		'ORDER_DISPATCHED': 'icon-bg-blue',
		'ORDER_DELIVERED': 'icon-bg-purple',
		'PRODUCT_REVIEWED': 'icon-bg-yellow',
		'USER_CREATED': 'icon-bg-aqua',
		'USER_REGISTERED': 'icon-bg-dark-blue',
		'PRODUCT_ASSIGNED': 'icon-bg-brown',
		'PRODUCT_SOLDOUT': 'icon-bg-blue',
		'DEMAND_ASSIGNED': 'icon-bg-aqua',
		'DEMAND_FULFILLED': 'icon-bg-purple',
		'ADVANCED_BOOKING': 'icon-bg-dark-blue',
		'COMPANY_CREATED': 'icon-bg-yellow',
		'COMPANY_REGISTERED': 'icon-bg-orange',
	};
	
	$scope.findNotifications = function() {
		$http.get('notifications/all')
			.then(function(response) {
				$scope.notifications = response.data;
			});
	};
	
	$scope.findNotifications();
	
	$scope.notificationIcon = function (code) {
		return $scope.notificationIcons[code];
	};
	
	$scope.notificationColor = function (code) {
		return $scope.notificationColors[code];
	};
	
	$scope.displayToUser = function (users, currentUser) {
		var shouldDisplay = false;
		angular.forEach(users, function(user, index) {
			if (angular.equals(user.userNo, currentUser.userNo)) {
				shouldDisplay = true;
			}
		});
		return shouldDisplay;
	};
	
	$scope.displayToGroup = function (groups, userGroup) {
		var shouldDisplay = false;
		angular.forEach(groups, function(group, index) {
			if (angular.equals(group.name, userGroup.name)) {
				shouldDisplay = true;
			}
		});
		return shouldDisplay;
	};
	
	$scope.displayToCompany = function (companies, currentCompany) {
		var shouldDisplay = false;
		angular.forEach(companies, function(company, index) {
			if (angular.equals(company.uniqueId, currentCompany.uniqueId)) {
				shouldDisplay = true;
			}
		});
		return shouldDisplay;
	};
	
	NotificationListener.receive().then(null, null, function(notification) {
		if ($scope.displayToUser(notification.users, $scope.currentUser) || 
				$scope.displayToGroup(notification.groups, $scope.currentUser.group) ||
				$scope.displayToCompany(notification.companies, $scope.currentUser.company)) {
			$scope.notifications.push(notification);
		}
	});

}]);

NotificationModule.controller("NotificationAddCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'NotificationListener', function ($scope, $http, $location, $modalInstance, ngToast, NotificationListener) {
	$scope.notification = {};
	$scope.notification.groups = [];
	$scope.notification.users = [];
	$scope.notification.companies = [];

	$http.get('notifCategories/all')
		.then(function(response) {
			$scope.categories = response.data;
		});
	
	$scope.searchGroup = function(keywords) {
		return $http.get('groups/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.searchUser = function(keywords) {
		return $http.get('users/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.searchCompany = function(keywords) {
		return $http.get('companies/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.userProperty = function(user) {
		return user.firstName + ' ' + user.lastName;
	}
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$http.post('profile/sendNotification?userNo=' + $scope.currentUser.userNo, $scope.notification)
				.then(function(response) {
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

NotificationModule.controller("NotificationEditCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'id', function ($scope, $http, $location, $modalInstance, ngToast, id) {
	
	$http.get('notifications/' + uniqueId)
		.then(function(response) {
			$scope.notification = response.data;
		});
	
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
		});

	
	$scope.ok = function(notificationFrm) {
		if (notificationFrm) {
			$http.post('profile/editNotification?userNo=' + $scope.currentUser.userNo, $scope.notification)
				.then(function(response) {
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);