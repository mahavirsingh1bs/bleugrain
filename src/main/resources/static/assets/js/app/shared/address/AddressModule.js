'use strict';

var AddressModule = angular.module("AddressModule", ['ngFileUpload', 'ngImgCrop']);

AddressModule.controller("AddressAddCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', function ($scope, $http, $location, $modalInstance, ngToast) {
	
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
		});

	$scope.ok = function(isValid) {
		if (isValid) {
			$http.post('profile/addAddress?userNo=' + $scope.currentUser.userNo, $scope.address)
				.then(function(response) {
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
    
}]);

AddressModule.controller("AddressEditCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'address', function ($scope, $http, $location, $modalInstance, ngToast, address) {
	
	$scope.address = address;
	
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
		});

	
	$scope.ok = function(isValid) {
		if (isValid) {
			$http.post('profile/editAddress?userNo=' + $scope.currentUser.userNo, $scope.address)
				.then(function(response) {
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

AddressModule.controller("AddressEntityAddCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'options', function ($scope, $http, $location, $modalInstance, ngToast, options) {
	$scope.options = angular.extend({}, options);
	
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
		});

	
	$scope.ok = function(isValid) {
		if (isValid) {
			if ($scope.options.category === 'billing') {
				$http.post('profile/addBillingAddress?userNo=' + $scope.currentUser.userNo, $scope.address)
					.then(function(response) {
						$modalInstance.close();
					});
			} else {
				$http.post('profile/addDeliveryAddress?userNo=' + $scope.currentUser.userNo, $scope.address)
					.then(function(response) {
						$modalInstance.close();
					});
			}
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

AddressModule.controller("AddressEntityListCtrl", ['$scope', '$http', '$location', '$modal', '$modalInstance', 'ngToast', 'options', function ($scope, $http, $location, $modal, $modalInstance, ngToast, options) {
	$scope.options = angular.extend({}, options);
	
	$scope.fetchAddresses = function() { 
		$http.get('addresses/' + options.category)
			.then(function(response) {
				$scope.addresses = response.data;
			});
	};
	
	$scope.fetchAddresses();
	
	$scope.editAddressEntity = function (title, category, address) {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/shared/address/edit-address-entity.component.html',
  			controller : 'AddressEntityEditCtrl',
  			size : 'md',
  			scope : $scope,
  			resolve : { 
  				options : function() {
  					return {
  						id: address.id,
  						title: title,
  						category: category
  					};
  				}
  			}
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.fetchAddresses();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }
	
	$scope.deleteAddress = function (address) {
		var uniqueId = address.id;
		var deleteRequest = $http.delete('addresses/delete/' + uniqueId);
		
		deleteRequest.success(function(response) {
			$scope.fetchAddresses();
		});
	};
	

	$scope.setDefaultAddress = function (category, address) {
		var params = { id: address.id, category: category, userNo: $scope.currentUser.userNo };
		$http.post('profile/setDefaultAddress', null, { params: params })
			.then(function(response) {
				$scope.fetchAddresses();
			});
    }
	
    $scope.close = function() {
    	$modalInstance.close();
    };
}]);

AddressModule.controller("AddressEntityEditCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'options', function ($scope, $http, $location, $modalInstance, ngToast, options) {
	$scope.options = angular.extend({}, options);
	
	$http.get('addresses/' + options.id)
		.then(function(response) {
			$scope.address = response.data;
		});
	
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
		});

	
	$scope.ok = function(isValid) {
		if (isValid) {
			$http.post('profile/editAddressEntity?userNo=' + $scope.currentUser.userNo, $scope.address)
				.then(function(response) {
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

/*
AddressModule.controller("AddressEntityAddCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', function ($scope, $http, $location, $modalInstance, ngToast) {
	
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
		});

	
	$scope.ok = function(isValid) {
		if (isValid) {
			if ($scope.addressCategory === 'billingAddress') {
				$http.post('profile/addBillingAddress?userNo=' + $scope.currentUser.userNo, $scope.address)
					.then(function(response) {
						$modalInstance.close();
					});
			} else {
				$http.post('profile/addDeliveryAddress?userNo=' + $scope.currentUser.userNo, $scope.address)
					.then(function(response) {
						$modalInstance.close();
					});
			}
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);
*/