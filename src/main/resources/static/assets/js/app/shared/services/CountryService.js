'use strict';

var CountryModule = angular.module("CountryModule")

CountryModule.service("CountryService", ['$http', function($http) {
	
	var countryService = {};
	
	countryService.config = function(options, callback) {
		var defaultOptions = {
		  create: false,
		  valueField: 'id',
		  labelField: 'name',
		  searchField: 'name',
		  delimiter: '|',
		  placeholder: 'Select country',
		  onItemAdd: function(value, $item) {
			callback(value, $item);
		  },
		  maxItems: 1
		};
		
		return angular.extend({}, defaultOptions, options);
	};
	
	countryService.findCountries = function(url) {
		return $http.get(url)
			.then(function(response) {
				return response.data.countries;
			});
	};
	
	return countryService;
}]);