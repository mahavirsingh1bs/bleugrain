'use strict';

var DiscussionModule = angular.module("DiscussionModule", ['ngFileUpload', 'ngImgCrop']);

DiscussionModule.controller("DiscussionAddCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', function ($scope, $http, $location, $modalInstance, ngToast) {
	
	$scope.searchGroup = function(keywords) {
		return $http.get('groups/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.searchTag = function(keywords) {
		return $http.get('tags/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$http.post('profile/addDiscussion?userNo=' + $scope.currentUser.userNo, $scope.discussion)
				.then(function(response) {
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

DiscussionModule.controller("DiscussionEditCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'id', function ($scope, $http, $location, $modalInstance, ngToast, id) {
	
	$http.get('discussions/' + uniqueId)
		.then(function(response) {
			$scope.discussion = response.data;
		});
	
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
		});

	
	$scope.ok = function(discussionFrm) {
		if (discussionFrm) {
			$http.post('profile/editDiscussion?userNo=' + $scope.currentUser.userNo, $scope.discussion)
				.then(function(response) {
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);