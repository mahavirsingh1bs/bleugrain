'use strict';

var SocialAccountModule = angular.module("SocialAccountModule", []);

SocialAccountModule.controller("SocialAccountAddCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', function ($scope, $http, $location, $modalInstance, ngToast) {
	$scope.socialAccount = {};
	
	$scope.setSocialSite = function(socialSite) {
		$scope.socialAccount.socialSite = socialSite;
	}
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$http.post('socialAccounts/add?userNo=' + $scope.currentUser.userNo, $scope.socialAccount)
				.then(function(response) {
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

SocialAccountModule.controller("SocialAccountEditCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'options', function ($scope, $http, $location, $modalInstance, ngToast, options) {
	
	$http.get('socialAccounts/get?uniqueId=' + options.uniqueId)
		.then(function(response) {
			$scope.socialAccount = response.data;
		}, function(resp) {
			console.log(resp);
		});
	
	$scope.setSocialSite = function(socialSite) {
		$scope.socialAccount.socialSite = socialSite;
	}
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$http.put('socialAccounts/update', $scope.socialAccount)
				.then(function(response) {
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);
