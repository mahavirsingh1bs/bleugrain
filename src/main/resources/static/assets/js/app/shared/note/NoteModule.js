'use strict';

var NoteModule = angular.module("NoteModule", ['ngFileUpload', 'ngImgCrop']);

NoteModule.controller("NoteAddCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', function ($scope, $http, $location, $modalInstance, ngToast) {
	$scope.maxlength = 1000;
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$http.post('profile/addNote?userNo=' + $scope.currentUser.userNo, $scope.note)
				.then(function(response) {
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

NoteModule.controller("NoteEditCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'id', function ($scope, $http, $location, $modalInstance, ngToast, id) {
	
	$http.get('notes/' + uniqueId)
		.then(function(response) {
			$scope.note = response.data;
		});
	
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
		});

	
	$scope.ok = function(noteFrm) {
		if (noteFrm) {
			$http.post('profile/editNote?userNo=' + $scope.currentUser.userNo, $scope.note)
				.then(function(response) {
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);