'use strict';

var StateModule = angular.module("StateModule")

StateModule.service("StateService", ['$http', function($http) {
	
	var stateService = {};
	
	stateService.config = function(options, callback) {
		var defaultOptions = {
		  create: false,
		  valueField: 'id',
		  labelField: 'name',
		  searchField: 'name',
		  delimiter: '|',
		  placeholder: 'Select state',
		  onItemAdd: function(value, $item) {
			callback(value, $item);
		  },
		  maxItems: 1
		};
		
		return angular.extend({}, defaultOptions, options);
	};
	
	stateService.findStates = function(url) {
		return $http.get(url)
			.then(function(response) {
				return response.data.states;
			});
	};
	
	return stateService;
}]);