'use strict';

var ExpertiseModule = angular.module("ExpertiseModule", []);

ExpertiseModule.controller("ExpertiseAddCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', function ($scope, $http, $location, $modalInstance, ngToast) {
	
	$http.get('assets/resources/data/json/years.json')
		.then(function(response) {
			$scope.years = response.data;
		}, function(resp) {
			console.log(resp);
		});
		
	$scope.ok = function(isValid) {
		if (isValid) {
			$scope.expertise.experLabel = $scope.expertise.experience.label;
			$scope.expertise.experience = $scope.expertise.experience.value;
			$http.post('expertises/add?userNo=' + $scope.currentUser.userNo, $scope.expertise)
				.then(function(response) {
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

ExpertiseModule.controller("ExpertiseEditCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'options', function ($scope, $http, $location, $modalInstance, ngToast, options) {
	
	$http.get('assets/resources/data/json/years.json')
		.then(function(response) {
			$scope.years = response.data;
		}, function(resp) {
			console.log(resp);
		});
		
	$http.get('expertises/get?uniqueId=' + options.uniqueId)
		.then(function(response) {
			$scope.expertise = response.data;
			$scope.expertise.experience = $scope.getExperience($scope.expertise.experience); 
		}, function(resp) {
			console.log(resp);
		});
	
	$scope.getExperience = function(experience) {
		var expYear = null; 
		angular.forEach($scope.years, function(year, index) {
			if (angular.equals(year.value, experience)) {
				expYear = year;
			}
		});
		return expYear;
	}
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$scope.expertise.experLabel = $scope.expertise.experience.label;
			$scope.expertise.experience = $scope.expertise.experience.value;
			$http.put('expertises/update', $scope.expertise)
				.then(function(response) {
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);
