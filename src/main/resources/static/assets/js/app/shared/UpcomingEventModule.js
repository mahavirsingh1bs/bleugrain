'use strict';

var UpcomingEventModule = angular.module("UpcomingEventModule", ['ngFileUpload', 'ngImgCrop']);

UpcomingEventModule.controller("UpcomingEventAddCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', function ($scope, $http, $location, $modalInstance, ngToast) {
	$scope.upcomingEvent = {};
	$scope.upcomingEvent.groups = [];
	$scope.upcomingEvent.users = [];
	$scope.upcomingEvent.companies = [];
	
	$scope.searchGroup = function(keywords) {
		return $http.get('groups/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.searchUser = function(keywords) {
		return $http.get('users/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.selectOrganizer = function(item, model, label) {
		$scope.upcomingEvent.organizerId = item.userNo;
	}
	
	$scope.searchCompany = function(keywords) {
		return $http.get('companies/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.userProperty = function(user) {
		return user.firstName + ' ' + user.lastName;
	}
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$http.post('profile/addUpcomingEvent?userNo=' + $scope.currentUser.userNo, $scope.upcomingEvent)
				.then(function(response) {
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

UpcomingEventModule.controller("UpcomingEventEditCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'id', function ($scope, $http, $location, $modalInstance, ngToast, id) {
	
	$http.get('upcomingEvents/' + uniqueId)
		.then(function(response) {
			$scope.upcomingEvent = response.data;
		});
	
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
		});

	
	$scope.ok = function(upcomingEventFrm) {
		if (upcomingEventFrm) {
			$http.post('profile/editUpcomingEvent?userNo=' + $scope.currentUser.userNo, $scope.upcomingEvent)
				.then(function(response) {
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);