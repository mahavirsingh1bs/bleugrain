'use strict';

var GroupModule = angular.module("GroupModule", []);

GroupModule.controller("GroupListCtrl", ['$scope', '$http', '$modal', '$log', 'HttpService', function ($scope, $http, $modal, $log, HttpService) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.keyword = null;
	$scope.pageSize = 10;
	$scope.history = false;
	$scope.groups = null;
	$scope.roles = [];
	$scope.pages = [];
	$scope.sortOrders = {
		"fields": ["name", "createdBy.firstName,createdBy.lastName", "dateCreated", "modifiedBy.firstName,modifiedBy.lastName", "dateModified"],
		"directions": ["asc", "asc", "asc", "asc", "asc" ]
	};
	
	$scope.sortFields = null;
	$scope.sortDirection = null;
	$scope.url = 'groups/all';

	$scope.getRoles = function() {
		$http.get('roles')
			.then(function(response) {
				$scope.roles = response.data;
			});
	}
	
	$scope.getGroups = function() {
		var params = { 
			keyword: $scope.keyword,
			sortFields: $scope.sortFields,
			direction: $scope.direction,
			page: $scope.pageNumber, 
			pageSize: $scope.pageSize,
			history: $scope.history, 
		};

		$scope.readData($scope.url, params, 'groups', $scope);
	};
	
	$scope.getGroups();
	$scope.getRoles();
	
	$scope.addGroup = function() {
		var resolveObj = { 
			roles: function() {
				return $scope.roles;
			}
		};
		
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'add-group.component.html',
  			controller : 'GroupAddCtrl',
  			size : 'lg',
	  		resolve: resolveObj,
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function() {
  			$scope.getGroups();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
  		
	};
	
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			group: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('detail-group.component.html', 'GroupDetailCtrl', 'groups/all', 'groups', $scope, resolveObj);
	};
	
	$scope.editDetail = function(uniqueId) {
		var resolveObj = { 
			group: function() {
				return {
					uniqueId: uniqueId
				}
			},
			roles: function() {
				return $scope.roles;
			}
		};
		
		$scope.editEntityInternal('edit-group.component.html', 'GroupEditCtrl', 'groups/all', 'groups', $scope, resolveObj);
	};
	
	$scope.remove = function(uniqueId) {
		var resolveObj = { 
			group: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.removeEntityInternal('delete-group.component.html', 'DeleteGroupCtrl', 'groups/all', 'groups', $scope, resolveObj);
	};

	$scope.prevPage = function() {
		$scope.prevPageInternal('groups/all', 'groups', $scope);
	};
	
	$scope.moveTo = function(page) {
		$scope.moveToInternal('groups/all', 'groups', $scope, page);
	};
	
	$scope.nextPage = function() {
		$scope.nextPageInternal('groups/all', 'groups', $scope);
	};

	$scope.searchByKeyword = function() {
		$scope.searchByKeywordInternal('groups/all', 'groups', $scope);
	};
	
	$scope.sortBy = function(field, direction) {
		$scope.sortByInternal('groups/all', 'groups', $scope, field, direction);
	};
	
}]);

GroupModule.controller("GroupAddCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', 'RoleService', function ($scope, $http, $modalInstance, ngToast, RoleService) {
	
	$scope.group = {};
	$scope.group.selectedRoles = [];
	
	$scope.config = RoleService.config({});
	
	RoleService.findRoles('roles/')
		.then(function(roles) {
			$scope.roles = roles; 
		});
	
	/*
	$http.get('roles')
		.then(function(response) {
			$scope.roles = response.data;
		});
	*/
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$http.post('groups/new/', $scope.group)
				.then(function(response) {
					if (response.data.code === 'success') {
						ngToast.create({ className: 'success', content: response.data.message });
					} else if (response.data.code === 'failure') {
						ngToast.create({ className: 'danger', content: response.data.message });
					}
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

GroupModule.controller("GroupDetailCtrl", ['$scope', '$http', '$modalInstance', 'group', function ($scope, $http, $modalInstance, group) {
	$http.get('groups/data/' + group.uniqueId)
		.then(function(response) {
			$scope.group = response.data;
		});
	
	$scope.ok = function() {
		$modalInstance.close();
    };
}]);

GroupModule.controller("GroupEditCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', 'group', 'RoleService', function ($scope, $http, $modalInstance, ngToast, group, RoleService) {
	
	$scope.config = RoleService.config({}, function(value, $item) {
		console.log(value);
	})
	
	RoleService.findRoles('roles/')
		.then(function(roles) {
			$scope.roles = roles; 
		});
	
	$http.get('groups/data/' + group.uniqueId)
		.then(function(response) {
			$scope.group = response.data;
		});
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$scope.postUpdateEntity('groups/update/', $scope.group, $scope.groupFrm, $modalInstance);
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

GroupModule.controller("DeleteGroupCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'group', function ($scope, $http, $location, $modalInstance, ngToast, group) {
	$scope.ok = function() {
		$scope.deleteEntity('groups/delete/' + group.uniqueId, $modalInstance);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);