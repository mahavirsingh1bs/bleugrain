'use strict';

var DesignationModule = angular.module("DesignationModule", []);

DesignationModule.controller("DesignationListCtrl", ['$scope', '$http', '$modal', '$log', 'HttpService', 'ngToast', function ($scope, $http, $modal, $log, HttpService, ngToast) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.keyword = null;
	$scope.pageSize = 10;
	$scope.history = false;
	$scope.designations = null; 
	$scope.pages = [];
	$scope.sortOrders = {
		"fields": ["name", "createdBy.firstName,createdBy.lastName", "dateCreated", "modifiedBy.firstName,modifiedBy.lastName", "dateModified"],
		"directions": ["asc", "asc", "asc", "asc", "asc" ]
	};
	
	$scope.sortFields = null;
	$scope.sortDirection = null;
	$scope.url = 'designations/all';
	
	$scope.getDesignations = function() {
		var params = { 
			keyword: $scope.keyword,
			sortFields: $scope.sortFields,
			direction: $scope.direction,
			page: $scope.pageNumber, 
			pageSize: $scope.pageSize,
			history: $scope.history, 
		};

		$scope.readData($scope.url, params, 'designations', $scope);
	};
	
	$scope.getDesignations();
	
	$scope.addDesignation = function() {
		$scope.addEntityInternal('add-designation.component.html', 'DesignationAddCtrl', 'designations/all', 'designations', $scope);
	};
	
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			designation: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('detail-designation.component.html', 'DesignationDetailCtrl', 'designations/all', 'designations', $scope, resolveObj);
	};
	
	$scope.editDetail = function(uniqueId) {
		var resolveObj = { 
			designation: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.editEntityInternal('edit-designation.component.html', 'DesignationEditCtrl', 'designations/all', 'designations', $scope, resolveObj);
	};
	
	$scope.remove = function(uniqueId) {
		var resolveObj = { 
			designation: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.removeEntityInternal('delete-designation.component.html', 'DeleteDesignationCtrl', 'designations/all', 'designations', $scope, resolveObj);
	};

	$scope.prevPage = function() {
		$scope.prevPageInternal('designations/all', 'designations', $scope);
	};
	
	$scope.moveTo = function(page) {
		$scope.moveToInternal('designations/all', 'designations', $scope, page);
	};
	
	$scope.nextPage = function() {
		$scope.nextPageInternal('designations/all', 'designations', $scope);
	};

	$scope.searchByKeyword = function() {
		$scope.searchByKeywordInternal('designations/all', 'designations', $scope);
	};
	
	$scope.sortBy = function(field, direction) {
		$scope.sortByInternal('designations/all', 'designations', $scope, field, direction);
	};
	
}]);

DesignationModule.controller("DesignationAddCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', function ($scope, $http, $modalInstance, ngToast) {
	$scope.ok = function(isValid) {
		if (isValid) {
			$http.post('designations/new/', $scope.designation)
				.then(function(response) {
					if (response.data.code === 'success') {
						ngToast.create({ className: 'success', content: response.data.message });
					} else if (response.data.code === 'failure') {
						ngToast.create({ className: 'danger', content: response.data.message });
					}
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

DesignationModule.controller("DesignationDetailCtrl", ['$scope', '$http', '$modalInstance', 'designation', function ($scope, $http, $modalInstance, designation) {
	$http.get('designations/data/' + designation.uniqueId)
		.then(function(response) {
			$scope.designation = response.data;
		});
	
	$scope.ok = function() {
		$modalInstance.close();
    };
}]);

DesignationModule.controller("DesignationEditCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', 'designation', function ($scope, $http, $modalInstance, ngToast, designation) {
	$http.get('designations/data/' + designation.uniqueId)
		.then(function(response) {
			$scope.designation = response.data;
		});
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$scope.postUpdateEntity('designations/update/', $scope.designation, $scope.designationFrm, $modalInstance);
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

DesignationModule.controller("DeleteDesignationCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'designation', function ($scope, $http, $location, $modalInstance, ngToast, designation) {
	$scope.ok = function() {
		$scope.deleteEntity('designations/delete/' + designation.uniqueId, $modalInstance);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);