'use strict';

var DepartmentModule = angular.module("DepartmentModule", []);

DepartmentModule.controller("DepartmentListCtrl", ['$scope', '$http', '$modal', '$log', 'HttpService', 'ngToast', function ($scope, $http, $modal, $log, HttpService, ngToast) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.keyword = null;
	$scope.pageSize = 10;
	$scope.history = false;
	$scope.departments = null; 
	$scope.pages = [];
	$scope.sortOrders = {
		"fields": ["name", "createdBy.firstName,createdBy.lastName", "dateCreated", "modifiedBy.firstName,modifiedBy.lastName", "dateModified"],
		"directions": ["asc", "asc", "asc", "asc", "asc" ]
	};
	
	$scope.sortFields = null;
	$scope.sortDirection = null;
	$scope.url = 'departments/all';

	$scope.getDepartments = function() {
		var params = { 
			keyword: $scope.keyword,
			sortFields: $scope.sortFields,
			direction: $scope.direction,
			page: $scope.pageNumber, 
			pageSize: $scope.pageSize,
			history: $scope.history, 
		};

		$scope.readData($scope.url, params, 'departments', $scope);
	};
	
	$scope.getDepartments();
	
	$scope.addDepartment = function() {
		$scope.addEntityInternal('add-department.component.html', 'DepartmentAddCtrl', 'departments/all', 'departments', $scope);
	};
	
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			department: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('detail-department.component.html', 'DepartmentDetailCtrl', 'departments/all', 'departments', $scope, resolveObj);
	};
	
	$scope.editDetail = function(uniqueId) {
		var resolveObj = { 
			department: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.editEntityInternal('edit-department.component.html', 'DepartmentEditCtrl', 'departments/all', 'departments', $scope, resolveObj);
	};
	
	$scope.remove = function(uniqueId) {
		var resolveObj = { 
			department: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.removeEntityInternal('delete-department.component.html', 'DeleteDepartmentCtrl', 'departments/all', 'departments', $scope, resolveObj);
	};

	$scope.prevPage = function() {
		$scope.prevPageInternal('departments/all', 'departments', $scope);
	};
	
	$scope.moveTo = function(page) {
		$scope.moveToInternal('departments/all', 'departments', $scope, page);
	};
	
	$scope.nextPage = function() {
		$scope.nextPageInternal('departments/all', 'departments', $scope);
	};

	$scope.searchByKeyword = function() {
		$scope.searchByKeywordInternal('departments/all', 'departments', $scope);
	};
	
	$scope.sortBy = function(field, direction) {
		$scope.sortByInternal('departments/all', 'departments', $scope, field, direction);
	};
	
}]);

DepartmentModule.controller("DepartmentAddCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', function ($scope, $http, $modalInstance, ngToast) {
	$scope.ok = function(isValid) {
		if (isValid) {
			$http.post('departments/new/', $scope.department)
				.then(function(response) {
					if (response.data.code === 'success') {
						ngToast.create({ className: 'success', content: response.data.message });
					} else if (response.data.code === 'failure') {
						ngToast.create({ className: 'danger', content: response.data.message });
					}
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

DepartmentModule.controller("DepartmentDetailCtrl", ['$scope', '$http', '$modalInstance', 'department', function ($scope, $http, $modalInstance, department) {
	$http.get('departments/data/' + department.uniqueId)
		.then(function(response) {
			$scope.department = response.data;
		});
	
	$scope.ok = function() {
		$modalInstance.close();
    };
}]);

DepartmentModule.controller("DepartmentEditCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', 'department', function ($scope, $http, $modalInstance, ngToast, department) {
	$http.get('departments/data/' + department.uniqueId)
		.then(function(response) {
			$scope.department = response.data;
		});
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$scope.postUpdateEntity('departments/update/', $scope.department, $scope.departmentFrm, $modalInstance);
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

DepartmentModule.controller("DeleteDepartmentCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'department', function ($scope, $http, $location, $modalInstance, ngToast, department) {
	$scope.ok = function() {
		$scope.deleteEntity('departments/delete/' + department.uniqueId, $modalInstance);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);