'use strict';

var RoleModule = angular.module("RoleModule", []);

RoleModule.controller("RoleListCtrl", ['$scope', '$http', '$modal', '$log', 'HttpService', 'ngToast', function ($scope, $http, $modal, $log, HttpService, ngToast) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.keyword = null;
	$scope.pageSize = 10;
	$scope.history = false;
	$scope.roles = null; 
	$scope.pages = [];
	$scope.sortOrders = {
		"fields": ["name", "createdBy.firstName,createdBy.lastName", "dateCreated", "modifiedBy.firstName,modifiedBy.lastName", "dateModified"],
		"directions": ["asc", "asc", "asc", "asc", "asc" ]
	};
	
	$scope.sortFields = null;
	$scope.sortDirection = null;
	$scope.url = 'roles/all';
	
	$scope.getRoles = function () {
		var params = { 
			keyword: $scope.keyword,
			sortFields: $scope.sortFields,
			direction: $scope.direction,
			page: $scope.pageNumber, 
			pageSize: $scope.pageSize,
			history: $scope.history, 
		};

		$scope.readData($scope.url, params, 'roles', $scope);
	};

	$scope.getRoles();
	
	$scope.addRole = function() {
		$scope.addEntityInternal('add-role.component.html', 'RoleAddCtrl', 'roles/all', 'roles', $scope);
	};
	
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			role: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('detail-role.component.html', 'RoleDetailCtrl', 'roles/all', 'roles', $scope, resolveObj);
	};
	
	$scope.editDetail = function(role) {
		if (role.frozen === 'true') {
			ngToast.create({ className: 'danger', content: 'You cannot edit this role. because it is frozon.' });
			return false;
		}
		
		var resolveObj = { 
			role: function() {
				return {
					uniqueId: role.uniqueId
				}
			}
		};
		
		$scope.editEntityInternal('edit-role.component.html', 'RoleEditCtrl', 'roles/all', 'roles', $scope, resolveObj);
	};
	
	$scope.remove = function(uniqueId) {
		var resolveObj = { 
			role: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.removeEntityInternal('delete-role.component.html', 'DeleteRoleCtrl', 'roles/all', 'roles', $scope, resolveObj);
	};

	$scope.prevPage = function() {
		$scope.prevPageInternal('roles/all', 'roles', $scope);
	};
	
	$scope.moveTo = function(page) {
		$scope.moveToInternal('roles/all', 'roles', $scope, page);
	};
	
	$scope.nextPage = function() {
		$scope.nextPageInternal('roles/all', 'roles', $scope);
	};

	$scope.searchByKeyword = function() {
		$scope.searchByKeywordInternal('roles/all', 'roles', $scope);
	};
	
	$scope.sortBy = function(field, direction) {
		$scope.sortByInternal('roles/all', 'roles', $scope, field, direction);
	};
	
}]);

RoleModule.controller("RoleAddCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', function ($scope, $http, $modalInstance, ngToast) {
	$scope.ok = function(isValid) {
		if (isValid) {
			$http.post('roles/new/', $scope.role)
				.then(function(response) {
					if (response.data.code === 'success') {
						ngToast.create({ className: 'success', content: response.data.message });
					} else if (response.data.code === 'failure') {
						ngToast.create({ className: 'danger', content: response.data.message });
					}
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

RoleModule.controller("RoleDetailCtrl", ['$scope', '$http', '$modalInstance', 'role', function ($scope, $http, $modalInstance, role) {
	$http.get('roles/data/' + role.uniqueId)
		.then(function(response) {
			$scope.role = response.data;
		});
	
	$scope.ok = function() {
		$modalInstance.close();
    };
}]);

RoleModule.controller("RoleEditCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', 'role', function ($scope, $http, $modalInstance, ngToast, role) {
	$http.get('roles/data/' + role.uniqueId)
		.then(function(response) {
			$scope.role = response.data;
		});
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$scope.postUpdateEntity('roles/update/', $scope.role, $scope.roleFrm, $modalInstance);
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

RoleModule.controller("DeleteRoleCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'role', function ($scope, $http, $location, $modalInstance, ngToast, role) {
	$scope.ok = function() {
		$scope.deleteEntity('roles/delete/' + role.uniqueId, $modalInstance);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);