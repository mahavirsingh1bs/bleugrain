'use strict';

var CustomerModule = angular.module("CustomerModule", []);

CustomerModule.controller("CustomerListCtrl", ['$scope', '$http', '$modal', '$log', 'HttpService', 'ngToast', function ($scope, $http, $modal, $log, HttpService, ngToast) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.keyword = null;
	$scope.pageSize = 10;
	$scope.history = false;
	$scope.customers = null; 
	$scope.pages = [];
	$scope.sortOrders = {
		"fields": ["firstName,lastName", "phoneNo", "email", "createdBy.firstName,createdBy.lastName", "dateModified", "modifiedBy.firstName,modifiedBy.lastName", "dateModified"],
		"directions": ["asc", "asc", "asc", "asc", "asc" ]
	};
	
	$scope.sortFields = $scope.sortOrders['fields'][0];
	$scope.sortDirection = $scope.sortOrders['directions'][0];
	$scope.url = 'customers/all';
	
	$scope.getCustomers = function() {
		var params = { 
			keyword: $scope.keyword,
			sortFields: $scope.sortFields,
			direction: $scope.direction,
			page: $scope.pageNumber, 
			pageSize: $scope.pageSize,
			history: $scope.history, 
		};

		$scope.readData($scope.url, params, 'customers', $scope);
	};
	
	$scope.getCustomers();
	
	$scope.addCustomer = function() {
		$scope.addEntityInternal('assets/js/app/customers/add-customer.component.html', 'CustomerAddCtrl', 'customers/all', 'customers', $scope);
	};
	
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			customer: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('assets/js/app/customers/detail-customer.component.html', 'CustomerDetailCtrl', 'customers/all', 'customers', $scope, resolveObj);
	};
	
	$scope.editDetail = function(uniqueId) {
		var resolveObj = { 
			customer: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.editEntityInternal('assets/js/app/customers/edit-customer.component.html', 'CustomerEditCtrl', 'customers/all', 'customers', $scope, resolveObj);
	};
	
	$scope.remove = function(uniqueId) {
		var resolveObj = { 
			customer: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.removeEntityInternal('delete-customer.component.html', 'DeleteCustomerCtrl', 'customers/all', 'customers', $scope, resolveObj);
	};

	$scope.prevPage = function() {
		$scope.prevPageInternal('customers/all', 'customers', $scope);
	};
	
	$scope.moveTo = function(page) {
		$scope.moveToInternal('customers/all', 'customers', $scope, page);
	};
	
	$scope.nextPage = function() {
		$scope.nextPageInternal('customers/all', 'customers', $scope);
	};

	$scope.searchByKeyword = function() {
		$scope.searchByKeywordInternal('customers/all', 'customers', $scope);
	};
	
	$scope.sortBy = function(field, direction) {
		$scope.sortByInternal('customers/all', 'customers', $scope, field, direction);
	};
	
}]);

CustomerModule.controller("CustomerAddCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'usSpinnerService', 'FormUtils', function ($scope, $http, $location, $modalInstance, ngToast, usSpinnerService, FormUtils) {
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
		});
	
	$scope.ok = function(isValid) {
		if (isValid) {
			var formData = new FormData();
			
			formData.append('customer', new Blob([JSON.stringify($scope.customer)], { type: "application/json" }));
			formData.append('customerImage', $scope.customerImage);
			$scope.postCreateEntity('customers/new', formData, $scope.customerFrm, $modalInstance);
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

CustomerModule.controller("CustomerDetailCtrl", ['$scope', '$http', '$modalInstance', 'customer', function ($scope, $http, $modalInstance, customer) {
	$http.get('customers/data/' + customer.uniqueId)
		.then(function(response) {
			$scope.customer = response.data;
		});
	
	$scope.close = function() {
		$modalInstance.close();
    };
}]);

CustomerModule.controller("CustomerEditCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', 'customer', function ($scope, $http, $modalInstance, ngToast, customer) {
	
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
		});
	
	$http.get('customers/data/' + customer.uniqueId)
		.then(function(response) {
			$scope.customer = response.data;
		});
	
	$scope.ok = function() {
		$scope.postUpdateEntity('customers/update/', $scope.customer, $scope.customerFrm, $modalInstance);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

CustomerModule.controller("DeleteCustomerCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'customer', function ($scope, $http, $location, $modalInstance, ngToast, customer) {
	$scope.ok = function() {
		$scope.deleteEntity('customers/delete/' + customer.uniqueId, $modalInstance);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);