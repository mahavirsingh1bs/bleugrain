'use strict';

var PricingModule = angular.module("PricingModule", ['ui.bootstrap']);

PricingModule.controller("PricingListCtrl", ['$scope', '$http', '$modal', '$log', 'ngToast', 'HttpService', 'UtilityService', function ($scope, $http, $modal, $log, ngToast, HttpService, UtilityService) {
	$scope.pageNumber = 0;
	$scope.pageSize = 12;
	$scope.sizeText = '12';
	$scope.totalPage = 0;
	$scope.consumerProducts = false;
	$scope.keyword = null;
	$scope.pages = [];
	
	$scope.products = null;
	$scope.demands = null;
	
	$scope.backedProduct = {};
	
	$scope.url = 'pricing/products';
	
	$scope.getProducts = function() {
		var params = { 
			keyword: $scope.keyword,
			page: $scope.pageNumber, 
			pageSize: $scope.pageSize,
			categoryId: $scope.category.uniqueId,
			consumerProducts: $scope.consumerProducts
		};
		
		$scope.readData($scope.url, params, 'products', $scope);
	};
	
	$http.get('categories/')
		.then(function(response) {
			$scope.categories = response.data;
			$scope.category = $scope.categories[0];
			$scope.getProducts();
		});
	
	$http.get('currencies/all').success(function(response) {
			$scope.currencies = response;
		}).error(function(data) {
			console.log('error occurred');
		});
	
	$http.get('units/all').success(function(response) {
			$scope.units = response;
		}).error(function(data) {
			console.log('error occurred');
		});
		
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			product: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('assets/js/app/products/detail-product.component.html', 'ProductDetailCtrl', $scope.url, 'products', $scope, resolveObj);
	};
	
	$scope.updatePageSize = function(pageSize, sizeText) {
		$scope.pageNumber = 0;
		$scope.pageSize = pageSize;
		$scope.sizeText = sizeText;
		$scope.getProducts();
	}
   	
	$scope.prevPage = function() {
   		$scope.pageNumber = $scope.pageNumber - 1;
   		$scope.getProducts();
   	};
   	
   	$scope.moveTo = function(page) {
   		$scope.pageNumber = page - 1;
   		$scope.getProducts();
   	};
   	
   	$scope.nextPage = function() {
   		$scope.pageNumber = $scope.pageNumber + 1;
   		$scope.getProducts();
   	};
   	
	$scope.setPriceCurrency = function(product, priceCurrency) {
		product.priceCurrency = priceCurrency;
	};
	
	$scope.setPriceUnit = function(product, priceUnit) {
		product.priceUnit = priceUnit;
	};
	
	$scope.editPrice = function(product) {
		$scope.products.forEach(function(p, index) {
			p.editing = false;
		});
		angular.copy(product, $scope.backedProduct);
		product.editing = true;		
	}
	
	$scope.done = function(product) {
		var updatePriceFrm = {
			uniqueId: product.uniqueId,
			price: product.price,
			priceCurrencyId: product.priceCurrency.id,
			priceUnitId: product.priceUnit.id
		};
		
		$http.post('products/update/price', updatePriceFrm)
			.then(function(response) {
				if (response.data.code === 'success') {
					ngToast.create({ className: 'success', content: response.data.message });
					product.editing = false;
				} else {
					ngToast.create({ className: 'danger', content: response.data.message });
				}
			});
	}
	
	$scope.cancel = function(product) {
		angular.copy($scope.backedProduct, product);
		product.editing = false;
	}
	
	$scope.onCategoryChange = function(category) {
		$scope.category = category;
		$scope.pageNumber = 0;
		$scope.getProducts();
	};
	
	$scope.fetchConsumerProducts = function (consumerProducts) {
		$scope.consumerProducts = consumerProducts;
		$scope.pageNumber = 0;
		$scope.getProducts();
	}
	
	$scope.reset = function () {
		$scope.getProducts();
	}
	
}]);