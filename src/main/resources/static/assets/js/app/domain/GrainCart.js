function GrainCart(items) {
	this.cartItems = items || [];
}

GrainCart.prototype.addItem = function(product) {
	if (this.getItem(product.id) != null) {
		alert('Item already in cart!');
		return false;
	}
	if (product != null) {
		this.cartItems.push(product);
	}
};

GrainCart.prototype.getItem = function(id) {
	for (var i = 0; i < this.cartItems.length; i++) {
		if (this.cartItems[i].id == id) {
			return this.cartItems[i];
		}
	}
};

GrainCart.prototype.getItemAt = function(index) {
	if (this.cartItems[index] != null) {
		return this.cartItems[index];
	}
	return null;
};

GrainCart.prototype.totalItem = function() {
	return this.cartItems.length;
};

GrainCart.prototype.removeItem = function(id) {
	for (var i = 0; i < this.cartItems.length; i++) {
		if (this.cartItems[i].id == id) {
			this.cartItems.splice(i, 1);
		}
	}
};

GrainCart.prototype.getItems = function() {
	return this.cartItems;
};

GrainCart.prototype.getTotalPrice = function() {
	var totalPrice = 0;
	for (var i = 0; i < this.cartItems.length; i++) {
		var quantity = this.cartItems[i].quantity;
		var price = this.cartItems[i].price;
		quantity = quantity.replace(/,/g , "");
		price = price.replace(/,/g , "");
		totalPrice = totalPrice + parseInt(price) * parseInt(quantity);
	};
	return totalPrice;
};

GrainCart.prototype.getAdvancedPayment = function() {
	var advancedPayment = this.getTotalPrice() * 0.10 / 100;
	return advancedPayment;
};

GrainCart.prototype.getCurrency = function() {
	return 'INR';
};

GrainCart.prototype.getTotalQuantity = function() {
	var totalQuantity = 0;
	for (var i = 0; i < this.cartItems.length; i++) {
		totalQuantity = totalQuantity + this.cartItems[i].quantity;
	}
	return totalQuantity;
};

GrainCart.prototype.getTotalItems = function() {
	return this.cartItems.length;
};

var grainCart = new GrainCart();