var CartServiceModule = angular.module('CartServiceModule', []);

CartServiceModule.factory('GrainCartService', ['$http', function($http) {
	var grainCartService = {};
	
	grainCartService.findGrainCart = function() {
		return $http.get('graincart/')
			.then(function(response) {
				return response.data;
			});
	};
	
	grainCartService.addToCart = function(uniqueId) {
		return $http.post('graincart/addToCart', uniqueId)
			.then(function(response) {
				return response.data;
			});
	};
	
	grainCartService.addToConsumerCart = function(product) {
		return $http.post('graincart/addToConsumerCart', product)
			.then(function(response) {
				return response.data;
			});
	};
	
	grainCartService.removeFromCart = function(uniqueId) {
		return $http.post('graincart/removeFromCart', uniqueId)
			.then(function(response) {
				return response.data;
			});
	};
	
	grainCartService.removeFromConsumerCart = function(uniqueId) {
		return $http.post('graincart/removeFromConsumerCart', uniqueId)
			.then(function(response) {
				return response.data;
			});
	};
	
	return grainCartService;
}]);