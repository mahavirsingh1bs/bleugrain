var CoreService = angular.module('CoreService');

CoreService.factory('SettingsService', ['$http', function($http) {
	var settingsService = {};
	
	settingsService.findCountrySettings = function() {
		return $http.get('setting/country')
			.then(function(response) {
				return response.data;
			});
	};
	
	return settingsService;
}]);