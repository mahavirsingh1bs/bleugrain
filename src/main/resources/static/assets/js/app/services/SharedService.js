var SharedServiceModule = angular.module('SharedServiceModule', []);

SharedServiceModule.factory('SharedProperties', [function() {
	var companyId = -1;
	var userId = -1;
	var requestedBy = '';
	var lastUrl = '';
	
	return {
		getCompanyId: function() {
			return companyId;
		},
		setCompanyId: function(compId) {
			companyId = compId;
		},
		getUserId: function() {
			return userId;
		},
		setUserId: function(usrId) {
			userId = usrId;
		},
		getRequestedBy: function() {
			return requestedBy;
		},
		setRequestedBy: function(requestdBy) {
			requestedBy = requestdBy;
		},
		getLastUrl: function() {
			return lastUrl;
		},
		setLastUrl: function(url) {
			lastUrl = url;
		}
	};
}]);

SharedServiceModule.factory('CommonMethods', ['$http', '$location', 'SharedProperties', function($http, $location, SharedProperties) {
	return {
		addNew: function(addUrl) {
			SharedProperties.setLastUrl($location.path());
			$scope.apply($location.path(addUrl));
		},
		edit: function(editUrl) {
			SharedProperties.setLastUrl($location.path());
			$location.path(editUrl);
		}, 
		submit: function(submitUrl, entity) {
			var response = $http.post(contextPath + submitUrl, entity);
			response.success(function(data, status, headers, config) {
				$location.path(SharedProperties.getLastUrl());
			});
			
			response.error(function(data, status, headers, config) {
				console.log("Exception Details: " + data);
			});
		}, 
		cancel: function() {
			$location.path(SharedProperties.getLastUrl());
		}
	};
}]);

SharedServiceModule.factory('UserSession', [function() {
	var currentUser = null;
	
	return {
		getCurrentUser: function() {
			return currentUser;
		},
		setCurrentUser: function(currtUser) {
			currentUser = currtUser;
		}
	};
}]);