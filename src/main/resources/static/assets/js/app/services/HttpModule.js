var HttpModule = angular.module('HttpModule', []);

HttpModule.factory('HttpService', ['$http', function($http) {
	var httpService = {};
	
	httpService.readData = function(url, params) {
		return $http.get(url, { params : params })
			.then(function(response) {
				return response.data;
			});
	};
	
	return httpService;
}]);