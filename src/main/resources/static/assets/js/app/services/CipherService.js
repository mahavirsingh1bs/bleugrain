angular.module("CipherModule", []).service("CipherService", function() {
 
    /*
     * Encrypt a message with a passphrase or password
     *
     * @param    string message
     * @param    string password
     * @return   object
     */
    this.encrypt = function(message, password) {
        // var salt = forge.random.getBytesSync(128);
    	var salt = '3FF2EC019C627B945225DEBAD71A01B6985FE84C95A70EB132882F88C0A59A55';
    	var iterationCount = 4;
    	var keySize = 16;
        var key = forge.pkcs5.pbkdf2(password, salt, iterationCount, keySize);
        // var iv = forge.random.getBytesSync(16);
        var iv = 'F27D5C9927726BCEFE7510B1BDD3D137';
        var cipher = forge.cipher.createCipher('AES-CBC', key);
        cipher.start({iv: iv});
        cipher.update(forge.util.createBuffer(message));
        cipher.finish();
        var cipherText = forge.util.encode64(cipher.output.getBytes());
        return { cipher_text: cipherText, salt: forge.util.encode64(salt), iv: forge.util.encode64(iv) };
    }
 
    /*
     * Decrypt cipher text using a password or passphrase and a corresponding salt and iv
     *
     * @param    string (Base64) cipherText
     * @param    string password
     * @param    string (Base64) salt
     * @param    string (Base64) iv
     * @return   string
     */
    this.decrypt = function(cipherText, password, salt, iv) {
        var key = forge.pkcs5.pbkdf2(password, forge.util.decode64(salt), 4, 16);
        var decipher = forge.cipher.createDecipher('AES-CBC', key);
        decipher.start({iv: forge.util.decode64(iv)});
        decipher.update(forge.util.createBuffer(forge.util.decode64(cipherText)));
        decipher.finish();
        return decipher.output.toString();
    }
 
});