var AccessCtrlModule = angular.module('AccessCtrlModule', ['ngCookies']);

AccessCtrlModule.factory('AuthService', ['$rootScope', '$http', '$cookies', 'Session', 'CipherService', 'AUTH_EVENTS', 'CIPHER_PHRASE', function($rootScope, $http, $cookies, Session, CipherService, AUTH_EVENTS, CIPHER_PHRASE) {
	var authService = {};
	
	authService.login = function (credentials) {
		var ciphered = CipherService.encrypt(credentials.username, CIPHER_PHRASE.password);
		console.log(ciphered);
		console.log(CipherService.decrypt(ciphered.cipher_text, CIPHER_PHRASE.password, ciphered.salt, ciphered.iv));
	    return $http
	      .post('login', credentials)
	      .then(function (res) {
	    	  if (!angular.isUndefined(res.data.user)) {
		    	  Session.create(res.data.id, res.data.user);
		    	  return res.data.user;
	    	  } else {
	    		  return null;
	    	  }
	      });
	  };
	
    authService.register = function (registerUrl, entity) {
	    return $http.post(registerUrl, entity)
	      .then(function (response) {
	        if (response.data.code !== 'error' || response.data.code !== 'failure') {
	    		Session.create(response.data.id, response.data.user);
	    	}
	    	return response.data;
	      });
	  };
  
    authService.verifyAndProceed = function (verifyUrl, data) {
	    return $http.put(verifyUrl, data)
	      .then(function (res) {
	    	  return res.data;
	      });
	  };
	
    authService.createSession = function () {
    	var userData = Session.fetchUserData();
    	Session.create(userData.id, userData.user);
	  };
	
	authService.logout = function (credentials) {
		return $http.post('logout', {}).then(function() {
			Session.destroy();
		});
	};
	
	authService.currentUser = function() {
		return Session.getCurrentUser(function(currentUser) {
			return currentUser;
		});
	}
	
	authService.isAuthenticated = function() {
		return !!Session.currentUser;
	};
	
	authService.andIsAuthorized = function(authorizedRoles) {
		var isAuthorized = true;
		angular.forEach(authorizedRoles, function(authorizedRole, index) {
			isAuthorized = isAuthorized && authService.isAuthorized(authorizedRole)
		});
		return isAuthorized;
	}
	
	authService.orIsAuthorized = function(authorizedRoles) {
		var isAuthorized = false;
		angular.forEach(authorizedRoles, function(authorizedRole, index) {
			isAuthorized = isAuthorized || authService.isAuthorized(authorizedRole)
		});
		return isAuthorized;
	}
	
	authService.isAuthorized = function(authorizedRoles) {
		if (!angular.isArray(authorizedRoles)) {
			authorizedRoles = [ authorizedRoles ];
		}
		
		if (!authService.isAuthenticated()) {
			return false;
		}
		
		var user = Session.currentUser;
		if (user === null) {
			return false;
		} else {
			for (var index = 0; index < user.group.roles.length; index++) {
				var role = user.group.roles[index];
				if (authorizedRoles.indexOf(role.name) > -1) {
					return true;
				}
			}
		}
		
		return false;
	};
	
	return authService;
}]);

AccessCtrlModule.factory('Session', ['$http', '$cookies', function($http, $cookies) {
	var session = {};
	session.sessionId = null;
	session.currentUser = null;
	
	session.create = function(id, user) {
		session.sessionId = id;
		session.currentUser = user;
	};
	
	session.getCurrentUser = function () {
		if (angular.isObject(session.currentUser)) {
			return session.currentUser;
		}
		return $http
	      .get('currentUser')
	      .then(function (res) {
	    	  session.currentUser = res.data;
	    	  return session.currentUser;
	      }, function() {
	    	  return null;
	      });
	};

	session.destroy = function() {
		session.currentUser = null;
	};
	
	session.storeUserData = function (usrData) {
		session.currentUser = usrData;
	};
	
	session.fetchUserData = function () {
		var usrData = { id: session.sessionId, user: session.currentUser };
		session.sessionId = null;
		session.currentUser = null;
		return usrData;
	};
	
	return session;
}]);