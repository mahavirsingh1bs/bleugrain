var UtilityModule = angular.module('UtilityModule');

UtilityModule.factory('UtilityService', [function() {
	var utilityService = {};
	
	utilityService.isUndefinedOrNull = function(val) {
	    return angular.isUndefined(val) || val === null 
	}
	
	return utilityService;
}]);