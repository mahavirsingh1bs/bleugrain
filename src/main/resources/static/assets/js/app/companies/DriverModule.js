'use strict';

var DriverModule = angular.module("DriverModule", []);

DriverModule.controller("DriverListCtrl", ['$scope', '$http', '$modal', '$log', 'HttpService', 'ngToast', function ($scope, $http, $modal, $log, HttpService, ngToast) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.keyword = null;
	$scope.pageSize = 10;
	$scope.history = false;
	$scope.drivers = null; 
	$scope.pages = [];
	$scope.sortOrders = {
		"fields": ["firstName,lastName", "phoneNo", "email", "createdBy.firstName,createdBy.lastName", "dateModified", "modifiedBy.firstName,modifiedBy.lastName", "dateModified"],
		"directions": ["asc", "asc", "asc", "asc", "asc" ]
	};
	
	$scope.sortFields = $scope.sortOrders['fields'][0];
	$scope.sortDirection = $scope.sortOrders['directions'][0];
	$scope.url = 'drivers/all';
	
	$scope.getDrivers = function() {
		var params = { 
			keyword: $scope.keyword,
			sortFields: $scope.sortFields,
			direction: $scope.direction,
			page: $scope.pageNumber, 
			pageSize: $scope.pageSize,
			history: $scope.history, 
		};
		
		$scope.readData($scope.url, params, 'drivers', $scope);
	};
	
	$scope.getDrivers();
	
	$scope.addDriver = function() {
		var resolveObj = { 
			currentUser: function() {
				return $scope.currentUser;
			}
		};
		
		$scope.addEntityInternal('assets/js/app/companies/add-driver.component.html', 'DriverAddCtrl', 'drivers/all', 'drivers', $scope, resolveObj);
	};
	
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			driver: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('assets/js/app/companies/detail-driver.component.html', 'DriverDetailCtrl', 'drivers/all', 'drivers', $scope, resolveObj);
	};
	
	$scope.editDetail = function(uniqueId) {
		var resolveObj = { 
			driver: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.editEntityInternal('assets/js/app/companies/edit-driver.component.html', 'DriverEditCtrl', 'drivers/all', 'drivers', $scope, resolveObj);
	};
	
	$scope.remove = function(uniqueId) {
		var resolveObj = { 
			driver: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.removeEntityInternal('delete-driver.component.html', 'DeleteDriverCtrl', 'drivers/all', 'drivers', $scope, resolveObj);
	};

	$scope.prevPage = function() {
		$scope.prevPageInternal('drivers/all', 'drivers', $scope);
	};
	
	$scope.moveTo = function(page) {
		$scope.moveToInternal('drivers/all', 'drivers', $scope, page);
	};
	
	$scope.nextPage = function() {
		$scope.nextPageInternal('drivers/all', 'drivers', $scope);
	};

	$scope.searchByKeyword = function() {
		$scope.searchByKeywordInternal('drivers/all', 'drivers', $scope);
	};
	
	$scope.sortBy = function(field, direction) {
		$scope.sortByInternal('drivers/all', 'drivers', $scope, field, direction);
	};
	
}]);

DriverModule.controller("DriverAddCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'usSpinnerService', 'FormUtils', 'currentUser', function ($scope, $http, $location, $modalInstance, ngToast, usSpinnerService, FormUtils, currentUser) {
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
		});

	$scope.searchCompany = function(keywords) {
		return $http.get('companies/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.selectCompany = function(item, model, label) {
		$scope.driver.companyId = item.uniqueId;
	}
	
	$scope.ok = function(isValid) {
		if (isValid) {
			if ($scope.isAuthorized('ROLE_COMPANY_ADMIN')) {
				$scope.driver.companyId = currentUser.company.uniqueId;
			}
			
			var formData = new FormData();
			
			formData.append('driver', new Blob([JSON.stringify($scope.driver)], { type: "application/json" }));
			formData.append('driverImage', $scope.driverImage);
			$scope.postCreateEntity('drivers/new', formData, $scope.driverFrm, $modalInstance);
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

DriverModule.controller("DriverDetailCtrl", ['$scope', '$http', '$modalInstance', 'driver', function ($scope, $http, $modalInstance, driver) {
	$http.get('drivers/data/' + driver.uniqueId)
		.then(function(response) {
			$scope.driver = response.data;
		});
	
	$scope.close = function() {
		$modalInstance.close();
    };
}]);

DriverModule.controller("DriverEditCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', 'driver', function ($scope, $http, $modalInstance, ngToast, driver) {
	
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
		});
	
	$http.get('drivers/data/' + driver.uniqueId)
		.then(function(response) {
			$scope.driver = response.data;
		});
	
	$scope.searchCompany = function(keywords) {
		return $http.get('companies/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.selectCompany = function(item, model, label) {
		$scope.driver.companyId = item.uniqueId;
	}
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$scope.postUpdateEntity('drivers/update/', $scope.driver, $scope.driverFrm, $modalInstance);
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

DriverModule.controller("DeleteDriverCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'driver', function ($scope, $http, $location, $modalInstance, ngToast, driver) {
	$scope.ok = function() {
		$scope.deleteEntity('drivers/delete/' + driver.uniqueId, $modalInstance);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);