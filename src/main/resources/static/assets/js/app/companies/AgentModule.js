'use strict';

var AgentModule = angular.module("AgentModule", []);

AgentModule.controller("AgentListCtrl", ['$scope', '$http', '$modal', '$log', 'HttpService', 'ngToast', function ($scope, $http, $modal, $log, HttpService, ngToast) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.keyword = null;
	$scope.pageSize = 10;
	$scope.history = false;
	$scope.agents = null; 
	$scope.pages = [];
	$scope.sortOrders = {
		"fields": ["firstName,lastName", "phoneNo", "email", "createdBy.firstName,createdBy.lastName", "dateModified", "modifiedBy.firstName,modifiedBy.lastName", "dateModified"],
		"directions": ["asc", "asc", "asc", "asc", "asc" ]
	};
	
	$scope.sortFields = $scope.sortOrders['fields'][0];
	$scope.sortDirection = $scope.sortOrders['directions'][0];
	$scope.url = 'agents/all';
	
	$scope.getAgents = function() {
		var params = { 
			keyword: $scope.keyword,
			sortFields: $scope.sortFields,
			direction: $scope.direction,
			page: $scope.pageNumber, 
			pageSize: $scope.pageSize,
			history: $scope.history, 
		};
		
		$scope.readData($scope.url, params, 'agents', $scope);
	};
	
	$scope.getAgents();
	
	$scope.addAgent = function() {
		$scope.addEntityInternal('assets/js/app/companies/add-agent.component.html', 'AgentAddCtrl', 'agents/all', 'agents', $scope);
	};
	
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			agent: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('assets/js/app/companies/detail-agent.component.html', 'AgentDetailCtrl', 'agents/all', 'agents', $scope, resolveObj);
	};
	
	$scope.editDetail = function(uniqueId) {
		var resolveObj = { 
			agent: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.editEntityInternal('assets/js/app/companies/edit-agent.component.html', 'AgentEditCtrl', 'agents/all', 'agents', $scope, resolveObj);
	};
	
	$scope.remove = function(uniqueId) {
		var resolveObj = { 
			agent: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.removeEntityInternal('delete-agent.component.html', 'DeleteAgentCtrl', 'agents/all', 'agents', $scope, resolveObj);
	};

	$scope.prevPage = function() {
		$scope.prevPageInternal('agents/all', 'agents', $scope);
	};
	
	$scope.moveTo = function(page) {
		$scope.moveToInternal('agents/all', 'agents', $scope, page);
	};
	
	$scope.nextPage = function() {
		$scope.nextPageInternal('agents/all', 'agents', $scope);
	};

	$scope.searchByKeyword = function() {
		$scope.searchByKeywordInternal('agents/all', 'agents', $scope);
	};
	
	$scope.sortBy = function(field, direction) {
		$scope.sortByInternal('agents/all', 'agents', $scope, field, direction);
	};
	
}]);

AgentModule.controller("AgentAddCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'usSpinnerService', 'FormUtils', function ($scope, $http, $location, $modalInstance, ngToast, usSpinnerService, FormUtils) {
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
		});

	$scope.searchCompany = function(keywords) {
		return $http.get('companies/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.selectCompany = function(item, model, label) {
		$scope.agent.companyId = item.uniqueId;
	}
	
	$scope.ok = function(isValid) {
		if (isValid) {
			var formData = new FormData();
			
			formData.append('agent', new Blob([JSON.stringify($scope.agent)], { type: "application/json" }));
			formData.append('agentImage', $scope.agentImage);
			$scope.postCreateEntity('agents/new', formData, $scope.agentFrm, $modalInstance);
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

AgentModule.controller("AgentDetailCtrl", ['$scope', '$http', '$modalInstance', 'agent', function ($scope, $http, $modalInstance, agent) {
	$http.get('agents/data/' + agent.uniqueId)
		.then(function(response) {
			$scope.agent = response.data;
		});
	
	$scope.close = function() {
		$modalInstance.close();
    };
}]);

AgentModule.controller("AgentEditCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', 'agent', function ($scope, $http, $modalInstance, ngToast, agent) {
	
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
		});
	
	$http.get('agents/data/' + agent.uniqueId)
		.then(function(response) {
			$scope.agent = response.data;
		});
	
	$scope.searchCompany = function(keywords) {
		return $http.get('companies/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.selectCompany = function(item, model, label) {
		$scope.agent.companyId = item.uniqueId;
	}
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$scope.postUpdateEntity('agents/update/', $scope.agent, $scope.agentFrm, $modalInstance);
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

AgentModule.controller("DeleteAgentCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'agent', function ($scope, $http, $location, $modalInstance, ngToast, agent) {
	$scope.ok = function() {
		$scope.deleteEntity('agents/delete/' + agent.uniqueId, $modalInstance);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);