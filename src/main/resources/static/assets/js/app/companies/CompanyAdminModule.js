'use strict';

var CompanyAdminModule = angular.module("CompanyAdminModule", []);

CompanyAdminModule.controller("CompanyAdminListCtrl", ['$scope', '$http', '$modal', '$log', 'HttpService', 'ngToast', function ($scope, $http, $modal, $log, HttpService, ngToast) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.keyword = null;
	$scope.pageSize = 10;
	$scope.history = false;
	$scope.companyAdmins = null; 
	$scope.pages = [];
	$scope.sortOrders = {
		"fields": ["firstName,lastName", "phoneNo", "email", "createdBy.firstName,createdBy.lastName", "dateModified", "modifiedBy.firstName,modifiedBy.lastName", "dateModified"],
		"directions": ["asc", "asc", "asc", "asc", "asc" ]
	};
	
	$scope.sortFields = $scope.sortOrders['fields'][0];
	$scope.sortDirection = $scope.sortOrders['directions'][0];
	$scope.url = 'companyAdmins/all';
	
	$scope.getCompanyAdmins = function () {
		var params = { 
			keyword: $scope.keyword,
			sortFields: $scope.sortFields,
			direction: $scope.direction,
			page: $scope.pageNumber, 
			pageSize: $scope.pageSize,
			history: $scope.history, 
		};

		$scope.readData($scope.url, params, 'companyAdmins', $scope);
	};
	
	$scope.getCompanyAdmins();
	
	$scope.addCompanyAdmin = function() {
		$scope.addEntityInternal('assets/js/app/companies/add-company-admin.component.html', 'CompanyAdminAddCtrl', 'companyAdmins/all', 'companyAdmins', $scope);
	};
	
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			companyAdmin: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('assets/js/app/companies/detail-company-admin.component.html', 'CompanyAdminDetailCtrl', 'companyAdmins/all', 'companyAdmins', $scope, resolveObj);
	};
	
	$scope.editDetail = function(uniqueId) {
		var resolveObj = { 
			companyAdmin: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.editEntityInternal('assets/js/app/companies/edit-company-admin.component.html', 'CompanyAdminEditCtrl', 'companyAdmins/all', 'companyAdmins', $scope, resolveObj);
	};
	
	$scope.remove = function(uniqueId) {
		var resolveObj = { 
			companyAdmin: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.removeEntityInternal('delete-company-admin.component.html', 'DeleteCompanyAdminCtrl', 'companyAdmins/all', 'companyAdmins', $scope, resolveObj);
	};

	$scope.prevPage = function() {
		$scope.prevPageInternal('companyAdmins/all', 'companyAdmins', $scope);
	};
	
	$scope.moveTo = function(page) {
		$scope.moveToInternal('companyAdmins/all', 'companyAdmins', $scope, page);
	};
	
	$scope.nextPage = function() {
		$scope.nextPageInternal('companyAdmins/all', 'companyAdmins', $scope);
	};

	$scope.searchByKeyword = function() {
		$scope.searchByKeywordInternal('companyAdmins/all', 'companyAdmins', $scope);
	};
	
	$scope.sortBy = function(field, direction) {
		$scope.sortByInternal('companyAdmins/all', 'companyAdmins', $scope, field, direction);
	};
	
}]);

CompanyAdminModule.controller("CompanyAdminAddCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'usSpinnerService', 'FormUtils', function ($scope, $http, $location, $modalInstance, ngToast, usSpinnerService, FormUtils) {
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
		});

	$scope.searchCompany = function(keywords) {
		return $http.get('companies/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.selectCompany = function(item, model, label) {
		$scope.companyAdmin.companyId = item.uniqueId;
	}
	
	$scope.ok = function(isValid) {
		if (isValid) {
			var formData = new FormData();
			
			formData.append('companyAdmin', new Blob([JSON.stringify($scope.companyAdmin)], { type: "application/json" }));
			formData.append('companyAdminImage', $scope.companyAdminImage);
			$scope.postCreateEntity('companyAdmins/new', formData, $scope.companyAdminFrm, $modalInstance);
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

CompanyAdminModule.controller("CompanyAdminDetailCtrl", ['$scope', '$http', '$modalInstance', 'companyAdmin', function ($scope, $http, $modalInstance, companyAdmin) {
	$http.get('companyAdmins/data/' + companyAdmin.uniqueId)
		.then(function(response) {
			$scope.companyAdmin = response.data;
		});
	
	$scope.close = function() {
		$modalInstance.close();
    };
}]);

CompanyAdminModule.controller("CompanyAdminEditCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', 'companyAdmin', function ($scope, $http, $modalInstance, ngToast, companyAdmin) {
	
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
		});
	
	$http.get('companyAdmins/data/' + companyAdmin.uniqueId)
		.then(function(response) {
			$scope.companyAdmin = response.data;
		});
	
	$scope.searchCompany = function(keywords) {
		return $http.get('companies/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.selectCompany = function(item, model, label) {
		$scope.companyAdmin.companyId = item.uniqueId;
	}
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$scope.postUpdateEntity('companyAdmins/update/', $scope.companyAdmin, $scope.companyAdminFrm, $modalInstance);
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

CompanyAdminModule.controller("DeleteCompanyAdminCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'companyAdmin', function ($scope, $http, $location, $modalInstance, ngToast, companyAdmin) {
	$scope.ok = function() {
		$scope.deleteEntity('companyAdmins/delete/' + companyAdmin.uniqueId, $modalInstance);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);