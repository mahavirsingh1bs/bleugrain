'use strict';

var CompanyModule = angular.module("CompanyModule", []);

CompanyModule.controller("CompanyListCtrl", ['$scope', '$http', '$modal', '$log', 'HttpService', 'ngToast', function ($scope, $http, $modal, $log, HttpService, ngToast) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.keyword = null;
	$scope.pageSize = 10;
	$scope.history = false;
	$scope.companies = null; 
	$scope.pages = [];
	$scope.sortOrders = {
		"fields": ["name", "phoneNo", "email", "createdBy.firstName,createdBy.lastName", "dateModified", "modifiedBy.firstName,modifiedBy.lastName", "dateModified"],
		"directions": ["asc", "asc", "asc", "asc", "desc", "asc", "asc" ]
	};
	
	$scope.sortFields = $scope.sortOrders['fields'][4];
	$scope.sortDirection = $scope.sortOrders['directions'][4];
	$scope.url = 'companies/all';
	
	$scope.getCompanies = function () {
		var params = { 
			keyword: $scope.keyword,
			sortFields: $scope.sortFields,
			direction: $scope.direction,
			page: $scope.pageNumber, 
			pageSize: $scope.pageSize,
			history: $scope.history, 
		};

		$scope.readData($scope.url, params, 'companies', $scope);
	}
	
	$scope.getCompanies();
	
	$scope.addCompany = function() {
		$scope.addEntityInternal('assets/js/app/companies/add-company.component.html', 'CompanyAddCtrl', 'companies/all', 'companies', $scope);
	};
	
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			company: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('assets/js/app/companies/detail-company.component.html', 'CompanyDetailCtrl', 'companies/all', 'companies', $scope, resolveObj);
	};
	
	$scope.editDetail = function(uniqueId) {
		var resolveObj = { 
			company: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.editEntityInternal('assets/js/app/companies/edit-company.component.html', 'CompanyEditCtrl', 'companies/all', 'companies', $scope, resolveObj);
	};
	
	$scope.remove = function(uniqueId) {
		var resolveObj = { 
			company: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.removeEntityInternal('delete-company.component.html', 'DeleteCompanyCtrl', 'companies/all', 'companies', $scope, resolveObj);
	};

	$scope.prevPage = function() {
		$scope.prevPageInternal('companies/all', 'companies', $scope);
	};
	
	$scope.moveTo = function(page) {
		$scope.moveToInternal('companies/all', 'companies', $scope, page);
	};
	
	$scope.nextPage = function() {
		$scope.nextPageInternal('companies/all', 'companies', $scope);
	};

	$scope.searchByKeyword = function() {
		$scope.searchByKeywordInternal('companies/all', 'companies', $scope);
	};
	
	$scope.sortBy = function(field, direction) {
		$scope.sortByInternal('companies/all', 'companies', $scope, field, direction);
	};
	
}]);

CompanyModule.controller("CompanyAddCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'usSpinnerService', 'BusinessCategoryService', 'FormUtils', function ($scope, $http, $location, $modalInstance, ngToast, usSpinnerService, BusinessCategoryService, FormUtils) {
	$scope.company = {};
	
	$scope.businessCategoryConfig = BusinessCategoryService.config({}, function(value, $item) {
		$scope.company.businessCategory = value;
	});
	
	BusinessCategoryService.findBusinessCategories('businessCategories/all')
		.then(function(businessCategories) {
			$scope.businessCategories = businessCategories; 
		});
	
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
		});

	$scope.ok = function(isValid) {
		if (isValid) {
			var formData = new FormData();
			
			formData.append('company', new Blob([JSON.stringify($scope.company)], { type: "application/json" }));
			formData.append('companyLogo', $scope.companyLogo);
			formData.append('adminImage', $scope.adminImage);
			$scope.postCreateEntity('companies/new', formData, $scope.companyFrm, $modalInstance);
		}
    };
    
    $scope.close = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

CompanyModule.controller("CompanyDetailCtrl", ['$scope', '$http', '$modalInstance', 'company', function ($scope, $http, $modalInstance, company) {
	$http.get('companies/data/' + company.uniqueId)
		.then(function(response) {
			$scope.company = response.data;
		});
	
	$scope.close = function() {
		$modalInstance.close();
    };
}]);

CompanyModule.controller("CompanyEditCtrl", ['$scope', '$http', '$modalInstance', 'BusinessCategoryService', 'ngToast', 'company', function ($scope, $http, $modalInstance, BusinessCategoryService, ngToast, company) {
	$http.get('companies/data/' + company.uniqueId)
		.then(function(response) {
			$scope.company = response.data;
		});	
	
	$scope.businessCategoryConfig = BusinessCategoryService.config({}, function(value, $item) {
		$scope.company.businessCategory = value;
	});
	
	BusinessCategoryService.findBusinessCategories('businessCategories/all')
		.then(function(businessCategories) {
			$scope.businessCategories = businessCategories; 
		});
	
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
		});
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$scope.postUpdateEntity('companies/update/', $scope.company, $scope.companyFrm, $modalInstance);
		}
    };
    
    $scope.close = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

CompanyModule.controller("DeleteCompanyCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'company', function ($scope, $http, $location, $modalInstance, ngToast, company) {
	$scope.ok = function() {
		$scope.deleteEntity('companies/delete/' + company.uniqueId, $modalInstance);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);