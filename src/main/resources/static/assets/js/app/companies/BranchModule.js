'use strict';

var BranchModule = angular.module("BranchModule", []);

BranchModule.controller("CompanyBranchListCtrl", ['$scope', '$http', '$modal', '$log', 'ENTITY_TYPES', function ($scope, $http, $modal, $log, ENTITY_TYPES) {
	$scope.pageNumber = 0;
	$scope.keywords = null;
	$scope.pageSize = 10;
	$scope.pages = [];
	
	var data = null;
	if ($scope.detail.entity === ENTITY_TYPES.company) {
		var companyId = $scope.detail.companyId;
		data = { keyword : null, draw: 0, start : $scope.pageNumber, length : $scope.pageSize, companyId: companyId };
	} else {
		data = { keyword : null, draw: 0, start : $scope.pageNumber, length : $scope.pageSize };
	}
	
	$http.get(contextPath + '/branches/company', { params: data }).success(function(response) {
		$scope.branches = response.data || {};
		$scope.totalPage = Math.ceil(response.recordsTotal / $scope.pageSize);
		for (var i = 0; i < $scope.totalPage; i++) {
			$scope.pages.push(i + 1);
		}
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$scope.remove = function(uniqueId) {
		var modalInstance = $modal.open({
			animation : $scope.animationsEnabled,
			templateUrl : 'delete-branch.component.html',
			controller : 'DeleteBranchCtrl',
			size : 'lg',
			resolve: {
				branch: function() {
					return {
						uniqueId: uniqueId
					};
				}
			}
		});
	
		modalInstance.result.then(function(uniqueId) {
			var deleteRequest = $http.delete(contextPath + '/branches/delete/' + uniqueId);
			
			deleteRequest.success(function(response) {
				var companyId = SharedProperties.getCompanyId();
				var data = { keyword : null, draw: 0, start : $scope.pageNumber, length : $scope.pageSize, companyId: companyId };
				$http.get(contextPath + '/branches/company', { params : data}).success(function(response) {
					$scope.branches = response.data || {};
				}).error(function(data) {
					console.log('error occurred');
				});
			});
			
			deleteRequest.error(function(response) {
				console.log('We cannot delete Credit Card. we got some error.');
			});
		}, function() {
			$log.info('Modal dismissed at: ' + new Date());
		});
	};
}]);

BranchModule.controller("BranchAddCtrl", ['$scope', '$http', '$location', function ($scope, $http, $location) {
	$scope.branch = { }
	
	$http.get(contextPath + '/countries/all').success(function(response) {
		$scope.countries = response.data;
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$http.get(contextPath + '/states/all').success(function(response) {
		$scope.states = response.data;
	}).error(function(data) {
		console.log('error occurred');
	});
	
	var companyId = ''; 
	if ($scope.isAuthorized('ROLE_ADMIN')) {
		companyId = $scope.detail.companyId;
	} else if ($scope.isAuthorized('ROLE_COMPANY_ADMIN') || $scope.isAuthorized('ROLE_AGENT')){
		companyId = $scope.currentUser.companyId;
	}
	$scope.branch.companyId = companyId;
}]);

BranchModule.controller("BranchEditCtrl", ['$scope', '$http', '$stateParams', '$location', function ($scope, $http, $stateParams, $location) {
	$http.get(contextPath + '/countries/all').success(function(response) {
		$scope.countries = response.data;
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$http.get(contextPath + '/states/all').success(function(response) {
		$scope.states = response.data;
	}).error(function(data) {
		console.log('error occurred');
	});
	
	var uniqueId = $stateParams.uniqueId;
	$http.get(contextPath + '/branches/data/' + uniqueId).success(function(response) {
		$scope.branch = response || {};
	}).error(function(data) {
		console.log('error occurred');
	});
}]);

BranchModule.controller("DeleteBranchCtrl", ['$scope', '$http', '$location', '$modalInstance', 'branch', function ($scope, $http, $location, $modalInstance, branch) {
	$scope.ok = function() {
		$modalInstance.close(branch.uniqueId);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);