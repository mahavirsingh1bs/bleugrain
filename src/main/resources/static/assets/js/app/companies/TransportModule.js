'use strict';

var TransportModule = angular.module("TransportModule", ['ui.bootstrap', 'ui.bootstrap.timepicker', 'ngFileUpload']);

TransportModule.controller("TransportListCtrl", ['$scope', '$http', '$modal', '$log', 'ngToast', 'HttpService', 'UtilityService', 'usSpinnerService', 'ENTITY_TYPES', 
           function ($scope, $http, $modal, $log, ngToast, HttpService, UtilityService, usSpinnerService, ENTITY_TYPES) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.keyword = null;
	$scope.pageSize = 10;
	$scope.transports = null;
	$scope.pages = [];
	$scope.sortOrders = {
		"fields": ["name", "capacity.capacity", "company.name", "createdBy.firstName,createdBy.lastName", "dateCreated"],
		"directions": ["asc", "asc", "asc", "asc", "asc", "asc", "asc" ]
	};
	
	$scope.sortFields = $scope.sortOrders['fields'][0];
	$scope.sortDirection = $scope.sortOrders['directions'][0];
	
	if (!$scope.isAuthorized('ROLE_ADMIN')) {
		$scope.url = 'transports/company';
		$scope.companyId = $scope.currentUser.company.uniqueId;
	} else {
		$scope.url = 'transports/all';
	}

	$scope.getTransports = function() {
		var params = { 
			keyword: $scope.keyword,
			sortFields: $scope.sortFields,
			direction: $scope.direction,
			page: $scope.pageNumber, 
			pageSize: $scope.pageSize
		};
		
		if (!UtilityService.isUndefinedOrNull($scope.companyId)) {
			params.companyId = $scope.companyId;
		}

		$scope.readData($scope.url, params, 'transports', $scope);
	};
	
	$scope.getTransports();
	
	$scope.addTransport = function() {
		var resolveObj = { 
			currentUser: function() {
				return $scope.currentUser;
			}
		};
		
		$scope.addEntityInternal('assets/js/app/companies/add-transport.component.html', 'TransportAddCtrl', $scope.url, 'transports', $scope, resolveObj);
	};
	
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			transport: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('assets/js/app/companies/detail-transport.component.html', 'TransportDetailCtrl', $scope.url, 'transports', $scope, resolveObj);
	};
	
	$scope.editDetail = function(uniqueId) {
		var resolveObj = { 
			transport: function() {
				return {
					uniqueId: uniqueId
				}
			},
			currentUser: function() {
				return $scope.currentUser;
			}
		};
		
		$scope.editEntityInternal('assets/js/app/companies/edit-transport.component.html', 'TransportEditCtrl', $scope.url, 'transports', $scope, resolveObj);
	};
	
	$scope.remove = function(uniqueId) {
		var resolveObj = { 
			transport: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.removeEntityInternal('delete-transport.component.html', 'DeleteTransportCtrl', $scope.url, 'transports', $scope, resolveObj);
	};

	$scope.assignDriver = function(uniqueId) {
		var resolveObj = { 
			transport: function() {
				return {
					uniqueId: uniqueId
				}
			},
			currentUser: function() {
				return $scope.currentUser;
			}
		}
		
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/companies/assign-driver.component.html',
  			controller : 'DriverAssignCtrl',
  			size : 'md',
	  		resolve: resolveObj,
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function() {
  			$scope.getTransports();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
	};

	$scope.assignDelivery = function(uniqueId) {
		var resolveObj = { 
			transport: function() {
				return {
					uniqueId: uniqueId
				}
			},
			currentUser: function() {
				return $scope.currentUser;
			}
		}
		
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/companies/assign-delivery.component.html',
  			controller : 'DeliveryAssignCtrl',
  			size : 'lg',
	  		resolve: resolveObj,
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function() {
  			$scope.getTransports();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
	};
	
	$scope.prevPage = function() {
		$scope.prevPageInternal('transports/all', 'transports', $scope);
	};
	
	$scope.moveTo = function(page) {
		$scope.moveToInternal('transports/all', 'transports', $scope, page);
	};
	
	$scope.nextPage = function() {
		$scope.nextPageInternal('transports/all', 'transports', $scope);
	};

	$scope.searchByKeyword = function() {
		$scope.searchByKeywordInternal('transports/all', 'transports', $scope);
	};
	
	$scope.sortBy = function(field, direction) {
		$scope.sortByInternal('transports/all', 'transports', $scope, field, direction);
	};

}]);

TransportModule.controller("TransportAddCtrl", ['$rootScope', '$scope', '$http', '$location', '$modalInstance', 'currentUser', 'ngToast', 'PermitService', function ($rootScope, $scope, $http, $location, $modalInstance, currentUser, ngToast, PermitService) {
	$scope.transport = { };
	$scope.transport.selectedPermitIds = [];
		
	$scope.permitConfig = PermitService.config({
		maxItems: 10,
		placeholder: 'Select permits',
		onItemAdd: function(value, $item) {
			$scope.transport.selectedPermitIds.push(value);
		}
	});
	
	PermitService.findPermits('permits/all')
		.then(function(permits) {
			$scope.permits = permits; 
		});
	
	$http.get('currencies/all').success(function(response) {
		$scope.currencies = response;
		angular.forEach($scope.currencies, function(currency) {
			if (currency.countryCode === currentUser.defaultCountry.code) {
				$scope.setPriceCurrency(currency);
			}
		});
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$http.get('units/all').success(function(response) {
		$scope.units = response;
		angular.forEach($scope.units, function(unit) {
			if (unit.countryCode === currentUser.defaultCountry.code && unit['default'] === true) {
				$scope.setCapacityUnit(unit);
				$scope.setPriceUnit(unit);
			}
		});
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$scope.setCapacityUnit = function(capacityUnit) {
		$scope.transport.capacityUnitId = capacityUnit.id;
		$scope.transport.capacityUnit = capacityUnit;
	};

	$scope.setPriceCurrency = function(priceCurrency) {
		$scope.transport.priceCurrencyId = priceCurrency.id;
		$scope.transport.priceCurrency = priceCurrency;
	};
	
	$scope.setPriceUnit = function(priceUnit) {
		$scope.transport.priceUnitId = priceUnit.id;
		$scope.transport.priceUnit = priceUnit;
	};

	$scope.ok = function(isValid) {
		if (isValid) {
			if ($scope.isAuthorized('ROLE_COMPANY_ADMIN')) {
				$scope.transport.companyId = currentUser.company.uniqueId;
			}
			
			$http.post('transports/new/', $scope.transport)
				.then(function(response) {
					if (response.data.code === 'success') {
						ngToast.create({ className: 'success', content: response.data.message });
					} else if (response.data.code === 'failure') {
						ngToast.create({ className: 'danger', content: response.data.message });
					}
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

TransportModule.controller("TransportDetailCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', 'transport', function ($scope, $http, $modalInstance, ngToast, transport) {
	$http.get('transports/data/' + transport.uniqueId)
		.then(function(response) {
			$scope.transport = response.data;
		});
	
	$scope.close = function() {
		$modalInstance.close();
    };
}]);

TransportModule.controller("TransportEditCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', 'PermitService', 'UtilityService', 'transport', 'currentUser', function ($scope, $http, $modalInstance, ngToast, PermitService, UtilityService, transport, currentUser) {
	$scope.transport = {};
	
	$http.get('transports/data/' + transport.uniqueId)
		.then(function(response) {
			$scope.transport = response.data;
		});
	
	$scope.permitConfig = PermitService.config({
		maxItems: 10,
		placeholder: 'Select permits',
		onItemAdd: function(value, $item) {
			if (UtilityService.isUndefinedOrNull($scope.transport.selectedPermitIds)) {
				$scope.transport.selectedPermitIds = [];
			}
			$scope.transport.selectedPermitIds.push(value);
		}
	});
	
	PermitService.findPermits('permits/all')
		.then(function(permits) {
			$scope.permits = permits; 
		});
	
	$http.get('currencies/all').success(function(response) {
		$scope.currencies = response;
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$http.get('units/all').success(function(response) {
		$scope.units = response;
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$scope.setCapacityUnit = function(capacityUnit) {
		$scope.transport.capacityUnit = capacityUnit;
	};
	
	$scope.setPriceCurrency = function(priceCurrency) {
		$scope.transport.priceCurrency = priceCurrency;
	};
	
	$scope.setPriceUnit = function(priceUnit) {
		$scope.transport.priceUnit = priceUnit;
	};
	
	$scope.ok = function() {
		$scope.postUpdateEntity('transports/update/', $scope.transport, $scope.transportFrm, $modalInstance);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

TransportModule.controller("DriverAssignCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'transport', 'currentUser', function ($scope, $http, $location, $modalInstance, ngToast, transport, currentUser) {
	$scope.driverAssign = { };
	
	$scope.searchDriver = function(keywords) {
		var params = {
			keywords: keywords,
			companyId: currentUser.company.uniqueId
		};
		
		return $http.get('drivers/search', { params: params })
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.selectDriver = function(item, model, label) {
		$scope.driverAssign.userNo = item.userNo;
		$scope.driverAssign.driver = item;
	}
	
	$scope.submit = function(isValid) {
		if (isValid) {
			$scope.driverAssign['uniqueId'] = transport.uniqueId;
			$scope.driverAssign['userNo'] = currentUser.userNo;
			
			$http.post('transports/assignDriver', $scope.driverAssign)
				.then(function(response) {
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

TransportModule.controller("DeliveryAssignCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'transport', 'currentUser', function ($scope, $http, $location, $modalInstance, ngToast, transport, currentUser) {
	$scope.assignDelivery = { };
	$scope.assignDelivery.pickupAddress = {};
	$scope.assignDelivery.deliveryAddress = {};
	
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
		});
	
	$scope.submit = function(isValid) {
		if (isValid) {
			$scope.assignDelivery['uniqueId'] = transport.uniqueId;
			$scope.assignDelivery['userNo'] = currentUser.userNo;
			
			$http.post('transports/assignDelivery', $scope.assignDelivery)
				.then(function(response) {
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

TransportModule.controller("DeleteTransportCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'transport', function ($scope, $http, $location, $modalInstance, ngToast, transport) {
	$scope.ok = function() {
		$scope.deleteEntity('transports/delete/' + transport.uniqueId, $modalInstance);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);