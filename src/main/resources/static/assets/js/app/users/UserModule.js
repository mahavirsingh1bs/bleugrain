'use strict';

var UserModule = angular.module("UserModule", []);

UserModule.controller("UserAccountCtrl", ['$scope', '$http', 'ENTITY_TYPES', function($scope, $http, ENTITY_TYPES) {
	var userNo = $scope.currentUser.userNo;
	
	$scope.detail = {
		userId: userNo,
		entity: ENTITY_TYPES.user
	}
	
	$http.get('user/data/' + userNo).success(function(response) {
		$scope.user = response || {};
	}).error(function(data) {
		console.log('error occurred');
	});
}]);

UserModule.controller("SellerDetailCtrl", ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams) {
	var userNo = $routeParams.userNo;
	
	$http.get(contextPath + '/sellers/data/' + userNo).success(function(response) {
		$scope.seller = response || {};
	}).error(function(data) {
		console.log('error occurred');
	});
}]);

UserModule.controller("BuyerDetailCtrl", ['$scope', '$http', '$routeParams', function($scope, $http, $routeParams) {
	var userNo = $routeParams.userNo;
	
	$http.get(contextPath + '/buyers/data/' + userNo).success(function(response) {
		$scope.buyer = response || {};
	}).error(function(data) {
		console.log('error occurred');
	});
}]);