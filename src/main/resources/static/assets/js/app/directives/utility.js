'use strict';

var utility = angular.module('utility', []);

utility.directive('delayedSearch', ['$http', '$timeout', function ($http, $timeout) {
    return {
    	restrict: 'E',
    	template: '<input type="text" class="form-control" ng-model="ngModel" uib-typeahead="category as category.name for category in categories | filter:{name:$viewValue}" placeholder="Search by email id, phone number"/>',
    	scope: {
    		ngModel: '='
    	},
        link: function (scope, element, attrs) {
        	var timer = false;
        	scope.$watch('ngModel', function () {
                if (timer) {
                    $timeout.cancel(timer);
                }
                timer = $timeout(function () {
                    if (element.find('input').val()) {
                    	alert(scope.ngModel);
                        alert('search now: ' + element.find('input').val());
                    }
                }, 1000)
            });
        }
    };
}]);