'use strict';

var UserPrefsModule = angular.module("UserPrefsModule", []);

UserPrefsModule.controller("UserPrefsCtrl", ['$scope', '$http', '$modal', '$log', function ($scope, $http, $modal, $log) {
	$scope.changingPasswd = false;
	
	$http.get(contextPath + '/addresses/delivery').success(function(response) {
		$scope.deliveryAddresses = response || {};
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$http.get(contextPath + '/addresses/billing').success(function(response) {
		$scope.billingAddresses = response || {};
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$scope.changePasswd = function() {
		$scope.changingPasswd = true;
		$scope.passwdChangeMessage = null;
	}
	
	$scope.updatePasswd = function(oldPasswd, newPasswd, confirmNewPasswd) {
		var data = { 
			username: $scope.currentUser.username,
			emailAddress: $scope.currentUser.emailId,
			oldPasswd: oldPasswd,
			newPasswd: newPasswd,
			confirmNewPasswd: confirmNewPasswd
		};
		
		$http.post(contextPath + '/account/changePasswd', data)
			.then(function(response) {
				if (response.data.code === 'success') {
					$scope.changingPasswd = false;
				}
				$scope.passwdChangeMessage = response.data.message;
			});
	}
	
	$scope.deleteBillingAddress = function(billingAddress) {
		var modalInstance = $modal.open({
			animation : $scope.animationsEnabled,
			templateUrl : 'delete-billing-address.component.html',
			controller : 'DeleteAddressCtrl',
			size : 'lg',
			resolve: {
				address: function() {
					return billingAddress;
				}
			}
		});
	
		modalInstance.result.then(function(id) {
			$http.delete(contextPath + '/addresses/delete/' + id)
				.then(function(response) {
					if (response.data.code === 'success') {
						$log.info("Address got deleted successfully");
						$http.get(contextPath + '/addresses/billing').success(function(response) {
							$scope.billingAddresses = response || {};
						}).error(function(data) {
							console.log('error occurred');
						});
					}
				});
			
		}, function() {
			$log.info('Modal dismissed at: ' + new Date());
		});
	};
	
	$scope.deleteDeliveryAddress = function(deliveryAddress) {
		var modalInstance = $modal.open({
			animation : $scope.animationsEnabled,
			templateUrl : 'delete-delivery-address.component.html',
			controller : 'DeleteAddressCtrl',
			size : 'lg',
			resolve: {
				address: function() {
					return deliveryAddress;
				}
			}
		});
	
		modalInstance.result.then(function(id) {
			$http.delete(contextPath + '/addresses/delete/' + id)
				.then(function(response) {
					if (response.data.code === 'success') {
						$log.info("Address got deleted successfully");
						$http.get(contextPath + '/addresses/delivery').success(function(response) {
							$scope.deliveryAddresses = response || {};
						}).error(function(data) {
							console.log('error occurred');
						});
					}
				});
			
		}, function() {
			$log.info('Modal dismissed at: ' + new Date());
		});
	};
	
	$scope.cancel = function() {
		$scope.passwdChangeMessage = null;
		$scope.changingPasswd = false;
	}
}]);

UserPrefsModule.controller("DeleteAddressCtrl", ['$scope', '$http', '$location', '$modalInstance', 'address', function ($scope, $http, $location, $modalInstance, address) {
	$scope.ok = function() {
		$modalInstance.close(address.id);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

UserPrefsModule.controller("AddAddressCtrl", ['$scope', '$http', '$location', function ($scope, $http, $location) {
	$scope.addAddressSuccessfull = false;
	$scope.addAddressMessage = null;
	
	$http.get(contextPath + '/countries/all').success(function(response) {
		$scope.countries = response.data || {};
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$http.get(contextPath + '/states/all').success(function(response) {
		$scope.states = response.data || {};
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$scope.addBillingAddr = function(addressFrm) {
		if (addressFrm.$valid) {
			$http.post(contextPath + '/addresses/billing/new', $scope.billingAddress)
				.then(function(response) {
					$scope.addAddressMessage = response.data.message;
					if (response.data.code === 'success') {
						$scope.addAddressSuccessfull = true;
						$location.path('/user/prefs');
					}
				});
		}
	}
	
	$scope.addDeliveryAddr = function(addressFrm) {
		if (addressFrm.$valid) {
			$http.post(contextPath + '/addresses/delivery/new', $scope.deliveryAddress)
				.then(function(response) {
					$scope.addAddressMessage = response.data.message;
					if (response.data.code === 'success') {
						$scope.addAddressSuccessfull = true;
						$location.path('/user/prefs');
					}
				});
		}
	}
	
	$scope.cancel = function() {
		$scope.addAddressSuccessfull = false;
		$scope.addAddressMessage = null;;
		$location.path('/user/prefs');
	}
}]);

UserPrefsModule.controller("EditAddressCtrl", ['$scope', '$http', '$location', '$stateParams', function ($scope, $http, $location, $stateParams) {
	$scope.addAddressSuccessfull = false;
	$scope.addAddressMessage = null;
	
	$http.get(contextPath + '/countries/all').success(function(response) {
		$scope.countries = response.data || {};
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$http.get(contextPath + '/states/all').success(function(response) {
		$scope.states = response.data || {};
	}).error(function(data) {
		console.log('error occurred');
	});
	
	var uniqueId = $stateParams.uniqueId;
	$http.get(contextPath + '/addresses/' + uniqueId)
		.then(function(response) {
			$scope.address = response.data || {};
		});
	
	$scope.updateAddress = function(addressFrm) {
		if (addressFrm.$valid) {
			var category = $stateParams.category;
			if (category === 'billing') {
				$http.post(contextPath + '/addresses/billing/update', $scope.address)
					.then(function(response) {
						$scope.addAddressMessage = response.data.message;
						if (response.data.code === 'success') {
							$scope.addAddressSuccessfull = true;
							$location.path('/user/prefs');
						}
					});
			} else if (category === 'delivery') {
				$http.post(contextPath + '/addresses/delivery/update', $scope.address)
				.then(function(response) {
					$scope.addAddressMessage = response.data.message;
					if (response.data.code === 'success') {
						$scope.addAddressSuccessfull = true;
						$location.path('/user/prefs');
					}
				});
			}
		}
	}
	
	$scope.cancel = function() {
		$scope.addAddressSuccessfull = false;
		$scope.addAddressMessage = null;;
		$location.path('/user/prefs');
	}
}]);