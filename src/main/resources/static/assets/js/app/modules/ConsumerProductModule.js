'use strict';

var ConsumerProductModule = angular.module("ConsumerProductModule", []);

ConsumerProductModule.controller("ConsumerProductListCtrl", ['$scope', '$http', '$stateParams', '$log', 'HttpService', 'ngToast', function ($scope, $http, $stateParams, $log, HttpService, ngToast) {
	$scope.category = null;
	$scope.productCategories = null;
	
	$scope.noteColors = {
		0: 'funny-boxes-top-red',
		1: 'funny-boxes-top-sea',
		2: 'funny-boxes-top-purple',
		3: 'funny-boxes-top-yellow'
	};
	
	$http.get('consumerProducts/?uniqueId=' + $stateParams.uniqueId)
		.then(function(response) {
			$scope.category = response.data.category;
			$scope.consumerProducts = response.data.consumerProducts;
		});
}]);