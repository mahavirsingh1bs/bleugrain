'use strict';

var ServiceModule = angular.module("ServiceModule", []);

ServiceModule.controller("CompanyOrderListCtrl", ['$scope', '$http', function ($scope, $http) {
	$scope.pageNumber = 0;
	$scope.keywords = null;
	$scope.pageSize = 10;
	$scope.pages = [];
	
	var data = { keyword : null, draw: 0, start : $scope.pageNumber, length : $scope.pageSize};
	$http.get(contextPath + '/orders/company', { params : data}).success(function(response) {
		$scope.orders = response.data || {};
		$scope.totalPage = Math.ceil(response.recordsTotal / $scope.pageSize);
		for (var i = 0; i < $scope.totalPage; i++) {
			$scope.pages.push(i + 1);
		}
	}).error(function(data) {
		console.log('error occurred');
	});
	
}]);

ServiceModule.controller("UserOrderListCtrl", ['$scope', '$http', function ($scope, $http) {
	$scope.pageNumber = 0;
	$scope.keywords = null;
	$scope.pageSize = 10;
	$scope.pages = [];
	
	var data = { keyword : null, draw: 0, start : $scope.pageNumber, length : $scope.pageSize};
	$http.get(contextPath + '/orders/user', { params : data}).success(function(response) {
		$scope.orders = response.data || {};
		$scope.totalPage = Math.ceil(response.recordsTotal / $scope.pageSize);
		for (var i = 0; i < $scope.totalPage; i++) {
			$scope.pages.push(i + 1);
		}
	}).error(function(data) {
		console.log('error occurred');
	});
	
}]);

ServiceModule.controller("UserProductListCtrl", ['$scope', '$http', function ($scope, $http) {
	$scope.pageNumber = 0;
	$scope.keywords = null;
	$scope.pageSize = 10;
	$scope.pages = [];
	
	var data = { keyword : null, draw: 0, start : $scope.pageNumber, length : $scope.pageSize};
	$http.get(contextPath + '/products/user', { params : data}).success(function(response) {
		$scope.products = response.data || {};
		$scope.totalPage = Math.ceil(response.recordsTotal / $scope.pageSize);
		for (var i = 0; i < $scope.totalPage; i++) {
			$scope.pages.push(i + 1);
		}
	}).error(function(data) {
		console.log('error occurred');
	});
}]);

ServiceModule.controller("CompanyDemandListCtrl", ['$scope', '$http', function ($scope, $http) {
	$scope.pageNumber = 0;
	$scope.keywords = null;
	$scope.pageSize = 10;
	$scope.pages = [];
	
	var data = { keyword : null, draw: 0, start : $scope.pageNumber, length : $scope.pageSize};
	$http.get(contextPath + '/demands/company', { params : data}).success(function(response) {
		$scope.demands = response.data || {};
		$scope.totalPage = Math.ceil(response.recordsTotal / $scope.pageSize);
		for (var i = 0; i < $scope.totalPage; i++) {
			$scope.pages.push(i + 1);
		}
	}).error(function(data) {
		console.log('error occurred');
	});
	
}])
.controller("UserDemandListCtrl", ['$scope', '$http', function ($scope, $http) {
	$scope.pageNumber = 0;
	$scope.keywords = null;
	$scope.pageSize = 10;
	$scope.pages = [];
	
	var data = { keyword : null, draw: 0, start : $scope.pageNumber, length : $scope.pageSize};
	$http.get(contextPath + '/demands/user', { params : data}).success(function(response) {
		$scope.demands = response.data || {};
		$scope.totalPage = Math.ceil(response.recordsTotal / $scope.pageSize);
		for (var i = 0; i < $scope.totalPage; i++) {
			$scope.pages.push(i + 1);
		}
	}).error(function(data) {
		console.log('error occurred');
	});
	
}]);

ServiceModule.controller("CompanyWishlistListCtrl", ['$scope', '$http', function ($scope, $http) {
	$scope.pageNumber = 0;
	$scope.keywords = null;
	$scope.pageSize = 10;
	$scope.pages = [];
	
	var data = { keyword : null, draw: 0, start : $scope.pageNumber, length : $scope.pageSize};
	$http.get(contextPath + '/wishlist/buyer/company', { params : data}).success(function(response) {
		$scope.buyerWishlist = response.data || {};
		$scope.totalPage = Math.ceil(response.recordsTotal / $scope.pageSize);
		for (var i = 0; i < $scope.totalPage; i++) {
			$scope.pages.push(i + 1);
		}
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$http.get(contextPath + '/wishlist/seller/company', { params : data}).success(function(response) {
		$scope.sellerWishlist = response.data || {};
		$scope.totalPage = Math.ceil(response.recordsTotal / $scope.pageSize);
		for (var i = 0; i < $scope.totalPage; i++) {
			$scope.pages.push(i + 1);
		}
	}).error(function(data) {
		console.log('error occurred');
	});
}]);

ServiceModule.controller("UserWishlistListCtrl", ['$scope', '$http', function ($scope, $http) {
	$scope.pageNumber = 0;
	$scope.keywords = null;
	$scope.pageSize = 10;
	$scope.pages = [];
	
	var data = { keyword : null, draw: 0, start : $scope.pageNumber, length : $scope.pageSize};
	$http.get(contextPath + '/wishlist/buyer/user', { params : data}).success(function(response) {
		$scope.buyerWishlist = response.data || {};
		$scope.totalPage = Math.ceil(response.recordsTotal / $scope.pageSize);
		for (var i = 0; i < $scope.totalPage; i++) {
			$scope.pages.push(i + 1);
		}
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$http.get(contextPath + '/wishlist/seller/user', { params : data}).success(function(response) {
		$scope.sellerWishlist = response.data || {};
		$scope.totalPage = Math.ceil(response.recordsTotal / $scope.pageSize);
		for (var i = 0; i < $scope.totalPage; i++) {
			$scope.pages.push(i + 1);
		}
	}).error(function(data) {
		console.log('error occurred');
	});
}]);