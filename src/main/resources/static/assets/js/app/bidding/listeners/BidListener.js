(function(angular, SockJS, Stomp, _, undefined) {
	angular.module("BidModule").service("BidListener", ['$q', '$timeout', function($q, $timeout) {
	
		var service = {}, listener = $q.defer(), socket = {
			client : null,
			stomp : null
		}, subscription = {}, notificationIds = [];
	
		service.RECONNECT_TIMEOUT = 30000;
		service.SOCKET_URL = '/notification/send';
		service.BIDS_TOPIC = '/topic/bids';
		service.BIDS_BROKER = '/bleugrain/notification/send';
	
		service.receive = function() {
			return listener.promise;
		};
	
		service.send = function(message) {
			var id = Math.floor(Math.random() * 1000000);
			socket.stomp.send(service.BIDS_BROKER, {}, JSON.stringify({
				'message' : message,
				'id' : id
			}));
			notificationIds.push(id);
		};
	
		service.connect = function() {
			socket.client = new SockJS(service.SOCKET_URL);
			socket.stomp = Stomp.over(socket.client);
			socket.stomp.connect({}, subscribeAndListen);
			socket.stomp.onclose = reconnect;
		}

		service.subscribe = function() {
			subscribeAndListen();
		}
		
		service.unsubscribe = function() {
			if (angular.isUndefined(subscription)) {
				subscription.unsubscribe();
			}
		}
		
		service.disconnect = function() {
			service.unsubscribe();
		}
		
		var reconnect = function() {
			$timeout(function() {
				service.connect();
			}, this.RECONNECT_TIMEOUT);
		};
	
		var getNotification = function(data) {
			var notification = JSON.parse(data), out = {};
			out.notification = notification.message;
			out.time = new Date(notification.time);
			if (_.contains(notificationIds, notification.id)) {
				out.self = true;
				notificationIds = _.remove(notificationIds, notification.id);
			}
			return out;
		};
	
		var subscribeAndListen = function() {
			subscription = socket.stomp.subscribe(service.BIDS_TOPIC, function(data) {
				var event = JSON.parse(data.body);
				if (angular.equals(event.category, 'bid')) {
					listener.notify(event.object);
				}
			});
		};
	
		return service;
	}]);
})(angular, SockJS, Stomp, _);