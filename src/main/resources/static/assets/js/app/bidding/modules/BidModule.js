'use strict';

var BidModule = angular.module("BidModule", ['ngFileUpload', 'ngImgCrop']);

BidModule.controller("BidListCtrl", ['$scope', '$http', '$location', '$modal', '$log', 'ngToast', 'BidListener', 'ProductListener', 
         function ($scope, $http, $location, $modal, $log, ngToast, BidListener, ProductListener) {
	$scope.products = [];
	$scope.pageNumber = 0;
	$scope.pageSize = 20;
	$scope.category = null;
	$scope.state = null;	
	$scope.message = '<span class="label label-orange rounded">Bidding done!</span>';
	
	BidListener.connect();
	ProductListener.connect();
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
			$scope.states.unshift({"id" : "-1", "name": "All States"})
			$scope.state = $scope.states[0];
			$scope.getCategories();
		});
	
	$scope.getCategories = function() {
		$http.get('categories/')
			.then(function(response) {
				$scope.categories = response.data;
				$scope.category = $scope.categories[0];
				$scope.getBiddableProducts();
			});
	}
	
	$scope.getBiddableProducts = function () {
		var params = {
			uniqueId: $scope.category.uniqueId,
			stateId: $scope.state.id,
			page: $scope.pageNumber,
			pageSize: $scope.pageSize
		};
		
		$http.get('products/biddable', { params: params })
			.then(function(response) {
				$scope.products = response.data;
			});
	};
	
	$scope.changeCategory = function(category) {
		$scope.category = category;
		$scope.getBiddableProducts();
	};
	
	$scope.changeState = function(state) {
		$scope.state = state;
		$scope.getBiddableProducts();
	};
	
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			product: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('assets/js/app/products/detail-product.component.html', 'ProductDetailCtrl', 'products/all', 'products', $scope, resolveObj);
	};
	
	$scope.placeBid = function(product) {
		
		var enableTillDate = moment(product.enableTillDate, 'YYYY-MM-DD HH:mm:ss.SSS');
		var isAfter = moment(new Date()).isAfter(enableTillDate);
		
		if (isAfter) {
			ngToast.create({
				  className: 'danger',
				  content: '<strong>Warning!</strong> Bidding has already been done.'
			});
			return false;
		}
		
		var resolveObj = { 
			product: function() {
				return product;
			}
		}
		
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/partials/bidding/html/place-bid.component.html',
  			controller : 'PlaceBidCtrl',
  			size : 'md',
	  		resolve: resolveObj,
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
	};
	
	$scope.viewAllBids = function(product) {
		
		var resolveObj = { 
			product: function() {
				return product;
			}
		}
		
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/bidding/html/all-bids.component.html',
  			controller : 'ProductBidsCtrl',
  			size : 'md',
	  		resolve: resolveObj,
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
	};
	
	BidListener.receive().then(null, null, function(bid) {
		angular.forEach($scope.products, function(product, index) {
			if (angular.equals(product.uniqueId, bid.uniqueId)) {
				$scope.products[index].highestBid = bid;
				$scope.products[index].bids.push(bid);
			}
		});
	});

	ProductListener.receive().then(null, null, function(product) {
		if ($scope.isWithinState(product) && $scope.isWithinCategory(product)) {
			$scope.products.push(product);
		}
	});
	
	$scope.isWithinState = function(product) {
		if (angular.equals($scope.state.id, "-1") || 
				angular.equals($scope.state.id, product.owner.address.state.id)) {
			return true;
		}
		return false;
	}
	
	$scope.isWithinCategory = function(product) {
		var withinCategory = false;
		angular.forEach(product.categories, function(category, index) {
			if (angular.equals($scope.category.id, category.id) || 
					angular.equals($scope.category.id, category.parentCategoryId)) {
				withinCategory = true;
			}
		})
		return withinCategory;
	}
	
}]);

BidModule.controller("PlaceBidCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'product', 'FormUtils', function ($scope, $http, $location, $modalInstance, ngToast, product, FormUtils) {
	$scope.bid = {};
	
	$scope.submit = function(isValid) {
		if (isValid) {
			$scope.bid['uniqueId'] = product.uniqueId;
			$scope.bid['userNo'] = $scope.currentUser.userNo;
			
			$http.post('bids/place', $scope.bid)
				.then(function(response) {
					$modalInstance.close();
				}, function(response) {
					FormUtils.resetFormErrors($scope.bidFrm);
					FormUtils.handleFormErrors($scope.bidFrm, response.data.fieldErrors);
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

BidModule.controller("ProductBidsCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'product', 'FormUtils', function ($scope, $http, $location, $modalInstance, ngToast, product, FormUtils) {
	$scope.pageNumber = 0;
	$scope.pageSize = 10;
	$scope.product = product;
	
	var params = {
		uniqueId: product.uniqueId,
		page: $scope.pageNumber,
		pageSize: $scope.pageSize
	};
	
	$http.get('bids/product', { params: params })
		.then(function(response) {
			$scope.bids = response.data;
		});
	
	$scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);