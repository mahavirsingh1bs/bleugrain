'use strict';

var ProductModule = angular.module("ProductModule", ['ui.bootstrap', 'ngFileUpload']);

ProductModule.controller("ProductListCtrl", ['$scope', '$http', '$modal', '$log', 'ngToast', 'HttpService', 'UtilityService', 'usSpinnerService', 'ENTITY_TYPES', 
           function ($scope, $http, $modal, $log, ngToast, HttpService, UtilityService, usSpinnerService, ENTITY_TYPES) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.keyword = null;
	$scope.pageSize = 10;
	$scope.products = null;
	$scope.history = false;
	$scope.consumerProducts = false;
	$scope.pages = [];
	$scope.sortOrders = {
		"fields": ["name", "quantity.quantity", "latestPrice.price", "owner.firstName,owner.lastName", "createdBy.firstName,createdBy.lastName", "dateCreated"],
		"directions": ["asc", "asc", "asc", "asc", "asc", "asc", "asc" ]
	};
	
	$scope.sortFields = $scope.sortOrders['fields'][0];
	$scope.sortDirection = $scope.sortOrders['directions'][0];
	
	if (!$scope.isAuthorized('ROLE_ADMIN')) {
		$scope.url = 'products/user';
		$scope.userNo = $scope.currentUser.userNo;
	} else {
		$scope.url = 'products/all';
	}

	$scope.getProducts = function() {
		var params = { 
			keyword: $scope.keyword,
			sortFields: $scope.sortFields,
			direction: $scope.direction,
			page: $scope.pageNumber, 
			pageSize: $scope.pageSize,
			history: $scope.history, 
			consumerProducts: $scope.consumerProducts,
		};
		
		if (!UtilityService.isUndefinedOrNull($scope.category)) {
			params.categoryId = $scope.category.uniqueId;
		}
		
		if (!UtilityService.isUndefinedOrNull($scope.userNo)) {
			params.userNo = $scope.userNo;
		}

		$scope.readData($scope.url, params, 'products', $scope);
	};
	
	$scope.getProducts();
	
	$scope.changeConsumerProducts = function () {
		if ($scope.consumerProducts) {
			$scope.pageSize = 12;
		}
		$scope.getProducts();
	}
	
	$scope.onCategoryChange = function(category) {
		$scope.category = category;
		$scope.getProducts();
	};
	
	$http.get('categories/')
		.then(function(response) {
			$scope.categories = response.data;
			$scope.category = $scope.categories[0];
			$scope.getProducts();
		});

	$scope.changeCategory = function(category) {
		$scope.category = category;
		$scope.getProducts();
	};
	
	$scope.addProduct = function() {
		var resolveObj = { 
			currentUser: function() {
				return $scope.currentUser;
			}
		};
		
		$scope.addEntityInternal('assets/js/app/products/add-product.component.html', 'ProductAddCtrl', $scope.url, 'products', $scope, resolveObj);
	};
	
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			product: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('assets/js/app/products/detail-product.component.html', 'ProductDetailCtrl', $scope.url, 'products', $scope, resolveObj);
	};
	
	$scope.editDetail = function(uniqueId) {
		var resolveObj = { 
			product: function() {
				return {
					uniqueId: uniqueId
				}
			},
			currentUser: function() {
				return $scope.currentUser;
			}
		};
		
		$scope.editEntityInternal('assets/js/app/products/edit-product.component.html', 'ProductEditCtrl', $scope.url, 'products', $scope, resolveObj);
	};
	
	$scope.editConsumerProduct = function(uniqueId) {
		var resolveObj = { 
			product: function() {
				return {
					uniqueId: uniqueId
				}
			},
			currentUser: function() {
				return $scope.currentUser;
			}
		};
		
		$scope.editEntityInternal('assets/js/app/products/edit-consumer-product.component.html', 'ConsumerProductEditCtrl', $scope.url, 'products', $scope, resolveObj);
	};
	
	$scope.addAttribute = function() {
		var resolveObj = { 
			product: function() {
				return product;
			}
		}
		
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/products/add-attribute.component.html',
  			controller : 'EnableBiddingCtrl',
  			size : 'md',
	  		resolve: resolveObj,
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function() {
  			$scope.getProducts();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
	};

	$scope.assign = function(product) {

		var resolveObj = { 
			product: function() {
				return product;
			}
		}
		
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/products/assign-product.component.html',
  			controller : 'ProductAssignCtrl',
  			size : 'md',
	  		resolve: resolveObj,
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function() {
  			$scope.getProducts();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
	};
	
	$scope.addEditDescription = function(product) {

		var resolveObj = { 
			product: function() {
				return product;
			}
		}
		
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/products/add-description.component.html',
  			controller : 'ProductDescriptionCtrl',
  			size : 'lg',
	  		resolve: resolveObj,
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function() {
  			$scope.getProducts();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
	};
	
	$scope.addChangeImages = function(product) {
		var resolveObj = { 
			product: function() {
				return product;
			}
		};
		
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/products/upload-images.component.html',
  			controller : 'UploadImagesCtrl',
  			size : 'lg',
  			resolve: resolveObj,
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getProducts();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
	};
	
	$scope.enableBidding = function(product) {

		if (product.bidEnabled) {
			ngToast.create({
				  className: 'danger',
				  content: '<strong>Warning!</strong> Bidding is already enabled.'
			});
			return false;
		}
		
		var resolveObj = { 
			product: function() {
				return product;
			}
		}
		
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/products/enable-bidding.component.html',
  			controller : 'EnableBiddingCtrl',
  			size : 'md',
	  		resolve: resolveObj,
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function() {
  			$scope.getProducts();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
	};
	
	$scope.markAsSoldOut = function(product) {
		var resolveObj = { 
			product: function() {
				return product;
			}
		}
		
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'mark-as-soldout.component.html',
  			controller : 'ProductSoldOutCtrl',
  			size : 'md',
	  		resolve: resolveObj,
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function() {
  			$scope.getProducts();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
	};
	
	$scope.remove = function(uniqueId) {
		var resolveObj = { 
			product: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.removeEntityInternal('delete-product.component.html', 'DeleteProductCtrl', $scope.url, 'products', $scope, resolveObj);
	};

	$scope.prevPage = function() {
		$scope.prevPageInternal('products/all', 'products', $scope);
	};
	
	$scope.moveTo = function(page) {
		$scope.moveToInternal('products/all', 'products', $scope, page);
	};
	
	$scope.nextPage = function() {
		$scope.nextPageInternal('products/all', 'products', $scope);
	};

	$scope.searchByKeyword = function() {
		$scope.searchByKeywordInternal('products/all', 'products', $scope);
	};
	
	$scope.sortBy = function(field, direction) {
		$scope.sortByInternal('products/all', 'products', $scope, field, direction);
	};

}]);

ProductModule.controller("ProductAddCtrl", ['$rootScope', '$scope', '$http', '$location', '$modalInstance', 'currentUser', 'ngToast', 'CategoryService', function ($rootScope, $scope, $http, $location, $modalInstance, currentUser, ngToast, CategoryService) {
	$scope.product = { };
	$scope.product.categories = [];
	$scope.product.owner = { };
	$scope.files = [];
	$scope.ownerImage = {};
	$scope.uploadProgress = 0;
	$scope.isCustomHeaderOpen = false;
	$scope.isExistingUser = null;
		
	$scope.categoryConfig = CategoryService.config({
		maxItems: 3,
		placeholder: 'Select categories'
	}, function(value, $item) {
		$scope.product.categories.push({ uniqueId: value });
	});
	
	CategoryService.findParentCategories('categories/')
		.then(function(categories) {
			$scope.categories = categories; 
		});
	
	$scope.$on('$dropletReady', function whenDropletReady() {
		$scope.interface.allowedExtensions([/.+/]);
	});
	
	$http.get('assets/resources/data/json/attributes.json')
		.then(function(response) {
			$scope.attributes = response.data;
			$scope.product.attributes = response.data;
		}, function(resp) {
			console.log(resp);
		});
	
	$http.get('groups/all')
		.then(function(response) {
			$scope.groups = response.data.groups;
		});
	
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
		});
	
	$http.get('currencies/all').success(function(response) {
		$scope.currencies = response;
		angular.forEach($scope.currencies, function(currency) {
			if (currency.countryCode === currentUser.defaultCountry.code) {
				$scope.setPriceCurrency(currency);
			}
		});
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$http.get('units/all').success(function(response) {
		$scope.units = response;
		angular.forEach($scope.units, function(unit) {
			if (unit.countryCode === currentUser.defaultCountry.code && unit['default'] === true) {
				$scope.setQuantityUnit(unit);
				$scope.setPriceUnit(unit);
			}
		});
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$scope.setQuantityUnit = function(quantityUnit) {
		$scope.product.quantityUnitId = quantityUnit.id;
		$scope.product.quantityUnit = quantityUnit;
	};
	
	$scope.setPriceCurrency = function(priceCurrency) {
		$scope.product.priceCurrencyId = priceCurrency.id;
		$scope.product.priceCurrency = priceCurrency;
	};
	
	$scope.setPriceUnit = function(priceUnit) {
		$scope.product.priceUnitId = priceUnit.id;
		$scope.product.priceUnit = priceUnit;
	};

	$scope.searchOwner = function(keywords) {
		return $http.get('owners/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.searchCategory = function(keywords) {
		return $http.get('categories/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.searchTag = function(keywords) {
		return $http.get('tags/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.selectOwner = function(item, model, label) {
		$scope.isExistingUser = true;
		$scope.product.ownerId = item.userNo;
	}

	$scope.ok = function(productFrm) {
		
		if (customValidation(productFrm, $scope.product)) {
			var formData = new FormData();
			
			if ($scope.isAuthorized('ROLE_BROKER') || $scope.isAuthorized('ROLE_FARMER')) {
				$scope.product.owner = null;
			}
			
			formData.append('product', new Blob([JSON.stringify($scope.product)], { type: "application/json" }));
			
			angular.forEach($scope.files, function(file) {
				formData.append('file', file);
			});
			
			$http.post('products/new', formData, {
					transformRequest: angular.identity,
					headers: { 'Content-Type': undefined }
				}).then(function(response) {
					if (response.data.code === 'success') {
						ngToast.create({ className: 'success', content: response.data.message });
						$modalInstance.close();
					} else if (response.data.code === 'failure') {
						$modalInstance.close();
					}
				});
				
		}
	};
	
	function customValidation(productFrm, product) {
		if (product.consumerProduct) {
			if (!productFrm.name.$error.required && !productFrm.name.$error.minlength
					&& !productFrm.quantity.$error.required && !productFrm.quantity.$error.pattern 
					&& !productFrm.price.$error.required) {
				return true;
			} else {
				return false;
			}
		} else {
			if ($scope.isExistingUser || $scope.isAuthorized('ROLE_BROKER') || $scope.isAuthorized('ROLE_FARMER')) {
				if (!productFrm.name.$error.required && !productFrm.name.$error.minlength
						&& !productFrm.quantity.$error.required && !productFrm.quantity.$error.pattern 
						&& !productFrm.price.$error.required) {
					return true;
				} else {
					return false;
				}
			} else {
				if (!productFrm.name.$error.required && !productFrm.name.$error.minlength
					&& !productFrm.quantity.$error.required && !productFrm.quantity.$error.pattern 
					&& !productFrm.price.$error.required && !productFrm.price.$error.pattern
					&& !productFrm.firstName.$error.required && !productFrm.firstName.$error.minlength
					&& !productFrm.firstName.$error.pattern && !productFrm.phoneNo.$error.required
					&& !productFrm.group.$error.required && !productFrm.addressLine1.$error.required 
					&& !productFrm.state.$error.required && !productFrm.country.$error.required
					&& !productFrm.zipcode.$error.required) {
					return true;
				} else {
					return false;
				}
			} 
		}
	};
	
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

ProductModule.controller("ProductDetailCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', 'product', function ($scope, $http, $modalInstance, ngToast, product) {
	$http.get('products/data/' + product.uniqueId)
		.then(function(response) {
			$scope.product = response.data;
		});
	
	$scope.setAsDefault = function(image) {
		var params = {
			productId: product.uniqueId,
			imageId: image.id
		};
		
		$http.post('products/setAsDefault', null, { params: params })
			.then(function(response) {
				if (response.data.code === 'success') {
					ngToast.create({ className: 'success', content: response.data.message });
				} else if (response.data.code === 'failure') {
					ngToast.create({ className: 'danger', content: response.data.message });
				}
			}, function(response) {
				ngToast.create({ className: 'danger', content: response.data.message });
			});
	}
	
	$scope.close = function() {
		$modalInstance.close();
    };
}]);

ProductModule.controller("ProductEditCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', 'CategoryService', 'product', 'currentUser', function ($scope, $http, $modalInstance, ngToast, CategoryService, product, currentUser) {
	$scope.maxlength = {
		shortDesc: 1000,
		desc: 5000,
	}
	$scope.isCustomHeaderOpen = false;
	$scope.product = {};
	$scope.product.categories = [];
	
	$scope.categoryConfig = CategoryService.config({
		create: true,
		maxItems: 3,
		placeholder: 'Select categories'
	}, function(value, $item) {
		$scope.product.categories.push({ uniqueId: value });
	});
	
	CategoryService.findCategories('categories/')
		.then(function(categories) {
			$scope.categories = categories; 
		});
	
	$http.get('products/data/' + product.uniqueId)
		.then(function(response) {
			$scope.product = response.data;
		});
	
	$http.get('currencies/all').success(function(response) {
		$scope.currencies = response;
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$http.get('units/all').success(function(response) {
		$scope.units = response;
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$http.get('productStatuses/all').success(function(response) {
		$scope.productStatuses = response.data;
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$scope.setQuantityUnit = function(quantityUnit) {
		$scope.product.quantityUnit = quantityUnit;
	};
	
	$scope.setPriceCurrency = function(priceCurrency) {
		$scope.product.priceCurrency = priceCurrency;
	};
	
	$scope.setPriceUnit = function(priceUnit) {
		$scope.product.priceUnit = priceUnit;
	};
	
	$scope.searchOwner = function(keywords) {
		return $http.get('owners/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.selectOwner = function(item, model, label) {
		$scope.product.owner = item;
	}
	
	$scope.searchCategory = function(keywords) {
		return $http.get('categories/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.searchTag = function(keywords) {
		return $http.get('tags/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.ok = function() {
		$scope.postUpdateEntity('products/update/', $scope.product, $scope.productFrm, $modalInstance);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

ProductModule.controller("ConsumerProductEditCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', 'product', 'currentUser', function ($scope, $http, $modalInstance, ngToast, product, currentUser) {
	$scope.isCustomHeaderOpen = false;
	$scope.product = {};
	
	$http.get('products/data/' + product.uniqueId)
		.then(function(response) {
			$scope.product = response.data;
		});

	$http.get('categories/all')
		.then(function(response) {
			$scope.categories = response.data.categories;
		});
	
	$http.get('currencies/all').success(function(response) {
		$scope.currencies = response;
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$http.get('units/all').success(function(response) {
		$scope.units = response;
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$http.get('productStatuses/all').success(function(response) {
		$scope.productStatuses = response.data;
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$scope.setPriceCurrency = function(priceCurrency) {
		$scope.product.priceCurrency = priceCurrency;
	};
	
	$scope.setPriceUnit = function(priceUnit) {
		$scope.product.priceUnit = priceUnit;
	};
	
	$scope.searchTag = function(keywords) {
		return $http.get('tags/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.ok = function() {
		$scope.postUpdateEntity('products/update/', $scope.product, $scope.productFrm, $modalInstance);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

ProductModule.controller("ProductAssignCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'product', function ($scope, $http, $location, $modalInstance, ngToast, product) {
	$scope.productAssign = { };
	
	$scope.searchAssignee = function(keywords) {
		return $http.get('employees/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.selectAssignee = function(item, model, label) {
		$scope.productAssign.assignTo = item;
	}
	
	$scope.submit = function(isValid) {
		if (isValid) {
			$scope.productAssign['uniqueId'] = product.uniqueId;
			$scope.productAssign['userNo'] = $scope.currentUser.userNo;
			
			$http.post('products/assign', $scope.productAssign)
				.then(function(response) {
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

ProductModule.controller("ProductDescriptionCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'product', function ($scope, $http, $location, $modalInstance, ngToast, product) {
	$scope.productDesc = { };
	
	$scope.submit = function(isValid) {
		if (isValid) {
			$scope.productDesc['productId'] = product.uniqueId;
			
			$http.post('products/addUpdateDesc', $scope.productDesc)
				.then(function(response) {
					ngToast.create({ className: 'success', content: response.data.message });
					$modalInstance.close(response.data);
				}, function(response) {
					ngToast.create({ className: 'danger', content: response.data.message });
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

ProductModule.controller("ProductSoldOutCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'product', function ($scope, $http, $location, $modalInstance, ngToast, product) {
	
	$scope.submit = function() {
		$http.post('products/soldOut?uniqueId=' + product.uniqueId)
			.then(function(response) {
				$modalInstance.close();
			});
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

ProductModule.controller("DeleteProductCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'product', function ($scope, $http, $location, $modalInstance, ngToast, product) {
	$scope.ok = function() {
		$scope.deleteEntity('products/delete/' + product.uniqueId, $modalInstance);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

ProductModule.controller("EnableBiddingCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'product', function ($scope, $http, $location, $modalInstance, ngToast, product) {
	$scope.productBidding = { };
	
	$http.get('currencies/all').success(function(response) {
		$scope.currencies = response;
		angular.forEach($scope.currencies, function(currency) {
			if (currency.countryCode === $scope.currentUser.defaultCountry.code) {
				$scope.setBiddingCurrency(currency);
			}
		});
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$http.get('units/all').success(function(response) {
		$scope.units = response;
		angular.forEach($scope.units, function(unit) {
			if (unit.countryCode === $scope.currentUser.defaultCountry.code && unit['default'] === true) {
				$scope.setBiddingUnit(unit);
			}
		});
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$scope.setBiddingCurrency = function(biddingCurrency) {
		$scope.productBidding.biddingCurrency = biddingCurrency;
	};
	
	$scope.setBiddingUnit = function(biddingUnit) {
		$scope.productBidding.biddingUnit = biddingUnit;
	};
	
	$scope.submit = function(isValid) {
		if (isValid) {
			$scope.productBidding['uniqueId'] = product.uniqueId;
			$scope.productBidding['userNo'] = $scope.currentUser.userNo;
			
			$http.post('products/enableBidding', $scope.productBidding)
				.then(function(response) {
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

ProductModule.controller("UploadImagesCtrl", ['$scope', '$http', '$location', '$modalInstance', 'Upload', 'ngToast', 'Cropper', '$timeout', 'UtilityService', 'product', function ($scope, $http, $location, $modalInstance, Upload, ngToast, Cropper, $timeout, UtilityService, product) {
	  var file, data;
	  $scope.images = [];
	
	  /**
	   * Method is called every time file input's value changes.
	   * Because of Angular has not ng-change for file inputs a hack is needed -
	   * call `angular.element(this).scope().onFile(this.files[0])`
	   * when input's event is fired.
	   */
	  $scope.onFileSelect = function(blob) {
		  if (!UtilityService.isUndefinedOrNull(blob)) {
			  if (!UtilityService.isUndefinedOrNull($scope.dataUrl)) {
				  hideCropper();
			  }
			  
			  Cropper.encode((file = blob)).then(function(dataUrl) {
				  $scope.dataUrl = dataUrl;
				  $timeout(function() {
					  showCropper();
				  }, 50);  // wait for $digest to set image's src
			  });
		  }
		  
	  };
	
	  /**
	   * Croppers container object should be created in controller's scope
	   * for updates by directive via prototypal inheritance.
	   * Pass a full proxy name to the `ng-cropper-proxy` directive attribute to
	   * enable proxing.
	   */
	  $scope.cropper = {};
	  $scope.cropperProxy = 'cropper.first';
	
	  /**
	   * When there is a cropped image to show encode it to base64 string and
	   * use as a source for an image element.
	   */
	  $scope.preview = function() {
	    if (!file || !data) return;
	    Cropper.crop(file, data).then(Cropper.encode).then(function(dataUrl) {
	      ($scope.preview || ($scope.preview = {})).dataUrl = dataUrl;
	    });
	  };
	
	  /**
	   * Use cropper function proxy to call methods of the plugin.
	   * See https://github.com/fengyuanchen/cropper#methods
	   */
	  $scope.clear = function(degrees) {
	    if (!$scope.cropper.first) return;
	    $scope.cropper.first('clear');
	  };
	
	  $scope.scale = function(width) {
	    Cropper.crop(file, data)
	      .then(function(blob) {
	        return Cropper.scale(blob, {width: width});
	      })
	      .then(Cropper.encode).then(function(dataUrl) {
	        ($scope.preview || ($scope.preview = {})).dataUrl = dataUrl;
	      });
	  }
	
	  /**
	   * Object is used to pass options to initalize a cropper.
	   * More on options - https://github.com/fengyuanchen/cropper#options
	   */
	  $scope.options = {
	    maximize: true,
	    aspectRatio: 6 / 4,
	    crop: function(dataNew) {
	      data = dataNew;
	    }
	  };
	
	  /**
	   * Showing (initializing) and hiding (destroying) of a cropper are started by
	   * events. The scope of the `ng-cropper` directive is derived from the scope of
	   * the controller. When initializing the `ng-cropper` directive adds two handlers
	   * listening to events passed by `ng-cropper-show` & `ng-cropper-hide` attributes.
	   * To show or hide a cropper `$broadcast` a proper event.
	   */
	  $scope.showEvent = 'show';
	  $scope.hideEvent = 'hide';
	
	  function showCropper() { $scope.$broadcast($scope.showEvent); }
	  function hideCropper() { $scope.$broadcast($scope.hideEvent); }
	  
	  $scope.addPicture = function() {
		  $scope.images.push({
			  index: $scope.images.length + 1,
			  dataUrl: $scope.preview.dataUrl 
		  });
	  }
	  
	  $scope.remove = function(index) {
		  $scope.images.splice(index, 1);
	  }
	  
	  $scope.upload = function() {
		  var formData = new FormData();
		  angular.forEach($scope.images, function(image, index) {
			  	var filename = "images" + index + '.' + extension(mimeString(image.dataUrl));
			  	var file = new File([dataURItoBlob(image.dataUrl)], filename, {type:mimeString(image.dataUrl)});
				formData.append('images', file);
			});
		  
		  $http.post('products/uploadImages', formData, {
					transformRequest: angular.identity,
					headers: { 'Content-Type': undefined },
					params: { productId: product.uniqueId }
				}).then(
					function(response) {
						ngToast.create({ className: 'success', content: response.data.message });
						$modalInstance.close(response.data);
					}, function(response) {
						ngToast.create({ className: 'danger', content: response.data.message });
					});
	  }
	
	  function dataURItoBlob(dataURI) {
	    // convert base64/URLEncoded data component to raw binary data held in a string
	    var byteString;
	    if (dataURI.split(',')[0].indexOf('base64') >= 0)
	        byteString = atob(dataURI.split(',')[1]);
	    else
	        byteString = unescape(dataURI.split(',')[1]);

	    // separate out the mime component
	    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

	    // write the bytes of the string to a typed array
	    var ia = new Uint8Array(byteString.length);
	    for (var i = 0; i < byteString.length; i++) {
	        ia[i] = byteString.charCodeAt(i);
	    }

	    var blob = new Blob([ia], {type:mimeString}); 
	    return blob;
	  }
  
	  function mimeString(dataURI) {
		  return dataURI.split(',')[0].split(':')[1].split(';')[0];
	  }
	  
	  function extension(mimeString) {
		  return mimeString.split('/')[1];
	  }

	  $scope.ok = function() {
		  $modalInstance.close({});
	  };

	  $scope.close = function() {
		  $modalInstance.dismiss('cancel');
	  };
}]);

ProductModule.controller("AddAttributeCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'product', function ($scope, $http, $location, $modalInstance, ngToast, product) {
	$scope.productBidding = { };
	
	$http.get('assets/resources/data/json/attributes.json')
		.then(function(response) {
			$scope.attributes = response.data;
		}, function(resp) {
			console.log(resp);
		});
	
	$scope.submit = function(isValid) {
		if (isValid) {
			$scope.productBidding['uniqueId'] = product.uniqueId;
			$scope.productBidding['userNo'] = $scope.currentUser.userNo;
			
			$http.post('products/enableBidding', $scope.productBidding)
				.then(function(response) {
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);