'use strict';

var CategoryModule = angular.module("CategoryModule", []);

CategoryModule.controller("CategoryListCtrl", ['$scope', '$http', '$modal', '$log', 'HttpService', 'UtilityService', 'ngToast', 'usSpinnerService', function ($scope, $http, $modal, $log, HttpService, UtilityService, ngToast, usSpinnerService) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.keyword = null;
	$scope.pageSize = 10;
	$scope.history = false;
	$scope.showMainCategories = false;
	$scope.mainCategories = null;
	$scope.categories = null; 
	$scope.pages = [];
	$scope.sortOrders = {
		"fields": ["name", "parentCategory.name", "createdBy.firstName,createdBy.lastName", "dateCreated", "modifiedBy.firstName,modifiedBy.lastName", "dateModified"],
		"directions": ["asc", "asc", "asc", "asc", "asc" ]
	};
	
	$scope.sortFields = null;
	$scope.sortDirection = null;
	$scope.url = 'categories/all';
	
	$http.get('categories/')
		.then(function(response) {
			$scope.mainCategories = response.data;
			$scope.mainCategory = $scope.mainCategories[0];
			$scope.getCategories();
		});	
	
	$scope.getCategories = function () {
		var params = { 
			keyword: $scope.keyword,
			mainCategories: $scope.showMainCategories,
			sortFields: $scope.sortFields,
			direction: $scope.direction,
			page: $scope.pageNumber, 
			pageSize: $scope.pageSize,
			history: $scope.history, 
		};
		
		if (!UtilityService.isUndefinedOrNull($scope.mainCategory)) {
			params.mainCategoryId = $scope.mainCategory.uniqueId;
		} 
		
		$scope.readData($scope.url, params, 'categories', $scope);
	}

	$scope.changeShowMainCategories = function () {
		if ($scope.showMainCategories) {
			$scope.mainCategory = null;
		} else {
			$scope.mainCategory = $scope.mainCategories[0];
		}
		$scope.getCategories();
	}
	
	$scope.changeMainCategory = function(mainCategory) {
		$scope.mainCategory = mainCategory;
		$scope.getCategories();
	};
	
	$scope.addCategory = function() {
		$scope.addEntityInternal('assets/js/app/site-admin/add-category.component.html', 'CategoryAddCtrl', 'categories/all', 'categories', $scope);
	};
	
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			category: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('detail-category.component.html', 'CategoryDetailCtrl', 'categories/all', 'categories', $scope, resolveObj);
	};
	
	$scope.editDetail = function(uniqueId) {
		var resolveObj = { 
			category: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.editEntityInternal('assets/js/app/site-admin/edit-category.component.html', 'CategoryEditCtrl', 'categories/all', 'categories', $scope, resolveObj);
	};
	
	$scope.remove = function(uniqueId) {
		var resolveObj = { 
			category: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.removeEntityInternal('delete-category.component.html', 'DeleteCategoryCtrl', 'categories/all', 'categories', $scope, resolveObj);
	};

	$scope.prevPage = function() {
		$scope.prevPageInternal('categories/all', 'categories', $scope);
	};
	
	$scope.moveTo = function(page) {
		$scope.moveToInternal('categories/all', 'categories', $scope, page);
	};
	
	$scope.nextPage = function() {
		$scope.nextPageInternal('categories/all', 'categories', $scope);
	};

	$scope.searchByKeyword = function() {
		$scope.searchByKeywordInternal('categories/all', 'categories', $scope);
	};
	
	$scope.sortBy = function(field, direction) {
		$scope.sortByInternal('categories/all', 'categories', $scope, field, direction);
	};
	
}]);

CategoryModule.controller("CategoryAddCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', 'CategoryService', function ($scope, $http, $modalInstance, ngToast, CategoryService) {
	$scope.category = {};
	$scope.parentCategories = null;
	$scope.config = CategoryService.config({});
	
	CategoryService.findParentCategories('categories/')
		.then(function(parentCategories) {
			$scope.parentCategories = parentCategories; 
		});
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$http.post('categories/new/', $scope.category)
				.then(function(response) {
					if (response.data.code === 'success') {
						ngToast.create({ className: 'success', content: response.data.message });
					} else if (response.data.code === 'failure') {
						ngToast.create({ className: 'danger', content: response.data.message });
					}
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

CategoryModule.controller("CategoryDetailCtrl", ['$scope', '$http', '$modalInstance', 'category', function ($scope, $http, $modalInstance, category) {
	$http.get('categories/data/' + category.uniqueId)
		.then(function(response) {
			$scope.category = response.data;
		});
	
	$scope.ok = function() {
		$modalInstance.close();
    };
}]);

CategoryModule.controller("CategoryEditCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', 'category', 'CategoryService', function ($scope, $http, $modalInstance, ngToast, category, CategoryService) {
	$scope.parentCategories = null;
	$scope.config = CategoryService.config({});
	
	CategoryService.findParentCategories('categories/')
		.then(function(parentCategories) {
			$scope.parentCategories = parentCategories; 
		});
	
	$http.get('categories/data/' + category.uniqueId)
		.then(function(response) {
			$scope.category = response.data;
		});
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$scope.postUpdateEntity('categories/update/', $scope.category, $scope.categoryFrm, $modalInstance);
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

CategoryModule.controller("DeleteCategoryCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'category', function ($scope, $http, $location, $modalInstance, ngToast, category) {
	$scope.ok = function() {
		$scope.deleteEntity('categories/delete/' + category.uniqueId, $modalInstance);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);