'use strict';

var CountryModule = angular.module("CountryModule", []);

CountryModule.controller("CountryListCtrl", ['$scope', '$http', '$modal', '$log', 'HttpService', 'ngToast', function ($scope, $http, $modal, $log, HttpService, ngToast) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.keyword = null;
	$scope.pageSize = 10;
	$scope.history = false;
	$scope.countries = null; 
	$scope.pages = [];
	$scope.sortOrders = {
		"fields": ["name", "createdBy.firstName,createdBy.lastName", "dateCreated", "modifiedBy.firstName,modifiedBy.lastName", "dateModified"],
		"directions": ["asc", "asc", "asc", "asc", "asc" ]
	};
	
	$scope.sortFields = null;
	$scope.sortDirection = null;
	$scope.url = 'countries/all';
	
	$scope.getCountries = function () {
		var params = { 
			keyword: $scope.keyword,
			sortFields: $scope.sortFields,
			direction: $scope.direction,
			page: $scope.pageNumber, 
			pageSize: $scope.pageSize,
			history: $scope.history, 
		};

		$scope.readData($scope.url, params, 'countries', $scope);
	}
	
	$scope.getCountries();
	
	$scope.addCountry = function() {
		$scope.addEntityInternal('add-country.component.html', 'CountryAddCtrl', 'countries/all', 'countries', $scope);
	};
	
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			country: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('detail-country.component.html', 'CountryDetailCtrl', 'countries/all', 'countries', $scope, resolveObj);
	};
	
	$scope.editDetail = function(country) {
		if (country.frozen === 'true') {
			ngToast.create({ className: 'danger', content: 'You cannot edit this country. because it is frozon.' });
			return false;
		}
		
		var resolveObj = { 
			country: function() {
				return {
					uniqueId: country.uniqueId
				}
			}
		};
		
		$scope.editEntityInternal('edit-country.component.html', 'CountryEditCtrl', 'countries/all', 'countries', $scope, resolveObj);
	};
	
	$scope.remove = function(uniqueId) {
		var resolveObj = { 
			country: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.removeEntityInternal('delete-country.component.html', 'DeleteCountryCtrl', 'countries/all', 'countries', $scope, resolveObj);
	};

	$scope.prevPage = function() {
		$scope.prevPageInternal('countries/all', 'countries', $scope);
	};
	
	$scope.moveTo = function(page) {
		$scope.moveToInternal('countries/all', 'countries', $scope, page);
	};
	
	$scope.nextPage = function() {
		$scope.nextPageInternal('countries/all', 'countries', $scope);
	};

	$scope.searchByKeyword = function() {
		$scope.searchByKeywordInternal('countries/all', 'countries', $scope);
	};
	
	$scope.sortBy = function(field, direction) {
		$scope.sortByInternal('countries/all', 'countries', $scope, field, direction);
	};
	
}]);

CountryModule.controller("CountryAddCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', function ($scope, $http, $modalInstance, ngToast) {
	$scope.ok = function(isValid) {
		if (isValid) {
			$http.post('countries/new/', $scope.country)
				.then(function(response) {
					if (response.data.code === 'success') {
						ngToast.create({ className: 'success', content: response.data.message });
					} else if (response.data.code === 'failure') {
						ngToast.create({ className: 'danger', content: response.data.message });
					}
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

CountryModule.controller("CountryDetailCtrl", ['$scope', '$http', '$modalInstance', 'country', function ($scope, $http, $modalInstance, country) {
	$http.get('countries/data/' + country.uniqueId)
		.then(function(response) {
			$scope.country = response.data;
		});
	
	$scope.ok = function() {
		$modalInstance.close();
    };
}]);

CountryModule.controller("CountryEditCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', 'country', function ($scope, $http, $modalInstance, ngToast, country) {
	$http.get('countries/data/' + country.uniqueId)
		.then(function(response) {
			$scope.country = response.data;
		});
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$scope.postUpdateEntity('countries/update/', $scope.country, $scope.countryFrm, $modalInstance);
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

CountryModule.controller("DeleteCountryCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'country', function ($scope, $http, $location, $modalInstance, ngToast, country) {
	$scope.ok = function() {
		$scope.deleteEntity('countries/delete/' + country.uniqueId, $modalInstance);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);