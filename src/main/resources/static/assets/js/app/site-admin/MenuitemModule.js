'use strict';

var MenuitemModule = angular.module("MenuitemModule", []);

MenuitemModule.controller("MenuitemListCtrl", ['$scope', '$http', '$modal', '$log', 'HttpService', 'UtilityService', 'ngToast', function ($scope, $http, $modal, $log, HttpService, UtilityService, ngToast) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.keyword = null;
	$scope.pageSize = 10;
	$scope.history = false;
	$scope.showMainMenuitems = false;
	$scope.mainMenuitem = null;
	$scope.menuitems = null; 
	$scope.pages = [];
	$scope.sortOrders = {
		"fields": ["name", "parentMenuitem.name", "createdBy.firstName,createdBy.lastName", "dateCreated", "modifiedBy.firstName,modifiedBy.lastName", "dateModified"],
		"directions": ["asc", "asc", "asc", "asc", "asc" ]
	};
	
	$scope.sortFields = null;
	$scope.sortDirection = null;
	$scope.url = 'menuitems/all';

	$http.get('menuitems/')
		.then(function(response) {
			$scope.mainMenuitems = response.data;
			$scope.mainMenuitem = $scope.mainMenuitems[0];
			$scope.getMenuitems();
		});
	
	$scope.getMenuitems = function () {
		var params = { 
			keyword: $scope.keyword,
			mainMenuitems: $scope.showMainMenuitems,
			sortFields: $scope.sortFields,
			direction: $scope.direction,
			page: $scope.pageNumber, 
			pageSize: $scope.pageSize,
			history: $scope.history, 
		};
	
		if (!UtilityService.isUndefinedOrNull($scope.mainMenuitem)) {
			params.mainMenuitemId = $scope.mainMenuitem.uniqueId;
		} 
		
		$scope.readData($scope.url, params, 'menuitems', $scope);
	}

	$scope.changeShowMainMenuitems = function () {
		if ($scope.showMainMenuitems) {
			$scope.mainMenuitem = null;
		} else {
			$scope.mainMenuitem = $scope.mainMenuitems[0];
		}
		$scope.getMenuitems();
	}
	
	$scope.changeMainMenuitem = function(mainMenuitem) {
		$scope.mainMenuitem = mainMenuitem;
		$scope.getMenuitems();
	};
	
	
	$scope.addMenuitem = function() {
		$scope.addEntityInternal('assets/js/app/site-admin/add-menuitem.component.html', 'MenuitemAddCtrl', 'menuitems/all', 'menuitems', $scope);
	};
	
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			menuitem: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('assets/js/app/site-admin/detail-menuitem.component.html', 'MenuitemDetailCtrl', 'menuitems/all', 'menuitems', $scope, resolveObj);
	};
	
	$scope.editDetail = function(uniqueId) {
		var resolveObj = { 
			menuitem: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.editEntityInternal('assets/js/app/site-admin/edit-menuitem.component.html', 'MenuitemEditCtrl', 'menuitems/all', 'menuitems', $scope, resolveObj);
	};
	
	$scope.remove = function(uniqueId) {
		var resolveObj = { 
			menuitem: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.removeEntityInternal('delete-menuitem.component.html', 'DeleteMenuitemCtrl', 'menuitems/all', 'menuitems', $scope, resolveObj);
	};

	$scope.prevPage = function() {
		$scope.prevPageInternal('menuitems/all', 'menuitems', $scope);
	};
	
	$scope.moveTo = function(page) {
		$scope.moveToInternal('menuitems/all', 'menuitems', $scope, page);
	};
	
	$scope.nextPage = function() {
		$scope.nextPageInternal('menuitems/all', 'menuitems', $scope);
	};

	$scope.searchByKeyword = function() {
		$scope.searchByKeywordInternal('menuitems/all', 'menuitems', $scope);
	};
	
	$scope.sortBy = function(field, direction) {
		$scope.sortByInternal('menuitems/all', 'menuitems', $scope, field, direction);
	};
	
}]);

MenuitemModule.controller("MenuitemAddCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', 'MenuitemService', 'CategoryService', function ($scope, $http, $modalInstance, ngToast, MenuitemService, CategoryService) {
	$scope.menuitem = {}
	$scope.categoryConfig = CategoryService.config({});
	$scope.menuitemConfig = MenuitemService.config({});
	
	CategoryService.findParentCategories('categories/')
		.then(function(categories) {
			$scope.categories = categories; 
		});
	
	MenuitemService.findParentMenuitems('menuitems/')
		.then(function(parentMenuitems) {
			$scope.parentMenuitems = parentMenuitems; 
		});
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$http.post('menuitems/new/', $scope.menuitem)
				.then(function(response) {
					if (response.data.code === 'success') {
						ngToast.create({ className: 'success', content: response.data.message });
					} else if (response.data.code === 'failure') {
						ngToast.create({ className: 'danger', content: response.data.message });
					}
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

MenuitemModule.controller("MenuitemDetailCtrl", ['$scope', '$http', '$modalInstance', 'menuitem', function ($scope, $http, $modalInstance, menuitem) {
	$http.get('menuitems/data/' + menuitem.uniqueId)
		.then(function(response) {
			$scope.menuitem = response.data;
		});
	
	$scope.ok = function() {
		$modalInstance.close();
    };
}]);

MenuitemModule.controller("MenuitemEditCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', 'MenuitemService', 'CategoryService', 'menuitem', function ($scope, $http, $modalInstance, ngToast, MenuitemService, CategoryService, menuitem) {
	$scope.categoryConfig = CategoryService.config({});
	$scope.menuitemConfig = MenuitemService.config({});
	
	CategoryService.findParentCategories('categories/')
		.then(function(categories) {
			$scope.categories = categories; 
		});
	
	MenuitemService.findParentMenuitems('menuitems/')
		.then(function(parentMenuitems) {
			$scope.parentMenuitems = parentMenuitems; 
		});
	/*
	$http.get('categories/')
		.then(function(response) {
			$scope.categories = response.data;
		});	
	
	$http.get('menuitems/')
		.then(function(response) {
			$scope.parentMenuitems = response.data;
		});

	$scope.setParentMenuitem = function(parentMenuitem) {
		$scope.menuitem.parentMenuitemId = parentMenuitem.uniqueId;
		$scope.menuitem.parentMenuitem = parentMenuitem;
	};
	
	$scope.setCategory = function(category) {
		$scope.menuitem.categoryId = category.uniqueId;
		$scope.menuitem.category = category;
	};
	*/
	$http.get('menuitems/data/' + menuitem.uniqueId)
		.then(function(response) {
			$scope.menuitem = response.data;
		});
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$scope.postUpdateEntity('menuitems/update/', $scope.menuitem, $scope.menuitemFrm, $modalInstance);
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

MenuitemModule.controller("DeleteMenuitemCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'menuitem', function ($scope, $http, $location, $modalInstance, ngToast, menuitem) {
	$scope.ok = function() {
		$scope.deleteEntity('menuitems/delete/' + menuitem.uniqueId, $modalInstance);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);