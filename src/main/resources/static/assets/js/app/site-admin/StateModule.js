'use strict';

var StateModule = angular.module("StateModule", []);

StateModule.controller("StateListCtrl", ['$scope', '$http', '$modal', '$log', 'HttpService', 'ngToast', function ($scope, $http, $modal, $log, HttpService, ngToast) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.keyword = null;
	$scope.pageSize = 10;
	$scope.history = false;
	$scope.states = null; 
	$scope.pages = [];
	$scope.sortOrders = {
		"fields": ["name", "country.name", "createdBy.firstName,createdBy.lastName", "dateCreated", "modifiedBy.firstName,modifiedBy.lastName", "dateModified"],
		"directions": ["asc", "asc", "asc", "asc", "asc" ]
	};
	
	$scope.sortFields = null;
	$scope.sortDirection = null;
	$scope.url = 'states/all';
	
	$scope.getStates = function () {
		var params = { 
			keyword: $scope.keyword,
			sortFields: $scope.sortFields,
			direction: $scope.direction,
			page: $scope.pageNumber, 
			pageSize: $scope.pageSize,
			history: $scope.history, 
		};

		$scope.readData($scope.url, params, 'states', $scope);
	};

	$scope.getStates();
	
	$scope.addState = function() {
		$scope.addEntityInternal('add-state.component.html', 'StateAddCtrl', 'states/all', 'states', $scope);
	};
	
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			state: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('detail-state.component.html', 'StateDetailCtrl', 'states/all', 'states', $scope, resolveObj);
	};
	
	$scope.editDetail = function(uniqueId) {
		var resolveObj = { 
			state: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.editEntityInternal('edit-state.component.html', 'StateEditCtrl', 'states/all', 'states', $scope, resolveObj);
	};
	
	$scope.remove = function(uniqueId) {
		var resolveObj = { 
			state: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.removeEntityInternal('delete-state.component.html', 'DeleteStateCtrl', 'states/all', 'states', $scope, resolveObj);
	};

	$scope.prevPage = function() {
		$scope.prevPageInternal('states/all', 'states', $scope);
	};
	
	$scope.moveTo = function(page) {
		$scope.moveToInternal('states/all', 'states', $scope, page);
	};
	
	$scope.nextPage = function() {
		$scope.nextPageInternal('states/all', 'states', $scope);
	};

	$scope.searchByKeyword = function() {
		$scope.searchByKeywordInternal('states/all', 'states', $scope);
	};
	
	$scope.sortBy = function(field, direction) {
		$scope.sortByInternal('states/all', 'states', $scope, field, direction);
	};
	
}]);

StateModule.controller("StateAddCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', function ($scope, $http, $modalInstance, ngToast) {
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$http.post('states/new/', $scope.state)
				.then(function(response) {
					if (response.data.code === 'success') {
						ngToast.create({ className: 'success', content: response.data.message });
					} else if (response.data.code === 'failure') {
						ngToast.create({ className: 'danger', content: response.data.message });
					}
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

StateModule.controller("StateDetailCtrl", ['$scope', '$http', '$modalInstance', 'state', function ($scope, $http, $modalInstance, state) {
	$http.get('states/data/' + state.uniqueId)
		.then(function(response) {
			$scope.state = response.data;
		});
	
	$scope.ok = function() {
		$modalInstance.close();
    };
}]);

StateModule.controller("StateEditCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', 'state', function ($scope, $http, $modalInstance, ngToast, state) {
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/data/' + state.uniqueId)
		.then(function(response) {
			$scope.state = response.data;
		});
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$scope.postUpdateEntity('states/update/', $scope.state, $scope.stateFrm, $modalInstance);
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

StateModule.controller("DeleteStateCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'state', function ($scope, $http, $location, $modalInstance, ngToast, state) {
	$scope.ok = function() {
		$scope.deleteEntity('states/delete/' + state.uniqueId, $modalInstance);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);