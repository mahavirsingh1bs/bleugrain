'use strict';

var BankModule = angular.module("BankModule", []);

BankModule.controller("BankListCtrl", ['$scope', '$http', '$modal', '$log', 'HttpService', 'ngToast', function ($scope, $http, $modal, $log, HttpService, ngToast) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.keyword = null;
	$scope.pageSize = 10;
	$scope.history = false;
	$scope.banks = null; 
	$scope.pages = [];
	$scope.sortOrders = {
		"fields": ["name", "createdBy.firstName,createdBy.lastName", "dateCreated", "modifiedBy.firstName,modifiedBy.lastName", "dateModified"],
		"directions": ["asc", "asc", "asc", "asc", "asc" ]
	};
	
	$scope.sortFields = null;
	$scope.sortDirection = null;
	$scope.url = 'banks/all';
	
	$scope.getBanks = function () {
		var params = { 
			keyword: $scope.keyword,
			sortFields: $scope.sortFields,
			direction: $scope.direction,
			page: $scope.pageNumber, 
			pageSize: $scope.pageSize,
			history: $scope.history, 
		};

		$scope.readData($scope.url, params, 'banks', $scope);
	}
	
	$scope.getBanks();
	
	$scope.addBank = function() {
		$scope.addEntityInternal('add-bank.component.html', 'BankAddCtrl', 'banks/all', 'banks', $scope);
	};
	
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			bank: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('detail-bank.component.html', 'BankDetailCtrl', 'banks/all', 'banks', $scope, resolveObj);
	};
	
	$scope.editDetail = function(uniqueId) {
		var resolveObj = { 
			bank: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.editEntityInternal('edit-bank.component.html', 'BankEditCtrl', 'banks/all', 'banks', $scope, resolveObj);
	};
	
	$scope.remove = function(uniqueId) {
		var resolveObj = { 
			bank: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.removeEntityInternal('delete-bank.component.html', 'DeleteBankCtrl', 'banks/all', 'banks', $scope, resolveObj);
	};

	$scope.prevPage = function() {
		$scope.prevPageInternal('banks/all', 'banks', $scope);
	};
	
	$scope.moveTo = function(page) {
		$scope.moveToInternal('banks/all', 'banks', $scope, page);
	};
	
	$scope.nextPage = function() {
		$scope.nextPageInternal('banks/all', 'banks', $scope);
	};

	$scope.searchByKeyword = function() {
		$scope.searchByKeywordInternal('banks/all', 'banks', $scope);
	};
	
	$scope.sortBy = function(field, direction) {
		$scope.sortByInternal('banks/all', 'banks', $scope, field, direction);
	};
	
}]);

BankModule.controller("BankAddCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', function ($scope, $http, $modalInstance, ngToast) {
	$scope.ok = function(isValid) {
		if (isValid) {
			$http.post('banks/new/', $scope.bank)
				.then(function(response) {
					if (response.data.code === 'success') {
						ngToast.create({ className: 'success', content: response.data.message });
						$modalInstance.close();
					} else if (response.data.code === 'failure') {
						ngToast.create({ className: 'error', content: response.data.message });
					}
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

BankModule.controller("BankDetailCtrl", ['$scope', '$http', '$modalInstance', 'bank', function ($scope, $http, $modalInstance, bank) {
	$http.get('banks/data/' + bank.uniqueId)
		.then(function(response) {
			$scope.bank = response.data;
		});
	
	$scope.ok = function() {
		$modalInstance.close();
    };
}]);

BankModule.controller("BankEditCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', 'bank', function ($scope, $http, $modalInstance, ngToast, bank) {
	$http.get('banks/data/' + bank.uniqueId)
		.then(function(response) {
			$scope.bank = response.data;
		});
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$scope.postUpdateEntity('banks/update/', $scope.bank, $scope.bankFrm, $modalInstance);
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

BankModule.controller("DeleteBankCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'bank', function ($scope, $http, $location, $modalInstance, ngToast, bank) {
	$scope.ok = function() {
		$scope.deleteEntity('banks/delete/' + bank.uniqueId, $modalInstance);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);