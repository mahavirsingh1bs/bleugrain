'use strict';

var SuperAdminModule = angular.module("SuperAdminModule", []);

SuperAdminModule.controller("SuperAdminListCtrl", ['$scope', '$http', '$modal', '$log', 'HttpService', 'ngToast', function ($scope, $http, $modal, $log, HttpService, ngToast) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.keyword = null;
	$scope.pageSize = 10;
	$scope.history = false;
	$scope.superAdmins = null; 
	$scope.pages = [];
	$scope.sortOrders = {
		"fields": ["firstName,lastName", "phoneNo", "email", "createdBy.firstName,createdBy.lastName", "dateModified", "modifiedBy.firstName,modifiedBy.lastName", "dateModified"],
		"directions": ["asc", "asc", "asc", "asc", "asc" ]
	};
	
	$scope.sortFields = $scope.sortOrders['fields'][0];
	$scope.sortDirection = $scope.sortOrders['directions'][0];
	$scope.url = 'superAdmins/all';
	
	$scope.getSuperAdmins = function () {
		var params = { 
			keyword: $scope.keyword,
			sortFields: $scope.sortFields,
			direction: $scope.direction,
			page: $scope.pageNumber, 
			pageSize: $scope.pageSize,
			history: $scope.history, 
		};
		
		$scope.readData($scope.url, params, 'superAdmins', $scope);
	};

	$scope.getSuperAdmins();
	
	$scope.addSuperAdmin = function() {
		$scope.addEntityInternal('assets/js/app/employees/add-super-admin.component.html', 'SuperAdminAddCtrl', 'superAdmins/all', 'superAdmins', $scope);
	};
	
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			superAdmin: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('assets/js/app/employees/detail-super-admin.component.html', 'SuperAdminDetailCtrl', 'superAdmins/all', 'superAdmins', $scope, resolveObj);
	};
	
	$scope.editDetail = function(uniqueId) {
		var resolveObj = { 
			superAdmin: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.editEntityInternal('assets/js/app/employees/edit-super-admin.component.html', 'SuperAdminEditCtrl', 'superAdmins/all', 'superAdmins', $scope, resolveObj);
	};
	
	$scope.remove = function(uniqueId) {
		var resolveObj = { 
			superAdmin: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.removeEntityInternal('delete-super-admin.component.html', 'DeleteSuperAdminCtrl', 'superAdmins/all', 'superAdmins', $scope, resolveObj);
	};

	$scope.prevPage = function() {
		$scope.prevPageInternal('superAdmins/all', 'superAdmins', $scope);
	};
	
	$scope.moveTo = function(page) {
		$scope.moveToInternal('superAdmins/all', 'superAdmins', $scope, page);
	};
	
	$scope.nextPage = function() {
		$scope.nextPageInternal('superAdmins/all', 'superAdmins', $scope);
	};

	$scope.searchByKeyword = function() {
		$scope.searchByKeywordInternal('superAdmins/all', 'superAdmins', $scope);
	};
	
	$scope.sortBy = function(field, direction) {
		$scope.sortByInternal('superAdmins/all', 'superAdmins', $scope, field, direction);
	};
	
}]);

SuperAdminModule.controller("SuperAdminAddCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'usSpinnerService', 'FormUtils', function ($scope, $http, $location, $modalInstance, ngToast, usSpinnerService, FormUtils) {
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
		});
	
	$scope.ok = function(isValid) {
		if (isValid) {
			var formData = new FormData();
			formData.append('superAdmin', new Blob([JSON.stringify($scope.superAdmin)], { type: "application/json" }));
			formData.append('superAdminImage', $scope.superAdminImage);
			$scope.postCreateEntity('superAdmins/new', formData, $scope.superAdminFrm, $modalInstance);
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
    
}]);

SuperAdminModule.controller("SuperAdminDetailCtrl", ['$scope', '$http', '$modalInstance', 'superAdmin', function ($scope, $http, $modalInstance, superAdmin) {
	$http.get('superAdmins/data/' + superAdmin.uniqueId)
		.then(function(response) {
			$scope.superAdmin = response.data;
		});
	
	$scope.close = function() {
		$modalInstance.close();
    };
}]);

SuperAdminModule.controller("SuperAdminEditCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', 'superAdmin', function ($scope, $http, $modalInstance, ngToast, superAdmin) {
	
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
		});
	
	$http.get('superAdmins/data/' + superAdmin.uniqueId)
		.then(function(response) {
			$scope.superAdmin = response.data;
		});
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$scope.postUpdateEntity('superAdmins/update/', $scope.superAdmin, $scope.superAdminFrm, $modalInstance);
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

SuperAdminModule.controller("DeleteSuperAdminCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'superAdmin', function ($scope, $http, $location, $modalInstance, ngToast, superAdmin) {
	$scope.ok = function() {
		$scope.deleteEntity('superAdmins/delete/' + superAdmin.uniqueId, $modalInstance);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);