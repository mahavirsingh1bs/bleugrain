'use strict';

var EmployeeModule = angular.module("EmployeeModule", []);

EmployeeModule.controller("EmployeeListCtrl", ['$scope', '$http', '$modal', '$log', 'HttpService', 'ngToast', function ($scope, $http, $modal, $log, HttpService, ngToast) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.keyword = null;
	$scope.pageSize = 10;
	$scope.history = false;
	$scope.employees = null; 
	$scope.pages = [];
	$scope.sortOrders = {
		"fields": ["firstName,lastName", "phoneNo", "email", "employeeId", "designation.name", "createdBy.firstName,createdBy.lastName", "modifiedBy.firstName,modifiedBy.lastName"],
		"directions": ["asc", "asc", "asc", "asc", "asc" ]
	};
	
	$scope.sortFields = $scope.sortOrders['fields'][0];
	$scope.sortDirection = $scope.sortOrders['directions'][0];
	$scope.url = 'employees/all';
	
	$scope.getEmployees = function() {
		var params = { 
			keyword: $scope.keyword,
			sortFields: $scope.sortFields,
			direction: $scope.direction,
			page: $scope.pageNumber, 
			pageSize: $scope.pageSize,
			history: $scope.history, 
		};

		$scope.readData($scope.url, params, 'employees', $scope);
	};
	
	$scope.getEmployees();
	
	$scope.addEmployee = function() {
		$scope.addEntityInternal('assets/js/app/employees/add-employee.component.html', 'EmployeeAddCtrl', 'employees/all', 'employees', $scope);
	};
	
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			employee: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('assets/js/app/employees/detail-employee.component.html', 'EmployeeDetailCtrl', 'employees/all', 'employees', $scope, resolveObj);
	};
	
	$scope.editDetail = function(uniqueId) {
		var resolveObj = { 
			employee: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.editEntityInternal('assets/js/app/employees/edit-employee.component.html', 'EmployeeEditCtrl', 'employees/all', 'employees', $scope, resolveObj);
	};
	
	$scope.editRoles = function(uniqueId) {
		var resolveObj = { 
			employee: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/employees/modify-roles.component.html',
  			controller : 'ModifyRolesCtrl',
  			size : 'md',
	  		resolve: resolveObj,
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function() {
  			$scope.getEmployees();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
	};
	
	$scope.remove = function(uniqueId) {
		var resolveObj = { 
			employee: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.removeEntityInternal('delete-employee.component.html', 'DeleteEmployeeCtrl', 'employees/all', 'employees', $scope, resolveObj);
	};

	$scope.prevPage = function() {
		$scope.prevPageInternal('employees/all', 'employees', $scope);
	};
	
	$scope.moveTo = function(page) {
		$scope.moveToInternal('employees/all', 'employees', $scope, page);
	};
	
	$scope.nextPage = function() {
		$scope.nextPageInternal('employees/all', 'employees', $scope);
	};

	$scope.searchByKeyword = function() {
		$scope.searchByKeywordInternal('employees/all', 'employees', $scope);
	};
	
	$scope.sortBy = function(field, direction) {
		$scope.sortByInternal('employees/all', 'employees', $scope, field, direction);
	};
	
}]);

EmployeeModule.controller("EmployeeAddCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'usSpinnerService', 'FormUtils', function ($scope, $http, $location, $modalInstance, ngToast, usSpinnerService, FormUtils) {
	$scope.employeeImage = {};
	$http.get('departments/all')
		.then(function(response) {
			$scope.departments = response.data.departments;
		});
	
	$http.get('designations/all')
		.then(function(response) {
			$scope.designations = response.data.designations;
		});

	
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
		});
	
	$scope.ok = function(isValid) {
		if (isValid) {
			var formData = new FormData();
			
			formData.append('employee', new Blob([JSON.stringify($scope.employee)], { type: "application/json" }));
			formData.append('employeeImage', $scope.employeeImage);
			$scope.postCreateEntity('employees/new', formData, $scope.employeeFrm, $modalInstance);
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

EmployeeModule.controller("EmployeeDetailCtrl", ['$scope', '$http', '$stateParams', '$modalInstance', 'employee', function ($scope, $http, $stateParams, $modalInstance, employee) {
	$http.get('employees/data/' + employee.uniqueId)
		.then(function(response) {
			$scope.employee = response.data;
		});
	
	$scope.close = function() {
		$modalInstance.close();
	};
	
}]);

EmployeeModule.controller("EmployeeEditCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', 'employee', function ($scope, $http, $modalInstance, ngToast, employee) {
	$http.get('departments/all')
		.then(function(response) {
			$scope.departments = response.data.departments;
		});
	
	$http.get('designations/all')
		.then(function(response) {
			$scope.designations = response.data.designations;
		});

	
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
		});
	
	$http.get('employees/data/' + employee.uniqueId)
		.then(function(response) {
			$scope.employee = response.data;
		});
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$scope.postUpdateEntity('employees/update/', $scope.employee, $scope.employeeFrm, $modalInstance);
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

EmployeeModule.controller("ModifyRolesCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'employee', function ($scope, $http, $location, $modalInstance, ngToast, employee) {
	$http.get('roles/')
		.then(function(response) {
			$scope.roles = response.data;
		});
	
	$scope.ok = function() {
		$http.post('employees/modifyRoles' + employee.uniqueId)
			.then(function(response) {
				if (response.data.code === 'success') {
					ngToast.create({ className: 'success', content: response.data.message });
				} else if (response.data.code === 'error') {
					ngToast.create({ className: 'danger', content: response.data.message });
				}
				$modalInstance.close();
			});
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

EmployeeModule.controller("DeleteEmployeeCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'employee', function ($scope, $http, $location, $modalInstance, ngToast, employee) {
	$scope.ok = function() {
		$scope.deleteEntity('employees/delete/' + employee.uniqueId, $modalInstance);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

EmployeeModule.controller("CTMemberListCtrl", ['$scope', '$http', function ($scope, $http) {
	$scope.pageNumber = 0;
	$scope.keywords = null;
	$scope.pageSize = 10;
	$scope.pages = [];
	
	var data = { keyword : null, draw: 0, start : $scope.pageNumber, length : $scope.pageSize};
	$http.get('admin/contentTeam', { params : data}).success(function(response) {
		$scope.contentTeam = response.data || {};
		$scope.totalPage = Math.ceil(response.recordsTotal / $scope.pageSize);
		for (var i = 0; i < $scope.totalPage; i++) {
			$scope.pages.push(i + 1);
		}
	}).error(function(data) {
		console.log('error occurred');
	});
	
}]);

EmployeeModule.controller("STMemberListCtrl", ['$scope', '$http', function ($scope, $http) {
	$scope.pageNumber = 0;
	$scope.keywords = null;
	$scope.pageSize = 10;
	$scope.pages = [];
	
	var data = { keyword : null, draw: 0, start : $scope.pageNumber, length : $scope.pageSize};
	$http.get('admin/salesTeam', { params : data}).success(function(response) {
		$scope.salesTeam = response.data || {};
		$scope.totalPage = Math.ceil(response.recordsTotal / $scope.pageSize);
		for (var i = 0; i < $scope.totalPage; i++) {
			$scope.pages.push(i + 1);
		}
	}).error(function(data) {
		console.log('error occurred');
	});
	
}]);