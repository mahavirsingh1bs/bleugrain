'use strict';

var AppModule = angular.module('AppModule', [
   'ui.router',
   'CoreDirectives',
   'utility',
   'CoreModule',
   'UserPrefsModule',
   'ProfileModule',
   
   'AddressModule',
   'DiscussionModule',
   'ExpertiseModule',
   'NoteModule',
   'SocialAccountModule',
   'UpcomingEventModule',
   'NotificationModule',
   
   'CompanyModule',
   'UserModule',
   'SuperAdminModule',
   'CompanyAdminModule',
   'AgentModule',
   'BrokerModule',
   'RetailerModule',
   'FarmerModule',
   'CustomerModule',
   'EmployeeModule',
   'ServiceModule',
   'PaymentModule',
   'CategoryModule',
   'MenuitemModule',
   'ProductModule',
   'ConsumerProductModule',
   'CountryModule',
   'StateModule',
   'BankModule',
   'DepartmentModule',
   'DesignationModule',
   'GroupModule',
   'RoleModule',
   'CheckoutModule',
   'SharedServiceModule',
   'ReviewModule',
   'BankAccountModule',
   'CreditCardModule',
   'HistoryModule',
   'BranchModule',
   'TransportModule',
   'DriverModule',
   'OrderModule',
   'DemandModule',
   'OthersModule',
   'CoreService',
   'HttpModule',
   'AccessCtrlModule',
   'CoreConstants',
   'CoreFilters',
   'CartServiceModule',
   'GrainCartModule',
   'WishlistModule',
   'PricingModule',
   'ProductSearchModule',
   'DemandSearchModule',
   'BidModule',
   'UtilityModule',
   'CipherModule',
   'PermitModule',

   'ui.bootstrap',
   'ui.mask',
   'pascalprecht.translate',
   'ui-ngToast',
   'trNgGrid',
   'internationalPhoneNumber',
   'ngSanitize',
   'ngImgCrop',
   'ngCropper',
   'ngDroplet',
   'ngToast',
   'ngTagsInput',
   'ui.utils.masks',
   'chart.js',
   'angular-elevate-zoom',
   'ngIdle',
   'btorfs.multiselect',
   'angularSpinner',
   'textAngular',
   'selectize'
]);

AppModule.config(['$stateProvider',
	function ($stateProvider) {
	   $stateProvider
			.state('index', {
				url: '/',
				templateUrl: 'assets/js/app/index.component.html',
				controller: 'WelcomeCtrl',
				data: {
					authorizedRoles: ['ROLE_GUEST']
				}
			})
			.state('login', {
				url: '/login',
				templateUrl: 'assets/js/app/account/login.component.html',
				controller: 'LoginCtrl',
				data: {
					authorizedRoles: ['ROLE_GUEST']
				}
			})
			.state('register', {
				url: '/register',
				templateUrl: 'assets/js/app/account/register.component.html',
				controller: 'RegisterCtrl',
				data: {
					authorizedRoles: ['ROLE_GUEST']
				}
			})
			.state('accountVerifyContactNo', {
				url: '/verifyContactNo?code',
				templateUrl: 'assets/js/app/account/verify-contact-details.component.html',
				controller: 'VerifyContactNoCtrl',
				data: {
					authorizedRoles: ['ROLE_GUEST']
				}
			})
			.state('accountVerifyEmailAddress', {
				url: '/VerifyEmailAddress?verificationCode',
				templateUrl: 'assets/js/app/account/email-verification.component.html',
				controller: 'VerifyEmailAddressCtrl',
				data: {
					authorizedRoles: ['ROLE_GUEST']
				}
			})
			.state('accountThankYou', {
				url: '/thankYou',
				templateUrl: 'assets/js/app/account/thank-you.component.html',
				data: {
					authorizedRoles: ['ROLE_GUEST']
				}
			})
			.state('accountForgotPasswd', {
				url: '/ForgotPasswd',
				templateUrl: 'assets/js/app/account/forgot-passwd.component.html',
				controller: 'AccountCtrl',
				data: {
					authorizedRoles: ['ROLE_GUEST']
				}
			})
			.state('accountResetPasswd', {
				url: '/ResetPasswd?resetCode',
				templateUrl: 'assets/js/app/account/reset-passwd.component.html',
				controller: 'ResetPasswdCtrl',
				data: {
					authorizedRoles: ['ROLE_GUEST']
				}
			})
			.state('bidding', {
				url: '/bidding',
				templateUrl: 'assets/js/app/bidding/html/bidding.component.html',
				controller: 'BidListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('home', {
				url: '/home',
				templateUrl: 'assets/js/app/profile/dashboard.component.html',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('profileDashboard', {
				url: '/dashboard',
				templateUrl: 'assets/js/app/profile/dashboard.component.html',
				controller: 'ProfileCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('profileMe', {
				url: '/profile/me',
				templateUrl: 'assets/js/app/profile/profile.component.html',
				controller: 'MyProfileCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('profileCompany', {
				url: '/profile/company',
				templateUrl: 'assets/js/app/profile/company.component.html',
				controller: 'CompanyProfileCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('profileReviews', {
				url: '/profile/reviews',
				templateUrl: 'assets/js/app/profile/reviews.component.html',
				controller: 'ReviewCtrl',
				data: {
					authorizedRoles: ['ROLE_USER', 'ROLE_ADMIN']
				}
			})
			.state('profileBankAccounts', {
				url: '/profile/bankAccounts',
				templateUrl: 'assets/js/app/profile/bank-accounts.component.html',
				controller: 'BankAccountCtrl',
				data: {
					authorizedRoles: ['ROLE_USER', 'ROLE_ADMIN']
				}
			})
			.state('profileCreditCards', {
				url: '/profile/creditCards',
				templateUrl: 'assets/js/app/profile/credit-cards.component.html',
				controller: 'CreditCardCtrl',
				data: {
					authorizedRoles: ['ROLE_USER', 'ROLE_ADMIN']
				}
			})
			.state('profileProducts', {
				url: '/profile/products',
				templateUrl: 'assets/js/app/profile/products.component.html',
				controller: 'ProductListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('profileDemands', {
				url: '/profile/demands',
				templateUrl: 'assets/js/app/profile/demands.component.html',
				controller: 'DemandListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('profileTransports', {
				url: '/profile/transports',
				templateUrl: 'assets/js/app/profile/transports.component.html',
				controller: 'TransportListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('profileDrivers', {
				url: '/profile/drivers',
				templateUrl: 'assets/js/app/profile/drivers.component.html',
				controller: 'DriverListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('profileHistory', {
				url: '/profile/history',
				templateUrl: 'assets/js/app/profile/history.component.html',
				controller: 'HistoryCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('profileOrders', {
				url: '/profile/orders',
				templateUrl: 'assets/js/app/profile/orders.component.html',
				controller: 'OrderListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('profileBids', {
				url: '/profile/bids',
				templateUrl: 'assets/js/app/profile/bids.component.html',
				controller: 'ProfileBidListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('profileSettings', {
				url: '/profile/settings',
				templateUrl: 'assets/js/app/profile/settings.component.html',
				controller: 'SettingsCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('searchProductList', {
				url: '/search/list/product/{keywords}',
				templateUrl: 'assets/js/app/search/html/product-list.component.html',
				controller: 'ProductSearchListCtrl',
				data: {
					authorizedRoles: ['ROLE_GUEST', 'ROLE_ADMIN', 'ROLE_COMPANY_ADMIN', 'ROLE_BROKER', 'ROLE_AGENT', 'ROLE_RETAILER', 'ROLE_FARMER']
				}
			})
			.state('searchProductGrid', {
				url: '/search/grid/product/{keywords}',
				templateUrl: 'assets/js/app/search/html/product-grid.component.html',
				controller: 'ProductSearchGridCtrl',
				data: {
					authorizedRoles: ['ROLE_GUEST', 'ROLE_ADMIN', 'ROLE_COMPANY_ADMIN', 'ROLE_BROKER', 'ROLE_AGENT', 'ROLE_RETAILER', 'ROLE_FARMER']
				}
			})
			.state('searchProductListByCategory', {
				url: '/search/list-category/product/{uniqueId}',
				templateUrl: 'assets/js/app/search/html/product-list.component.html',
				controller: 'ProductSearchListCtrl',
				data: {
					authorizedRoles: ['ROLE_GUEST', 'ROLE_ADMIN', 'ROLE_COMPANY_ADMIN', 'ROLE_BROKER', 'ROLE_AGENT', 'ROLE_RETAILER', 'ROLE_FARMER']
				}
			})
			.state('searchProductGridByCategory', {
				url: '/search/grid-category/product/{uniqueId}',
				templateUrl: 'assets/js/app/search/html/product-grid.component.html',
				controller: 'ProductSearchGridCtrl',
				data: {
					authorizedRoles: ['ROLE_GUEST', 'ROLE_ADMIN', 'ROLE_COMPANY_ADMIN', 'ROLE_BROKER', 'ROLE_AGENT', 'ROLE_RETAILER', 'ROLE_FARMER']
				}
			})
			.state('searchDemandList', {
				url: '/search/list/demand/{keywords}',
				templateUrl: 'assets/js/app/search/html/demand-list.component.html',
				controller: 'DemandSearchListCtrl',
				data: {
					authorizedRoles: ['ROLE_GUEST', 'ROLE_ADMIN', 'ROLE_COMPANY_ADMIN', 'ROLE_BROKER', 'ROLE_AGENT', 'ROLE_RETAILER', 'ROLE_FARMER']
				}
			})
			.state('searchDemandGrid', {
				url: '/search/grid/demand/{keywords}',
				templateUrl: 'assets/js/app/search/html/demand-grid.component.html',
				controller: 'DemandSearchGridCtrl',
				data: {
					authorizedRoles: ['ROLE_GUEST', 'ROLE_ADMIN', 'ROLE_COMPANY_ADMIN', 'ROLE_BROKER', 'ROLE_AGENT', 'ROLE_RETAILER', 'ROLE_FARMER']
				}
			})
			.state('searchDemandListByCategory', {
				url: '/search/list-category/demand/{uniqueId}',
				templateUrl: 'assets/js/app/search/html/demand-list.component.html',
				controller: 'DemandSearchListCtrl',
				data: {
					authorizedRoles: ['ROLE_GUEST', 'ROLE_ADMIN', 'ROLE_COMPANY_ADMIN', 'ROLE_BROKER', 'ROLE_AGENT', 'ROLE_RETAILER', 'ROLE_FARMER']
				}
			})
			.state('searchDemandGridByCategory', {
				url: '/search/grid-category/demand/{uniqueId}',
				templateUrl: 'assets/js/app/search/html/demand-grid.component.html',
				controller: 'DemandSearchGridCtrl',
				data: {
					authorizedRoles: ['ROLE_GUEST', 'ROLE_ADMIN', 'ROLE_COMPANY_ADMIN', 'ROLE_BROKER', 'ROLE_AGENT', 'ROLE_RETAILER', 'ROLE_FARMER']
				}
			})
			.state('searchProductDetail', {
				url: '/search/product?uniqueId&layout&categoryId&keywords',
				templateUrl: 'assets/js/app/search/html/product-detail.component.html',
				controller: 'SearchProductDetailCtrl',
				data: {
					authorizedRoles: ['ROLE_GUEST', 'ROLE_ADMIN', 'ROLE_COMPANY_ADMIN', 'ROLE_BROKER', 'ROLE_AGENT', 'ROLE_RETAILER', 'ROLE_FARMER']
				}
			})
			.state('checkout', {
				url: '/checkout',
				templateUrl: 'assets/js/app/checkout/checkout.component.html',
				controller: 'CheckoutCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('wishlist', {
				url: '/wishlist',
				templateUrl: 'assets/js/app/checkout/wishlist.component.html',
				controller: 'WishlistCtrl',
				data: {
					authorizedRoles: ['ROLE_USER', 'ROLE_ADMIN']
				}
			})
			.state('servicesCompany', {
				url: '/services/companies',
				templateUrl: 'assets/js/app/companies/companies.component.html',
				controller: 'CompanyListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('servicesBrokers', {
				url: '/services/brokers',
				templateUrl: 'assets/js/app/brokers/brokers.component.html',
				controller: 'BrokerListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('servicesRetailers', {
				url: '/services/retailers',
				templateUrl: 'assets/js/app/retailers/retailers.component.html',
				controller: 'RetailerListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('servicesCustomers', {
				url: '/services/customers',
				templateUrl: 'assets/js/app/customers/customers.component.html',
				controller: 'CustomerListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('servicesFarmers', {
				url: '/services/farmers',
				templateUrl: 'assets/js/app/farmers/farmers.component.html',
				controller: 'FarmerListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('servicesAgents', {
				url: '/services/agents',
				templateUrl: 'assets/js/app/companies/agents.component.html',
				controller: 'AgentListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('servicesCompanyAdmins', {
				url: '/services/companyAdmins',
				templateUrl: 'assets/js/app/companies/company-admins.component.html',
				controller: 'CompanyAdminListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('servicesEmployees', {
				url: '/services/employees',
				templateUrl: 'assets/js/app/employees/employees.component.html',
				controller: 'EmployeeListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('servicesSuperAdmins', {
				url: '/services/superAdmins',
				templateUrl: 'assets/js/app/employees/super-admins.component.html',
				controller: 'SuperAdminListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('servicesProducts', {
				url: '/services/products',
				templateUrl: 'assets/js/app/products/products.component.html',
				controller: 'ProductListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('servicesOrders', {
				url: '/services/orders',
				templateUrl: 'assets/js/app/orders/orders.component.html',
				controller: 'OrderListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('servicesDemands', {
				url: '/services/demands',
				templateUrl: 'assets/js/app/demands/demands.component.html',
				controller: 'DemandListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('servicesPricing', {
				url: '/services/pricing',
				templateUrl: 'assets/js/app/pricing/pricing.component.html',
				controller: 'PricingListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('servicesPaymentMethods', {
				url: '/services/paymentMethods',
				templateUrl: 'assets/js/app/payment-options/payment-methods.component.html',
				controller: 'PaymentMethodListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('servicesPaymentCards', {
				url: '/services/paymentCards',
				templateUrl: 'assets/js/app/payment-options/payment-cards.component.html',
				controller: 'PaymentCardListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('servicesCategories', {
				url: '/services/categories',
				templateUrl: 'assets/js/app/site-admin/categories.component.html',
				controller: 'CategoryListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('servicesMenuitems', {
				url: '/services/menuitems',
				templateUrl: 'assets/js/app/site-admin/menuitems.component.html',
				controller: 'MenuitemListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('servicesCountries', {
				url: '/services/countries',
				templateUrl: 'assets/js/app/site-admin/countries.component.html',
				controller: 'CountryListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('servicesStates', {
				url: '/services/states',
				templateUrl: 'assets/js/app/site-admin/states.component.html',
				controller: 'StateListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('servicesBanks', {
				url: '/services/banks',
				templateUrl: 'assets/js/app/site-admin/banks.component.html',
				controller: 'BankListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('servicesDepartments', {
				url: '/services/departments',
				templateUrl: 'assets/js/app/org-admin/departments.component.html',
				controller: 'DepartmentListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('servicesDesignations', {
				url: '/services/designations',
				templateUrl: 'assets/js/app/org-admin/designations.component.html',
				controller: 'DesignationListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('servicesGroups', {
				url: '/services/groups',
				templateUrl: 'assets/js/app/org-admin/groups.component.html',
				controller: 'GroupListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('servicesRoles', {
				url: '/services/roles',
				templateUrl: 'assets/js/app/org-admin/roles.component.html',
				controller: 'RoleListCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('othersFeedback', {
				url: '/others/feedback',
				templateUrl: 'assets/js/app/others/feedback.component.html',
				controller: 'FeedbackCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('othersPricing', {
				url: '/others/pricing',
				templateUrl: 'assets/js/app/others/pricing.component.html',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('othersHiring', {
				url: '/others/hiring',
				templateUrl: 'assets/js/app/others/hiring.component.html',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('othersAboutUs', {
				url: '/others/about',
				templateUrl: 'assets/js/app/others/about-us.component.html',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('othersPrivacyPolicy', {
				url: '/others/privacy',
				templateUrl: 'assets/js/app/others/privacy-policy.component.html',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('othersTermsOfService', {
				url: '/others/terms',
				templateUrl: 'assets/js/app/others/terms-of-service.component.html',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
		   .state('othersContactUs', {
				url: '/others/contact',
				templateUrl: 'assets/js/app/others/contact-us.component.html',
				controller: 'ContactUsCtrl',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			})
			.state('othersFaqs', {
				url: '/others/faqs',
				templateUrl: 'assets/js/app/others/faqs.component.html',
				data: {
					authorizedRoles: ['ROLE_USER']
				}
			});
	}])			
	.run(['$rootScope', '$location', '$cookies', '$log', 'AUTH_EVENTS', 'AuthService', function($rootScope, $location, $cookies, $log, AUTH_EVENTS, AuthService) {
		$rootScope.$on( "$stateChangeStart", function(event, next, current) {
			var authorizedRoles = next.data.authorizedRoles;
			/*
			if (next.url === '/ResetPasswd?resetCode' || next.url === '/ForgotPasswd') {
				$log.info('Forwarding to ' + next.url);
			} else if (!AuthService.isAuthorized(authorizedRoles)) {
				$location.path('/login');
			} else if (authorizedRoles.indexOf('ROLE_GUEST') <= -1 && !AuthService.isAuthorized(authorizedRoles)) {
				event.preventDefault();
				if (AuthService.isAuthenticated()) {
					$rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
				} else {
					$rootScope.$broadcast(AUTH_EVENTS.notAuthenticated);
				}
				$location.path('/login');
			}*/
	    });
		
		$rootScope.$on('$stateNotFound', function(event, unfoundState, fromState, fromParams){ 
		    console.log(unfoundState.to);
		    console.log(unfoundState.toParams);
		    console.log(unfoundState.options);
		});
		
		$rootScope.$on('$stateChangeError', function(event, toState, toParams, fromState, fromParams, error){
			console.log(error);
		});

	 }])
	.factory('GrainCart', [function() {
		return new GrainCart(null);
	}]);

AppModule.config(['$httpProvider', function($httpProvider) {
	$httpProvider.interceptors.push([ '$injector', function($injector) {
		return $injector.get('AuthInterceptor');
	}]);
}]);

AppModule.config(['$translateProvider', function($translateProvider) {
	$translateProvider.useUrlLoader('Messages');
	$translateProvider.useStorage('UrlLanguageStorage');
	$translateProvider.useSanitizeValueStrategy('sanitize');
	$translateProvider.preferredLanguage('en');
	$translateProvider.fallbackLanguage('en');
}]);

/*
AppModule.config(['ngToastProvider', function(ngToastProvider) {
    ngToastProvider.setOptions({
        delay: 10000,
        startTop: 20,
        startRight: 10,
        verticalSpacing: 20,
        horizontalSpacing: 20,
        positionX: 'right',
        positionY: 'bottom',
        templateUrl: 'assets/js/vendor/angular-ui-ngToast/src/angular-ui-ngToast.html'
    });
}]);
*/

AppModule.factory('AuthInterceptor', ['$rootScope', '$q', 'AUTH_EVENTS', function($rootScope, $q, AUTH_EVENTS) {
	return {
		responseError : function(response) {
			$rootScope.$broadcast({
				401 : AUTH_EVENTS.notAuthenticated,
				403 : AUTH_EVENTS.notAuthorized,
				419 : AUTH_EVENTS.sessionTimeout,
				440 : AUTH_EVENTS.sessionTimeout
			}[response.status], response);
			return $q.reject(response);
		}
	};
}]);

AppModule.config(['KeepaliveProvider', 'IdleProvider', function(KeepaliveProvider, IdleProvider) {
	IdleProvider.idle(1200);
	IdleProvider.timeout(10);
	KeepaliveProvider.interval(10);
}]);

AppModule.run(['Idle', function(Idle) {
	//Idle.watch();
}]);