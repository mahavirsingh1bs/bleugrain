'use strict';

var CreditCardModule = angular.module("CreditCardModule", []);

CreditCardModule.controller("CreditCardCtrl", ['$scope', '$http', '$modal', '$log', 'HttpService', 'UtilityService', 'ngToast', 'ENTITY_TYPES', 
           function ($scope, $http, $modal, $log, HttpService, UtilityService, ngToast, ENTITY_TYPES) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.keyword = null;
	$scope.pageSize = 10;
	$scope.history = false;
	$scope.creditCards = null; 
	$scope.pages = [];
	$scope.sortOrders = {
		"fields": ["paymentCard.name", "number", "expiryMonth.name", "expiryYear.name", "createdBy.firstName,createdBy.lastName", "dateCreated"],
		"directions": ["asc", "asc", "asc", "asc", "asc", "asc", "asc" ]
	};
	
	$scope.sortFields = $scope.sortOrders['fields'][0];
	$scope.sortDirection = $scope.sortOrders['directions'][0];
	$scope.url = 'creditCards/all';
	
	if (!$scope.isAuthorized('ROLE_COMPANY_ADMIN') && !$scope.isAuthorized('ROLE_AGENT')) {
		$scope.userNo = $scope.currentUser.userNo;
	} else {
		$scope.companyId = $scope.currentUser.company.uniqueId;
	}
	
	$scope.getCreditCards = function () {
		var params = { 
			keyword: $scope.keyword,
			sortFields: $scope.sortFields,
			direction: $scope.direction,
			page: $scope.pageNumber, 
			pageSize: $scope.pageSize,
			history: $scope.history, 
		};
	
		if (!UtilityService.isUndefinedOrNull($scope.userNo)) {
			params.userNo = $scope.userNo;
		} else if (!UtilityService.isUndefinedOrNull($scope.companyId)) {
			params.companyId = $scope.companyId;
		}

		$scope.readData($scope.url, params, 'creditCards', $scope);
	};

	$scope.getCreditCards();
	
	$scope.addCreditCard = function () {
		
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/credit-cards/add-credit-card.component.html',
  			controller : 'CreditCardAddCtrl',
  			size : 'md',
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getCreditCards();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }
	
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			creditCard: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('assets/js/app/credit-cards/detail-credit-card.component.html', 'CreditCardDetailCtrl', $scope.url, 'creditCards', $scope, resolveObj);
	};
	
	$scope.editDetail = function(uniqueId) {
		var resolveObj = { 
			creditCard: function() {
				return {
					uniqueId: uniqueId
				}
			},
			currentUser: function() {
				return $scope.currentUser;
			}
		};
		
		$scope.editEntityInternal('assets/js/app/credit-cards/edit-credit-card.component.html', 'CreditCardEditCtrl', $scope.url, 'creditCards', $scope, resolveObj);
	};
	
	$scope.remove = function(uniqueId) {
		var resolveObj = { 
			creditCard: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.removeEntityInternal('delete-credit-card.component.html', 'DeleteCreditCardCtrl', $scope.url, 'creditCards', $scope, resolveObj);
	};

	$scope.prevPage = function() {
		$scope.prevPageInternal($scope.url, 'creditCards', $scope);
	};
	
	$scope.moveTo = function(page) {
		$scope.moveToInternal($scope.url, 'creditCards', $scope, page);
	};
	
	$scope.nextPage = function() {
		$scope.nextPageInternal($scope.url, 'creditCards', $scope);
	};

	$scope.searchByKeyword = function() {
		$scope.searchByKeywordInternal($scope.url, 'creditCards', $scope);
	};
	
	$scope.sortBy = function(field, direction) {
		$scope.sortByInternal($scope.url, 'creditCards', $scope, field, direction);
	};
	
}]);


CreditCardModule.controller("CreditCardAddCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', function ($scope, $http, $location, $modalInstance, ngToast) {
	$scope.creditCard = { };
	
	$http.get('paymentCards/all').success(function(response) {
		$scope.paymentCards = response.data;
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$http.get('expiryMonths/all').success(function(response) {
		$scope.expiryMonths = response;
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$http.get('expiryYears/all').success(function(response) {
		$scope.expiryYears = response;
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$http.post('profile/addCreditCard?userNo=' + $scope.currentUser.userNo, $scope.creditCard)
			.then(function(response) {
				$modalInstance.close();
			});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
	
}]);

CreditCardModule.controller("CreditCardEditCtrl", ['$scope', '$http', '$modalInstance', 'creditCard', function ($scope, $http, $modalInstance, creditCard) {
	$http.get('paymentCards/all').success(function(response) {
		$scope.paymentCards = response.data;
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$http.get('expiryMonths/all').success(function(response) {
		$scope.expiryMonths = response;
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$http.get('expiryYears/all').success(function(response) {
		$scope.expiryYears = response;
	}).error(function(data) {
		console.log('error occurred');
	});
	
	var uniqueId = creditCard.uniqueId;
	$http.get('creditCards/data/' + uniqueId).success(function(response) {
		$scope.creditCard = response || {};
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$scope.ok = function(creditCardFrm) {
		if (creditCardFrm) {
			$http.post('creditCards/update', $scope.creditCard)
				.then(function(response) {
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
	
}]);

CreditCardModule.controller("DeleteCreditCardCtrl", ['$scope', '$http', '$location', '$modalInstance', 'creditCard', function ($scope, $http, $location, $modalInstance, creditCard) {
	$scope.ok = function() {
		$http.delete('creditCards/delete/' + creditCard.uniqueId)
			.then(function(response) {
				$modalInstance.close();
			});
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);