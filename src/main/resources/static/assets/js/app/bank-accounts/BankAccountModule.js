'use strict';

var BankAccountModule = angular.module("BankAccountModule", []);

BankAccountModule.controller("BankAccountCtrl", ['$scope', '$http', '$modal', '$log', 'HttpService', 'UtilityService', 'ngToast', 'ENTITY_TYPES', 
           function ($scope, $http, $modal, $log, HttpService, UtilityService, ngToast, ENTITY_TYPES) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.keyword = null;
	$scope.pageSize = 10;
	$scope.history = false;
	$scope.bankAccounts = null;
	$scope.pages = [];
	$scope.sortOrders = {
		"fields": ["account", "bank.name", "owner", "createdBy.firstName,createdBy.lastName", "dateCreated"],
		"directions": ["asc", "asc", "asc", "asc", "asc", "asc" ]
	};
	
	$scope.sortFields = $scope.sortOrders['fields'][0];
	$scope.sortDirection = $scope.sortOrders['directions'][0];
	$scope.url = 'bankAccounts/all';
	
	if (!$scope.isAuthorized('ROLE_COMPANY_ADMIN') && !$scope.isAuthorized('ROLE_AGENT')) {
		$scope.userNo = $scope.currentUser.userNo;
	} else {
		$scope.companyId = $scope.currentUser.company.uniqueId;
	}
	
	$scope.getBankAccounts = function () {
		var params = { 
			keyword: $scope.keyword,
			sortFields: $scope.sortFields,
			direction: $scope.direction,
			page: $scope.pageNumber, 
			pageSize: $scope.pageSize,
			history: $scope.history, 
		};
		
		if (!UtilityService.isUndefinedOrNull($scope.userNo)) {
			params.userNo = $scope.userNo;
		} else if (!UtilityService.isUndefinedOrNull($scope.companyId)) {
			params.companyId = $scope.companyId;
		}

		$scope.readData($scope.url, params, 'bankAccounts', $scope);
	}
	
	$scope.getBankAccounts();
	
	$scope.addBankAccount = function () {
		
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/bank-accounts/add-bank-account.component.html',
  			controller : 'BankAccountAddCtrl',
  			size : 'md',
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.getBankAccounts();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }
	
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			bankAccount: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('assets/js/app/bank-accounts/detail-bank-account.component.html', 'BankAccountDetailCtrl', $scope.url, 'bankAccounts', $scope, resolveObj);
	};
	
	$scope.editDetail = function(uniqueId) {
		var resolveObj = { 
			bankAccount: function() {
				return {
					uniqueId: uniqueId
				}
			},
			currentUser: function() {
				return $scope.currentUser;
			}
		};
		
		$scope.editEntityInternal('assets/js/app/bank-accounts/edit-bank-account.component.html', 'BankAccountEditCtrl', $scope.url, 'bankAccounts', $scope, resolveObj);
	};
	
	$scope.remove = function(uniqueId) {
		var resolveObj = { 
			bankAccount: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.removeEntityInternal('assets/js/app/bank-accounts/delete-bank-account.component.html', 'DeleteBankAccountCtrl', $scope.url, 'bankAccounts', $scope, resolveObj);
	};

	$scope.prevPage = function() {
		$scope.prevPageInternal($scope.url, 'bankAccounts', $scope);
	};
	
	$scope.moveTo = function(page) {
		$scope.moveToInternal($scope.url, 'bankAccounts', $scope, page);
	};
	
	$scope.nextPage = function() {
		$scope.nextPageInternal($scope.url, 'bankAccounts', $scope);
	};

	$scope.searchByKeyword = function() {
		$scope.searchByKeywordInternal($scope.url, 'bankAccounts', $scope);
	};
	
	$scope.sortBy = function(field, direction) {
		$scope.sortByInternal($scope.url, 'bankAccounts', $scope, field, direction);
	};
	
}]);

BankAccountModule.controller("BankAccountAddCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', function ($scope, $http, $location, $modalInstance, ngToast) {
	$scope.bankAccount = { };
	$http.get('banks/all').success(function(response) {
		$scope.banks = response.data;
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$http.post('profile/addBankAccount?userNo=' + $scope.currentUser.userNo, $scope.bankAccount)
				.then(function(response) {
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
    
}]);

BankAccountModule.controller("BankAccountEditCtrl", ['$scope', '$http', '$modalInstance', 'bankAccount', function ($scope, $http, $modalInstance, bankAccount) {
	$http.get('banks/all').success(function(response) {
		$scope.banks = response.data;
	}).error(function(data) {
		console.log('error occurred');
	});
	
	var uniqueId = bankAccount.uniqueId;
	$http.get('bankAccounts/data/' + uniqueId).success(function(response) {
		$scope.bankAccount = response || {};
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$http.post('bankAccounts/update', $scope.bankAccount)
				.then(function(response) {
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
	
}]);

BankAccountModule.controller("DeleteBankAccountCtrl", ['$scope', '$http', '$location', '$modalInstance', 'bankAccount', function ($scope, $http, $location, $modalInstance, bankAccount) {
	$scope.ok = function() {
		$http.delete('bankAccounts/delete/' + bankAccount.uniqueId)
			.then(function(response) {
				$modalInstance.close();
			});
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);