'use strict';

var OrderModule = angular.module("OrderModule", ['ui.bootstrap', 'ngFileUpload']);

OrderModule.controller("OrderListCtrl", ['$scope', '$http', '$modal', '$log', 'HttpService', 'UtilityService', 'ENTITY_TYPES', 
           function ($scope, $http, $modal, $log, HttpService, UtilityService, ENTITY_TYPES) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.keyword = null;
	$scope.pageSize = 10;
	$scope.history = false;
	$scope.orders = null; 
	$scope.pages = [];
	$scope.sortOrders = {
		"fields": ["orderDate", "quantity.quantity", "latestPrice.price", "owner.firstName,owner.lastName", "createdBy.firstName,createdBy.lastName", "dateCreated"],
		"directions": ["desc", "asc", "asc", "asc", "asc", "asc", "asc" ]
	};
	
	$scope.sortFields = $scope.sortOrders['fields'][0];
	$scope.sortDirection = $scope.sortOrders['directions'][0];
	
	if (!$scope.isAuthorized('ROLE_ADMIN')) {
		if (!$scope.isAuthorized('ROLE_COMPANY_ADMIN')) {
			$scope.url = 'orders/user';
			$scope.userNo = $scope.currentUser.userNo;
		} else {
			$scope.url = 'orders/company';
			$scope.companyId = $scope.currentUser.company.uniqueId;
		}
	} else {
		$scope.url = 'orders/all';
	}
	
	$scope.getOrders = function() {
		var params = { 
			keyword: $scope.keyword,
			sortFields: $scope.sortFields,
			direction: $scope.direction,
			page: $scope.pageNumber, 
			pageSize: $scope.pageSize,
			history: $scope.history, 
		};
		
		if (!UtilityService.isUndefinedOrNull($scope.userNo)) {
			params.userNo = $scope.userNo;
		} else if (!UtilityService.isUndefinedOrNull($scope.companyId)) {
			params.companyId = $scope.companyId;
		}

		$scope.readData($scope.url, params, 'orders', $scope);
	}

	$scope.getOrders();
	
	$scope.addOrder = function() {
		var resolveObj = { 
			currentUser: function() {
				return $scope.currentUser;
			}
		};
		
		$scope.addEntityInternal('assets/js/app/orders/add-order.component.html', 'OrderAddCtrl', 'orders/all', 'orders', $scope, resolveObj);
	};
	
	$scope.viewDetail = function(orderNo) {
		var resolveObj = { 
			order: function() {
				return {
					orderNo: orderNo
				}
			}
		};
		
		$scope.viewEntityInternal('assets/js/app/orders/detail-order.component.html', 'OrderDetailCtrl', 'orders/all', 'orders', $scope, resolveObj);
	};
	
	$scope.editDetail = function(uniqueId) {
		var resolveObj = { 
			order: function() {
				return {
					uniqueId: uniqueId
				}
			},
			currentUser: function() {
				return $scope.currentUser;
			}
		};
		
		$scope.editEntityInternal('assets/js/app/orders/edit-order.component.html', 'OrderEditCtrl', 'orders/all', 'orders', $scope, resolveObj);
	};
	
	$scope.remove = function(uniqueId) {
		var resolveObj = { 
			order: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.removeEntityInternal('delete-order.component.html', 'DeleteOrderCtrl', 'orders/all', 'orders', $scope, resolveObj);
	};

	$scope.assignOrder = function(order) {

		var resolveObj = { 
			order: function() {
				return order;
			}
		}
		
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/orders/assign-order.component.html',
  			controller : 'OrderAssignCtrl',
  			size : 'md',
	  		resolve: resolveObj,
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function() {
  			$scope.getOrders();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
	};
	
	$scope.markAsShipped = function(order) {
		var resolveObj = { 
			order: function() {
				return order;
			}
		}
		
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'mark-as-shipped.component.html',
  			controller : 'OrderShipCtrl',
  			size : 'md',
	  		resolve: resolveObj,
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function() {
  			$scope.getOrders();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
	};
	
	$scope.markAsDispatched = function(order) {
		var resolveObj = { 
			order: function() {
				return order;
			}
		}
		
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/orders/mark-as-dispatched.component.html',
  			controller : 'OrderDispatchCtrl',
  			size : 'md',
	  		resolve: resolveObj,
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function() {
  			$scope.getOrders();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
	};
	
	$scope.markAsDelivered = function(order) {
		var resolveObj = { 
			order: function() {
				return order;
			}
		}
		
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'mark-as-delivered.component.html',
  			controller : 'OrderDeliverCtrl',
  			size : 'md',
	  		resolve: resolveObj,
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function() {
  			$scope.getOrders();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
	};

	$scope.prevPage = function() {
		$scope.prevPageInternal('orders/all', 'orders', $scope);
	};
	
	$scope.moveTo = function(page) {
		$scope.moveToInternal('orders/all', 'orders', $scope, page);
	};
	
	$scope.nextPage = function() {
		$scope.nextPageInternal('orders/all', 'orders', $scope);
	};

	$scope.searchByKeyword = function() {
		$scope.searchByKeywordInternal('orders/all', 'orders', $scope);
	};
	
	$scope.sortBy = function(field, direction) {
		$scope.sortByInternal('orders/all', 'orders', $scope, field, direction);
	};

}]);

OrderModule.controller("OrderDetailCtrl", ['$scope', '$http', '$modalInstance', 'order', function ($scope, $http, $modalInstance, order) {
	$http.get('orders/data/' + order.orderNo)
		.then(function(response) {
			$scope.order = response.data;
		});
	
	$scope.close = function() {
		$modalInstance.close();
    };
}]);

OrderModule.controller("OrderShipCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'order', function ($scope, $http, $location, $modalInstance, ngToast, order) {
	$scope.submit = function() {
		$http.post('orders/shipped?orderNo=' + order.orderNo)
			.then(function(response) {
				$modalInstance.close();
			});
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

OrderModule.controller("OrderDispatchCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'order', function ($scope, $http, $location, $modalInstance, ngToast, order) {
	$scope.orderDispatch = { };
	
	$scope.searchDispatcher = function(keywords) {
		return $http.get('employees/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.selectDispatcher = function(item, model, label) {
		$scope.orderDispatch.deliverBy = item;
	}
	
	$scope.submit = function(isValid) {
		if (isValid) {
			$scope.orderDispatch['orderNo'] = order.orderNo;
			$scope.orderDispatch['userNo'] = $scope.currentUser.userNo;
			
			$http.post('orders/dispatched', $scope.orderDispatch)
				.then(function(response) {
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

OrderModule.controller("OrderDeliverCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'order', function ($scope, $http, $location, $modalInstance, ngToast, order) {
	
	$scope.submit = function() {
		$http.post('orders/delivered?orderNo=' + order.orderNo)
			.then(function(response) {
				$modalInstance.close();
			});
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

OrderModule.controller("OrderAssignCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'order', function ($scope, $http, $location, $modalInstance, ngToast, order) {
	$scope.orderAssign = { };
	
	$scope.searchAssignee = function(keywords) {
		return $http.get('employees/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.selectAssignee = function(item, model, label) {
		$scope.orderAssign.assignTo = item;
	}
	
	$scope.submit = function(isValid) {
		if (isValid) {
			$scope.orderAssign['orderNo'] = order.orderNo;
			$scope.orderAssign['userNo'] = $scope.currentUser.userNo;
			
			$http.post('orders/assign', $scope.orderAssign)
				.then(function(response) {
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

OrderModule.controller("DeleteOrderCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'order', function ($scope, $http, $location, $modalInstance, ngToast, order) {
	$scope.ok = function() {
		$http.delete('orders/delete/' + order.uniqueId)
			.then(function(response) {
				if (response.data.code === 'success') {
					ngToast.create({ className: 'success', content: response.data.message });
				} else if (response.data.code === 'error') {
					ngToast.create({ className: 'danger', content: response.data.message });
				}
				$modalInstance.close();
			});
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);