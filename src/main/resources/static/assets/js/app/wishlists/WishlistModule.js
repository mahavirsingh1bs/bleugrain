'use strict';

var WishlistModule = angular.module('WishlistModule', []);

WishlistModule.controller('WishlistCtrl', ['$rootScope', '$scope', 'WishlistService', 'WISHLIST_EVENTS', function($rootScope, $scope, WishlistService, WISHLIST_EVENTS) {
	
	$scope.removeProductFromWishlist = function(uniqueId) {
		WishlistService.removeProductFromWishlist(uniqueId)
			.then(function(response) {
				$rootScope.$broadcast(WISHLIST_EVENTS.itemRemoved);
			});
	};

}]);

WishlistModule.factory('WishlistService', ['$http', function($http) {
	var wishlistService = {};
	
	wishlistService.findWishlist = function() {
		return $http.get('wishlist/')
			.then(function(response) {
				return response.data;
			});
	};
	
	wishlistService.addProductToWishlist = function(uniqueId) {
		return $http.post('wishlist/addProduct', uniqueId)
			.then(function(response) {
				return response.data;
			});
	};
	
	wishlistService.addDemandToWishlist = function(uniqueId) {
		return $http.post('wishlist/addDemand', uniqueId)
			.then(function(response) {
				return response.data;
			});
	};
	
	wishlistService.removeProductFromWishlist = function(uniqueId) {
		return $http.post('wishlist/removeProduct', uniqueId)
			.then(function(response) {
				return response.data;
			});
	};
	
	wishlistService.removeDemandFromWishlist = function(uniqueId) {
		return $http.post('wishlist/removeDemand', uniqueId)
			.then(function(response) {
				return response.data;
			});
	};
	
	return wishlistService;
}]);