'use strict';

var DemandModule = angular.module("DemandModule", ['ui.bootstrap', 'ngFileUpload']);

DemandModule.controller("DemandListCtrl", ['$scope', '$http', '$modal', '$log', 'HttpService', 'UtilityService', 'ENTITY_TYPES', 
           function ($scope, $http, $modal, $log, HttpService, UtilityService, ENTITY_TYPES) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.keyword = null;
	$scope.pageSize = 10;
	$scope.history = false;
	$scope.demands = null; 
	$scope.pages = [];
	$scope.sortDemands = {
		"fields": ["name", "quantity.quantity", "latestPrice.price", "owner.firstName,owner.lastName", "createdBy.firstName,createdBy.lastName", "dateCreated"],
		"directions": ["asc", "asc", "asc", "asc", "asc", "asc", "asc" ]
	};
	
	$scope.sortFields = $scope.sortDemands['fields'][0];
	$scope.sortDirection = $scope.sortDemands['directions'][0];
	
	if (!$scope.isAuthorized('ROLE_ADMIN')) {
		if (!$scope.isAuthorized('ROLE_COMPANY_ADMIN')) {
			$scope.url = 'demands/user';
			$scope.userNo = $scope.currentUser.userNo;
		} else {
			$scope.url = 'demands/company';
			$scope.companyId = $scope.currentUser.company.uniqueId;
		}
	} else {
		$scope.url = 'demands/all';
	}
	
	$scope.getDemands = function () {
		var params = { 
			keyword: $scope.keyword,
			sortFields: $scope.sortFields,
			direction: $scope.direction,
			page: $scope.pageNumber, 
			pageSize: $scope.pageSize,
			history: $scope.history, 
		};
	
		if (!UtilityService.isUndefinedOrNull($scope.userNo)) {
			params.userNo = $scope.userNo;
		} else if (!UtilityService.isUndefinedOrNull($scope.companyId)) {
			params.companyId = $scope.companyId;
		}
		
		$scope.readData($scope.url, params, 'demands', $scope);
	};
	
	$scope.getDemands();
	
	$scope.addDemand = function() {
		var resolveObj = { 
			currentUser: function() {
				return $scope.currentUser;
			}
		};
		
		$scope.addEntityInternal('assets/js/app/demands/add-demand.component.html', 'DemandAddCtrl', 'demands/all', 'demands', $scope, resolveObj);
	};
	
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			demand: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('assets/js/app/demands/detail-demand.component.html', 'DemandDetailCtrl', 'demands/all', 'demands', $scope, resolveObj);
	};

	$scope.addDemand = function() {
		var resolveObj = { 
			currentUser: function() {
				return $scope.currentUser;
			}
		};
		
		$scope.addEntityInternal('assets/js/app/demands/add-demand.component.html', 'DemandAddCtrl', 'demands/all', 'demands', $scope, resolveObj);
	};
	
	$scope.editDetail = function(uniqueId) {
		var resolveObj = { 
			demand: function() {
				return {
					uniqueId: uniqueId
				}
			},
			currentUser: function() {
				return $scope.currentUser;
			}
		};
		
		$scope.editEntityInternal('assets/js/app/demands/edit-demand.component.html', 'DemandEditCtrl', 'demands/all', 'demands', $scope, resolveObj);
	};

	$scope.assign = function(demand) {

		var resolveObj = { 
			demand: function() {
				return demand;
			}
		}
		
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/demands/assign-demand.component.html',
  			controller : 'DemandAssignCtrl',
  			size : 'md',
	  		resolve: resolveObj,
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function() {
  			$scope.getDemands();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
	};

	$scope.markAsFulfilled = function(demand) {
		var resolveObj = { 
			demand: function() {
				return demand;
			}
		}
		
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'mark-as-fulfilled.component.html',
  			controller : 'DemandFulfilledCtrl',
  			size : 'md',
	  		resolve: resolveObj,
  			scope : $scope
  		});
  		
  		modalInstance.result.then(function() {
  			$scope.getDemands();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
	};
	
	$scope.remove = function(uniqueId) {
		var resolveObj = { 
			demand: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.removeEntityInternal('delete-demand.component.html', 'DeleteDemandCtrl', 'demands/all', 'demands', $scope, resolveObj);
	};

	$scope.prevPage = function() {
		$scope.prevPageInternal('demands/all', 'demands', $scope);
	};
	
	$scope.moveTo = function(page) {
		$scope.moveToInternal('demands/all', 'demands', $scope, page);
	};
	
	$scope.nextPage = function() {
		$scope.nextPageInternal('demands/all', 'demands', $scope);
	};

	$scope.searchByKeyword = function() {
		$scope.searchByKeywordInternal('demands/all', 'demands', $scope);
	};
	
	$scope.sortBy = function(field, direction) {
		$scope.sortByInternal('demands/all', 'demands', $scope, field, direction);
	};

}]);

DemandModule.controller("DemandAddCtrl", ['$scope', '$http', '$location', '$modalInstance', 'currentUser', 'ngToast', 'CategoryService', function ($scope, $http, $location, $modalInstance, currentUser, ngToast, CategoryService) {
	$scope.demand = { };
	$scope.demand.owner = { };
	$scope.ownerImage = {};
	$scope.uploadProgress = 0;
	$scope.isExistingUser = null;
	
	$scope.categoryConfig = CategoryService.config({
		placeholder: 'Select category'
	}, function(value, $item) {
		$scope.demand.categoryId = value;
	});
	
	CategoryService.findParentCategories('categories/')
		.then(function(categories) {
			$scope.categories = categories; 
		});
	
	$http.get('groups/all')
		.then(function(response) {
			$scope.groups = response.data.groups;
		});
	
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries;
		});
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states;
		});
	
	$http.get('currencies/all').success(function(response) {
		$scope.currencies = response;
		angular.forEach($scope.currencies, function(currency) {
			if (currency.countryCode === currentUser.defaultCountry.code) {
				$scope.setPriceCurrency(currency);
			}
		});
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$http.get('units/all').success(function(response) {
		$scope.units = response;
		angular.forEach($scope.units, function(unit) {
			if (unit.countryCode === currentUser.defaultCountry.code && unit['default'] === true) {
				$scope.setQuantityUnit(unit);
				$scope.setPriceUnit(unit);
			}
		});
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$scope.setCategory = function(category) {
		$scope.demand.categoryId = category.id;
		$scope.demand.category = category;
	};
	
	$scope.setQuantityUnit = function(quantityUnit) {
		$scope.demand.quantityUnitId = quantityUnit.id;
		$scope.demand.quantityUnit = quantityUnit;
	};
	
	$scope.setPriceCurrency = function(priceCurrency) {
		$scope.demand.priceCurrencyId = priceCurrency.id;
		$scope.demand.priceCurrency = priceCurrency;
	};
	
	$scope.setPriceUnit = function(priceUnit) {
		$scope.demand.priceUnitId = priceUnit.id;
		$scope.demand.priceUnit = priceUnit;
	};

	$scope.searchOwner = function(keywords) {
		return $http.get('owners/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.selectOwner = function(item, model, label) {
		$scope.isExistingUser = true;
		$scope.demand.owner.email = item.emailId;
	}

	$scope.searchTag = function(keywords) {
		return $http.get('tags/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.ok = function(demandFrm) {
		
		if (customValidation(demandFrm, $scope.demand)) {
			var formData = new FormData();
			
			if ($scope.isAuthorized('ROLE_COMPANY_ADMIN') || $scope.isAuthorized('ROLE_AGENT')) {
				$scope.demand.companyId = $scope.currentUser.company.uniqueId;
			} else if ($scope.isAuthorized('ROLE_BROKER') || $scope.isAuthorized('ROLE_RETAILER')) {
				$scope.demand.userId = $scope.currentUser.userNo;
			}
			
			formData.append('demand', new Blob([JSON.stringify($scope.demand)], { type: "application/json" }));
			if (angular.isObject($scope.file)) {
				formData.append('file', $scope.file);
			}
			
			$http.post('demands/new', formData, {
					transformRequest: angular.identity,
					headers: { 'Content-Type': undefined }
				}).then(function(response) {
					if (response.data.code === 'success') {
						ngToast.create({ className: 'success', content: response.data.message });
						$modalInstance.close();
					} else if (response.data.code === 'failure') {
						$modalInstance.close();
					}
				});
				
		}
	};
	
	function customValidation(demandFrm, demand) {
		if ($scope.isAuthorized('ROLE_BROKER') || $scope.isAuthorized('ROLE_RETAILER') || 
				$scope.isAuthorized('ROLE_COMPANY_ADMIN') || $scope.isAuthorized('ROLE_AGENT')) {
			if (!demandFrm.name.$error.required && !demandFrm.name.$error.minlength
					&& !demandFrm.quantity.$error.required && !demandFrm.quantity.$error.pattern 
					&& !demandFrm.pricePerUnit.$error.required && !demandFrm.deliveryAddressAddressLine1.$error.required 
					&& !demandFrm.deliveryAddressCity.$error.required && !demandFrm.deliveryAddressState.$error.required 
					&& !demandFrm.deliveryAddressCountry.$error.required && !demandFrm.deliveryAddressZipcode.$error.required) {
				return true;
			} else {
				return false;
			}
		} else {
			if (!demandFrm.name.$error.required && !demandFrm.name.$error.minlength
				&& !demandFrm.quantity.$error.required && !demandFrm.quantity.$error.pattern 
				&& !demandFrm.pricePerUnit.$error.required && !demandFrm.pricePerUnit.$error.pattern
				&& !demandFrm.firstName.$error.required && !demandFrm.firstName.$error.minlength
				&& !demandFrm.firstName.$error.pattern && !demandFrm.mobileNo.$error.required
				&& !demandFrm.group.$error.required && !demandFrm.addressLine1.$error.required 
				&& !demandFrm.state.$error.required && !demandFrm.country.$error.required
				&& !demandFrm.zipcode.$error.required) {
				return true;
			} else {
				return false;
			}
		} 
	};
	
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

DemandModule.controller("DemandDetailCtrl", ['$scope', '$http', '$modalInstance', 'demand', function ($scope, $http, $modalInstance, demand) {
	$http.get('demands/data/' + demand.uniqueId)
		.then(function(response) {
			$scope.demand = response.data;
		});
	
	$scope.close = function() {
		$modalInstance.close();
    };
}]);

DemandModule.controller("DemandEditCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', 'CategoryService', 'demand', 'currentUser', function ($scope, $http, $modalInstance, ngToast, CategoryService, demand, currentUser) {
	$scope.demand = {};
	
	$http.get('demands/data/' + demand.uniqueId)
		.then(function(response) {
			$scope.demand = response.data;
		});

	$scope.categoryConfig = CategoryService.config({
		placeholder: 'Select category'
	}, function(value, $item) {
		$scope.demand.categoryId = value;
	});
	
	CategoryService.findParentCategories('categories/')
		.then(function(categories) {
			$scope.categories = categories; 
		});
	
	$http.get('currencies/all').success(function(response) {
		$scope.currencies = response;
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$http.get('units/all').success(function(response) {
		$scope.units = response;
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$http.get('demandStatuses/all').success(function(response) {
		$scope.demandStatuses = response.data;
	}).error(function(data) {
		console.log('error occurred');
	});
	
	$scope.setCategory = function(category) {
		$scope.demand.category = category;
	};
	
	$scope.setQuantityUnit = function(quantityUnit) {
		$scope.demand.quantityUnit = quantityUnit;
	};
	
	$scope.setPriceCurrency = function(priceCurrency) {
		$scope.demand.priceCurrency = priceCurrency;
	};
	
	$scope.setPriceUnit = function(priceUnit) {
		$scope.demand.priceUnit = priceUnit;
	};
	
	$scope.searchOwner = function(keywords) {
		return $http.get('owners/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.selectOwner = function(item, model, label) {
		$scope.demand.owner = item;
	}
	
	$scope.searchTag = function(keywords) {
		return $http.get('tags/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$scope.postUpdateEntity('demands/update/', $scope.demand, $scope.demandFrm, $modalInstance);
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

DemandModule.controller("DemandAssignCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'demand', function ($scope, $http, $location, $modalInstance, ngToast, demand) {
	$scope.demandAssign = { };
	
	$scope.searchAssignee = function(keywords) {
		return $http.get('employees/search?keywords=' + keywords)
			.then(function(response) {
				return response.data;
			});
	}
	
	$scope.selectAssignee = function(item, model, label) {
		$scope.demandAssign.assignTo = item;
	}
	
	$scope.submit = function(isValid) {
		if (isValid) {
			$scope.demandAssign['uniqueId'] = demand.uniqueId;
			$scope.demandAssign['userNo'] = $scope.currentUser.userNo;
			
			$http.post('demands/assign', $scope.demandAssign)
				.then(function(response) {
					$modalInstance.close();
				});
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

DemandModule.controller("DemandFulfilledCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'demand', function ($scope, $http, $location, $modalInstance, ngToast, demand) {
	
	$scope.submit = function() {
		$http.post('demands/fulfilled?uniqueId=' + demand.uniqueId)
			.then(function(response) {
				$modalInstance.close();
			});
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

DemandModule.controller("DeleteDemandCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'demand', function ($scope, $http, $location, $modalInstance, ngToast, demand) {
	$scope.ok = function() {
		$scope.deleteEntity('demands/delete/' + demand.uniqueId, $modalInstance);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);