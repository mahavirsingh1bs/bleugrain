'use strict';

var FarmerModule = angular.module("FarmerModule", []);

FarmerModule.controller("FarmerListCtrl", ['$scope', '$http', '$modal', '$log', 'HttpService', 'ngToast', 'usSpinnerService', function ($scope, $http, $modal, $log, HttpService, ngToast, usSpinnerService) {
	$scope.pageNumber = 0;
	$scope.totalPage = 0;
	$scope.keyword = null;
	$scope.pageSize = 10;
	$scope.history = false;
	$scope.farmers = null; 
	$scope.pages = [];
	$scope.sortOrders = {
		"fields": ["firstName,lastName", "phoneNo", "email", "createdBy.firstName,createdBy.lastName", "dateModified", "modifiedBy.firstName,modifiedBy.lastName", "dateModified"],
		"directions": ["asc", "asc", "asc", "asc", "asc" ]
	};
	
	$scope.sortFields = $scope.sortOrders['fields'][0];
	$scope.sortDirection = $scope.sortOrders['directions'][0];
	$scope.url = 'farmers/all';
	
	$scope.getFarmers = function() {
		var params = { 
			keyword: $scope.keyword,
			sortFields: $scope.sortFields,
			direction: $scope.direction,
			page: $scope.pageNumber, 
			pageSize: $scope.pageSize,
			history: $scope.history, 
		};
		
		$scope.readData($scope.url, params, 'farmers', $scope);
	};
	
	$scope.getFarmers();
	
	$scope.addFarmer = function() {
		$scope.addEntityInternal('assets/js/app/farmers/add-farmer.component.html', 'FarmerAddCtrl', 'farmers/all', 'farmers', $scope);
	};
	
	$scope.viewDetail = function(uniqueId) {
		var resolveObj = { 
			farmer: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.viewEntityInternal('assets/js/app/farmers/detail-farmer.component.html', 'FarmerDetailCtrl', 'farmers/all', 'farmers', $scope, resolveObj);
	};
	
	$scope.editDetail = function(uniqueId) {
		var resolveObj = { 
			farmer: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.editEntityInternal('assets/js/app/farmers/edit-farmer.component.html', 'FarmerEditCtrl', 'farmers/all', 'farmers', $scope, resolveObj);
	};
	
	$scope.remove = function(uniqueId) {
		var resolveObj = { 
			farmer: function() {
				return {
					uniqueId: uniqueId
				}
			}
		};
		
		$scope.removeEntityInternal('delete-farmer.component.html', 'DeleteFarmerCtrl', 'farmers/all', 'farmers', $scope, resolveObj);
	};

	$scope.prevPage = function() {
		$scope.prevPageInternal('farmers/all', 'farmers', $scope);
	};
	
	$scope.moveTo = function(page) {
		$scope.moveToInternal('farmers/all', 'farmers', $scope, page);
	};
	
	$scope.nextPage = function() {
		$scope.nextPageInternal('farmers/all', 'farmers', $scope);
	};

	$scope.searchByKeyword = function() {
		$scope.searchByKeywordInternal('farmers/all', 'farmers', $scope);
	};
	
	$scope.sortBy = function(field, direction) {
		$scope.sortByInternal('farmers/all', 'farmers', $scope, field, direction);
	};

	$scope.startSpin = function() {
        usSpinnerService.spin('prak-spinner');
    }
    $scope.stopSpin = function(){
        usSpinnerService.stop('prak-spinner');
    }
	
}]);

FarmerModule.controller("FarmerAddCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'usSpinnerService', 'FormUtils', 'CountryService', 'StateService',  
        function ($scope, $http, $location, $modalInstance, ngToast, usSpinnerService, FormUtils, CountryService, StateService) {
	$scope.farmer = {};
	
	CountryService.findCountries('countries/all')
		.then(function(countries) {
			$scope.countries = countries; 
		});
	
	StateService.findStates('states/all')
		.then(function(states) {
			$scope.states = states; 
		});
	
	$scope.ok = function(isValid) {
		if (isValid) {
			var formData = new FormData();
			
			formData.append('farmer', new Blob([JSON.stringify($scope.farmer)], { type: "application/json" }));
			formData.append('farmerImage', $scope.farmerImage);
			$scope.postCreateEntity('farmers/new', formData, $scope.farmerFrm, $modalInstance);
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };

    $scope.startSpin = function() {
        usSpinnerService.spin('prak-spinner');
    }
    $scope.stopSpin = function(){
        usSpinnerService.stop('prak-spinner');
    }

}]);

FarmerModule.controller("FarmerDetailCtrl", ['$scope', '$http', '$modalInstance', 'farmer', function ($scope, $http, $modalInstance, farmer) {
	$http.get('farmers/data/' + farmer.uniqueId)
		.then(function(response) {
			$scope.farmer = response.data;
		});
	
	$scope.close = function() {
		$modalInstance.close();
    };
}]);

FarmerModule.controller("FarmerEditCtrl", ['$scope', '$http', '$modalInstance', 'ngToast', 'CountryService', 'StateService', 'farmer', function ($scope, $http, $modalInstance, ngToast, CountryService, StateService, farmer) {
	
	CountryService.findCountries('countries/all')
		.then(function(countries) {
			$scope.countries = countries; 
		});
	
	StateService.findStates('states/all')
		.then(function(states) {
			$scope.states = states; 
		});

	$http.get('farmers/data/' + farmer.uniqueId)
		.then(function(response) {
			$scope.farmer = response.data;
		});
	
	$scope.ok = function(isValid) {
		if (isValid) {
			$scope.postUpdateEntity('farmers/update/', $scope.farmer, $scope.farmerFrm, $modalInstance);
		}
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);

FarmerModule.controller("DeleteFarmerCtrl", ['$scope', '$http', '$location', '$modalInstance', 'ngToast', 'farmer', function ($scope, $http, $location, $modalInstance, ngToast, farmer) {
	$scope.ok = function() {
		$scope.deleteEntity('farmers/delete/' + farmer.uniqueId, $modalInstance);
    };
    
    $scope.cancel = function() {
    	$modalInstance.dismiss('cancel');
    };
}]);