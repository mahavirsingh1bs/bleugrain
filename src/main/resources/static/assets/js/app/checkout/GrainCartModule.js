'use strict';

var GrainCartModule = angular.module("GrainCartModule", []);

GrainCartModule.controller('GrainCartCtrl', ['$rootScope', '$scope', 'GrainCartService', 'GRAINCART_EVENTS', 'ngToast', function($rootScope, $scope, GrainCartService, GRAINCART_EVENTS, ngToast) {
	
	$scope.removeFromCart = function(uniqueId) {
		GrainCartService.removeFromCart(uniqueId)
			.then(function(response) {
				$rootScope.$broadcast(GRAINCART_EVENTS.itemRemoved);
				ngToast.create({
					className: 'success',
					content: 'Product has been successfully removed from the grain cart.' 
				});
			});
	};
	
	$scope.removeFromConsumerCart = function(uniqueId) {
		GrainCartService.removeFromConsumerCart(uniqueId)
			.then(function(response) {
				$rootScope.$broadcast(GRAINCART_EVENTS.itemRemoved);
				ngToast.create({
					className: 'success',
					content: 'Item has been successfully removed from the grain cart.' 
				});
			});
	};
	
}]);