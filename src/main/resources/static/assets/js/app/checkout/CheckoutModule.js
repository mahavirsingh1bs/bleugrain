'use strict';

var CheckoutModule = angular.module("CheckoutModule", []);

CheckoutModule.controller("CheckoutCtrl", ['$rootScope', '$scope', '$http', '$log', '$location', '$modal', 'GrainCartService', 'GRAINCART_EVENTS', 'ngToast', function ($rootScope, $scope, $http, $log, $location, $modal, GrainCartService, GRAINCART_EVENTS, ngToast) {
	$scope.orderDetails = { };
	$scope.shipItemsToBilling = null;
	$scope.pageNumber = 1;
	$scope.shortDescMaxLimit = 80;
	
	$http.get('states/all')
		.then(function(response) {
			$scope.states = response.data.states || {};
		});
	
	$http.get('countries/all')
		.then(function(response) {
			$scope.countries = response.data.countries || {};
		});
	
	$scope.findDeliveryAddresses = function() {
		$http.get('addresses/delivery').success(function(response) {
			$scope.deliveryAddresses = response || {};
		}).error(function(data) {
			console.log('error occurred');
		});
	}
	
	$scope.findDeliveryAddresses();
	
	$scope.findBillingAddresses = function() {
		$http.get('addresses/billing').success(function(response) {
			$scope.billingAddresses = response || {};
		}).error(function(data) {
			console.log('error occurred');
		});
	}
	
	$scope.findBillingAddresses();
	
	$scope.editAddressEntity = function (title, category, address) {
		var modalInstance = $modal.open({
  			animation : $scope.animationsEnabled,
  			templateUrl : 'assets/js/app/shared/address/edit-address-entity.component.html',
  			controller : 'AddressEntityEditCtrl',
  			size : 'md',
  			scope : $scope,
  			resolve : { 
  				options : function() {
  					return {
  						id: address.id,
  						title: title,
  						category: category
  					};
  				}
  			}
  		});
  		
  		modalInstance.result.then(function(defaultImage) {
  			$scope.findDeliveryAddresses();
			$scope.findBillingAddresses();
  			$log.info('Modal dismissed at: ' + new Date());
  		}, function() {
  			$log.info('Modal dismissed at: ' + new Date());
  		});
    }
	
	$scope.deleteAddress = function (address) {
		var uniqueId = address.id;
		var deleteRequest = $http.delete('addresses/delete/' + uniqueId);
		
		deleteRequest.success(function(response) {
			$scope.findDeliveryAddresses();
			$scope.findBillingAddresses();
		});
	};
	
	$scope.removeFromCart = function(uniqueId) {
		GrainCartService.removeFromCart(uniqueId)
			.then(function(response) {
				$rootScope.$broadcast(GRAINCART_EVENTS.itemRemoved);
			});
	};
	
	$scope.removeFromConsumerCart = function(uniqueId) {
		GrainCartService.removeFromConsumerCart(uniqueId)
			.then(function(response) {
				$rootScope.$broadcast(GRAINCART_EVENTS.itemRemoved);
			});
	};
	
	$scope.setBillingAddress = function (billingAddress) {
		$scope.orderDetails.billingAddress = billingAddress;
	};
	
	$scope.setShippingAddress = function (shippingAddress) {
		$scope.orderDetails.shippingAddress = shippingAddress;
	};
	
	$scope.shipItemsToBillingAddress = function () {
		if ($scope.shipItemsToBilling) {
			$scope.orderDetails.shippingAddress = $scope.orderDetails.billingAddress; 
		} else {
			$scope.orderDetails.shippingAddress = {};
		}
	};
	
	$scope.placeOrder = function () {
		$scope.orderDetails.grainCartId = $scope.grainCart.id;
		$http.post('orders/processOrder', $scope.orderDetails)
			.then(function(response) {
				$rootScope.$broadcast(GRAINCART_EVENTS.grainCartCleared);
				ngToast.create({ className: 'success', content: response.data.message });
				$location.path("/dashboard");
			});
	};
	
}]);