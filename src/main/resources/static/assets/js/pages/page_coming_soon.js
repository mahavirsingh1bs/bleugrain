var PageComingSoon = function () {

    return {
        
        //Coming Soon
        initPageComingSoon: function () {
			var newYear = new Date(2016, 06, 14, 23, 59);
			//$('#defaultCountdown').countdown({until: newYear});
			$('#defaultCountdown').countdown(newYear)
			.on('update.countdown', function(event) {
			  var format = '%H:%M:%S';
			  $(this).html(event.strftime(format));
			})
			.on('finish.countdown', function(event) {
			  $(this).html('This offer has expired!')
			    .parent().addClass('disabled');

			});
        }

    };
}();        