var MouseWheel = function () {

    return {

        initMouseWheel: function () {
        	jQuery('.quantity-snap').noUiSlider({
                start: [ 60, 430 ],
                snap: false,
                connect: true,
                range: {
                    'min': 0,
                    '12.5%': 75,
                    '25%': 150,
                    '37.5%': 225,
                    '50%': 300,
                    '62.5%': 375,
                    '75%': 450,
                    '87.5%': 525,
                    'max': 600
                }
            });
            jQuery('.quantity-snap').Link('lower').to(jQuery('.quantity-snap-value-lower'));
            jQuery('.quantity-snap').Link('upper').to(jQuery('.quantity-snap-value-upper'));
        	
            jQuery('.slider-snap').noUiSlider({
                start: [ 120, 420 ],
                snap: true,
                connect: true,
                range: {
                    'min': 0,
                    '5%': 20,
                    '10%': 40,
                    '15%': 80,
                    '20%': 120,
                    '25%': 160,
                    '30%': 200,
                    '35%': 240,
                    '40%': 280,
                    '50%': 300,
                    '60%': 340,
                    '70%': 380,
                    '80%': 420,
                    '90%': 460,
                    'max': 500
                }
            });
            jQuery('.slider-snap').Link('lower').to(jQuery('.slider-snap-value-lower'));
            jQuery('.slider-snap').Link('upper').to(jQuery('.slider-snap-value-upper'));
        }

    };

}();        