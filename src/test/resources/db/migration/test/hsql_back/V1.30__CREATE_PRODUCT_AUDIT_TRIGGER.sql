DROP TABLE CF_PRODUCT_HISTORY;

CREATE TABLE cf_product_history 
  ( 
     operation           CHAR(1) NOT NULL, 
     stamp               TIMESTAMP NOT NULL, 
     userid              TEXT NOT NULL, 
     id                  BIGINT NOT NULL, 
     address_line_1      VARCHAR(255) DEFAULT NULL, 
     address_line_2      VARCHAR(255) DEFAULT NULL, 
     city                VARCHAR(255) DEFAULT NULL, 
     zipcode             VARCHAR(255) DEFAULT NULL, 
     is_archived         BOOLEAN, 
     is_bid_enabled      BOOLEAN, 
     comments            VARCHAR(255) DEFAULT NULL, 
     is_consumer_product BOOLEAN, 
     assigned_on         TIMESTAMP, 
     created_on          TIMESTAMP, 
     modified_on         TIMESTAMP, 
     description         TEXT, 
     enable_till_date    TIMESTAMP, 
     initial_bid_amount  NUMERIC(19, 2) DEFAULT NULL, 
     NAME                VARCHAR(255) DEFAULT NULL, 
     quantity            NUMERIC(19, 2) DEFAULT NULL, 
     short_desc          TEXT, 
     unique_id           VARCHAR(255) NOT NULL, 
     obj_version         INTEGER, 
     country_id          BIGINT, 
     state_id            BIGINT, 
     assigned_to         BIGINT, 
     bidding_currency_id BIGINT, 
     bidding_unit_id     BIGINT, 
     created_by          BIGINT, 
     default_image_id    BIGINT, 
     highest_bid_id      BIGINT, 
     last_price_id       BIGINT, 
     latest_price_id     BIGINT, 
     max_price_id        BIGINT, 
     min_price_id        BIGINT, 
     modified_by         BIGINT, 
     owner_id            BIGINT, 
     qunit_id            BIGINT, 
     status_id           BIGINT, 
     is_mocked           BOOLEAN, 
     rating              NUMERIC(19, 2) DEFAULT 0, 
     total_reviews       BIGINT DEFAULT 0, 
     assigned_by         BIGINT, 
     product_no          VARCHAR(50) 
  );

CREATE OR REPLACE FUNCTION audit_product_details() RETURNS TRIGGER
AS 
$body$
	BEGIN
		IF (TG_OP = 'DELETE') THEN
			INSERT INTO CF_PRODUCT_HISTORY SELECT 'D', now(), user, OLD.*;
			RETURN OLD;
		ELSIF (TG_OP = 'UPDATE') THEN
			INSERT INTO CF_PRODUCT_HISTORY SELECT 'U', now(), user, NEW.*;
			RETURN NEW;
		ELSIF (TG_OP = 'INSERT') THEN
			INSERT INTO CF_PRODUCT_HISTORY SELECT 'I', now(), user, NEW.*;
			RETURN NEW;
		END IF;
		RETURN NULL;
	END;
$body$ 
LANGUAGE plpgsql;

CREATE TRIGGER audit_product 
	AFTER INSERT OR UPDATE OR DELETE ON CF_PRODUCT
	FOR EACH ROW EXECUTE PROCEDURE audit_product_details();