DROP TABLE CF_DEMAND_HISTORY;

create table CF_DEMAND_HISTORY (
	operation           CHAR(1) NOT NULL, 
    stamp               TIMESTAMP NOT NULL, 
    userid              TEXT NOT NULL, 
 id                 bigint                      not null,
 is_archived        boolean                     ,
 comments           varchar(255)      default NULL,
 assigned_on        timestamp,
 created_on         timestamp,
 modified_on        timestamp,
 address_line_1     varchar(255)      default NULL,
 address_line_2     varchar(255)      default NULL,
 city               varchar(255)      default NULL,
 zipcode            varchar(255)      default NULL,
 delivery_date      timestamp,
 description        text,
 max_price_per_unit numeric(19,2)               default NULL,
 min_price_per_unit numeric(19,2)               default NULL,
 name               varchar(255)      default NULL,
 price_per_unit     numeric(19,2)               default NULL,
 quantity           numeric(19,2)               default NULL,
 request_date       timestamp,
 short_desc         varchar(255)      default NULL,
 unique_id          varchar(255)      not null,
 assigned_to        bigint                      ,
 category_id        bigint                      ,
 company_id         bigint                      ,
 created_by         bigint                      ,
 currency_id        bigint                      ,
 default_image_id   bigint                      ,
 country_id         bigint                      ,
 state_id           bigint                      ,
 modified_by        bigint                      ,
 qunit_id           bigint                      ,
 status_id          bigint                      ,
 punit_id           bigint                      ,
 user_id            bigint                      ,
 is_mocked          boolean                     ,
 buyer_category     varchar(255)      default NULL,
 assigned_by        bigint                      ,
 obj_version        integer                     default 0
);

CREATE OR REPLACE FUNCTION audit_demand_details() RETURNS TRIGGER
AS 
$body$
	BEGIN
		IF (TG_OP = 'DELETE') THEN
			INSERT INTO CF_DEMAND_HISTORY SELECT 'D', now(), user, OLD.*;
			RETURN OLD;
		ELSIF (TG_OP = 'UPDATE') THEN
			INSERT INTO CF_DEMAND_HISTORY SELECT 'U', now(), user, NEW.*;
			RETURN NEW;
		ELSIF (TG_OP = 'INSERT') THEN
			INSERT INTO CF_DEMAND_HISTORY SELECT 'I', now(), user, NEW.*;
			RETURN NEW;
		END IF;
		RETURN NULL;
	END;
$body$ 
LANGUAGE plpgsql;

CREATE TRIGGER audit_demand 
	AFTER INSERT OR UPDATE OR DELETE ON CF_DEMAND
	FOR EACH ROW EXECUTE PROCEDURE audit_demand_details();