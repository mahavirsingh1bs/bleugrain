UPDATE CF_USER SET EMAIL = 'retailer@vocac.com', username = 'retailer@vocac.com' where id = 2;
UPDATE CF_USER SET EMAIL = 'company.admin@vocac.com', username = 'company.admin@vocac.com' where id = 3;
UPDATE CF_USER SET EMAIL = 'farmer@vocac.com', username = 'farmer@vocac.com' where id = 4;
UPDATE CF_USER SET EMAIL = 'company.agent@vocac.com', username = 'company.agent@vocac.com' where id = 5;
UPDATE CF_USER SET FIRST_NAME='Mahavir', LAST_NAME='Singh', ALTERNATE_USERNAME='+919717819586', MOBILE_NO='+919717819586', EMAIL = 'mahavir.singh@vocac.com', username = 'mahavir.singh@vocac.com' where id = 10;
UPDATE CF_USER SET FIRST_NAME='Ramhari', LAST_NAME='Singh', ALTERNATE_USERNAME='+918750851107', MOBILE_NO='+918750851107', EMAIL = 'ramhari.singh@vocac.com', username = 'ramhari.singh@vocac.com' where id = 11;
UPDATE CF_USER SET FIRST_NAME='Mahesh', LAST_NAME='Kumar', ALTERNATE_USERNAME='+918958484276', MOBILE_NO='+918958484276', EMAIL = 'mahesh.kumar@vocac.com', username = 'mahesh.kumar@vocac.com' where id = 12;
UPDATE CF_USER SET FIRST_NAME='Ramvir', LAST_NAME='Singh', ALTERNATE_USERNAME='+917351017710', MOBILE_NO='+917351017710', EMAIL = 'ramvir.singh@vocac.com', username = 'ramvir.singh@vocac.com' where id = 14;
UPDATE CF_USER SET EMAIL = 'broker@vocac.com', username = 'broker@vocac.com' where id = 17;

ALTER TABLE CF_CUSTOMER_CONCERN ALTER COLUMN CONCERN RENAME TO MESSAGE;