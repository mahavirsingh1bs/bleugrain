
    create table CF_ACCOUNT (
        id bigint not null,
        ACCOUNT_NO varchar(255),
        BANK_NAME varchar(255),
        BRANCH varchar(255),
        IFSC_CODE varchar(255),
        IS_ACTIVE boolean,
        IS_PRIMARY boolean,
        NAME varchar(255),
        OWNER_ID bigint,
        primary key (id)
    );

    create table CF_ACCOUNT_STATUS (
        id bigint not null,
        IS_ARCHIVED boolean,
        STATUS varchar(255),
        primary key (id)
    );

    create table CF_ACTIVITY (
        ID bigint not null,
        ACTIVITY integer,
        ACTIVITY_DATE timestamp,
        DEMAND_ID bigint,
        PRODUCT_ID bigint,
        USER_ID bigint,
        primary key (ID)
    );

    create table CF_ADDRESS (
        id bigint not null,
        ADDRESS_CATEGORY varchar(255),
        ADDRESS_LINE_1 varchar(255),
        ADDRESS_LINE_2 varchar(255),
        IS_ARCHIVED boolean,
        CITY varchar(255),
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        IS_DEFAULT_ADDRESS boolean,
        EMAIL_ADDRESS varchar(255),
        FIRST_NAME varchar(255),
        LAST_NAME varchar(255),
        PHONE_NO varchar(255),
        ZIPCODE varchar(255),
        COMPANY_ID bigint,
        COUNTRY_ID bigint,
        CREATED_BY bigint,
        MODIFIED_BY bigint,
        STATE_ID bigint,
        USER_ID bigint,
        primary key (id)
    );

    create table CF_BANK (
        id bigint not null,
        IS_ARCHIVED boolean,
        AUTH_URL varchar(255),
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        NAME varchar(255),
        PASSWORD varchar(255),
        USERNAME varchar(255),
        CREATED_BY bigint,
        MODIFIED_BY bigint,
        primary key (id)
    );

    create table CF_BID (
        ID bigint not null,
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        PRICE numeric(19,2),
        COMPANY_ID bigint,
        CREATED_BY bigint,
        CURRENCY_ID bigint,
        MODIFIED_BY bigint,
        PRODUCT_ID bigint,
        UNIT_ID bigint,
        USER_ID bigint,
        primary key (ID)
    );

    create table CF_BILLING_DETAILS (
        BILLING_DETAILS_TYPE varchar(31) not null,
        id bigint not null,
        IS_ARCHIVED boolean,
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        IS_DEFAULT_BILLING boolean,
        OWNER varchar(255),
        UNIQUE_ID varchar(255) not null,
        BA_ACCOUNT varchar(96),
        BA_SWIFT varchar(48),
        COMPANY_ID bigint,
        CREATED_BY bigint,
        MODIFIED_BY bigint,
        USER_ID bigint,
        BA_BANK_ID bigint,
        primary key (id)
    );

    create table CF_BRANCH (
        id bigint not null,
        ADDRESS_LINE_1 varchar(255),
        ADDRESS_LINE_2 varchar(255),
        CITY varchar(255),
        ZIPCODE varchar(255),
        IS_ARCHIVED boolean,
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        NAME varchar(255),
        COUNTRY_ID bigint,
        STATE_ID bigint,
        COMPANY_ID bigint,
        CREATED_BY bigint,
        MODIFIED_BY bigint,
        primary key (id)
    );

    create table CF_BUYER_WISHLIST (
        BUYER_ID bigint not null,
        PRODUCT_ID bigint not null,
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        CREATED_BY bigint,
        MODIFIED_BY bigint,
        primary key (BUYER_ID, PRODUCT_ID)
    );

    create table CF_CARD_TYPE (
        id bigint not null,
        IS_ARCHIVED boolean,
        TYPE varchar(255),
        primary key (id)
    );

    create table CF_CART_PRODUCT (
        GRAIN_CART_ID bigint not null,
        PRODUCT_ID bigint not null,
        primary key (GRAIN_CART_ID, PRODUCT_ID)
    );

    create table CF_CATEGORY (
        id bigint not null,
        IS_ARCHIVED boolean,
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        SHORT_DESC varchar(4000),
        name varchar(255),
        UNIQUE_ID varchar(255) not null,
        CREATED_BY bigint,
        DEFAULT_IMAGE_ID bigint,
        MODIFIED_BY bigint,
        PARENT_CATEGORY_ID bigint,
        primary key (id)
    );

    create table CF_CATEGORY_REVIEW (
        CATEGORY_ID bigint not null,
        COMMENT varchar(255),
        EMAIL_ADDRESS varchar(255),
        NAME varchar(255),
        RATING_ID bigint,
        REVIEW_BY bigint,
        REVIEW_DATE timestamp,
        IS_VALID boolean
    );

    create table CF_COMMENT (
        id bigint not null,
        COMMENT varchar(255),
        COMMENT_DATE timestamp,
        USER_ID bigint,
        DISCUSSION_ID bigint,
        primary key (id)
    );

    create table CF_COMPANY (
        id bigint not null,
        ADDRESS_LINE_1 varchar(255),
        ADDRESS_LINE_2 varchar(255),
        CITY varchar(255),
        ZIPCODE varchar(255),
        IS_ARCHIVED boolean,
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        DOMAIN varchar(255),
        EMAIL_ID varchar(255),
        IS_MOCKED boolean,
        NAME varchar(255),
        PHONE_NO varchar(255),
        REGISTR_NO varchar(255),
        SHORT_DESC varchar(4000),
        UNIQUE_ID varchar(255),
        VERIFIED boolean,
        OBJ_VERSION integer,
        WEBSITE_URL varchar(255),
        COUNTRY_ID bigint,
        STATE_ID bigint,
        CREATED_BY bigint,
        DEFAULT_BILLING_ADDRESS_ID bigint,
        DEFAULT_BILLING_DETAILS_ID bigint,
        DEFAULT_DELIVERY_ADDRESS_ID bigint,
        DEFAULT_IMAGE_ID bigint,
        MODIFIED_BY bigint,
        primary key (id)
    );

    create table CF_COMPANY_PLAN (
        COMPANY_ID bigint not null,
        PLAN_ID bigint not null,
        EXPIRY_DATE timestamp,
        IS_ACTIVE boolean,
        START_DATE timestamp,
        primary key (COMPANY_ID, PLAN_ID)
    );

    create table CF_COMPANY_REVIEW (
        COMPANY_ID bigint not null,
        COMMENT varchar(255),
        EMAIL_ADDRESS varchar(255),
        NAME varchar(255),
        RATING_ID bigint,
        REVIEW_BY bigint,
        REVIEW_DATE timestamp,
        IS_VALID boolean
    );

    create table CF_CONSUMER_PRODUCT (
        id bigint not null,
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        SHORT_DESC varchar(4000),
        NAME varchar(255),
        CATEGORY_ID bigint,
        CREATED_BY bigint,
        DEFAULT_IMAGE_ID bigint,
        LAST_PRICE_ID bigint,
        LATEST_PRICE_ID bigint,
        MODIFIED_BY bigint,
        primary key (id)
    );

    create table CF_CONSUMER_PRODUCT_REVIEW (
        PRODUCT_CATEGORY_ID bigint not null,
        COMMENT varchar(255),
        EMAIL_ADDRESS varchar(255),
        NAME varchar(255),
        RATING_ID bigint,
        REVIEW_BY bigint,
        REVIEW_DATE timestamp,
        IS_VALID boolean
    );

    create table CF_CONTACT (
        GROUP_ID bigint not null,
        USER_ID bigint not null,
        AREA varchar(255),
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        IS_ACTIVE boolean,
        MESSAGE varchar(255),
        COUNTRY_ID bigint,
        CREATED_BY bigint,
        MODIFIED_BY bigint,
        primary key (GROUP_ID, USER_ID)
    );

    create table CF_COUNTRY (
        id bigint not null,
        IS_ARCHIVED boolean,
        CODE varchar(255),
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        NAME varchar(255),
        CREATED_BY bigint,
        MODIFIED_BY bigint,
        primary key (id)
    );

    create table CF_CREDIT_CARD (
        CC_NUMBER varchar(96) not null,
        CC_EXP_MONTH_ID bigint,
        CC_EXP_YEAR_ID bigint,
        CC_PAYMENT_CARD_ID bigint,
        CREDIT_CARD_ID bigint not null,
        primary key (CREDIT_CARD_ID)
    );

    create table CF_CURRENCY (
        id bigint not null,
        IS_ARCHIVED boolean,
        COUNTRY_CODE varchar(255),
        MNEOMIC varchar(255),
        NAME varchar(255),
        SYMBOL varchar(255),
        primary key (id)
    );

    create table CF_CUSTOMER_CONCERN (
        id bigint not null,
        CONCERN varchar(255),
        CREATED_ON timestamp,
        EMAIL_ID varchar(255),
        NAME varchar(255),
        PHONE_NO varchar(255),
        USER_ID bigint,
        primary key (id)
    );

    create table CF_DELIVERY (
        id bigint not null,
        Type varchar(255),
        ADDRESS_LINE_1 varchar(255),
        ADDRESS_LINE_2 varchar(255),
        CITY varchar(255),
        ZIPCODE varchar(255),
        deliveryDate timestamp,
        COUNTRY_ID bigint,
        STATE_ID bigint,
        DRIVER_ID bigint,
        TRANSPORT_ID bigint,
        primary key (id)
    );

    create table CF_DELIVERY_CHARGE (
        id bigint not null,
        charge double not null,
        unit integer,
        primary key (id)
    );

    create table CF_DEMAND (
        id bigint not null,
        IS_ARCHIVED boolean,
        COMMENTS varchar(255),
        ASSIGNED_ON timestamp,
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        ADDRESS_LINE_1 varchar(255),
        ADDRESS_LINE_2 varchar(255),
        CITY varchar(255),
        ZIPCODE varchar(255),
        DELIVERY_DATE timestamp,
        DESCRIPTION TEXT,
        MAX_PRICE_PER_UNIT numeric(19,2),
        MIN_PRICE_PER_UNIT numeric(19,2),
        IS_MOCKED boolean,
        NAME varchar(255),
        PRICE_PER_UNIT numeric(19,2),
        QUANTITY double,
        REQUEST_DATE timestamp,
        SHORT_DESC varchar(255),
        UNIQUE_ID varchar(255) not null,
        ASSIGNED_TO bigint,
        CATEGORY_ID bigint,
        COMPANY_ID bigint,
        CREATED_BY bigint,
        CURRENCY_ID bigint,
        DEFAULT_IMAGE_ID bigint,
        COUNTRY_ID bigint,
        STATE_ID bigint,
        MODIFIED_BY bigint,
        QUNIT_ID bigint,
        STATUS_ID bigint,
        PUNIT_ID bigint,
        USER_ID bigint,
        primary key (id)
    );

    create table CF_DEMAND_HISTORY (
        id bigint not null,
        IS_ARCHIVED boolean,
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        ADDRESS_LINE_1 varchar(255),
        ADDRESS_LINE_2 varchar(255),
        CITY varchar(255),
        ZIPCODE varchar(255),
        DELIVERY_DATE timestamp,
        NAME varchar(255),
        PRICE_PER_UNIT numeric(19,2),
        QUANTITY double,
        REQUEST_DATE timestamp,
        COMPANY_ID bigint,
        CREATED_BY bigint,
        COUNTRY_ID bigint,
        STATE_ID bigint,
        DEMAND_ID bigint,
        MODIFIED_BY bigint,
        CURRENCY_ID bigint,
        PUNIT_ID bigint,
        QUNIT_ID bigint,
        STATUS_ID bigint,
        USER_ID bigint,
        primary key (id)
    );

    create table CF_DEMAND_STATUS (
        id bigint not null,
        IS_ARCHIVED boolean,
        CODE varchar(255),
        STATUS varchar(255),
        primary key (id)
    );

    create table CF_DEMAND_TAG (
        DEMAND_ID bigint not null,
        TAG_ID bigint not null,
        primary key (DEMAND_ID, TAG_ID)
    );

    create table CF_DEPARTMENT (
        id bigint not null,
        IS_ARCHIVED boolean,
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        NAME varchar(255),
        CREATED_BY bigint,
        MODIFIED_BY bigint,
        primary key (id)
    );

    create table CF_DESIGNATION (
        id bigint not null,
        IS_ARCHIVED boolean,
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        NAME varchar(255),
        CREATED_BY bigint,
        MODIFIED_BY bigint,
        primary key (id)
    );

    create table CF_DISCUSSION (
        id bigint not null,
        IS_ARCHIVED boolean,
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        DESCRIPTION varchar(255),
        LAST_COMMENT_DATE timestamp,
        TITLE varchar(255),
        UNIQUE_ID varchar(255) not null,
        COMPANY_ID bigint,
        CREATED_BY bigint,
        MODIFIED_BY bigint,
        USER_ID bigint,
        primary key (id)
    );

    create table CF_DISCUSSION_GROUP (
        DISCUSSION_ID bigint not null,
        GROUP_ID bigint not null,
        primary key (DISCUSSION_ID, GROUP_ID)
    );

    create table CF_DISCUSSION_TAG (
        DISCUSSION_ID bigint not null,
        TAG_ID bigint not null,
        primary key (DISCUSSION_ID, TAG_ID)
    );

    create table CF_EVENT (
        id bigint not null,
        CODE varchar(255),
        NAME varchar(255),
        primary key (id)
    );

    create table CF_EXPERTISE (
        id bigint not null,
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        EXPR_LABEL varchar(255),
        EXPERIENCE varchar(255),
        NAME varchar(255),
        UNIQUE_ID varchar(255) not null,
        OBJ_VERSION integer,
        COMPANY_ID bigint,
        CREATED_BY bigint,
        MODIFIED_BY bigint,
        USER_ID bigint,
        primary key (id)
    );

    create table CF_EXPERTISE_REVIEW (
        EXPERTISE_ID bigint not null,
        COMMENT varchar(255),
        EMAIL_ADDRESS varchar(255),
        NAME varchar(255),
        RATING_ID bigint,
        REVIEW_BY bigint,
        REVIEW_DATE timestamp,
        IS_VALID boolean
    );

    create table CF_EXPIRY_MONTH (
        id bigint not null,
        IS_ARCHIVED boolean,
        MONTH varchar(255),
        NAME varchar(255),
        primary key (id)
    );

    create table CF_EXPIRY_YEAR (
        id bigint not null,
        IS_ARCHIVED boolean,
        NAME varchar(255),
        primary key (id)
    );

    create table CF_FEEDBACK (
        id bigint not null,
        CREATED_ON timestamp,
        FEEDBACK varchar(255),
        EXECUTIVES_RATING bigint,
        SERVICES_RATING bigint,
        SITE_RATING bigint,
        USER_ID bigint,
        primary key (id)
    );

    create table CF_FILTER (
        id bigint not null,
        IS_ARCHIVED boolean,
        CATEGORY varchar(255),
        NAME varchar(255),
        primary key (id)
    );

    create table CF_FLOOR_CHARGE (
        id bigint not null,
        charge double not null,
        unit integer,
        primary key (id)
    );

    create table CF_GRAIN_CART (
        id bigint not null,
        IS_CONSUMER_CART boolean,
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        COMPANY_ID bigint,
        CREATED_BY bigint,
        MODIFIED_BY bigint,
        USER_ID bigint,
        primary key (id)
    );

    create table CF_GROUP (
        id bigint not null,
        IS_ARCHIVED boolean,
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        NAME varchar(255),
        UNIQUE_ID varchar(255) not null,
        CREATED_BY bigint,
        MODIFIED_BY bigint,
        primary key (id)
    );

    create table CF_GROUP_ROLE (
        GROUP_ID bigint not null,
        ROLE_ID bigint not null,
        primary key (GROUP_ID, ROLE_ID)
    );

    create table CF_IMAGE (
        id bigint not null,
        IMAGE_CATEGORY varchar(255),
        FILENAME varchar(255),
        FILEPATH varchar(255),
        IS_DEFAULT boolean,
        LARGE_FILEPATH varchar(255),
        MEDIUM_FILEPATH varchar(255),
        NAME varchar(255),
        SMALL_FILEPATH varchar(255),
        COMPANY_ID bigint,
        PRODUCT_ID bigint,
        USER_ID bigint,
        primary key (id)
    );

    create table CF_INDIVIDUAL_PLAN (
        PLAN_ID bigint not null,
        USER_ID bigint not null,
        EXPIRY_DATE timestamp,
        IS_ACTIVE boolean,
        START_DATE timestamp,
        primary key (PLAN_ID, USER_ID)
    );

    create table CF_ITEM (
        id bigint not null,
        NAME varchar(255),
        QUANTITY double,
        UNIQUE_ID varchar(255) not null,
        OBJ_VERSION integer,
        IMAGE_ID bigint,
        PRICE_ID bigint,
        QUNIT_ID bigint,
        GRAIN_CART_ID bigint,
        ORDER_ID bigint,
        primary key (id)
    );

    create table CF_LANGUAGE (
        id bigint not null,
        IS_ARCHIVED boolean,
        CODE varchar(255),
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        NAME varchar(255),
        CREATED_BY bigint,
        MODIFIED_BY bigint,
        primary key (id)
    );

    create table CF_LICENSE (
        id bigint not null,
        licenseNo varchar(255),
        type integer,
        validFor varchar(255),
        DRIVER_ID bigint,
        primary key (id)
    );

    create table CF_NOTE (
        id bigint not null,
        IS_ARCHIVED boolean,
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        NOTE varchar(1000),
        TITLE varchar(255),
        UNIQUE_ID varchar(255) not null,
        COMPANY_ID bigint,
        CREATED_BY bigint,
        MODIFIED_BY bigint,
        USER_ID bigint,
        primary key (id)
    );

    create table CF_NOTIF_CATEGORY (
        id bigint not null,
        IS_ARCHIVED boolean,
        CODE varchar(255),
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        NAME varchar(255),
        CREATED_BY bigint,
        DEFAULT_IMAGE_ID bigint,
        MODIFIED_BY bigint,
        primary key (id)
    );

    create table CF_NOTIFICATION (
        id bigint not null,
        IS_ARCHIVED boolean,
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        EXPIRY_DATE timestamp,
        NOTIFICATION varchar(255),
        START_DATE timestamp,
        TITLE varchar(255),
        CATEGORY_ID bigint,
        COMPANY_ID bigint,
        CREATED_BY bigint,
        MODIFIED_BY bigint,
        USER_ID bigint,
        primary key (id)
    );

    create table CF_NOTIFICATION_COMPANY (
        NOTIFICATION_ID bigint not null,
        COMPANY_ID bigint not null,
        primary key (NOTIFICATION_ID, COMPANY_ID)
    );

    create table CF_NOTIFICATION_GROUP (
        NOTIFICATION_ID bigint not null,
        GROUP_ID bigint not null,
        primary key (NOTIFICATION_ID, GROUP_ID)
    );

    create table CF_NOTIFICATION_USER (
        NOTIFICATION_ID bigint not null,
        USER_ID bigint not null,
        primary key (NOTIFICATION_ID, USER_ID)
    );

    create table CF_ORDER (
        id bigint not null,
        AMOUNT_PAID numeric(19,2),
        IS_ARCHIVED boolean,
        COMMENTS varchar(255),
        ASSIGNED_ON timestamp,
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        DELIVERY_DATE timestamp,
        IS_MOCKED boolean,
        ORDER_DATE timestamp,
        ORDER_NO varchar(255) not null,
        SHIPPING_COST numeric(19,2),
        SUBTOTAL numeric(19,2),
        TOTAL_PRICE numeric(19,2),
        ASSIGNED_TO bigint,
        BILLING_ADDRESS_ID bigint,
        COMPANY_ID bigint,
        CREATED_BY bigint,
        DELIVER_BY bigint,
        FULFILLED_BY bigint,
        MODIFIED_BY bigint,
        SHIPPING_ADDRESS_ID bigint,
        STATUS_ID bigint,
        USER_ID bigint,
        primary key (id)
    );

    create table CF_ORDER_HISTORY (
        id bigint not null,
        AMOUNT_PAID numeric(19,2),
        IS_ARCHIVED boolean,
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        DELIVERY_DATE timestamp,
        ORDER_DATE timestamp,
        ORDER_NO varchar(255) not null,
        SHIPPING_COST numeric(19,2),
        SUBTOTAL numeric(19,2),
        TOTAL_PRICE numeric(19,2),
        BILLING_ADDRESS_ID bigint,
        BUYER_ID bigint,
        COMPANY_ID bigint,
        CREATED_BY bigint,
        FULFILLED_BY bigint,
        MODIFIED_BY bigint,
        ORDER_ID bigint,
        SHIPPING_ADDRESS_ID bigint,
        STATUS_ID bigint,
        primary key (id)
    );

    create table CF_ORDER_PRODUCTS (
        ORDER_ID bigint not null,
        PRODUCT_ID bigint not null,
        primary key (ORDER_ID, PRODUCT_ID)
    );

    create table CF_ORDER_STATUS (
        id bigint not null,
        IS_ARCHIVED boolean,
        CODE varchar(255),
        STATUS varchar(255),
        primary key (id)
    );

    create table CF_PAYMENT (
        id bigint not null,
        amount numeric(19,2),
        method varchar(255),
        paymentDate timestamp,
        primary key (id)
    );

    create table CF_PAYMENT_CARD (
        id bigint not null,
        IS_ARCHIVED boolean,
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        NAME varchar(255),
        CREATED_BY bigint,
        DEFAULT_IMAGE_ID bigint,
        MODIFIED_BY bigint,
        primary key (id)
    );

    create table CF_PAYMENT_METHOD (
        id bigint not null,
        IS_ARCHIVED boolean,
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        NAME varchar(255),
        IS_PRIMARY boolean,
        CREATED_BY bigint,
        MODIFIED_BY bigint,
        COMPANY_ID bigint,
        primary key (id)
    );

    create table CF_PERMIT (
        id bigint not null,
        state varchar(255),
        TRANSPORT_ID bigint,
        primary key (id)
    );

    create table CF_PERSON (
        id bigint not null,
        ADDRESS_LINE_1 varchar(255),
        ADDRESS_LINE_2 varchar(255),
        CITY varchar(255),
        ZIPCODE varchar(255),
        email varchar(255),
        firstName varchar(255),
        lastName varchar(255),
        mobileNo varchar(255),
        phoneNo varchar(255),
        COUNTRY_ID bigint,
        STATE_ID bigint,
        primary key (id)
    );

    create table CF_PLAN (
        id bigint not null,
        CURRENCY varchar(255),
        NAME varchar(255),
        NO_OF_USERS integer,
        PRICE numeric(19,2),
        PRICING_BY varchar(255),
        primary key (id)
    );

    create table CF_PRAKRATI_SEQUENCES (
        SEQUENCE_NAME varchar(255) not null,
        NEXT_VALUE bigint,
        primary key (SEQUENCE_NAME)
    );

    create table CF_PRICE (
        id bigint not null,
        IS_INITIAL boolean,
        PRICE double,
        PRICE_DATE timestamp,
        CURRENCY_ID bigint,
        UNIT_ID bigint,
        PRODUCT_ID bigint,
        CONSUMER_PRODUCT_ID bigint,
        primary key (id)
    );

    create table CF_PROD_ATTRS (
        PRODUCT_ID bigint not null,
        NAME varchar(255),
        VALUE varchar(255)
    );

    create table CF_PRODUCT (
        id bigint not null,
        ADDRESS_LINE_1 varchar(255),
        ADDRESS_LINE_2 varchar(255),
        CITY varchar(255),
        ZIPCODE varchar(255),
        IS_ARCHIVED boolean,
        IS_BID_ENABLED boolean,
        COMMENTS varchar(255),
        IS_CONSUMER_PRODUCT boolean,
        ASSIGNED_ON timestamp,
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        DESCRIPTION TEXT,
        ENABLE_TILL_DATE timestamp,
        INITIAL_BID_AMOUNT numeric(19,2),
        IS_MOCKED boolean,
        NAME varchar(255),
        QUANTITY double,
        RATING double,
        SHORT_DESC varchar(600),
        TOTAL_REVIEWS integer default 0,
        UNIQUE_ID varchar(255) not null,
        OBJ_VERSION integer,
        COUNTRY_ID bigint,
        STATE_ID bigint,
        ASSIGNED_TO bigint,
        BIDDING_CURRENCY_ID bigint,
        BIDDING_UNIT_ID bigint,
        CREATED_BY bigint,
        DEFAULT_IMAGE_ID bigint,
        HIGHEST_BID_ID bigint,
        LAST_PRICE_ID bigint,
        LATEST_PRICE_ID bigint,
        MAX_PRICE_ID bigint,
        MIN_PRICE_ID bigint,
        MODIFIED_BY bigint,
        OWNER_ID bigint,
        QUNIT_ID bigint,
        STATUS_ID bigint,
        CATEGORY_ID bigint,
        primary key (id)
    );

    create table CF_PRODUCT_CATEGORY (
        PRODUCT_ID bigint not null,
        CATEGORY_ID bigint not null,
        primary key (PRODUCT_ID, CATEGORY_ID)
    );

    create table CF_PRODUCT_EVENT (
        id bigint not null,
        EVENT_DATE timestamp,
        UNIQUE_ID varchar(255),
        OBJ_VERSION integer,
        EVENT_BY bigint,
        PRODUCT_ID bigint,
        PRODUCT_STATUS_ID bigint,
        primary key (id)
    );

    create table CF_PRODUCT_HISTORY (
        id bigint not null,
        IS_ARCHIVED boolean,
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        DESCRIPTION TEXT,
        NAME varchar(255),
        QUANTITY double,
        TAGS varchar(255),
        OBJ_VERSION integer,
        CATEGORY_ID bigint,
        CREATED_BY bigint,
        DEFAULT_IMAGE_ID bigint,
        LAST_PRICE_ID bigint,
        LATEST_PRICE_ID bigint,
        MODIFIED_BY bigint,
        OWNER_ID bigint,
        PRODUCT_ID bigint,
        QUNIT_ID bigint,
        STATUS_ID bigint,
        primary key (id)
    );

    create table CF_PRODUCT_REVIEW (
        PRODUCT_ID bigint not null,
        COMMENT varchar(255),
        EMAIL_ADDRESS varchar(255),
        NAME varchar(255),
        RATING_ID bigint,
        REVIEW_BY bigint,
        REVIEW_DATE timestamp,
        IS_VALID boolean
    );

    create table CF_PRODUCT_STATUS (
        id bigint not null,
        IS_ARCHIVED boolean,
        CODE varchar(255),
        STATUS varchar(255),
        primary key (id)
    );

    create table CF_PRODUCT_TAG (
        PRODUCT_ID bigint not null,
        TAG_ID bigint not null,
        primary key (PRODUCT_ID, TAG_ID)
    );

    create table CF_PRODUCT_TYPE (
        id bigint not null,
        IS_ARCHIVED boolean,
        TYPE varchar(255),
        TYPE_CODE varchar(255),
        primary key (id)
    );

    create table CF_RATING (
        id bigint not null,
        IS_ARCHIVED boolean,
        VALUE double,
        primary key (id)
    );

    create table CF_ROLE (
        id bigint not null,
        IS_ARCHIVED boolean,
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        IS_FROZEN boolean,
        NAME varchar(255),
        CREATED_BY bigint,
        MODIFIED_BY bigint,
        primary key (id)
    );

    create table CF_SELLER_WISHLIST (
        DEMAND_ID bigint not null,
        SELLER_ID bigint not null,
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        CREATED_BY bigint,
        MODIFIED_BY bigint,
        primary key (DEMAND_ID, SELLER_ID)
    );

    create table CF_SOCIAL_ACCOUNTS (
        id bigint not null,
        ACCOUNT_URL varchar(255),
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        DISPLAY_NAME varchar(255),
        SOCIAL_SITE varchar(255),
        UNIQUE_ID varchar(255) not null,
        USERNAME varchar(255),
        OBJ_VERSION integer,
        COMPANY_ID bigint,
        CREATED_BY bigint,
        MODIFIED_BY bigint,
        USER_ID bigint,
        primary key (id)
    );

    create table CF_STATE (
        id bigint not null,
        IS_ARCHIVED boolean,
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        NAME varchar(255),
        COUNTRY_ID bigint,
        CREATED_BY bigint,
        MODIFIED_BY bigint,
        primary key (id)
    );

    create table CF_TAG (
        id bigint not null,
        IS_ARCHIVED boolean,
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        SHORT_DESC varchar(255),
        NAME varchar(255),
        CREATED_BY bigint,
        MODIFIED_BY bigint,
        primary key (id)
    );

    create table CF_TRANSACTION (
        id bigint not null,
        STATE varchar(255),
        primary key (id)
    );

    create table CF_TRANSPORT (
        id bigint not null,
        type varchar(255),
        vehicleNbr varchar(255),
        primary key (id)
    );

    create table CF_UNIT (
        id bigint not null,
        IS_ARCHIVED boolean,
        COUNTRY_CODE varchar(255),
        IS_DEFAULT boolean,
        NAME varchar(255),
        SYMBOL varchar(255),
        primary key (id)
    );

    create table CF_UPCOMING_EVENT (
        id bigint not null,
        IS_ARCHIVED boolean,
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        EVENT varchar(255),
        EVENT_DATE timestamp,
        EVENT_DETAIL varchar(255),
        UNIQUE_ID varchar(255) not null,
        COMPANY_ID bigint,
        CREATED_BY bigint,
        MODIFIED_BY bigint,
        USER_ID bigint,
        primary key (id)
    );

    create table CF_UPCOMING_EVENT_COMPANY (
        UPCOMING_EVENT_ID bigint not null,
        COMPANY_ID bigint not null,
        primary key (UPCOMING_EVENT_ID, COMPANY_ID)
    );

    create table CF_UPCOMING_EVENT_GROUP (
        UPCOMING_EVENT_ID bigint not null,
        GROUP_ID bigint not null,
        primary key (UPCOMING_EVENT_ID, GROUP_ID)
    );

    create table CF_UPCOMING_EVENT_USER (
        UPCOMING_EVENT_ID bigint not null,
        USER_ID bigint not null,
        primary key (UPCOMING_EVENT_ID, USER_ID)
    );

    create table CF_USER (
        ID bigint not null,
        ADDRESS_LINE_1 varchar(255),
        ADDRESS_LINE_2 varchar(255),
        CITY varchar(255),
        ZIPCODE varchar(255),
        ALTERNATE_USERNAME varchar(255),
        IS_ARCHIVED boolean,
        CREATED_ON timestamp,
        MODIFIED_ON timestamp,
        DEFAULT_LOCALE varchar(255),
        EMAIL varchar(255),
        EMAIL_VERIFIED boolean,
        EMAIL_VERIFY_CODE varchar(255),
        EMPLOYEE_ID varchar(255),
        FAILED_LOGIN_ATTEMPT integer,
        FIRST_NAME varchar(255),
        LAST_NAME varchar(255),
        MIDDLE_INITIAL varchar(255),
        MOBILE_NO varchar(255),
        MOBILE_VERIFIED boolean,
        MOBILE_VERIFY_CODE varchar(255),
        IS_MOCKED boolean,
        CF_PASSWD varchar(255),
        PHONE_NO varchar(255),
        RESET_PASSWD_CODE varchar(255),
        SHORT_DESC varchar(4000),
        IS_TRANSPORT_AVAILABLE boolean,
        UNIQUE_CODE varchar(255),
        USER_NO varchar(255) not null,
        USER_TYPE varchar(255) not null,
        USERNAME varchar(255),
        VERIFIED boolean,
        OBJ_VERSION integer,
        WEBSITE_URL varchar(255),
        COUNTRY_ID bigint,
        STATE_ID bigint,
        COMPANY_ID bigint,
        CREATED_BY bigint,
        DEFAULT_BILLING_ADDRESS_ID bigint,
        DEFAULT_BILLING_DETAILS_ID bigint,
        DEFAULT_COUNTRY_ID bigint,
        DEFAULT_DELIVERY_ADDRESS_ID bigint,
        DEFAULT_IMAGE_ID bigint,
        DEFAULT_LANGUAGE_ID bigint,
        DEPARTMENT_ID bigint,
        DESIGNATION_ID bigint,
        GROUP_ID bigint,
        MODIFIED_BY bigint,
        primary key (ID)
    );

    create table CF_USER_PLAN (
        PLAN_ID bigint not null,
        USER_ID bigint not null,
        EXPIRY_DATE timestamp,
        IS_ACTIVE boolean,
        START_DATE timestamp,
        primary key (PLAN_ID, USER_ID)
    );

    create table CF_USER_REVIEW (
        USER_ID bigint not null,
        COMMENT varchar(255),
        EMAIL_ADDRESS varchar(255),
        NAME varchar(255),
        RATING_ID bigint,
        REVIEW_BY bigint,
        REVIEW_DATE timestamp,
        IS_VALID boolean
    );

    create table CF_WISHLIST (
        id bigint not null,
        IS_ACTIVE boolean,
        CREATED_ON timestamp,
        IS_MOCKED boolean,
        MODIFIED_ON timestamp,
        UNIQUE_ID varchar(255),
        OBJ_VERSION integer,
        COMPANY_ID bigint,
        CREATED_BY bigint,
        MODIFIED_BY bigint,
        USER_ID bigint,
        primary key (id)
    );

    create table CF_WISHLIST_DEMAND (
        WISHLIST_ID bigint not null,
        DEMAND_ID bigint not null,
        primary key (WISHLIST_ID, DEMAND_ID)
    );

    create table CF_WISHLIST_PRODUCT (
        WISHLIST_ID bigint not null,
        PRODUCT_ID bigint not null,
        primary key (WISHLIST_ID, PRODUCT_ID)
    );

    alter table CF_BILLING_DETAILS 
        add constraint UK_8h9fk02312dwkopgr7lr9g99p unique (UNIQUE_ID);

    alter table CF_CATEGORY 
        add constraint UK_jp4vmdexy6x2qgu404b045y3a unique (UNIQUE_ID);

    alter table CF_DEMAND 
        add constraint UK_fbaoiw43crs6hhuh8rj7nxles unique (UNIQUE_ID);

    alter table CF_DISCUSSION 
        add constraint UK_m50ylmp0y6ucgsm50vhqojpac unique (UNIQUE_ID);

    alter table CF_EXPERTISE 
        add constraint UK_e3wgvh2yqv70dkr496qlnc8oh unique (UNIQUE_ID);

    alter table CF_GROUP 
        add constraint UK_gf98itqa6b5wbvhax53awnt4y unique (UNIQUE_ID);

    alter table CF_ITEM 
        add constraint UK_7c2i76hqm3wnfo0o7l3t97s2q unique (UNIQUE_ID);

    alter table CF_NOTE 
        add constraint UK_he0n6xm1bvwat21nctaqfbugl unique (UNIQUE_ID);

    alter table CF_ORDER 
        add constraint UK_9p97tb3ehmkvu46xaap6rhwho unique (ORDER_NO);

    alter table CF_ORDER_HISTORY 
        add constraint UK_4wcxd7jiap2vbnbgv8pm89iks unique (ORDER_NO);

    alter table CF_ORDER_PRODUCTS 
        add constraint UK_i5wyo0mn5sv1h9fj7msbw45qa unique (PRODUCT_ID);

    alter table CF_PRODUCT 
        add constraint UK_qkk822n1110dk98wlw5y77wsi unique (UNIQUE_ID);

    alter table CF_SOCIAL_ACCOUNTS 
        add constraint UK_o807myk1u6y4hwjgtd9djj9u1 unique (UNIQUE_ID);

    alter table CF_UPCOMING_EVENT 
        add constraint UK_osxeqs882h00krjx32tx9cbed unique (UNIQUE_ID);

    alter table CF_USER 
        add constraint UK_ipmfxgb8xja6xaqlwh3s7q5cv unique (ALTERNATE_USERNAME);

    alter table CF_USER 
        add constraint UK_qcmore2xd8y9l4b7n60f0nye4 unique (USER_NO);

    alter table CF_USER 
        add constraint UK_tmo2hvbcvvx4ifpjf41vfy6hr unique (USERNAME);

    alter table CF_ACCOUNT 
        add constraint FKbe8gxixtm696sae32ttrsbgv4 
        foreign key (OWNER_ID) 
        references CF_USER;

    alter table CF_ACTIVITY 
        add constraint FKomdq4ucxd0wtlk4xuqw2y34il 
        foreign key (DEMAND_ID) 
        references CF_DEMAND;

    alter table CF_ACTIVITY 
        add constraint FKpulbq0mwnhwgasun2h5ckvgvl 
        foreign key (PRODUCT_ID) 
        references CF_PRODUCT;

    alter table CF_ACTIVITY 
        add constraint FKmqwu9oa724mlc6m4cmexjtof0 
        foreign key (USER_ID) 
        references CF_USER;

    alter table CF_ADDRESS 
        add constraint FKqbu9rvb52xndk6uwwu0obb43r 
        foreign key (COMPANY_ID) 
        references CF_COMPANY;

    alter table CF_ADDRESS 
        add constraint FKp7d31mmr4kx7hwy2rg9wy2qlk 
        foreign key (COUNTRY_ID) 
        references CF_COUNTRY;

    alter table CF_ADDRESS 
        add constraint FKh8qkqo48xi3x3sbh8p5cry03c 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_ADDRESS 
        add constraint FKhso72omeiienygn87ng82gmwm 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_ADDRESS 
        add constraint FKh4wtafcwbmtf8riwf4am97rct 
        foreign key (STATE_ID) 
        references CF_STATE;

    alter table CF_ADDRESS 
        add constraint FKs16mar6w4adgs940ksxvs0jty 
        foreign key (USER_ID) 
        references CF_USER;

    alter table CF_BANK 
        add constraint FK88otnv6xopcai80tt7xo6rocv 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_BANK 
        add constraint FKpod3ox582omdfuetbwahrng9v 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_BID 
        add constraint FKqiju6cj86ye3g3qwy3lkvrk7l 
        foreign key (COMPANY_ID) 
        references CF_COMPANY;

    alter table CF_BID 
        add constraint FKtrhidt21v2uko59w5yoqd2l76 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_BID 
        add constraint FK129x3kgq6d06a13uo9h23tf5g 
        foreign key (CURRENCY_ID) 
        references CF_CURRENCY;

    alter table CF_BID 
        add constraint FKckolkg93g714o0i0lwycd87he 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_BID 
        add constraint FKt7w663wr87b3c7ct92nek88g4 
        foreign key (PRODUCT_ID) 
        references CF_PRODUCT;

    alter table CF_BID 
        add constraint FKcyiic4wmv08p8mrjrx3p135y4 
        foreign key (UNIT_ID) 
        references CF_UNIT;

    alter table CF_BID 
        add constraint FKmpp0fh8fslcfbd44a19e7nuyb 
        foreign key (USER_ID) 
        references CF_USER;

    alter table CF_BILLING_DETAILS 
        add constraint FK93b9478sc6tc81f59j6asu7x7 
        foreign key (COMPANY_ID) 
        references CF_COMPANY;

    alter table CF_BILLING_DETAILS 
        add constraint FKfx63lur1sk2yu7gks2uop69l2 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_BILLING_DETAILS 
        add constraint FKky03le3r7rixf8w4eeijiee5i 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_BILLING_DETAILS 
        add constraint FK669s1a7r9tc4m3pqyekioqyiq 
        foreign key (USER_ID) 
        references CF_USER;

    alter table CF_BILLING_DETAILS 
        add constraint FKtq0thsyhvh0yv84j1pcm8tomb 
        foreign key (BA_BANK_ID) 
        references CF_BANK;

    alter table CF_BRANCH 
        add constraint FKa51e9pmwnjh0a9f6v9jjcjwv8 
        foreign key (COUNTRY_ID) 
        references CF_COUNTRY;

    alter table CF_BRANCH 
        add constraint FK9rt2qer54vpqlf8yr9yhwe0jo 
        foreign key (STATE_ID) 
        references CF_STATE;

    alter table CF_BRANCH 
        add constraint FKpombm0d7cqls6gp36gpoydig6 
        foreign key (COMPANY_ID) 
        references CF_COMPANY;

    alter table CF_BRANCH 
        add constraint FK3xii8q89m1vdenp0ccuyl4l 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_BRANCH 
        add constraint FKshfkxfjs7bfs5unom9js17dnk 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_BUYER_WISHLIST 
        add constraint FKf19ckiiquwl1fl15nw2jbmtns 
        foreign key (BUYER_ID) 
        references CF_USER;

    alter table CF_BUYER_WISHLIST 
        add constraint FKlyuknejucwqpe9kq2uwc8r5h4 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_BUYER_WISHLIST 
        add constraint FK6recwbdjypfka292ylm5yyrn 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_BUYER_WISHLIST 
        add constraint FKgjq81djbnou9so9b9vc2g9mbd 
        foreign key (PRODUCT_ID) 
        references CF_PRODUCT;

    alter table CF_CART_PRODUCT 
        add constraint FKatq18goal76jftd84699ko7wa 
        foreign key (PRODUCT_ID) 
        references CF_PRODUCT;

    alter table CF_CART_PRODUCT 
        add constraint FKnq4ayf4iynh65nyxo5o5jo0u0 
        foreign key (GRAIN_CART_ID) 
        references CF_GRAIN_CART;

    alter table CF_CATEGORY 
        add constraint FKm082m4e0ce7em7ddqui39vx2e 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_CATEGORY 
        add constraint FKn0pen1eox0r1a4fh5kiq795d8 
        foreign key (DEFAULT_IMAGE_ID) 
        references CF_IMAGE;

    alter table CF_CATEGORY 
        add constraint FKbpcgcqylkcl1aet6r957nwmqg 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_CATEGORY 
        add constraint FKqafncmwucntf6l94aiwof3eu 
        foreign key (PARENT_CATEGORY_ID) 
        references CF_CATEGORY;

    alter table CF_CATEGORY_REVIEW 
        add constraint FKptayy9hnjfni70qwxkf88llry 
        foreign key (RATING_ID) 
        references CF_RATING;

    alter table CF_CATEGORY_REVIEW 
        add constraint FK1qppp4cvub535mqlpkcp1yhi5 
        foreign key (REVIEW_BY) 
        references CF_USER;

    alter table CF_CATEGORY_REVIEW 
        add constraint FKt31v91j6m2nwgobtu3xio3q5u 
        foreign key (CATEGORY_ID) 
        references CF_CATEGORY;

    alter table CF_COMMENT 
        add constraint FKshiv5miipxdvoxgjaff9sgrbp 
        foreign key (USER_ID) 
        references CF_USER;

    alter table CF_COMMENT 
        add constraint FKfs5bhkena42ptd4foks7vpd4 
        foreign key (DISCUSSION_ID) 
        references CF_DISCUSSION;

    alter table CF_COMPANY 
        add constraint FKa8bnfpaawvnp7p4uuky221rig 
        foreign key (COUNTRY_ID) 
        references CF_COUNTRY;

    alter table CF_COMPANY 
        add constraint FKgrcaeuupp3vhxib473cmtgbfy 
        foreign key (STATE_ID) 
        references CF_STATE;

    alter table CF_COMPANY 
        add constraint FKcglv81evin41psy1cf9gs1r99 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_COMPANY 
        add constraint FK9hy2sr0whuql43tgm2lvh3311 
        foreign key (DEFAULT_BILLING_ADDRESS_ID) 
        references CF_ADDRESS;

    alter table CF_COMPANY 
        add constraint FKlxnapd6uaqx331a03831nd65x 
        foreign key (DEFAULT_BILLING_DETAILS_ID) 
        references CF_BILLING_DETAILS;

    alter table CF_COMPANY 
        add constraint FKq39qnmlanfc8gsddil3gtyc9d 
        foreign key (DEFAULT_DELIVERY_ADDRESS_ID) 
        references CF_ADDRESS;

    alter table CF_COMPANY 
        add constraint FKjv8uh68irdw36d974k1tcqsmv 
        foreign key (DEFAULT_IMAGE_ID) 
        references CF_IMAGE;

    alter table CF_COMPANY 
        add constraint FKh97uu3gs49gx2tv6cmj9ecs8c 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_COMPANY_PLAN 
        add constraint FK2bcnhswbjh1dj12q2v4p5o4fs 
        foreign key (COMPANY_ID) 
        references CF_COMPANY;

    alter table CF_COMPANY_PLAN 
        add constraint FKhfhbwxc402gk4yhm76a3myl40 
        foreign key (PLAN_ID) 
        references CF_PLAN;

    alter table CF_COMPANY_REVIEW 
        add constraint FKtnngxflgksdepdt5o8x5lybm6 
        foreign key (RATING_ID) 
        references CF_RATING;

    alter table CF_COMPANY_REVIEW 
        add constraint FKbr4mevv0ivaictqhb7hp0slyu 
        foreign key (REVIEW_BY) 
        references CF_USER;

    alter table CF_COMPANY_REVIEW 
        add constraint FKhh8enjux74k5021a0d2ax07vb 
        foreign key (COMPANY_ID) 
        references CF_COMPANY;

    alter table CF_CONSUMER_PRODUCT 
        add constraint FKm7nxijmd72cremt3q395seiei 
        foreign key (CATEGORY_ID) 
        references CF_CATEGORY;

    alter table CF_CONSUMER_PRODUCT 
        add constraint FK42i9si9198ybqbhsbf0pq92qr 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_CONSUMER_PRODUCT 
        add constraint FKio78jrd6gx358eti3t61vm03p 
        foreign key (DEFAULT_IMAGE_ID) 
        references CF_IMAGE;

    alter table CF_CONSUMER_PRODUCT 
        add constraint FK7f37e7yoa60i4vov5dydpvrx4 
        foreign key (LAST_PRICE_ID) 
        references CF_PRICE;

    alter table CF_CONSUMER_PRODUCT 
        add constraint FKn2yqxm5a0kvn4f8mj8curt64q 
        foreign key (LATEST_PRICE_ID) 
        references CF_PRICE;

    alter table CF_CONSUMER_PRODUCT 
        add constraint FKapavi8v2ew8wwmeuyb928pjoy 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_CONSUMER_PRODUCT_REVIEW 
        add constraint FK2sjqsu2picny2gvn1kq3qs15y 
        foreign key (RATING_ID) 
        references CF_RATING;

    alter table CF_CONSUMER_PRODUCT_REVIEW 
        add constraint FKmnterspk443yaroe4cb4tw92v 
        foreign key (REVIEW_BY) 
        references CF_USER;

    alter table CF_CONSUMER_PRODUCT_REVIEW 
        add constraint FKj4ifxjiiwu134swgb15j3qwqp 
        foreign key (PRODUCT_CATEGORY_ID) 
        references CF_CONSUMER_PRODUCT;

    alter table CF_CONTACT 
        add constraint FK5yuqebimner8y77v5t06e8kar 
        foreign key (COUNTRY_ID) 
        references CF_COUNTRY;

    alter table CF_CONTACT 
        add constraint FKcocvub8vxw6uk235kjnjkyn5h 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_CONTACT 
        add constraint FKbtkpbr9gf62096ak8wmneyd4x 
        foreign key (GROUP_ID) 
        references CF_GROUP;

    alter table CF_CONTACT 
        add constraint FKqu95wddsw6aph01r3gd2gion7 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_CONTACT 
        add constraint FKkguhvil8gpyibvr80sdacgdtx 
        foreign key (USER_ID) 
        references CF_USER;

    alter table CF_COUNTRY 
        add constraint FKkvfbkpwhk43cs9dlv1npx11pu 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_COUNTRY 
        add constraint FKk29dbhsce86js9yr7q8yv8t71 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_CREDIT_CARD 
        add constraint FKkltduiayon9svdudfh7qa48id 
        foreign key (CC_EXP_MONTH_ID) 
        references CF_EXPIRY_MONTH;

    alter table CF_CREDIT_CARD 
        add constraint FKmpo4fr8vlya06u121v41mk5t5 
        foreign key (CC_EXP_YEAR_ID) 
        references CF_EXPIRY_YEAR;

    alter table CF_CREDIT_CARD 
        add constraint FKbcdjqxpfqjcgdivxwtf6vxc0x 
        foreign key (CC_PAYMENT_CARD_ID) 
        references CF_PAYMENT_CARD;

    alter table CF_CREDIT_CARD 
        add constraint FKk7oftbv397ikjg38uukyt6x3o 
        foreign key (CREDIT_CARD_ID) 
        references CF_BILLING_DETAILS;

    alter table CF_CUSTOMER_CONCERN 
        add constraint FK65morge5bamv3jcpevbusj7pg 
        foreign key (USER_ID) 
        references CF_USER;

    alter table CF_DELIVERY 
        add constraint FKrwe7dbd0ner34bkye7c7adpt1 
        foreign key (COUNTRY_ID) 
        references CF_COUNTRY;

    alter table CF_DELIVERY 
        add constraint FK2c4e7rbjxr804utahph58xfg3 
        foreign key (STATE_ID) 
        references CF_STATE;

    alter table CF_DELIVERY 
        add constraint FKtm5as43e9ns2vq6oy2xrpvjsx 
        foreign key (DRIVER_ID) 
        references CF_USER;

    alter table CF_DELIVERY 
        add constraint FKr6sqj2f8w29vuooqrx2n0s20l 
        foreign key (TRANSPORT_ID) 
        references CF_TRANSPORT;

    alter table CF_DEMAND 
        add constraint FK3q3sn2hsyo0rq2fwnma9jl3y9 
        foreign key (ASSIGNED_TO) 
        references CF_USER;

    alter table CF_DEMAND 
        add constraint FK25jxwyss15sxii0vw0e53491e 
        foreign key (CATEGORY_ID) 
        references CF_CATEGORY;

    alter table CF_DEMAND 
        add constraint FKo7unbyifwxkq24yea4rhtn2ho 
        foreign key (COMPANY_ID) 
        references CF_COMPANY;

    alter table CF_DEMAND 
        add constraint FKewygydt5mbv0rwb482w9xdj8y 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_DEMAND 
        add constraint FK65n4tkuhca0m854r5m6uc4yx4 
        foreign key (CURRENCY_ID) 
        references CF_CURRENCY;

    alter table CF_DEMAND 
        add constraint FK4mp8jvxmiwfq4ruhmfm40o2v4 
        foreign key (DEFAULT_IMAGE_ID) 
        references CF_IMAGE;

    alter table CF_DEMAND 
        add constraint FKank8aw051v17aws638jej8hy9 
        foreign key (COUNTRY_ID) 
        references CF_COUNTRY;

    alter table CF_DEMAND 
        add constraint FKavb91408ubpdo232xd4paix1q 
        foreign key (STATE_ID) 
        references CF_STATE;

    alter table CF_DEMAND 
        add constraint FK6eamralj2wb8jc2bddu1b8rsf 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_DEMAND 
        add constraint FKgny32c0j2ggrda02i8wi65obw 
        foreign key (QUNIT_ID) 
        references CF_UNIT;

    alter table CF_DEMAND 
        add constraint FKk61o8n8trd8lr0heyrkkmyt2d 
        foreign key (STATUS_ID) 
        references CF_DEMAND_STATUS;

    alter table CF_DEMAND 
        add constraint FKm0vuig56mcsfj767ibspag83h 
        foreign key (PUNIT_ID) 
        references CF_UNIT;

    alter table CF_DEMAND 
        add constraint FK2qn63o3w6hotygkt3ermat2w3 
        foreign key (USER_ID) 
        references CF_USER;

    alter table CF_DEMAND_HISTORY 
        add constraint FK4diwltiq94qyyh1l0ybekg57p 
        foreign key (COMPANY_ID) 
        references CF_COMPANY;

    alter table CF_DEMAND_HISTORY 
        add constraint FKo8tmgvivt5ju89ueescly78e8 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_DEMAND_HISTORY 
        add constraint FKjyu2dfj6jx3h764sgr0fa64ip 
        foreign key (COUNTRY_ID) 
        references CF_COUNTRY;

    alter table CF_DEMAND_HISTORY 
        add constraint FK1e56e6d4wiggob1jfejyg4h7x 
        foreign key (STATE_ID) 
        references CF_STATE;

    alter table CF_DEMAND_HISTORY 
        add constraint FKaupte5k8gr5x7pwsi1qjkaj8v 
        foreign key (DEMAND_ID) 
        references CF_DEMAND;

    alter table CF_DEMAND_HISTORY 
        add constraint FKawooojr6jc1iu75sahxp89cvr 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_DEMAND_HISTORY 
        add constraint FK2b3nkroileqmnp7g1e03s98q0 
        foreign key (CURRENCY_ID) 
        references CF_CURRENCY;

    alter table CF_DEMAND_HISTORY 
        add constraint FK6kd2ydewolios5oi7j7nirkhd 
        foreign key (PUNIT_ID) 
        references CF_UNIT;

    alter table CF_DEMAND_HISTORY 
        add constraint FKcej4isxxga9j8vw2mlqtgooud 
        foreign key (QUNIT_ID) 
        references CF_UNIT;

    alter table CF_DEMAND_HISTORY 
        add constraint FK9noc9iamugnt78uemhs1f33gw 
        foreign key (STATUS_ID) 
        references CF_DEMAND_STATUS;

    alter table CF_DEMAND_HISTORY 
        add constraint FKlr525k7nb0fak8uks6vgxkh93 
        foreign key (USER_ID) 
        references CF_USER;

    alter table CF_DEMAND_TAG 
        add constraint FK9d4a019196ensgjco5g6sd46w 
        foreign key (TAG_ID) 
        references CF_TAG;

    alter table CF_DEMAND_TAG 
        add constraint FKjy6yjcgbfs6vjaumt57h6ddc5 
        foreign key (DEMAND_ID) 
        references CF_DEMAND;

    alter table CF_DEPARTMENT 
        add constraint FKh8qo9yojaadjseduf2thrca6l 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_DEPARTMENT 
        add constraint FKicn6ceeg6lrlnr2mj7vylj2cp 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_DESIGNATION 
        add constraint FKq3faee64nimmufd4jtoimpxwh 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_DESIGNATION 
        add constraint FKkiesfh76iu38yvahv9jsw2ov6 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_DISCUSSION 
        add constraint FKdbfxya7dgb92fwt00qmvhh6xc 
        foreign key (COMPANY_ID) 
        references CF_USER;

    alter table CF_DISCUSSION 
        add constraint FKs94q2c5yoqxli7v8m3c14v9lo 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_DISCUSSION 
        add constraint FKrn248vbioekl5vcpmeern53gp 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_DISCUSSION 
        add constraint FKd53cs8te7ib80tr9j6uilbd0w 
        foreign key (USER_ID) 
        references CF_USER;

    alter table CF_DISCUSSION_GROUP 
        add constraint FKmx4vhv2noupeqyo9onynyxa6c 
        foreign key (GROUP_ID) 
        references CF_GROUP;

    alter table CF_DISCUSSION_GROUP 
        add constraint FKa57x9nyeg8y9pisvs88la2im5 
        foreign key (DISCUSSION_ID) 
        references CF_DISCUSSION;

    alter table CF_DISCUSSION_TAG 
        add constraint FK7d6s24vvwp8k1ps3qvpgnqugx 
        foreign key (TAG_ID) 
        references CF_TAG;

    alter table CF_DISCUSSION_TAG 
        add constraint FK91abairdoa0toys601kqit7l1 
        foreign key (DISCUSSION_ID) 
        references CF_DISCUSSION;

    alter table CF_EXPERTISE 
        add constraint FK5vin3o1nj1t25509cvbtlp1c5 
        foreign key (COMPANY_ID) 
        references CF_COMPANY;

    alter table CF_EXPERTISE 
        add constraint FKqfb8ljyj9jy7j4l7qid6tw6sh 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_EXPERTISE 
        add constraint FKtou4vlke2v7v0qfcd8uqvl0cq 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_EXPERTISE 
        add constraint FKe2lbc1vgn5te325ohi5e1j9a4 
        foreign key (USER_ID) 
        references CF_USER;

    alter table CF_EXPERTISE_REVIEW 
        add constraint FKcp7bxop3yns8940c0ab4w54uq 
        foreign key (RATING_ID) 
        references CF_RATING;

    alter table CF_EXPERTISE_REVIEW 
        add constraint FK8o61trjbft2s5chtmw2em5h9e 
        foreign key (REVIEW_BY) 
        references CF_USER;

    alter table CF_EXPERTISE_REVIEW 
        add constraint FKmsiofi669m2s8oscwebj94p82 
        foreign key (EXPERTISE_ID) 
        references CF_EXPERTISE;

    alter table CF_FEEDBACK 
        add constraint FKplk8o6ic2llq432n6jdilfh8g 
        foreign key (EXECUTIVES_RATING) 
        references CF_RATING;

    alter table CF_FEEDBACK 
        add constraint FKmcfxx0cplx8krtori276i17pt 
        foreign key (SERVICES_RATING) 
        references CF_RATING;

    alter table CF_FEEDBACK 
        add constraint FKdq0fbo1nyb17u3abki4a839tv 
        foreign key (SITE_RATING) 
        references CF_RATING;

    alter table CF_FEEDBACK 
        add constraint FKett1l8h2yvmgfefh4dkf32uf0 
        foreign key (USER_ID) 
        references CF_USER;

    alter table CF_GRAIN_CART 
        add constraint FKo2o8yqbv4hqlrnl7sx6rgg9xl 
        foreign key (COMPANY_ID) 
        references CF_COMPANY;

    alter table CF_GRAIN_CART 
        add constraint FK39nv6y4e6rh7oa4jni80dh0bm 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_GRAIN_CART 
        add constraint FKqco8wlpfi21axwfg2f61hg8bw 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_GRAIN_CART 
        add constraint FKnsm4n6lqccyaq79qypuc0726t 
        foreign key (USER_ID) 
        references CF_USER;

    alter table CF_GROUP 
        add constraint FKg6ip7c59efvlcdoedxw3amar6 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_GROUP 
        add constraint FK1fpen9qq250jk92ndeo0apljy 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_GROUP_ROLE 
        add constraint FKmmt2nxtwa2r8grx7j9a6w2fwj 
        foreign key (ROLE_ID) 
        references CF_ROLE;

    alter table CF_GROUP_ROLE 
        add constraint FKph9p037a9ahyhdn0q48mkicah 
        foreign key (GROUP_ID) 
        references CF_GROUP;

    alter table CF_IMAGE 
        add constraint FKnbwqyx7gm15qfnkarb0abm990 
        foreign key (COMPANY_ID) 
        references CF_COMPANY;

    alter table CF_IMAGE 
        add constraint FKoinlhq80grqh7k7ha1bjp6hcc 
        foreign key (PRODUCT_ID) 
        references CF_PRODUCT;

    alter table CF_IMAGE 
        add constraint FK8v5andr5j2rdird0yjhx2w70c 
        foreign key (USER_ID) 
        references CF_USER;

    alter table CF_INDIVIDUAL_PLAN 
        add constraint FKec0ybia0mv4wp18xtta5dlden 
        foreign key (PLAN_ID) 
        references CF_PLAN;

    alter table CF_INDIVIDUAL_PLAN 
        add constraint FKav48kyfaawvfsc42ua6gcpvth 
        foreign key (USER_ID) 
        references CF_USER;

    alter table CF_ITEM 
        add constraint FKax1wgth4q25q99g34oak6rahk 
        foreign key (IMAGE_ID) 
        references CF_IMAGE;

    alter table CF_ITEM 
        add constraint FKn2udj9oa60xjcvycr5tuabfno 
        foreign key (PRICE_ID) 
        references CF_PRICE;

    alter table CF_ITEM 
        add constraint FKlkl7e8r6ubw6lfxlullccuxvw 
        foreign key (QUNIT_ID) 
        references CF_UNIT;

    alter table CF_ITEM 
        add constraint FK526hx2uak6itmqnkbvs12ah8l 
        foreign key (GRAIN_CART_ID) 
        references CF_GRAIN_CART;

    alter table CF_ITEM 
        add constraint FK57h9pvsq2exfgsrxadtelwod9 
        foreign key (ORDER_ID) 
        references CF_ORDER;

    alter table CF_LANGUAGE 
        add constraint FKfvgl31fv208vkkopuwp5s4xdk 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_LANGUAGE 
        add constraint FKdvhjs520p7y1g32cotmoep1gf 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_LICENSE 
        add constraint FK8d42emftt1knvy62n1w6x0t9o 
        foreign key (DRIVER_ID) 
        references CF_USER;

    alter table CF_NOTE 
        add constraint FK37r5frj7bqfw8hyhhhojtbu2y 
        foreign key (COMPANY_ID) 
        references CF_USER;

    alter table CF_NOTE 
        add constraint FKlu0af28dtuv4nnqlkhmc1yrh0 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_NOTE 
        add constraint FKqen0ggpvrkj7tmgpuifibnlkp 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_NOTE 
        add constraint FKtp9blk6tfabuqxiiwt4kvs2rw 
        foreign key (USER_ID) 
        references CF_USER;

    alter table CF_NOTIF_CATEGORY 
        add constraint FK7q5i33q3jbxi7gu2pe5obnk1h 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_NOTIF_CATEGORY 
        add constraint FK3i85mgfmr7h11b20s7q906qrk 
        foreign key (DEFAULT_IMAGE_ID) 
        references CF_IMAGE;

    alter table CF_NOTIF_CATEGORY 
        add constraint FKivgdocpcomo9uqroxjeetbbyh 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_NOTIFICATION 
        add constraint FK4lady2sfvwvxwx0myy2bdt215 
        foreign key (CATEGORY_ID) 
        references CF_NOTIF_CATEGORY;

    alter table CF_NOTIFICATION 
        add constraint FKb00d01vjebfcldjjqe48awk32 
        foreign key (COMPANY_ID) 
        references CF_COMPANY;

    alter table CF_NOTIFICATION 
        add constraint FKj78dqhrf28k20rlb5o606go2g 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_NOTIFICATION 
        add constraint FKgm3erfhb5v1xvje75fxwd90nq 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_NOTIFICATION 
        add constraint FKt46w0ml9bs9vbfnqnrsonri3b 
        foreign key (USER_ID) 
        references CF_USER;

    alter table CF_NOTIFICATION_COMPANY 
        add constraint FKfpcgbuioajghf6f0bfjth8sw 
        foreign key (COMPANY_ID) 
        references CF_COMPANY;

    alter table CF_NOTIFICATION_COMPANY 
        add constraint FKtq2ibnt6d998nr5m4lxde8xcn 
        foreign key (NOTIFICATION_ID) 
        references CF_NOTIFICATION;

    alter table CF_NOTIFICATION_GROUP 
        add constraint FKoh7o7h6osddj4y0qvbvtr53sf 
        foreign key (GROUP_ID) 
        references CF_GROUP;

    alter table CF_NOTIFICATION_GROUP 
        add constraint FK8bsbfngv3ygxbitjinrwcn7od 
        foreign key (NOTIFICATION_ID) 
        references CF_NOTIFICATION;

    alter table CF_NOTIFICATION_USER 
        add constraint FKfvjpq7q4fu3woepdy5u6w8ysb 
        foreign key (USER_ID) 
        references CF_USER;

    alter table CF_NOTIFICATION_USER 
        add constraint FKp48lugp0q4tse2qn0rv0yqpsr 
        foreign key (NOTIFICATION_ID) 
        references CF_NOTIFICATION;

    alter table CF_ORDER 
        add constraint FKdtnde052n1s5ksl9riqnncwwg 
        foreign key (ASSIGNED_TO) 
        references CF_USER;

    alter table CF_ORDER 
        add constraint FKalrn1lq37qno07tgeo262gmrk 
        foreign key (BILLING_ADDRESS_ID) 
        references CF_ADDRESS;

    alter table CF_ORDER 
        add constraint FKlyqhbrds4h1ob53l394wtbvbj 
        foreign key (COMPANY_ID) 
        references CF_COMPANY;

    alter table CF_ORDER 
        add constraint FKjgube1n6q3nn2hi5kx4pg42al 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_ORDER 
        add constraint FK5bdn0vnydnuhgkoh9alsumdmj 
        foreign key (DELIVER_BY) 
        references CF_USER;

    alter table CF_ORDER 
        add constraint FKcgqsafat5e4qf40tvwknqb63u 
        foreign key (FULFILLED_BY) 
        references CF_COMPANY;

    alter table CF_ORDER 
        add constraint FKfrw2g02a8w6dv63032iwjx9ed 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_ORDER 
        add constraint FK8xplo3sj3pdqyyy306fp0hmde 
        foreign key (SHIPPING_ADDRESS_ID) 
        references CF_ADDRESS;

    alter table CF_ORDER 
        add constraint FKftjsgu8w5djlgg2dyijacc403 
        foreign key (STATUS_ID) 
        references CF_ORDER_STATUS;

    alter table CF_ORDER 
        add constraint FKfcgm13yot8e48ttvurunmrl1x 
        foreign key (USER_ID) 
        references CF_USER;

    alter table CF_ORDER_HISTORY 
        add constraint FK39132px5uij8gmligbnutk064 
        foreign key (BILLING_ADDRESS_ID) 
        references CF_ADDRESS;

    alter table CF_ORDER_HISTORY 
        add constraint FKb0rydj3p46lq98f283mqknx7j 
        foreign key (BUYER_ID) 
        references CF_USER;

    alter table CF_ORDER_HISTORY 
        add constraint FKpmsf3i3sxx9mi0mfnl4diaq82 
        foreign key (COMPANY_ID) 
        references CF_COMPANY;

    alter table CF_ORDER_HISTORY 
        add constraint FKnfif2qtqcky90brw3wg7ma3xh 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_ORDER_HISTORY 
        add constraint FK2rmad850g85qbojs76u4lojti 
        foreign key (FULFILLED_BY) 
        references CF_COMPANY;

    alter table CF_ORDER_HISTORY 
        add constraint FKpche09kfle3gkty15roo8e434 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_ORDER_HISTORY 
        add constraint FK7egqbey0vmbdxyaw6eu97wt5q 
        foreign key (ORDER_ID) 
        references CF_ORDER;

    alter table CF_ORDER_HISTORY 
        add constraint FKavm8n8r16obw7mblmmgbeotel 
        foreign key (SHIPPING_ADDRESS_ID) 
        references CF_ADDRESS;

    alter table CF_ORDER_HISTORY 
        add constraint FKheap70svqks1p2ji89h3574kl 
        foreign key (STATUS_ID) 
        references CF_ORDER_STATUS;

    alter table CF_ORDER_PRODUCTS 
        add constraint FK483d7spyod5tvnb4t7319bet0 
        foreign key (PRODUCT_ID) 
        references CF_PRODUCT;

    alter table CF_ORDER_PRODUCTS 
        add constraint FKjxeun9ia5jnya1ucmcsn93xqa 
        foreign key (ORDER_ID) 
        references CF_ORDER;

    alter table CF_PAYMENT_CARD 
        add constraint FKe63s1r8b0gks455rraq43g1kc 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_PAYMENT_CARD 
        add constraint FKm2ncy91bc2bi8ljniwombaljp 
        foreign key (DEFAULT_IMAGE_ID) 
        references CF_IMAGE;

    alter table CF_PAYMENT_CARD 
        add constraint FK2auabuuwg0rn3vv2xu1iqhk8n 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_PAYMENT_METHOD 
        add constraint FKieacc2j6m3evpfxt8hqk3h22l 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_PAYMENT_METHOD 
        add constraint FKg2u5d815ra2fs34bsaro081wr 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_PAYMENT_METHOD 
        add constraint FKo9q660n39v5v48p6o05j260rf 
        foreign key (COMPANY_ID) 
        references CF_COMPANY;

    alter table CF_PERMIT 
        add constraint FKkq6rmxjg2j014fje4ubn2yew8 
        foreign key (TRANSPORT_ID) 
        references CF_TRANSPORT;

    alter table CF_PERSON 
        add constraint FKtjctt8nv16df3j51gooa3dxsg 
        foreign key (COUNTRY_ID) 
        references CF_COUNTRY;

    alter table CF_PERSON 
        add constraint FK32rnggv26xm71qy3idmfofg3w 
        foreign key (STATE_ID) 
        references CF_STATE;

    alter table CF_PRICE 
        add constraint FKg2ix3gkfrjbiheck20tx75ht9 
        foreign key (CURRENCY_ID) 
        references CF_CURRENCY;

    alter table CF_PRICE 
        add constraint FK2xy9vmt9g10rn8ca1llr60i07 
        foreign key (UNIT_ID) 
        references CF_UNIT;

    alter table CF_PRICE 
        add constraint FK8qpt5v1n59wrrpnahm073i3r2 
        foreign key (PRODUCT_ID) 
        references CF_PRODUCT;

    alter table CF_PRICE 
        add constraint FK89xjeucm9oqg2awa95duqjd4y 
        foreign key (CONSUMER_PRODUCT_ID) 
        references CF_CONSUMER_PRODUCT;

    alter table CF_PROD_ATTRS 
        add constraint FKbblxh4gof0ksrvjc0wt6vlfdn 
        foreign key (PRODUCT_ID) 
        references CF_PRODUCT;

    alter table CF_PRODUCT 
        add constraint FK8tac3ebx3by9diehuoqi7uame 
        foreign key (COUNTRY_ID) 
        references CF_COUNTRY;

    alter table CF_PRODUCT 
        add constraint FKoy6k4iv4scke2xq88bwhgifur 
        foreign key (STATE_ID) 
        references CF_STATE;

    alter table CF_PRODUCT 
        add constraint FK8kobyprm4j6ckmxklfgp0gid9 
        foreign key (ASSIGNED_TO) 
        references CF_USER;

    alter table CF_PRODUCT 
        add constraint FKpopaxe0dgmow095026lfyv8np 
        foreign key (BIDDING_CURRENCY_ID) 
        references CF_CURRENCY;

    alter table CF_PRODUCT 
        add constraint FKfhrjw9kjtoxpcewh3blqltcus 
        foreign key (BIDDING_UNIT_ID) 
        references CF_UNIT;

    alter table CF_PRODUCT 
        add constraint FKli42kgdwogxa2h376lijmxy28 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_PRODUCT 
        add constraint FK5nx2pghgoooh40vockv85vou4 
        foreign key (DEFAULT_IMAGE_ID) 
        references CF_IMAGE;

    alter table CF_PRODUCT 
        add constraint FKfkh4hlsodvl22tgwl21yoggvx 
        foreign key (HIGHEST_BID_ID) 
        references CF_BID;

    alter table CF_PRODUCT 
        add constraint FKklovmhicvb61r2ytwxjkr75sk 
        foreign key (LAST_PRICE_ID) 
        references CF_PRICE;

    alter table CF_PRODUCT 
        add constraint FKjixmm7246ykkho875nman2ouu 
        foreign key (LATEST_PRICE_ID) 
        references CF_PRICE;

    alter table CF_PRODUCT 
        add constraint FK619hxx0s8qsccep9davayqsvj 
        foreign key (MAX_PRICE_ID) 
        references CF_PRICE;

    alter table CF_PRODUCT 
        add constraint FKjdu4yo3butq5q2ldnetgrgg17 
        foreign key (MIN_PRICE_ID) 
        references CF_PRICE;

    alter table CF_PRODUCT 
        add constraint FKosdmiyylwe2mrqfp49w4yhull 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_PRODUCT 
        add constraint FK8b4ge97odon0eoqtemwi0tadw 
        foreign key (OWNER_ID) 
        references CF_USER;

    alter table CF_PRODUCT 
        add constraint FKfxsebynfp3n5k0jbcpj9era59 
        foreign key (QUNIT_ID) 
        references CF_UNIT;

    alter table CF_PRODUCT 
        add constraint FK1x60rb9u8egh34ptxvlastsx3 
        foreign key (STATUS_ID) 
        references CF_PRODUCT_STATUS;

    alter table CF_PRODUCT_CATEGORY 
        add constraint FKcmgag27joxsag2fxojp93dnrh 
        foreign key (CATEGORY_ID) 
        references CF_CATEGORY;

    alter table CF_PRODUCT_CATEGORY 
        add constraint FKcevcvxf8v0gq1bxrjtimtqt1r 
        foreign key (PRODUCT_ID) 
        references CF_PRODUCT;

    alter table CF_PRODUCT_EVENT 
        add constraint FK7trl11itbqei3fkmq5de93c33 
        foreign key (EVENT_BY) 
        references CF_USER;

    alter table CF_PRODUCT_EVENT 
        add constraint FKswwjfxqkpqqdm5aq33rwkg9g0 
        foreign key (PRODUCT_ID) 
        references CF_PRODUCT;

    alter table CF_PRODUCT_EVENT 
        add constraint FK8vj6kqoyr5f2vyrenug87irmc 
        foreign key (PRODUCT_STATUS_ID) 
        references CF_PRODUCT_STATUS;

    alter table CF_PRODUCT_HISTORY 
        add constraint FK933kwfsm31qmw0yuqqllp7x8n 
        foreign key (CATEGORY_ID) 
        references CF_CATEGORY;

    alter table CF_PRODUCT_HISTORY 
        add constraint FKt027snihf8f49vmbldmlea13h 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_PRODUCT_HISTORY 
        add constraint FK5xmqje4dsgrnqdumdqtbunbyr 
        foreign key (DEFAULT_IMAGE_ID) 
        references CF_IMAGE;

    alter table CF_PRODUCT_HISTORY 
        add constraint FKpo918vswlmm5ok2f5nlayw0ll 
        foreign key (LAST_PRICE_ID) 
        references CF_PRICE;

    alter table CF_PRODUCT_HISTORY 
        add constraint FKrnkvgeigp725hdh5vfw5hpipr 
        foreign key (LATEST_PRICE_ID) 
        references CF_PRICE;

    alter table CF_PRODUCT_HISTORY 
        add constraint FKr68dxhr4o66j27y3t6rpwmjs0 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_PRODUCT_HISTORY 
        add constraint FK7hb1n0u83mxyrrxw6f59h6o7t 
        foreign key (OWNER_ID) 
        references CF_USER;

    alter table CF_PRODUCT_HISTORY 
        add constraint FKlvu2tp1heknn3nl7145b5a1x2 
        foreign key (PRODUCT_ID) 
        references CF_PRODUCT;

    alter table CF_PRODUCT_HISTORY 
        add constraint FKjxws602xyvm96qnh7nbwfoxui 
        foreign key (QUNIT_ID) 
        references CF_UNIT;

    alter table CF_PRODUCT_HISTORY 
        add constraint FK3c4o0k32f0n1h6d9p67x76hhu 
        foreign key (STATUS_ID) 
        references CF_PRODUCT_STATUS;

    alter table CF_PRODUCT_REVIEW 
        add constraint FK54qth6n8ukpucilteyq8xjle3 
        foreign key (RATING_ID) 
        references CF_RATING;

    alter table CF_PRODUCT_REVIEW 
        add constraint FKl389hcl4te9avom5bckrn70uj 
        foreign key (REVIEW_BY) 
        references CF_USER;

    alter table CF_PRODUCT_REVIEW 
        add constraint FKalp1l16pebug52o0bhnli0egh 
        foreign key (PRODUCT_ID) 
        references CF_PRODUCT;

    alter table CF_PRODUCT_TAG 
        add constraint FKmipy8pmh8jqh5a1eafx71wayt 
        foreign key (TAG_ID) 
        references CF_TAG;

    alter table CF_PRODUCT_TAG 
        add constraint FKf383hrn3cl57sxf0qvxgo94r 
        foreign key (PRODUCT_ID) 
        references CF_PRODUCT;

    alter table CF_ROLE 
        add constraint FK87uf02buvsc4q3d93marx1irf 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_ROLE 
        add constraint FKhgjkofxn292ir9orui7qfhk3g 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_SELLER_WISHLIST 
        add constraint FKkpdh7jnotqpcbvpegh908jlv9 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_SELLER_WISHLIST 
        add constraint FK4wy4ccgj9tw5457jhmrw3osxc 
        foreign key (DEMAND_ID) 
        references CF_DEMAND;

    alter table CF_SELLER_WISHLIST 
        add constraint FKx9spufp4qibxmomiptcg05i4 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_SELLER_WISHLIST 
        add constraint FKc5slo3ela7xnu5fkam4nhsyk3 
        foreign key (SELLER_ID) 
        references CF_USER;

    alter table CF_SOCIAL_ACCOUNTS 
        add constraint FK576dbwlummkygi6hk2t1uit6d 
        foreign key (COMPANY_ID) 
        references CF_COMPANY;

    alter table CF_SOCIAL_ACCOUNTS 
        add constraint FKsaftv3prw839e2slici4dm7mw 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_SOCIAL_ACCOUNTS 
        add constraint FKm051q21gs7w2qq89ch6ugk7x0 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_SOCIAL_ACCOUNTS 
        add constraint FK5nudb0clwfh7bjjllybf6h3t5 
        foreign key (USER_ID) 
        references CF_USER;

    alter table CF_STATE 
        add constraint FKny5wrwou53y5q5o8fv11xd8q7 
        foreign key (COUNTRY_ID) 
        references CF_COUNTRY;

    alter table CF_STATE 
        add constraint FKoos8oc7rsg2tkljo77ihrin63 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_STATE 
        add constraint FK5dcqiu2jqx7m4s526h27srpqe 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_TAG 
        add constraint FKmo1rflkyfqiida6tq17h6mt5u 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_TAG 
        add constraint FKt3aewylvq5tje93br2cbhmmed 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_UPCOMING_EVENT 
        add constraint FKl2kul7k00nijwsm2do9cyloqu 
        foreign key (COMPANY_ID) 
        references CF_COMPANY;

    alter table CF_UPCOMING_EVENT 
        add constraint FKg2uykskk2cswdk1svvw89wicb 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_UPCOMING_EVENT 
        add constraint FK1a98t2rhdk96v7lknsakbqwr5 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_UPCOMING_EVENT 
        add constraint FKbwqrybha7odhd94ekq46po4y3 
        foreign key (USER_ID) 
        references CF_USER;

    alter table CF_UPCOMING_EVENT_COMPANY 
        add constraint FKd53o3deo48k69l7oar4hqqkjq 
        foreign key (COMPANY_ID) 
        references CF_COMPANY;

    alter table CF_UPCOMING_EVENT_COMPANY 
        add constraint FKt6660twpb7h3e3pj8r1bpf87v 
        foreign key (UPCOMING_EVENT_ID) 
        references CF_UPCOMING_EVENT;

    alter table CF_UPCOMING_EVENT_GROUP 
        add constraint FK2k0985c53ktkwenr9rdd67g9f 
        foreign key (GROUP_ID) 
        references CF_GROUP;

    alter table CF_UPCOMING_EVENT_GROUP 
        add constraint FK29s3mux104371a0a37mf2a4ux 
        foreign key (UPCOMING_EVENT_ID) 
        references CF_UPCOMING_EVENT;

    alter table CF_UPCOMING_EVENT_USER 
        add constraint FKr3aerg7nmqfc9n9turaxlbet5 
        foreign key (USER_ID) 
        references CF_USER;

    alter table CF_UPCOMING_EVENT_USER 
        add constraint FKsnmqdyvqw5pkb1a61cotg6qcq 
        foreign key (UPCOMING_EVENT_ID) 
        references CF_UPCOMING_EVENT;

    alter table CF_USER 
        add constraint FKe29s49vn0konhgnxit2604wi 
        foreign key (COUNTRY_ID) 
        references CF_COUNTRY;

    alter table CF_USER 
        add constraint FKt5si4rej96y97netlqay3cpo4 
        foreign key (STATE_ID) 
        references CF_STATE;

    alter table CF_USER 
        add constraint FKbbjk1ehefb50pb0epgqufesxf 
        foreign key (COMPANY_ID) 
        references CF_COMPANY;

    alter table CF_USER 
        add constraint FKsvp5xt98598375k3qtvtyye15 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_USER 
        add constraint FKifmpsgttpa1xjlkv8x1ov4yii 
        foreign key (DEFAULT_BILLING_ADDRESS_ID) 
        references CF_ADDRESS;

    alter table CF_USER 
        add constraint FKec94y7e29ulboujg7ne0atnov 
        foreign key (DEFAULT_BILLING_DETAILS_ID) 
        references CF_BILLING_DETAILS;

    alter table CF_USER 
        add constraint FKcfhmclyrlr0762pl5c6cp0qd5 
        foreign key (DEFAULT_COUNTRY_ID) 
        references CF_COUNTRY;

    alter table CF_USER 
        add constraint FKiof1y5js5f5hnppure4n6yb52 
        foreign key (DEFAULT_DELIVERY_ADDRESS_ID) 
        references CF_ADDRESS;

    alter table CF_USER 
        add constraint FKdmnl1vui8i9ktwvjwx60bj7dl 
        foreign key (DEFAULT_IMAGE_ID) 
        references CF_IMAGE;

    alter table CF_USER 
        add constraint FKg205ta5ui3nwhveu8fjbbyenl 
        foreign key (DEFAULT_LANGUAGE_ID) 
        references CF_LANGUAGE;

    alter table CF_USER 
        add constraint FKt9hq56lioaug9bw0bmrf23dcs 
        foreign key (DEPARTMENT_ID) 
        references CF_DEPARTMENT;

    alter table CF_USER 
        add constraint FK9hl9byrq7v6n61njbf9e2gxo4 
        foreign key (DESIGNATION_ID) 
        references CF_DESIGNATION;

    alter table CF_USER 
        add constraint FKhpjpbonhky7x31x0dxh90s30l 
        foreign key (GROUP_ID) 
        references CF_GROUP;

    alter table CF_USER 
        add constraint FKgk6qq8y872adjj3hi9s7k4ncm 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_USER_PLAN 
        add constraint FKemi9r8bjd7w9xapm49mfmpfxn 
        foreign key (PLAN_ID) 
        references CF_PLAN;

    alter table CF_USER_PLAN 
        add constraint FKoyndoitnwq8uqd3lfjim6rk2c 
        foreign key (USER_ID) 
        references CF_USER;

    alter table CF_USER_REVIEW 
        add constraint FKfhcfofr50n7vyg1omj1km2mb3 
        foreign key (RATING_ID) 
        references CF_RATING;

    alter table CF_USER_REVIEW 
        add constraint FK5uhxkpbmf94oc28xoykcf6ai7 
        foreign key (REVIEW_BY) 
        references CF_USER;

    alter table CF_USER_REVIEW 
        add constraint FK88542uh8bglde4eehgrqchk1r 
        foreign key (USER_ID) 
        references CF_USER;

    alter table CF_WISHLIST 
        add constraint FK2e0i5wg05ew3935pfe9t9pl5m 
        foreign key (COMPANY_ID) 
        references CF_COMPANY;

    alter table CF_WISHLIST 
        add constraint FKt2awj3sbs5leuscc3qt6qyc2m 
        foreign key (CREATED_BY) 
        references CF_USER;

    alter table CF_WISHLIST 
        add constraint FKakf2c61wdd1uqmkblcxtige72 
        foreign key (MODIFIED_BY) 
        references CF_USER;

    alter table CF_WISHLIST 
        add constraint FKntli0csrnjx0yr4tar8h3le7m 
        foreign key (USER_ID) 
        references CF_USER;

    alter table CF_WISHLIST_DEMAND 
        add constraint FKspc98rivdk32rjae78mfu2us7 
        foreign key (DEMAND_ID) 
        references CF_DEMAND;

    alter table CF_WISHLIST_DEMAND 
        add constraint FK26vvn5n2nvu030b02oqtmonfg 
        foreign key (WISHLIST_ID) 
        references CF_WISHLIST;

    alter table CF_WISHLIST_PRODUCT 
        add constraint FK5o8exhbp4fpnxk4ansphleqmk 
        foreign key (PRODUCT_ID) 
        references CF_PRODUCT;

    alter table CF_WISHLIST_PRODUCT 
        add constraint FKck5mrt8bow6c2uw174gxjmwld 
        foreign key (WISHLIST_ID) 
        references CF_WISHLIST;
