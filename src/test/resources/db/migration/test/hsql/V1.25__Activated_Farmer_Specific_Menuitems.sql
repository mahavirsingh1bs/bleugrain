update cf_menuitem set is_active = true where id in (71, 72, 73, 76);
update cf_menuitem set is_active = false where id in (80);
update cf_menuitem set is_active = false where id in (2);
update cf_menuitem set name = 'Fruits & Dry Fruits' where id in (3);
update cf_menuitem set display_index = 1 where id = 1;
update cf_menuitem set display_index = 2 where id = 3;
update cf_menuitem set display_index = 3 where id = 4;
update cf_menuitem set display_index = 4 where id = 5;
update cf_menuitem set display_index = 5 where id = 6;
update cf_menuitem set display_index = 6 where id = 71;
update cf_menuitem set display_index = 7 where id = 72;
update cf_menuitem set display_index = 8 where id = 73;
update cf_menuitem set display_index = 9 where id = 76;
update cf_menuitem set name = 'Fruits' where id = 3;
update cf_menuitem set is_active = true where id = 2;

INSERT INTO CF_CATEGORY(id, is_archived, created_on, modified_on, short_desc, name, unique_id, created_by, default_image_id, modified_by, parent_category_id) 
	VALUES 
	(81,FALSE,'2015-02-15 00:00:00','2015-02-22 00:00:00','','Fruits & Dry Fruits','555e4a79-205d-db23-5f45-e1f18c341bc4',1,214,1,NULL);

INSERT INTO CF_MENUITEM(ID, OBJ_VERSION, UNIQUE_ID, NAME, KEYWORDS, CATEGORY_ID, PARENT_MENUITEM_ID, DISPLAY_INDEX, IS_ACTIVE, IS_ARCHIVED, CREATED_BY, CREATED_ON, MODIFIED_BY, MODIFIED_ON)
	VALUES
	(81, 0, 'f46c8517-707a-85db-8f1f-06e2877ac3e6', 'Fruits & Dry Fruits', NULL, 81, NULL, 2, TRUE, FALSE, 1, '2016-07-25 00:00:00', NULL, NULL);

update cf_menuitem set parent_menuitem_id = 81 where id in (2, 3);
update cf_menuitem set is_active = false where parent_menuitem_id in (240, 241);

ALTER TABLE CF_CATEGORY ADD COLUMN IS_ACTIVE boolean DEFAULT TRUE;