package com.carefarm.prakrati.search.helper;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.search.document.BankDoc;
import com.carefarm.prakrati.search.document.DemandDoc;
import com.carefarm.prakrati.search.document.ProductDoc;
import com.carefarm.prakrati.search.document.Shared.Quantity;
import com.carefarm.prakrati.search.document.Shared.Unit;

public class SearchHelper {

	public static BankDoc createBankDoc(long id, String name) {
		BankDoc bankDoc = new BankDoc();
		bankDoc.setId(id);
		bankDoc.setUniqueId(UUID.randomUUID().toString());
		bankDoc.setName(name);
		return bankDoc;
	}

	public static ProductDoc createProductDoc(long id, String name, double price, double quantity) {
		ProductDoc productDoc = new ProductDoc();
		productDoc.setId(id);
		productDoc.setName(name);
		productDoc.setLatestPrice(BigDecimal.valueOf(price));
		productDoc.setQuantity(new Quantity(BigDecimal.valueOf(quantity), new Unit("Quintal", "Q")));
		productDoc.setDateCreated(Date.from(Environment.clock().instant()));
		return productDoc;
	}

	public static DemandDoc createDemandDoc(long id, String name, double price, double quantity) {
		DemandDoc demandDoc = new DemandDoc();
		demandDoc.setId(id);
		demandDoc.setName(name);
		demandDoc.setPricePerUnit(BigDecimal.valueOf(price));
		demandDoc.setQuantity(new Quantity(BigDecimal.valueOf(quantity), new Unit("Quintal", "Q")));
		demandDoc.setDateCreated(Date.from(Environment.clock().instant()));
		return demandDoc;
	}

}
