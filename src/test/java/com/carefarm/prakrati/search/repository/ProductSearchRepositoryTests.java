package com.carefarm.prakrati.search.repository;

import static com.carefarm.prakrati.search.helper.SearchHelper.createProductDoc;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import com.carefarm.prakrati.config.SearchIntegrationTest;
import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.document.ProductDoc;
import com.carefarm.prakrati.search.dto.SearchCriteria;
import com.carefarm.prakrati.web.bean.FiltersBean;

public class ProductSearchRepositoryTests extends SearchIntegrationTest {

	@Autowired
	private ProductSearchRepository productSearchRepository;
	
	@Before
	public void setUp() throws IOException {
		createIndex(ProductDoc.class);
		productSearchRepository.save(createProductDoc(1L, "Kashmiri Rice", 3800.0, 380.0));
		productSearchRepository.save(createProductDoc(2L, "Sharbati Rice", 2800.0, 340.0));
	}
	
	@Test
	public void testFindByName() {
		Page<ProductDoc> products = productSearchRepository.findByName("Rice", new PageRequest(0, 10));
		assertThat(products.getNumberOfElements(), is(2));
	}
	
	@Test
	public void testFindByMatchingKeywords() {
		SearchCriteria criteria = new SearchCriteria();
		criteria.setCnsmrPrdt(false);
		criteria.setKeywords("rice");
		criteria.setFilters(new FiltersBean());
		SearchResult<Long> searchResult = productSearchRepository.findByKeywordContains(criteria);
		Assert.assertThat("TotalHits count should be 2", searchResult.getTotalHits(), is(2L));
	}
	
}
