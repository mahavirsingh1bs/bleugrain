package com.carefarm.prakrati.search.repository;

import static com.carefarm.prakrati.search.helper.SearchHelper.createBankDoc;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.test.annotation.Rollback;

import com.carefarm.prakrati.config.SearchIntegrationTest;
import com.carefarm.prakrati.search.document.BankDoc;

public class BankSearchRepositoryTests extends SearchIntegrationTest {

	@Autowired
	private BankSearchRepository bankSearchRepository;
	
	@Autowired
	private ElasticsearchTemplate elasticsearchTemplate;
	
	@Before
	public void setUp() throws IOException {
		createIndex(BankDoc.class);
		bankSearchRepository.save(createBankDoc(1L, "ICICI BANK"));
		bankSearchRepository.save(createBankDoc(2L, "HDFC BANK"));
	}
	
	@Test
	@Rollback(false)
	public void testSave() {
		
		Page<BankDoc> banks = bankSearchRepository.findByName("ICICI", new PageRequest(0, 10));
		assertThat(banks.getNumberOfElements(), is(1));
	}
	
}
