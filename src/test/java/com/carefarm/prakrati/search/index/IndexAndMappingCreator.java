package com.carefarm.prakrati.search.index;

import java.io.IOException;
import java.nio.file.Files;

import org.apache.commons.lang.StringUtils;
import org.elasticsearch.action.admin.indices.create.CreateIndexRequestBuilder;
import org.elasticsearch.action.admin.indices.create.CreateIndexResponse;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.IndicesAdminClient;
import org.elasticsearch.common.settings.Settings;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourcePatternResolver;

import com.carefarm.prakrati.config.PrakratiIntegrationTest;

public class IndexAndMappingCreator extends PrakratiIntegrationTest {
	
	private static final Logger LOGGER = LoggerFactory.getLogger(IndexAndMappingCreator.class);
	
	@Autowired
	private Client client;
	
	@Autowired
	private ResourcePatternResolver resourceResolver;
	
	@Test
	public void addIndexesAndTypes() throws IOException {
		IndicesAdminClient indicesAdminClient = client.admin().indices();
		DeleteIndexResponse deleteResponse = indicesAdminClient.prepareDelete("bleugrain").get();
		if (deleteResponse.isAcknowledged()) {
			LOGGER.info("Bleugrain index has been deleted successfully.");
		}
		
		CreateIndexRequestBuilder indexBuilder = indicesAdminClient.prepareCreate("bleugrain")
			.setSettings(Settings.builder()
				.put("index.number_of_shards", 3)
                .put("index.number_of_replicas", 2)
	         );
		
		Resource[] mappings = resourceResolver.getResources("classpath:elasticsearch/*.mapping");
		for (Resource mapping: mappings) {
			String type = StringUtils.split(mapping.getFilename(), ".")[0];
			indexBuilder.addMapping(type, new String(Files.readAllBytes(mapping.getFile().toPath())));
		}
		CreateIndexResponse createResponse = indexBuilder.get();
		
		if (createResponse.isAcknowledged()) {
			LOGGER.info("Bleugrain Index and types has been created successfully.");
		}
	}
	
}
