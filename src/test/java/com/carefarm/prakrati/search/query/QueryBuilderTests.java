package com.carefarm.prakrati.search.query;

import static org.elasticsearch.search.aggregations.AggregationBuilders.nested;

import org.elasticsearch.search.aggregations.bucket.nested.NestedBuilder;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.elasticsearch.search.aggregations.AggregationBuilders;

public class QueryBuilderTests {

	public static void main(String...args) {
		NestedBuilder builder = new NestedBuilder("categories");
		builder.path("categories");
		// builder.subAggregation()
		SearchQuery searchQuery = new NativeSearchQueryBuilder()
			.addAggregation(nested("categories").path("categories").subAggregation(AggregationBuilders.terms("name").field("categories.name")))
			.build();
		System.out.println(nested("categories").path("categories").subAggregation(AggregationBuilders.terms("name").field("categories.name")));
		System.out.println(searchQuery);
	}
	
}
