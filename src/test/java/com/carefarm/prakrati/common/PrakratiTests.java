package com.carefarm.prakrati.common;

import static org.mockito.Mockito.mock;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.dozer.DozerBeanMapper;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.util.ReflectionUtils;

import com.carefarm.prakrati.common.entity.Country;
import com.carefarm.prakrati.common.entity.Currency;
import com.carefarm.prakrati.common.entity.ProductStatus;
import com.carefarm.prakrati.common.entity.Rating;
import com.carefarm.prakrati.common.entity.State;
import com.carefarm.prakrati.common.entity.Unit;
import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.entity.Address;
import com.carefarm.prakrati.entity.AddressEntity;
import com.carefarm.prakrati.entity.Bid;
import com.carefarm.prakrati.entity.Category;
import com.carefarm.prakrati.entity.Company;
import com.carefarm.prakrati.entity.Demand;
import com.carefarm.prakrati.entity.GrainCart;
import com.carefarm.prakrati.entity.Group;
import com.carefarm.prakrati.entity.Image;
import com.carefarm.prakrati.entity.Item;
import com.carefarm.prakrati.entity.Language;
import com.carefarm.prakrati.entity.Order;
import com.carefarm.prakrati.entity.Price;
import com.carefarm.prakrati.entity.Product;
import com.carefarm.prakrati.entity.Quantity;
import com.carefarm.prakrati.entity.Review;
import com.carefarm.prakrati.entity.Role;
import com.carefarm.prakrati.entity.Tag;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.factory.Factories;
import com.carefarm.prakrati.generators.UserNoGenerator;
import com.carefarm.prakrati.messaging.AccountEmailHandler;
import com.carefarm.prakrati.payment.repository.BankAccountRepository;
import com.carefarm.prakrati.payment.repository.CreditCardRepository;
import com.carefarm.prakrati.repository.ActivityRepository;
import com.carefarm.prakrati.repository.AddressRepository;
import com.carefarm.prakrati.repository.BidRepository;
import com.carefarm.prakrati.repository.CategoryRepository;
import com.carefarm.prakrati.repository.CompanyRepository;
import com.carefarm.prakrati.repository.CountryRepository;
import com.carefarm.prakrati.repository.DemandRepository;
import com.carefarm.prakrati.repository.DemandStatusRepository;
import com.carefarm.prakrati.repository.GrainCartRepository;
import com.carefarm.prakrati.repository.GroupRepository;
import com.carefarm.prakrati.repository.ImageRepository;
import com.carefarm.prakrati.repository.NoteRepository;
import com.carefarm.prakrati.repository.OrderRepository;
import com.carefarm.prakrati.repository.OrderStatusRepository;
import com.carefarm.prakrati.repository.ProductRepository;
import com.carefarm.prakrati.repository.ProductStatusRepository;
import com.carefarm.prakrati.repository.RatingRepository;
import com.carefarm.prakrati.repository.StateRepository;
import com.carefarm.prakrati.repository.TagRepository;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.repository.WishlistRepository;
import com.carefarm.prakrati.service.helper.AddressEntityHelper;
import com.carefarm.prakrati.service.helper.AmazonS3Helper;
import com.carefarm.prakrati.service.helper.BidHelper;
import com.carefarm.prakrati.service.helper.DemandHelper;
import com.carefarm.prakrati.service.helper.ItemHelper;
import com.carefarm.prakrati.service.helper.OrderHelper;
import com.carefarm.prakrati.service.helper.ProductEventHelper;
import com.carefarm.prakrati.service.helper.ProductHelper;
import com.carefarm.prakrati.service.helper.UserHelper;
import com.carefarm.prakrati.util.ObjectFactory;
import com.carefarm.prakrati.web.bean.AddressBean;
import com.carefarm.prakrati.web.bean.BankAccountBean;
import com.carefarm.prakrati.web.bean.BankBean;
import com.carefarm.prakrati.web.bean.CountryBean;
import com.carefarm.prakrati.web.bean.StateBean;

import edu.emory.mathcs.backport.java.util.Arrays;

public abstract class PrakratiTests {
	
	protected CategoryRepository categoryRepository;
	
	protected UserRepository userRepository;
	
	protected GroupRepository groupRepository;
	
	protected CompanyRepository companyRepository;
	
	protected ProductRepository productRepository;
	
	protected DemandRepository demandRepository;

	protected TagRepository tagRepository;
	
	protected ProductStatusRepository productStatusRepository;
	
	protected DemandStatusRepository demandStatusRepository;
	
	protected OrderStatusRepository orderStatusRepository;
	
	protected BidRepository bidRepository;
	
	protected RatingRepository ratingRepository;
	
	protected WishlistRepository wishlistRepository;
	
	protected GrainCartRepository grainCartRepository;
	
	protected ActivityRepository activityRepository;

	protected NoteRepository noteRepository;
	
	protected BankAccountRepository bankAccountRepository;
	
	protected CreditCardRepository creditCardRepository;
	
	protected OrderRepository orderRepository;
	
	protected CountryRepository countryRepository;
	
	protected StateRepository stateRepository;
	
	protected ImageRepository imageRepository;
	
	protected AddressRepository addressRepository;
	
	protected UserHelper userHelper;
	
	protected ProductHelper productHelper;
	
	protected DemandHelper demandHelper;
	
	protected OrderHelper orderHelper;
	
	protected ItemHelper itemHelper;
	
	protected BidHelper bidHelper;

	protected ProductEventHelper productEventHelper;
	
	protected AddressEntityHelper addressEntityHelper;
	
	protected AmazonS3Helper amazonS3Helper;

	protected DozerBeanMapper mapper;
	
	protected AccountEmailHandler emailHandler;
	
	protected ApplicationEventPublisher eventPublisher;
	
	protected void setUp() {
		categoryRepository = mock(CategoryRepository.class);
		userRepository = mock(UserRepository.class);
		groupRepository = mock(GroupRepository.class);
		companyRepository = mock(CompanyRepository.class);
		
		productRepository = mock(ProductRepository.class);
		demandRepository = mock(DemandRepository.class);
		tagRepository = mock(TagRepository.class);
		
		productStatusRepository = mock(ProductStatusRepository.class);
		demandStatusRepository = mock(DemandStatusRepository.class);
		orderStatusRepository = mock(OrderStatusRepository.class);
		bidRepository = mock(BidRepository.class);
		ratingRepository = mock(RatingRepository.class);
		
		wishlistRepository = mock(WishlistRepository.class);
		grainCartRepository = mock(GrainCartRepository.class);
		activityRepository = mock(ActivityRepository.class);
		noteRepository = mock(NoteRepository.class);
		
		bankAccountRepository = mock(BankAccountRepository.class);
		creditCardRepository = mock(CreditCardRepository.class);
		orderRepository = mock(OrderRepository.class);
		stateRepository = mock(StateRepository.class);
		countryRepository = mock(CountryRepository.class);
		imageRepository = mock(ImageRepository.class);
		addressRepository = mock(AddressRepository.class);
		
		userHelper = mock(UserHelper.class);
		productHelper = mock(ProductHelper.class);
		demandHelper = mock(DemandHelper.class);
		orderHelper = mock(OrderHelper.class);
		itemHelper = mock(ItemHelper.class);
		bidHelper = mock(BidHelper.class);
		
		productEventHelper = mock(ProductEventHelper.class);
		addressEntityHelper = mock(AddressEntityHelper.class);
		amazonS3Helper = mock(AmazonS3Helper.class);
		
		mapper = ObjectFactory.dozerBeanMapper();
		
		emailHandler = mock(AccountEmailHandler.class);
		eventPublisher = mock(ApplicationEventPublisher.class);
	}
	
	protected void setField(String fieldName, Object target, Object value) {
		Field field = ReflectionUtils.findField(target.getClass(), fieldName);
		ReflectionUtils.makeAccessible(field);
		ReflectionUtils.setField(field, target, value);
	}

	protected List<Product> mockedProducts() {
		List<Product> mockedProducts = new ArrayList<>();
		mockedProducts.add(mockedProduct());
		return mockedProducts;
	}
	
	protected List<Product> mockedNoProducts() {
		List<Product> mockedProducts = new ArrayList<>();
		return mockedProducts;
	}
	
	protected List<Product> mockedProducts(Product...products) {
		List<Product> mockedProducts = new ArrayList<>();
		for (Product product: products)
			mockedProducts.add(product);
		return mockedProducts;
	}
	
	protected List<Product> mockedProducts(User user) {
		List<Product> mockedProducts = new ArrayList<>();
		mockedProducts.add(mockedProduct(user));
		return mockedProducts;
	}
	
	protected Product mockedProduct(User user) {
		Product product = mockedProduct();
		product.clearBids();
		product.addBid(mockedBid(3400.0, user));
		product.addBid(mockedBid(3450.0));
		product.addBid(mockedBid(3500.0, user));
		return product;
	}
	
	protected List<Product> mockedProducts(Company company) {
		List<Product> mockedProducts = new ArrayList<>();
		mockedProducts.add(mockedProduct(company));
		return mockedProducts;
	}
	
	protected Product mockedProduct() {
		Product product = new Product();
		product.setId(1L);
		product.setName("Paddy 1121");
		product.setQuantity(mockedQuantity(300.0));
		product.setLatestPrice(mockedPrice(3400.0));
		product.setStatus(mockedStatus("Newly Added"));
		User owner = mockedUser();
		owner.setGroup(mockedGroup("Farmer", "ROLE_USER", "ROLE_FARMER"));
		product.setOwner(owner);
		product.addReview(mockedReview());
		product.setDateCreated(Date.from(Environment.clock().instant()));
		product.addBid(mockedBid(3400.0));
		product.addBid(mockedBid(3450.0));
		product.addBid(mockedBid(3500.0));
		return product;
	}
	
	protected Item mockedItem() {
		Item mockedItem = new Item();
		mockedItem.setName("Basmati Rice");
		mockedItem.setPrice(new Price(200.0, new Currency("INR", "Rs."), new Unit("Kilogram", "Kg")));
		mockedItem.setQuantity(new Quantity(230.0, new Unit("Kilogram", "Kg")));
		return mockedItem;
	}
	
	protected Product mockedProduct(String uniqueId) {
		Product product = this.mockedProduct();
		product.setUniqueId(uniqueId);
		return product;
	}
	
	protected Product mockedProduct(String uniqueId, ProductStatus status) {
		Product product = this.mockedProduct();
		product.setUniqueId(uniqueId);
		product.setStatus(status);
		return product;
	}
	
	protected Product mockedProduct(Company company) {
		Product product = mockedProduct();
		product.clearBids();
		product.addBid(mockedBid(3400.0, company));
		product.addBid(mockedBid(3450.0));
		product.addBid(mockedBid(3500.0, company));
		return product;
	}
	
	protected List<Demand> mockedDemands() {
		List<Demand> mockedDemands = new ArrayList<>();
		mockedDemands.add(mockedDemand());
		return mockedDemands;
	}
	
	protected List<Demand> mockedNoDemands() {
		List<Demand> mockedDemands = new ArrayList<>();
		return mockedDemands;
	}
	
	protected Demand mockedDemand() {
		Demand mockedDemand = new Demand();
		mockedDemand.setName("Paddy 1121");
		mockedDemand.setCategory(mockedCategory());
		mockedDemand.setCompany(mockedCompany("Company Name", "84932898"));
		mockedDemand.setCurrency(mockedCurrency());
		mockedDemand.setQuantity(mockedQuantity(200.0));
		mockedDemand.addTag(new Tag("Cereals"));
		mockedDemand.setDateCreated(Date.from(Environment.clock().instant()));
		mockedDemand.setCreatedBy(mockedUser());
		return mockedDemand;
	}
	
	protected User currentUser(String emailAddress, Group group) {
		User currentUser = Factories.USER_FACTORY.create("Ryan", "Jenith", emailAddress, emailAddress, "43294932343", "+919717818596");
		currentUser.setGroup(group);
		currentUser.setUserNo(UserNoGenerator.generateUserNo());
		return currentUser;
	}
	
	protected User currentUser(String emailAddress, Group group, Company company) {
		User currentUser = Factories.USER_FACTORY.create("Ryan", "Jenith", emailAddress, emailAddress, "43294932343", "+919717818596");
		currentUser.setGroup(group);
		currentUser.setCompany(company);
		return currentUser;
	}
	
	protected Group mockedGroup(String name, String...roles) {
		Group group = new Group(name);
		for (String role : roles) {
			group.addRole(new Role(role));
		}
		return group;
	}
	
	protected List<Company> mockedCompanies() {
		List<Company> companies = new ArrayList<>();
		companies.add(mockedCompany("Company Name", "4932898"));
		return companies;
	}
	
	protected Company mockedCompany(String name, String registrNo) {
		Company company = new Company(name, registrNo);
		company.setId(1L);
		company.setUniqueId(UUID.randomUUID().toString());	
		return company;
	}
	
	protected Bid mockedBid(BigDecimal price) {
		Bid mockedBid = new Bid();
		mockedBid.setPrice(price);
		mockedBid.setCurrency(mockedCurrency());
		mockedBid.setUnit(mockedUnit());
		mockedBid.setUser(mockedUser());
		return mockedBid;
	}
	
	protected Bid mockedBid(BigDecimal price, User mockedUser) {
		Bid mockedBid = new Bid();
		mockedBid.setPrice(price);
		mockedBid.setCurrency(mockedCurrency());
		mockedBid.setUnit(mockedUnit());
		mockedBid.setUser(mockedUser);
		return mockedBid;
	}
	
	protected Bid mockedBid(BigDecimal price, Company mockedCompany) {
		Bid mockedBid = new Bid();
		mockedBid.setPrice(price);
		mockedBid.setCurrency(mockedCurrency());
		mockedBid.setUnit(mockedUnit());
		mockedBid.setCompany(mockedCompany);
		mockedBid.setCreatedBy(mockedUser());
		return mockedBid;
	}
	
	protected Bid mockedBid(BigDecimal price, Company mockedCompany, User mockedUser) {
		Bid mockedBid = new Bid();
		mockedBid.setPrice(price);
		mockedBid.setCurrency(mockedCurrency());
		mockedBid.setUnit(mockedUnit());
		mockedBid.setCompany(mockedCompany);
		mockedBid.setCreatedBy(mockedUser);
		return mockedBid;
	}
	
	protected Category mockedCategory() {
		Category category = new Category("Cereals & Pulses");
		category.setId(1L);
		return category;
	}
	
	protected Currency mockedCurrency() {
		Currency mockedCurrency = new Currency();
		mockedCurrency.setId(1L);
		mockedCurrency.setName("Rs.");
		return mockedCurrency;
	}
	
	protected Unit mockedUnit() {
		Unit mockedUnit = new Unit();
		mockedUnit.setId(1L);
		mockedUnit.setName("Q");
		return mockedUnit;
	}
	
	protected ProductStatus mockedStatus(String status) {
		ProductStatus productStatus = new ProductStatus(status);
		return productStatus;
	}
	
	protected List<User> mockedUsers() {
		List<User> users = new ArrayList<>();
		users.add(mockedUser());
		return users;
	}
	
	protected User mockedUser() {
		User mockedUser = new User("Mahie", "Singh", "msingh@vocac.com");
		mockedUser.setUserNo(UserNoGenerator.generateUserNo());
		mockedUser.addReview(mockedReview());
		return mockedUser;
	}

	protected User mockedUserWithBasicSettings() {
		User mockedUser = mockedUser();
		mockedUser.setGroup(mockedGroup("Farmer", "ROLE_USER", "ROLE_FARMER"));
		mockedUser.setDefaultCountry(mockedCountry());
		mockedUser.setDefaultLanguage(mockedLanguage());
		mockedUser.setDefaultImage(mockedImage());
		mockedUser.setAddress(mockedAddress());
		mockedUser.setCompany(mockedCompany("ABC Company", "83924839"));
		return mockedUser;
	}
	
	protected List<Order> mockedOrders() {
		List<Order> mockedOrders = new ArrayList<>();
		mockedOrders.add(mockedOrder());
		return mockedOrders;
	}
	
	protected Order mockedOrder() {
		Order mockedOrder = new Order();
		mockedOrder.setBillingAddress(mockedAddressEntity());
		mockedOrder.setShippingAddress(mockedAddressEntity());
		mockedOrder.setDeliveryDate(Date.from(Environment.clock().instant()));
		return mockedOrder;
	}
	
	protected Review mockedReview() {
		Review mockedReview = new Review(new Rating(4.0));
		User reviewBy = new User("Reviewer", "Name", "reviewer@vocac.com");
		reviewBy.setUserNo(UserNoGenerator.generateUserNo());
		reviewBy.setGroup(mockedGroup("Retailer", "ROLE_USER", "ROLE_RETAILER"));
		mockedReview.setReviewBy(reviewBy);
		return mockedReview;
	}
	
	protected Quantity mockedQuantity(double quantity) {
		Quantity mockedQuantity = new Quantity();
		mockedQuantity.setQuantity(quantity);
		mockedQuantity.setUnit(mockedUnit());
		return mockedQuantity;
	}

	protected Country mockedCountry() {
		Country mockedCountry = new Country("India");
		return mockedCountry;
	}
	
	protected Language mockedLanguage() {
		Language mockedLanguage = new Language("English");
		return mockedLanguage;
	}
	
	protected Address mockedAddress() {
		Address mockedAddress = new Address();
		mockedAddress.setAddressLine1("Address Line 1");
		mockedAddress.setAddressLine2("Address Line 2");
		mockedAddress.setCity("New York");
		return mockedAddress;
	}
	
	protected AddressBean mockedAddressBean() {
		AddressBean address = new AddressBean();
		address.setAddressLine1("Address Line 1");
		address.setAddressLine2("Address Line 2");
		address.setCity("City");
		address.setState(new StateBean(1L, "Uttar Pradesh"));
		address.setCountry(new CountryBean(1L, "India"));
		return address;
	}
	
	protected List<AddressEntity> mockedAddressEntities() {
		List<AddressEntity> addressEntities = new ArrayList<>();
		addressEntities.add(mockedAddressEntity());
		return addressEntities;
	}
	
	protected AddressEntity mockedAddressEntity() {
		AddressEntity mockedAddress = new AddressEntity();
		mockedAddress.setAddressLine1("Address Line 1");
		mockedAddress.setAddressLine2("Address Line 2");
		mockedAddress.setCity("New York");
		mockedAddress.setState(new State("Uttar Pradesh"));
		mockedAddress.setCountry(new Country("India"));
		return mockedAddress;
	}
	
	protected BankAccountBean mockedBankAccountBean() {
		BankAccountBean mockedBankAccount = new BankAccountBean();
		mockedBankAccount.setAccount("9432832732783892");
		mockedBankAccount.setBank(new BankBean(1L, "ICICI BANK"));
		mockedBankAccount.setOwner("Owner Name");
		mockedBankAccount.setSwift("485");
		return mockedBankAccount;
	}
	
	protected GrainCart mockedGrainCart() {
		GrainCart mockedGrainCart = new GrainCart();
		mockedGrainCart.setConsumerCart(Boolean.FALSE);
		mockedGrainCart.addProduct(mockedProduct());
		return mockedGrainCart;
	}
	
	protected GrainCart mockedConsumerGrainCart() {
		GrainCart mockedGrainCart = new GrainCart();
		mockedGrainCart.setConsumerCart(Boolean.TRUE);
		mockedGrainCart.addItem(mockedItem());
		return mockedGrainCart;
	}
	
	protected Image mockedImage() {
		Image mockedImage = new Image();
		return mockedImage;
	}
	
	protected Price mockedPrice(double price) {
		Price mockedPrice = new Price();
		mockedPrice.setPrice(price);
		return mockedPrice;
	}

	protected Bid mockedBid(double price) {
		return this.mockedBid(BigDecimal.valueOf(price));
	}
	
	protected Bid mockedBid(double price, User user) {
		return this.mockedBid(BigDecimal.valueOf(price), user);
	}
	
	protected Bid mockedBid(double price, Company company) {
		return this.mockedBid(BigDecimal.valueOf(price), company);
	}
	
	protected Bid mockedBid(double price, Company company, User mockedUser) {
		return this.mockedBid(BigDecimal.valueOf(price), company, mockedUser);
	}

	@SuppressWarnings("unchecked")
	protected List<Long> mockedSearchResults(Long...ids) {
		return Arrays.asList(ids);
	}
	
}
