package com.carefarm.prakrati.common.entity;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;

import com.openpojo.reflection.PojoClass;
import com.openpojo.reflection.PojoClassFilter;
import com.openpojo.reflection.filters.FilterPackageInfo;
import com.openpojo.reflection.impl.PojoClassFactory;
import com.openpojo.validation.Validator;
import com.openpojo.validation.ValidatorBuilder;
import com.openpojo.validation.affirm.Affirm;
import com.openpojo.validation.rule.impl.GetterMustExistRule;
import com.openpojo.validation.rule.impl.NoFieldShadowingRule;
import com.openpojo.validation.rule.impl.NoPublicFieldsExceptStaticFinalRule;
import com.openpojo.validation.rule.impl.NoStaticExceptFinalRule;
import com.openpojo.validation.rule.impl.SerializableMustHaveSerialVersionUIDRule;
import com.openpojo.validation.test.impl.GetterTester;
import com.openpojo.validation.test.impl.SetterTester;

import edu.emory.mathcs.backport.java.util.Arrays;

public class PojoTests {

	// Configured for expectation, so we know when a class gets added or
	// removed.
	private static final int EXPECTED_CLASS_COUNT = 15;

	// The package to test
	private static final String POJO_PACKAGE = "com.carefarm.prakrati.common.entity";

	private static final PojoClassFilter filterIdClasses = new FilterIdClasses();

	@Test
	public void ensureExpectedPojoCount() {
		List<PojoClass> pojoClasses = PojoClassFactory.getPojoClasses(
				POJO_PACKAGE, new FilterPackageInfo());
		Affirm.affirmEquals("Classes added / removed?", EXPECTED_CLASS_COUNT,
				pojoClasses.size());
	}

	@Test
	public void testPojoStructureAndBehavior() {
		Validator validator = ValidatorBuilder
				.create()
				// Add Rules to validate structure for POJO_PACKAGE
				// See com.openpojo.validation.rule.impl for more ...
				.with(new GetterMustExistRule())
				
				.with(new NoStaticExceptFinalRule())
				.with(new SerializableMustHaveSerialVersionUIDRule())
				.with(new NoFieldShadowingRule())
				.with(new NoPublicFieldsExceptStaticFinalRule())
				// Add Testers to validate behaviour for POJO_PACKAGE
				// See com.openpojo.validation.test.impl for more ...
				.with(new SetterTester()).with(new GetterTester()).build();

		validator.validate(POJO_PACKAGE, new FilterPackageInfo(),
				filterIdClasses);
	}

	private static class FilterIdClasses implements PojoClassFilter {

		private Set<String> idClasses = new HashSet<>();
		
		@SuppressWarnings("unchecked")
		public FilterIdClasses() {
			String[] classes = { 
					"UserPlan$Id", "BuyerWishlist$Id", "Contact$Id", 
					"IndividualPlan$Id", "SellerWishlist$Id", "CompanyPlan$Id", 
					"PojoTests$FilterIdClasses" 
				};
			idClasses.addAll(Arrays.asList(classes));
		}
		
		@Override
		public boolean include(PojoClass pojoClass) {
			String className = StringUtils.substring(pojoClass.getName(), pojoClass.getName().lastIndexOf(".") + 1);
			return !idClasses.contains(className);
		}

	}

}