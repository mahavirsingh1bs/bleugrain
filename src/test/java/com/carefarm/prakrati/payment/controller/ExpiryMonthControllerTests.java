package com.carefarm.prakrati.payment.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.carefarm.prakrati.config.PrakratiIntegrationTest;

public class ExpiryMonthControllerTests  extends PrakratiIntegrationTest {

	@Autowired
	private WebApplicationContext context;
	
	private MockMvc mvc;
	
	@Before
	public void setup() {
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.build();
	}

	@Test
	public void testFindExpiryMonths() throws Exception {
		
		mvc.perform(get("/expiryMonths/all"))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
}
