package com.carefarm.prakrati.payment.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.config.SearchIntegrationTest;
import com.carefarm.prakrati.web.payment.model.BillingDetailsForm;
import com.fasterxml.jackson.databind.ObjectMapper;

public class BillingDetailsControllerTests extends SearchIntegrationTest {
	
	@Autowired
	private WebApplicationContext context;
	
	private DomainUsernamePasswordAuthenticationToken authentication;
	
	private MockMvc mvc;
	
	@Before
	public void setup() {
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}

	@Test
	public void testThatBankAccountHasBeenAddedSuccessfully() throws Exception {
	
		BillingDetailsForm bankAccount = new BillingDetailsForm();
		bankAccount.setDiscrType("bankAccount");
		bankAccount.setBankId(2L);
		bankAccount.setAccount("8432898327323232");
		bankAccount.setOwner("MAHAVIR SINGH");
		bankAccount.setSwift("345");
		bankAccount.setUserNo("2714270627055727201");
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(bankAccount);
		
	    authentication = new DomainUsernamePasswordAuthenticationToken("msingh@vocac.com", "prak1234", null);
		
		mvc.perform(post("/billing/new")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json)
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}


	@Test
	public void testThatCreditCardHasBeenAddedSuccessfully() throws Exception {
	
		BillingDetailsForm creditCard = new BillingDetailsForm();
		creditCard.setDiscrType("creditCard");
		creditCard.setPaymentCardId(2L);;
		creditCard.setNumber("8432898327323232");
		creditCard.setOwner("MAHAVIR SINGH");
		creditCard.setExpiryMonthId(3L);
		creditCard.setExpiryYearId(10L);
		creditCard.setCompanyId("2714270627055427201");
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(creditCard);
		
	    authentication = new DomainUsernamePasswordAuthenticationToken("rjenith@vocac.com", "prak1234", null);
	    
		mvc.perform(post("/billing/new")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json)
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
}
