package com.carefarm.prakrati.io;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.codec.binary.StringUtils;
import org.codehaus.plexus.util.FileUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.config.DataInitIntegrationTest;
import com.carefarm.prakrati.entity.Image;
import com.carefarm.prakrati.repository.ImageRepository;
import com.carefarm.prakrati.repository.ProductRepository;

@Ignore
public class ImageFolderNameUpdator extends DataInitIntegrationTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(ImageFolderNameUpdator.class);
	
	private static final String PREFIX = "/home/msingh/Local_Disk_D/JavaEE6/TOGAF9/backup/bleugrain_assets/";
	private static final String SLASH = "/";
	
	@Autowired
	private ImageRepository imageRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	@Rollback(false)
	@Test
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void validateThatTheChangesAreCommittedOnImageEntity() {
		Image image = imageRepository.findOne(1L);
		image.setFilename("abc");
	}
	
	@Rollback(false)
	@Test
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void renameTheImageFolders() {
		List<Image> images = imageRepository.findProductImages();
		images.forEach(i -> { 
			Long id = i.getProduct().getId();
			String uniqueId = i.getProduct().getUniqueId();
			changeImagePathAndItsDirectory(id, uniqueId, i);
		});
	}
	
	private void changeImagePathAndItsDirectory(Long id, String uniqueId, Image i) {
		this.renameDirectory(id, uniqueId, i.getFilepath());
		i.setFilepath(i.getFilepath().replace(SLASH + id.toString() + SLASH, SLASH + uniqueId + SLASH));
		
		this.renameDirectory(i.getProduct().getId(), i.getProduct().getUniqueId(), i.getLargeFilepath());
		i.setLargeFilepath(i.getLargeFilepath().replace(SLASH + id.toString() + SLASH, SLASH + uniqueId + SLASH));
		
		this.renameDirectory(i.getProduct().getId(), i.getProduct().getUniqueId(), i.getMediumFilepath());
		i.setMediumFilepath(i.getMediumFilepath().replace(SLASH + id.toString() + SLASH, SLASH + uniqueId + SLASH));
		
		this.renameDirectory(i.getProduct().getId(), i.getProduct().getUniqueId(), i.getSmallFilepath());
		i.setSmallFilepath(i.getSmallFilepath().replace(SLASH + id.toString() + SLASH, SLASH + uniqueId + SLASH));
	}
	
	private void renameDirectory(Long id, String uniqueId, String filepath) {
		String replaced = filepath.replace(SLASH + id.toString() + SLASH, SLASH + uniqueId + SLASH);
		
		if (StringUtils.equals(filepath, replaced)) return;
		
		String source = filepath.substring(0, filepath.indexOf(SLASH + id.toString() + SLASH)).concat(SLASH + id.toString() + SLASH);
		String dest = filepath.substring(0, replaced.indexOf(SLASH + uniqueId + SLASH)).concat(SLASH + uniqueId + SLASH);
		
		if (!new File(PREFIX + source).exists()) {
			LOGGER.info("Source Directory: {} doesn't exists", PREFIX + source);
			return;
		}
		try {
			FileUtils.copyDirectoryStructure(new File(PREFIX + source), new File(PREFIX + dest));
			FileUtils.deleteDirectory(new File(PREFIX + source));
		} catch (IOException e) {
			LOGGER.error("Error occurred while trying to copy Source Directory: {} to Destination Directory: ", source, dest);
		}
	}
}
