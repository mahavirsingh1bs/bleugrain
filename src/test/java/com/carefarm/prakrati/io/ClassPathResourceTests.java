package com.carefarm.prakrati.io;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.util.Base64Utils;

public class ClassPathResourceTests {

	@Test
	public void testClassPathResource() throws IOException {
		ClassPathResource res = new ClassPathResource("static/assets/emails/img/icons/icon_layers_orange.png");
		Path path = Paths.get(res.getFile().getAbsolutePath());
		String base64Str = "data:image/png;base64," + Base64Utils.encodeToString(Files.readAllBytes(path));
		System.out.println(base64Str);
		System.out.println(Base64Utils.encodeToString(Files.readAllBytes(path)));
		
	}
}
