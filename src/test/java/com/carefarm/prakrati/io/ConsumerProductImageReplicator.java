package com.carefarm.prakrati.io;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.codec.binary.StringUtils;
import org.codehaus.plexus.util.FileUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.config.DataInitIntegrationTest;
import com.carefarm.prakrati.entity.Image;
import com.carefarm.prakrati.entity.Product;
import com.carefarm.prakrati.repository.ProductRepository;

@Ignore
public class ConsumerProductImageReplicator extends DataInitIntegrationTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(ConsumerProductImageReplicator.class);
	
	private static final String PREFIX = "/home/msingh/Local_Disk_D/JavaEE6/TOGAF9/backup/bleugrain_assets/";
	private static final String SLASH = "/";
	
	private static final String PRO_DIR_PREFIX = "assets/resources/data/products";
	
	@Autowired
	private ProductRepository productRepository;
	
	@Rollback(false)
	@Test
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void renameTheImageFoldersOfConsumerProducts() {
		List<Product> consumerProducts = productRepository.findAllConsumerProducts();
		consumerProducts.forEach(p -> {
			changeImagePathAndItsDirectory(p.getUniqueId(), p.getDefaultImage());
		});
	}
	
	private void changeImagePathAndItsDirectory(String uniqueId, Image i) {
		this.renameDirectory(uniqueId, i.getFilepath());
		i.setFilepath(getTargetDirectory(uniqueId, i.getFilepath()) + getImageSuffix(i.getFilepath()));
		
		this.renameDirectory(uniqueId, i.getLargeFilepath());
		i.setLargeFilepath(getTargetDirectory(uniqueId, i.getLargeFilepath()) + getImageSuffix(i.getLargeFilepath()));
		
		this.renameDirectory(uniqueId, i.getMediumFilepath());
		i.setMediumFilepath(getTargetDirectory(uniqueId, i.getMediumFilepath()) + getImageSuffix(i.getMediumFilepath()));
		
		this.renameDirectory(uniqueId, i.getSmallFilepath());
		i.setSmallFilepath(getTargetDirectory(uniqueId, i.getSmallFilepath()) + getImageSuffix(i.getSmallFilepath()));
	}
	
	private void renameDirectory(String uniqueId, String filepath) {
		String source = getSourceDirectory(filepath);
		String dest = getTargetDirectory(uniqueId, filepath);
		
		if (StringUtils.equals(source, dest)) return;
		
		if (!new File(PREFIX + source).exists()) {
			LOGGER.info("Source Directory: {} doesn't exists", PREFIX + source);
			return;
		}
		try {
			FileUtils.copyDirectoryStructure(new File(PREFIX + source), new File(PREFIX + dest));
			FileUtils.deleteDirectory(new File(PREFIX + source));
		} catch (IOException e) {
			LOGGER.error("Error occurred while trying to copy Source Directory: {} to Destination Directory: ", source, dest);
		}
	}

	private String getSourceDirectory(String filepath) {
		String[] tokens = filepath.split(SLASH);
		
		String[] target = null;
		if (tokens.length == 7) {
			target = new String[5];
			System.arraycopy(tokens, 0, target, 0, 5);
		} else {
			target = new String[4];
			System.arraycopy(tokens, 0, target, 0, 4);
		}
		
		return String.join(SLASH, target).concat(SLASH);
	}
	
	private String getTargetDirectory(String uniqueId, String filepath) {
		/*
		String[] tokens = filepath.split(SLASH);
		String[] target = new String[3];
		System.arraycopy(tokens, 0, target, 0, 3);
		*/
		return PRO_DIR_PREFIX.concat(SLASH + uniqueId + SLASH);
	}
	
	private String getImageSuffix(String filepath) {
		String[] tokens = filepath.split(SLASH);
		
		String[] suffixes = new String[2];
		if (tokens.length == 7) {
			System.arraycopy(tokens, 5, suffixes, 0, 2);
		} else {
			System.arraycopy(tokens, 4, suffixes, 0, 2);
		}
		
		return String.join(SLASH, suffixes);
	}
	
}
