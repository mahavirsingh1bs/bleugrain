package com.carefarm.prakrati.validators;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.springframework.validation.BeanPropertyBindingResult;

import com.carefarm.prakrati.common.PrakratiTests;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.factory.Factories;
import com.carefarm.prakrati.factory.UserFactory;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.web.model.UserForm;

public class CustomerValidatorTests extends PrakratiTests {
	
	@Test
	public void customerShouldBeGetRejectedWhenPhoneNoAndEmailAddressIsSameAsOfAnotherUser() {
		UserRepository userRepository = mock(UserRepository.class);
		UserFactory<User> userFactory = User::new;
		User user1 = userFactory.create("Mahie", "Singh", "msingh@vocac.com", "msingh@vocac.com", "+919717819586", "+919717819586");
		when(userRepository.findByAlternateUsername("+919717819586")).thenReturn(user1);
		when(userRepository.findByEmailAddress("msingh@vocac.com")).thenReturn(user1);
		
		UserForm customer = Factories.CUSTOMER_FACTORY.create("New", "Customer", "919717819586", "msingh@vocac.com");
		
		CustomerValidator validator = new CustomerValidator();
		this.setField("userRepository", validator, userRepository);

		BeanPropertyBindingResult errors = new BeanPropertyBindingResult(customer, "customer");
		validator.validate(customer, errors);
		assertThat("There should be 2 validation error", errors.getErrorCount(), is(2));
		verify(userRepository, times(1)).findByAlternateUsername("+919717819586");
		verify(userRepository, times(1)).findByEmailAddress("msingh@vocac.com");
	}
	
	@Test
	public void customerIsNotGetRejectedWhenEmailAddressAndMobileNosAreDifferent() {
		UserRepository userRepository = mock(UserRepository.class);
		when(userRepository.findByAlternateUsername("+919717819586")).thenReturn(null);
		when(userRepository.findByEmailAddress("new.customer@vocac.com")).thenReturn(null);
		
		UserForm customer = Factories.CUSTOMER_FACTORY.create("New", "Customer", "919717819586", "new.customer@vocac.com");
		
		CustomerValidator validator = new CustomerValidator();
		this.setField("userRepository", validator, userRepository);

		BeanPropertyBindingResult errors = new BeanPropertyBindingResult(customer, "customer");
		validator.validate(customer, errors);
		assertThat("There should not be any validation error", errors.getErrorCount(), is(0));
		verify(userRepository, times(1)).findByAlternateUsername("+919717819586");
		verify(userRepository, times(1)).findByEmailAddress("new.customer@vocac.com");
	}
	
}
