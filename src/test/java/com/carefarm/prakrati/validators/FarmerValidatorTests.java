package com.carefarm.prakrati.validators;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.springframework.validation.BeanPropertyBindingResult;

import com.carefarm.prakrati.common.PrakratiTests;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.factory.Factories;
import com.carefarm.prakrati.factory.UserFactory;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.web.model.FarmerForm;

public class FarmerValidatorTests extends PrakratiTests {

	@Test
	public void farmerShouldBeRejectedWhenMobileNoIsSameAsOfAnotherUser() {
		UserRepository userRepository = mock(UserRepository.class);
		UserFactory<User> userFactory = User::new;
		User user1 = userFactory.create("Mahie", "Singh", "msingh@vocac.com", "msingh@vocac.com", "+919717819586", "+919717819586");
		when(userRepository.findByAlternateUsername("+919717819586")).thenReturn(user1);
		
		FarmerForm farmer = Factories.FARMER_FACTORY.create("New", "Farmer", "919717819586");
		
		FarmerValidator validator = new FarmerValidator();
		this.setField("userRepository", validator, userRepository);

		BeanPropertyBindingResult errors = new BeanPropertyBindingResult(farmer, "farmer");
		validator.validate(farmer, errors);
		assertThat("There should be 1 validation error", errors.getErrorCount(), is(1));
		verify(userRepository, times(1)).findByAlternateUsername("+919717819586");
	}
	
	@Test
	public void farmerShouldRejectedWhenEmailAddressIsSameAsOfAnExistingUser() {
		UserRepository userRepository = mock(UserRepository.class);
		UserFactory<User> userFactory = User::new;
		User user1 = userFactory.create("Mahie", "Singh", "msingh@vocac.com", "msingh@vocac.com", "+919717819586", "+919717819586");
		when(userRepository.findByAlternateUsername("+919717819586")).thenReturn(user1);
		when(userRepository.findByAlternateUsername("msingh@vocac.com")).thenReturn(user1);
		
		FarmerForm farmer = Factories.FARMER_FACTORY.create("New", "Farmer", "919717819586");
		farmer.setEmail("msingh@vocac.com");
		
		FarmerValidator validator = new FarmerValidator();
		this.setField("userRepository", validator, userRepository);

		BeanPropertyBindingResult errors = new BeanPropertyBindingResult(farmer, "farmer");
		validator.validate(farmer, errors);
		assertThat("There should be 1 validation error", errors.getErrorCount(), is(1));
		verify(userRepository, times(1)).findByAlternateUsername("+919717819586");
		verify(userRepository, times(1)).findByAlternateUsername("+919717819586");
	}
	
	@Test
	public void farmerIsNotGetRejectedWhenEmailAddressAndMobileNosAreDifferent() {
		UserRepository userRepository = mock(UserRepository.class);
		when(userRepository.findByAlternateUsername("+919717819586")).thenReturn(null);
		when(userRepository.findByEmailAddress("new.farmer@vocac.com")).thenReturn(null);
		
		FarmerForm farmer = Factories.FARMER_FACTORY.create("New", "Farmer", "919717819586");
		farmer.setEmail("new.farmer@vocac.com");
		
		FarmerValidator validator = new FarmerValidator();
		this.setField("userRepository", validator, userRepository);

		BeanPropertyBindingResult errors = new BeanPropertyBindingResult(farmer, "farmer");
		validator.validate(farmer, errors);
		assertThat("There should not be any validation error", errors.getErrorCount(), is(0));
		verify(userRepository, times(1)).findByAlternateUsername("+919717819586");
		verify(userRepository, times(1)).findByEmailAddress("new.farmer@vocac.com");
	}
}
