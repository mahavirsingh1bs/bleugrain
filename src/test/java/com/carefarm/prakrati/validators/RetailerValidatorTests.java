package com.carefarm.prakrati.validators;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.springframework.validation.BeanPropertyBindingResult;

import com.carefarm.prakrati.common.PrakratiTests;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.factory.Factories;
import com.carefarm.prakrati.factory.UserFactory;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.web.model.UserForm;

public class RetailerValidatorTests extends PrakratiTests {

	@Test
	public void retailerShouldBeGetRejectedWhenPhoneNoAndEmailAddressIsSameAsOfAnotherUser() {
		UserRepository userRepository = mock(UserRepository.class);
		UserFactory<User> userFactory = User::new;
		User user1 = userFactory.create("Mahie", "Singh", "msingh@vocac.com", "msingh@vocac.com", "+911247819586", "+911247819586");
		when(userRepository.findByAlternateUsername("+911247819586")).thenReturn(user1);
		when(userRepository.findByEmailAddress("msingh@vocac.com")).thenReturn(user1);
		
		UserForm retailer = Factories.RETAILER_FACTORY.create("New", "Retailer", "911247819586", "msingh@vocac.com");
		
		RetailerValidator validator = new RetailerValidator();
		this.setField("userRepository", validator, userRepository);

		BeanPropertyBindingResult errors = new BeanPropertyBindingResult(retailer, "retailer");
		validator.validate(retailer, errors);
		assertThat("There should be 2 validation error", errors.getErrorCount(), is(2));
		verify(userRepository, times(1)).findByAlternateUsername("+911247819586");
		verify(userRepository, times(1)).findByEmailAddress("msingh@vocac.com");
	}
	
	@Test
	public void retailerIsNotGetRejectedWhenEmailAddressAndMobileNosAreDifferent() {
		UserRepository userRepository = mock(UserRepository.class);
		when(userRepository.findByAlternateUsername("+911247819586")).thenReturn(null);
		when(userRepository.findByEmailAddress("new.retailer@vocac.com")).thenReturn(null);
		
		UserForm retailer = Factories.RETAILER_FACTORY.create("New", "Retailer", "911247819586", "new.retailer@vocac.com");
		
		RetailerValidator validator = new RetailerValidator();
		this.setField("userRepository", validator, userRepository);

		BeanPropertyBindingResult errors = new BeanPropertyBindingResult(retailer, "retailer");
		validator.validate(retailer, errors);
		assertThat("There should not be any validation error", errors.getErrorCount(), is(0));
		verify(userRepository, times(1)).findByAlternateUsername("+911247819586");
		verify(userRepository, times(1)).findByEmailAddress("new.retailer@vocac.com");
	}
	
}
