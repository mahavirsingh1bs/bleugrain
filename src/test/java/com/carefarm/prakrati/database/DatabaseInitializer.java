package com.carefarm.prakrati.database;

import static org.dbunit.PropertiesBasedJdbcDatabaseTester.DBUNIT_CONNECTION_URL;
import static org.dbunit.PropertiesBasedJdbcDatabaseTester.DBUNIT_DRIVER_CLASS;
import static org.dbunit.PropertiesBasedJdbcDatabaseTester.DBUNIT_PASSWORD;
import static org.dbunit.PropertiesBasedJdbcDatabaseTester.DBUNIT_USERNAME;

import java.io.File;

import org.dbunit.DBTestCase;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Ignore;

@Ignore
public class DatabaseInitializer extends DBTestCase {

	public DatabaseInitializer() {
		super("Database Initializer");

		/** Configuration for MySQL database */
		System.setProperty(DBUNIT_DRIVER_CLASS, "com.mysql.jdbc.Driver");
        System.setProperty(DBUNIT_CONNECTION_URL, "jdbc:mysql://localhost:3306/prakrati_back");
        System.setProperty(DBUNIT_USERNAME, "root");
        System.setProperty(DBUNIT_PASSWORD, "root");
    }

    @Override
    protected IDataSet getDataSet() throws Exception {
    	FlatXmlDataSetBuilder builder = new FlatXmlDataSetBuilder();
    	IDataSet dataSet = builder.build(new File("src/test/resources/com/carefarm/prakrati/data/prakrati_data.xml"));
    	return dataSet;
    }

    public void testMe() {
        System.out.println("Text Me");
    }

}
