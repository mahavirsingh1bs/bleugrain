package com.carefarm.prakrati.creators;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.carefarm.prakrati.entity.Image;

public class ImageCreatorTests {
	
	@Test
	public void testCreateInstance() {
		
		String filepath = "/home/msingh/Local_Disk_D/JavaEE6/TOGAF9/data/resources/products/Kashmiri_Rice_81.jpg";
		String smallFilepath = "/home/msingh/Local_Disk_D/JavaEE6/TOGAF9/data/resources/products/small/Kashmiri_Rice_81.jpg";
		String mediumFilepath = "/home/msingh/Local_Disk_D/JavaEE6/TOGAF9/data/resources/products/small/Kashmiri_Rice_81.jpg";
		String largeFilepath = "/home/msingh/Local_Disk_D/JavaEE6/TOGAF9/data/resources/products/small/Kashmiri_Rice_81.jpg";
		Image image = ImageCreator.createInstance("Kashmiri_Rice_81", filepath, smallFilepath, mediumFilepath, largeFilepath);
		assertThat("Filepath should not correct", image.getFilepath(), is(filepath));
		assertThat("Small Filepath should not correct", image.getSmallFilepath(), is(smallFilepath));
		assertThat("Medium Filepath should not correct", image.getMediumFilepath(), is(mediumFilepath));
		assertThat("Big Filepath should not correct", image.getLargeFilepath(), is(largeFilepath));
	}
	
}
