package com.carefarm.prakrati.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.config.SearchIntegrationTest;
import com.carefarm.prakrati.search.document.UserDoc;
import com.carefarm.prakrati.web.model.RegisterForm;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SignUpControllerTests extends SearchIntegrationTest {

	@Autowired
	private WebApplicationContext context;
	
	private DomainUsernamePasswordAuthenticationToken authentication;
	
	private MockMvc mvc;
	
	@Before
	public void setup() {
		createIndex(UserDoc.class);
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}
	
	@Test
	public void testThatFarmerHasBeenRegisteredSuccessfully() throws Exception {
		
		RegisterForm farmer = new RegisterForm();
		farmer.setFirstName("Mahavir");
		farmer.setLastName("Singh");
		farmer.setMobileNo("919718847237");
		farmer.setPassword("prak1234");
		farmer.setConfirmPassword("prak1234");
		farmer.setEmailAddress("register.farmer@gmail.com");
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(farmer);
		
		mvc.perform(post("/register/farmer")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON)
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
	@Test
	public void testThatCustomerHasBeenRegisteredSuccessfully() throws Exception {
		
		RegisterForm customer = new RegisterForm();
		customer.setFirstName("Mahavir");
		customer.setLastName("Singh");
		customer.setMobileNo("9197178184389");
		customer.setEmailAddress("register.customer@gmail.com");
		customer.setPassword("prak1234");
		customer.setConfirmPassword("prak1234");
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(customer);
		
		mvc.perform(post("/register/customer")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON)
				.with(csrf().asHeader()))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
	@Test
	public void testThatBrokerHasBeenRegisteredSuccessfully() throws Exception {
		
		RegisterForm broker = new RegisterForm();
		broker.setFirstName("Mahavir");
		broker.setLastName("Singh");
		broker.setPhoneNo("9111178184389");
		broker.setMobileNo("9197178184389");
		broker.setEmailAddress("register.broker@gmail.com");
		broker.setPassword("prak1234");
		broker.setConfirmPassword("prak1234");
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(broker);
		
		mvc.perform(post("/register/broker")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON)
				.with(csrf().asHeader()))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
	@Test
	public void testThatRetailerHasBeenRegisteredSuccessfully() throws Exception {
		
		RegisterForm retailer = new RegisterForm();
		retailer.setFirstName("Mahavir");
		retailer.setLastName("Singh");
		retailer.setPhoneNo("9111178184389");
		retailer.setMobileNo("9197178184389");
		retailer.setEmailAddress("register.retailer@gmail.com");
		retailer.setPassword("prak1234");
		retailer.setConfirmPassword("prak1234");
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(retailer);
		
		mvc.perform(post("/register/retailer")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON)
				.with(csrf().asHeader()))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
	@Test
	public void testThatCompanyHasBeenRegisteredSuccessfully() throws Exception {
		
		RegisterForm company = new RegisterForm();
		company.setFirstName("Mahavir");
		company.setLastName("Singh");
		company.setPhoneNo("9111178184389");
		company.setEmailAddress("register.company@gmail.com");
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(company);
		
		mvc.perform(post("/register/company")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON)
				.with(csrf().asHeader()))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
}
