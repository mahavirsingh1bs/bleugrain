package com.carefarm.prakrati.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.config.SearchIntegrationTest;
import com.carefarm.prakrati.search.document.OrderDoc;
import com.carefarm.prakrati.web.bean.AddressBean;
import com.carefarm.prakrati.web.bean.CountryBean;
import com.carefarm.prakrati.web.bean.GrainCartBean;
import com.carefarm.prakrati.web.bean.OrderDetailsBean;
import com.carefarm.prakrati.web.bean.StateBean;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class OrderControllerTests extends SearchIntegrationTest {
	
	@Autowired
	private WebApplicationContext context;
	
	private DomainUsernamePasswordAuthenticationToken authentication;
	
	private MockMvc mvc;
	
	@Before
	public void setup() {
		createIndex(OrderDoc.class);
		authentication = new DomainUsernamePasswordAuthenticationToken("rjenith@vocac.com", "prak1234", null);
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}
	
	@Test
	public void testThatOrderHasBeenAddedSuccessfully() throws Exception {
		
		mvc.perform(post("/graincart/addToCart")
				.content("ba7c5b0c-297a-4c88-9b45-dc6b7d937ee9")
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
		
		mvc.perform(post("/graincart/addToCart")
				.content("f4eb7252-68c8-4c9e-b4ac-b303dc034367")
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();

		
		AddressBean shippingAddress = new AddressBean();
		shippingAddress.setAddressLine1("Village-Pali, Post-Bharna Kalan");
		shippingAddress.setAddressLine2("District-Mathura");
		shippingAddress.setCity("Goverdhan");
		shippingAddress.setState(new StateBean(1L, "Uttar Pradesh"));
		shippingAddress.setCountry(new CountryBean(1L, "India"));
		shippingAddress.setZipcode("281501");
		
		AddressBean billingAddress = new AddressBean();
		billingAddress.setAddressLine1("Village-Pali, Post-Bharna Kalan");
		billingAddress.setAddressLine2("District-Mathura");
		billingAddress.setCity("Goverdhan");
		shippingAddress.setState(new StateBean(1L, "Uttar Pradesh"));
		shippingAddress.setCountry(new CountryBean(1L, "India"));
		billingAddress.setZipcode("281501");

		OrderDetailsBean order = new OrderDetailsBean();
		order.setGrainCartId(1L);
		order.setSubtotal(BigDecimal.valueOf(3903.0));
		order.setShippingCost(BigDecimal.valueOf(99.0));
		order.setBillingAddress(billingAddress);
		order.setShippingAddress(shippingAddress);

		ObjectMapper mapper = new ObjectMapper();
	    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		
		MockHttpServletResponse response = mvc.perform(get("/graincart/")
				.with(csrf().asHeader())
				.with(authentication(authentication)))
				.andReturn().getResponse();
		GrainCartBean grainCart = mapper.readValue(response.getContentAsString(), GrainCartBean.class);
		order.setGrainCartId(grainCart.getId());
		
		mvc.perform(post("/orders/processOrder")
				.contentType(MediaType.APPLICATION_JSON)
				.content(mapper.writeValueAsString(order))
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
}
