package com.carefarm.prakrati.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.config.SearchIntegrationTest;
import com.carefarm.prakrati.search.document.UserDoc;
import com.carefarm.prakrati.web.model.UserForm;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CustomerControllerTests extends SearchIntegrationTest {

	@Autowired
	private WebApplicationContext context;
	
	private DomainUsernamePasswordAuthenticationToken authentication;
	
	private MockMvc mvc;
	
	@Before
	public void setup() {
		createIndex(UserDoc.class);
		authentication = new DomainUsernamePasswordAuthenticationToken("admin@vocac.com", "prak1234", null);
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}
	
	@Test
	public void testThatCustomerHasBeenAddedSuccessfully() throws Exception {
		
		UserForm customer = new UserForm();
		customer.setFirstName("Mahavir");
		customer.setMiddleInitial("N");
		customer.setLastName("Singh");
		customer.setAddressLine1("Village-Pali, Post-Bharna Kalan");
		customer.setAddressLine2("District-Mathura");
		customer.setCity("Goverdhan");
		customer.setStateId(1L);
		customer.setCountryId(1L);
		customer.setZipcode("281501");
		customer.setMobileNo("9197178184389");
		customer.setEmail("new.customer@gmail.com");
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(customer);
		
	    MockMultipartFile customerPart = new MockMultipartFile("customer", "blob", "application/json", json.getBytes());
	    
		mvc.perform(fileUpload("/customers/new")
				.file(customerPart)
				.contentType(MediaType.MULTIPART_FORM_DATA)
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
}
