package com.carefarm.prakrati.controller;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.config.SearchIntegrationTest;
import com.carefarm.prakrati.search.document.DemandDoc;
import com.carefarm.prakrati.search.document.ProductDoc;

public class SearchControllerTests extends SearchIntegrationTest {
	
	@Autowired
	private WebApplicationContext context;
	
	private MockMvc mvc;

	@Autowired
	private RestTemplate restTemplate;
	
	@Before
	public void setup() {
		createIndex(ProductDoc.class);
		createIndex(DemandDoc.class);
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}
	
	@Test
	public void testThatProductsMatchingTheKeywords() throws Exception {
		
	    MvcResult result = mvc.perform(get("/search/")
	    		.param("page", "0")
	    		.param("pageSize", "10")
	    		.param("sort", "dateModified")
	    		.param("cnsmrPrdt", "true")
	    		.param("keyword", "cereals")
	    		.param("facets", "")
	    		.with(csrf().asHeader()))
	            .andExpect(status().isOk())
	            .andReturn();
	    assertThat("Response is not null", result.getResponse(), is(notNullValue()));
	}
	
	@Test
	public void testSearchDemandsMatchingTheKeywords() throws Exception {
		
		DomainUsernamePasswordAuthenticationToken authentication = new DomainUsernamePasswordAuthenticationToken("msingh@vocac.com", "prak1234", null);
	    MvcResult result = mvc.perform(get("/search/demands")
	    		.param("page", "0")
	    		.param("pageSize", "10")
	    		.param("sort", "dateModified")
	    		.param("keyword", "cereals")
	    		.param("facets", "")
	    		.with(csrf().asHeader())
	    		.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	    assertThat("Response is not null", result.getResponse(), is(notNullValue()));
	}
	
	@Test
	public void testThatFindProductDetail() throws Exception {
		
		String uniqueId = "f4eb7252-68c8-4c9e-b4ac-b303dc034367";
		DomainUsernamePasswordAuthenticationToken authentication = new DomainUsernamePasswordAuthenticationToken("rjenith@vocac.com", "prak1234", null);
		
	    MvcResult result = mvc.perform(get("/search/detail")
	    		.param("uniqueId", uniqueId)
	    		.with(csrf().asHeader())
	    		.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	    assertThat("Response is not null", result.getResponse(), is(notNullValue()));
	}
	
}
