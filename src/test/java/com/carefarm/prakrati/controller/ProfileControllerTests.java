package com.carefarm.prakrati.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.config.PrakratiIntegrationTest;
import com.carefarm.prakrati.web.bean.GroupBean;
import com.carefarm.prakrati.web.model.AddressEntityForm;
import com.carefarm.prakrati.web.model.AddressForm;
import com.carefarm.prakrati.web.model.DiscussionForm;
import com.carefarm.prakrati.web.model.NoteForm;
import com.carefarm.prakrati.web.model.NotificationForm;
import com.carefarm.prakrati.web.model.UpcomingEventForm;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ProfileControllerTests extends PrakratiIntegrationTest {

	@Autowired
	private WebApplicationContext context;
	
	private DomainUsernamePasswordAuthenticationToken authentication;
	
	private MockMvc mvc;
	
	@Before
	public void setup() {
		authentication = new DomainUsernamePasswordAuthenticationToken("wbroker1@vocac.com", "prak1234", null);
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}

	@Test
	public void testThatAdminDashboard() throws Exception {
		authentication = new DomainUsernamePasswordAuthenticationToken("admin@vocac.com", "prak1234", null);
		mvc.perform(get("/profile/dashboard")
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}

	@Test
	public void testThatEmployeeDashboard() throws Exception {
		authentication = new DomainUsernamePasswordAuthenticationToken("wregion@vocac.com", "prak1234", null);
		mvc.perform(get("/profile/dashboard")
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
	@Test
	public void testThatCompanyAdminDashboard() throws Exception {
		authentication = new DomainUsernamePasswordAuthenticationToken("rjenith@vocac.com", "prak1234", null);
		mvc.perform(get("/profile/dashboard")
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
	@Test
	public void testThatAgentDashboard() throws Exception {
		authentication = new DomainUsernamePasswordAuthenticationToken("jagent@vocac.com", "prak1234", null);
		mvc.perform(get("/profile/dashboard")
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}

	@Test
	public void testThatBrokerDashboard() throws Exception {
		authentication = new DomainUsernamePasswordAuthenticationToken("wbroker1@vocac.com", "prak1234", null);
		mvc.perform(get("/profile/dashboard")
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
	@Test
	public void testThatRetailerDashboard() throws Exception {
		authentication = new DomainUsernamePasswordAuthenticationToken("jjones@vocac.com", "prak1234", null);
		mvc.perform(get("/profile/dashboard")
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
	@Test
	public void testThatFarmerDashboard() throws Exception {
		authentication = new DomainUsernamePasswordAuthenticationToken("msingh@vocac.com", "prak1234", null);
		mvc.perform(get("/profile/dashboard")
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
	@Test
	public void testThatCustomerDashboard() throws Exception {
		authentication = new DomainUsernamePasswordAuthenticationToken("mahavirsingh1bs@gmail.com", "prak1234", null);
		mvc.perform(get("/profile/dashboard")
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
	@Test
	public void testThatNoteHasBeenAddedSuccessfully() throws Exception {
	
		NoteForm note = new NoteForm();
		note.setTitle("Doing well");
		note.setNote("Patanjali is doing well");
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(note);
		
		mvc.perform(post("/profile/addNote?userNo=2714270627057027201")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json)
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
	@Test
	public void testThatUpcomingEventHasBeenAddedSuccessfully() throws Exception {
	
		UpcomingEventForm upcomingEvent = new UpcomingEventForm();
		upcomingEvent.setEvent("Doing well");
		upcomingEvent.setEventDetail("Patanjali is doing well");
		upcomingEvent.setEventDate("01/11/2016");
		upcomingEvent.setOrganizerId("432432432423432");
		upcomingEvent.addGroup(new GroupBean(1L, "Admin"));
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(upcomingEvent);
		
		mvc.perform(post("/profile/addUpcomingEvent?userNo=2714270627057027201")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json)
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
	@Test
	public void testThatDiscussionHasBeenAddedSuccessfully() throws Exception {
	
		DiscussionForm discussion = new DiscussionForm();
		discussion.setTitle("Doing well");
		discussion.setDescription("Patanjali is doing well");
		discussion.addGroup(new GroupBean(1L, "Admin"));
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(discussion);
		
		mvc.perform(post("/profile/addDiscussion?userNo=2714270627057027201")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json)
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
	@Test
	public void testThatNotificationHasBeenAddedSuccessfully() throws Exception {
	
		NotificationForm notification = new NotificationForm();
		notification.setCategoryId(1L);
		notification.setTitle("Doing well");
		notification.setNotification("Patanjali is doing well");
		notification.setStartDate("01/11/2016");
		notification.setExpiryDate("21/11/2016");
		notification.addGroup(new GroupBean(1L, "Admin"));
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(notification);
		
		mvc.perform(post("/profile/sendNotification?userNo=2714270627057027201")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json)
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
	@Test
	public void testThatAddressHasBeenAddedSuccessfully() throws Exception {
	
		AddressForm address = new AddressForm();
		address.setAddressLine1("Village-Pali, Post-Bharna Kalan");
		address.setAddressLine2("District-Mathura");
		address.setCity("Goverdhan");
		address.setStateId(1L);
		address.setCountryId(1L);
		address.setZipcode("281501");
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(address);
		
		mvc.perform(post("/profile/addDeliveryAddress?userNo=2714270627057027201")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json)
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
	@Test
	public void testThatBillingAddressHasBeenAddedSuccessfully() throws Exception {
	
		AddressEntityForm billingAddress = new AddressEntityForm();
		billingAddress.setFirstName("Wales");
		billingAddress.setFirstName("Broker");
		billingAddress.setPhoneNo("919717819586");
		billingAddress.setEmailAddress("wbroker1@vocac.com");
		billingAddress.setAddressLine1("Village-Pali, Post-Bharna Kalan");
		billingAddress.setAddressLine2("District-Mathura");
		billingAddress.setCity("Goverdhan");
		billingAddress.setStateId(1L);
		billingAddress.setCountryId(1L);
		billingAddress.setZipcode("281501");
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(billingAddress);
		
		mvc.perform(post("/profile/addBillingAddress?userNo=2714270627057027201")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json)
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
	@Test
	public void testThatShippingAddressHasBeenAddedSuccessfully() throws Exception {
	
		AddressEntityForm shippingAddress = new AddressEntityForm();
		shippingAddress.setFirstName("Wales");
		shippingAddress.setFirstName("Broker");
		shippingAddress.setPhoneNo("919717819586");
		shippingAddress.setEmailAddress("wbroker1@vocac.com");
		shippingAddress.setAddressLine1("Village-Pali, Post-Bharna Kalan");
		shippingAddress.setAddressLine2("District-Mathura");
		shippingAddress.setCity("Goverdhan");
		shippingAddress.setStateId(1L);
		shippingAddress.setCountryId(1L);
		shippingAddress.setZipcode("281501");
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(shippingAddress);
		
		mvc.perform(post("/profile/addAddress?userNo=2714270627057027201")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json)
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
}
