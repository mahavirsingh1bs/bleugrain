package com.carefarm.prakrati.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.config.SearchIntegrationTest;
import com.carefarm.prakrati.search.document.DepartmentDoc;
import com.carefarm.prakrati.web.model.DepartmentForm;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DepartmentControllerTests extends SearchIntegrationTest {

	@Autowired
	private WebApplicationContext context;
	
	private DomainUsernamePasswordAuthenticationToken authentication;
	
	private MockMvc mvc;
	
	@Before
	public void setup() {
		createIndex(DepartmentDoc.class);
		authentication = new DomainUsernamePasswordAuthenticationToken("admin@vocac.com", "prak1234", null);
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}

	@Test
	public void testThatDepartmentHasBeenAddedSuccessfully() throws Exception {
	
		DepartmentForm department = new DepartmentForm();
		department.setName("Technology");
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(department);
		
		mvc.perform(post("/departments/new")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json)
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
}
