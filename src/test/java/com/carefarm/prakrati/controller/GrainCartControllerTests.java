package com.carefarm.prakrati.controller;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.config.PrakratiIntegrationTest;
import com.carefarm.prakrati.web.bean.GrainCartBean;
import com.fasterxml.jackson.databind.ObjectMapper;

public class GrainCartControllerTests extends PrakratiIntegrationTest {

	@Autowired
	private WebApplicationContext context;

	@Autowired
	private ObjectMapper mapper;
	
	private DomainUsernamePasswordAuthenticationToken authentication;
	
	private MockMvc mvc;
	
	@Before
	public void setup() {
		authentication = new DomainUsernamePasswordAuthenticationToken("rjenith@vocac.com", "prak1234", null);
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}
	
	@Test
	public void testThatOrderHasBeenAddedSuccessfully() throws Exception {
		
		mvc.perform(post("/graincart/addToCart")
				.content("ba7c5b0c-297a-4c88-9b45-dc6b7d937ee9")
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
		
		mvc.perform(post("/graincart/addToCart")
				.content("f4eb7252-68c8-4c9e-b4ac-b303dc034367")
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();

		MockHttpServletResponse response = mvc.perform(get("/graincart/")
				.with(csrf().asHeader())
				.with(authentication(authentication)))
				.andReturn().getResponse();
		GrainCartBean grainCart = mapper.readValue(response.getContentAsString(), GrainCartBean.class);
		
		assertThat("Total Items in graincart should be 2", grainCart.getTotalItems(), is(2));
		assertThat("Total cost of items in graincart should be Rs.582,000.00", grainCart.getTotalCost(), is("Rs.582,000.00"));
		assertThat("This graincart should belong to a company", grainCart.getCompany(), is(nullValue()));
		assertThat("This graincart should not belong to a user", grainCart.getUser(), is(notNullValue()));
	}
	
}
