package com.carefarm.prakrati.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.config.SearchIntegrationTest;
import com.carefarm.prakrati.search.document.UserDoc;
import com.carefarm.prakrati.web.model.AgentForm;
import com.fasterxml.jackson.databind.ObjectMapper;

public class AgentControllerTests extends SearchIntegrationTest {
	
	@Autowired
	private WebApplicationContext context;
	
	private DomainUsernamePasswordAuthenticationToken authentication;
	
	private MockMvc mvc;
	
	@Before
	public void setup() {
		createIndex(UserDoc.class);
		authentication = new DomainUsernamePasswordAuthenticationToken("admin@vocac.com", "prak1234", null);
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}
	
	@Test
	public void testThatAgentHasBeenAddedSuccessfully() throws Exception {
		
		AgentForm agent = new AgentForm();
		agent.setFirstName("Mahavir");
		agent.setMiddleInitial("N");
		agent.setLastName("Singh");
		agent.setAddressLine1("Village-Pali, Post-Bharna Kalan");
		agent.setAddressLine2("District-Mathura");
		agent.setCity("Goverdhan");
		agent.setStateId(1L);
		agent.setCountryId(1L);
		agent.setZipcode("281501");
		agent.setPhoneNo("9111178184389");
		agent.setMobileNo("9197178184389");
		agent.setEmail("agent.user@gmail.com");
		agent.setCompanyId("fjsdfdsfsd");
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(agent);
		
	    MockMultipartFile agentPart = new MockMultipartFile("agent", "blob", "application/json", json.getBytes());
	    
		mvc.perform(fileUpload("/agents/new")
				.file(agentPart)
				.contentType(MediaType.MULTIPART_FORM_DATA)
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
}
