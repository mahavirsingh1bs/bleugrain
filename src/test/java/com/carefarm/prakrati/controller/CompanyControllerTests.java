package com.carefarm.prakrati.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.config.SearchIntegrationTest;
import com.carefarm.prakrati.search.document.CompanyDoc;
import com.carefarm.prakrati.web.model.CompanyForm;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CompanyControllerTests extends SearchIntegrationTest {

	@Autowired
	private WebApplicationContext context;
	
	private DomainUsernamePasswordAuthenticationToken authentication;
	
	private MockMvc mvc;
	
	@Before
	public void setup() {
		createIndex(CompanyDoc.class);
		authentication = new DomainUsernamePasswordAuthenticationToken("admin@vocac.com", "prak1234", null);
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}
	
	@Test
	public void testThatCompanyHasBeenAddedSuccessfully() throws Exception {
		
		CompanyForm company = new CompanyForm();
		company.setName("Prakrati Agro Pvt. Ltd.");
		company.setRegistrNo("4572532377");
		company.setEmail("contact@prakrati.com");
		company.setPhoneNo("911248373872");
		company.setFirstName("Company");
		company.setMiddleInitial("N");
		company.setLastName("Admin");
		company.setAddressLine1("Village-Pali, Post-Bharna Kalan");
		company.setAddressLine2("District-Mathura");
		company.setCity("Goverdhan");
		company.setStateId(1L);
		company.setCountryId(1L);
		company.setZipcode("281501");
		company.setMobileNo("9197178184389");
		company.setUserEmail("company.admin@gmail.com");
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(company);
		
	    MockMultipartFile companyPart = new MockMultipartFile("company", "blob", "application/json", json.getBytes());
	    
		mvc.perform(fileUpload("/companies/new")
				.file(companyPart)
				.contentType(MediaType.MULTIPART_FORM_DATA)
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
}
