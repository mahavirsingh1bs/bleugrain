package com.carefarm.prakrati.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.config.SearchIntegrationTest;
import com.carefarm.prakrati.search.document.UserDoc;
import com.carefarm.prakrati.web.model.UserForm;
import com.fasterxml.jackson.databind.ObjectMapper;

public class RetailerControllerTests extends SearchIntegrationTest {

	@Autowired
	private WebApplicationContext context;
	
	private DomainUsernamePasswordAuthenticationToken authentication;
	
	private MockMvc mvc;
	
	@Before
	public void setup() {
		createIndex(UserDoc.class);
		authentication = new DomainUsernamePasswordAuthenticationToken("admin@vocac.com", "prak1234", null);
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}
	
	@Test
	public void testThatRetailerHasBeenAddedSuccessfully() throws Exception {
		
		UserForm retailer = new UserForm();
		retailer.setFirstName("Mahavir");
		retailer.setMiddleInitial("N");
		retailer.setLastName("Singh");
		retailer.setAddressLine1("Village-Pali, Post-Bharna Kalan");
		retailer.setAddressLine2("District-Mathura");
		retailer.setCity("Goverdhan");
		retailer.setStateId(1L);
		retailer.setCountryId(1L);
		retailer.setZipcode("281501");
		retailer.setPhoneNo("9111178184389");
		retailer.setMobileNo("9197178184389");
		retailer.setEmail("retailer.user@gmail.com");
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(retailer);
		
	    MockMultipartFile retailerPart = new MockMultipartFile("retailer", "blob", "application/json", json.getBytes());
	    
		mvc.perform(fileUpload("/retailers/new")
				.file(retailerPart)
				.contentType(MediaType.MULTIPART_FORM_DATA)
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
}
