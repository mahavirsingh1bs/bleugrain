package com.carefarm.prakrati.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.config.SearchIntegrationTest;
import com.carefarm.prakrati.search.document.UserDoc;
import com.carefarm.prakrati.web.model.BrokerForm;
import com.fasterxml.jackson.databind.ObjectMapper;

public class BrokerControllerTests extends SearchIntegrationTest {
	
	@Autowired
	private WebApplicationContext context;
	
	private DomainUsernamePasswordAuthenticationToken authentication;
	
	private MockMvc mvc;
	
	@Before
	public void setup() {
		createIndex(UserDoc.class);
		authentication = new DomainUsernamePasswordAuthenticationToken("admin@vocac.com", "prak1234", null);
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}
	
	@Test
	public void testThatBrokerHasBeenAddedSuccessfully() throws Exception {
		
		BrokerForm broker = new BrokerForm();
		broker.setFirstName("Mahavir");
		broker.setMiddleInitial("N");
		broker.setLastName("Singh");
		broker.setAddressLine1("Village-Pali, Post-Bharna Kalan");
		broker.setAddressLine2("District-Mathura");
		broker.setCity("Goverdhan");
		broker.setStateId(1L);
		broker.setCountryId(1L);
		broker.setZipcode("281501");
		broker.setPhoneNo("9111178184389");
		broker.setMobileNo("9197178184389");
		broker.setEmail("broker.user@gmail.com");
		broker.setTransportAvailable(Boolean.TRUE);
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(broker);
		
	    MockMultipartFile brokerPart = new MockMultipartFile("broker", "blob", "application/json", json.getBytes());
	    
		mvc.perform(fileUpload("/brokers/new")
				.file(brokerPart)
				.contentType(MediaType.MULTIPART_FORM_DATA)
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
}
