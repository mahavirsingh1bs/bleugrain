package com.carefarm.prakrati.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.config.SearchIntegrationTest;
import com.carefarm.prakrati.search.document.DemandDoc;
import com.carefarm.prakrati.web.model.AddressForm;
import com.carefarm.prakrati.web.model.DemandForm;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DemandControllerTests extends SearchIntegrationTest {
	
	@Autowired
	private WebApplicationContext context;
	
	private DomainUsernamePasswordAuthenticationToken authentication;
	
	private MockMvc mvc;
	
	@Before
	public void setup() {
		createIndex(DemandDoc.class);
		authentication = new DomainUsernamePasswordAuthenticationToken("rjenith@vocac.com", "prak1234", null);
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}
	
	@Test
	public void testThatDemandHasBeenAddedSuccessfully() throws Exception {
		
		AddressForm deliveryAddress = new AddressForm();
		deliveryAddress.setAddressLine1("Village-Pali, Post-Bharna Kalan");
		deliveryAddress.setAddressLine2("District-Mathura");
		deliveryAddress.setCity("Goverdhan");
		deliveryAddress.setStateId(1L);
		deliveryAddress.setCountryId(1L);
		deliveryAddress.setZipcode("281501");
		
		DemandForm demand = new DemandForm();
		demand.setName("Kashmiri Rice 81");
		demand.setCategoryId(UUID.randomUUID().toString());
		demand.setQuantity(BigDecimal.valueOf(230.0));
		demand.setQuantityUnitId(1L);
		demand.setPricePerUnit(BigDecimal.valueOf(2300.0));
		demand.setPriceCurrencyId(1L);
		demand.setPriceUnitId(1L);
		demand.setDeliveryDate("28/11/2016");
		demand.setDeliveryAddress(deliveryAddress);
		demand.setCompanyId("2714270627055427201");
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(demand);
		
	    MockMultipartFile demandPart = new MockMultipartFile("demand", "blob", "application/json", json.getBytes());
	    
		mvc.perform(fileUpload("/demands/new")
				.file(demandPart)
				.contentType(MediaType.MULTIPART_FORM_DATA)
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
}
