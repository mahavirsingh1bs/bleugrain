package com.carefarm.prakrati.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.config.SearchIntegrationTest;
import com.carefarm.prakrati.search.document.BankDoc;
import com.carefarm.prakrati.web.model.BankForm;
import com.fasterxml.jackson.databind.ObjectMapper;

public class BankControllerTests extends SearchIntegrationTest {

	@Autowired
	private WebApplicationContext context;
	
	private DomainUsernamePasswordAuthenticationToken authentication;
	
	private MockMvc mvc;
	
	@Before
	public void setup() {
		createIndex(BankDoc.class);
		authentication = new DomainUsernamePasswordAuthenticationToken("admin@vocac.com", "prak1234", null);
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}

	@Test
	public void testThatBankHasBeenAddedSuccessfully() throws Exception {
	
		BankForm bank = new BankForm();
		bank.setName("ICICI BANK");
		bank.setAuthURL("Patanjali is doing well");
		bank.setUsername("username");
		bank.setPassword("password");
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(bank);
		
		mvc.perform(post("/banks/new")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json)
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}

}
