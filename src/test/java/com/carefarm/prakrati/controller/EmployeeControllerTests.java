package com.carefarm.prakrati.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.config.SearchIntegrationTest;
import com.carefarm.prakrati.search.document.UserDoc;
import com.carefarm.prakrati.util.EmployeeGroup;
import com.carefarm.prakrati.web.model.EmployeeForm;
import com.fasterxml.jackson.databind.ObjectMapper;

public class EmployeeControllerTests extends SearchIntegrationTest {

	@Autowired
	private WebApplicationContext context;
	
	private DomainUsernamePasswordAuthenticationToken authentication;
	
	private MockMvc mvc;
	
	@Before
	public void setup() {
		createIndex(UserDoc.class);
		authentication = new DomainUsernamePasswordAuthenticationToken("admin@vocac.com", "prak1234", null);
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}
	
	@Test
	public void testThatEmployeeHasBeenAddedSuccessfully() throws Exception {
		
		EmployeeForm employee = new EmployeeForm();
		employee.setFirstName("Mahavir");
		employee.setMiddleInitial("N");
		employee.setLastName("Singh");
		employee.setAddressLine1("Village-Pali, Post-Bharna Kalan");
		employee.setAddressLine2("District-Mathura");
		employee.setCity("Goverdhan");
		employee.setStateId(1L);
		employee.setCountryId(1L);
		employee.setZipcode("281501");
		employee.setPhoneNo("9111178184389");
		employee.setMobileNo("9197178184389");
		employee.setEmail("employee.user@gmail.com");
		employee.setGroupId(15L);
		employee.setDesignationId(6L);
		employee.setDepartmentId(4L);
		employee.setDepartmentId(4L);
		employee.setEmployeeGroup(EmployeeGroup.EXECUTIVE);
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(employee);
		
	    MockMultipartFile employeePart = new MockMultipartFile("employee", "blob", "application/json", json.getBytes());
	    
		mvc.perform(fileUpload("/employees/new")
				.file(employeePart)
				.contentType(MediaType.MULTIPART_FORM_DATA)
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
}
