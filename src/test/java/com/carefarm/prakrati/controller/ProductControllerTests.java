package com.carefarm.prakrati.controller;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.config.SearchIntegrationTest;
import com.carefarm.prakrati.search.document.ProductDoc;
import com.carefarm.prakrati.web.bean.CategoryBean;
import com.carefarm.prakrati.web.bean.CurrencyBean;
import com.carefarm.prakrati.web.bean.TagBean;
import com.carefarm.prakrati.web.bean.UnitBean;
import com.carefarm.prakrati.web.model.EnableBiddingForm;
import com.carefarm.prakrati.web.model.OwnerForm;
import com.carefarm.prakrati.web.model.ProductForm;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ProductControllerTests extends SearchIntegrationTest {
	
	@Autowired
	private WebApplicationContext context;
	
	private MockMvc mvc;

	@Autowired
	private RestTemplate restTemplate;
	
	@Before
	public void setup() {
		createIndex(ProductDoc.class);
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}
	
	@Test
	public void testThatFindAllConsumerProductsInSpicesAndHerbsCategory() throws Exception {
		
	    DomainUsernamePasswordAuthenticationToken authentication = new DomainUsernamePasswordAuthenticationToken("admin@vocac.com", "prak1234", null);
	    MvcResult result = mvc.perform(get("/products/all")
	    		.param("page", "0")
	    		.param("pageSize", "10")
	    		.param("sortFields", "dateModified")
	    		.param("direction", "ASC")
	    		.param("consumerProducts", "true")
	    		.param("categoryId", "00c95745-29ba-413f-a117-ffdb87a99e5a")
	    		.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	    assertThat("Response is not null", result.getResponse(), is(notNullValue()));
	}
	
	@Test
	public void testThatFindAllMatchingProducts() throws Exception {
		
	    DomainUsernamePasswordAuthenticationToken authentication = new DomainUsernamePasswordAuthenticationToken("admin@vocac.com", "prak1234", null);
	    MvcResult result = mvc.perform(get("/products/all")
	    		.param("page", "0")
	    		.param("pageSize", "10")
	    		.param("sortFields", "dateModified")
	    		.param("direction", "ASC")
	    		.param("consumerProducts", "false")
	    		.param("keyword", "wheat")
	    		.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	    assertThat("Response is not null", result.getResponse(), is(notNullValue()));
	}
	
	@Test
	public void testThatFindAllMatchingProductsOfTheUser() throws Exception {
		
	    DomainUsernamePasswordAuthenticationToken authentication = new DomainUsernamePasswordAuthenticationToken("msingh@vocac.com", "prak1234", null);
	    MvcResult result = mvc.perform(get("/products/user")
	    		.param("page", "0")
	    		.param("pageSize", "10")
	    		.param("sortFields", "dateModified")
	    		.param("direction", "ASC")
	    		.param("consumerProducts", "false")
	    		.param("keyword", "wheat")
	    		.param("userNo", "2714270627055727201")
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	    assertThat("Response is not null", result.getResponse(), is(notNullValue()));
	}

	@Test
	public void testFindAllMatchingHistoryProductsOfTheUser() throws Exception {
		
	    DomainUsernamePasswordAuthenticationToken authentication = new DomainUsernamePasswordAuthenticationToken("msingh@vocac.com", "prak1234", null);
	    MvcResult result = mvc.perform(get("/products/user")
	    		.param("page", "0")
	    		.param("pageSize", "10")
	    		.param("sortFields", "dateModified")
	    		.param("direction", "ASC")
	    		.param("consumerProducts", "false")
	    		.param("keyword", "wheat")
	    		.param("userNo", "2714270627055727201")
	    		.param("history", "true")
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	    assertThat("Response is not null", result.getResponse(), is(notNullValue()));
	}
	
	@Test
	public void testThatFindAllBiddableProducts() throws Exception {
		String uniqueId = "539a69b0-63c9-49de-a995-f64ef7ea0987";
		String stateId = "-1";
		String page = "0";
		String pageSize = "10";
		
		DomainUsernamePasswordAuthenticationToken authentication = new DomainUsernamePasswordAuthenticationToken("rjenith@vocac.com", "prak1234", null);
		MvcResult result = mvc.perform(get("/products/biddable")
	    		.param("uniqueId", uniqueId)
	    		.param("stateId", stateId)
	    		.param("page", page)
	    		.param("pageSize", pageSize)
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	    assertThat("Response is not null", result.getResponse(), is(notNullValue()));
	}
	
	@Test
	public void testThatProductDetailIsWorkingAsExpected() throws Exception {
		
		String uniqueId = "65f34cc8-fe88-43ec-bd8a-d25c9a92d4a1";
		
		DomainUsernamePasswordAuthenticationToken authentication = new DomainUsernamePasswordAuthenticationToken("msingh@vocac.com", "prak1234", null);
	    MvcResult result = mvc.perform(get("/products/data/" + uniqueId)
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	    assertThat("Response is not null", result.getResponse(), is(notNullValue()));
	}
	
	@Test
	public void testThatProductHasBeenAddedSuccessfully() throws Exception {
		
		ProductForm productForm = new ProductForm();
		productForm.setName("Kashmiri Rice 81");
		productForm.setDesc("A Good Quality product.");
		productForm.setConsumerProduct(Boolean.FALSE);
		productForm.setPrice(BigDecimal.valueOf(2300.0));
		productForm.setPriceCurrencyId(2L);
		productForm.setPriceUnitId(1L);
		productForm.setQuantity(BigDecimal.valueOf(200.0));
		productForm.setQuantityUnitId(1L);
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(productForm);
		
	    MockMultipartFile product = new MockMultipartFile("product", "blob", "application/json", json.getBytes());
	    
	    DomainUsernamePasswordAuthenticationToken authentication = new DomainUsernamePasswordAuthenticationToken("msingh@vocac.com", "prak1234", null);
		
		mvc.perform(fileUpload("/products/new")
				.file(product)
				.contentType(MediaType.MULTIPART_FORM_DATA)
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
	@Test
	public void testThatConsumerProductHasBeenAddedSuccessfully() throws Exception {
		
		ProductForm productForm = new ProductForm();
		productForm.setName("Kashmiri Rice 81");
		productForm.setDesc("A Good Quality product.");
		productForm.setConsumerProduct(Boolean.TRUE);
		productForm.setPrice(BigDecimal.valueOf(2300.0));
		productForm.setPriceCurrencyId(2L);
		productForm.setPriceUnitId(1L);
		productForm.setQuantity(BigDecimal.valueOf(200.0));
		productForm.setQuantityUnitId(1L);
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(productForm);
		
	    MockMultipartFile product = new MockMultipartFile("product", "blob", "application/json", json.getBytes());
	    
        DomainUsernamePasswordAuthenticationToken authentication = new DomainUsernamePasswordAuthenticationToken("msingh@vocac.com", "prak1234", null);
		
		mvc.perform(fileUpload("/products/new")
				.file(product)
				.contentType(MediaType.MULTIPART_FORM_DATA)
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
	@Test
	public void testThatExistingUsersProductHasBeenAddedSuccessfullyByTheAdmin() throws Exception {
		
		List<TagBean> tags = new ArrayList<>();
		tags.add(new TagBean("Kashmiri"));
		
		List<CategoryBean> categories = new ArrayList<>();
		categories.add(new CategoryBean("Rice"));
		
		ProductForm productForm = new ProductForm();
		productForm.setName("Kashmiri Rice 81");
		productForm.setDesc("A Good Quality product.");
		productForm.setConsumerProduct(Boolean.FALSE);
		productForm.setPrice(BigDecimal.valueOf(2300.0));
		productForm.setPriceCurrencyId(2L);
		productForm.setPriceUnitId(1L);
		productForm.setQuantity(BigDecimal.valueOf(200.0));
		productForm.setQuantityUnitId(1L);
		productForm.setOwnerId("2714270627055727201");
		productForm.setTags(tags);
		productForm.setCategories(categories);		
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(productForm);
		
	    MockMultipartFile product = new MockMultipartFile("product", "blob", "application/json", json.getBytes());
	    
        DomainUsernamePasswordAuthenticationToken authentication = new DomainUsernamePasswordAuthenticationToken("msingh@vocac.com", "prak1234", null);
		
		mvc.perform(fileUpload("/products/new")
				.file(product)
				.contentType(MediaType.MULTIPART_FORM_DATA)
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
	@Test
	public void testThatNewUsersProductHasBeenAddedSuccessfullyByTheAdmin() throws Exception {
		
		OwnerForm owner = new OwnerForm();
		owner.setFirstName("Mahavir");
		owner.setMiddleInitial("N");
		owner.setLastName("Singh");
		owner.setAddressLine1("Village-Pali, Post-Bharna Kalan");
		owner.setAddressLine2("District-Mathura");
		owner.setCity("Goverdhan");
		owner.setStateId(1L);
		owner.setCountryId(1L);
		owner.setZipcode("281501");
		owner.setMobileNo("919717818493");
		owner.setEmail("owner@gmail.com");
		owner.setGroupId(2L);
		
		ProductForm productForm = new ProductForm();
		productForm.setName("Kashmiri Rice 81");
		productForm.setDesc("A Good Quality product.");
		productForm.setConsumerProduct(Boolean.FALSE);
		productForm.setPrice(BigDecimal.valueOf(2300.0));
		productForm.setPriceCurrencyId(2L);
		productForm.setPriceUnitId(1L);
		productForm.setQuantity(BigDecimal.valueOf(200.0));
		productForm.setQuantityUnitId(1L);
		productForm.setOwner(owner);
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(productForm);
		
	    MockMultipartFile product = new MockMultipartFile("product", "blob", "application/json", json.getBytes());
	    
        DomainUsernamePasswordAuthenticationToken authentication = new DomainUsernamePasswordAuthenticationToken("msingh@vocac.com", "prak1234", null);
		
		mvc.perform(fileUpload("/products/new")
				.file(product)
				.contentType(MediaType.MULTIPART_FORM_DATA)
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
	@Test
	public void testThatBiddingHasBeenEnabledSuccessfully() throws Exception {
		
		EnableBiddingForm enableBidding = new EnableBiddingForm();
		enableBidding.setInitialBidAmount(BigDecimal.valueOf(3400.0));
		enableBidding.setBiddingCurrency(new CurrencyBean(1L, "Rs."));
		enableBidding.setBiddingUnit(new UnitBean(1L, "Q"));
		enableBidding.setEnableTillDate("10/12/2016");
		enableBidding.setUniqueId("f4eb7252-68c8-4c9e-b4ac-b303dc034367");
		enableBidding.setUserNo("2714270627055427201");
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(enableBidding);
		
	    DomainUsernamePasswordAuthenticationToken authentication = new DomainUsernamePasswordAuthenticationToken("admin@vocac.com", "prak1234", null);
	    
	    mvc.perform(post("/products/enableBidding")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json)
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
}
