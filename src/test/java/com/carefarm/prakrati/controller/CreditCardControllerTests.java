package com.carefarm.prakrati.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.config.PrakratiIntegrationTest;
import com.carefarm.prakrati.web.model.CreditCardForm;
import com.fasterxml.jackson.databind.ObjectMapper;

public class CreditCardControllerTests extends PrakratiIntegrationTest {
	
	@Autowired
	private WebApplicationContext context;
	
	private DomainUsernamePasswordAuthenticationToken authentication;
	
	private MockMvc mvc;
	
	@Before
	public void setup() {
		authentication = new DomainUsernamePasswordAuthenticationToken("wbroker1@vocac.com", "prak1234", null);
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}
	
	@Test
	public void testThatCreditCardHasBeenAddedSuccessfully() throws Exception {
	
		CreditCardForm creditCard = new CreditCardForm();
		creditCard.setPaymentCardId(1L);
		creditCard.setNumber("8237743623547463");
		creditCard.setOwner("MAHAVIR SINGH");
		creditCard.setExpiryMonthId(1L);
		creditCard.setExpiryYearId(2L);
		creditCard.setDefaultBilling(Boolean.TRUE);
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(creditCard);
		
		mvc.perform(post("/profile/addCreditCard?userNo=2714270627057027201")
				.contentType(MediaType.APPLICATION_JSON)
				.content(json)
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
}
