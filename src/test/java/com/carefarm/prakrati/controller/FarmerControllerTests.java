package com.carefarm.prakrati.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.config.SearchIntegrationTest;
import com.carefarm.prakrati.search.document.UserDoc;
import com.carefarm.prakrati.web.model.FarmerForm;
import com.fasterxml.jackson.databind.ObjectMapper;

public class FarmerControllerTests extends SearchIntegrationTest {

	@Autowired
	private WebApplicationContext context;
	
	private DomainUsernamePasswordAuthenticationToken authentication;
	
	private MockMvc mvc;
	
	@Before
	public void setup() {
		createIndex(UserDoc.class);
		authentication = new DomainUsernamePasswordAuthenticationToken("admin@vocac.com", "prak1234", null);
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}
	
	@Test
	public void testThatFarmerHasBeenAddedSuccessfully() throws Exception {
		
		FarmerForm farmer = new FarmerForm();
		farmer.setFirstName("Mahavir");
		farmer.setMiddleInitial("N");
		farmer.setLastName("Singh");
		farmer.setAddressLine1("Village-Pali, Post-Bharna Kalan");
		farmer.setAddressLine2("District-Mathura");
		farmer.setCity("Goverdhan");
		farmer.setStateId(1L);
		farmer.setCountryId(1L);
		farmer.setZipcode("281501");
		farmer.setMobileNo("919717818493");
		farmer.setEmail("farmer.user@gmail.com");
		farmer.setTransportAvailable(Boolean.TRUE);
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(farmer);
		
	    MockMultipartFile farmerPart = new MockMultipartFile("farmer", "blob", "application/json", json.getBytes());
	    
		mvc.perform(fileUpload("/farmers/new")
				.file(farmerPart)
				.contentType(MediaType.MULTIPART_FORM_DATA)
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
}
