package com.carefarm.prakrati.util;

import static org.mockito.Mockito.mock;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.dozer.CustomConverter;
import org.dozer.DozerBeanMapper;

import com.carefarm.prakrati.converters.PaymentDecryptor;
import com.carefarm.prakrati.converters.PaymentEncryptor;

public class ObjectFactory {

	public static Validator createValidator() {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		Validator validator = factory.getValidator();
		return validator;
	}
	
	public static DozerBeanMapper dozerBeanMapper() {
		Map<String, CustomConverter> customConvertersWithId = new HashMap<>();
		customConvertersWithId.put("paymentEncryptor", mock(PaymentEncryptor.class));
		customConvertersWithId.put("paymentDecryptor", mock(PaymentDecryptor.class));
		DozerBeanMapper dozerBeanMapper = new DozerBeanMapper();
		List<String> mappingFiles = new ArrayList<>();
		mappingFiles.add("mapping/dozer-bean-mappings.xml");
		dozerBeanMapper.setMappingFiles(mappingFiles);
		dozerBeanMapper.setCustomConvertersWithId(customConvertersWithId);
		return dozerBeanMapper;
	}
	
}
