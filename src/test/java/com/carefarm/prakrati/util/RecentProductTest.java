package com.carefarm.prakrati.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

import org.junit.Test;

import com.carefarm.prakrati.core.Environment;

public class RecentProductTest {
	
	@Test
	public void testIsRecent() {
		Date dateCreated = Date.from(Environment.clock().instant().minus(49, ChronoUnit.DAYS));
		String pattern = "dd/MM/yyyy";
		DateFormat dateFormat = new SimpleDateFormat(pattern);
		LocalDate currentDate = LocalDate.now();
		
		DateTimeFormatter indianFormatter = DateTimeFormatter.ofPattern(pattern);
		LocalDate createdDate = LocalDate.parse(dateFormat.format(dateCreated), indianFormatter);
		
		long numberOfDays = ChronoUnit.DAYS.between(createdDate, currentDate);
		System.out.println(numberOfDays);
	}
}
