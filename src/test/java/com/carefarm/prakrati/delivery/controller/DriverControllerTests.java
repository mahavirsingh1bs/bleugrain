package com.carefarm.prakrati.delivery.controller;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.authentication;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.carefarm.prakrati.authentication.DomainUsernamePasswordAuthenticationToken;
import com.carefarm.prakrati.config.SearchIntegrationTest;
import com.carefarm.prakrati.search.document.UserDoc;
import com.carefarm.prakrati.web.model.UserForm;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DriverControllerTests extends SearchIntegrationTest {
	
	@Autowired
	private WebApplicationContext context;
	
	private DomainUsernamePasswordAuthenticationToken authentication;
	
	private MockMvc mvc;
	
	@Before
	public void setup() {
		createIndex(UserDoc.class);
		authentication = new DomainUsernamePasswordAuthenticationToken("admin@vocac.com", "prak1234", null);
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.apply(springSecurity())
				.build();
	}
	
	@Test
	public void testThatDriverHasBeenAddedSuccessfully() throws Exception {
		
		UserForm driver = new UserForm();
		driver.setFirstName("Mahavir");
		driver.setMiddleInitial("N");
		driver.setLastName("Singh");
		driver.setAddressLine1("Village-Pali, Post-Bharna Kalan");
		driver.setAddressLine2("District-Mathura");
		driver.setCity("Goverdhan");
		driver.setStateId(1L);
		driver.setCountryId(1L);
		driver.setZipcode("281501");
		driver.setMobileNo("919717818493");
		driver.setEmail("driver.user@gmail.com");
		
	    ObjectMapper mapper = new ObjectMapper();
	    String json = mapper.writeValueAsString(driver);
		
		mvc.perform(post("/drivers/new")
				.content(json)
				.contentType(MediaType.APPLICATION_JSON)
				.with(csrf().asHeader())
				.with(authentication(authentication)))
	            .andExpect(status().isOk())
	            .andReturn();
	}
	
}
