package com.carefarm.prakrati.repository;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.time.Clock;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.config.PrakratiIntegrationTest;
import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.entity.Notification;
import com.carefarm.prakrati.repository.NotificationCategoryRepository;
import com.carefarm.prakrati.repository.NotificationRepository;
import com.carefarm.prakrati.repository.UserRepository;

public class NotificationRepositoryTests extends PrakratiIntegrationTest {

	@Autowired
	private NotificationRepository notificationRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private NotificationCategoryRepository categoryRepository;
	
	@Test
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void testSendNotification() {
		
		String emailAddress = "msingh@vocac.com";
		String title = "Please verify your mobile number";
		
		
		Clock clock = Environment.clock();
		Notification notification = new Notification();
		notification.setCategory(categoryRepository.findOne(1L));
		notification.setTitle(title);
		notification.setStartDate(Date.from(clock.instant()));
		notification.setExpiryDate(Date.from(clock.instant()));
		notification.addUser(userRepository.findByEmailAddress(emailAddress));
		
		notificationRepository.save(notification);
	}
	
	@Test
	public void testFindByGroupIdAndUserNo() {
		String groupId = "7527ca3b-3c16-4cd6-bda1-53ceca0f8ba5";
		String userNo = "2714270627055727201";
		
		Clock clock = Environment.clock();
		
		List<Notification> notifications = notificationRepository.findByGroupIdAndUserNo(groupId, userNo, Date.from(clock.instant()));
		
		assertThat("There should be 0 notifications", notifications.size(), is(equalTo(0)));
	}
}
