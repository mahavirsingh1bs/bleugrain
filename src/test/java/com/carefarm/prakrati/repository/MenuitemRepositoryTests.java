package com.carefarm.prakrati.repository;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.jackrabbit.uuid.UUID;
import org.dozer.DozerBeanMapper;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.comparator.DisplayIndexComparator;
import com.carefarm.prakrati.config.PrakratiIntegrationTest;
import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.entity.Menuitem;
import com.carefarm.prakrati.web.bean.MenuitemBean;

public class MenuitemRepositoryTests extends PrakratiIntegrationTest {

	@Autowired
	private MenuitemRepository menuitemRepository;
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	protected DozerBeanMapper mapper;
	
	@Test
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void testSaveMenuitem() {
		
		Menuitem menuitem = new Menuitem();
		menuitem.setUniqueId(UUID.randomUUID().toString());
		menuitem.setName("Cereals & Pulses");
		menuitem.setCategory(categoryRepository.findOne(1L));
		menuitem.setCreatedBy(userRepository.findOne(1L));
		menuitem.setDateCreated(Date.from(Environment.clock().instant()));
		
		menuitemRepository.save(menuitem);
	}
	
	@Test
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	public void testFindMainMenuitems() {
		List<Menuitem> mainMenuitems = menuitemRepository.findMainMenuitems(Boolean.TRUE);
		assertThat("Total top menus should be 6", mainMenuitems.size(), is(9));
		List<MenuitemBean> menuitems = new ArrayList<>();
		for (Menuitem menuitem : mainMenuitems) {
			MenuitemBean menuitemBean = mapper.map(menuitem, MenuitemBean.class);
			
			Collections.sort(menuitem.getChildMenuitems(), new DisplayIndexComparator());
			for (Menuitem childMenuitem : menuitem.getChildMenuitems()) {
				createNode(menuitemBean, childMenuitem);
			}
			menuitems.add(menuitemBean);
		}
		assertThat("Cereals & Pulses should have two child menuitems", mainMenuitems.get(0).getChildMenuitems().size(), is(2));
		assertThat("Fruits should have two child menuitems", mainMenuitems.get(2).getChildMenuitems().size(), is(2));
	}
	
	public void createNode(MenuitemBean parentMenuitemBean, Menuitem menuitem) {
		MenuitemBean menuitemBean = mapper.map(menuitem, MenuitemBean.class);
		menuitemBean.setParentMenuitem(parentMenuitemBean);
		parentMenuitemBean.addChildMenuitem(menuitemBean);
		
		Collections.sort(menuitem.getChildMenuitems(), new DisplayIndexComparator());
		for (Menuitem childMenuitem : menuitem.getChildMenuitems()) {
			createNode(menuitemBean, childMenuitem);
		}
		
	}
	
}
