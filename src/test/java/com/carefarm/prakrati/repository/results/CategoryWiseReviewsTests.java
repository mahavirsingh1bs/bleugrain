package com.carefarm.prakrati.repository.results;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.math.BigDecimal;

import org.junit.Test;

public class CategoryWiseReviewsTests {

	@Test
	public void testCategoryWiseReviews() {
		CategoryWiseReviews reviews = new CategoryWiseReviews(1L, "Cereals & Pulses", 54L, BigDecimal.valueOf(46.5));
		assertThat("Category Id should not be null", reviews.getId(), is(notNullValue()));
		assertThat("Category Name should not be null", reviews.getName(), is(notNullValue()));
		assertThat("Total Reviews should not be null", reviews.getTotalReviews(), is(notNullValue()));
		assertThat("Total Rating should not be null", reviews.getTotalRating(), is(notNullValue()));
		assertThat("Rating Percent is not correct", reviews.getRatingPercent(), is(17));
	}
	
}
