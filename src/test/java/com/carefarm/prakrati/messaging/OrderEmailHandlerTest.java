package com.carefarm.prakrati.messaging;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.carefarm.prakrati.config.PrakratiIntegrationTest;
import com.carefarm.prakrati.repository.OrderRepository;
import com.carefarm.prakrati.repository.ProductRepository;
import com.carefarm.prakrati.web.bean.AddressEntityBean;
import com.carefarm.prakrati.web.bean.OrderBean;
import com.carefarm.prakrati.web.bean.ProductBean;
import com.carefarm.prakrati.web.bean.UserBean;

public class OrderEmailHandlerTest extends PrakratiIntegrationTest {
	
	@Autowired
	private OrderEmailHandler emailHandler;

	@Autowired
	private OrderRepository orderRepository;
	
	@Autowired
	private ProductRepository productRepository;
	
	private OrderBean order;
	private UserBean createdBy;
	private UserBean deliverBy;
	
	@Before
	public void setUp() {
		String administratorEmailAddress = "mahavirsingh1bs@gmail.com";
		String dispatcherEmailAddress = "wdispatcher@vocac.com";
		
		order = new OrderBean();
		order.setOrderNo("43294329432848");
		order.setAmountPaid(BigDecimal.valueOf(1000.0));
		order.setSubtotal("12000.0");
		order.setShippingCost("1300.0");
		order.setTotalPrice("12300.0");
		order.setBillingAddress(mapper.map(addressRepository.findOne(1L), AddressEntityBean.class));
		order.setShippingAddress(mapper.map(addressRepository.findOne(5L), AddressEntityBean.class));
		
		List<ProductBean> products = new ArrayList<>();
		ProductBean product1 = mapper.map(productRepository.findByUniqueId("f4eb7252-68c8-4c9e-b4ac-b303dc034367"), ProductBean.class);
		ProductBean product2 = mapper.map(productRepository.findByUniqueId("ba7c5b0c-297a-4c88-9b45-dc6b7d937ee9"), ProductBean.class);
		products.add(product1); products.add(product2);
		order.setProducts(products);
		
		createdBy = mapper.map(userRepository.findByEmailAddress(administratorEmailAddress), UserBean.class);
		order.setCreatedBy(createdBy);
		order.setCreatedOn("2017/04/13");
		
		deliverBy = mapper.map(userRepository.findByEmailAddress(dispatcherEmailAddress), UserBean.class);
		order.setDeliverBy(deliverBy);
		order.setDeliveryDate("2017/04/17");
	}

	@Test
	@Transactional
	public void testOrderHasPlacedEmail() {
		emailHandler.sendOrderHasPlaced(order, createdBy);
	}
	
	@Test
	@Transactional
	public void testOrderHasShippedEmail() {
		emailHandler.sendOrderHasShipped(order, createdBy);
	}

	@Test
	@Transactional
	public void testOrderHasDispatchedEmail() {
		emailHandler.sendOrderHasDispatched(order, createdBy);
	}
	
	@Test
	@Transactional
	public void testOrderHasAssignedEmail() {
		emailHandler.sendOrderHasAssigned(order, createdBy);
	}
	
	@Test
	@Transactional
	public void testOrderHasDeliveredEmail() {
		emailHandler.sendOrderHasDelivered(order, createdBy);
	}
	
}
