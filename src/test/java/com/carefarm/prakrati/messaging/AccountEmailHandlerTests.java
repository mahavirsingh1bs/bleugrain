package com.carefarm.prakrati.messaging;

import java.io.IOException;

import javax.mail.MessagingException;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import com.carefarm.prakrati.config.PrakratiIntegrationTest;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.repository.UserRepository;

import freemarker.template.TemplateException;

public class AccountEmailHandlerTests extends PrakratiIntegrationTest {

	@Autowired
	private AccountEmailHandler emailHandler;
	
	@Autowired
	private JavaMailSenderImpl mailSender;
	
	@Autowired
	private UserRepository userRepository;
	
	@Test
	public void testSendUserConcern() {
		String emailAddress = "mahavirsingh1bs@gmail.com";
		String concern = "Your product quality was not good.";
		emailHandler.sendUserConcern(emailAddress, concern);
	}
	
	@Test
	public void testSendFeedbackThanks() {
		String emailAddress = "mahavirsingh1bs@gmail.com";
		emailHandler.sendFeedbackThanks(emailAddress);
	}

	@Test
	public void testSendResetPassword() throws MessagingException, IOException {
		String emailAddress = "mahavirsingh1bs@gmail.com";
		String passwdResetUrl = "passwdResetUrl";
		User user = userRepository.findByEmailAddress(emailAddress);
		
		emailHandler.sendResetPassword(emailAddress, passwdResetUrl, user);
	}

	@Test
	public void testNotifyOnSuccessfulReset() throws MessagingException, IOException, TemplateException {
		String emailAddress = "mahavirsingh1bs@gmail.com";
		User user = userRepository.findByEmailAddress(emailAddress);
		
		emailHandler.notifyOnSuccessfulReset(emailAddress, user);
	}
	
	@Test
	public void testNotifyOnSuccessfulChange() throws MessagingException, IOException, TemplateException {
		String emailAddress = "mahavirsingh1bs@gmail.com";
		emailHandler.notifyOnSuccessfulChange(emailAddress);
	}
	
	@Test
	public void testSuccessfulRegistrationEmail() throws MessagingException, IOException {
		String emailAddress = "mahavirsingh1bs@gmail.com";
		String contextPath = "http://localhost";
		User currentUser = userRepository.findByEmailAddress(emailAddress);
		
		emailHandler.verifyEmailAddress(emailAddress, contextPath, currentUser);
	}
	
	@Test
	public void testSendVerificationLink() {
		String emailAddress = "mahavirsingh1bs@gmail.com";
		String verifyEmailPath = "verifyEmailPath";
		User user = userRepository.findByEmailAddress(emailAddress);
		
		emailHandler.sendVerificationLink(emailAddress, verifyEmailPath, user);
	}
	
	@Test
	public void testSuccessfulAccountCreationEmail() throws MessagingException, IOException {
		String emailAddress = "mahavirsingh1bs@gmail.com";
		String verifyEmailPath = "verifyEmailPath";
		String resetPasswdPath = "resetPasswdPath";
		User currentUser = userRepository.findByEmailAddress(emailAddress);
		
		emailHandler.accountCreatedEmail(emailAddress, verifyEmailPath, resetPasswdPath, currentUser);
	}
	
}
