package com.carefarm.prakrati.messaging;

import java.io.IOException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;

import org.junit.Ignore;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

@Ignore
public class SMSSender {
	
	private static final String BASE_URL = "http://instantalerts.co/api/web/send";
	private static final String WORKING_KEY = "141t6c857zj21jww579v5cy0j15i7p5y5a7";
	private static final String SENDER_ID = "VOCACI";
	
	public SMSSender() { }
	
	public static void main(String...args) {
		String mobileNo = "918750851107";
		String message = "Congratulations! You have been successfully registered with us. Please verify your mobile number. Your unique PIN is 948372.";
		SMSSender smsSender = new SMSSender();
		try {
			smsSender.processSms(mobileNo, message);
		} catch (KeyManagementException | NoSuchAlgorithmException | IOException e) {
			e.printStackTrace();
		}
	}
	
	private String processSms(String mobileNo, String message)
			throws IOException, KeyManagementException, NoSuchAlgorithmException {
		UriComponents uri = UriComponentsBuilder.fromHttpUrl(BASE_URL)
			.queryParam("apikey", WORKING_KEY)
			.queryParam("sender", SENDER_ID)
			.queryParam("to", mobileNo)
			.queryParam("message", message)
			.build();
		RestTemplate restTemplate = new RestTemplate();
		String response = restTemplate.getForObject(uri.encode("UTF-8").toUri(), String.class);
		return response;
	}

}
