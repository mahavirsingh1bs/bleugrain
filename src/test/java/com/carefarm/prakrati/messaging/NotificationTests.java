package com.carefarm.prakrati.messaging;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.sql.Date;
import java.time.Clock;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import org.apache.camel.Produce;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;

import com.carefarm.prakrati.config.PrakratiIntegrationTest;
import com.carefarm.prakrati.core.Environment;
import com.carefarm.prakrati.entity.Notification;
import com.carefarm.prakrati.messaging.listener.NotificationListener;
import com.carefarm.prakrati.repository.NotificationRepository;
import com.carefarm.prakrati.repository.UserRepository;
import com.carefarm.prakrati.service.dto.NotificationDTO;
import com.carefarm.prakrati.util.NotificationCategory;

@Ignore
public class NotificationTests extends PrakratiIntegrationTest {

	private static final String ORDER_PLACED = "ORDER_PLACED";
	
	private List<Notification> notifications = null;
	private List<String> groups = null;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private NotificationRepository notificationRepository;
	
	@Produce(uri ="direct:prakrati.notifications")
	private NotificationListener notificationListener;
	
	public void setUp() {
		groups = new ArrayList<>();
		groups.add("Farmer");
		groups.add("Broker");
	}
	
	@Test
	@DirtiesContext
	public void testNotifications() throws Exception {
		Clock clock = Environment.clock();
		String notification = "Mahavir Singh has placed his order successfully";
		
		NotificationDTO notificationDTO = NotificationDTO.Builder.getInstance()
				.category(NotificationCategory.ORDER_PLACED)
				.groups(groups)
				.notification(notification)
				.currentUser(userRepository.findOne(1L))
				.startDate(Date.from(clock.instant()))
				.expiryDate(Date.from(clock.instant().plus(5L, ChronoUnit.DAYS)))
				.build();
		
		notifications = notificationRepository.findByCategoryCode(ORDER_PLACED);
		assertThat("Notifications should not be null", notifications, is(notNullValue()));
		assertThat("Size should be 4", notifications.size(), is(equalTo(4)));
		
		notificationListener.sendNotification(notificationDTO);
		
		notifications = notificationRepository.findByCategoryCode(ORDER_PLACED);
		assertThat("Notifications should not be null", notifications, is(notNullValue()));
		assertThat("Size should be 5", notifications.size(), is(equalTo(5)));
		assertThat("Notification is not correct", notifications.get(4).getNotification(), is(equalTo(notification)));
		
	}
}
