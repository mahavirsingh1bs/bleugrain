package com.carefarm.prakrati.messaging;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import com.carefarm.prakrati.entity.User;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

public class FreemarkerTest {

	public static void main(String...args) throws IOException, TemplateException {
		ResourceLoader resourceLoader = new DefaultResourceLoader();
		Resource resource = resourceLoader.getResource("classpath:static/assets/emails/img/freemarker");
		freemarker.template.Configuration cfg = new freemarker.template.Configuration(freemarker.template.Configuration.VERSION_2_3_25);
		cfg.setDirectoryForTemplateLoading(resource.getFile());
		cfg.setDefaultEncoding("UTF-8");
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
		cfg.setLogTemplateExceptions(false);
		
		Template template = cfg.getTemplate("resetPassword.ftl");
		
		Map<String, Object> model = new HashMap<>();
		User user = new User();
		user.setFirstName("Mahavir");
		user.setLastName("Singh");
		model.put("user", user);
		
		Writer consoleWriter = new OutputStreamWriter(System.out);
		template.process(model, consoleWriter);
		
	}
}
