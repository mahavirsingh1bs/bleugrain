package com.carefarm.prakrati.authentication;

import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.carefarm.prakrati.config.PrakratiIntegrationTest;

public class PrakratiAuthenticationTests extends PrakratiIntegrationTest {

	@Autowired
	private WebApplicationContext context;
	
	private MockMvc mvc;
	
	@Before
	public void setup() {
		mvc = MockMvcBuilders
				.webAppContextSetup(context)
				.build();
	}
	
	@Test
	public void testUserAuthentication() throws Exception {
		mvc.perform(formLogin("/login").user("msingh@vocac.com").password("prak1234"));
	}
	
}
