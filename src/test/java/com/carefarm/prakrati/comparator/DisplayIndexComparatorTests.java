package com.carefarm.prakrati.comparator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.carefarm.prakrati.entity.Menuitem;

import edu.emory.mathcs.backport.java.util.Collections;

public class DisplayIndexComparatorTests {
	
	@Test
	public void testCompareTo() {
		Menuitem menuitem1 = new Menuitem();
		menuitem1.setName("Cereals & Pulses");
		menuitem1.setDisplayIndex(5);
		
		Menuitem menuitem2 = new Menuitem();
		menuitem2.setName("Dry Fruits");
		menuitem2.setDisplayIndex(2);
		
		Menuitem menuitem3 = new Menuitem();
		menuitem3.setName("Fruits");
		menuitem3.setDisplayIndex(7);
		
		List<Menuitem> menuitems = new ArrayList<>();
		menuitems.add(menuitem1);
		menuitems.add(menuitem2);
		menuitems.add(menuitem3);
		
		Collections.sort(menuitems, new DisplayIndexComparator());
		System.out.println(menuitems);
	}
	
}
