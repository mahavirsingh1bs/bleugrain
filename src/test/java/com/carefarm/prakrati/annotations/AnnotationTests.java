package com.carefarm.prakrati.annotations;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.springframework.data.elasticsearch.annotations.Document;

import com.carefarm.prakrati.search.document.ProductDoc;

public class AnnotationTests {
	
	@Test
	public void testAnnotation() {
		Document document = ProductDoc.class.getAnnotation(Document.class);
		assertThat("Index name should be bleugrain", document.indexName(), is("bleugrain"));
		assertThat("Type should be products", document.type(), is("products"));
	}
	
}
