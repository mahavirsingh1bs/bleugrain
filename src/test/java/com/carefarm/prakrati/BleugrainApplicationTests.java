package com.carefarm.prakrati;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/*
import org.hibernate.search.jpa.FullTextEntityManager;
import org.hibernate.search.jpa.Search;
*/
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.carefarm.prakrati.config.DefaultConfig;
import com.carefarm.prakrati.config.JpaConfig;
import com.carefarm.prakrati.config.SecurityConfig;
import com.carefarm.prakrati.config.TestConfig;
import com.carefarm.prakrati.config.WebConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = { BleugrainApplication.class, DefaultConfig.class, WebConfig.class, SecurityConfig.class, JpaConfig.class, TestConfig.class })
@WebAppConfiguration
public class BleugrainApplicationTests {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Before
	public void setUp() {
		/*
		FullTextEntityManager fullTextEntityManager = Search.getFullTextEntityManager(entityManager);
		try {
			fullTextEntityManager.createIndexer().startAndWait();
			fullTextEntityManager.close();
		} catch (InterruptedException e) {
			e.printStackTrace();
		};
		*/
	}
	
	@Test
	public void contextLoads() {
		
	}

}
