package com.carefarm.prakrati.core.authority;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.Collection;

import org.junit.Test;
import org.springframework.security.core.GrantedAuthority;

import com.carefarm.prakrati.entity.Group;
import com.carefarm.prakrati.entity.Role;
import com.carefarm.prakrati.entity.User;

public class PrakratiUserAuthorityUtilsTests {
	
	@Test
	public void testCreateAuthorities() {
		Group group = new Group("Farmer");
		group.addRole(new Role("ROLE_USER"));
		group.addRole(new Role("ROLE_FARMER"));
		group.addRole(new Role("ROLE_SELLER"));
		User user = new User("Mahavir", "Singh", "msingh@vocac.com");
		user.setGroup(group);
		Collection<? extends GrantedAuthority> authorities = PrakratiUserAuthorityUtils.createAuthorities(user);
		assertThat("Total Authorities should be 3", authorities.size(), is(3));
	}
	
}
