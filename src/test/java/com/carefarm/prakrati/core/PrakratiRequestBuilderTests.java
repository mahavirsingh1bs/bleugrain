package com.carefarm.prakrati.core;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.apache.jackrabbit.uuid.UUID;
import org.junit.Test;
import org.springframework.data.domain.PageRequest;

import com.carefarm.prakrati.builders.PrakratiRequestBuilder;

public class PrakratiRequestBuilderTests {
	
	@Test
	public void testBuild() {
		Long companyId = 1L;
		Long userId = 1L;
		String uniqueId = UUID.randomUUID().toString();
		PrakratiRequest prakratiRequest = PrakratiRequestBuilder.getInstance()
				.setCompanyId(companyId)
				.setUserId(userId)
				.setPageable(new PageRequest(0, 10))
				.addParameter("uniqueId", uniqueId)
				.build();
		assertThat("User Id is not correct", prakratiRequest.getUserId(), is(1L));
		assertThat("Company Id is not correct", prakratiRequest.getCompanyId(), is(1L));
		assertThat("Pageable should not be null", prakratiRequest.getPageable(), is(notNullValue()));
		assertThat("UniqueId is not correct", prakratiRequest.getParameter("uniqueId"), is(uniqueId));
	}
	
}
