package com.carefarm.prakrati.config;

import org.dozer.DozerBeanMapper;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.carefarm.prakrati.BleugrainApplication;
import com.carefarm.prakrati.repository.AddressRepository;
import com.carefarm.prakrati.repository.UserRepository;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = { BleugrainApplication.class, DefaultConfig.class, WebConfig.class, MessagingConfig.class, SecurityConfig.class, JpaConfig.class, TestConfig.class })
@WebAppConfiguration
@ActiveProfiles("test")
public abstract class PrakratiIntegrationTest {
	
	@Autowired
	protected DozerBeanMapper mapper;
	
	@Autowired
	protected UserRepository userRepository;

	@Autowired
	protected AddressRepository addressRepository;
	
	@BeforeClass
	public static void setUpClass() {
		
	}
	
	@AfterClass
	public static void tearDownClass() {
		
	}
	
}
