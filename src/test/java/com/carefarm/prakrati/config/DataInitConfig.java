package com.carefarm.prakrati.config;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;

public class DataInitConfig {

	@Autowired
	private JpaVendorAdapter jpaVendorAdapter;
	
	@Bean
	@Primary
	@Profile("data-init")
	public DataSource dataSource() {
		DataSourceBuilder factory = DataSourceBuilder
	            .create()
	            .driverClassName("com.mysql.jdbc.Driver")
	            .url("jdbc:mysql://localhost:3306/prakrati_back")
	            .username("root")
	            .password("root");
		return factory.build();
	}
	
	@Bean
	@Profile("data-init")
	public EntityManagerFactory entityManagerFactory() {
		Properties jpaProperties = new Properties();
		jpaProperties.put("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		jpaProperties.put("hibernate.show_sql", true);
		jpaProperties.put("hibernate.hbm2ddl.auto", "create");
		jpaProperties.put("hibernate.cache.region.factory_class", "org.hibernate.cache.ehcache.EhCacheRegionFactory");
	
        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setDataSource(dataSource());
        factory.setPackagesToScan("com.carefarm.prakrati");
        factory.setJpaVendorAdapter(jpaVendorAdapter);
        factory.setPersistenceProvider(new HibernatePersistenceProvider());
        factory.setJpaProperties(jpaProperties);
        factory.setPersistenceUnitName("bleugrain-PU");
        factory.afterPropertiesSet();
        return factory.getObject();
	}
	
}
