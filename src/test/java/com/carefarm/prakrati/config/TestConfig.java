package com.carefarm.prakrati.config;

import static org.mockito.Mockito.mock;

import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import org.hibernate.jpa.HibernatePersistenceProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.web.client.RestTemplate;

import com.carefarm.prakrati.messaging.MobileMessanger;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TestConfig {

	@Autowired
	private JpaVendorAdapter jpaVendorAdapter;
	
	@Bean
	@Profile("test")
	public DataSource dataSource() {
		DataSourceBuilder factory = DataSourceBuilder
	            .create()
	            .driverClassName("org.hsqldb.jdbc.JDBCDriver")
	            .url("jdbc:hsqldb:mem:.;sql.syntax_mys=true")
	            //.url("jdbc:hsqldb:file:~/databases/prakrati;sql.syntax_mys=true")
	            .username("sa")
	            .password("");
		return factory.build();
	}
	
	@Bean
	@Profile("test")
	public EntityManagerFactory entityManagerFactory() {
		Properties jpaProperties = new Properties();
		jpaProperties.put("hibernate.dialect", "org.hibernate.dialect.HSQLDialect");
		jpaProperties.put("hibernate.show_sql", true);
		jpaProperties.put("hibernate.hbm2ddl.auto", "none");
		jpaProperties.put("hibernate.cache.region.factory_class", "org.hibernate.cache.ehcache.EhCacheRegionFactory");
	
        LocalContainerEntityManagerFactoryBean factory = new LocalContainerEntityManagerFactoryBean();
        factory.setDataSource(dataSource());
        factory.setPackagesToScan("com.carefarm.prakrati.entity", "com.carefarm.prakrati.common.entity", "com.carefarm.prakrati.delivery.entity", "com.carefarm.prakrati.payment.entity");
        factory.setJpaVendorAdapter(jpaVendorAdapter);
        factory.setPersistenceProvider(new HibernatePersistenceProvider());
        factory.setJpaProperties(jpaProperties);
        factory.setPersistenceUnitName("bleugrain-PU");
        factory.afterPropertiesSet();
        return factory.getObject();
	}
	
	@Bean
	public ObjectMapper mapper() {
		ObjectMapper mapper = new ObjectMapper();
	    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		return mapper;
	}
	
	@Bean
	public RestTemplate restTemplate() {
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate;
	}
	
	/*
	@Bean
	@Profile("test")
	public AccountEmailHandler accountEmailHandler() {
		return mock(AccountEmailHandler.class);
	}

	@Bean
	@Profile("test")
	public OrderEmailHandler orderEmailHandler() {
		return mock(OrderEmailHandler.class);
	}
	*/
	
	@Bean
	@Profile("test")
	public MobileMessanger mobileMessanger() {
		return mock(MobileMessanger.class);
	}
	
}
