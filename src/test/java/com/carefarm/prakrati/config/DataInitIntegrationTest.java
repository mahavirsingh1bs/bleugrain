package com.carefarm.prakrati.config;

import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import com.carefarm.prakrati.BleugrainApplication;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = { BleugrainApplication.class, DefaultConfig.class, WebConfig.class, SecurityConfig.class, JpaConfig.class, DataInitConfig.class })
@WebAppConfiguration
@ActiveProfiles("data-init")
public class DataInitIntegrationTest {

}