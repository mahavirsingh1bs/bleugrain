package com.carefarm.prakrati.config;

import java.io.IOException;
import java.nio.file.Files;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;

import com.carefarm.prakrati.search.document.AbstractDoc;

public class SearchIntegrationTest extends PrakratiIntegrationTest {

	private static final Logger LOGGER = LoggerFactory.getLogger(SearchIntegrationTest.class);
	
	@Autowired
	protected ElasticsearchTemplate elasticsearchTemplate;
	
	@Autowired
	protected ResourceLoader resourceLoader;
	
	protected void createIndices(Class<? extends AbstractDoc>[] clazzes) {
		for (Class<? extends AbstractDoc> clazz: clazzes) {
			createIndex(clazz);
		}
	}
	
	protected void createIndex(Class<? extends AbstractDoc> clazz) {
		try {
			Document annotation = (Document)clazz.getAnnotation(Document.class);
			Resource resource = resourceLoader.getResource("classpath:elasticsearch/" + annotation.type() + ".mapping");
			elasticsearchTemplate.deleteIndex(annotation.indexName());
			elasticsearchTemplate.createIndex(annotation.indexName());
			elasticsearchTemplate.putMapping(annotation.indexName(), annotation.type(), new String(Files.readAllBytes(resource.getFile().toPath())));
			elasticsearchTemplate.refresh(annotation.indexName());
		} catch (IOException e) {
			LOGGER.error("An error occured while trying to create index for class {}", clazz.getSimpleName());
		}
	}
	
}
