package com.carefarm.prakrati.web.util;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class SortByTests {
	
	@Test
	public void testSortBys() {
		assertThat("Sort By ASC code should be \"1\"", SortBy.ASC.getCode(), is(1));
		assertThat("Sort By ASC name should be \"asc\"", SortBy.ASC.getName(), is("asc"));
		assertThat("Sort By DESC code should be \"2\"", SortBy.DESC.getCode(), is(2));
		assertThat("Sort By DESC name should be \"desc\"", SortBy.DESC.getName(), is("desc"));
	}
	
}
