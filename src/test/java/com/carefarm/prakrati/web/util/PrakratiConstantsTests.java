package com.carefarm.prakrati.web.util;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.springframework.data.domain.Sort.Direction;

public class PrakratiConstantsTests {
	
	@Test
	public void testPrakratiConstants() {
		assertThat("Default page number should be \"0\"", PrakratiConstants.DEFAULT_PAGE_NO, is(0));
		assertThat("Default page size should be \"20\"", PrakratiConstants.DEFAULT_PAGE_SIZE, is(20));
		assertThat("Default direction should be \"DESC\"", PrakratiConstants.DEFAULT_DIRECTION, is(Direction.DESC));
		assertThat("Default sort column should be \"dateModified\"", PrakratiConstants.DEFAULT_SORT[0], is("dateModified"));
		assertThat("Demand status should be \"Open\"", PrakratiConstants.DEMAND_STATUS_OPEN, is("Open"));
		assertThat("Status should be \"Newly Added\"", PrakratiConstants.STATUS_ADDED_NEW, is("Newly Added"));
		assertThat("Status should be \"Sold Out\"", PrakratiConstants.SOLD_OUT_STATUS, is("Sold Out"));
	}
	
}
