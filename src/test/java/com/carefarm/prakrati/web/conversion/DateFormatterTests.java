package com.carefarm.prakrati.web.conversion;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;

import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.context.MessageSource;

import com.carefarm.prakrati.common.PrakratiTests;
import com.carefarm.prakrati.core.Environment;

public class DateFormatterTests extends PrakratiTests {

	@Test
	public void testDateFormatter() throws ParseException {
		MessageSource messageSource = Mockito.mock(MessageSource.class);
		when(messageSource.getMessage("date.format", null, Locale.US)).thenReturn("dd/MM/yyyy");
		DateFormatter formatter = new DateFormatter();
		
		this.setField("messageSource", formatter, messageSource);
		
		Date currentDate = Date.from(LocalDate.of(1986, 07, 28).atStartOfDay(Environment.zoneId()).toInstant());
		String formattedDate = formatter.print(currentDate, Locale.US);
		assertThat("Formatted Date is not correct", formattedDate, is("28/07/1986"));
		
		Date parsedDate = formatter.parse(formattedDate, Locale.US);
		assertThat("Parsed Date is not correct", parsedDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(), is(currentDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate()));
	}
	
}
