package com.carefarm.prakrati.factory;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.hamcrest.CoreMatchers;
import org.junit.Test;

import com.carefarm.prakrati.entity.Product;
import com.carefarm.prakrati.events.ProductAddEvent;
import com.carefarm.prakrati.events.router.DynamicRouter;

public class DynamicRouterFactoryTests {
	
	@Test
	public void testCreateProductAddEventRouter() {
		DynamicRouter<ProductAddEvent, Product> productAddEventRouter = 
				DynamicRouterFactory.getInstance().createProductAddEventRouter();
		assertThat("Product Add Event Router should not be null", productAddEventRouter, is(CoreMatchers.notNullValue()));
	}
	
}
