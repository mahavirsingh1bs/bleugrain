package com.carefarm.prakrati.holder;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;

import com.carefarm.prakrati.entity.Group;
import com.carefarm.prakrati.entity.Role;
import com.carefarm.prakrati.entity.User;

public class ThreadContextTests {
	
	@Test
	public void testThreadContext() throws InterruptedException {
		Group group = new Group("Farmer");
		group.addRole(new Role("ROLE_USER"));
		group.addRole(new Role("ROLE_FARMER"));
		group.addRole(new Role("ROLE_SELLER"));
		User user = new User("Mahavir", "Singh", "msingh@vocac.com");
		user.setGroup(group);
		
		ThreadContext.setCurrentUser(user);
		Thread.sleep(100);
		assertThat("CurrentUser should not be null", ThreadContext.getCurrentUser(), is(notNullValue()));
		System.out.println("Verified current User in main thread");
		
		Thread anotherThread = new Thread(() -> { 
			assertThat("CurrentUser should be null in another thread", ThreadContext.getCurrentUser(), is(nullValue()));
			System.out.println("Verified current User in another thread");
		});
		anotherThread.start();
		
	}
	
}
