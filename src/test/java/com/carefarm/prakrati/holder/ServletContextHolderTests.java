package com.carefarm.prakrati.holder;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

import javax.servlet.ServletContext;

import org.junit.Test;
import org.mockito.Mockito;

public class ServletContextHolderTests {

	@Test
	public void testHoldServletContext() {
		ServletContext servletContext = Mockito.mock(ServletContext.class);
		ServletContextHolder.holdServletContext(servletContext);
		assertThat("ServletContext should not be null", ServletContextHolder.getServletContext(), is(notNullValue()));
	}
	
	@Test
	public void testUnholdServletContext() {
		ServletContext servletContext = Mockito.mock(ServletContext.class);
		ServletContextHolder.holdServletContext(servletContext);
		assertThat("ServletContext should not be null", ServletContextHolder.getServletContext(), is(notNullValue()));
		ServletContextHolder.unholdServletContext();
		assertThat("ServletContext should be null", ServletContextHolder.getServletContext(), is(nullValue()));
	}
	
}
