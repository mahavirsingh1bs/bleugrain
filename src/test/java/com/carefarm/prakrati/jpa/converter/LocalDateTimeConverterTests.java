package com.carefarm.prakrati.jpa.converter;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.time.LocalDateTime;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import com.carefarm.prakrati.core.Environment;

public class LocalDateTimeConverterTests {

	private LocalDateTimeConverter converter;
	
	@Before
	public void setUp() {
		converter = new LocalDateTimeConverter();
	}
	
	@Test
	public void testConvertToDatabaseColumn() {
		LocalDateTime currentDate = LocalDateTime.now();
		assertThat("Able to convert LocalDateTime to Date", converter.convertToDatabaseColumn(currentDate), is(notNullValue()));
	}
	
	@Test
	public void testConvertToEntityAttribute() {
		Date currentDate = Date.from(Environment.clock().instant());
		assertThat("Able to convert Date to LocalDateTime", converter.convertToEntityAttribute(currentDate), is(notNullValue()));	
	}
	
}
