package com.carefarm.prakrati.jpa.converter;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.time.LocalTime;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

import com.carefarm.prakrati.core.Environment;

public class LocalTimeConverterTests {
	
	private LocalTimeConverter converter;
	
	@Before
	public void setUp() {
		converter = new LocalTimeConverter();
	}
	
	@Test
	public void testConvertToDatabaseColumn() {
		LocalTime currentDate = LocalTime.now();
		assertThat("Able to convert LocalTime to Date", converter.convertToDatabaseColumn(currentDate), is(notNullValue()));
	}
	
	@Test
	public void testConvertToEntityAttribute() {
		Date currentDate = Date.from(Environment.clock().instant());
		assertThat("Able to convert Date to LocalTime", converter.convertToEntityAttribute(currentDate), is(notNullValue()));	
	}
	
}
