/*package com.carefarm.prakrati.translator;

import java.util.EnumSet;

import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.tool.hbm2ddl.SchemaExport;
import org.hibernate.tool.hbm2ddl.Target;

public class Hibernate5SchemaTranslator {

	public static void main(String...args) {
		Configuration config = new Configuration();
		MetadataSources metadata = new MetadataSources(
			    new StandardServiceRegistryBuilder()
			        .applySetting("hibernate.dialect", "org.hibernate.dialect.H2Dialect")
			        .applySetting("javax.persistence.schema-generation-connection", null connection)
			        .build());

			// [...] adding annotated classes to metadata here...
			metadata.addPackage("com.carefarm.prakrati.entity");
			metadata.addPackage("com.carefarm.prakrati.common.entity");
			metadata.addPackage("com.carefarm.prakrati.delivery.entity");
			metadata.addPackage("com.carefarm.prakrati.payment.entity");

			SchemaExport export = new SchemaExport();
			export.create(Target.SCRIPT));
	}
	
}
*/