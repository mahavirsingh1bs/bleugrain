package com.carefarm.prakrati.service.helper;

import static org.hamcrest.CoreMatchers.startsWith;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.UUID;

import javax.imageio.ImageIO;

import org.fusesource.hawtbuf.ByteArrayInputStream;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mock.web.MockMultipartFile;

import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.carefarm.prakrati.config.PrakratiIntegrationTest;
import com.carefarm.prakrati.entity.Company;
import com.carefarm.prakrati.entity.Image;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.util.ImageType;

@Ignore
public class AmazonS3ClientTests extends PrakratiIntegrationTest {
	
	private static final String DEFAULT_FILE_PATH = "/home/msingh/Pictures/Screenshot.png";
	
	@Autowired
	private AmazonS3Client amazonS3Client;
	
	@Value("${cloud.aws.s3.bucket}")
	private String bucket;
	
	@Value("${cloud.aws.s3.bucket.prefix}")
	private String prefix;
	
	@Autowired
	private AmazonS3Helper amazonS3Helper;
	
	@Before
	public void setUp() { 
		
	}
	
	@Test
	@Ignore
	public void testSaveImage() throws IOException {
		BufferedImage image = ImageIO.read(new File(DEFAULT_FILE_PATH));
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		ImageIO.write(image, "png", os);
		byte[] buffer = os.toByteArray();
		InputStream is = new ByteArrayInputStream(buffer);
		ObjectMetadata meta = new ObjectMetadata();
		meta.setContentLength(buffer.length);
		meta.setContentType("image/png");
		amazonS3Client.putObject(new PutObjectRequest(bucket, "input/data/image.png", is, meta));
	}
	
	@Test
	public void testHandleImageOfProduct() throws IOException {
		BufferedImage image = ImageIO.read(new File(DEFAULT_FILE_PATH));
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		ImageIO.write(image, "png", os);
		byte[] buffer = os.toByteArray();
		MockMultipartFile firstFile = new MockMultipartFile("screenshot", "Screenshot.png", "image/png", buffer);
		Image savedImage = amazonS3Helper.handleImage(firstFile, ImageType.PRODUCTS, UUID.randomUUID().toString());
		assertThat("Filepath should start with " + prefix, savedImage.getFilepath(), startsWith(prefix));
		assertThat("Big Filepath should start with " + prefix, savedImage.getLargeFilepath(), startsWith(prefix));
		assertThat("Medium Filepath should start with " + prefix, savedImage.getMediumFilepath(), startsWith(prefix));
		assertThat("Small Filepath should start with " + prefix, savedImage.getSmallFilepath(), startsWith(prefix));
	}
	
	@Test
	public void testHandleImageOfUser() throws IOException {
		BufferedImage image = ImageIO.read(new File(DEFAULT_FILE_PATH));
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		ImageIO.write(image, "png", os);
		byte[] buffer = os.toByteArray();
		MockMultipartFile firstFile = new MockMultipartFile("screenshot", "Screenshot.png", "image/png", buffer);
		User user = mock(User.class);
		when(user.getUserNo()).thenReturn(UUID.randomUUID().toString());
		when(user.getFirstName()).thenReturn("Mahavir");
		Image savedImage = amazonS3Helper.handleImage(firstFile, ImageType.USERS, user);
		assertThat("Filepath should start with " + prefix, savedImage.getFilepath(), startsWith(prefix));
	}
	
	@Test
	public void testHandleImageOfCompany() throws IOException {
		BufferedImage image = ImageIO.read(new File(DEFAULT_FILE_PATH));
		ByteArrayOutputStream os = new ByteArrayOutputStream();
		ImageIO.write(image, "png", os);
		byte[] buffer = os.toByteArray();
		MockMultipartFile firstFile = new MockMultipartFile("screenshot", "Screenshot.png", "image/png", buffer);
		Company company = mock(Company.class);
		when(company.getUniqueId()).thenReturn(UUID.randomUUID().toString());
		when(company.getName()).thenReturn("Usher Agro");
		Image savedImage = amazonS3Helper.handleImage(firstFile, company);
		assertThat("Filepath should start with " + prefix, savedImage.getFilepath(), startsWith(prefix));
	}
	
	@After
	public void tearDown() { 
		
	}
}
