package com.carefarm.prakrati.service;

import org.junit.Before;

import com.carefarm.prakrati.common.PrakratiTests;
import com.carefarm.prakrati.service.impl.BidServiceImpl;

public class MenuitemServiceTests extends PrakratiTests {
	
	private BidService bidService;
	
	@Before
	public void setUp() {
		super.setUp();
		
		bidService = new BidServiceImpl();
		this.setField("bidRepository", bidService, bidRepository);
		this.setField("productRepository", bidService, productRepository);
		this.setField("bidHelper", bidService, bidHelper);
		this.setField("mapper", bidService, mapper);
	}
}
