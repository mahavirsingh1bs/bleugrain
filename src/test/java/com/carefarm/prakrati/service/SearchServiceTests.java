package com.carefarm.prakrati.service;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import org.jgroups.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.PageRequest;

import com.carefarm.prakrati.common.PrakratiTests;
import com.carefarm.prakrati.entity.Group;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.search.bean.SearchResult;
import com.carefarm.prakrati.search.dto.ModelMap;
import com.carefarm.prakrati.search.dto.SearchCriteria;
import com.carefarm.prakrati.search.repository.DemandSearchRepository;
import com.carefarm.prakrati.search.repository.ProductSearchRepository;
import com.carefarm.prakrati.service.impl.SearchServiceImpl;
import com.carefarm.prakrati.web.bean.DemandBean;
import com.carefarm.prakrati.web.bean.ProductBean;

public class SearchServiceTests extends PrakratiTests {
	
	private SearchService searchService;
	
	private ProductSearchRepository productSearchRepository;
	
	private DemandSearchRepository demandSearchRepository;
	
	@Before
	public void setUp() {
		super.setUp();
		searchService = new SearchServiceImpl();

		productSearchRepository = mock(ProductSearchRepository.class);
		demandSearchRepository = mock(DemandSearchRepository.class);
		
		this.setField("productSearchRepository", searchService, productSearchRepository);
		this.setField("demandSearchRepository", searchService, demandSearchRepository);
		
		this.setField("userRepository", searchService, userRepository);
		this.setField("companyRepository", searchService, companyRepository);
		this.setField("productRepository", searchService, productRepository);
		this.setField("demandRepository", searchService, demandRepository);
		
		this.setField("productEventHelper", searchService, productEventHelper);
		this.setField("mapper", searchService, mapper);
	}
	
	@Test
	public void testSearchProductsWhenMatchingProductsAreFound() {
		String uniqueId = UUID.randomUUID().toString();
		
		Group group = mockedGroup("Company Admin", "ROLE_USER", "ROLE_COMPANY_ADMIN");
		User currentUser = currentUser("rjenith@vocac.com", group);
		
		SearchCriteria criteria = new SearchCriteria();
		criteria.setCnsmrPrdt(Boolean.TRUE);
		criteria.setKeywords("Cereals & Pulses");
		criteria.setPageable(new PageRequest(0, 10));
		criteria.setUniqueId(uniqueId);
		criteria.setCurrentUser(currentUser);
		
		List<Long> results = mockedSearchResults(1L);
		SearchResult<Long> result = new SearchResult.Builder<Long>().results(results).totalHits(1L).build();
		
		when(productSearchRepository.findByKeywordContains(criteria)).thenReturn(result);
		when(productRepository.findAll(results)).thenReturn(mockedProducts());
		
		ModelMap<ProductBean> modelMap = searchService.search(criteria);
		
		assertThat("Result Size should be 1", modelMap.getResultSize(), is(1L));
		assertThat("Product name should be ", modelMap.getFirstResult().getName(), is("Paddy 1121"));
		
		verify(productSearchRepository).findByKeywordContains(criteria);
		verify(productRepository).findAll(results);
	}

	@Test
	public void testSearchProductsWhenMatchingProductsAreNotFound() {
		String uniqueId = UUID.randomUUID().toString();
		
		Group group = mockedGroup("Company Admin", "ROLE_USER", "ROLE_COMPANY_ADMIN");
		User currentUser = currentUser("rjenith@vocac.com", group);
		
		SearchCriteria criteria = new SearchCriteria();
		criteria.setCnsmrPrdt(Boolean.TRUE);
		criteria.setKeywords("Cereals & Pulses");
		criteria.setPageable(new PageRequest(0, 10));
		criteria.setUniqueId(uniqueId);
		criteria.setCurrentUser(currentUser);
		
		List<Long> results = mockedSearchResults();
		SearchResult<Long> result = new SearchResult.Builder<Long>().results(results).totalHits(0L).build();
		
		when(productSearchRepository.findByKeywordContains(criteria)).thenReturn(result);
		when(productRepository.findAll(results)).thenReturn(mockedNoProducts());
		
		ModelMap<ProductBean> modelMap = searchService.search(criteria);
		
		assertThat("Result Size should be 1", modelMap.getResultSize(), is(0L));
		assertThat("Results should be empty", modelMap.isEmptyResult(), is(true));
		
		verify(productSearchRepository).findByKeywordContains(criteria);
		verify(productRepository).findAll(results);
	}

	@Test
	public void testSearchDemandsWhenMatchingDemandsAreFound() {
		String uniqueId = UUID.randomUUID().toString();
		
		Group group = mockedGroup("Company Admin", "ROLE_USER", "ROLE_COMPANY_ADMIN");
		User currentUser = currentUser("rjenith@vocac.com", group);
		
		SearchCriteria criteria = new SearchCriteria();
		criteria.setKeywords("Cereals & Pulses");
		criteria.setPageable(new PageRequest(0, 10));
		criteria.setUniqueId(uniqueId);
		criteria.setCurrentUser(currentUser);
		
		List<Long> results = mockedSearchResults(1L);
		SearchResult<Long> result = new SearchResult.Builder<Long>().results(results).totalHits(1L).build();
		
		when(demandSearchRepository.findByKeywordContains(criteria)).thenReturn(result);
		when(demandRepository.findAll(results)).thenReturn(mockedDemands());
		
		ModelMap<DemandBean> modelMap = searchService.searchDemands(criteria);
		
		assertThat("Result Size should be 1", modelMap.getResultSize(), is(1L));
		assertThat("Result should not be empty", modelMap.isEmptyResult(), is(false));
		assertThat("Demand name should be ", modelMap.getFirstResult().getName(), is("Paddy 1121"));
		
		verify(demandSearchRepository).findByKeywordContains(criteria);
		verify(demandRepository).findAll(results);
	}
	
	@Test
	public void testSearchDemandsWhenMatchingDemandsAreNotFound() {
		String uniqueId = UUID.randomUUID().toString();
		
		Group group = mockedGroup("Company Admin", "ROLE_USER", "ROLE_COMPANY_ADMIN");
		User currentUser = currentUser("rjenith@vocac.com", group);
		
		SearchCriteria criteria = new SearchCriteria();
		criteria.setCnsmrPrdt(Boolean.TRUE);
		criteria.setKeywords("Cereals & Pulses");
		criteria.setPageable(new PageRequest(0, 10));
		criteria.setUniqueId(uniqueId);
		criteria.setCurrentUser(currentUser);
		
		List<Long> results = mockedSearchResults();
		SearchResult<Long> result = new SearchResult.Builder<Long>().results(results).totalHits(0L).build();
		
		when(demandSearchRepository.findByKeywordContains(criteria)).thenReturn(result);
		when(demandRepository.findAll(results)).thenReturn(mockedNoDemands());
		
		ModelMap<DemandBean> modelMap = searchService.searchDemands(criteria);
		
		assertThat("Result Size should be 1", modelMap.getResultSize(), is(0L));
		assertThat("Results should be empty", modelMap.isEmptyResult(), is(true));
		
		verify(demandSearchRepository).findByKeywordContains(criteria);
		verify(demandRepository).findAll(results);
	}
	
}
