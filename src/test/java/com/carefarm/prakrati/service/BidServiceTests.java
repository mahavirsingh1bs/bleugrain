package com.carefarm.prakrati.service;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.PageRequest;

import com.carefarm.prakrati.common.PrakratiTests;
import com.carefarm.prakrati.common.entity.Currency;
import com.carefarm.prakrati.common.entity.Unit;
import com.carefarm.prakrati.entity.Bid;
import com.carefarm.prakrati.entity.Company;
import com.carefarm.prakrati.entity.Group;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.generators.UserNoGenerator;
import com.carefarm.prakrati.service.impl.BidServiceImpl;
import com.carefarm.prakrati.web.bean.BidBean;
import com.carefarm.prakrati.web.bean.BidsRequest;
import com.carefarm.prakrati.web.model.BidForm;

public class BidServiceTests extends PrakratiTests {
	
	private BidService bidService;
	
	@Before
	public void setUp() {
		super.setUp();
		
		bidService = new BidServiceImpl();
		this.setField("bidRepository", bidService, bidRepository);
		this.setField("productRepository", bidService, productRepository);
		this.setField("bidHelper", bidService, bidHelper);
		this.setField("mapper", bidService, mapper);
	}
	
	@Test
	public void testFindByUniqueIdWhenTotalBidsAreZero() {
		String uniqueId = UUID.randomUUID().toString();
		
		BidsRequest bidsRequest = new BidsRequest();
		bidsRequest.setUniqueId(uniqueId);
		bidsRequest.setPage(0);
		bidsRequest.setPageSize(10);
		
		when(bidRepository.findByUniqueId(uniqueId, new PageRequest(0, 10))).thenReturn(new ArrayList<Bid>());
		List<BidBean> bids = bidService.findByUniqueId(bidsRequest);
		assertThat("My total bids should be 0", bids.size(), is(0));
	}
	
	@Test
	public void testFindByUniqueIdWhenTotalBidsAreMoreThanZero() {
		String uniqueId = UUID.randomUUID().toString();
		
		BidsRequest bidsRequest = new BidsRequest();
		bidsRequest.setUniqueId(uniqueId);
		bidsRequest.setPage(0);
		bidsRequest.setPageSize(10);
		
		when(bidRepository.findByUniqueId(uniqueId, new PageRequest(0, 10))).thenReturn(mockedBids());
		List<BidBean> bids = bidService.findByUniqueId(bidsRequest);
		assertThat("My total bids should be 1", bids.size(), is(1));
	}
	
	@Test
	public void testFindByUserWhenTotalBidsAreMoreThanZero() {
		String uniqueId = UUID.randomUUID().toString();
		String userNo = UserNoGenerator.generateUserNo();
		
		BidsRequest bidsRequest = new BidsRequest();
		bidsRequest.setUniqueId(uniqueId);
		bidsRequest.setUserNo(userNo);
		bidsRequest.setPage(0);
		bidsRequest.setPageSize(10);
		
		User user = new User("Mahavir", "Singh", "mahavir.singh@vocac.com");
		user.setUserNo(userNo);	
		
		when(productRepository.findProductsBiddedByUserAndCategory(userNo, uniqueId, new PageRequest(0, 10))).thenReturn(mockedProducts(user));
		List<BidBean> bids = bidService.findByUser(bidsRequest);
		assertThat("My total bids should be 1", bids.size(), is(1));
	}
	
	@Test
	public void testFindByCompanyWhenTotalBidsAreMoreThanZero() {
		String uniqueId = UUID.randomUUID().toString();
		String companyId = UUID.randomUUID().toString();
		
		BidsRequest bidsRequest = new BidsRequest();
		bidsRequest.setUniqueId(uniqueId);
		bidsRequest.setCompanyId(companyId);
		bidsRequest.setPage(0);
		bidsRequest.setPageSize(10);
		
		Company company = new Company("Usher Agro Pvt. Ltd.", "984327872");
		company.setUniqueId(companyId);	
		
		when(productRepository.findProductsBiddedByCompanyAndCategory(companyId, uniqueId, new PageRequest(0, 10))).thenReturn(mockedProducts(company));
		List<BidBean> bids = bidService.findByCompany(bidsRequest);
		assertThat("My total bids should be 1", bids.size(), is(1));
	}
	
	@Test
	public void testBidPlacement() {
		String userNo = UserNoGenerator.generateUserNo();
		String productId = UUID.randomUUID().toString();
		BidForm bidForm = new BidForm();
		bidForm.setPrice(BigDecimal.valueOf(3550.0));
		bidForm.setUniqueId(productId);
		bidForm.setUserNo(userNo);
		
		Group group = mockedGroup("Company Admin", "ROLE_USER", "ROLE_COMPANY_ADMIN");
		Company company = mockedCompany("Usher Agro Pvt Ltd", "948948323");
		User currentUser = currentUser("rjenith@vocac.com", group, company);
		
		when(productRepository.findByUniqueId(productId)).thenReturn(mockedProduct());
		bidService.place(bidForm, currentUser);
	}
	
	private List<Bid> mockedBids() {
		List<Bid> mockedBids = new ArrayList<>();
		Bid bid1 = new Bid();
		bid1.setCurrency(mock(Currency.class));
		bid1.setUnit(mock(Unit.class));
		bid1.setUser(mock(User.class));
		mockedBids.add(bid1);
		return mockedBids;
	}
	
}
