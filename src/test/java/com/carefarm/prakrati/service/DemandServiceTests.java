package com.carefarm.prakrati.service;

import static com.carefarm.prakrati.web.util.PrakratiConstants.DEMAND_STATUS_OPEN;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.jgroups.util.UUID;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.PageRequest;

import com.carefarm.prakrati.common.PrakratiTests;
import com.carefarm.prakrati.entity.Group;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.generators.UserNoGenerator;
import com.carefarm.prakrati.service.impl.DemandServiceImpl;
import com.carefarm.prakrati.util.DemandStatus;
import com.carefarm.prakrati.web.bean.CurrencyBean;
import com.carefarm.prakrati.web.bean.DemandBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.bean.TagBean;
import com.carefarm.prakrati.web.bean.UnitBean;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.model.DemandAssignForm;

public class DemandServiceTests extends PrakratiTests {
	
	private static final String[] STATUS_CODES = { "OPEN", "TENTATIVE" };
	
	private DemandService demandService;
	
	@Before
	public void setUp() {
		super.setUp();
		demandService = new DemandServiceImpl();
		this.setField("categoryRepository", demandService, categoryRepository);
		this.setField("companyRepository", demandService, companyRepository);
		this.setField("userRepository", demandService, userRepository);
		this.setField("demandRepository", demandService, demandRepository);
		this.setField("tagRepository", demandService, tagRepository);
		this.setField("demandStatusRepository", demandService, demandStatusRepository);
		this.setField("imageRepository", demandService, imageRepository);
		this.setField("demandHelper", demandService, demandHelper);
		this.setField("amazonS3Helper", demandService, amazonS3Helper);
		this.setField("mapper", demandService, mapper);
		this.setField("eventPublisher", demandService, eventPublisher);
	}
	
	@Test
	public void testFindAllTheDemands() {
		SearchRequest searchRequest = new SearchRequest();
		searchRequest.setPage(0);
		searchRequest.setPageSize(10);
		
		when(demandRepository.countAllOpenOnes(DEMAND_STATUS_OPEN)).thenReturn(1L);
		when(demandRepository.findAllOpenOnes(DEMAND_STATUS_OPEN, new PageRequest(0, 10))).thenReturn(mockedDemands());
		demandService.findByKeyword(searchRequest);
		verify(demandRepository).countAllOpenOnes(DEMAND_STATUS_OPEN);
		verify(demandRepository).findAllOpenOnes(DEMAND_STATUS_OPEN, new PageRequest(0, 10));
	}
	
	@Test
	public void testFindCompanyDemands() {
		String uniqueId = UUID.randomUUID().toString();
		SearchRequest searchRequest = new SearchRequest();
		searchRequest.setPage(0);
		searchRequest.setPageSize(10);
		searchRequest.setCompanyId(uniqueId);
		
		when(demandRepository.countByCompanyIdAndStatusCodes(uniqueId, STATUS_CODES)).thenReturn(1L);
		when(demandRepository.findByCompanyIdAndStatusCodes(uniqueId, STATUS_CODES, new PageRequest(0, 10))).thenReturn(mockedDemands());
		demandService.findByKeyword(searchRequest);
		verify(demandRepository).countByCompanyIdAndStatusCodes(uniqueId, STATUS_CODES);
		verify(demandRepository).findByCompanyIdAndStatusCodes(uniqueId, STATUS_CODES, new PageRequest(0, 10));
		
	}
	
	@Test
	public void testFindUserDemands() {
		String userNo = UserNoGenerator.generateUserNo();
		SearchRequest searchRequest = new SearchRequest();
		searchRequest.setPage(0);
		searchRequest.setPageSize(10);
		searchRequest.setUserNo(userNo);
		
		when(demandRepository.countByUserNoAndStatusCodes(userNo, STATUS_CODES)).thenReturn(1L);
		when(demandRepository.findByUserNoAndStatusCodes(userNo, STATUS_CODES, new PageRequest(0, 10))).thenReturn(mockedDemands());
		demandService.findByKeyword(searchRequest);
		verify(demandRepository).countByUserNoAndStatusCodes(userNo, STATUS_CODES);
		verify(demandRepository).findByUserNoAndStatusCodes(userNo, STATUS_CODES, new PageRequest(0, 10));
	}
	
	@Test
	public void testUpdate() {
		String uniqueId = UUID.randomUUID().toString();
		DemandBean demandBean = new DemandBean();
		demandBean.setUniqueId(uniqueId);
		demandBean.setQuantity("300.0");
		demandBean.setQuantityUnit(new UnitBean(1L , "Quintal"));
		demandBean.addTag(new TagBean("Cereals"));
		demandBean.setPrice(BigDecimal.valueOf(3400.0));
		demandBean.setPriceCurrency(new CurrencyBean(1L , "INR"));
		demandBean.setPriceUnit(new UnitBean(1L, "Quintal"));
		demandBean.setDeliveryDate("12/11/2016");
		
		when(demandRepository.findByUniqueId(uniqueId)).thenReturn(mockedDemand());
		demandService.update(demandBean, mockedUser());
		verify(demandRepository).findByUniqueId(uniqueId);
	}
	
	@Test
	public void testDelete() {
		String uniqueId = UUID.randomUUID().toString();
		
		when(demandRepository.findByUniqueId(uniqueId)).thenReturn(mockedDemand());
		demandService.delete(uniqueId, mockedUser());
		verify(demandRepository).findByUniqueId(uniqueId);
	}
	
	@Test
	public void testFindDetail() {
		String uniqueId = UUID.randomUUID().toString();
		
		when(demandRepository.findByUniqueId(uniqueId)).thenReturn(mockedDemand());
		DemandBean demandBean = demandService.findDetail(uniqueId);
		verify(demandRepository).findByUniqueId(uniqueId);
		Assert.assertThat("Demand should not be null", demandBean, is(notNullValue()));
	}
	
	@Test
	public void testAssignment() {
		String uniqueId = UUID.randomUUID().toString();
		String userNo = UserNoGenerator.generateUserNo();
		
		UserBean assignTo = new UserBean("Region", "Manager");
		assignTo.setUserNo(userNo);
		
		DemandAssignForm demandAssignment = new DemandAssignForm();
		demandAssignment.setAssignTo(assignTo);
		demandAssignment.setComments("Please look into this");
		demandAssignment.setUniqueId(uniqueId);
		demandAssignment.setUserNo(userNo);
		
		Group group = mockedGroup("Admin", "ROLE_USER", "ROLE_ADMIN");
		User currentUser = currentUser("admin@vocac.com", group);
		
		when(demandRepository.findByUniqueId(uniqueId)).thenReturn(mockedDemand());
		demandService.assign(demandAssignment, currentUser);
		verify(demandRepository).findByUniqueId(uniqueId);
	}
	
	@Test
	public void testMarkAsFulfilled() {
		String uniqueId = UUID.randomUUID().toString();
		
		Group group = mockedGroup("Admin", "ROLE_USER", "ROLE_ADMIN");
		User currentUser = currentUser("admin@vocac.com", group);
		
		when(demandRepository.findByUniqueId(uniqueId)).thenReturn(mockedDemand());
		when(demandStatusRepository.findByCode(DemandStatus.FULFILLED.getCode())).thenReturn(new com.carefarm.prakrati.common.entity.DemandStatus());
		demandService.markAsFulfilled(uniqueId, currentUser);
		verify(demandRepository).findByUniqueId(uniqueId);
		verify(demandStatusRepository).findByCode(DemandStatus.FULFILLED.getCode());
	}
	
}
