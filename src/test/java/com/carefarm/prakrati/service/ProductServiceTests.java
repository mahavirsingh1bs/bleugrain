package com.carefarm.prakrati.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.apache.jackrabbit.uuid.UUID;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.carefarm.prakrati.common.PrakratiTests;
import com.carefarm.prakrati.common.entity.Rating;
import com.carefarm.prakrati.entity.Category;
import com.carefarm.prakrati.entity.Group;
import com.carefarm.prakrati.entity.Product;
import com.carefarm.prakrati.entity.Review;
import com.carefarm.prakrati.entity.Tag;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.generators.UserNoGenerator;
import com.carefarm.prakrati.service.impl.ProductServiceImpl;
import com.carefarm.prakrati.web.bean.AttributeBean;
import com.carefarm.prakrati.web.bean.CategoryBean;
import com.carefarm.prakrati.web.bean.CurrencyBean;
import com.carefarm.prakrati.web.bean.ProductBean;
import com.carefarm.prakrati.web.bean.TagBean;
import com.carefarm.prakrati.web.bean.UnitBean;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.model.EnableBiddingForm;
import com.carefarm.prakrati.web.model.ProductAssignForm;
import com.carefarm.prakrati.web.model.ReviewForm;

public class ProductServiceTests extends PrakratiTests {
	
	private ProductService productService;
	
	@Before
	public void setUp() {
		super.setUp();
		
		productService = new ProductServiceImpl();
		
		this.setField("categoryRepository", productService, categoryRepository);
		this.setField("userRepository", productService, userRepository);
		this.setField("groupRepository", productService, groupRepository);
		
		this.setField("productRepository", productService, productRepository);
		this.setField("tagRepository", productService, tagRepository);
		this.setField("productStatusRepository", productService, productStatusRepository);
		this.setField("bidRepository", productService, bidRepository);
		this.setField("ratingRepository", productService, ratingRepository);
		
		this.setField("wishlistRepository", productService, wishlistRepository);
		this.setField("grainCartRepository", productService, grainCartRepository);
		this.setField("activityRepository", productService, activityRepository);
		
		this.setField("productHelper", productService, productHelper);
		this.setField("productEventHelper", productService, productEventHelper);
		
		this.setField("amazonS3Helper", productService, amazonS3Helper);
		this.setField("mapper", productService, mapper);
		this.setField("eventPublisher", productService, eventPublisher);
		
	}
	
	@Test
	public void testFindProductDetail() {
		String uniqueId = UUID.randomUUID().toString();
		
		Product product = mockedProduct(uniqueId, mockedStatus("Newly Added"));
		product.addCategory(new Category("Cereals"));
		product.addTag(new Tag("Cereals"));
		product.addReview(new Review(new Rating(5.0)));
		product.setOwner(mockedUser());
		
		when(productRepository.findByUniqueId(uniqueId)).thenReturn(product);
		productService.findProductDetail(uniqueId, null);
		verify(productRepository, times(1)).findByUniqueId(uniqueId);
	}
	
	@Test
	public void testUpdateProduct() {
		String uniqueId = UUID.randomUUID().toString();
		ProductBean bean = new ProductBean();
		bean.setId(1L);
		bean.addCategory(new CategoryBean("Cereals"));
		bean.addTag(new TagBean("Cereals"));
		bean.addAttribute(new AttributeBean("Weight", "500g"));
		
		Group group = mockedGroup("Admin", "ROLE_USER", "ROLE_ADMIN");
		User currentUser = currentUser("admin@vocac.com", group);
		
		when(productRepository.findOne(1L)).thenReturn(mockedProduct(uniqueId));
		productService.update(bean, currentUser);
		verify(productRepository, times(1)).findOne(1L);
	}
	
	@Test
	public void testDeleteProduct() {
		String uniqueId = UUID.randomUUID().toString();
		
		Group group = mockedGroup("Admin", "ROLE_USER", "ROLE_ADMIN");
		User currentUser = currentUser("admin@vocac.com", group);
		
		when(productRepository.findByUniqueId(uniqueId)).thenReturn(mockedProduct());
		productService.delete(uniqueId, currentUser);
		verify(productRepository, times(1)).findByUniqueId(uniqueId);
	}
	
	@Test
	public void testFindBiddableProductsWhenStateIsSelected() {
		String uniqueId = UUID.randomUUID().toString();
		Pageable pageable = new PageRequest(0, 10);
		when(productRepository.findBiddableByCategoryId(uniqueId, pageable)).thenReturn(mockedProducts());
		productService.findBiddableProducts(-1L, uniqueId, pageable);
		verify(productRepository, times(1)).findBiddableByCategoryId(uniqueId, pageable);
	}
	
	@Test
	public void testFindBiddableProductsWhenStateIsNotSelected() {
		String uniqueId = UUID.randomUUID().toString();
		Pageable pageable = new PageRequest(0, 10);
		when(productRepository.findBiddableByStateIdAndCategoryId(1L, uniqueId, pageable)).thenReturn(mockedProducts());
		productService.findBiddableProducts(1L, uniqueId, pageable);
		verify(productRepository, times(1)).findBiddableByStateIdAndCategoryId(1L, uniqueId, pageable);
	}
	
	@Test
	public void testEnableBidding() {
		String uniqueId = UUID.randomUUID().toString();
		String userNo = UserNoGenerator.generateUserNo();
		EnableBiddingForm enableBidding = new EnableBiddingForm();
		enableBidding.setBiddingCurrency(new CurrencyBean(1L, "Rs."));
		enableBidding.setBiddingUnit(new UnitBean(1L, "Quintal"));
		enableBidding.setEnableTillDate("12/11/2016");
		enableBidding.setInitialBidAmount(BigDecimal.valueOf(2300.0));
		enableBidding.setUniqueId(uniqueId);
		enableBidding.setUserNo(userNo);
		
		Group group = mockedGroup("Admin", "ROLE_USER", "ROLE_ADMIN");
		User currentUser = currentUser("admin@vocac.com", group);
		
		when(productRepository.findByUniqueId(uniqueId)).thenReturn(mockedProduct());
		productService.enableBidding(enableBidding, currentUser);
		verify(productRepository, times(1)).findByUniqueId(uniqueId);
	}
	
	@Test
	public void testSubmitReview() {
		String uniqueId = UUID.randomUUID().toString();
		
		ReviewForm review = new ReviewForm();
		review.setComment("Very Good Product");
		review.setEmailAddress("test@gmail.com");
		review.setName("Test Name");
		review.setRating(5.0);
		review.setUniqueId(uniqueId);
		
		Group group = mockedGroup("Admin", "ROLE_USER", "ROLE_ADMIN");
		User currentUser = currentUser("admin@vocac.com", group);
		
		when(productRepository.findByUniqueId(uniqueId)).thenReturn(mockedProduct());
		when(ratingRepository.findByValue(5.0)).thenReturn(mockedRating());

		productService.submitReview(review, currentUser);
		
		verify(productRepository, times(1)).findByUniqueId(uniqueId);
		verify(ratingRepository, times(1)).findByValue(5.0);
	}
	
	private Rating mockedRating() {
		Rating rating = new Rating(5.0);
		return rating;
	}
	
	@Test
	public void testAssignment() {
		String uniqueId = UUID.randomUUID().toString();
		String userNo = UserNoGenerator.generateUserNo();
		
		ProductAssignForm productAssignment = new ProductAssignForm();
		UserBean assignTo = new UserBean("Region", "Manager");
		assignTo.setUserNo(userNo);
		productAssignment.setAssignTo(assignTo);
		productAssignment.setComments("Please check the product availability");
		productAssignment.setUniqueId(uniqueId);
		
		Group group = mockedGroup("Admin", "ROLE_USER", "ROLE_ADMIN");
		User currentUser = currentUser("admin@vocac.com", group);
		
		when(productRepository.findByUniqueId(uniqueId)).thenReturn(mockedProduct());
		when(userRepository.findByUserNo(userNo)).thenReturn(mockedUser());
		productService.assign(productAssignment, currentUser);
		verify(productRepository, times(1)).findByUniqueId(uniqueId);
		verify(userRepository, times(1)).findByUserNo(userNo);
	}
	
	@Test
	public void testMarkAsSoldOut() {
		String uniqueId = UUID.randomUUID().toString();
		
		Group group = mockedGroup("Admin", "ROLE_USER", "ROLE_ADMIN");
		User currentUser = currentUser("admin@vocac.com", group);
		
		when(productRepository.findByUniqueId(uniqueId)).thenReturn(mockedProduct());
		productService.markAsSoldOut(uniqueId, currentUser);
		verify(productRepository, times(1)).findByUniqueId(uniqueId);
	}
}
