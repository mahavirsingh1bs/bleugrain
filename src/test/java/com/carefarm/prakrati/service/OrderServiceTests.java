package com.carefarm.prakrati.service;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.jgroups.util.UUID;
import org.junit.Before;
import org.junit.Test;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.carefarm.prakrati.common.PrakratiTests;
import com.carefarm.prakrati.entity.Group;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.generators.OrderNoGenerator;
import com.carefarm.prakrati.generators.UserNoGenerator;
import com.carefarm.prakrati.service.impl.OrderServiceImpl;
import com.carefarm.prakrati.web.bean.OrderDetailsBean;
import com.carefarm.prakrati.web.bean.OrderStatusBean;
import com.carefarm.prakrati.web.bean.SearchRequest;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.model.OrderAssignForm;
import com.carefarm.prakrati.web.model.OrderDispatchForm;
import com.carefarm.prakrati.web.model.OrderUpdateForm;

public class OrderServiceTests extends PrakratiTests {
	
	protected final String[] notInOrderStatuses = { "DELIVERED" };
	
	private OrderService orderService;
	
	@Before
	public void setUp() {
		super.setUp();
		orderService = new OrderServiceImpl();
		
		this.setField("itemHelper", orderService, itemHelper);
		this.setField("productHelper", orderService, productHelper);
		this.setField("orderHelper", orderService, orderHelper);
		
		this.setField("userRepository", orderService, userRepository);
		this.setField("orderRepository", orderService, orderRepository);
		this.setField("grainCartRepository", orderService, grainCartRepository);
		
		this.setField("productStatusRepository", orderService, productStatusRepository);
		this.setField("orderStatusRepository", orderService, orderStatusRepository);
		
		this.setField("mapper", orderService, mapper);
		this.setField("eventPublisher", orderService, eventPublisher);
	}
	
	@Test
	public void testFindOrdersByUser() {
		Long userId = 1L;
		
		when(orderRepository.findOrdersByUserId(userId)).thenReturn(mockedOrders());
		orderService.findOrdersByUser(userId);
		verify(orderRepository).findOrdersByUserId(userId);
	}
	
	@Test
	public void testFindCompanyOrdersByKeyword() {
		String uniqueId = UUID.randomUUID().toString();
		
		SearchRequest searchRequest = searchRequest();
		searchRequest.setCompanyId(uniqueId);
		
		Pageable pageable = new PageRequest(0, 10);
		when(orderRepository.countByCompanyIdAndStatusCodesNotIn(uniqueId, notInOrderStatuses)).thenReturn(10L);
		when(orderRepository.findByCompanyIdAndStatusCodesNotIn(uniqueId, notInOrderStatuses, pageable)).thenReturn(mockedOrders());
		orderService.findByKeyword(searchRequest);
		verify(orderRepository).countByCompanyIdAndStatusCodesNotIn(uniqueId, notInOrderStatuses);
		verify(orderRepository).findByCompanyIdAndStatusCodesNotIn(uniqueId, notInOrderStatuses, pageable);
	}

	private SearchRequest searchRequest() {
		SearchRequest searchRequest = new SearchRequest();
		searchRequest.setPage(0);
		searchRequest.setPageSize(10);
		return searchRequest;
	}
	
	@Test
	public void testFindUserOrdersByKeyword() {
		String userNo = UserNoGenerator.generateUserNo();
		
		SearchRequest searchRequest = searchRequest();
		searchRequest.setUserNo(userNo);
		
		Pageable pageable = new PageRequest(0, 10);
		when(orderRepository.countByUserNoAndStatusCodesNotIn(userNo, notInOrderStatuses)).thenReturn(10L);
		when(orderRepository.findByUserNoAndStatusCodesNotIn(userNo, notInOrderStatuses, pageable)).thenReturn(mockedOrders());
		orderService.findByKeyword(searchRequest);
		verify(orderRepository).countByUserNoAndStatusCodesNotIn(userNo, notInOrderStatuses);
		verify(orderRepository).findByUserNoAndStatusCodesNotIn(userNo, notInOrderStatuses, pageable);
	}
	
	@Test
	public void testFindAllOrdersByKeyword() {
		SearchRequest searchRequest = searchRequest();
		
		Pageable pageable = new PageRequest(0, 10);
		when(orderRepository.countByStatusCodesNotIn(notInOrderStatuses)).thenReturn(10L);
		when(orderRepository.findByStatusCodesNotIn(notInOrderStatuses, pageable)).thenReturn(mockedOrders());
		orderService.findByKeyword(searchRequest);
		verify(orderRepository).countByStatusCodesNotIn(notInOrderStatuses);
		verify(orderRepository).findByStatusCodesNotIn(notInOrderStatuses, pageable);
	}
	
	@Test
	public void testCompanysProcessOrder() {
		OrderDetailsBean orderDetailsBean = mockedOrderDetailsBean();
		
		Group group = mockedGroup("Company Admin", "ROLE_USER", "ROLE_COMPANY_ADMIN");
		User currentUser = currentUser("rjenith@vocac.com", group);
		
		when(grainCartRepository.findOne(1L)).thenReturn(mockedGrainCart());
		orderService.processOrder(orderDetailsBean, currentUser);
		verify(grainCartRepository).findOne(1L);
	}
	
	@Test
	public void testUsersProcessOrder() {
		OrderDetailsBean orderDetailsBean = mockedOrderDetailsBean();
		
		Group group = mockedGroup("Broker", "ROLE_USER", "ROLE_BROKER");
		User currentUser = currentUser("jbroker@vocac.com", group);
		
		when(grainCartRepository.findOne(1L)).thenReturn(mockedGrainCart());
		orderService.processOrder(orderDetailsBean, currentUser);
		verify(grainCartRepository).findOne(1L);
	}

	private OrderDetailsBean mockedOrderDetailsBean() {
		OrderDetailsBean orderDetailsBean = new OrderDetailsBean();
		orderDetailsBean.setGrainCartId(1L);
		orderDetailsBean.setAdvancedPayment(BigDecimal.valueOf(4000.0));
		orderDetailsBean.setBillingAddress(mockedAddressBean());
		orderDetailsBean.setShippingAddress(mockedAddressBean());
		orderDetailsBean.setBankAccount(mockedBankAccountBean());
		orderDetailsBean.setSubtotal(BigDecimal.valueOf(8600.0));
		return orderDetailsBean;
	}
	
	@Test
	public void testCustomersProcessOrder() {
		OrderDetailsBean orderDetailsBean = mockedOrderDetailsBean();
		
		Group group = mockedGroup("Broker", "ROLE_USER", "ROLE_BROKER");
		User currentUser = currentUser("jbroker@vocac.com", group);
		
		when(grainCartRepository.findOne(1L)).thenReturn(mockedConsumerGrainCart());
		orderService.processOrder(orderDetailsBean, currentUser);
		verify(grainCartRepository).findOne(1L);
	}
	
	@Test
	public void testFindDetail() {
		String orderNo = UUID.randomUUID().toString();
		
		when(orderRepository.findByOrderNo(orderNo)).thenReturn(mockedOrder());
		orderService.findDetail(orderNo);
		verify(orderRepository).findByOrderNo(orderNo);
	}
	
	@Test
	public void testDelete() {
		Long orderId = 1L;
		
		when(orderRepository.findOne(orderId)).thenReturn(mockedOrder());
		orderService.delete(orderId.toString());
		verify(orderRepository).findOne(orderId);
	}
	
	@Test
	public void testUpdateOrder() {
		String orderNo = OrderNoGenerator.generateOrderNo().toString();
		OrderUpdateForm orderUpdateForm = new OrderUpdateForm();
		
		UserBean deliverBy = new UserBean();
		deliverBy.setUserNo(UserNoGenerator.generateUserNo());
		orderUpdateForm.setDeliverBy(deliverBy);
		orderUpdateForm.setDeliveryDate("11/12/2016");
		orderUpdateForm.setOrderNo(orderNo);
		orderUpdateForm.setStatus(new OrderStatusBean(1L, "DISPATCHED"));
		
		Group group = mockedGroup("Broker", "ROLE_USER", "ROLE_BROKER");
		User currentUser = currentUser("jbroker@vocac.com", group);
		
		when(orderRepository.findByOrderNo(orderNo)).thenReturn(mockedOrder());
		orderService.updateOrder(orderUpdateForm, currentUser);
		verify(orderRepository).findByOrderNo(orderNo);
	}
	
	@Test
	public void testAssignOrder() {
		String orderNo = OrderNoGenerator.generateOrderNo().toString();
		OrderAssignForm orderAssignForm = new OrderAssignForm();
		
		UserBean assignTo = new UserBean();
		assignTo.setUserNo(UserNoGenerator.generateUserNo());
		orderAssignForm.setAssignTo(assignTo);
		orderAssignForm.setComments("Generate something");
		orderAssignForm.setOrderNo(orderNo);
		orderAssignForm.setUserNo(UserNoGenerator.generateUserNo());
		
		Group group = mockedGroup("Broker", "ROLE_USER", "ROLE_BROKER");
		User currentUser = currentUser("jbroker@vocac.com", group);
		
		when(orderRepository.findByOrderNo(orderNo)).thenReturn(mockedOrder());
		orderService.assignOrder(orderAssignForm, currentUser);
		verify(orderRepository).findByOrderNo(orderNo);
	}
	
	@Test
	public void testShipOrder() {
		String orderNo = OrderNoGenerator.generateOrderNo().toString();
		
		Group group = mockedGroup("Broker", "ROLE_USER", "ROLE_BROKER");
		User currentUser = currentUser("jbroker@vocac.com", group);
		
		when(orderRepository.findByOrderNo(orderNo)).thenReturn(mockedOrder());
		orderService.shipOrder(orderNo, currentUser);
		verify(orderRepository).findByOrderNo(orderNo);
	}
	
	@Test
	public void testDispatchOrder() {
		String orderNo = OrderNoGenerator.generateOrderNo().toString();
		
		UserBean deliverBy = new UserBean();
		deliverBy.setUserNo(UserNoGenerator.generateUserNo());
		
		OrderDispatchForm orderDispatchForm = new OrderDispatchForm();
		orderDispatchForm.setDeliverBy(deliverBy);
		orderDispatchForm.setDeliveryDate("11/12/2016");
		orderDispatchForm.setOrderNo(orderNo);
		orderDispatchForm.setUserNo(UserNoGenerator.generateUserNo());
		
		Group group = mockedGroup("Broker", "ROLE_USER", "ROLE_BROKER");
		User currentUser = currentUser("jbroker@vocac.com", group);
		
		when(orderRepository.findByOrderNo(orderNo)).thenReturn(mockedOrder());
		orderService.dispatchOrder(orderDispatchForm, currentUser);
		verify(orderRepository).findByOrderNo(orderNo);
	}
	
	@Test
	public void testDeliverOrder() {
		String orderNo = OrderNoGenerator.generateOrderNo().toString();
		
		Group group = mockedGroup("Broker", "ROLE_USER", "ROLE_BROKER");
		User currentUser = currentUser("jbroker@vocac.com", group);
		
		when(orderRepository.findByOrderNo(orderNo)).thenReturn(mockedOrder());
		orderService.deliverOrder(orderNo, currentUser);
		verify(orderRepository).findByOrderNo(orderNo);
	}
}
