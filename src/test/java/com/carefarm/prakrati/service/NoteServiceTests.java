package com.carefarm.prakrati.service;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import com.carefarm.prakrati.common.PrakratiTests;
import com.carefarm.prakrati.entity.Group;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.generators.UserNoGenerator;
import com.carefarm.prakrati.service.impl.NoteServiceImpl;
import com.carefarm.prakrati.web.model.NoteForm;

public class NoteServiceTests extends PrakratiTests {
	
	private NoteService noteService;
	
	@Before
	public void setUp() {
		super.setUp();
		noteService = new NoteServiceImpl();
		this.setField("noteRepository", noteService, noteRepository);
		this.setField("userRepository", noteService, userRepository);
		this.setField("mapper", noteService, mapper);
	}
	
	@Test
	public void testAddNote() {
		String userNo = UserNoGenerator.generateUserNo();
		
		NoteForm note = new NoteForm();
		note.setTitle("Learn Technologies");
		note.setNote("Learning new technologies");
		
		Group group = mockedGroup("Admin", "ROLE_USER", "ROLE_FARMER");
		User currentUser = currentUser("msingh@vocac.com", group);
		
		when(userRepository.findByUserNo(userNo)).thenReturn(mockedUser());
		noteService.addNote(note, userNo, currentUser);
		verify(userRepository, times(1)).findByUserNo(userNo);
	}
	
}
