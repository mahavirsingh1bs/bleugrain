package com.carefarm.prakrati.service;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

import com.carefarm.prakrati.common.PrakratiTests;
import com.carefarm.prakrati.entity.Image;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.generators.UserNoGenerator;
import com.carefarm.prakrati.service.helper.UserHelper;
import com.carefarm.prakrati.service.impl.UserServiceImpl;
import com.carefarm.prakrati.util.UserType;
import com.carefarm.prakrati.vo.ImageVO;
import com.carefarm.prakrati.web.bean.AddressBean;
import com.carefarm.prakrati.web.bean.CountryBean;
import com.carefarm.prakrati.web.bean.LanguageBean;
import com.carefarm.prakrati.web.bean.StateBean;
import com.carefarm.prakrati.web.bean.UserBean;
import com.carefarm.prakrati.web.model.ChangePasswdForm;

public class UserServiceTests extends PrakratiTests {

	private UserService userService;
	
	@Before
	public void setUp() {
		super.setUp();
		userService = new UserServiceImpl();
		userHelper = new UserHelper();
		this.setField("stateRepository", userHelper, stateRepository);
		this.setField("countryRepository", userHelper, countryRepository);
		this.setField("mapper", userHelper, mapper);
		this.setField("userHelper", userService, userHelper);
		this.setField("userRepository", userService, userRepository);
		this.setField("emailHandler", userService, emailHandler);
		this.setField("bankAccountRepository", userService, bankAccountRepository);
		this.setField("creditCardRepository", userService, creditCardRepository);
		this.setField("orderRepository", userService, orderRepository);
		this.setField("amazonS3Helper", userService, amazonS3Helper);
		this.setField("mapper", userService, mapper);
	}
	
	@Test
	public void testFindDetail() {
		String userNo = UserNoGenerator.generateUserNo();
		when(userRepository.findByUserNo(userNo)).thenReturn(mockedUser());
		userService.findDetail(userNo);
		verify(userRepository, times(1)).findByUserNo(userNo);
	}
	
	@Test
	public void testForgotPassword() {
		String emailAddress = "msingh@vocac.com";
		String contextPath = "http://www.vocac.com";
		when(userRepository.findByEmailAddress(emailAddress)).thenReturn(mockedUser());
		userService.forgotPassword(emailAddress, contextPath);
		verify(userRepository, times(1)).findByEmailAddress(emailAddress);
	}
	
	@Test
	public void testResetPassword() {
		String resetPasswdCode = UUID.randomUUID().toString();
		String newPassword = "Password@1234";
		when(userRepository.findByResetCode(resetPasswdCode)).thenReturn(mockedUser());
		userService.resetPasswd(newPassword, resetPasswdCode);
		verify(userRepository, times(1)).findByResetCode(resetPasswdCode);
	}
	
	@Test
	public void testValidateCode() {
		String resetPasswdCode = UUID.randomUUID().toString();
		when(userRepository.findByResetCode(resetPasswdCode)).thenReturn(mockedUser());
		userService.validateCode(resetPasswdCode);
		verify(userRepository, times(1)).findByResetCode(resetPasswdCode);
	}
	
	@Test
	public void testChangePassword() {
		String userNo = UserNoGenerator.generateUserNo();
		ChangePasswdForm changePasswdForm = new ChangePasswdForm();
		changePasswdForm.setUserId(userNo);
		changePasswdForm.setOldPasswd("PasswdOld@1234");
		changePasswdForm.setNewPasswd("PasswdNew@1234");
		changePasswdForm.setConfirmNewPasswd("PasswdNew@1234");
		
		when(userRepository.findByUserNo(userNo)).thenReturn(mockedUser());
		userService.changePasswd(changePasswdForm);
		verify(userRepository, times(1)).findByUserNo(userNo);
	}
	
	@Test
	public void testFindByMatchingKeywords() {
		String keywords = "msingh@vocac.com";
		when(userRepository.findByMatchingEmailAddressOrPhoneNoOrMobileNoOrUniqueId("%" + keywords + "%")).thenReturn(mockedUsers());
		userService.findByMatchingKeywords(keywords);
		verify(userRepository, times(1)).findByMatchingEmailAddressOrPhoneNoOrMobileNoOrUniqueId("%" + keywords + "%");
	}
	
	@Test
	public void testFindByMatchingKeywordsAndUserTypes() {
		String keywords = "msingh@vocac.com";
		when(userRepository.findByMatchingEmailAddressOrPhoneNoOrMobileNoOrUniqueIdAndUserType("%" + keywords + "%", UserType.FARMER)).thenReturn(mockedUsers());
		userService.findByMatchingKeywords(keywords, UserType.FARMER);
		verify(userRepository, times(1)).findByMatchingEmailAddressOrPhoneNoOrMobileNoOrUniqueIdAndUserType("%" + keywords + "%", UserType.FARMER);
	}
	
	@Test
	public void testFindReviews() {
		String userNo = UserNoGenerator.generateUserNo();
		when(userRepository.findByUserNo(userNo)).thenReturn(mockedUser());
		userService.findReviews(userNo);
		verify(userRepository, times(1)).findByUserNo(userNo);
	}
	
	@Test
	public void testFindOrders() {
		String uniqueId = UUID.randomUUID().toString();
		when(orderRepository.findByUniqueId(uniqueId)).thenReturn(mockedOrders());
		userService.findOrders(uniqueId);
		verify(orderRepository, times(1)).findByUniqueId(uniqueId);
	}
	
	@Test
	public void testFindSettings() {
		String userNo = UserNoGenerator.generateUserNo();
		User mockedUser = mockedUserWithBasicSettings();
		when(userRepository.findByUserNo(userNo)).thenReturn(mockedUser);
		userService.findSettings(userNo);
		verify(userRepository, times(1)).findByUserNo(userNo);
	}

	@Test
	public void testVerifyContactNo() {
		String uniqueCode = UUID.randomUUID().toString();
		String oneTimePasswd = "432489";
		
		User mockedUser = mockedUserWithBasicSettings();
		when(userRepository.findByUniqueCode(uniqueCode)).thenReturn(mockedUser);
		userService.verifyContactNo(uniqueCode, oneTimePasswd);
		verify(userRepository, times(1)).findByUniqueCode(uniqueCode);
	}
	
	@Test
	public void testVerifyEmailAddress() {
		String emailVerifCode = UUID.randomUUID().toString();
		
		User mockedUser = mockedUserWithBasicSettings();
		when(userRepository.findByEmailVerifCode(emailVerifCode)).thenReturn(mockedUser);
		userService.verifyEmailAddress(emailVerifCode);
		verify(userRepository, times(1)).findByEmailVerifCode(emailVerifCode);
	}
	
	@Test
	public void testChangeProfilePicture() {
		String userNo = UserNoGenerator.generateUserNo();
		
		ImageVO userImage = new ImageVO();
		
		User mockedUser = mockedUserWithBasicSettings();
		when(userRepository.findByUserNo(userNo)).thenReturn(mockedUser);
		when(amazonS3Helper.handleImage(userImage, mockedUser)).thenReturn(new Image());
		userService.changeProfilePicture(userImage, userNo, mockedUser());
		verify(userRepository, times(1)).findByUserNo(userNo);
		verify(amazonS3Helper, times(1)).handleImage(userImage, mockedUser);
	}
	
	@Test
	public void testFindCountrySettings() {
		String userNo = UserNoGenerator.generateUserNo();
		
		User mockedUser = mockedUserWithBasicSettings();
		when(userRepository.findByUserNo(userNo)).thenReturn(mockedUser);
		userService.findCountrySettings(userNo);
		verify(userRepository, times(1)).findByUserNo(userNo);
	}
	
	@Test
	public void testUpdate() {
		String userNo = UserNoGenerator.generateUserNo();
		UserBean user = mockedUserBean(userNo);
		
		User mockedUser = mockedUserWithBasicSettings();
		when(userRepository.findByUserNo(userNo)).thenReturn(mockedUser);
		userService.update(user, userNo, mockedUser());
		verify(userRepository, times(1)).findByUserNo(userNo);
	}

	private UserBean mockedUserBean(String userNo) {
		UserBean user = new UserBean();
		user.setUserNo(userNo);
		user.setMobileNo("919717818437");
		user.setPhoneNo("911247818437");
		user.setDefaultLanguage(new LanguageBean(1L, "English"));

		AddressBean address = new AddressBean();
		address.setState(new StateBean(1L, "Uttar Pradesh"));
		address.setCountry(new CountryBean(1L, "India"));
		user.setAddress(address);
		return user;
	}
	
	@Test
	public void testFindCurrentUser() {
		Long userId = 1L;
		User mockedUser = mockedUserWithBasicSettings();
		when(userRepository.findOne(userId)).thenReturn(mockedUser);
		UserBean currentUser = userService.findCurrentUser(userId);
		verify(userRepository, times(1)).findOne(userId);
		assertThat("Current User should not be null", currentUser, is(notNullValue()));
	}
	
}
