package com.carefarm.prakrati.service;

import static com.carefarm.prakrati.web.util.PrakratiConstants.COMPANY_ID;
import static com.carefarm.prakrati.web.util.PrakratiConstants.USER_ID;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import com.carefarm.prakrati.common.PrakratiTests;
import com.carefarm.prakrati.common.util.Constants;
import com.carefarm.prakrati.entity.Company;
import com.carefarm.prakrati.entity.Group;
import com.carefarm.prakrati.entity.User;
import com.carefarm.prakrati.generators.UserNoGenerator;
import com.carefarm.prakrati.service.helper.AddressEntityHelper;
import com.carefarm.prakrati.service.impl.AddressServiceImpl;
import com.carefarm.prakrati.util.AddressCategory;
import com.carefarm.prakrati.web.bean.AddressBean;
import com.carefarm.prakrati.web.bean.AddressEntityBean;
import com.carefarm.prakrati.web.bean.CountryBean;
import com.carefarm.prakrati.web.bean.StateBean;
import com.carefarm.prakrati.web.model.AddressEntityForm;
import com.carefarm.prakrati.web.model.AddressForm;
import com.carefarm.prakrati.web.util.PrakratiConstants;

public class AddressServiceTests extends PrakratiTests {
	
	private AddressService addressService;

	private AddressEntityHelper addressEntityHelper;
	
	@Before
	public void setUp() {
		super.setUp();
		addressEntityHelper = new AddressEntityHelper();
		addressService = new AddressServiceImpl();
		this.setField("stateRepository", addressEntityHelper, stateRepository);
		this.setField("countryRepository", addressEntityHelper, countryRepository);
		this.setField("userRepository", addressService, userRepository);
		this.setField("companyRepository", addressService, companyRepository);
		this.setField("stateRepository", addressService, stateRepository);
		this.setField("countryRepository", addressService, countryRepository);
		this.setField("addressRepository", addressService, addressRepository);
		this.setField("addressEntityHelper", addressService, addressEntityHelper);
		this.setField("mapper", addressService, mapper);
	}
	
	@Test
	public void testFindDeliveryAddressesOfCompany() {
		Map<String, Object> params = new HashMap<>();
		params.put(COMPANY_ID, 1L);
		when(addressRepository.findByCompanyIdAndCategory(1L, AddressCategory.DELIVERY)).thenReturn(mockedAddressEntities());
		addressService.findDeliveryAddresses(params);
		verify(addressRepository).findByCompanyIdAndCategory(1L, AddressCategory.DELIVERY);
	}
	
	@Test
	public void testFindDeliveryAddressesOfUser() {
		Map<String, Object> params = new HashMap<>();
		params.put(USER_ID, 1L);
		when(addressRepository.findByUserIdAndCategory(1L, AddressCategory.DELIVERY)).thenReturn(mockedAddressEntities());
		addressService.findDeliveryAddresses(params);
		verify(addressRepository).findByUserIdAndCategory(1L, AddressCategory.DELIVERY);
	}
	
	@Test
	public void testFindBillingAddressesOfCompany() {
		Map<String, Object> params = new HashMap<>();
		params.put(COMPANY_ID, 1L);
		when(addressRepository.findByCompanyIdAndCategory(1L, AddressCategory.BILLING)).thenReturn(mockedAddressEntities());
		addressService.findBillingAddresses(params);
		verify(addressRepository).findByCompanyIdAndCategory(1L, AddressCategory.BILLING);
	}
	
	@Test
	public void testFindBillingAddressesOfUser() {
		Map<String, Object> params = new HashMap<>();
		params.put(USER_ID, 1L);
		when(addressRepository.findByUserIdAndCategory(1L, AddressCategory.BILLING)).thenReturn(mockedAddressEntities());
		addressService.findBillingAddresses(params);
		verify(addressRepository).findByUserIdAndCategory(1L, AddressCategory.BILLING);
	}
	
	@Test
	public void testFindDefaultDeliveryAddressesOfCompany() {
		Map<String, Object> params = new HashMap<>();
		params.put(COMPANY_ID, 1L);
		when(addressRepository.findByCompanyIdAndCategoryAndType(1L, AddressCategory.DELIVERY, Boolean.TRUE)).thenReturn(mockedAddressEntities());
		addressService.findDefaultDeliveryAddress(params);
		verify(addressRepository).findByCompanyIdAndCategoryAndType(1L, AddressCategory.DELIVERY, Boolean.TRUE);
	}
	
	@Test
	public void testFindDefaultDeliveryAddressesOfUser() {
		Map<String, Object> params = new HashMap<>();
		params.put(USER_ID, 1L);
		when(addressRepository.findByUserIdAndCategoryAndType(1L, AddressCategory.DELIVERY, Boolean.TRUE)).thenReturn(mockedAddressEntities());
		addressService.findDefaultDeliveryAddress(params);
		verify(addressRepository).findByUserIdAndCategoryAndType(1L, AddressCategory.DELIVERY, Boolean.TRUE);
	}
	
	@Test
	public void testFindDefaultBillingAddressesOfCompany() {
		Map<String, Object> params = new HashMap<>();
		params.put(COMPANY_ID, 1L);
		when(addressRepository.findByCompanyIdAndCategoryAndType(1L, AddressCategory.BILLING, Boolean.TRUE)).thenReturn(mockedAddressEntities());
		addressService.findDefaultBillingAddress(params);
		verify(addressRepository).findByCompanyIdAndCategoryAndType(1L, AddressCategory.BILLING, Boolean.TRUE);
	}
	
	@Test
	public void testFindDefaultBillingAddressesOfUser() {
		Map<String, Object> params = new HashMap<>();
		params.put(USER_ID, 1L);
		when(addressRepository.findByUserIdAndCategoryAndType(1L, AddressCategory.BILLING, Boolean.TRUE)).thenReturn(mockedAddressEntities());
		addressService.findDefaultBillingAddress(params);
		verify(addressRepository).findByUserIdAndCategoryAndType(1L, AddressCategory.BILLING, Boolean.TRUE);
	}
	
	@Test
	public void testAddAddress() {
		String userNo = UserNoGenerator.generateUserNo();
		
		AddressForm address = mockedAddressForm();
		
		Group group = mockedGroup("Admin", "ROLE_USER", "ROLE_ADMIN");
		User currentUser = currentUser("admin@vocac.com", group);
		
		when(userRepository.findByUserNo(userNo)).thenReturn(mockedUser());
		addressService.addAddress(address, userNo, currentUser);
		verify(userRepository).findByUserNo(userNo);
	}

	private AddressForm mockedAddressForm() {
		AddressForm address = new AddressForm();
		address.setAddressLine1("Address Line 1");
		address.setAddressLine2("Address Line 2");
		address.setCity("City");
		address.setStateId(1L);
		address.setCountryId(1L);
		return address;
	}
	
	@Test
	public void testEditAddress() {
		String userNo = UserNoGenerator.generateUserNo();
		
		AddressBean address = mockedAddressBean();
		
		Group group = mockedGroup("Admin", "ROLE_USER", "ROLE_ADMIN");
		User currentUser = currentUser("admin@vocac.com", group);
		
		when(userRepository.findByUserNo(userNo)).thenReturn(mockedUser());
		addressService.editAddress(address, userNo, currentUser);
		verify(userRepository).findByUserNo(userNo);
	}

	@Test
	public void testAddBillingAddressOfCompany() {
		String userNo = UserNoGenerator.generateUserNo();
		
		AddressEntityForm address = mockedAddressEntityForm();
		
		Group group = mockedGroup("Admin", "ROLE_USER", "ROLE_COMPANY_ADMIN");
		User currentUser = currentUser("admin@vocac.com", group);
		
		User mockedUser = this.mockedUserWithBasicSettings();
		
		Company company = mockedCompany("Company Name", "84392874");
		company.addBillingAddress(mockedAddressEntity());
		
		mockedUser.setGroup(group);
		mockedUser.setCompany(company);
		mockedUser.addBillingAddress(mockedAddressEntity());
		
		when(userRepository.findByUserNo(userNo)).thenReturn(mockedUser);
		addressService.addBillingAddress(address, userNo, currentUser);
		verify(userRepository).findByUserNo(userNo);
	}
	
	@Test
	public void testAddDeliveryAddressOfCompany() {
		String userNo = UserNoGenerator.generateUserNo();
		
		AddressEntityForm address = mockedAddressEntityForm();
		
		Group group = mockedGroup("Admin", "ROLE_USER", "ROLE_COMPANY_ADMIN");
		User currentUser = currentUser("admin@vocac.com", group);
		
		User mockedUser = this.mockedUserWithBasicSettings();
		
		Company company = mockedCompany("Company Name", "84392874");
		company.addDeliveryAddress(mockedAddressEntity());
		
		mockedUser.setGroup(group);
		mockedUser.setCompany(company);
		
		when(userRepository.findByUserNo(userNo)).thenReturn(mockedUser);
		addressService.addDeliveryAddress(address, userNo, currentUser);
		verify(userRepository).findByUserNo(userNo);
	}
	
	@Test
	public void testAddBillingAddressOfUser() {
		String userNo = UserNoGenerator.generateUserNo();
		
		AddressEntityForm address = mockedAddressEntityForm();
		
		Group group = mockedGroup("Admin", "ROLE_USER", "ROLE_BROKER");
		User currentUser = currentUser("admin@vocac.com", group);
		
		User mockedUser = this.mockedUserWithBasicSettings();
		mockedUser.setGroup(group);
		mockedUser.addBillingAddress(mockedAddressEntity());
		
		when(userRepository.findByUserNo(userNo)).thenReturn(mockedUser);
		addressService.addBillingAddress(address, userNo, currentUser);
		verify(userRepository).findByUserNo(userNo);
	}
	
	@Test
	public void testAddDeliveryAddressOfUser() {
		String userNo = UserNoGenerator.generateUserNo();
		
		AddressEntityForm address = mockedAddressEntityForm();
		
		Group group = mockedGroup("Admin", "ROLE_USER", "ROLE_BROKER");
		User currentUser = currentUser("admin@vocac.com", group);
		
		User mockedUser = this.mockedUserWithBasicSettings();
		mockedUser.setGroup(group);
		mockedUser.addDeliveryAddress(mockedAddressEntity());
		
		when(userRepository.findByUserNo(userNo)).thenReturn(mockedUser);
		addressService.addDeliveryAddress(address, userNo, currentUser);
		verify(userRepository).findByUserNo(userNo);
	}

	@Test
	public void testEditAddressEntity() {
		String userNo = UserNoGenerator.generateUserNo();
		
		AddressEntityBean addressEntityBean = mockedAddressEntityBean();
		
		Group group = mockedGroup("Admin", "ROLE_USER", "ROLE_BROKER");
		User currentUser = currentUser("admin@vocac.com", group);
		
		User mockedUser = this.mockedUserWithBasicSettings();
		mockedUser.addDeliveryAddress(mockedAddressEntity());
		
		addressService.editAddress(addressEntityBean, userNo, currentUser);
	}
	
	@Test
	public void testSetAsDefaultBillingAddressOfUser() {
		String userNo = UserNoGenerator.generateUserNo();
		
		Group group = mockedGroup("Admin", "ROLE_USER", "ROLE_BROKER");
		User currentUser = currentUser("admin@vocac.com", group);
		
		User mockedUser = this.mockedUserWithBasicSettings();
		mockedUser.setGroup(group);
		mockedUser.addBillingAddress(mockedAddressEntity());
		
		when(userRepository.findByUserNo(userNo)).thenReturn(mockedUser);
		when(addressRepository.findOne(1L)).thenReturn(mockedAddressEntity());
		addressService.setDefaultAddress(1L, Constants.ADDR_CAT_BILLING, userNo, currentUser);
		verify(userRepository).findByUserNo(userNo);
		verify(addressRepository).findOne(1L);
	}

	@Test
	public void testSetAsDefaultDeliveryAddressOfUser() {
		String userNo = UserNoGenerator.generateUserNo();
		Group group = mockedGroup("Admin", "ROLE_USER", "ROLE_BROKER");
		User currentUser = currentUser("admin@vocac.com", group);
		
		User mockedUser = this.mockedUserWithBasicSettings();
		mockedUser.setGroup(group);
		mockedUser.addDeliveryAddress(mockedAddressEntity());
		
		when(userRepository.findByUserNo(userNo)).thenReturn(mockedUser);
		when(addressRepository.findOne(1L)).thenReturn(mockedAddressEntity());
		addressService.setDefaultAddress(1L, Constants.ADDR_CAT_DELIVERY, userNo, currentUser);
		verify(userRepository).findByUserNo(userNo);
		verify(addressRepository).findOne(1L);
	}

	@Test
	public void testSetAsDefaultBillingAddressOfCompany() {
		String userNo = UserNoGenerator.generateUserNo();
		Group group = mockedGroup("Admin", "ROLE_USER", "ROLE_COMPANY_ADMIN");
		User currentUser = currentUser("admin@vocac.com", group);
		
		Company company = mockedCompany("Company Name", "84392874");
		company.addBillingAddress(mockedAddressEntity());
		
		User mockedUser = this.mockedUserWithBasicSettings();
		mockedUser.setCompany(company);
		mockedUser.setGroup(group);
		
		when(userRepository.findByUserNo(userNo)).thenReturn(mockedUser);
		when(addressRepository.findOne(1L)).thenReturn(mockedAddressEntity());
		addressService.setDefaultAddress(1L, Constants.ADDR_CAT_BILLING, userNo, currentUser);
		verify(userRepository).findByUserNo(userNo);
		verify(addressRepository).findOne(1L);
	}

	@Test
	public void testSetAsDefaultDeliveryAddressOfCompany() {
		String userNo = UserNoGenerator.generateUserNo();
		Group group = mockedGroup("Admin", "ROLE_USER", "ROLE_COMPANY_ADMIN");
		User currentUser = currentUser("admin@vocac.com", group);
		
		Company company = mockedCompany("Company Name", "84392874");
		company.addDeliveryAddress(mockedAddressEntity());
		
		User mockedUser = this.mockedUserWithBasicSettings();
		mockedUser.setCompany(company);
		mockedUser.setGroup(group);
		
		when(userRepository.findByUserNo(userNo)).thenReturn(mockedUser);
		when(addressRepository.findOne(1L)).thenReturn(mockedAddressEntity());
		addressService.setDefaultAddress(1L, Constants.ADDR_CAT_DELIVERY, userNo, currentUser);
		verify(userRepository).findByUserNo(userNo);
		verify(addressRepository).findOne(1L);
	}
	
	@Test
	public void testDelete() {
		Group group = mockedGroup("Admin", "ROLE_USER", "ROLE_COMPANY_ADMIN");
		User currentUser = currentUser("admin@vocac.com", group);
		
		when(addressRepository.findOne(1L)).thenReturn(mockedAddressEntity());
		addressService.delete("1", currentUser);
		verify(addressRepository).findOne(1L);
	}
	
	@Test
	public void testFindAddressDetails() {
		when(addressRepository.findOne(1L)).thenReturn(mockedAddressEntity());
		addressService.findAddressDetails("1");
		verify(addressRepository).findOne(1L);
	}
	
	@Test
	public void updateAddressOfCompany() {
		Map<String, Object> params = new HashMap<>();
		params.put(PrakratiConstants.CATEGORY, AddressCategory.BILLING);
		params.put(PrakratiConstants.COMPANY_ID, 1L);
		
		Group group = mockedGroup("Admin", "ROLE_USER", "ROLE_COMPANY_ADMIN");
		User currentUser = currentUser("admin@vocac.com", group);
		
		when(addressRepository.findOne(1L)).thenReturn(mockedAddressEntity());
		when(companyRepository.findOne(1L)).thenReturn(mockedCompany("Company Name", "54893584"));
		addressService.updateAddress(mockedAddressEntityBean(), params, currentUser);
		verify(addressRepository).findOne(1L);
		verify(companyRepository).findOne(1L);
	}
	
	@Test
	public void updateAddressOfUser() {
		Map<String, Object> params = new HashMap<>();
		params.put(PrakratiConstants.CATEGORY, AddressCategory.BILLING);
		params.put(PrakratiConstants.USER_ID, 1L);
		
		Group group = mockedGroup("Admin", "ROLE_USER", "ROLE_COMPANY_ADMIN");
		User currentUser = currentUser("admin@vocac.com", group);
		
		when(addressRepository.findOne(1L)).thenReturn(mockedAddressEntity());
		when(userRepository.findOne(1L)).thenReturn(mockedUser());
		addressService.updateAddress(mockedAddressEntityBean(), params, currentUser);
		verify(addressRepository).findOne(1L);
		verify(userRepository).findOne(1L);
	}
	
	private AddressEntityBean mockedAddressEntityBean() {
		AddressEntityBean addressEntityBean = new AddressEntityBean();
		addressEntityBean.setAddressCategory(AddressCategory.BILLING);
		addressEntityBean.setId(1L);;
		addressEntityBean.setAddressLine1("Address Line 1");
		addressEntityBean.setAddressLine2("Address Line 2");
		addressEntityBean.setCity("City");
		addressEntityBean.setState(new StateBean(1L, "U.P."));
		addressEntityBean.setCountry(new CountryBean(1L, "India"));
		addressEntityBean.setDefaultAddress(Boolean.TRUE);
		return addressEntityBean;
	}
	
	private AddressEntityForm mockedAddressEntityForm() {
		AddressEntityForm address = new AddressEntityForm();
		address.setAddressLine1("Address Line 1");
		address.setAddressLine2("Address Line 2");
		address.setCity("City");
		address.setStateId(1L);
		address.setCountryId(1L);
		address.setDefaultAddress(Boolean.TRUE);
		return address;
	}
	
}
