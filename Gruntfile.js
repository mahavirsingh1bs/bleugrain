var pkgjson = require('./package.json');

var config = {
  pkg: pkgjson,
  app: 'src/main/resources/static/assets/js/vendor',
  css: 'src/main/resources/static/assets/css',
  plugins: 'src/main/resources/static/assets/plugins',
  modules: 'src/main/resources/static/assets/js/app',
  unify: 'src/main/resources/static/assets/js',
  dist: 'target'
}

module.exports = function (grunt) {

  // Configuration
  grunt.initConfig({
    config: config,
    pkg: config.pkg,
    bower: grunt.file.readJSON('./.bowerrc'),
    copy: {
      dist: {
       files: [{
         expand: true,
         cwd: '<%= config.app %>/jquery',
         src: 'dist/jquery.js',
         dest: '<%= config.dist %>'
       },
       {
         expand: true,
         cwd: '<%= config.app %>/angular',
         src: 'angular.js',
         dest: '<%= config.dist %>'
       }]
      }
    },
    concat: {
    	options: {
    		stripBanners: true,
      		banner: '/*! <%= pkg.name %> - v<%= pkg.version %> - ' +
        		'<%= grunt.template.today("yyyy-mm-dd") %> */',
        	separator: ';\r\n',
    	},
    	vstyles: {
	        src: [
	        	'<%= config.plugins %>/animate.css',
	        	'<%= config.plugins %>/line-icons/line-icons.css',
	        	'<%= config.plugins %>/brand-buttons/brand-buttons.css',
	        	'<%= config.plugins %>/brand-buttons/brand-buttons-inversed.css',
	        	'<%= config.plugins %>/owl-carousel/owl-carousel/owl.carousel.css',
	        	'<%= config.plugins %>/scrollbar/css/jquery.mCustomScrollbar.css',
	        	'<%= config.plugins %>/sky-forms-pro/skyforms/css/sky-forms.css',
	        	'<%= config.plugins %>/sky-forms-pro/skyforms/custom/custom-sky-forms.css',
	        	'<%= config.plugins %>/master-slider/masterslider/style/masterslider.css',
	        	'<%= config.plugins %>/master-slider/masterslider/skins/default/style.css',
	        	'<%= config.plugins %>/jquery-steps/css/custom-jquery.steps.css',
	        	'<%= config.plugins %>/cube-portfolio/cubeportfolio/custom/custom-cubeportfolio.css',
	        	'<%= config.app %>/ng-img-crop/compile/minified/ng-img-crop.css',
	        ],
	        dest: '<%= config.css %>/vstyles.min.css',
	        nonull: true,
    	},
    	app: {
	      src: [
				'<%= config.css %>/style.css',
				'<%= config.css %>/headers/header-default.css',
				'<%= config.css %>/footers/footer-v1.css',
				'<%= config.css %>/pages/page_search.css',
				'<%= config.css %>/pages/page_about.css',
				'<%= config.css %>/pages/page_contact.css',
				'<%= config.css %>/pages/pricing/pricing_v1.css',
				'<%= config.css %>/pages/pricing/pricing_v4.css',
				'<%= config.css %>/pages/page_log_reg_v1.css',
				'<%= config.css %>/pages/profile.css',
				'<%= config.css %>/pages/shortcode_timeline2.css',
				'<%= config.css %>/pages/page_search_inner_tables.css',
				'<%= config.css %>/css-rtl/pages/page_job-rtl.css',
				'<%= config.css %>/search/search.css',
				'<%= config.css %>/pages/page_invoice.css',	
				'<%= config.css %>/shopping_cart.css',
				'<%= config.css %>/product_header.css',
				'<%= config.css %>/product_detail.css',
			],
	    	dest: '<%= config.css %>/app.min.css',
	    	nonull: true,
    	},
    	vendor: {
	      src: [
	      	'<%= bower.directory %>/jquery/dist/jquery.min.js',
			'<%= bower.directory %>/bootstrap/dist/js/bootstrap.min.js',
			'<%= bower.directory %>/angular/angular.min.js',
			'<%= bower.directory %>/angular-animate/angular-animate.min.js',
			'<%= bower.directory %>/angular-bootstrap/ui-bootstrap.min.js',
			'<%= bower.directory %>/angular-bootstrap/ui-bootstrap-tpls.min.js',
			'<%= bower.directory %>/angular-ui-router/release/angular-ui-router.min.js',
			'<%= bower.directory %>/angular-cookies/angular-cookies.min.js',
			'<%= bower.directory %>/angular-chart.js/dist/angular-chart.min.js',
			'<%= bower.directory %>/ng-file-upload/ng-file-upload.min.js',
			'<%= bower.directory %>/angular-sanitize/angular-sanitize.min.js',
			'<%= bower.directory %>/angular-touch/angular-touch.min.js',
			'<%= bower.directory %>/formatter/dist/jquery.formatter.min.js',
			'<%= bower.directory %>/angular-ui-mask/dist/mask.min.js',
			'<%= bower.directory %>/angular-translate/angular-translate.min.js',
			'<%= bower.directory %>/angular-translate-loader-url/angular-translate-loader-url.min.js',
			'<%= bower.directory %>/tr-ng-grid/trNgGrid.min.js',
			'<%= bower.directory %>/intl-tel-input/build/js/intlTelInput.min.js',
			'<%= bower.directory %>/international-phone-number/releases/international-phone-number.min.js',
			'<%= bower.directory %>/angular-input-masks/angular-input-masks-standalone.min.js',
			'<%= bower.directory %>/ng-droplet/dist/ng-droplet.min.js',
			'<%= bower.directory %>/ngToast/dist/ngToast.min.js',
			'<%= bower.directory %>/ng-tags-input/ng-tags-input.min.js',
			'<%= bower.directory %>/sockjs/sockjs.min.js',
			'<%= bower.directory %>/stomp-websocket/lib/stomp.min.js',
			'<%= bower.directory %>/lodash/dist/lodash.min.js',
			'<%= bower.directory %>/forge/forge.min.js',
			'<%= bower.directory %>/phoneformat/phoneformat.min.js',
			'<%= bower.directory %>/cropper/dist/cropper.min.js',
			'<%= bower.directory %>/ng-cropper/dist/ngCropper.all.min.js',
			'<%= bower.directory %>/ng-file-upload/ng-file-upload-shim.min.js',
			'<%= bower.directory %>/angular-elevate-zoom/dist/angular-elevate-zoom.min.js',
			'<%= bower.directory %>/moment/min/moment.min.js',
			'<%= bower.directory %>/ng-idle/angular-idle.min.js',
	      ],
	      dest: '<%= bower.directory %>/vendor.min.js',
	      nonull: true,
	    },
	    plugins: {
	      src: [
	      	'<%= config.plugins %>/extras.min.js',
            '<%= config.plugins %>/noUiSlider/jquery.nouislider.all.min.js',
            '<%= config.plugins %>/scrollbar/js/jquery.mCustomScrollbar.concat.min.js',
            '<%= config.plugins %>/counter/waypoints.min.js',
            '<%= config.plugins %>/counter/jquery.counterup.min.js',
            '<%= config.plugins %>/owl-carousel/owl-carousel/owl.carousel.min.js',
            '<%= config.plugins %>/cube-portfolio/cubeportfolio/js/jquery.cubeportfolio.min.js',
            '<%= config.plugins %>/master-slider/masterslider/masterslider.min.js',
            '<%= config.plugins %>/master-slider/masterslider/jquery.easing.min.js',
            '<%= config.plugins %>/jquery-steps/build/jquery.steps.min.js',
            '<%= config.plugins %>/sky-forms-pro/skyforms/js/jquery-ui.min.js',
            '<%= config.plugins %>/sky-forms-pro/skyforms/js/jquery.validate.min.js',
	      ],
	      dest: '<%= config.plugins %>/plugins.min.js',
	      nonull: true,
	    },
	    unify: {
	      src: [
	      	'<%= config.unify %>/unify.js',
			'<%= config.unify %>/plugins/mouse-wheel.js',
			'<%= config.unify %>/plugins/owl-carousel.js',
			'<%= config.unify %>/plugins/parallax-slider.js',
			'<%= config.unify %>/plugins/master-slider.js',
			'<%= config.unify %>/forms/contact.js',
			'<%= config.unify %>/pages/page_contacts.js',
			'<%= config.unify %>/plugins/stepWizard.js',
			'<%= config.unify %>/plugins/cube-portfolio/cube-portfolio-4.js',
			'<%= config.unify %>/forms/product-quantity.js',
			'<%= config.unify %>/plugins/circles-master.js',
	      ],
	      dest: '<%= config.plugins %>/unify.min.js',
	      nonull: true,
	    },
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> lib - v<%= pkg.version %> -' +
          '<%= grunt.template.today("yyyy-mm-dd") %> */'
      },
      extras: {
        files: {
          '<%= config.plugins %>/extras.min.js': [
          	'<%= config.plugins %>/back-to-top.js',
			'<%= config.plugins %>/smoothScroll.js',
			'<%= config.plugins %>/jquery.parallax.js',
			'<%= config.plugins %>/parallax-slider/js/modernizr.js',
			'<%= config.plugins %>/parallax-slider/js/jquery.cslider.js',
			'<%= config.plugins %>/circles-master/circles.js',
          ]
        }
      },
      modules: {
        files: {
          '<%= config.modules %>/modules.min.js': [
			'<%= config.modules %>/app/AppModule.js',
			'<%= config.modules %>/app/core/CoreDirectives.js',
			'<%= config.modules %>/app/core/CoreFilters.js',
			'<%= config.modules %>/app/core/CoreService.js',
			'<%= config.modules %>/app/core/CoreConstants.js',
			'<%= config.modules %>/app/domain/GrainCart.js',
			'<%= config.modules %>/app/directives/utility.js',
			'<%= config.modules %>/app/core/CoreModule.js',
			'<%= config.modules %>/app/modules/UserPrefsModule.js',
			'<%= config.modules %>/app/shared/address/AddressModule.js',
			'<%= config.modules %>/app/shared/discussion/DiscussionModule.js',
			'<%= config.modules %>/app/shared/expertise/ExpertiseModule.js',
			'<%= config.modules %>/app/shared/note/NoteModule.js',
			'<%= config.modules %>/app/shared/social-account/SocialAccountModule.js',
			'<%= config.modules %>/app/shared/NotificationModule.js',
			'<%= config.modules %>/app/shared/UpcomingEventModule.js',
			'<%= config.modules %>/app/profile/ProfileModule.js',
			'<%= config.modules %>/app/profile/HistoryModule.js',
			'<%= config.modules %>/app/profile/ReviewModule.js',
			'<%= config.modules %>/app/brokers/BrokerModule.js',
			'<%= config.modules %>/app/customers/CustomerModule.js',
			'<%= config.modules %>/app/companies/AgentModule.js',
			'<%= config.modules %>/app/companies/CompanyAdminModule.js',
			'<%= config.modules %>/app/companies/CompanyModule.js',
			'<%= config.modules %>/app/companies/BranchModule.js',
			'<%= config.modules %>/app/employees/EmployeeModule.js',
			'<%= config.modules %>/app/employees/SuperAdminModule.js',
			'<%= config.modules %>/app/farmers/FarmerModule.js',
			'<%= config.modules %>/app/retailers/RetailerModule.js',
			'<%= config.modules %>/app/users/UserModule.js',
			'<%= config.modules %>/app/modules/ServiceModule.js',
			'<%= config.modules %>/app/modules/ConsumerProductModule.js',
			'<%= config.modules %>/app/payment-options/PaymentModule.js',
			'<%= config.modules %>/app/site-admin/CategoryModule.js',
			'<%= config.modules %>/app/site-admin/MenuitemModule.js',
			'<%= config.modules %>/app/site-admin/CountryModule.js',
			'<%= config.modules %>/app/site-admin/StateModule.js',
			'<%= config.modules %>/app/site-admin/BankModule.js',
			'<%= config.modules %>/app/org-admin/DepartmentModule.js',
			'<%= config.modules %>/app/org-admin/DesignationModule.js',
			'<%= config.modules %>/app/org-admin/GroupModule.js',
			'<%= config.modules %>/app/org-admin/RoleModule.js',
			'<%= config.modules %>/app/checkout/CheckoutModule.js',
			'<%= config.modules %>/app/checkout/GrainCartModule.js',
			'<%= config.modules %>/app/bank-accounts/BankAccountModule.js',
			'<%= config.modules %>/app/credit-cards/CreditCardModule.js',
			'<%= config.modules %>/app/demands/DemandModule.js',
			'<%= config.modules %>/app/orders/OrderModule.js',
			'<%= config.modules %>/app/products/ProductModule.js',
			'<%= config.modules %>/app/pricing/PricingModule.js',
			'<%= config.modules %>/app/wishlists/WishlistModule.js',
			'<%= config.modules %>/app/search/modules/ProductSearchModule.js',
			'<%= config.modules %>/app/search/modules/DemandSearchModule.js',
			'<%= config.modules %>/app/search/services/SearchService.js',
			'<%= config.modules %>/app/others/OthersModule.js',
			'<%= config.modules %>/app/bidding/modules/BidModule.js',
			'<%= config.modules %>/app/modules/UtilityModule.js',
			'<%= config.modules %>/app/services/GrainCartService.js',
			'<%= config.modules %>/app/services/SharedService.js',
			'<%= config.modules %>/app/services/AccessCtrlModule.js',
			'<%= config.modules %>/app/services/SharedService.js',
			'<%= config.modules %>/app/services/SettingsService.js',
			'<%= config.modules %>/app/services/HttpModule.js',
			'<%= config.modules %>/app/services/CipherService.js',
			'<%= config.modules %>/app/shared/services/CategoryService.js',
			'<%= config.modules %>/app/shared/services/MenuitemService.js',
			'<%= config.modules %>/app/shared/services/CountryService.js',
			'<%= config.modules %>/app/shared/services/StateService.js',
			'<%= config.modules %>/app/shared/services/BusinessCategoryService.js',
			'<%= config.modules %>/app/shared/services/RoleService.js',
			'<%= config.modules %>/app/core/listeners/NotificationListener.js',
			'<%= config.modules %>/app/bidding/listeners/BidListener.js',
			'<%= config.modules %>/app/bidding/listeners/ProductListener.js',
			'<%= config.modules %>/app/services/UtilityService.js'
          ]
        }
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  grunt.registerTask('default', [
    'copy',
    'concat:vstyles',
    'concat:app',
    'concat:vendor',
    'uglify:extras',
    'uglify:modules',
    'concat:plugins',
    'concat:unify',
  ]);
};
